//
//  LocationViewController.swift
//  rideally
//
//  Created by Sarav on 18/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LocationViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    
    let view1 = UIView()
    let lblTitle1 = UILabel()
    let lblCurrentLocation = UILabel()
    let btnMap = UIButton()
    
    let view2 = UIView()
    let lblTitle2 = UILabel()
    
    let lblHomeTitle = UILabel()
    let lblHomeLocation = UILabel()
    
    let lblWorkTitle = UILabel()
    let lblWorkLocation = UILabel()
    
    var onSelectionBlock: actionBlockWithParam?
    var isFromWP: Bool = false
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Choose Location"
        self.view.backgroundColor = Colors.WHITE_COLOR
        
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        
        
        view1.translatesAutoresizingMaskIntoConstraints = false
        lblTitle1.translatesAutoresizingMaskIntoConstraints = false
        
        view1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        view1.translatesAutoresizingMaskIntoConstraints = false
        lblTitle1.text = "CURRENT LOCATION"
        lblTitle1.font = boldFontWithSize(15)
        view1.addSubview(lblTitle1)
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[title]-10-|", options: [], metrics: nil, views: ["title":lblTitle1]))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title]|", options: [], metrics: nil, views: ["title":lblTitle1]))
        self.view.addSubview(view1)
        
        lblCurrentLocation.translatesAutoresizingMaskIntoConstraints = false
        lblCurrentLocation.font = boldFontWithSize(13)
        lblCurrentLocation.textColor = Colors.GRAY_COLOR
        lblCurrentLocation.text = ""
        self.view.addSubview(lblCurrentLocation)
        
        btnMap.translatesAutoresizingMaskIntoConstraints = false
        btnMap.setImage(UIImage(named: "map"), for: UIControlState())
        btnMap.addTarget(self, action: #selector(gotoMap), for: .touchDown)
        self.view.addSubview(btnMap)
        
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblTitle2.translatesAutoresizingMaskIntoConstraints = false
        
        lblTitle2.text = "FAVOURITES"
        lblTitle2.font = boldFontWithSize(15)
        view2.addSubview(lblTitle2)
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[title]-10-|", options: [], metrics: nil, views: ["title":lblTitle2]))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title]|", options: [], metrics: nil, views: ["title":lblTitle2]))
        self.view.addSubview(view2)
        
        
        lblHomeTitle.translatesAutoresizingMaskIntoConstraints = false
        lblHomeTitle.text = "HOME"
        lblHomeTitle.font = boldFontWithSize(14)
        self.view.addSubview(lblHomeTitle)
        lblHomeLocation.translatesAutoresizingMaskIntoConstraints = false
        lblHomeLocation.text = UserInfo.sharedInstance.homeAddress
        lblHomeLocation.textColor = Colors.GRAY_COLOR
        lblHomeLocation.font = boldFontWithSize(13)
        self.view.addSubview(lblHomeLocation)
        
        lblWorkTitle.translatesAutoresizingMaskIntoConstraints = false
        lblWorkTitle.text = "WORK"
        lblWorkTitle.font = boldFontWithSize(14)
        self.view.addSubview(lblWorkTitle)
        lblWorkLocation.translatesAutoresizingMaskIntoConstraints = false
        lblWorkLocation.text = UserInfo.sharedInstance.workAddress
        lblWorkLocation.textColor = Colors.GRAY_COLOR
        lblWorkLocation.font = boldFontWithSize(13)
        self.view.addSubview(lblWorkLocation)
        
        lblHomeTitle.isHidden = true
        lblHomeLocation.isHidden = true
        lblWorkTitle.isHidden = true
        lblWorkLocation.isHidden = true
        
        lblCurrentLocation.isUserInteractionEnabled = true
        let currentLocationGesture = UITapGestureRecognizer(target: self, action: #selector(currentLocationSelected))
        lblCurrentLocation.addGestureRecognizer(currentLocationGesture)
        
        lblHomeLocation.isUserInteractionEnabled = true
        lblHomeTitle.isUserInteractionEnabled = true
        let homeTitleGesture = UITapGestureRecognizer(target: self, action: #selector(homeSelected))
        lblHomeTitle.addGestureRecognizer(homeTitleGesture)
        let homeLocationGesture = UITapGestureRecognizer(target: self, action: #selector(homeSelected))
        lblHomeLocation.addGestureRecognizer(homeLocationGesture)
        
        lblWorkLocation.isUserInteractionEnabled = true
        lblWorkTitle.isUserInteractionEnabled = true
        let workTitleGesture = UITapGestureRecognizer(target: self, action: #selector(workSelected))
        lblWorkTitle.addGestureRecognizer(workTitleGesture)
        let workLocationGesture = UITapGestureRecognizer(target: self, action: #selector(workSelected))
        lblWorkLocation.addGestureRecognizer(workLocationGesture)
        
        let viewsDict = ["view1": view1, "lblcurrent":lblCurrentLocation, "map":btnMap, "view2":view2, "hometitle":lblHomeTitle, "homelocation":lblHomeLocation, "worktitle":lblWorkTitle, "worklocation":lblWorkLocation]
        
        if((UserInfo.sharedInstance.homeAddress != "") && (UserInfo.sharedInstance.workAddress != "")){
            lblHomeTitle.isHidden = false
            lblHomeLocation.isHidden = false
            lblWorkTitle.isHidden = false
            lblWorkLocation.isHidden = false
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view1(30)]-5-[lblcurrent(25)]-5-[view2(30)]-5-[hometitle][homelocation]-5-[worktitle][worklocation]", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[hometitle]-10-|", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[homelocation]-10-|", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[worktitle]-10-|", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[worklocation]-10-|", options: [], metrics: nil, views: viewsDict))
        }else if(UserInfo.sharedInstance.homeAddress != ""){
            lblHomeTitle.isHidden = false
            lblHomeLocation.isHidden = false
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view1(30)]-5-[lblcurrent(25)]-5-[view2(30)]-5-[hometitle][homelocation]", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[hometitle]-10-|", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[homelocation]-10-|", options: [], metrics: nil, views: viewsDict))
        }
        else if(UserInfo.sharedInstance.workAddress != ""){
            lblWorkTitle.isHidden = false
            lblWorkLocation.isHidden = false
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view1(30)]-5-[lblcurrent(25)]-5-[view2(30)]-5-[worktitle][worklocation]", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[worktitle]-10-|", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[worklocation]-10-|", options: [], metrics: nil, views: viewsDict))
        }
        else{
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view1(30)]-5-[lblcurrent(25)]-5-[view2(30)]", options: [], metrics: nil, views: viewsDict))
        }
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[map(30)]", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view1]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view2]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblcurrent]-10-[map(30)]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraint(NSLayoutConstraint(item: btnMap, attribute: .centerY, relatedBy: .equal, toItem: lblCurrentLocation, attribute: .centerY, multiplier: 1, constant: 0))
        
        getCurrentLocation()
    }
    
    func getCurrentLocation () {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .denied {
                //                AlertController.showAlertForMessage("You have denied access to your location, please enable it in settings.")
                callLocationServiceEnablePopUp()
                return
            }
        }
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else {
            print("location service not enabled")
        }
    }
    
    func callLocationServiceEnablePopUp() -> Void
    {
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedWhenInUse, .restricted, .denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order to be notified, please open this app's settings and set location access to 'Always'.",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            self.present(alertController, animated: true, completion: nil)
        default:
            break
            
            
        }
    }
    
    // MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = manager.location{
            self.locationCoord = location.coordinate
            reverseGeocodeCoordinate(self.locationCoord)
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        let geocoder = GMSGeocoder()
        lblCurrentLocation.lock()
        geocoder.reverseGeocodeCoordinate(coord) { [weak self] response , error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]!
                self?.lblCurrentLocation.text = lines?.joined(separator: "")
                self?.lblCurrentLocation.unlock()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            //AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
            callLocationServiceEnablePopUp()
        }
    }
    
    func gotoMap()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = MapViewController()
            vc.jumpOneScreen = true
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.isFromWP = self.isFromWP
            vc.onSelectionBlock = { (location) -> Void in
                self.onSelectionBlock?(location)
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func currentLocationSelected()
    {
        if(lblCurrentLocation.text != ""){
            onSelectionBlock?(["lat":"\(locationCoord!.latitude)", "long":"\(locationCoord!.longitude)", "location":lblCurrentLocation.text!])
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func homeSelected()
    {
        onSelectionBlock?(["lat":"\(UserInfo.sharedInstance.homeLatValue)", "long":"\(UserInfo.sharedInstance.homeLongValue)", "location":UserInfo.sharedInstance.homeAddress])
        _ = self.navigationController?.popViewController(animated: true)
    }
    func workSelected()
    {
        onSelectionBlock?(["lat":"\(UserInfo.sharedInstance.workLatValue)", "long":"\(UserInfo.sharedInstance.workLongValue)", "location":UserInfo.sharedInstance.workAddress])
        _ = self.navigationController?.popViewController(animated: true)
    }
}
