//
//  MobileVerificationViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/20/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class MobileVerificationViewController: BaseScrollViewController {
    
    let imgMobVer = UIImageView()
    let lblOtp = UILabel()
    var mobileNumberTextBox = RATextField()
    var lblMobileCode = UILabel()
    var editImg = UIImageView()
    var otpTextBox = RATextField()
    var referralLabelText = UILabel()
    var referralTextBox = RATextField()
    var referralUserName = UILabel()
    var referralApplyText = UILabel()
    var referralInfoImage = UIImageView()
    var referredBy = ""
    var isInvalidCode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Mobile Verification"
        addViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .Plain, target: self, action: #selector(goBack))
    }
    
    func addViews() {
        if(UserInfo.sharedInstance.mobile == "") {
            viewsArray.append(getFbTopView())
            addBottomView(getFbBottomView())
        } else {
            viewsArray.append(getTopView())
            addBottomView(getBottomView())
        }
    }
    
    func getTopView() -> SubView
    {
        let view = UIView()
        
        imgMobVer.image = UIImage(named: "lock_green")
        imgMobVer.translatesAutoresizingMaskIntoConstraints = false
        imgMobVer.backgroundColor = UIColor.clear
        imgMobVer.layer.cornerRadius = 50
        imgMobVer.layer.masksToBounds = true
        view.addSubview(imgMobVer)
        
        lblOtp.text = "OTP sent to"
        lblOtp.translatesAutoresizingMaskIntoConstraints = false
        lblOtp.textColor = Colors.GRAY_COLOR
        lblOtp.textAlignment = .center
        lblOtp.font = normalFontWithSize(16)
        view.addSubview(lblOtp)
        
        lblMobileCode.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        lblMobileCode.text = "+91"
        lblMobileCode.textColor = Colors.GRAY_COLOR
        lblMobileCode.font = boldFontWithSize(15)
        lblMobileCode.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblMobileCode)
        
        let image = UIImage(named: "edit_gray")
        editImg.image = image
        editImg.contentMode = UIViewContentMode.center
        let editImgtapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(editClicked))
        editImg.isUserInteractionEnabled = true
        editImg.translatesAutoresizingMaskIntoConstraints = false
        editImg.addGestureRecognizer(editImgtapGestureRecognizer)
        view.addSubview(editImg)
        
        mobileNumberTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        mobileNumberTextBox.text = UserInfo.sharedInstance.mobile
        mobileNumberTextBox.font = normalFontWithSize(15)!
        mobileNumberTextBox.textColor = Colors.GRAY_COLOR
        mobileNumberTextBox.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberTextBox.returnKeyType = UIReturnKeyType.next
        mobileNumberTextBox.delegate = self
        mobileNumberTextBox.placeholder = "ENTER MOBILE NUMBER"
        mobileNumberTextBox.isUserInteractionEnabled = false
        view.addSubview(mobileNumberTextBox)
        
        otpTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        otpTextBox.font = normalFontWithSize(15)!
        otpTextBox.textColor = Colors.GRAY_COLOR
        otpTextBox.translatesAutoresizingMaskIntoConstraints = false
        otpTextBox.returnKeyType = UIReturnKeyType.done
        otpTextBox.delegate = self
        otpTextBox.placeholder = "OTP"
        view.addSubview(otpTextBox)
        
        let viewsDict = ["pic":imgMobVer, "name":lblOtp, "lblMob":lblMobileCode, "txtMob":mobileNumberTextBox, "iEdit":editImg, "txtOtp":otpTextBox]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[pic(100)]-10-[name(20)]-20-[txtMob(20)]-20-[txtOtp(20)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[name]-10-|", options: [], metrics: nil, views: ["name":lblOtp]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[pic(100)]", options: [], metrics: nil, views: ["pic":imgMobVer]))
        view.addConstraint(NSLayoutConstraint(item: imgMobVer, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblMob(25)]-5-[txtMob]-5-[iEdit(15)]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txtOtp]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblMobileCode, attribute: .centerY, relatedBy: .equal, toItem: mobileNumberTextBox, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: mobileNumberTextBox, attribute: .centerY, relatedBy: .equal, toItem: editImg, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblMobileCode, attribute: .height, relatedBy: .equal, toItem: mobileNumberTextBox, attribute: .height, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: editImg, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 254340)
        return sub
    }
    
    func getFbTopView() -> SubView
    {
        let view = UIView()
        
        imgMobVer.image = UIImage(named: "lock_green")
        imgMobVer.translatesAutoresizingMaskIntoConstraints = false
        imgMobVer.backgroundColor = UIColor.clear
        imgMobVer.layer.cornerRadius = 50
        imgMobVer.layer.masksToBounds = true
        view.addSubview(imgMobVer)
        
        lblOtp.text = "Please enter your mobile number"
        lblOtp.translatesAutoresizingMaskIntoConstraints = false
        lblOtp.textColor = Colors.GRAY_COLOR
        lblOtp.textAlignment = .center
        lblOtp.font = normalFontWithSize(16)
        view.addSubview(lblOtp)
        
        lblMobileCode.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        lblMobileCode.text = "+91"
        lblMobileCode.textColor = Colors.GRAY_COLOR
        lblMobileCode.font = boldFontWithSize(15)
        lblMobileCode.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblMobileCode)
        
        mobileNumberTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        mobileNumberTextBox.font = normalFontWithSize(15)!
        mobileNumberTextBox.textColor = Colors.GRAY_COLOR
        mobileNumberTextBox.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberTextBox.returnKeyType = UIReturnKeyType.done
        mobileNumberTextBox.delegate = self
        mobileNumberTextBox.placeholder = "ENTER MOBILE NUMBER"
        view.addSubview(mobileNumberTextBox)
        
        let image = UIImage(named: "Infoicon_user")
        referralInfoImage.image = image
        referralInfoImage.contentMode = UIViewContentMode.center
        let referralInfotapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(referralInfoImageTapped(_:)))
        referralInfoImage.isUserInteractionEnabled = true
        referralInfoImage.addGestureRecognizer(referralInfotapGestureRecognizer)
        
        referralLabelText.font = normalFontWithSize(15)
        referralLabelText.backgroundColor = UIColor.clear
        referralLabelText.isUserInteractionEnabled = true
        let myReferralAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myReferralString = NSMutableAttributedString(string: "Have a Referral Code?", attributes: myReferralAttribute)
        let myReferralRange = NSRange(location: 0, length: 21)
        myReferralString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myReferralRange)
        referralLabelText.attributedText = myReferralString
        let referralTapGesture = UITapGestureRecognizer(target:self, action:#selector(showTextField))
        referralTapGesture.numberOfTapsRequired = 1
        referralTapGesture.numberOfTouchesRequired = 1
        referralLabelText.addGestureRecognizer(referralTapGesture)
        
        
        referralApplyText.font = normalFontWithSize(15)
        referralApplyText.backgroundColor = UIColor.clear
        let myApplyAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myApplyString = NSMutableAttributedString(string: "APPLY", attributes: myApplyAttribute)
        let myApplyRange = NSRange(location: 0, length: 5)
        myApplyString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myApplyRange)
        referralApplyText.attributedText = myApplyString
        let referralApplyTapGesture = UITapGestureRecognizer(target:self, action:#selector(applyReferralCode))
        referralApplyTapGesture.numberOfTapsRequired = 1
        referralApplyTapGesture.numberOfTouchesRequired = 1
        referralApplyText.addGestureRecognizer(referralApplyTapGesture)
        
        referralTextBox.delegate = self
        referralTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        referralTextBox.placeholder = "REFERRAL CODE (OPTIONAL)"
        referralTextBox.font = normalFontWithSize(15)!
        referralTextBox.textColor = Colors.GRAY_COLOR
        referralTextBox.rightView = referralApplyText
        referralTextBox.rightViewMode = UITextFieldViewMode.always
        referralTextBox.returnKeyType = UIReturnKeyType.done
        referralTextBox.autocapitalizationType = .allCharacters
        referralTextBox.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        referralUserName.font = normalFontWithSize(15)
        referralUserName.backgroundColor = UIColor.clear
        
        referralLabelText.translatesAutoresizingMaskIntoConstraints = false
        referralTextBox.translatesAutoresizingMaskIntoConstraints = false
        referralUserName.translatesAutoresizingMaskIntoConstraints = false
        referralInfoImage.translatesAutoresizingMaskIntoConstraints = false
        referralApplyText.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(mobileNumberTextBox)
        view.addSubview(referralLabelText)
        view.addSubview(referralTextBox)
        view.addSubview(referralUserName)
        view.addSubview(referralInfoImage)
        
        referralTextBox.isHidden = true
        referralApplyText.isHidden = true
        
        if(UserInfo.sharedInstance.refCode != "") {
            referralLabelText.isHidden = true
            referralInfoImage.isHidden = true
            referralTextBox.isHidden = false
            referralTextBox.isUserInteractionEnabled = false
            referralTextBox.text = UserInfo.sharedInstance.refCode
            referralApplyText.isHidden = false
            referralApplyText.text = "APPLIED"
            referralApplyText.textColor = Colors.GREEN_COLOR
            referralApplyText.isUserInteractionEnabled = false
        } else {
            referralLabelText.isHidden = false
            referralInfoImage.isHidden = false
        }
        
        let viewsDict = ["pic":imgMobVer, "name":lblOtp, "lblMob":lblMobileCode, "txtMob":mobileNumberTextBox,"referralLabel":referralLabelText,"referralText":referralTextBox,"referralName":referralUserName,"referralInfo":referralInfoImage]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[pic(100)]-10-[name(20)]-20-[txtMob(20)]-20-[referralLabel]-1-[referralText(20)]-2-[referralName]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[name]-10-|", options: [], metrics: nil, views: ["name":lblOtp]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[pic(100)]", options: [], metrics: nil, views: ["pic":imgMobVer]))
        view.addConstraint(NSLayoutConstraint(item: imgMobVer, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblMob(25)]-5-[txtMob]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[referralLabel]-5-[referralInfo(15)]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[referralText]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[referralName]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblMobileCode, attribute: .centerY, relatedBy: .equal, toItem: mobileNumberTextBox, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblMobileCode, attribute: .height, relatedBy: .equal, toItem: mobileNumberTextBox, attribute: .height, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: referralLabelText, attribute: .centerY, relatedBy: .equal, toItem: referralInfoImage, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 270)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "Please wait while we send your OTP"
        lblTitle.font = normalFontWithSize(15)
        lblTitle.textAlignment = .center
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.backgroundColor = Colors.WHITE_COLOR
        view.addSubview(lblTitle)
        
        let btnResend = UIButton()
        btnResend.setTitle("RESEND OTP", for: UIControlState())
        btnResend.backgroundColor = Colors.GREEN_COLOR
        btnResend.titleLabel?.font = boldFontWithSize(20)
        btnResend.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnResend.translatesAutoresizingMaskIntoConstraints = false
        btnResend.addTarget(self, action: #selector(resendCode), for: .touchDown)
        view.addSubview(btnResend)
        
        let btnVerify = UIButton()
        btnVerify.setTitle("VERIFY", for: UIControlState())
        btnVerify.backgroundColor = Colors.GREEN_COLOR
        btnVerify.titleLabel?.font = boldFontWithSize(20)
        btnVerify.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnVerify.translatesAutoresizingMaskIntoConstraints = false
        btnVerify.addTarget(self, action: #selector(verifyCode), for: .touchDown)
        view.addSubview(btnVerify)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(20)][btnResend(40)]", options: [], metrics: nil, views: ["lbl":lblTitle,"btnResend":btnResend]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnResend]-1-[btnVerify(==btnResend)]|", options: [], metrics: nil, views: ["btnResend":btnResend,"btnVerify":btnVerify]))
        
        view.addConstraint(NSLayoutConstraint(item: btnVerify, attribute: .centerY, relatedBy: .equal, toItem: btnResend, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: btnVerify, attribute: .height, relatedBy: .equal, toItem: btnResend, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    func getFbBottomView() -> SubView
    {
        let view = UIView()
        
        let btnSubmit = UIButton()
        btnSubmit.setTitle("SUBMIT", for: UIControlState())
        btnSubmit.backgroundColor = Colors.GREEN_COLOR
        btnSubmit.titleLabel?.font = boldFontWithSize(20)
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnSubmit.translatesAutoresizingMaskIntoConstraints = false
        btnSubmit.addTarget(self, action: #selector(resendCode), for: .touchDown)
        view.addSubview(btnSubmit)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btnSubmit(40)]", options: [], metrics: nil, views: ["btnSubmit":btnSubmit]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnSubmit]|", options: [], metrics: nil, views: ["btnSubmit":btnSubmit]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func resendCode () {
        if mobileNumberTextBox.text?.characters.count > 0 && mobileNumberTextBox.text?.characters.count == 10 && !isInvalidCode
        {
            let reqObject = ResendCodeNumberRequest()
            reqObject.userID = UserInfo.sharedInstance.userID
            reqObject.mobileNumber = mobileNumberTextBox.text
            
            showIndicator("Loading...")
            
            VerifyMobileNumberRequestor().resendCodeNumber(reqObject, success:{ (success, object) in
                
                hideIndicator()
                
                if (object as! ResendCodeNumberResponse).code == "257"
                {
                    if(UserInfo.sharedInstance.mobile == "") {
                        UserInfo.sharedInstance.mobile = self.mobileNumberTextBox.text!
                        AlertController.showAlertFor("SEND OTP", message: "You would receive verification code shortly via OTP. Please enter the code to verify your number.", okAction: {
                            let vc = MobileVerificationViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                    } else {
                        UserInfo.sharedInstance.mobile = self.mobileNumberTextBox.text!
                        AlertController.showToastForInfo("You would receive verification code shortly via OTP. Please enter the code to verify your number.")
                    }
                    
                }
                else if (object as! ResendCodeNumberResponse).code == "199"
                {
                    AlertController.showToastForError("Sorry, this mobile number is already registered with us.")
                }
                else
                {
                    AlertController.showToastForError("Something went wrong. Please try again.")
                }
                
                })
            {(error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
        }
        else if mobileNumberTextBox.text?.characters.count > 0 && mobileNumberTextBox.text?.characters.count < 10 {
            AlertController.showToastForError("Please provide valid 'Mobile No'.")
        } else if isInvalidCode {
            AlertController.showToastForError("Please provide valid 'Referral Code'.")
        } else {
            AlertController.showToastForError("Please provide your Indian mobile number to proceed further.")
        }
    }
    
    func verifyCode () {
        if UserInfo.sharedInstance.mobile == mobileNumberTextBox.text
        {
            if otpTextBox.text?.characters.count > 0
            {
                let reqObject = VerifyMobileNumberRequest()
                showIndicator("Loading...")
                reqObject.userID = UserInfo.sharedInstance.userID
                reqObject.verificationCode = otpTextBox.text
                
                VerifyMobileNumberRequestor().getVerifyNumber(reqObject, success:{ (success, object) in
                    
                    hideIndicator()
                    
                    if (object as! VerifyMobileNumberResponse).code == "251"
                    {
                        if(UserInfo.sharedInstance.userState == "3") {
                            UserInfo.sharedInstance.userState = "5"
                        } else {
                            UserInfo.sharedInstance.userState = "4"
                        }
                        AlertController.showAlertFor("RideAlly", message: "Your mobile number is verified successfully.", okAction: {
                            AlertController.showToastForInfo("Please provide 'Home Location' to proceed further.")
                            let vc = SignUpMapViewController()
                            self.navigationController?.pushViewController(vc, animated: true)
                        })
                    }
                        
                    else if (object as! VerifyMobileNumberResponse).code == "252"
                    {
                        AlertController.showToastForError("Verification code is incorrect. Please try again.")
                    }
                    else
                    {
                        AlertController.showToastForError("Something went wrong. Please try again.")
                    }
                    
                    })
                {(error) in
                    hideIndicator()
                    AlertController.showAlertForError(error)
                }
            }
            else
            {
                AlertController.showToastForError("Please provide verification code.")
            }
        }
        else
        {
            AlertController.showToastForError("Your number does not match with registered number. Please either resend code or enter existing number.")
        }
    }
    
    func editClicked() {
        mobileNumberTextBox.isUserInteractionEnabled = true
        mobileNumberTextBox.becomeFirstResponder()
    }
    
    func showTextField() {
        self.referralLabelText.isHidden = true
        self.referralTextBox.isHidden = false
        self.referralInfoImage.isHidden = true
        mobileNumberTextBox.returnKeyType = UIReturnKeyType.next
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if(textField == referralTextBox) {
            textField.text = textField.text!.uppercased(with: Locale.current)
            self.isInvalidCode = false
            if(referralTextBox.text?.characters.count == 0) {
                self.referralUserName.isHidden = true
            } else {
                self.referralUserName.isHidden = false
                isReferralCodeValid()
            }
        }
    }
    
    func isReferralCodeValid () {
        if(referralTextBox.text?.characters.count == 0) {
            self.isInvalidCode = false
        } else {
            let reqObj = referralCodeRequestModel()
            reqObj.ref_code = referralTextBox.text
            SignUpRequestor().isReferralCodeValid(reqObj, success:{(success, object) in
                if (object as! referralCodeResponseModel).code == "111" {
                    self.isInvalidCode = false
                    self.referralApplyText.isHidden = false
                    self.referralApplyText.isUserInteractionEnabled = true
                    self.referredBy = ((object as! referralCodeResponseModel).dataObj?.first?.firstName)!
                    self.referralUserName.textColor = Colors.GREEN_COLOR
                    self.referralUserName.text = (object as! referralCodeResponseModel).message!+" "+((object as! referralCodeResponseModel).dataObj?.first?.firstName)!
                } else {
                    self.isInvalidCode = true
                    self.referralUserName.textColor = Colors.RED_COLOR
                    self.referralUserName.text = (object as! referralCodeResponseModel).message
                }
            }) { (error) in
            }
        }
    }
    
    func applyReferralCode () {
        if(isInvalidCode) {
            AlertController.showToastForError("Please provide valid 'Referral Code'.")
        } else {
            mobileNumberTextBox.resignFirstResponder()
            let reqObj = applyRefCodeRequestModel()
            reqObj.coupon_code = referralTextBox.text
            reqObj.user_id = UserInfo.sharedInstance.userID
            reqObj.device_id = UIDevice.current.identifierForVendor?.uuidString
            reqObj.referred_by = self.referredBy
            showIndicator("Applying Referral Code...")
            SignUpRequestor().applyRefCode(reqObj, success:{(success, object) in
                hideIndicator()
                if (object as! applyRefCodeResponseModel).code == "101010" {
                    AlertController.showToastForInfo("Congratulations, Referral code applied successfully. You will get bonus points once you creates a ride.")
                    UserInfo.sharedInstance.refCode = self.referralTextBox.text!
                    self.referralTextBox.isUserInteractionEnabled = false
                    self.referralApplyText.text = "APPLIED"
                    self.referralApplyText.textColor = Colors.GREEN_COLOR
                    self.referralApplyText.isUserInteractionEnabled = false
                    self.referralUserName.isHidden = true
                } else {
                }
            }) { (error) in
                hideIndicator()
            }
        }
    }
    
    func referralInfoImageTapped(_ imgObject: AnyObject) -> Void {
        AlertController.showToastForInfo("With the referral code, you will get bonus points once you creates a ride.")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if(UserInfo.sharedInstance.mobile == "") {
            if (textField == mobileNumberTextBox)
            {
                if(textField.returnKeyType == .done) {
                    mobileNumberTextBox.resignFirstResponder()
                } else {
                    referralTextBox.becomeFirstResponder()
                }
            }
            else{
                referralTextBox.resignFirstResponder()
            }
        } else {
            if (textField == mobileNumberTextBox)
            {
                otpTextBox.becomeFirstResponder()
            }
            else{
                otpTextBox.resignFirstResponder()
            }
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textField == mobileNumberTextBox) {
            let lenghtValue = (textField.text?.characters.count)! + string.characters.count - range.length
            if lenghtValue > 10
            {
                return false
            }
            else
            {
                let aSet = CharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
                
            }
        } else if(textField == otpTextBox) {
            let aSet = CharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
        return true
    }
}
