//
//  VehicleInsuranceViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 6/7/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
import MobileCoreServices
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class VehicleInsuranceViewController: BaseScrollViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate{
    
    var isUpdateReqneedtobeSend : String?
    var isSeatCapacityNeedToBeUpdated : String?
    var isFromWp: String?
    var noVehicle: Bool?
    var selectedTxtField: UITextField?
    let addVehInsuranceObj = AddVehicleInsuranceRequest()
    let pickerView = UIPickerView()
    let txtCmpnyName = UITextField()
    let txtInsuranceId = UITextField()
    let txtExpiryDate = UITextField()
    let txtInsurerName = UITextField()
    var datePickerView = UIDatePicker()
    let calendarIcon = UITextField()
    let uploadInsBtn = UIButton()
    let uploadRcBtn = UIButton()
    var pickerDataSource :Array = ["Bajaj Allianz", "Bharti Axa", "Cholamandalam", "Future Generali", "HDFC ERGO", "ICICI Lombard", "L & T", "Iffco Tokio", "Magma HDI", "Liberty Videocon", "National Insurance", "New India Assurance", "Oriental", "Raheja", "Reliance", "SBI", "Royal Sundaram", "Shriram General Insurance", "Tata AIG", "United Insurance", "Universal Sompo", "Others"]
    var onAddSuccessAction: actionBlock?
    var signupFlow = false
    var btnTag = ""
    var vehicleKind = ""
    var vehicleOwnerName = ""
    var vehicleType = ""
    var vehicleModel = ""
    var vehicleRegNo = ""
    var vehicleCapacity = ""
    let uploadRequest = AWSS3TransferManagerUploadRequest()
    var uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    var insBtn = false
    var rcBtn = false
    var imgUpdated = false
    let updateVehicleObj = UpdateVehicleInsuranceRequest()
    var expiryDate = ""
    var insImgView = UIImageView()
    var rcImgView = UIImageView()
    var insImgAdded = false
    var rcImgAdded = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            self.title = "Update Insurance and RC"
        }
        else
        {
            self.title = "Add Insurance and RC"
        }
        if(signupFlow == true) {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = Colors.GREEN_COLOR
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        }
        viewsArray.append(getInsurerName())
        viewsArray.append(getCompanyName())
        viewsArray.append(getInsuranceId())
        viewsArray.append(getExpiryDate())
        viewsArray.append(uploadInsuranceUrl())
        viewsArray.append(uploadRcUrl())
        addBottomView(getBottomView())
    }
    
    func getInsurerName() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "INSURER NAME"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtInsurerName.delegate=self
        txtInsurerName.translatesAutoresizingMaskIntoConstraints = false
        txtInsurerName.font = boldFontWithSize(14)
        txtInsurerName.placeholder = "Enter Insurer Name"
        txtInsurerName.returnKeyType = UIReturnKeyType.next
        view.addSubview(txtInsurerName)
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            txtInsurerName.text = UserInfo.sharedInstance.insurerName
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtInsurerName, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtInsurerName]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
        
    }
    
    func getCompanyName() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "INSURED COMPANY NAME"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        txtCmpnyName.delegate=self
        txtCmpnyName.translatesAutoresizingMaskIntoConstraints = false
        txtCmpnyName.text = "Choose your policy"
        txtCmpnyName.inputView = pickerView
        txtCmpnyName.font = boldFontWithSize(14)
        txtCmpnyName.placeholder = "Select"
        view.addSubview(txtCmpnyName)
        let imgV = UIImageView(image: UIImage(named: "dropdown")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        txtCmpnyName.rightView = imgV
        txtCmpnyName.rightViewMode = .always
        
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtCmpnyName.inputAccessoryView = keyboardDoneButtonShow
        
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            if(UserInfo.sharedInstance.insuredCmpnyName != "") {
                txtCmpnyName.text = UserInfo.sharedInstance.insuredCmpnyName
                pickerView.selectRow(pickerDataSource.index(of: UserInfo.sharedInstance.insuredCmpnyName)!, inComponent: 0, animated: true)
            } else {
                txtCmpnyName.text = "Choose your policy"
            }
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtCmpnyName, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtCmpnyName]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    
    func getInsuranceId() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "INSURANCE ID"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtInsuranceId.delegate=self
        txtInsuranceId.translatesAutoresizingMaskIntoConstraints = false
        txtInsuranceId.font = boldFontWithSize(14)
        txtInsuranceId.placeholder = "Enter Insurance Id"
        txtInsuranceId.returnKeyType = UIReturnKeyType.next
        view.addSubview(txtInsuranceId)
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            txtInsuranceId.text = UserInfo.sharedInstance.insuranceId
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtInsuranceId, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtInsuranceId]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    
    func getExpiryDate() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "EXPIRY DATE OF INSURANCE"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtExpiryDate.delegate=self
        txtExpiryDate.translatesAutoresizingMaskIntoConstraints = false
        txtExpiryDate.font = boldFontWithSize(14)
        txtExpiryDate.placeholder = "DD-MM-YYYY"
        txtExpiryDate.returnKeyType = UIReturnKeyType.done
        view.addSubview(txtExpiryDate)
        
        datePickerView.addTarget(self, action:#selector(birthDayPickerValueChanged(_:)), for: UIControlEvents.valueChanged)
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date()
        datePickerView.maximumDate = (Calendar.current as NSCalendar).date(byAdding: .year, value: +10, to: Date(), options: [])
        txtExpiryDate.inputView = datePickerView
        //txtExpiryDate.userInteractionEnabled = false
        
        calendarIcon.backgroundColor = UIColor(patternImage: UIImage(named: "CalendarIcon")!)
        calendarIcon.tintColor = UIColor.clear
        //calendarIcon.userInteractionEnabled = false
        calendarIcon.delegate=self
        calendarIcon.inputView = datePickerView
        calendarIcon.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(calendarIcon)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        
        calendarIcon.inputAccessoryView = keyboardDoneButtonShow
        txtExpiryDate.inputAccessoryView = keyboardDoneButtonShow
        
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            if(UserInfo.sharedInstance.insuranceExpiryDate != "0000-00-00") {
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let dateObj = formatter.date(from: UserInfo.sharedInstance.insuranceExpiryDate)
                formatter.dateFormat = "dd-MMM-yyyy"
                txtExpiryDate.text = (formatter.string(from: dateObj!))
                expiryDate = txtExpiryDate.text!
            }
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtExpiryDate, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtExpiryDate]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[img(15)]", options:[], metrics:nil, views: ["img":calendarIcon]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[img(15)]-10-|", options:[], metrics:nil, views: ["img":calendarIcon]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    func uploadInsuranceUrl() ->SubView{
        let  view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "INSURANCE PHOTO COPY"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)

        insImgView.isUserInteractionEnabled = true
        insImgView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(insImgView)
        
        if(UserInfo.sharedInstance.insUploadStatus == "1"){
            if UserInfo.sharedInstance.insUrl != ""{
                insImgView.kf.setImage(with: URL(string: UserInfo.sharedInstance.insUrl))
            }
            else{
                insImgView.image = UIImage(named: "ProfileImage_S")
            }
        }
        else{
            insImgView.image = UIImage(named: "ProfileImage_S")
        }

        uploadInsBtn.setTitle("UPLOAD", for: UIControlState())
        uploadInsBtn.backgroundColor = Colors.GREEN_COLOR
        uploadInsBtn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        uploadInsBtn.titleLabel?.font = boldFontWithSize(16.0)
        uploadInsBtn.translatesAutoresizingMaskIntoConstraints = false
        uploadInsBtn.addTarget(self, action: #selector(uploadBtnTapped(_:)), for: .touchDown)
        view.addSubview(uploadInsBtn)

        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[imgView(100)]-10-[line(0.5)]|", options:[], metrics:nil, views: ["lbl":lblTitle, "imgView": insImgView, "line":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imgView(100)][uploadBtn(70)]-10-|", options:[], metrics:nil, views: ["imgView": insImgView,"uploadBtn":uploadInsBtn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[uploadBtn]", options:[], metrics:nil, views: ["uploadBtn":uploadInsBtn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 150)
        return sub
    }
    
    func uploadRcUrl() ->SubView{
        let  view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "RC PHOTO COPY"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)

        rcImgView.isUserInteractionEnabled = true
        rcImgView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(rcImgView)
        
        if(UserInfo.sharedInstance.rcUploadStatus == "1"){
            if UserInfo.sharedInstance.rcUrl != ""{
                rcImgView.kf.setImage(with: URL(string: UserInfo.sharedInstance.rcUrl))
            }
            else{
                rcImgView.image = UIImage(named: "ProfileImage_S")
            }
        }
        else{
            rcImgView.image = UIImage(named: "ProfileImage_S")
        }
        
        uploadRcBtn.setTitle("UPLOAD", for: UIControlState())
        uploadRcBtn.backgroundColor = Colors.GREEN_COLOR
        uploadRcBtn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        uploadRcBtn.titleLabel?.font = boldFontWithSize(16.0)
        uploadRcBtn.translatesAutoresizingMaskIntoConstraints = false
        uploadRcBtn.addTarget(self, action: #selector(uploadBtnTapped(_:)), for: .touchDown)
        view.addSubview(uploadRcBtn)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[imgView(100)]-10-[line(0.5)]|", options:[], metrics:nil, views: ["lbl":lblTitle, "imgView": rcImgView, "line":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imgView(100)][uploadBtn(70)]-10-|", options:[], metrics:nil, views: ["imgView": rcImgView,"uploadBtn":uploadRcBtn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[uploadBtn]", options:[], metrics:nil, views: ["uploadBtn":uploadRcBtn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 150)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let btn = UIButton()
        btn.setTitle("SUBMIT", for: UIControlState())
        btn.backgroundColor = Colors.GREEN_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.setTitleColor(Colors.WHITE_COLOR, for: .highlighted)
        btn.titleLabel?.font = boldFontWithSize(16)
        btn.addTarget(self, action: #selector(proceed(_:)), for: .touchDown)
        view.addSubview(btn)
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            //lblTitle.hidden = true
            btn.setTitle("UPDATE", for: UIControlState())
        }
        else
        {
            //lblTitle.hidden = false
            btn.setTitle("SUBMIT", for: UIControlState())
        }
        
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(40)]", options: [], metrics: nil, views: ["btn":btn]))
        //view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btn]))
        
        
        let sub = SubView()
        sub.view = view
        
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }

    func uploadBtnTapped(_ btn: UIButton) -> Void {
        if(btn == uploadInsBtn) {
            insBtn = true
            rcBtn = false
        } else {
            insBtn = false
            rcBtn = true
        }
        //let vc = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.mediaTypes = [kUTTypeImage as String]
        
//        let capturePhoto = UIAlertAction(title: "Capture Photo", style: .Default, handler: { (action) in
//            imgPicker.sourceType = .Camera
//            self.presentViewController(imgPicker, animated: true, completion: nil)
//        })
//        vc.addAction(capturePhoto)
//        
//        let photoLibrary = UIAlertAction(title: "Photo Library", style: .Default, handler: { (action) in
            imgPicker.sourceType = .photoLibrary
            self.present(imgPicker, animated: true, completion: nil)
//        })
//        vc.addAction(photoLibrary)
//        
//        let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) in
//            
//        })
//        vc.addAction(actionCancel)
//        
//        presentViewController(vc, animated: true, completion: nil)
    }
    
    func imageType(_ imgData : Data) -> String
    {
        var c = [UInt8](repeating: 0, count: 1)
        (imgData as NSData).getBytes(&c, length: 1)
        
        let ext : String
        
        switch (c[0]) {
        case 0xFF:
            
            ext = "jpg"
            
        case 0x89:
            
            ext = "png"
        case 0x47:
            
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = ""
        }
        
        return ext
    }
    
    //Uiimage Picker view delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let tempImage:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //self.imageView.image = tempImage
        
        
        let assertvalue:URL = info[UIImagePickerControllerReferenceURL] as! URL
        let fileExtension:CFString =  assertvalue.pathExtension as CFString
        let unmanagedFileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)
        let fileUTI = unmanagedFileUTI!.takeRetainedValue()
        
        
        var extValue = ""
        
        if (UTTypeConformsTo(fileUTI, kUTTypeJPEG))
        {
            extValue = "jpeg"
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypePNG))
        {
            extValue = "png"
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypeTIFF))
        {
            extValue = "tiff"
        }
        
        let bucketName = S3BUCKETNAME
        let imagePath = "VehicleIdProofs/vehins_"
        var imageName = ""
        if(insBtn) {
            imageName = "_ins.\(extValue)"
        } else {
            imageName = "_rc.\(extValue)"
        }
 
 
        UserInfo.sharedInstance.profilepicfileFormat = extValue
        let localStorePath = "\(UserInfo.sharedInstance.userID)" + "_image.\(extValue)"
 
        // create a local image that we can use to upload to s3
        let path = NSTemporaryDirectory() + localStorePath
 
        if extValue == "jpeg" {
            let imageData = UIImageJPEGRepresentation(tempImage, 5.0)
            try? imageData?.write(to: URL(fileURLWithPath: path), options: [.atomic])
        }
        else
        {
            let imageData = UIImagePNGRepresentation(tempImage)
            try? imageData?.write(to: URL(fileURLWithPath: path), options: [.atomic])
        }
        
        // once the image is saved we can use the path to create a local fileurl
        let  urlPath = URL.init(fileURLWithPath: path)
        // next we set up the S3 upload request manager
        self.uploadRequest?.bucket = bucketName
        // I want this image to be public to anyone to view it so I'm setting it to Public Read
        self.uploadRequest?.acl = AWSS3ObjectCannedACL.publicRead
        // set the image's name that will be used on the s3 server. I am also creating a folder to place the image in
        self.uploadRequest?.key = imagePath+UserInfo.sharedInstance.userID+imageName
        // set the content type
        self.uploadRequest?.contentType = "image/\(extValue)"
        self.uploadRequest?.body = urlPath
        
        //print("path before upload",path,(self.uploadRequest.body).absoluteString)
        
        let express = AWSS3TransferUtilityUploadExpression()
        express.progressBlock = {(task: AWSS3TransferUtilityTask, progress:Progress) in
            DispatchQueue.main.async {
                self.showLoadingIndicator("Uploading...")
            }
//            dispatch_get_main_queue().asynchronously(execute: {
//                self.showLoadingIndicator("Uploading...")
//            })
        }
        
        self.uploadCompletionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                self.hideLoadingIndiactor()
                
                if ((error) != nil)
                {
                    AlertController.showToastForError("Sorry, there is some technical error, please try later")
                }
                else
                {
                    self.imgUpdated = true
                    var updatedImagePath = (self.uploadRequest?.body)?.absoluteString
                    updatedImagePath = updatedImagePath?.replacingOccurrences(of: "file:///", with:"/")
                    let image = UIImage(contentsOfFile:updatedImagePath!)
                    
                    if(self.insBtn) {
                        self.addVehInsuranceObj.insUrl = "\(IMG_URLS_BASE)\(self.uploadRequest?.key!)"
                        self.updateVehicleObj.insUrl = "\(IMG_URLS_BASE)\(self.uploadRequest?.key!)"
                        if image == nil
                        {
                            self.insImgView.image = UIImage(named: "ProfileImage_S")
                        }
                        else
                        {
                            self.insImgAdded = true
                            self.insImgView.image = image
                        }
                    }
                    if(self.rcBtn) {
                        self.addVehInsuranceObj.rcUrl = "\(IMG_URLS_BASE)\(self.uploadRequest?.key!)"
                        self.updateVehicleObj.rcUrl = "\(IMG_URLS_BASE)\(self.uploadRequest?.key!)"
                        if image == nil
                        {
                            self.rcImgView.image = UIImage(named: "ProfileImage_S")
                        }
                        else
                        {
                            self.rcImgAdded = true
                            self.rcImgView.image = image
                        }
                    }
                }
            })
        }
        
        
        let transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadFile((self.uploadRequest?.body)!, bucket: (self.uploadRequest?.bucket!)!, key: (self.uploadRequest?.key!)!, contentType:(self.uploadRequest?.contentType!)!, expression: express, completionHandler: self.uploadCompletionHandler).continueWith { (task) -> AnyObject! in
            
            if let error = task.error
            {
                if error._domain == AWSS3TransferManagerErrorDomain as String
                {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error._code)
                    {
                        switch (errorCode)
                        {
                        case .cancelled, .paused:
                            DispatchQueue.main.async {
                                
                            }
//                            DispatchQueue.main.asynchronously(execute: { () -> Void in
//                                
//                            })
                            break;
                        default:
                            AlertController.showToastForError("Sorry, there is some technical error, please try later.")
                            break;
                        }
                    }
                    else
                    {   AlertController.showToastForError("Sorry, there is some technical error, please try later.")
                    }
                }
                else
                {
                    AlertController.showToastForError("Sorry, there is some technical error, please try later.")
                }
            }
//            if let exception = task.exception
//            {
//                AlertController.showToastForError(exception.description)
//            }
//            if let _ = task.result
//            {
//                print("Upload Starting!")
//            }
            
            return nil;
        }
        
        
        self.dismiss(animated: true, completion: nil)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        txtCmpnyName.text = pickerDataSource[row]
        //setDefaultValueForSeatBasedOnVehicleType()
    }
    
    func proceed(_ sender: UIButton)
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        let dateObj = formatter.date(from: self.txtExpiryDate.text!)
        
        if txtInsurerName.text?.characters.count == 0 && txtCmpnyName.text == "Choose your policy" && txtInsuranceId.text?.characters.count == 0 && txtExpiryDate.text?.characters.count == 0
        {
            AlertController.showToastForError("Please provide information in all the fields.")
        }
        else if txtInsurerName.text?.characters.count == 0
        {
            AlertController.showToastForError("Please provide 'Insurer Name'.")
        }
        else if txtCmpnyName.text?.characters.count == 0 || txtCmpnyName.text == "Choose your policy"
        {
            AlertController.showToastForError("Please choose 'Insured Company Name'.")
        }
        else if txtInsuranceId.text?.characters.count == 0
        {
            AlertController.showToastForError("Please provide 'Insurance Id'.")
        }
        else if txtExpiryDate.text?.characters.count == 0
        {
            AlertController.showToastForError("Please provide 'Expiry Date of Insurance'.")
        }
        else if txtExpiryDate.text?.characters.count > 0 && dateObj == nil
        {
            AlertController.showToastForError("Please provide valid 'Expiry Date of Insurance'.")
        } else if (!insImgAdded && isUpdateReqneedtobeSend != "1") || (!insImgAdded && UserInfo.sharedInstance.insUploadStatus == "0" && isUpdateReqneedtobeSend == "1")
        {
            AlertController.showToastForError("Please provide 'Insurance Copy'.")
        } else if (!rcImgAdded && isUpdateReqneedtobeSend != "1") || (!rcImgAdded && UserInfo.sharedInstance.rcUploadStatus == "0" && isUpdateReqneedtobeSend == "1")
        {
            AlertController.showToastForError("Please provide 'RC Copy'.")
        }
        else
        {
            if  sender.titleLabel?.text == "UPDATE"{
                sendUpdateVehicleRequest()
            }
            else
            {
                sendAddVehicleInsuranceRequest()
            }
        }
    }
    
    
    func sendUpdateVehicleRequest() -> Void {
        if  vehicleKind == UserInfo.sharedInstance.vehicleKind && vehicleOwnerName == UserInfo.sharedInstance.vehicleOwnerName && vehicleType == UserInfo.sharedInstance.vehicleType && vehicleModel == UserInfo.sharedInstance.vehicleModel && vehicleRegNo == UserInfo.sharedInstance.vehicleRegNo && vehicleCapacity == UserInfo.sharedInstance.vehicleSeatCapacity && txtInsurerName.text! == UserInfo.sharedInstance.insurerName && txtCmpnyName.text! == UserInfo.sharedInstance.insuredCmpnyName && txtInsuranceId.text! == UserInfo.sharedInstance.insuranceId && txtExpiryDate.text! == expiryDate && !imgUpdated
        {
            AlertController.showToastForError("Oops! It seems that you have not modified anything.")
        }
        else
        {
            if  UserInfo.sharedInstance.userID.characters.count > 0
            {
                updateVehicleObj.userID = UserInfo.sharedInstance.userID
                updateVehicleObj.vehicleSeqID = UserInfo.sharedInstance.vehicleSeqID
                updateVehicleObj.vehicleCapacity = vehicleCapacity
                updateVehicleObj.vehicleModel = vehicleModel
                updateVehicleObj.vehicleRegNo = vehicleRegNo
                updateVehicleObj.vehicleType = vehicleType
                updateVehicleObj.vehicleKind = vehicleKind
                updateVehicleObj.vehicleOwnerName = vehicleOwnerName
                updateVehicleObj.insurerName = txtInsurerName.text
                updateVehicleObj.insuredCmpnyName = txtCmpnyName.text
                updateVehicleObj.insuranceId = txtInsuranceId.text
                updateVehicleObj.insuranceExpiryDate = txtExpiryDate.text
                
                showLoadingIndicator("Loading...")
                
                ProfileRequestor().updateVehicleInsurance(updateVehicleObj, success: { (success, object) in
                    
                    self.hideLoadingIndiactor()
                    
                    if(success)
                    {
                        AlertController.showAlertFor("RideAlly", message:"Your insurance information is successfully updated." , okButtonTitle: "Ok", okAction:
                            {
                                if self.isFromWp != nil && self.isFromWp == "1"{
                                    self.backThree()
                                    ISVEHICLEUPDATED = true
                                }else{
                                    self.backTwo()
                                }
                        })
                    } else {
                        if((object as! UpdateVehicleResponse).code == "221") {
                            AlertController.showToastForError("Sorry, you are not able to update vehicle information if there are any active rides.")
                        } else
                        {
                            AlertController.showToastForError((object as! UpdateVehicleResponse).message!)
                        }
                    }
                }) { (error) in
                    self.hideLoadingIndiactor()
                    AlertController.showAlertForError(error)
                }
                
            }
            else
            {
                AlertController.showToastForError("Please provide a valid UserID.")
            }
        }
    }
    
    func sendAddVehicleInsuranceRequest() -> Void {
        
        if  UserInfo.sharedInstance.userID.characters.count > 0 {
            
            addVehInsuranceObj.userID = UserInfo.sharedInstance.userID
            addVehInsuranceObj.vehicleCapacity = vehicleCapacity
            addVehInsuranceObj.vehicleModel = vehicleModel
            addVehInsuranceObj.vehicleRegNo = vehicleRegNo
            addVehInsuranceObj.vehicleType = vehicleType
            addVehInsuranceObj.vehicleKind = vehicleKind
            addVehInsuranceObj.vehicleOwnerName = vehicleOwnerName
            addVehInsuranceObj.insurerName = txtInsurerName.text
            addVehInsuranceObj.insuredCmpnyName = txtCmpnyName.text
            addVehInsuranceObj.insuranceId = txtInsuranceId.text
            addVehInsuranceObj.insuranceExpiryDate = txtExpiryDate.text
            
            showLoadingIndicator("Loading...")
            
            ProfileRequestor().addVehicleInsurance(addVehInsuranceObj, success: { (success, object) in
                
                self.hideLoadingIndiactor()
                
                if(success)
                {
                    AlertController.showAlertFor("RideAlly", message:"Thank you! Your insurance information is saved successfully." , okButtonTitle: "Ok", okAction:
                        {
                            if (self.signupFlow == true) {
                                if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1")
                                {
                                    let vc = WpSearchViewController()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else {
                                    let vc = GroupOptionsViewController()
                                    vc.btnTag = self.btnTag
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            } else if self.isFromWp != nil && self.isFromWp == "1"{
                                self.backThree()
                                ISVEHICLEUPDATED = true
                            }else{
                                self.backTwo()
                            }
                    })
                }
                else
                {
                    AlertController.showToastForError((object as! AddVehicleResponse).message!)
                }
                
            }) { (error) in
                self.hideLoadingIndiactor()
                AlertController.showAlertForError(error)
            }
        }
        else
        {
            AlertController.showToastForError("Please provide a valid UserID.")
        }
    }
    
    func backTwo() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        self.onAddSuccessAction?()
    }
    
    func backThree() {
        if(noVehicle != nil && noVehicle == true) {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        } else {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 4], animated: true)
        }
    }
    
    func donePressed() -> Void {
        if(selectedTxtField == txtExpiryDate){
            selectedTxtField?.text = prettyDateStringFromDate(datePickerView.date, toFormat: "dd-MMM-yyyy")
        }
        selectedTxtField!.resignFirstResponder()
    }
    
    //Uitextfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if  textField.text == "Choose your policy"
        {
            self.txtCmpnyName.text = pickerDataSource[0]
        }
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        selectedTxtField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField === txtInsurerName)
        {
            txtCmpnyName.becomeFirstResponder()
        } else if (textField === txtCmpnyName)
        {
            txtInsuranceId.becomeFirstResponder()
        } else if (textField === txtInsuranceId)
        {
            txtExpiryDate.becomeFirstResponder()
        } else if (textField === txtExpiryDate)
        {
            txtExpiryDate.resignFirstResponder()
        }
        return true
    }
    
    func birthDayPickerValueChanged(_ datePickerObject: UIDatePicker) -> Void
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        txtExpiryDate.text = formatter.string(from: datePickerObject.date)
        
        //formatter.dateFormat = "yyyy-MM-dd"
        //self.birthdayValue = formatter.stringFromDate(datePickerObject.date)
    }
}
