//
//  NeedRideViewController.swift
//  rideally
//
//  Created by Sarav on 05/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import QuartzCore

class NeedRideViewController: BaseScrollViewController {
    
    var selectedTxtField: UITextField?
    
    var isEditingRide: Bool = false
    var rideDetailData: RideDetail?
    var backFromSearchResultData: NeedRideRequest?
    var onRidesExistBlock: actionBlockWith3ParamsString?
    var backFromSearchResult = ""
    let needRideObj = NeedRideRequest()
    var signupFlow = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Need a Ride"
        addViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(signupFlow == true) {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = Colors.GREEN_COLOR
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        }
        if(backFromSearchResult != "") {
            self.navigationItem.leftBarButtonItem  = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBackToHome))
        }
    }
    
    func goBackToHome() {
        _ = self.navigationController?.popToRootViewController(animated: true)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateRidesList"), object: nil)
    }
    
    func addViews()
    {
        viewsArray.append(getLocationView())
        viewsArray.append(getTravelWithView())
        viewsArray.append(getTimeView())
        viewsArray.append(getCommentView())
        
        addBottomView(getBottomView())
        
        if(isEditingRide){
            
            self.title = "Update Ride"
            lblCaption.text = "Please update your travel details"
            
            lblSource.text = rideDetailData?.source
            lblDest.text = rideDetailData?.destination
            
            if(rideDetailData?.poolType == "M"){
                btnOnly.setTitle("Only Male", for: UIControlState())
                btnOnly.isSelected = true
            }
            else if(rideDetailData?.poolType == "F"){
                btnOnly.setTitle("Only Female", for: UIControlState())
                btnOnly.isSelected = true
            }
            else{
                btnAny.isSelected = true
            }
            
            txtDate.text = prettyDateStringFromString(rideDetailData?.startDate, fromFormat: "dd-MMM-yyyy", toFormat: "dd-MMM-yyyy")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            if let date = txtDate.text {
                datePicker.date = dateFormatter.date(from: date)!
            }
            
            txtTime.text = prettyDateStringFromString(rideDetailData?.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
            dateFormatter.dateFormat = "HH:mm"
            if let time = txtTime.text {
                timePicker.date = dateFormatter.date(from: time)!
            }
            
            txtComments.text = rideDetailData?.comments
            
            needRideObj.fromLocation = rideDetailData?.source
            needRideObj.fromLat = rideDetailData?.sourceLat
            needRideObj.fromLong = rideDetailData?.sourceLong
            needRideObj.toLocation = rideDetailData?.destination
            needRideObj.toLat = rideDetailData?.destinationLat
            needRideObj.toLong = rideDetailData?.destinationLong
        }
        if(backFromSearchResult != ""){
            
            lblSource.text = backFromSearchResultData?.fromLocation!
            lblDest.text = backFromSearchResultData?.toLocation!
            
            if(backFromSearchResultData?.poolType! == "M"){
                btnOnly.setTitle("Only Male", for: UIControlState())
                btnOnly.isSelected = true
            }
            else if(backFromSearchResultData?.poolType! == "F"){
                btnOnly.setTitle("Only Female", for: UIControlState())
                btnOnly.isSelected = true
            }
            else{
                btnAny.isSelected = true
            }
            
            txtDate.text = prettyDateStringFromString(backFromSearchResultData?.rideDate!, fromFormat: "dd-MMM-yyyy", toFormat: "dd-MMM-yyyy")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            if let date = txtDate.text {
                datePicker.date = dateFormatter.date(from: date)!
            }
            txtTime.text = prettyDateStringFromString(backFromSearchResultData?.rideTime!, fromFormat: "HH:mm", toFormat: "HH:mm")
            dateFormatter.dateFormat = "HH:mm"
            if let time = txtTime.text {
                timePicker.date = dateFormatter.date(from: time)!
            }
            
            txtComments.text = backFromSearchResultData?.message!
            
            needRideObj.fromLocation = backFromSearchResultData?.fromLocation!
            needRideObj.fromLat = backFromSearchResultData?.fromLat!
            needRideObj.fromLong = backFromSearchResultData?.fromLong!
            needRideObj.toLocation = backFromSearchResultData?.toLocation!
            needRideObj.toLat = backFromSearchResultData?.toLat!
            needRideObj.toLong = backFromSearchResultData?.toLong!
        }
    }
    
    let lblSource = UILabel()
    let lblDest = UILabel()
    
    let lblCaption = UILabel()
    func getLocationView() -> SubView
    {
        let view = UIView()
        
        lblCaption.text = "Please share your travel details"
        lblCaption.font = boldFontWithSize(18)
        lblCaption.textColor = Colors.GRAY_COLOR
        lblCaption.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCaption)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSource)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.text = "From (Select exact or nearby google places)"
        lblSource.font = boldFontWithSize(14)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(mapForFrom))
        lblSource.isUserInteractionEnabled = true
        lblSource.addGestureRecognizer(tapGestureFrom)
        view.addSubview(lblSource)
        
        let iconSourceArrow = UIImageView()
        iconSourceArrow.image = UIImage(named: "rightArrow")
        iconSourceArrow.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSourceArrow)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDest)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = boldFontWithSize(14)
        lblDest.text = "To (Select exact or nearby google places)"
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        
        let iconDestArrow = UIImageView()
        iconDestArrow.image = UIImage(named: "rightArrow")
        iconDestArrow.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDestArrow)
        
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForTo))
        lblDest.isUserInteractionEnabled = true
        lblDest.addGestureRecognizer(tapGestureTo)
        
        view.addSubview(lblDest)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        let viewsDict = ["caption":lblCaption, "line1":lblLine1, "isource":iconSource, "source":lblSource, "line2":lblLine2, "idest":iconDest, "dest":lblDest, "line3":lblLine3, "sarrow":iconSourceArrow, "darrow":iconDestArrow]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[caption(50)][line1(0.5)][source(40)][line2(0.5)][dest(40)][line3(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[caption]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource(15)][source][sarrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest(15)][dest][darrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: iconSourceArrow, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDestArrow, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 135)
        return sub
    }
    
    func mapForFrom()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.needRideObj.fromLocation = self.lblSource.text
                    self.needRideObj.fromLat = loc["lat"]
                    self.needRideObj.fromLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func mapForTo()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.needRideObj.toLocation = self.lblDest.text
                    self.needRideObj.toLat = loc["lat"]
                    self.needRideObj.toLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    let btnAny = DLRadioButton()
    let btnOnly = DLRadioButton()
    
    func getTravelWithView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "TRAVEL WITH"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        btnAny.setTitle("Any", for: UIControlState())
        btnAny.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnAny.iconColor = Colors.GREEN_COLOR
        btnAny.indicatorColor = Colors.GREEN_COLOR
        btnAny.titleLabel?.font = boldFontWithSize(14)
        btnAny.translatesAutoresizingMaskIntoConstraints = false
        btnAny.isSelected = true
        view.addSubview(btnAny)
        
        if(UserInfo.sharedInstance.gender == "F"){
            btnOnly.setTitle("Only Female", for: UIControlState())
        }
        else{
            btnOnly.setTitle("Only Male", for: UIControlState())
        }
        btnOnly.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnOnly.iconColor = Colors.GREEN_COLOR
        btnOnly.indicatorColor = Colors.GREEN_COLOR
        btnOnly.titleLabel?.font = boldFontWithSize(14)
        btnOnly.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(btnOnly)
        
        btnAny.otherButtons = [btnOnly]
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[any(50)]-10-[only(100)]", options: [], metrics: nil, views: ["any":btnAny, "only":btnOnly, "line":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title(20)]-5-[any(25)]-4-[line(0.5)]", options: [], metrics: nil, views: ["any":btnAny, "title":lblTitle, "line":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[title]", options: [], metrics: nil, views: ["title":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        view.addConstraint(NSLayoutConstraint(item: btnOnly, attribute: .centerY, relatedBy: .equal, toItem: btnAny, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    func datePicked()
    {
        
    }
    
    func timePicked()
    {
        
    }
    
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    let txtDate = RATextField()
    let txtTime = RATextField()
    
    func getTimeView() -> SubView
    {
        let view = UIView()
        
        let lblDate = UILabel()
        lblDate.text = "I WANT TO TRAVEL ON"
        lblDate.textColor = Colors.GRAY_COLOR
        lblDate.font = boldFontWithSize(11)
        lblDate.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDate)
        
        txtDate.layer.borderWidth = 0.5
        txtDate.delegate = self
        txtDate.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtDate.translatesAutoresizingMaskIntoConstraints = false
        txtDate.font = boldFontWithSize(14)
        
        datePicker.datePickerMode = .date
        txtDate.inputView = datePicker
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(datePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtDate.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "cal")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        txtDate.rightView = imgV
        txtDate.rightViewMode = .always
        view.addSubview(txtDate)
        
        let lblTime = UILabel()
        lblTime.text = "RIDE TIME"
        lblTime.textColor = Colors.GRAY_COLOR
        lblTime.font = boldFontWithSize(11)
        lblTime.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTime)
        
        txtTime.layer.borderWidth = 0.5
        txtTime.delegate = self
        txtTime.font = boldFontWithSize(14)
        txtTime.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtTime.translatesAutoresizingMaskIntoConstraints = false
        
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "NL")
        txtTime.inputView = timePicker
        timePicker.date = Date().addingTimeInterval(300)
        timePicker.addTarget(self, action: #selector(timePicked), for: .valueChanged)
        
        txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV1 = UIImageView(image: UIImage(named: "time")!)
        imgV1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV1.contentMode = .center
        
        txtTime.rightView = imgV1
        txtTime.rightViewMode = .always
        view.addSubview(txtTime)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbldate":lblDate, "txtdate":txtDate, "line":lblLine1, "lbltime":lblTime, "txttime":txtTime]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbldate(20)]-5-[txtdate(30)]-10-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbltime(20)]-5-[txttime(30)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbldate]-10-[lbltime(==lbldate)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txtdate]-10-[txttime(==txtdate)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        if(!isEditingRide){
            needRideObj.rideDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
            txtDate.text = needRideObj.rideDate
            needRideObj.rideTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
            txtTime.text = needRideObj.rideTime
        }
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
        
    }
    
    func donePressed()
    {
        if(selectedTxtField == txtDate){
            needRideObj.rideDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            selectedTxtField?.text = needRideObj.rideDate
        }
        else if(selectedTxtField == txtTime){
            needRideObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            selectedTxtField?.text = needRideObj.rideTime
        }
        selectedTxtField?.resignFirstResponder()
    }
    
    let txtComments = RATextField()
    func getCommentView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "COMMENT"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtComments.font = boldFontWithSize(14)
        txtComments.placeholder = "Enter your comment here"
        txtComments.translatesAutoresizingMaskIntoConstraints = false
        txtComments.returnKeyType = UIReturnKeyType.done
        txtComments.delegate = self
        view.addSubview(txtComments)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbl":lblTitle, "txt":txtComments, "line":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)][txt(40)]-2-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txt]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 10, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    let btnSubmit = UIButton()
    func getBottomView() -> SubView
    {
        btnSubmit.setTitle("SUBMIT", for: UIControlState())
        btnSubmit.backgroundColor = Colors.GRAY_COLOR
        btnSubmit.translatesAutoresizingMaskIntoConstraints = false
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnSubmit.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btnSubmit.addTarget(self, action: #selector(proceed), for: .touchDown)
        
        let sub = SubView()
        sub.view = btnSubmit
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: btnSubmit, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func proceed()
    {
        if(isEditingRide){
            
            let updateReqObj = UpdateRideRequest()
            
            updateReqObj.from_lon = rideDetailData?.sourceLong
            updateReqObj.from_lat = rideDetailData?.sourceLat
            updateReqObj.from_location = rideDetailData?.source
            updateReqObj.to_location = rideDetailData?.destination
            updateReqObj.to_lat = rideDetailData?.destinationLat
            updateReqObj.to_lon = rideDetailData?.destinationLong
            updateReqObj.cost = rideDetailData?.cost
            updateReqObj.cost_type = rideDetailData?.costType
            updateReqObj.poolCategory = rideDetailData?.poolCategory
            updateReqObj.poolID = rideDetailData?.poolID
            updateReqObj.poolShared = rideDetailData?.poolShared
            updateReqObj.poolstartdate = rideDetailData?.startDate
            updateReqObj.poolstarttime = rideDetailData?.startTime
            updateReqObj.pooltype = rideDetailData?.poolType
            updateReqObj.rideMessage = rideDetailData?.message
            updateReqObj.rideType = rideDetailData?.rideType
            updateReqObj.seatLeft = rideDetailData?.seatsLeft
            updateReqObj.vehregno = rideDetailData?.vehicleRegNo
            updateReqObj.userID = rideDetailData?.createdBy
            updateReqObj.email = rideDetailData?.ownerEmail
            updateReqObj.via = rideDetailData?.via
            updateReqObj.via_lat = ""
            updateReqObj.via_lon = ""
            
            updateReqObj.from_location = needRideObj.fromLocation
            updateReqObj.from_lat = needRideObj.fromLat
            updateReqObj.from_lon = needRideObj.fromLong
            updateReqObj.to_location = needRideObj.toLocation
            updateReqObj.to_lat = needRideObj.toLat
            updateReqObj.to_lon = needRideObj.toLong
            updateReqObj.pooltype = btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
            updateReqObj.poolstarttime = txtTime.text
            updateReqObj.poolstartdate = txtDate.text
            updateReqObj.rideMessage = txtComments.text
            
            
            if(updateReqObj.from_location != updateReqObj.to_location){
                
                CommonRequestor().getDistanceDuration(updateReqObj.from_location!, destination: updateReqObj.to_location!, success: { (success, object) in
                        updateReqObj.duration = ""
                        updateReqObj.distance = ""
                        if let data = object as? NSDictionary{
                            let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                            let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                            if((elements["status"] as! String) == "OK")
                            {
                                let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                
                                updateReqObj.duration = duration as? String
                                updateReqObj.distance = distance as? String
                        }
                    }
                    let sourceStr = "\(updateReqObj.from_lat!),\(updateReqObj.from_lon!)"
                    let destinationStr = "\(updateReqObj.to_lat!), \(updateReqObj.to_lon!)"
                    CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                        updateReqObj.via = ""
                        updateReqObj.waypoints = ""
                        if let data = object as? NSDictionary{
                            let routes = data["routes"] as! NSArray
                            if routes.count == 0{
                                return
                            }
                            let routesDict = routes[0] as! NSDictionary
                            let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                            let points = overview_polyline ["points"] as! NSString
                            updateReqObj.via = routesDict ["summary"] as? String
                            updateReqObj.waypoints = points as String
                        }
                        showIndicator("Updating Ride..")
                        RideRequestor().updateRide(updateReqObj, success: { (success, object) in
                            hideIndicator()
                            
                            if(success == true){
                                let poolID = (self.rideDetailData?.poolID)!
                                
                                showIndicator("Fetching Ride Details")
                                RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                    hideIndicator()
                                    if(success){
                                        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                        let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                        //let vc = RideDetailViewController()
                                        vc.hidesBottomBarWhenPushed = true
                                        vc.edgesForExtendedLayout = UIRectEdge()
                                        vc.data = object as? RideDetail
                                        vc.triggerFrom = "ride"
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }) { (error) in
                                    hideIndicator()
                                }
                            }
                            else{
                                AlertController.showToastForError((object as! BaseModel).message!)
                            }
                            }, failure: { (error) in
                                hideIndicator()
                        })
                        }, failure: { (error) in
                    })
                    }, failure: { (error) in
                        showIndicator("Updating Ride..")
                        RideRequestor().updateRide(updateReqObj, success: { (success, object) in
                            hideIndicator()
                            
                            if(success == true){
                                let poolID = (self.rideDetailData?.poolID)!
                                
                                showIndicator("Fetching Ride Details")
                                RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                    hideIndicator()
                                    if(success){
                                        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                        let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                        //let vc = RideDetailViewController()
                                        vc.hidesBottomBarWhenPushed = true
                                        vc.edgesForExtendedLayout = UIRectEdge()
                                        vc.data = object as? RideDetail
                                        vc.triggerFrom = "ride"
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }) { (error) in
                                    hideIndicator()
                                }
                            }
                            else{
                                AlertController.showToastForError((object as! BaseModel).message!)
                            }
                            }, failure: { (error) in
                                hideIndicator()
                        })
                })
            }
            else{
                AlertController.showAlertFor("Update Ride", message: "Please choose different locations for source and destination.")
            }
        }
        else{
            let searchReqObj = SearchRideRequest()
            searchReqObj.userId = UserInfo.sharedInstance.userID
            searchReqObj.reqDate = txtDate.text
            searchReqObj.reqTime = txtTime.text
            searchReqObj.startLoc = needRideObj.fromLocation
            searchReqObj.fromLat = needRideObj.fromLat
            searchReqObj.fromLong = needRideObj.fromLong
            searchReqObj.endLoc = needRideObj.toLocation
            searchReqObj.toLat = needRideObj.toLat
            searchReqObj.toLong = needRideObj.toLong
            searchReqObj.poolType = self.btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
            searchReqObj.searchType = "recurrent"
            searchReqObj.ridetype = ""
            var selectedDates = [String]()
            selectedDates.append(txtDate.text!)
            if(selectedDates.count > 0){
                searchReqObj.reqCurrentDate = selectedDates.joined(separator: ",")
            }
            
            self.needRideObj.userId = UserInfo.sharedInstance.userID
            self.needRideObj.poolType = self.btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
            self.needRideObj.rideDate = self.txtDate.text
            self.needRideObj.rideTime = self.txtTime.text
            self.needRideObj.ridemessage = self.txtComments.text!
            
            if((self.needRideObj.fromLocation != nil) && (self.needRideObj.toLocation != nil))
            {
                if(self.needRideObj.fromLocation != self.needRideObj.toLocation){
                    if((self.needRideObj.rideDate != nil) && (self.needRideObj.rideTime != nil)){
                        showIndicator("Searching for similair rides..")
                        RideRequestor().searchRides(searchReqObj, success: { (success, object) in
                            hideIndicator()
                            if(success){
                                if((object as! AllRidesResponse).pools!.count > 0){
                                    let vc = SearchResultViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.taxiRide = "0"
                                    vc.searchReqObj = searchReqObj
                                    vc.dataSource = (object as! AllRidesResponse).pools!
                                    vc.onAddNewBlock = {
                                        CommonRequestor().getDistanceDuration(self.needRideObj.fromLocation!, destination: self.needRideObj.toLocation!, success: { (success, object) in
                                            self.needRideObj.duration = ""
                                            self.needRideObj.distance = ""
                                            if let data = object as? NSDictionary{
                                                let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                                                let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                                                if((elements["status"] as! String) == "OK")
                                                {
                                                    let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                                    let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                                    self.needRideObj.duration = duration as! String
                                                    self.needRideObj.distance = distance as! String
                                                }
                                            }
                                            let sourceStr = "\(self.needRideObj.fromLat!),\(self.needRideObj.fromLong!)"
                                            let destinationStr = "\(self.needRideObj.toLat!), \(self.needRideObj.toLong!)"
                                            CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                                                self.needRideObj.via = ""
                                                self.needRideObj.waypoints = ""
                                                if let data = object as? NSDictionary{
                                                    let routes = data["routes"] as! NSArray
                                                    if routes.count == 0{
                                                        return
                                                    }
                                                    let routesDict = routes[0] as! NSDictionary
                                                    let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                                                    let points = overview_polyline ["points"] as! NSString
                                                    self.needRideObj.via = routesDict ["summary"] as? String
                                                    self.needRideObj.waypoints = points as String
                                                }
                                                self.createNewRide()
                                                }, failure: { (error) in
                                                    self.createNewRide()
                                            })
                                            }, failure: { (error) in
                                                self.createNewRide()
                                        })
                                    }
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                                else{
                                    CommonRequestor().getDistanceDuration(self.needRideObj.fromLocation!, destination: self.needRideObj.toLocation!, success: { (success, object) in
                                        self.needRideObj.duration = ""
                                        self.needRideObj.distance = ""
                                        if let data = object as? NSDictionary{
                                            let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                                            let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                                            if((elements["status"] as! String) == "OK")
                                            {
                                                let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                                let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                                self.needRideObj.duration = duration as! String
                                                self.needRideObj.distance = distance as! String
                                            }
                                        }
                                        let sourceStr = "\(self.needRideObj.fromLat!),\(self.needRideObj.fromLong!)"
                                        let destinationStr = "\(self.needRideObj.toLat!), \(self.needRideObj.toLong!)"
                                        CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                                            self.needRideObj.via = ""
                                            self.needRideObj.waypoints = ""
                                            if let data = object as? NSDictionary{
                                                let routes = data["routes"] as! NSArray
                                                if routes.count == 0{
                                                    return
                                                }
                                                let routesDict = routes[0] as! NSDictionary
                                                let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                                                let points = overview_polyline ["points"] as! NSString
                                                self.needRideObj.via = routesDict ["summary"] as? String
                                                self.needRideObj.waypoints = points as String
                                            }
                                            self.createNewRide()
                                            }, failure: { (error) in
                                                self.createNewRide()
                                        })
                                        }, failure: { (error) in
                                            self.createNewRide()
                                    })
                                }
                            }
                        }) { (error) in
                            hideIndicator()
                        }
                    }
                    else{
                        AlertController.showAlertFor("Need a Ride", message: "Please enter date and time for the Ride.")
                    }
                }
                else{
                    AlertController.showAlertFor("Need a Ride", message: "Please choose different locations for source and destination.")
                }
            }
            else{
                AlertController.showAlertFor("Need a Ride", message: "Please choose location in 'From' and 'To' field from Google Places.")
            }
        }
    }
    
    func createNewRide()
    {
        showIndicator("Creating New Ride..")
        NeedRideRequestor().createNeedaRide(self.needRideObj, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! NeedRideResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.triggerFrom = "ride"
                            vc.signupFlow = self.signupFlow
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! NeedRideResponse).code == "173") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! NeedRideResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
