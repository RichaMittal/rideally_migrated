//
//  SearchRideViewController.swift
//  rideally
//
//  Created by Sarav on 15/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class SearchRideViewController: BaseScrollViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(wpId != "") {
            title = "Filter your search"
        } else {
            title = "Search Rides"
        }
        addViews()
    }
    
    let lblSource = UILabel()
    let lblDest = UILabel()
    
    let lblErrorMsg = UILabel()
    
    var completionBlock: actionBlockWithParams?
    let searchReqObj = SearchRideRequest()
    var wpId = ""
    var searchData: SearchRideRequest?
    var searchResult: Bool?
    
    func getLocationView() -> SubView
    {
        let view = UIView()
        
        let lblCaption = UILabel()
        if(wpId != "") {
            lblCaption.text = "Filter your search"
        } else {
            lblCaption.text = "Search Ride(s)"
        }
        lblCaption.font = normalFontWithSize(15)
        lblCaption.textColor = Colors.GRAY_COLOR
        lblCaption.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCaption)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSource)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.text = "From (Select exact or nearby google places)"
        lblSource.font = boldFontWithSize(12)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(mapForFrom))
        lblSource.isUserInteractionEnabled = true
        lblSource.addGestureRecognizer(tapGestureFrom)
        view.addSubview(lblSource)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDest)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = boldFontWithSize(12)
        lblDest.text = "To (Select exact or nearby google places)"
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForTo))
        lblDest.isUserInteractionEnabled = true
        lblDest.addGestureRecognizer(tapGestureTo)
        
        view.addSubview(lblDest)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        let btnSearch = UIButton()
        if(wpId == "") {
            
            btnSearch.setTitle("SEARCH", for: UIControlState())
            btnSearch.backgroundColor = Colors.GREEN_COLOR
            btnSearch.titleLabel?.font = boldFontWithSize(20)
            btnSearch.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
            btnSearch.translatesAutoresizingMaskIntoConstraints = false
            btnSearch.addTarget(self, action: #selector(searchRides), for: .touchDown)
            view.addSubview(btnSearch)
        }
        
        lblErrorMsg.textColor = Colors.GRAY_COLOR
        lblErrorMsg.font = boldFontWithSize(15)
        lblErrorMsg.translatesAutoresizingMaskIntoConstraints = false
        lblErrorMsg.isHidden = true
        lblErrorMsg.numberOfLines = 0
        lblErrorMsg.textAlignment = .center
        //        if(wpId != "") {
        //            let nowText = "Create new ride"
        //            let combinedString = "Oops. no rides found for your search. Please \(nowText)" as NSString
        //            let range = combinedString.rangeOfString(nowText)
        //            let attributedString = NSMutableAttributedString(string: combinedString as String)
        //            let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1]
        //            attributedString.addAttributes(attributes, range: range)
        //            lblErrorMsg.attributedText = attributedString
        //            lblErrorMsg.userInteractionEnabled = true
        //            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(createNewRide))
        //            lblErrorMsg.addGestureRecognizer(tapGesture)
        //        } else {
        //            lblErrorMsg.userInteractionEnabled = false
        lblErrorMsg.text = "Oops. no rides found for you search. Please Create new ride."
        //}
        view.addSubview(lblErrorMsg)
        
        let viewsDict = ["caption":lblCaption, "line1":lblLine1, "isource":iconSource, "source":lblSource, "line2":lblLine2, "idest":iconDest, "dest":lblDest, "line3":lblLine3, "btn": btnSearch, "errmsg":lblErrorMsg]
        if(wpId == "") {
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[caption(50)][line1(0.5)][source(40)][line2(0.5)][dest(40)][line3(0.5)]-20-[btn(40)]-20-[errmsg]", options: [], metrics: nil, views: viewsDict))
        } else {
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[caption(50)][line1(0.5)][source(40)][line2(0.5)][dest(40)][line3(0.5)]-20-[errmsg]", options: [], metrics: nil, views: viewsDict))
        }
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[caption]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource][source]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest][dest]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        if(wpId == "") {
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[btn]-10-|", options: [], metrics: nil, views: viewsDict))
        }
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[errmsg]-10-|", options: [], metrics: nil, views: viewsDict))
        
        
        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        if(wpId != "" && searchData != nil) {
            lblSource.text = self.searchData?.startLoc
            lblDest.text = self.searchData?.endLoc
            self.searchReqObj.startLoc = self.searchData?.startLoc
            self.searchReqObj.fromLat = self.searchData?.fromLat
            self.searchReqObj.fromLong = self.searchData?.toLat
            self.searchReqObj.endLoc = self.searchData?.endLoc
            self.searchReqObj.toLat = self.searchData?.toLat
            self.searchReqObj.toLong = self.searchData?.toLong
        }
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 195)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let btnReset = UIButton()
        btnReset.setTitle("RESET", for: UIControlState())
        btnReset.backgroundColor = Colors.GRAY_COLOR
        btnReset.titleLabel?.font = boldFontWithSize(20)
        btnReset.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnReset.translatesAutoresizingMaskIntoConstraints = false
        btnReset.addTarget(self, action: #selector(resetFields), for: .touchDown)
        view.addSubview(btnReset)
        
        let btnSearch = UIButton()
        btnSearch.setTitle("APPLY", for: UIControlState())
        btnSearch.backgroundColor = Colors.GRAY_COLOR
        btnSearch.titleLabel?.font = boldFontWithSize(20)
        btnSearch.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnSearch.translatesAutoresizingMaskIntoConstraints = false
        btnSearch.addTarget(self, action: #selector(searchRides), for: .touchDown)
        view.addSubview(btnSearch)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btnReset(40)]", options: [], metrics: nil, views: ["btnReset":btnReset]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnReset]-1-[btnSearch(==btnReset)]|", options: [], metrics: nil, views: ["btnReset":btnReset,"btnSearch":btnSearch]))
        view.addConstraint(NSLayoutConstraint(item: btnSearch, attribute: .centerY, relatedBy: .equal, toItem: btnReset, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: btnSearch, attribute: .height, relatedBy: .equal, toItem: btnReset, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func addViews () {
        viewsArray.append(getLocationView())
        if(wpId != "") {
            addBottomView(getBottomView())
        }
    }
    
    func resetFields () {
        lblSource.text = "From (Select exact or nearby google places)"
        lblDest.text = "To (Select exact or nearby google places)"
        self.searchReqObj.startLoc = ""
        self.searchReqObj.fromLat = ""
        self.searchReqObj.fromLong = ""
        self.searchReqObj.endLoc = ""
        self.searchReqObj.toLat = ""
        self.searchReqObj.toLong = ""
        if(SEARCHREQUEST != nil) {
            SEARCHREQUEST = nil
        }
        if(searchResult == true) {
            ISSEARCHRESULT = false
            showIndicator("Resetting..")
            WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: wpId, success: { (success, object) in
                WorkplaceRequestor().getWPMembers("", groupID: self.wpId, distanceRange: "1", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, objectMembers) in
                    hideIndicator()
                    let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                    let vc : RideMapViewController = storyboard.instantiateViewController(withIdentifier: "RideMapViewController") as! RideMapViewController
                    vc.hidesBottomBarWhenPushed = true
                    vc.data = WPDATA
                    vc.dataSourceToOfc = (object as! WPRidesResponse).pools?.officeGoing
                    vc.dataSourceToHome = (object as! WPRidesResponse).pools?.homeGoing
                    vc.wpMembers = (objectMembers as! WPMembersResponseData).members
                    self.navigationController?.pushViewController(vc, animated: true)
                }) { (error) in
                    //hideIndicator()
                }
            }) { (error) in
                hideIndicator()
            }
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func searchRides()
    {
        lblErrorMsg.isHidden = true
        if(wpId != "") {
            searchReqObj.userId = UserInfo.sharedInstance.userID
            searchReqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
            searchReqObj.reqTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "hh:mm a")
            searchReqObj.poolType = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
            searchReqObj.searchType = "generic_location"
            searchReqObj.groupID = wpId
            searchReqObj.rideScope = wpId
            searchReqObj.groupType = "6"
        } else {
            searchReqObj.userId = UserInfo.sharedInstance.userID
            searchReqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
            searchReqObj.reqTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "hh:mm a")
            searchReqObj.poolType = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
            searchReqObj.searchType = "generic_location"
            searchReqObj.searchFrom = "anywhere"
        }
        
        if((searchReqObj.startLoc != nil) && (searchReqObj.endLoc != nil)){
            if(searchReqObj.startLoc != searchReqObj.endLoc)
            {
                showIndicator("Searching..")
                if(wpId != "") {
                    WorkplaceRequestor().searchWPRides(searchReqObj, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            if let totalPools = (object as! WPRidesResponse).pools {
                                _ = self.navigationController?.popViewController(animated: true)
                                self.completionBlock?(totalPools, self.searchReqObj)
                            } else {
                                self.lblErrorMsg.isHidden = false
                            }
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                } else {
                    RideRequestor().searchRides(searchReqObj, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            if((object as! AllRidesResponse).pools!.count > 0){
                                //self.navigationController?.popViewControllerAnimated(true)
                                let vc = RideViewController()
                                vc.isSearchResults = true
                                vc.searchReqObj = self.searchReqObj
                                vc.dataSource = (object as! AllRidesResponse).pools!
                                vc.hidesBottomBarWhenPushed = true
                                vc.edgesForExtendedLayout = UIRectEdge()
                                self.navigationController?.pushViewController(vc, animated: true)
                                //self.completionBlock?((object as! AllRidesResponse).pools!, self.searchReqObj)
                            }
                            else{
                                self.lblErrorMsg.isHidden = false
                            }
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                }
            }
            else{
                AlertController.showToastForError("Please choose different locations for source and destination.")
            }
        }
        else{
            AlertController.showToastForError("Please choose location in 'From' and 'To' field from Google Places.")
        }
    }
    
    func mapForFrom()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.searchReqObj.startLoc = self.lblSource.text
                    self.searchReqObj.fromLat = loc["lat"]
                    self.searchReqObj.fromLong = loc["long"]
                }
            }
             vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func mapForTo()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.searchReqObj.endLoc = self.lblDest.text
                    self.searchReqObj.toLat = loc["lat"]
                    self.searchReqObj.toLong = loc["long"]
                }
            }
             vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func createNewRide () {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
