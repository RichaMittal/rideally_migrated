//
//  LoginViewController.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LoginViewController: BaseScrollViewController,UIGestureRecognizerDelegate {
    
    var emailORmobileNumberTextbox: RATextField?
    var passwordTextBox : RATextField?
    var  loaderView : UIView!
    var activityView: UIActivityIndicatorView!
    var emailORmobileNumberLabel: UILabel?
    var passwordLabel: UILabel?
    var yPost_Email:  NSLayoutConstraint!
    var yPost_Password:  NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setBGImageForView()
        viewsArray.append(createLoginViewSubviews())
        loadActivityLoader()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = Colors.GREEN_COLOR
        }
        
    }
    
    func getTextBoxViews(_ placeHolderString:String) -> RATextField
    {
        let textBoxInstance = RATextField()
        textBoxInstance.returnKeyType = UIReturnKeyType.next
        textBoxInstance.delegate = self
        textBoxInstance.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        
        let placeHolder = NSAttributedString(string: placeHolderString, attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
        textBoxInstance.attributedPlaceholder = placeHolder
        
        textBoxInstance.font = normalFontWithSize(15)!
        textBoxInstance.textColor = Colors.WHITE_COLOR
        return textBoxInstance
    }
    
    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.WHITE_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(20)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    
    func loadActivityLoader() -> Void
    {
        self.loaderView = UIView()
        self.loaderView.backgroundColor = UIColor.black
        self.loaderView.isHidden = true
        self.loaderView.layer.borderWidth = 1.0
        self.loaderView.layer.borderColor = UIColor.white.cgColor
        self.loaderView.layer.cornerRadius = 5.0
        self.loaderView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.loaderView)
        
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[loaderView(35)]", options:[], metrics:nil, views: ["loaderView": self.loaderView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[loaderView(100)]", options:[], metrics:nil, views: ["loaderView": self.loaderView]))
        self.view.addConstraint(NSLayoutConstraint(item: self.loaderView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.loaderView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        
        let titleVlaue = UILabel()
        titleVlaue.text = "Loading..."
        titleVlaue.textAlignment = NSTextAlignment.left
        titleVlaue.font = boldFontWithSize(15)
        titleVlaue.textColor = UIColor.white
        titleVlaue.translatesAutoresizingMaskIntoConstraints = false
        self.loaderView.addSubview(titleVlaue)
        
        
        activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityView.hidesWhenStopped = true;
        activityView.translatesAutoresizingMaskIntoConstraints = false
        self.loaderView.addSubview(activityView)
        
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-1-[titleVlaue]-1-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[activityView]-5-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[activityView(25)]-5-[titleVlaue]-5-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
    }
    
    
    func setBGImageForView() -> Void {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "LoginBG.png")
        self.contentView.addSubview(backgroundImage)
    }
    
    func createLoginViewSubviews() ->  SubView{
        self.emailORmobileNumberLabel  =  getLabelSubViews("EMAIL OR MOBILE NUMBER")
        self.emailORmobileNumberLabel!.isHidden = true
        self.emailORmobileNumberLabel!.font = normalFontWithSize(12)
        self.emailORmobileNumberLabel!.textColor = Colors.GREEN_COLOR
        self.emailORmobileNumberLabel!.textAlignment = NSTextAlignment.left
        
        self.passwordLabel  =  getLabelSubViews("PASSWORD")
        self.passwordLabel!.isHidden = true
        self.passwordLabel!.font = normalFontWithSize(12)
        self.passwordLabel!.textColor = Colors.GREEN_COLOR
        self.passwordLabel!.textAlignment = NSTextAlignment.left
        
        let  facebookDescriptioLabelText  =  getLabelSubViews("We will not Post on your behalf")
        let  orLabelText        =  getLabelSubViews("OR")
        let  loginDescriptionLabelText  =  getLabelSubViews("Use your Email ID or Mobile Number")
        let  facebookButtonView =  getButtonSubViews("")
        facebookButtonView.backgroundColor = UIColor.clear
        //facebookButtonView.setBackgroundImage(UIImage(named:"LoginFacebook"), forState:UIControlState.Normal)
        facebookButtonView.setImage(UIImage(named:"LoginFacebook"), for: UIControlState())
        facebookButtonView.contentMode = .center
        facebookButtonView.tag = 99
        
        emailORmobileNumberTextbox = getTextBoxViews("EMAIL OR MOBILE NUMBER")
        emailORmobileNumberTextbox!.returnKeyType = UIReturnKeyType.next
        passwordTextBox = getTextBoxViews("PASSWORD")
        passwordTextBox!.isSecureTextEntry = true
        passwordTextBox!.returnKeyType = UIReturnKeyType.done
        
        let  forgotButtonView  =  getButtonSubViews("FORGOT PASSWORD?")
        forgotButtonView.tag = 100;
        forgotButtonView.backgroundColor = UIColor.clear
        forgotButtonView.contentHorizontalAlignment = .right
        forgotButtonView.titleLabel?.font = normalFontWithSize(15)
        forgotButtonView.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        
        let  loginButtonView  =  getButtonSubViews("LOGIN")
        loginButtonView.tag = 101
        
        
        let  signUpLabelText = UILabel()
        signUpLabelText.isUserInteractionEnabled = true
        signUpLabelText.backgroundColor = UIColor.clear
        signUpLabelText.textColor = Colors.WHITE_COLOR
        signUpLabelText.textAlignment = NSTextAlignment.center
        let myAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myString = NSMutableAttributedString(string: "Don't have a RideAlly account? SIGN UP", attributes: myAttribute)
        let myRange = NSRange(location: 30, length: 8)
        myString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange)
        signUpLabelText.attributedText = myString
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self, action: #selector(labelGesturePressed(_:)))
        signUpLabelText.addGestureRecognizer(tapGesture)
        
        
        let  loginView = UIView()
        loginView.addSubview(facebookButtonView)
        loginView.addSubview(facebookDescriptioLabelText)
        loginView.addSubview(orLabelText)
        loginView.addSubview(loginDescriptionLabelText)
        loginView.addSubview(emailORmobileNumberTextbox!)
        loginView.addSubview(emailORmobileNumberLabel!)
        loginView.addSubview(passwordTextBox!)
        loginView.addSubview(passwordLabel!)
        loginView.addSubview(forgotButtonView)
        loginView.addSubview(loginButtonView)
        loginView.addSubview(signUpLabelText)
        
        facebookButtonView.translatesAutoresizingMaskIntoConstraints = false
        facebookDescriptioLabelText.translatesAutoresizingMaskIntoConstraints = false
        orLabelText.translatesAutoresizingMaskIntoConstraints = false
        loginDescriptionLabelText.translatesAutoresizingMaskIntoConstraints = false
        emailORmobileNumberTextbox!.translatesAutoresizingMaskIntoConstraints = false
        emailORmobileNumberLabel!.translatesAutoresizingMaskIntoConstraints = false
        passwordTextBox!.translatesAutoresizingMaskIntoConstraints = false
        passwordLabel!.translatesAutoresizingMaskIntoConstraints = false
        forgotButtonView.translatesAutoresizingMaskIntoConstraints = false
        loginButtonView.translatesAutoresizingMaskIntoConstraints=false
        signUpLabelText.translatesAutoresizingMaskIntoConstraints = false
        
        let subViewsDict = ["facebookButton": facebookButtonView,"facebookDespt":facebookDescriptioLabelText,"orValue":orLabelText,"loginDesp":loginDescriptionLabelText,"emailormobile":emailORmobileNumberTextbox!,"password":passwordTextBox!,"forgetPassword":forgotButtonView,"loginButton":loginButtonView,"signupLabel":signUpLabelText,"emailLabel":emailORmobileNumberLabel!,"passwordLabel":passwordLabel!]
        
        loginView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[facebookButton(40)]-10-[facebookDespt(20)]-15-[orValue(20)]-15-[loginDesp(20)]-10-[emailormobile(==facebookButton)]-10-[password(==emailormobile)]-10-[forgetPassword(==emailormobile)]-10-[loginButton(==facebookButton)]-10-[signupLabel(==facebookButton)]", options:[], metrics:nil, views: subViewsDict))
        
        loginView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[facebookButton]-15-|", options:[], metrics:nil, views: subViewsDict))
        
        loginView.addConstraint(NSLayoutConstraint(item: facebookButtonView, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: facebookDescriptioLabelText, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: orLabelText, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: loginDescriptionLabelText, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: emailORmobileNumberLabel!, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: emailORmobileNumberLabel!, attribute: .width, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: emailORmobileNumberLabel!, attribute: .height, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: emailORmobileNumberTextbox!, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: emailORmobileNumberTextbox!, attribute: .width, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: passwordLabel!, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: passwordLabel!, attribute: .width, relatedBy: .equal, toItem: emailORmobileNumberTextbox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: passwordLabel!, attribute: .height, relatedBy: .equal, toItem: emailORmobileNumberTextbox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: passwordTextBox!, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: passwordTextBox!, attribute: .width, relatedBy: .equal, toItem: emailORmobileNumberTextbox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        loginView.addConstraint(NSLayoutConstraint(item: forgotButtonView, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: forgotButtonView, attribute: .width, relatedBy: .equal, toItem: emailORmobileNumberTextbox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        loginView.addConstraint(NSLayoutConstraint(item: loginButtonView, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: loginButtonView, attribute: .width, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        loginView.addConstraint(NSLayoutConstraint(item: signUpLabelText, attribute: .centerX, relatedBy: .equal, toItem: loginView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginView.addConstraint(NSLayoutConstraint(item: signUpLabelText, attribute: .width, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        yPost_Email = NSLayoutConstraint(item: emailORmobileNumberLabel!, attribute: .top, relatedBy: .equal, toItem:orLabelText , attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        loginView.addConstraint(yPost_Email)
        
        yPost_Password = NSLayoutConstraint(item: passwordLabel!, attribute: .top, relatedBy: .equal, toItem:emailORmobileNumberTextbox! , attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        loginView.addConstraint(yPost_Password)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let originY = screenSize.height - (screenSize.height * 0.90)
        
        let loginSubViewInstance = SubView()
        loginSubViewInstance.view = loginView
        loginSubViewInstance.padding = UIEdgeInsetsMake(originY, 0, 0, 0)
        loginSubViewInstance.heightConstraint = NSLayoutConstraint(item: loginView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 420)
        
        return loginSubViewInstance
    }
    
    
    func buttonPressed (_ sender: AnyObject)
    {
        
        if  sender.tag != 99
        {
            if sender.tag == 101
            {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"Login Page",
                    AnalyticsParameterContentType:"Submit Button"
                    ])
                callFunctionWhenLoginButtonisPressed()
            }
            else
            {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"Login Page",
                    AnalyticsParameterContentType:"Forgot Password Button"
                    ])
                navigate(ForgetPasswordViewController())
            }
        }
        else
        {
            facebookButtonPressedEvent()
        }
    }
    
    
    func facebookButtonPressedEvent() -> Void
    {
        let fbLogin = FBSDKLoginManager()
        fbLogin.logOut()
        
        fbLogin.logIn(withReadPermissions: ["public_profile","email"], from: self, handler: { (result, error) -> Void in
            
            self.view.bringSubview(toFront: self.loaderView)
            self.loaderView.isHidden = false
            self.activityView.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            
            if (error == nil)
            {
                
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if(fbloginresult.isCancelled)
                {
                    self.view.isUserInteractionEnabled = true
                    self.loaderView.isHidden = true
                    self.activityView.stopAnimating()
                }
                else
                {
                    self.getDetailsAndLogin()
                }
            }
            else
            {
                self.view.isUserInteractionEnabled = true
                self.loaderView.isHidden = true
                self.activityView.stopAnimating()
            }
        })
        
    }
    
    func getDetailsAndLogin() -> Void
    {
        
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,gender,name,first_name,last_name, email,link,birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil)
                {
                    let resultDict = result as! NSDictionary
                    let reqObj = FBLoginRequest()
                    reqObj.device_id = UIDevice.current.identifierForVendor?.uuidString
                    reqObj.id = resultDict.value(forKey: "id") as? String
                    reqObj.email = resultDict.value(forKey: "email") as? String
                    reqObj.firstName = resultDict.value(forKey: "first_name") as? String
                    reqObj.lastName = resultDict.value(forKey: "last_name") as? String
                    reqObj.link = resultDict.value(forKey: "link") as? String
                    reqObj.gender = resultDict.value(forKey: "gender") as? String
                    
                    LoginRequestor().sendFBLoginRequest(reqObj, success:{ (success, object) in
                        if ((object as! FBLoginResponse).FBdataObj?.first?.user_id)?.characters.count > 0
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loaderView.isHidden = true
                            self.activityView.stopAnimating()
                            UserInfo.sharedInstance.userID = ((object as! FBLoginResponse).FBdataObj?.first?.user_id)!
                            UserInfo.sharedInstance.userState = ((object as! FBLoginResponse).FBdataObj?.first?.user_state)!
                            UserInfo.sharedInstance.profilepicStatus = ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_status)!
                            UserInfo.sharedInstance.fullName = ((object as! FBLoginResponse).FBdataObj?.first?.full_name)!
                            UserInfo.sharedInstance.firstName = ((object as! FBLoginResponse).FBdataObj?.first?.first_name)!
                            UserInfo.sharedInstance.lastName = ((object as! FBLoginResponse).FBdataObj?.first?.last_name)!
                            UserInfo.sharedInstance.gender = ((object as! FBLoginResponse).FBdataObj?.first?.gender)!
                            
                            if((object as! FBLoginResponse).FBdataObj?.first?.work_address != nil) {
                                UserInfo.sharedInstance.workAddress = ((object as! FBLoginResponse).FBdataObj?.first?.work_address)!
                                UserInfo.sharedInstance.workLatValue = ((object as! FBLoginResponse).FBdataObj?.first?.work_lat)!
                                UserInfo.sharedInstance.workLongValue = ((object as! FBLoginResponse).FBdataObj?.first?.work_long)!
                            } else {
                                UserInfo.sharedInstance.workAddress = ""
                                UserInfo.sharedInstance.workLatValue = ""
                                UserInfo.sharedInstance.workLongValue = ""
                            }
                            
                            if((object as! FBLoginResponse).FBdataObj?.first?.home_address != nil) {
                                UserInfo.sharedInstance.homeAddress = ((object as! FBLoginResponse).FBdataObj?.first?.home_address)!
                                UserInfo.sharedInstance.homeLatValue = ((object as! FBLoginResponse).FBdataObj?.first?.home_lat)!
                                UserInfo.sharedInstance.homeLongValue = ((object as! FBLoginResponse).FBdataObj?.first?.home_long)!
                            } else {
                                UserInfo.sharedInstance.homeAddress = ""
                                UserInfo.sharedInstance.homeLatValue = ""
                                UserInfo.sharedInstance.homeLongValue = ""
                            }
                            
                            UserInfo.sharedInstance.dob = ((object as! FBLoginResponse).FBdataObj?.first?.dob)!
                            UserInfo.sharedInstance.faceBookid = ((object as! FBLoginResponse).FBdataObj?.first?.fb_id)!
                            UserInfo.sharedInstance.email = ((object as! FBLoginResponse).FBdataObj?.first?.email)!
                            UserInfo.sharedInstance.mobile = ((object as! FBLoginResponse).FBdataObj?.first?.mobile_number)!
                            UserInfo.sharedInstance.isOfficialEmail = ((object as! FBLoginResponse).FBdataObj?.first?.isOfficialEmail)!
                            UserInfo.sharedInstance.secEmail = ""
                            if((object as! FBLoginResponse).FBdataObj?.first?.secondary_email != nil) {
                                UserInfo.sharedInstance.secEmail = ((object as! FBLoginResponse).FBdataObj?.first?.secondary_email)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.secondary_email_status == "2") {
                                UserInfo.sharedInstance.isSecEmailIDVerified = true
                            } else {
                                UserInfo.sharedInstance.isSecEmailIDVerified = false
                            }
                            
                            UserInfo.sharedInstance.isEmailIDVerified = false
                            if (((object as! FBLoginResponse).FBdataObj?.first?.user_state) == "3" || ((object as! FBLoginResponse).FBdataObj?.first?.user_state) == "5")
                            {
                                UserInfo.sharedInstance.isEmailIDVerified = true
                            }
                            
                            UserInfo.sharedInstance.vehicleSeqID = ""
                            UserInfo.sharedInstance.vehicleModel = ""
                            UserInfo.sharedInstance.vehicleRegNo = ""
                            UserInfo.sharedInstance.vehicleSeatCapacity = ""
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.work_address)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.workAddress = ((object as! FBLoginResponse).FBdataObj?.first?.work_address)!
                            }
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_seqID)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeqID = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_seqID)!
                            }
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_model)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleModel = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_model)!
                            }
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_regNo)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleRegNo = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_regNo)!
                            }
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_capacity)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeatCapacity = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_capacity)!
                            }
                            
                            var boolVal = true
                            if ((object as! FBLoginResponse).FBdataObj?.first?.share_mobile_number)! == "0"
                            {
                                boolVal = false
                            }
                            UserInfo.sharedInstance.shareMobileNumber = boolVal
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_status)! == "0"
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                            }
                            else
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                                if ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_url)?.characters.count > 0
                                {
                                    UserInfo.sharedInstance.profilepicUrl = ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_url)!
                                }
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides != "") {
                                UserInfo.sharedInstance.allRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces != "") {
                                UserInfo.sharedInstance.allWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace != "") {
                                UserInfo.sharedInstance.myWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace != "") {
                                UserInfo.sharedInstance.myPendingWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered != "") {
                                UserInfo.sharedInstance.offeredRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed != "") {
                                UserInfo.sharedInstance.neededRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined != "") {
                                UserInfo.sharedInstance.joinedRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked != "") {
                                UserInfo.sharedInstance.bookedRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints != "") {
                                UserInfo.sharedInstance.maxAllowedPoints = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable != nil && String(describing: (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable) != "") {
                                UserInfo.sharedInstance.availablePoints = (((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable)!)
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere != nil && (object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere != "") {
                                UserInfo.sharedInstance.hideAnywhere = ((object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere)!
                            }
                            self.sendDeviceToken()
                            UserInfo.sharedInstance.isLoggedIn = true
                            
                            if (object as! FBLoginResponse).code == "411" && (UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3") {
                                hideIndicator()
                                AlertController.showAlertFor("RideAlly", message: "Welcome \(UserInfo.sharedInstance.firstName) ! you are successfully registered with RideAlly.", okButtonTitle: "Ok", okAction: {
                                    AlertController.showToastForInfo("Please provide 'Mobile Number' to proceed further.")
                                    let vc = MobileVerificationViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                })
                            } else if UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3" {
                                hideIndicator()
                                if(UserInfo.sharedInstance.mobile == "") {
                                    AlertController.showToastForInfo("Please provide 'Mobile Number' to proceed further.")
                                } else {
                                    AlertController.showToastForInfo("Please verify 'Mobile Number' to proceed further.")
                                }
                                let vc = MobileVerificationViewController()
                                vc.hidesBottomBarWhenPushed = true
                                vc.edgesForExtendedLayout = UIRectEdge()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else if(UserInfo.sharedInstance.homeAddress == "") {
                                hideIndicator()
                                AlertController.showToastForInfo("Please provide 'Home Location' to proceed further.")
                                let vc = SignUpMapViewController()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else {
                                hideIndicator()
                                let myRides = Int(UserInfo.sharedInstance.offeredRides)! + Int(UserInfo.sharedInstance.neededRides)! + Int(UserInfo.sharedInstance.joinedRides)! + Int(UserInfo.sharedInstance.bookedRides)!
                                let ins = UIApplication.shared.delegate as! AppDelegate
                                if(myRides > 0) {  //|| UserInfo.sharedInstance.isOfficialEmail == "0"
                                    let vc = RideViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                                    if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0) {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    } else if(Int(UserInfo.sharedInstance.myWorkplaces) == 1){
                                        self.getMyWP()
                                    } else {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    }
                                } else {
                                    let vc = PoolOptionsViewController()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        else
                        {
                            AlertController.showToastForError("User_ID is not there in the response.")
                        }
                        
                        
                    }){ (error) in
                        hideIndicator()
                        AlertController.showAlertForError(error)
                    }
                }                else
                {
                    AlertController.showToastForError("Sorry, either 'Email Id' or 'Password' is incorrect.")
                }
            })
        }
    }
    
    
    
    
    func callFunctionWhenLoginButtonisPressed() -> Void {
        if  handleValidation().characters.count  == 0
        {
            sendLoginRequest()
        }
        else
        {
            AlertController.showToastForError(handleValidation())
        }
        
    }
    
    func labelGesturePressed(_ sender: UITapGestureRecognizer)  {
        navigate(SignUpViewController())
    }
    
    func isPasswordInValid(_ stringValue:String) -> Bool{
        if stringValue == "402" {
            return true
        }
        return false
    }
    
    
    func handleValidation() -> String {
        
        if emailORmobileNumberTextbox!.text?.characters.count > 0 && passwordTextBox!.text?.characters.count > 0
        {
            if isNumberOnly(emailORmobileNumberTextbox!.text!)
            {
                if emailORmobileNumberTextbox!.text?.characters.count != 10
                {
                    return "Please provide valid 10 digit mobile number"
                }
            }
            else if !isValidEmailFormate(emailORmobileNumberTextbox!.text!)
            {
                return "Please enter valid 'Email Id'"
            }
            
        }
        else
        {
            if  emailORmobileNumberTextbox!.text?.characters.count == 0
            {
                return "Please provide 'Email Id' or 'Mobile Number'"
            }
            else if passwordTextBox!.text?.characters.count == 0
            {
                return "Please provide Password"
            }
        }
        
        return ""
    }
    
    func sendLoginRequest() -> Void {
        
        let reqObj = LoginRequest()
        showIndicator("Loading...")
        reqObj.email = emailORmobileNumberTextbox!.text
        reqObj.password = passwordTextBox!.text
        reqObj.device_id = UIDevice.current.identifierForVendor?.uuidString
        reqObj.device_type = "2"
        
        LoginRequestor().sendLoginRequest(reqObj, success:{ (success, object) in
            //hideIndicator()
            
            if (object as! LoginResponse).code == "400"
            {
                if (object as! LoginResponse).dataObj?.first != nil
                {
                    if (object as! LoginResponse).dataObj?.first?.user_id != nil
                    {
                        if (object as! LoginResponse).dataObj?.first?.user_id?.characters.count > 0
                        {
                            UserInfo.sharedInstance.userID = ((object as! LoginResponse).dataObj?.first?.user_id)!
                            UserInfo.sharedInstance.userState = ((object as! LoginResponse).dataObj?.first?.user_state)!
                            UserInfo.sharedInstance.password = self.passwordTextBox!.text!
                            UserInfo.sharedInstance.profilepicStatus = ((object as! LoginResponse).dataObj?.first?.profilepic_status)!
                            UserInfo.sharedInstance.fullName = ((object as! LoginResponse).dataObj?.first?.full_name)!
                            UserInfo.sharedInstance.firstName = ((object as! LoginResponse).dataObj?.first?.first_name)!
                            UserInfo.sharedInstance.lastName = ((object as! LoginResponse).dataObj?.first?.last_name)!
                            UserInfo.sharedInstance.gender = ((object as! LoginResponse).dataObj?.first?.gender)!
                            //                            UserInfo.sharedInstance.homeAddress = ((object as! LoginResponse).dataObj?.first?.home_address)!
                            //                            UserInfo.sharedInstance.homeLatValue = ((object as! LoginResponse).dataObj?.first?.home_lat)!
                            //                            UserInfo.sharedInstance.homeLongValue = ((object as! LoginResponse).dataObj?.first?.home_long)!
                            UserInfo.sharedInstance.isOfficialEmail = ((object as! LoginResponse).dataObj?.first?.isOfficialEmail)!
                            UserInfo.sharedInstance.secEmail = ""
                            if((object as! LoginResponse).dataObj?.first?.secondary_email != nil) {
                                UserInfo.sharedInstance.secEmail = ((object as! LoginResponse).dataObj?.first?.secondary_email)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.secondary_email_status == "2") {
                                UserInfo.sharedInstance.isSecEmailIDVerified = true
                            } else {
                                UserInfo.sharedInstance.isSecEmailIDVerified = false
                            }
                            
                            UserInfo.sharedInstance.isEmailIDVerified = false
                            if ((object as! LoginResponse).dataObj?.first?.user_state) == "3" || ((object as! LoginResponse).dataObj?.first?.user_state) == "5"
                            {
                                UserInfo.sharedInstance.isEmailIDVerified = true
                            }
                            
                            let  imageURLvalue = IMG_URLS_BASE + UserInfo.sharedInstance.profilepicUrl
                            let  actualURL = URL(string:imageURLvalue)
                            
                            UserInfo.sharedInstance.isProfilePicContent = false
                            if  (try? Data(contentsOf: actualURL!)) != nil
                            {
                                UserInfo.sharedInstance.isProfilePicContent = true
                            }
                            
                            if((object as! LoginResponse).dataObj?.first?.home_address != nil) {
                                UserInfo.sharedInstance.homeAddress = ((object as! LoginResponse).dataObj?.first?.home_address)!
                                UserInfo.sharedInstance.homeLatValue = ((object as! LoginResponse).dataObj?.first?.home_lat)!
                                UserInfo.sharedInstance.homeLongValue = ((object as! LoginResponse).dataObj?.first?.home_long)!
                            } else {
                                UserInfo.sharedInstance.homeAddress = ""
                                UserInfo.sharedInstance.homeLatValue = ""
                                UserInfo.sharedInstance.homeLongValue = ""
                            }
                            
                            if((object as! LoginResponse).dataObj?.first?.work_address != nil) {
                                UserInfo.sharedInstance.workAddress = ((object as! LoginResponse).dataObj?.first?.work_address)!
                                UserInfo.sharedInstance.workLatValue = ((object as! LoginResponse).dataObj?.first?.work_lat)!
                                UserInfo.sharedInstance.workLongValue = ((object as! LoginResponse).dataObj?.first?.work_long)!
                            } else {
                                UserInfo.sharedInstance.workAddress = ""
                                UserInfo.sharedInstance.workLatValue = ""
                                UserInfo.sharedInstance.workLongValue = ""
                            }
                            UserInfo.sharedInstance.vehicleSeqID = ""
                            UserInfo.sharedInstance.vehicleModel = ""
                            UserInfo.sharedInstance.vehicleRegNo = ""
                            UserInfo.sharedInstance.vehicleSeatCapacity = ""
                            
                            
                            if ((object as! LoginResponse).dataObj?.first?.work_address)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.workAddress = ((object as! LoginResponse).dataObj?.first?.work_address)!
                            }
                            
                            if ((object as! LoginResponse).dataObj?.first?.default_Vehicle_seqID)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeqID = ((object as! LoginResponse).dataObj?.first?.default_Vehicle_seqID)!
                            }
                            
                            if ((object as! LoginResponse).dataObj?.first?.default_Vehicle_model)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleModel = ((object as! LoginResponse).dataObj?.first?.default_Vehicle_model)!
                            }
                            
                            if ((object as! LoginResponse).dataObj?.first?.default_Vehicle_regNo)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleRegNo = ((object as! LoginResponse).dataObj?.first?.default_Vehicle_regNo)!
                            }
                            
                            if ((object as! LoginResponse).dataObj?.first?.default_Vehicle_capacity)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeatCapacity = ((object as! LoginResponse).dataObj?.first?.default_Vehicle_capacity)!
                            }
                            
                            UserInfo.sharedInstance.dob = ((object as! LoginResponse).dataObj?.first?.dob)!
                            UserInfo.sharedInstance.faceBookid = ((object as! LoginResponse).dataObj?.first?.fb_id)!
                            UserInfo.sharedInstance.email = ((object as! LoginResponse).dataObj?.first?.email)!
                            UserInfo.sharedInstance.mobile = ((object as! LoginResponse).dataObj?.first?.mobile_number)!
                            
                            var boolVal = true
                            if (object as! LoginResponse).dataObj?.first?.share_mobile_number == "0"
                            {
                                boolVal = false
                            }
                            
                            UserInfo.sharedInstance.shareMobileNumber = boolVal
                            
                            
                            if ((object as! LoginResponse).dataObj?.first?.profilepic_status)! == "0"
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                            }
                            else
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                                if(((object as! LoginResponse).dataObj?.first?.profilepic_url)?.characters.count > 0) {
                                    UserInfo.sharedInstance.profilepicUrl = ((object as! LoginResponse).dataObj?.first?.profilepic_url)!
                                }
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.allRides != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.allRides != "") {
                                UserInfo.sharedInstance.allRides = ((object as! LoginResponse).dataObj?.first?.userDetail?.allRides)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.allWorkplaces != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.allWorkplaces != "") {
                                UserInfo.sharedInstance.allWorkplaces = ((object as! LoginResponse).dataObj?.first?.userDetail?.allWorkplaces)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.myWorkplace != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.myWorkplace != "") {
                                UserInfo.sharedInstance.myWorkplaces = ((object as! LoginResponse).dataObj?.first?.userDetail?.myWorkplace)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.myPendingWorkplace != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.myPendingWorkplace != "") {
                                UserInfo.sharedInstance.myPendingWorkplaces = ((object as! LoginResponse).dataObj?.first?.userDetail?.myPendingWorkplace)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.offered != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.offered != "") {
                                UserInfo.sharedInstance.offeredRides = ((object as! LoginResponse).dataObj?.first?.userDetail?.offered)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.needed != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.needed != "") {
                                UserInfo.sharedInstance.neededRides = ((object as! LoginResponse).dataObj?.first?.userDetail?.needed)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.joined != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.joined != "") {
                                UserInfo.sharedInstance.joinedRides = ((object as! LoginResponse).dataObj?.first?.userDetail?.joined)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.booked != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.booked != "") {
                                UserInfo.sharedInstance.bookedRides = ((object as! LoginResponse).dataObj?.first?.userDetail?.booked)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.pointsDetail?.maximumallowedpoints != nil && (object as! LoginResponse).dataObj?.first?.userDetail?.pointsDetail?.maximumallowedpoints != "") {
                                UserInfo.sharedInstance.maxAllowedPoints = ((object as! LoginResponse).dataObj?.first?.userDetail?.pointsDetail?.maximumallowedpoints)!
                            }
                            if((object as! LoginResponse).dataObj?.first?.userDetail?.pointsDetail?.pointsavailable != nil && String(describing: (object as! LoginResponse).dataObj?.first?.userDetail?.pointsDetail?.pointsavailable) != "") {
                                UserInfo.sharedInstance.availablePoints = (((object as! LoginResponse).dataObj?.first?.userDetail?.pointsDetail?.pointsavailable)!)
                            }
                            if((object as! LoginResponse).dataObj?.first?.hideAnywhere != nil && (object as! LoginResponse).dataObj?.first?.hideAnywhere != "") {
                                UserInfo.sharedInstance.hideAnywhere = ((object as! LoginResponse).dataObj?.first?.hideAnywhere)!
                            }
                            
                            self.sendDeviceToken()
                            UserInfo.sharedInstance.isLoggedIn = true
                            
                            if  UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3" {
                                hideIndicator()
                                AlertController.showToastForInfo("Please verify 'Mobile Number' to proceed further.")
                                let vc = MobileVerificationViewController()
                                vc.hidesBottomBarWhenPushed = true
                                vc.edgesForExtendedLayout = UIRectEdge()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else if(UserInfo.sharedInstance.homeAddress == "") {
                                hideIndicator()
                                AlertController.showToastForInfo("Please provide 'Home Location' to proceed further.")
                                let vc = SignUpMapViewController()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else {
                                hideIndicator()
                                let myRides = Int(UserInfo.sharedInstance.offeredRides)! + Int(UserInfo.sharedInstance.neededRides)! + Int(UserInfo.sharedInstance.joinedRides)! + Int(UserInfo.sharedInstance.bookedRides)!
                                let ins = UIApplication.shared.delegate as! AppDelegate
                                if(myRides > 0) { // || UserInfo.sharedInstance.isOfficialEmail == "0"
                                    let vc = RideViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                                    if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0) {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    } else if(Int(UserInfo.sharedInstance.myWorkplaces) == 1){
                                        self.getMyWP()
                                    } else {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    }
                                } else {
                                    let vc = PoolOptionsViewController()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        else
                        {
                            hideIndicator()
                            //message used for development purpose
                            AlertController.showAlertForMessage("UserID string is empty from the response data")
                        }
                    }
                    else
                    {
                        hideIndicator()
                        //message used for development purpose
                        AlertController.showAlertForMessage("UserID object is nil from the response data")
                    }
                }
            }
            else
            {
                hideIndicator()
                if self.isPasswordInValid((object as! LoginResponse).code!)
                {
                    AlertController.showToastForError("Sorry, either 'Email Id' or 'Password' is incorrect.")
                }
                else
                {
                    AlertController.showToastForError("Sorry, either 'Email Id' or 'Mobile Number' is not registered with RideAlly. You may Sign Up.")
                }
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
    
    
    func navigate(_ viewController:UIViewController) -> Void {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField === emailORmobileNumberTextbox)
        {
            yPost_Email.constant = 45;
            emailORmobileNumberTextbox!.placeholder = ""
            emailORmobileNumberLabel!.isHidden = false
        }
        else if (textField === passwordTextBox)
        {
            yPost_Password.constant = 30;
            passwordTextBox!.placeholder = ""
            passwordLabel!.isHidden = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField === emailORmobileNumberTextbox)
        {
            yPost_Email.constant = 0
            let placeHolder = NSAttributedString(string: "EMAIL OR MOBILE NUMBER", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            emailORmobileNumberTextbox!.attributedPlaceholder = placeHolder
            emailORmobileNumberLabel!.isHidden = true
            passwordTextBox!.becomeFirstResponder()
        }
        else if (textField === passwordTextBox)
        {
            yPost_Password.constant = 0
            let placeHolder = NSAttributedString(string: "PASSWORD", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            passwordTextBox!.attributedPlaceholder = placeHolder
            passwordLabel!.isHidden = true
            passwordTextBox!.resignFirstResponder()
        }
        
        return true
    }
}
