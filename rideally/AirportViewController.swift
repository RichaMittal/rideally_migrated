//
//  AirportViewController.swift
//  rideally
//
//  Created by Sarav on 05/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import QuartzCore
import FirebaseAnalytics

class AirportViewController: BaseScrollViewController {
    
    lazy var priceEstimationView: SubView = self.getPriceEstimationView()
    lazy var paymentOptionView: SubView = self.getPaymentOptionView()
    var selectedTxtField: UITextField?
    var rideDetailData: RideDetail?
    var toggleState = 1
    var onRidesExistBlock: actionBlockWithParams?
    var defaultLocationData = GetDefaultLocationData()
    let airportRideObj = AirportRequest()
    var signupFlow = false
    
    override func hasTabBarItems() -> Bool
    {
        ISWPRIDESSCREEN = false
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getDefaultLocation()
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController?.tabBarController?.tabBar.isTranslucent = false
        if(signupFlow) {
            hidesBottomBarWhenPushed = true
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        } else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_menu"), style: .plain, target: self, action: #selector(showMenu))
        }
        self.txtComments.delegate = self;
        addViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Airport"
        if(self.airportRideObj.fromLocation != nil && self.airportRideObj.toLocation != nil) {
            CommonRequestor().getDistanceDuration(self.airportRideObj.fromLocation!, destination: self.airportRideObj.toLocation!, success: { (success, object) in
                self.airportRideObj.duration = ""
                self.airportRideObj.distance = ""
                
                if let data = object as? NSDictionary{
                   let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                    let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                    if((elements["status"] as! String) == "OK")
                    {
                        let distance = ((elements["distance"] as! NSDictionary)["text"])!
                        let duration = ((elements["duration"] as! NSDictionary)["text"])!
                        
                        self.airportRideObj.duration = duration as? String
                        self.airportRideObj.distance = distance as? String
                    }
                }
//                let sourceStr = "\(self.airportRideObj.fromLat!),\(self.airportRideObj.fromLong!)"
//                let destinationStr = "\(self.airportRideObj.toLat!), \(self.airportRideObj.toLong!)"
//                CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
//                    self.airportRideObj.via = ""
//                    self.airportRideObj.waypoints = ""
//                    if let data = object as? NSDictionary{
//                        let routes = data["routes"] as! NSArray
//                        if routes.count == 0{
//                            return
//                        }
//                        let routesDict = routes[0] as! NSDictionary
//                        let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
//                        let points = overview_polyline ["points"] as! NSString
//                        self.airportRideObj.via = routesDict ["summary"] as? String
//                        self.airportRideObj.waypoints = points as String
//                    }
//                    }, failure: { (error) in
//                })
                }, failure: { (error) in
            })
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    func addViews()
    {
        viewsArray.append(getLocationView())
        //viewsArray.append(getTravelWithView())
        viewsArray.append(getTimeView())
        viewsArray.append(priceEstimationView)
        viewsArray.append(paymentOptionView)
        viewsArray.append(getCommentView())
        addBottomView(getBottomView())
    }
    
    let lblSource = UILabel()
    let lblDest = UILabel()
    let iconSourceArrow = UIImageView()
    let iconDestArrow = UIImageView()
    
    let lblCaption = UILabel()
    func getLocationView() -> SubView
    {
        let view = UIView()
        
        lblCaption.text = "Book a taxi for Bangalore Airport only"
        lblCaption.font = boldFontWithSize(18)
        lblCaption.textColor = Colors.GRAY_COLOR
        lblCaption.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCaption)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSource)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.text = "From (Select exact or nearby google places)"
        lblSource.font = boldFontWithSize(14)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(mapForFrom))
        lblSource.isUserInteractionEnabled = true
        lblSource.addGestureRecognizer(tapGestureFrom)
        view.addSubview(lblSource)
        
        iconSourceArrow.image = UIImage(named: "rightArrow")
        iconSourceArrow.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSourceArrow)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let iconFlip = UIButton()
        iconFlip.setImage(UIImage(named: "airport_flip"), for: UIControlState())
        iconFlip.translatesAutoresizingMaskIntoConstraints = false
        iconFlip.addTarget(self, action: #selector(FlipLocations), for: .touchDown)
        view.addSubview(iconFlip)
        
        let lblLine4 = UILabel()
        lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine4.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine4)
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDest)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = boldFontWithSize(14)
        lblDest.text = self.defaultLocationData.locName
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        
        iconDestArrow.image = UIImage(named: "rightArrow")
        iconDestArrow.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDestArrow)
        iconDestArrow.isHidden = true
        
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForTo))
        lblDest.isUserInteractionEnabled = false
        lblDest.addGestureRecognizer(tapGestureTo)
        
        view.addSubview(lblDest)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        let viewsDict = ["caption":lblCaption, "line1":lblLine1, "isource":iconSource, "source":lblSource, "line2":lblLine2, "iflip":iconFlip, "line4":lblLine4, "idest":iconDest, "dest":lblDest, "line3":lblLine3, "sarrow":iconSourceArrow, "darrow":iconDestArrow]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[caption(50)][line1(0.5)][source(40)][line2(0.5)]-5-[iflip(30)]-5-[line4(0.5)][dest(40)][line3(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[caption]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource(15)][source][sarrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[iflip(30)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest(15)][dest][darrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: iconSourceArrow, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDestArrow, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: iconFlip, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 175)
        return sub
    }
    
    func mapForFrom()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.airportRideObj.fromLocation = self.lblSource.text
                    self.airportRideObj.fromLat = loc["lat"]
                    self.airportRideObj.fromLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func mapForTo()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.airportRideObj.toLocation = self.lblDest.text
                    self.airportRideObj.toLat = loc["lat"]
                    self.airportRideObj.toLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    /*let btnPersonal = DLRadioButton()
     let btnShared = DLRadioButton()
     
     func getTravelWithView() -> SubView
     {
     let view = UIView()
     
     let lblTitle = UILabel()
     lblTitle.text = "I WANT TO BOOK A"
     lblTitle.font = boldFontWithSize(11)
     lblTitle.textColor = Colors.GRAY_COLOR
     lblTitle.translatesAutoresizingMaskIntoConstraints = false
     view.addSubview(lblTitle)
     
     btnPersonal.setTitle("Personal Taxi", forState: .Normal)
     btnPersonal.setTitleColor(Colors.GRAY_COLOR, forState: .Normal)
     btnPersonal.iconColor = Colors.GREEN_COLOR
     btnPersonal.indicatorColor = Colors.GREEN_COLOR
     btnPersonal.titleLabel?.font = boldFontWithSize(14)
     btnPersonal.translatesAutoresizingMaskIntoConstraints = false
     btnPersonal.selected = true
     view.addSubview(btnPersonal)
     
     btnShared.setTitle("Shared Taxi", forState: .Normal)
     btnShared.setTitleColor(Colors.GRAY_COLOR, forState: .Normal)
     btnShared.iconColor = Colors.GREEN_COLOR
     btnShared.indicatorColor = Colors.GREEN_COLOR
     btnShared.titleLabel?.font = boldFontWithSize(14)
     btnShared.translatesAutoresizingMaskIntoConstraints = false
     view.addSubview(btnShared)
     btnPersonal.otherButtons = [btnShared]
     
     let lblLine1 = UILabel()
     lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
     lblLine1.translatesAutoresizingMaskIntoConstraints = false
     view.addSubview(lblLine1)
     
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[personal(100)]-10-[shared(150)]", options: [], metrics: nil, views: ["personal":btnPersonal, "shared":btnShared, "line":lblLine1]))
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[title(20)]-5-[personal(25)]-4-[line(0.5)]", options: [], metrics: nil, views: ["personal":btnPersonal, "title":lblTitle, "line":lblLine1]))
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[title]", options: [], metrics: nil, views: ["title":lblTitle]))
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
     
     view.addConstraint(NSLayoutConstraint(item: btnShared, attribute: .CenterY, relatedBy: .Equal, toItem: btnPersonal, attribute: .CenterY, multiplier: 1, constant: 0))
     
     let sub = SubView()
     sub.view = view
     sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
     sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 60)
     return sub
     }
     */
    func datePicked()
    {
        
    }
    
    func timePicked()
    {
        
    }
    
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    let txtDate = RATextField()
    let txtTime = RATextField()
    
    func getTimeView() -> SubView
    {
        let view = UIView()
        
        let lblDate = UILabel()
        lblDate.text = "I WANT TO TRAVEL ON"
        lblDate.textColor = Colors.GRAY_COLOR
        lblDate.font = boldFontWithSize(11)
        lblDate.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDate)
        
        txtDate.layer.borderWidth = 0.5
        txtDate.delegate = self
        txtDate.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtDate.translatesAutoresizingMaskIntoConstraints = false
        txtDate.font = boldFontWithSize(14)
        
        datePicker.datePickerMode = .date
        txtDate.inputView = datePicker
        datePicker.minimumDate = Date().addingTimeInterval(14400)
        datePicker.addTarget(self, action: #selector(datePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtDate.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "cal")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        txtDate.rightView = imgV
        txtDate.rightViewMode = .always
        view.addSubview(txtDate)
        
        let lblTime = UILabel()
        lblTime.text = "RIDE TIME"
        lblTime.textColor = Colors.GRAY_COLOR
        lblTime.font = boldFontWithSize(11)
        lblTime.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTime)
        
        txtTime.layer.borderWidth = 0.5
        txtTime.delegate = self
        txtTime.font = boldFontWithSize(14)
        txtTime.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtTime.translatesAutoresizingMaskIntoConstraints = false
        
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "NL")
        txtTime.inputView = timePicker
        timePicker.date = Date().addingTimeInterval(14400)
        timePicker.addTarget(self, action: #selector(timePicked), for: .valueChanged)
        
        txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV1 = UIImageView(image: UIImage(named: "time")!)
        imgV1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV1.contentMode = .center
        
        txtTime.rightView = imgV1
        txtTime.rightViewMode = .always
        view.addSubview(txtTime)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbldate":lblDate, "txtdate":txtDate, "line":lblLine1, "lbltime":lblTime, "txttime":txtTime]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbldate(20)]-5-[txtdate(30)]-10-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbltime(20)]-5-[txttime(30)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbldate]-10-[lbltime(==lbldate)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txtdate]-10-[txttime(==txtdate)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        airportRideObj.rideDate = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "dd-MMM-yyyy")
        txtDate.text = airportRideObj.rideDate
        airportRideObj.rideTime = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "HH:mm")
        txtTime.text = airportRideObj.rideTime
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
        
    }
    
    func donePressed()
    {
        if(selectedTxtField == txtDate){
            airportRideObj.rideDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            selectedTxtField?.text = airportRideObj.rideDate
        }
        else if(selectedTxtField == txtTime){
            let minDate = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "dd-MMM-yyyy")
            let minTime = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "HH:mm")
            airportRideObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            let compareResult = minDate.compare(airportRideObj.rideDate!)
            let compareResultTime = minTime.compare(airportRideObj.rideTime!)
            if(compareResult == ComparisonResult.orderedSame && compareResultTime == ComparisonResult.orderedDescending) {
                AlertController.showToastForError("Sorry, You can book taxi only after 4 hours from current time.")
            }
            selectedTxtField?.text = airportRideObj.rideTime
        }
        selectedTxtField?.resignFirstResponder()
    }
    
    var lblSubTopConstraint: NSLayoutConstraint?
    let txtVehicleTypeTitle = UILabel()
    let txtVehicleType = RATextField()
    let txtCostTitle = UILabel()
    let txtCost = RATextField()
    
    func getPriceEstimationView() -> SubView
    {
        let view = UIView()
        
        txtVehicleTypeTitle.translatesAutoresizingMaskIntoConstraints = false
        txtVehicleTypeTitle.text = "VEHICLE TYPE"
        txtVehicleTypeTitle.font = boldFontWithSize(11)
        txtVehicleTypeTitle.textColor = Colors.GRAY_COLOR
        txtVehicleTypeTitle.tag = 401
        view.addSubview(txtVehicleTypeTitle)
        
        txtVehicleType.translatesAutoresizingMaskIntoConstraints = false
        txtVehicleType.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtVehicleType.layer.borderWidth = 0.5
        txtVehicleType.tag = 501
        txtVehicleType.font = boldFontWithSize(14)
        view.addSubview(txtVehicleType)
        
        txtCostTitle.translatesAutoresizingMaskIntoConstraints = false
        txtCostTitle.text = "COST"
        txtCostTitle.font = boldFontWithSize(11)
        txtCostTitle.textColor = Colors.GRAY_COLOR
        txtCostTitle.tag = 403
        view.addSubview(txtCostTitle)
        
        txtCost.translatesAutoresizingMaskIntoConstraints = false
        txtCost.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtCost.layer.borderWidth = 0.5
        txtCost.tag = 503
        txtCost.font = boldFontWithSize(14)
        view.addSubview(txtCost)
        
        txtVehicleTypeTitle.isHidden = true
        txtVehicleType.isHidden = true
        txtCostTitle.isHidden = true
        txtCost.isHidden = true
        
        txtVehicleType.isUserInteractionEnabled = false
        txtCost.isUserInteractionEnabled = false
        
        let btnPriceEstimation = UIButton()
        btnPriceEstimation.translatesAutoresizingMaskIntoConstraints = false
        btnPriceEstimation.setTitle(" CLICK HERE TO SELECT VEHICLE TYPE AND GET PRICE ESTIMATION", for: UIControlState())
        btnPriceEstimation.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnPriceEstimation.setImage(UIImage(named: "addV"), for: UIControlState())
        btnPriceEstimation.contentHorizontalAlignment = .left
        btnPriceEstimation.titleLabel?.font = boldFontWithSize(10.8)
        btnPriceEstimation.addTarget(self, action: #selector(gotoPriceEstimationList), for: .touchDown)
        view.addSubview(btnPriceEstimation)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let viewsDict = ["lblTypeT":txtVehicleTypeTitle, "lblType":txtVehicleType, "txtCostT":txtCostTitle, "txtCost":txtCost, "lblsub":btnPriceEstimation, "line1":lblLine2]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lblTypeT(20)]-5-[lblType(30)]", options: [], metrics: nil, views: viewsDict))
        
        lblSubTopConstraint = NSLayoutConstraint(item: btnPriceEstimation, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 5)
        view.addConstraint(lblSubTopConstraint!)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lblsub(20)]-5-[line1(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[txtCostT(20)]-5-[txtCost(30)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblTypeT]-10-[txtCostT(==lblTypeT)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblType]-10-[txtCost(==lblType)]-5-|", options: [], metrics: nil, views: viewsDict))
        
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblsub]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 31)
        return sub
    }
    
    func setLblProp(_ lbl: UILabel, isTitle: Bool, parentView: UIView)
    {
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = boldFontWithSize(12)
        lbl.textColor = Colors.GRAY_COLOR
        if(isTitle){
            lbl.font = normalFontWithSize(8)
            lbl.textColor = UIColor.lightGray
        }
        
        parentView.addSubview(lbl)
    }
    
    let btnCashToDriver = DLRadioButton()
    let btnPayOnlineLater = DLRadioButton()
    let lblBottom = UILabel()
    let lblLine1 = UILabel()
    let lblLine2 = UILabel()
    
    func getPaymentOptionView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "CHOOSE PAYMENT OPTION BELOW"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        btnCashToDriver.setTitle("Cash To Driver", for: UIControlState())
        btnCashToDriver.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnCashToDriver.iconColor = Colors.GREEN_COLOR
        btnCashToDriver.indicatorColor = Colors.GREEN_COLOR
        btnCashToDriver.titleLabel?.font = boldFontWithSize(14)
        btnCashToDriver.translatesAutoresizingMaskIntoConstraints = false
        btnCashToDriver.addTarget(self, action: #selector(CashToDriverRDBtn), for: .touchDown)
        view.addSubview(btnCashToDriver)
        
        btnPayOnlineLater.setTitle("Pay Online Later", for: UIControlState())
        btnPayOnlineLater.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnPayOnlineLater.iconColor = Colors.GREEN_COLOR
        btnPayOnlineLater.indicatorColor = Colors.GREEN_COLOR
        btnPayOnlineLater.titleLabel?.font = boldFontWithSize(14)
        btnPayOnlineLater.translatesAutoresizingMaskIntoConstraints = false
        btnPayOnlineLater.addTarget(self, action: #selector(PayOnlineRDBtn), for: .touchDown)
        view.addSubview(btnPayOnlineLater)
        btnCashToDriver.otherButtons = [btnPayOnlineLater]
        
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        lblBottom.text = ""
        lblBottom.font = boldFontWithSize(11)
        lblBottom.textColor = Colors.GRAY_COLOR
        lblBottom.translatesAutoresizingMaskIntoConstraints = false
        lblBottom.isHidden = true
        view.addSubview(lblBottom)
        
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        lblLine2.isHidden = true
        view.addSubview(lblLine2)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[driver(120)]-10-[online(150)]", options: [], metrics: nil, views: ["driver":btnCashToDriver, "online":btnPayOnlineLater]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title(20)]-5-[driver(25)]-4-[line1(0.5)]-4-[bottom(20)]-4-[line2(0.5)]", options: [], metrics: nil, views: ["driver":btnCashToDriver, "title":lblTitle, "line1":lblLine1, "bottom":lblBottom, "line2":lblLine2]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[title]", options: [], metrics: nil, views: ["title":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[bottom]|", options: [], metrics: nil, views: ["bottom":lblBottom]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: ["line1":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: ["line2":lblLine2]))
        
        view.addConstraint(NSLayoutConstraint(item: btnPayOnlineLater, attribute: .centerY, relatedBy: .equal, toItem: btnCashToDriver, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    let txtComments = RATextField()
    func getCommentView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "COMMENT"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtComments.font = boldFontWithSize(14)
        txtComments.placeholder = "Enter your comment here"
        txtComments.translatesAutoresizingMaskIntoConstraints = false
        txtComments.returnKeyType = UIReturnKeyType.done
        view.addSubview(txtComments)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbl":lblTitle, "txt":txtComments, "line":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)][txt(40)]-2-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txt]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 10, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    let btnSubmit = UIButton()
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        let myAttribute = [NSFontAttributeName: boldFontWithSize(8)!]
        let myString = NSMutableAttributedString(string: "By clicking on 'Book Taxi', you agree to our Bangalore Airport Terms and Conditions", attributes: myAttribute)
        let myRange = NSRange(location: 45, length: 38)
        myString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange)
        lblTitle.attributedText = myString
        let tapGesture = UITapGestureRecognizer(target:self, action:#selector(labelGesturePressed(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        lblTitle.addGestureRecognizer(tapGesture)
        lblTitle.textAlignment = .center
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.isUserInteractionEnabled = true
        lblTitle.backgroundColor = Colors.WHITE_COLOR
        view.addSubview(lblTitle)
        
        let btn = UIButton()
//        if(signupFlow) {
//            btn.backgroundColor = Colors.GRAY_COLOR
//            btn.setTitleColor(Colors.WHITE_COLOR, forState: .Normal)
//        } else {
//            btn.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
//            btn.setTitleColor(Colors.GRAY_COLOR, forState: .Normal)
//        }
        btn.setTitle("BOOK TAXI", for: UIControlState())
        btn.backgroundColor = Colors.GREEN_COLOR
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = boldFontWithSize(16.0)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.addTarget(self, action: #selector(proceed), for: .touchDown)
        view.addSubview(btn)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(20)][btn(40)]", options: [], metrics: nil, views: ["lbl":lblTitle,"btn":btn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btn]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    func CashToDriverRDBtn()
    {
        lblBottom.isHidden = false
        lblLine1.isHidden = true
        lblLine2.isHidden = false
        self.paymentOptionView.heightConstraint?.constant = 87
        lblBottom.text = "Please pay the cash to driver once the ride completes."
    }
    func PayOnlineRDBtn()
    {
        lblBottom.isHidden = false
        lblLine1.isHidden = true
        lblLine2.isHidden = false
        self.paymentOptionView.heightConstraint?.constant = 87
        lblBottom.text = "Post trip, you would receive an SMS with trip details and payment options."
    }
    func proceed()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Airport Tab",
            AnalyticsParameterContentType:"Submit Button"
            ])
        let searchReqObj = SearchRideRequest()
        searchReqObj.userId = UserInfo.sharedInstance.userID
        searchReqObj.reqDate = txtDate.text
        searchReqObj.reqTime = txtTime.text
        searchReqObj.poolType = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        
        searchReqObj.startLoc = airportRideObj.fromLocation
        searchReqObj.fromLat = airportRideObj.fromLat
        searchReqObj.fromLong = airportRideObj.fromLong
        searchReqObj.endLoc = airportRideObj.toLocation
        searchReqObj.toLat = airportRideObj.toLat
        searchReqObj.toLong = airportRideObj.toLong
        searchReqObj.poolType = self.airportRideObj.poolType == "Any" ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
        searchReqObj.searchType = "recurrent"
        searchReqObj.ridetype = ""
        var selectedDates = [String]()
        selectedDates.append(txtDate.text!)
        if(selectedDates.count > 0){
            searchReqObj.reqCurrentDate = selectedDates.joined(separator: ",")
        }
        
        self.airportRideObj.userId = UserInfo.sharedInstance.userID
        //self.airportRideObj.pool_shared = self.btnPersonal.selected == true ? "0" : "1"
        self.airportRideObj.pool_shared = "0"
        self.airportRideObj.rideDate = self.txtDate.text
        self.airportRideObj.rideTime = self.txtTime.text
        self.airportRideObj.pay_to = self.btnCashToDriver.isSelected == true ? "1" : "2"
        self.airportRideObj.ridecoment = self.txtComments.text
        
        let minDate = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "dd-MMM-yyyy")
        let minTime = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "HH:mm")
        let compareResult = minDate.compare(airportRideObj.rideDate!)
        let compareResultTime = minTime.compare(airportRideObj.rideTime!)
        
        if((self.airportRideObj.fromLocation != nil) && (self.airportRideObj.toLocation != nil))
        {
            if(self.airportRideObj.fromLocation != self.airportRideObj.toLocation){
                if((self.airportRideObj.rideDate != nil) && (self.airportRideObj.rideTime != nil)){
                    if(self.airportRideObj.veh_type != nil) {
                        if(self.btnCashToDriver.isSelected == true || self.btnPayOnlineLater.isSelected == true) {
                            if(compareResult == ComparisonResult.orderedSame && compareResultTime == ComparisonResult.orderedDescending) {
                                AlertController.showToastForError("Sorry, You can book taxi only after 4 hours from current time.")
                            } else {
                                if(self.airportRideObj.pool_shared == "1") {
                                    showIndicator("Searching for similair rides..")
                                    RideRequestor().searchRides(searchReqObj, success: { (success, object) in
                                        hideIndicator()
                                        if(success){
                                            if((object as! AllRidesResponse).pools!.count > 0){
                                                let vc = SearchResultViewController()
                                                vc.hidesBottomBarWhenPushed = true
                                                vc.taxiRide = "1"
                                                vc.dataSource = (object as! AllRidesResponse).pools!
                                                vc.onAddNewBlock = {
                                                    self.createNewRide()
                                                }
                                                //                                                vc.onSearchBlock = {
                                                //                                                    showIndicator("Updating.")
                                                //                                                    RideRequestor().searchRides(searchReqObj, success: { (success, object) in
                                                //                                                        hideIndicator()
                                                //                                                        if(success){
                                                //                                                            if((object as! AllRidesResponse).pools!.count > 0){
                                                //                                                                let vc = SearchResultViewController()
                                                //                                                                vc.hidesBottomBarWhenPushed = true
                                                //                                                                vc.dataSource = (object as! AllRidesResponse).pools!
                                                //                                                                vc.onAddNewBlock = {
                                                //                                                                    self.createNewRide()
                                                //                                                                }
                                                //                                                            }
                                                //                                                        }
                                                //                                                    }) { (error) in
                                                //                                                        hideIndicator()
                                                //                                                    }
                                                //                                                }
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            }
                                            else{
                                                self.createNewRide()
                                            }
                                        }
                                    }) { (error) in
                                        hideIndicator()
                                    }
                                }else {
                                    self.createNewRide()
                                }
                            }
                        } else {
                            AlertController.showAlertFor("Taxi Ride", message: "Please select payment option.")
                        }
                    }
                    else {
                        AlertController.showAlertFor("Taxi Ride", message: "Please select a vehicle type and click save.", okAction: gotoPriceEstimationList)
                        //gotoPriceEstimationList()
                    }
                }
                else{
                    AlertController.showAlertFor("Taxi Ride", message: "Please enter date and time for the Ride.")
                }
            }
            else{
                AlertController.showAlertFor("Taxi Ride", message: "Please choose different locations for source and destination.")
            }
        }
        else{
            AlertController.showAlertFor("Taxi Ride", message: "Please choose location in 'From' and 'To' field from Google Places.")
        }
    }
    
    func createNewRide()
    {
        showIndicator("Creating Taxi Ride..")
        AirportRequestor().createAirportRide(self.airportRideObj, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Create Taxi Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! AirportResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    
                    self.airportRideObj.fromLocation = nil
                    self.lblSource.text = "From (Select exact or nearby google places)"
                    self.airportRideObj.rideDate = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "dd-MMM-yyyy")
                    self.txtDate.text = self.airportRideObj.rideDate
                    self.datePicker.date = Date().addingTimeInterval(14400)
                    self.airportRideObj.rideTime = prettyDateStringFromDate(Date().addingTimeInterval(14400), toFormat: "HH:mm")
                    self.txtTime.text = self.airportRideObj.rideTime
                    self.timePicker.date = Date().addingTimeInterval(14400)
                    //self.btnPersonal.selected = true
                    self.airportRideObj.ridecoment = ""
                    self.txtComments.text = self.airportRideObj.ridecoment
                    self.btnCashToDriver.isSelected = false
                    self.btnPayOnlineLater.isSelected = false
                    self.lblBottom.isHidden = true
                    self.lblLine1.isHidden = false
                    self.lblLine2.isHidden = true
                    self.paymentOptionView.heightConstraint?.constant = 60
                    self.airportRideObj.veh_type = ""
                    self.airportRideObj.main_cost = ""
                    self.airportRideObj.veh_seat = ""
                    self.txtVehicleTypeTitle.isHidden = true
                    self.txtVehicleType.isHidden = true
                    self.txtCostTitle.isHidden = true
                    self.txtCost.isHidden = true
                    self.lblSubTopConstraint?.constant = 5
                    self.priceEstimationView.heightConstraint?.constant = 31
                    
                    RideRequestor().getTaxiDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let vc = TaxiDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.signupFlow = self.signupFlow
                            vc.data = object as? RideDetail
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! AirportResponse).code == "173") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! AirportResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    func gotoPriceEstimationList()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            if(self.airportRideObj.fromLocation != nil && self.airportRideObj.toLocation != nil) {
                let vc = PriceEstimationViewController()
                vc.onCompletionBlock = { (selectionSubscription, genderType) -> Void in
                    if let obj = selectionSubscription as? GetPriceListVehicles{
                        self.lblSubTopConstraint?.constant = 65
                        self.priceEstimationView.heightConstraint?.constant = 91
                        self.priceEstimationView.view?.viewWithTag(501)?.isHidden = false
                        self.priceEstimationView.view?.viewWithTag(503)?.isHidden = false
                        self.priceEstimationView.view?.viewWithTag(401)?.isHidden = false
                        self.priceEstimationView.view?.viewWithTag(403)?.isHidden = false
                        
                        (self.priceEstimationView.view?.viewWithTag(501) as? RATextField)?.text = obj.vehicleType
                        self.airportRideObj.veh_type = obj.vehicleTypeId!
                        self.airportRideObj.veh_seat = obj.seatCapacity!
                        self.airportRideObj.main_cost = "\(obj.priceMain!)"
                        (self.priceEstimationView.view?.viewWithTag(503) as? RATextField)?.text = "\(RUPEE_SYMBOL) \(obj.pricePrivate!)"
                        self.airportRideObj.final_cost = "\(obj.pricePrivate!)"
                        self.airportRideObj.poolType = "Any"
                        
                        /*if(self.btnPersonal.selected == true) {
                         (self.priceEstimationView.view?.viewWithTag(503) as? RATextField)?.text = "\(RUPEE_SYMBOL) \(obj.pricePrivate!)"
                         self.airportRideObj.final_cost = "\(obj.pricePrivate!)"
                         self.airportRideObj.poolType = "Any"
                         } else {
                         if(genderType == "f"){
                         (self.priceEstimationView.view?.viewWithTag(503) as? RATextField)?.text = "\(RUPEE_SYMBOL) \(obj.priceFemale!)"
                         self.airportRideObj.final_cost = "\(obj.priceFemale!)"
                         self.airportRideObj.poolType = "Only Female"
                         } else if(genderType == "m"){
                         (self.priceEstimationView.view?.viewWithTag(503) as? RATextField)?.text = "\(RUPEE_SYMBOL) \(obj.priceMale!)"
                         self.airportRideObj.final_cost = "\(obj.priceMale!)"
                         self.airportRideObj.poolType = "Only Male"
                         } else {
                         (self.priceEstimationView.view?.viewWithTag(503) as? RATextField)?.text = "\(RUPEE_SYMBOL) \(obj.priceAny!)"
                         self.airportRideObj.final_cost = "\(obj.priceAny!)"
                         self.airportRideObj.poolType = "Any"
                         }
                         }*/
                        self.airportRideObj.base_cost = obj.basePrice
                    }
                }
                vc.source = self.airportRideObj.fromLocation
                vc.destination = self.airportRideObj.toLocation
                vc.distance = self.airportRideObj.distance
                vc.duration = self.airportRideObj.duration
                vc.date = self.airportRideObj.rideDate
                vc.time = self.airportRideObj.rideTime
                //vc.taxiType = self.btnPersonal.selected == true ? "P" : "S"
                vc.taxiType = "P"
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                AlertController.showAlertFor("Taxi Ride", message: "Please Provide From and To location to check price estimation.")
            }
        }
    }
    
    func FlipLocations()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Airport Tab",
            AnalyticsParameterContentType:"Booking Airport taxi"
            ])
        if(toggleState == 1) {
            if(self.airportRideObj.fromLocation != nil)
            {
                lblDest.text = self.airportRideObj.fromLocation
                self.airportRideObj.toLocation = self.airportRideObj.fromLocation
                self.airportRideObj.toLat = self.airportRideObj.fromLat
                self.airportRideObj.toLong = self.airportRideObj.fromLong
            } else {
                lblDest.text = "To (Select exact or nearby google places)"
                self.airportRideObj.toLocation = nil
            }
            lblDest.isUserInteractionEnabled = true
            lblSource.isUserInteractionEnabled = false
            iconDestArrow.isHidden = false
            iconSourceArrow.isHidden = true
            lblSource.text = self.defaultLocationData.locName
            self.airportRideObj.fromLocation = self.defaultLocationData.locName
            self.airportRideObj.fromLat = self.defaultLocationData.locLat
            self.airportRideObj.fromLong = self.defaultLocationData.locLong
            toggleState = 2
        } else {
            if(self.airportRideObj.toLocation != nil)
            {
                lblSource.text = self.airportRideObj.toLocation
                self.airportRideObj.fromLocation = self.airportRideObj.toLocation
                self.airportRideObj.fromLat = self.airportRideObj.toLat
                self.airportRideObj.fromLong = self.airportRideObj.toLong
            } else {
                lblSource.text = "From (Select exact or nearby google places)"
                self.airportRideObj.fromLocation = nil
            }
            lblSource.isUserInteractionEnabled = true
            lblDest.isUserInteractionEnabled = false
            iconSourceArrow.isHidden = false
            iconDestArrow.isHidden = true
            lblDest.text = self.defaultLocationData.locName
            self.airportRideObj.toLocation = self.defaultLocationData.locName
            self.airportRideObj.toLat = self.defaultLocationData.locLat
            self.airportRideObj.toLong = self.defaultLocationData.locLong
            toggleState = 1
        }
    }
    
    func labelGesturePressed(_ sender:AnyObject)  {
        let vc = StaticPagesViewController()
        vc.staticPage = "blr-airport-tc"
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getDefaultLocation()
    {
        let reqObj = GetDefaultLocationRequest()
        reqObj.loc_id = "1281"
        reqObj.offer_id = "bangalore_airport"
        showIndicator("Getting Airport Location..")
        AirportRequestor().getDefaultLocation(reqObj, success:{(success, object) in
            if (object as! GetDefaultLocationResponse).code == "1505" {
                self.defaultLocationData = ((object as! GetDefaultLocationResponse).dataObj!.first)!
                self.lblDest.text = self.defaultLocationData.locName
                self.airportRideObj.toLocation = self.defaultLocationData.locName
                self.airportRideObj.toLat = self.defaultLocationData.locLat
                self.airportRideObj.toLong = self.defaultLocationData.locLong
            }
            hideIndicator()
        }) { (error) in
            hideIndicator()
        }
    }
    
}
