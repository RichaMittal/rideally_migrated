//
//  VerifyMobileNumberRequestor.swift
//  rideally
//
//  Created by Raghunathan on 8/16/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

/*
 {"code":"251","message":"Verification Successful.","data":[]}
 Note: Mobile Verification is Mandatory and without it, user should not be able to login to HomePage.
 */


/* VerifyMobilenumber request and response Part*/

class VerifyMobileNumberRequest: BaseModel {
    var userID: String?
    var verificationCode: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userID <- map["user_id"]
        verificationCode <- map["verification_key"]
    }
}

class VerifyMobileNumberResponse: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
        
    }
}



/* ResendCode request and response Part*/

/*
 Response:
 {"code":"257","message":"Resend Verification Code Successful.","data":[]}
 {"code":"258","message":"Resending Not Completed. Please Try After Few Minutes.","data":[]}
 {"code":"259","message":"Incorrect Mobile Number.","data":[]}
*/

class ResendCodeNumberRequest: BaseModel {
    var userID: String?
    var mobileNumber: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userID <- map["user_id"]
        mobileNumber <- map["mnumber"]
    }
}

class ResendCodeNumberResponse: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
        
    }
}


class VerifyMobileNumberRequestor: BaseRequestor {

    func getVerifyNumber(_ requestObj: VerifyMobileNumberRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<VerifyMobileNumberRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.VERIFYMOBILE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: VerifyMobileNumberResponse = Mapper<VerifyMobileNumberResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
             
            })
        { (error) in
            failure(error)
        }
    }
    
    func resendCodeNumber(_ requestObj: ResendCodeNumberRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<ResendCodeNumberRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.RESENDCODE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: ResendCodeNumberResponse = Mapper<ResendCodeNumberResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }

    
}
