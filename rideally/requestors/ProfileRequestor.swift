//
//  ProfileRequestor.swift
//  rideally
//
//  Created by Sarav on 11/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper


/*
 user_id
 user_first_name
 user_last_name
 user_email
 user_dob
 user_gender => F/M
 user_mobile
 user_sharenumber => 1/0
 user_city
 address
 govtid_type => Passport/Aadhar/Driving_License/Voter_ID/Pancard
 view_f => Front Govt Id
 view_b => Back Govt Id
 view_o => Office/Workplace Id
 view_p => Proifle pic
 */


class EditProfileRequest: BaseModel {
    
    var userID: String?
    var userFirstName: String?
    var userLastName: String?
    var userEmail: String?
    var userDOB: String?
    var userGender: String?
    var userMobile: String?
    var userShareNumber: String?
    var userCity: String?
    var userAddress: String?
    var userGovtIDType: String?
    var userView_f: String?
    var userView_b: String?
    var userView_o: String?
    var userView_p: String?

    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        userFirstName <- map["user_first_name"]
        userLastName <- map["user_last_name"]
        userEmail <- map["user_email"]
        userDOB <- map["user_dob"]
        userGender <- map["user_gender"]
        userMobile <- map["user_mobile"]
        userShareNumber <- map["user_sharenumber"]
        userCity <- map["user_city"]
        userAddress <- map["address"]
        userGovtIDType <- map["govtid_type"]
        userView_f <- map["view_f"]
        userView_f <- map["view_b"]
        userView_f <- map["view_o"]
        userView_f <- map["view_p"]
    }
}


class EditProfileResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
    }
}

class SetDefaultVehicleRequest: BaseModel {
    
    var userID: String?
    var vehicleSeqID: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["userId"]
        vehicleSeqID <- map["vehicleSeqId"]
    }
}

class SetDefaultVehicleResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
    }
}


class UpdateProfilePictureURLRequest: BaseModel {
    
    var userID: String?
    var profilePicURL: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        profilePicURL <- map["profilepic_url"]
    }
}

class UpdateProfilePictureURLResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}


class AddVehicleRequest: BaseModel {
    
    var userID: String?
    var vehicleType: String?
    var vehicleModel: String?
    var vehicleRegNo: String?
    var vehicleCapacity: String?
    var vehicleKind: String?
    var vehicleOwnerName: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        vehicleType <- map["vehicle_type"]
        vehicleModel <- map["vehicle_model"]
        vehicleRegNo <- map["vehicle_regno"]
        vehicleCapacity <- map["vehicle_capacity"]
        vehicleKind <- map["vehicle_kind"]
        vehicleOwnerName <- map["vehicle_owner_name"]
    }
}


class AddVehicleResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}



class UpdateVehicleRequest: BaseModel {
    
    var userID: String?
    var vehicleSeqID: String?
    var vehicleType: String?
    var vehicleModel: String?
    var vehicleRegNo: String?
    var vehicleCapacity: String?
    var vehicleKind: String?
    var vehicleOwnerName: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        vehicleSeqID <- map["vehicle_seq_id"]
        vehicleType <- map["vehicle_type"]
        vehicleModel <- map["vehicle_model"]
        vehicleRegNo <- map["vehicle_regno"]
        vehicleCapacity <- map["vehicle_capacity"]
        vehicleKind <- map["vehicle_kind"]
        vehicleOwnerName <- map["vehicle_owner_name"]
    }
}


class UpdateVehicleResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class AddVehicleInsuranceRequest: BaseModel {
    
    var userID: String?
    var vehicleType: String?
    var vehicleModel: String?
    var vehicleRegNo: String?
    var vehicleCapacity: String?
    var vehicleKind: String?
    var vehicleOwnerName: String?
    var insurerName: String?
    var insuredCmpnyName: String?
    var insuranceId: String?
    var insuranceExpiryDate: String?
    var insUrl: String?
    var rcUrl: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        vehicleType <- map["vehicle_type"]
        vehicleModel <- map["vehicle_model"]
        vehicleRegNo <- map["vehicle_regno"]
        vehicleCapacity <- map["vehicle_capacity"]
        vehicleKind <- map["vehicle_kind"]
        vehicleOwnerName <- map["vehicle_owner_name"]
        insurerName <- map["insurer_name"]
        insuredCmpnyName <- map["insured_company_name"]
        insuranceId <- map["insurance_id"]
        insuranceExpiryDate <- map["insurance_expiry_date"]
        insUrl <- map["insurance_url"]
        rcUrl <- map["rc_url"]
    }
}

class UpdateVehicleInsuranceRequest: BaseModel {
    
    var userID: String?
    var vehicleSeqID: String?
    var vehicleType: String?
    var vehicleModel: String?
    var vehicleRegNo: String?
    var vehicleCapacity: String?
    var vehicleKind: String?
    var vehicleOwnerName: String?
    var insurerName: String?
    var insuredCmpnyName: String?
    var insuranceId: String?
    var insuranceExpiryDate: String?
    var insUrl: String?
    var rcUrl: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        vehicleSeqID <- map["vehicle_seq_id"]
        vehicleType <- map["vehicle_type"]
        vehicleModel <- map["vehicle_model"]
        vehicleRegNo <- map["vehicle_regno"]
        vehicleCapacity <- map["vehicle_capacity"]
        vehicleKind <- map["vehicle_kind"]
        vehicleOwnerName <- map["vehicle_owner_name"]
        insurerName <- map["insurer_name"]
        insuredCmpnyName <- map["insured_company_name"]
        insuranceId <- map["insurance_id"]
        insuranceExpiryDate <- map["insurance_expiry_date"]
        insUrl <- map["insurance_url"]
        rcUrl <- map["rc_url"]
    }
}

class DeleteSelecteVehicleRequest: BaseModel {
    
    var seqID: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        seqID <- map["vehicleSeqId"]
    }
}

class DeleteSelecteVehicleResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class ResendEmailRequest:  BaseModel{
    
    var userID : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userID <- map["user_id"]
    }
}

class ResendEmailResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class GetALLVehicleListRequest: BaseModel {
    
    var userID: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userID <- map["user_id"]
    }
}

class GetALLVehicleListResponse: BaseModel {
    
    var dataObj: [VehicleData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        dataObj <- map["data"]
    }
}

class VehicleData: BaseModel {
    
    var vehicleSeqId: String?
    var vehicleType: String?
    var vehicleModel: String?
    var RegNo : String?
    var capacity : String?
    var isDefault: String?
    var vehicleKind: String?
    var vehicleOwnerName: String?
    var insurerName : String?
    var insuredCmpnyName: String?
    var insuranceId: String?
    var insuranceExpiryDate: String?
    var insUploadStatus: String?
    var insUrl: String?
    var rcUploadStatus: String?
    var rcUrl: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        vehicleSeqId <- map["vehicleSeqId"]
        vehicleType <- map["vehicleType"]
        vehicleModel <- map["vehicleModel"]
        RegNo <- map["RegNo"]
        capacity <- map["capacity"]
        isDefault <- map["is_default"]
        vehicleKind <- map["vehicle_kind"]
        vehicleOwnerName <- map["vehicle_owner_name"]
        insurerName <- map["insurer_name"]
        insuredCmpnyName <- map["insured_company_name"]
        insuranceId <- map["insurance_id"]
        insuranceExpiryDate <- map["insurance_expiry_date"]
        insUploadStatus <- map["insurance_upload_status"]
        insUrl <- map["insurance_url"]
        rcUploadStatus <- map["rc_upload_status"]
        rcUrl <- map["rc_url"]
    }
}

class ProfileDetailsRequest: BaseModel {
    
    var userID: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userID <- map["user_id"]
    }
}

class SetWorkLocationRequest: BaseModel
{
    var user_id: String?
    var wp_loc : String?
    var wp_loc_lat: String?
    var wp_loc_long: String?

    override func mapping(map: Map) {
        super.mapping(map: map)

        user_id <- map["user_id"]
        wp_loc <- map["wp_loc"]
        wp_loc_lat <- map["wp_loc_lat"]
        wp_loc_long <- map["wp_loc_long"]
    }
}

class SetWorkLocationResponse: BaseModel
{
    var dataObj: [SetWorkLocationResponseData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        dataObj <- map["data"]
    }
    
}

class SetWorkLocationResponseData: BaseModel
{
    var user_id: String?
    //var address : String?
    var wp_loc_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        //address <- map["address"]
        wp_loc_id <- map["wp_loc_id"]
    }
}

class SetHomeLocationResponse: BaseModel
{
    var dataObj: [SetHomeLocationResponseData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        dataObj <- map["data"]
    }

}

class SetHomeLocationResponseData: BaseModel
{
    
    var user_id: String?
    var address : String?
    var home_loc_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        address <- map["address"]
        home_loc_id <- map["home_loc_id"]
    }
}

class SetHomeLocationRequest: BaseModel
{
    var user_id: String?
    var home_loc : String?
    var home_loc_lat: String?
    var home_loc_long: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        home_loc <- map["home_loc"]
        home_loc_lat <- map["home_loc_lat"]
        home_loc_long <- map["home_loc_long"]
    }
}

class ProfileDetailsResponse: BaseModel
{
    var dataObj: [ProfileData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class ProfileData: BaseModel {
    
    var user_id: String?
    var unique_id: String?
    var fb_id: String?
    var email : String?
    var device_id : String?
    var user_state : String?
    var pool_search_data : String?
    var created_time : String?
    var updated_time: String?
    var pool_scope: String?
    var first_name: String?
    var last_name: String?
    var full_name: String?
    var facebook_url: String?
    var email_id: String?
    var secondary_email: String?
    var secondary_email_status: String?
    var dob: String?
    var age: String?
    var mobile_number: String?
    var share_mobile_number: String?
    var gender : String?
    var address : String?
    var marital_status: String?
    var city_name: String?
    var pin: String?
    var linkedin_url: String?
    var current_company: String?
    var current_designation: String?
    var govt_id: String?
    var govtidf_url: String?
    var govtidb_url: String?
    var officeid_url: String?
    var govt_fid_status : String?
    var govt_bid_status: String?
    var profilepic_url: String?
    var profilepic_status: String?
    var office_id_status: String?
    var home_address: String?
    var invited_user_status: String?
    var vehicle_count: String?
    var loginTime: String?
    var route_request: String?
    var user_groups: String?
    var hideAnywhere: String?
    var userDetail : ProfileUserDetail?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        unique_id <- map["unique_id"]
        fb_id <- map["fb_id"]
        email <- map["email"]
        device_id <- map["device_id"]
        user_state <- map["user_state"]
        pool_search_data <- map["pool_search_data"]
        created_time <- map["created_time"]
        updated_time <- map["updated_time"]
        pool_scope <- map["pool_scope"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        full_name <- map["full_name"]
        facebook_url <- map["facebook_url"]
        email_id <- map["email_id"]
        secondary_email <- map["secondary_email"]
        secondary_email_status <- map["secondary_email_status"]
        dob <- map["dob"]
        age <- map["age"]
        mobile_number <- map["mobile_number"]
        share_mobile_number <- map["share_mobile_number"]
        gender <- map["gender"]
        address <- map["address"]
        marital_status <- map["marital_status"]
        city_name <- map["city_name"]
        pin <- map["pin"]
        linkedin_url <- map["linkedin_url"]
        current_company <- map["current_company"]
        current_designation <- map["current_designation"]
        govt_id <- map["govt_id"]
        govtidf_url <- map["govtidf_url"]
        govtidb_url <- map["govtidb_url"]
        officeid_url <- map["officeid_url"]
        govt_fid_status <- map["govt_fid_status"]
        govt_bid_status <- map["govt_bid_status"]
        profilepic_url <- map["profilepic_url"]
        profilepic_status <- map["profilepic_status"]
        office_id_status <- map["office_id_status"]
        home_address <- map["home_address"]
        invited_user_status <- map["invited_user_status"]
        vehicle_count <- map["vehicle_count"]
        loginTime <- map["loginTime"]
        route_request <- map["route_request"]
        user_groups <- map["user_groups"]
        hideAnywhere <- map["hide_anywhere"]
        userDetail <- map["user_Detail"]
    }
}

class ProfileUserDetail: BaseModel {
    var unreadNotification : String?
    var allRides: String?
    var allWorkplaces: String?
    var allRoutes: String?
    var offered: String?
    var needed: String?
    var joined: String?
    var booked: String?
    var sent: String?
    var received: String?
    var mygroup: String?
    var myroute: String?
    var myevent: String?
    var mytrip: String?
    var myWorkplace: String?
    var myPendingWorkplace: String?
    var mygroup_ride: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        unreadNotification <- map["unread_notification"]
        allRides <- map ["allrides"]
        allWorkplaces <- map ["allworkplace"]
        allRoutes <- map ["allroute"]
        offered <- map["offered"]
        needed <- map["needed"]
        joined <- map["joined"]
        booked <- map["booked"]
        sent <- map["sent"]
        received <- map["received"]
        mygroup <- map["mygroup"]
        myroute <- map["myroute"]
        myevent <- map["myevent"]
        mytrip <- map["mytrip"]
        myWorkplace <- map ["myworkplace"]
        myPendingWorkplace <- map ["mypendingworkplace"]
        mygroup_ride <- map["mygroup_ride"]
    }
}

class ProfileRequestor: BaseRequestor {

    func profileDetailRequest(_ requestObj: ProfileDetailsRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<ProfileDetailsRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.GETPROFILEDETAIL, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: ProfileDetailsResponse = Mapper<ProfileDetailsResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
        
    }

    
    func addVehicle(_ requestObj: AddVehicleRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<AddVehicleRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.ADD_VEHICLE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: AddVehicleResponse = Mapper<AddVehicleResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }

    }
    
    
    func getAddVehicleListData(_ requestObj: GetALLVehicleListRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<GetALLVehicleListRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.ALL_VEHICLE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: GetALLVehicleListResponse = Mapper<GetALLVehicleListResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
        
    }
    
    func deleteVehicleListData(_ requestObj: DeleteSelecteVehicleRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<DeleteSelecteVehicleRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.DELETE_VEHICLE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: DeleteSelecteVehicleResponse = Mapper<DeleteSelecteVehicleResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func updateVehicleListData(_ requestObj: UpdateVehicleRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<UpdateVehicleRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.UPDATE_VEHICLE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: UpdateVehicleResponse = Mapper<UpdateVehicleResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func sendVerifyEmailRequest(_ requestObj: ResendEmailRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<ResendEmailRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.RESENDEMAIL, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: ResendEmailResponse = Mapper<ResendEmailResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }
    

    func sendsetWorkLocationRequest(_ requestObj: SetWorkLocationRequest, success: @escaping NetworkCompletionHandler, failure: NetworkFailureHandler)
    {
        let parameters = Mapper<SetWorkLocationRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.SETWORKLOC, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: SetWorkLocationResponse = Mapper<SetWorkLocationResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            
        }
    }

    func sendsetHomeLocationRequest(_ requestObj: SetHomeLocationRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<SetHomeLocationRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.SETHOMELOC, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: SetHomeLocationResponse = Mapper<SetHomeLocationResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }

    func sendEditUserProfileRequest(_ requestObj: EditProfileRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<EditProfileRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.EDITUSERPROFILE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: EditProfileResponse = Mapper<EditProfileResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }

    func sendSetDefaultVehicleRequest(_ requestObj: SetDefaultVehicleRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<SetDefaultVehicleRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.SET_DEFAULT_VEHICLE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: SetDefaultVehicleResponse = Mapper<SetDefaultVehicleResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }

    func sendUpdateProfileImageURLRequest(_ requestObj: UpdateProfilePictureURLRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<UpdateProfilePictureURLRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.UPADATEPROFILEIMAGEURL, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: UpdateProfilePictureURLResponse = Mapper<UpdateProfilePictureURLResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func addVehicleInsurance(_ requestObj: AddVehicleInsuranceRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<AddVehicleInsuranceRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.ADD_VEHICLE_INSURANCE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: AddVehicleResponse = Mapper<AddVehicleResponse>().map(JSON: result as! [String : Any])!
            if(response.code == "204") {
                success(true, response)
            } else {
                success(false, response)
            }
            
        }) { (error) in
            failure(error)
        }
    }

    func updateVehicleInsurance(_ requestObj: UpdateVehicleInsuranceRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<UpdateVehicleInsuranceRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.UPDATE_VEHICLE_INSURANCE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: UpdateVehicleResponse = Mapper<UpdateVehicleResponse>().map(JSON: result as! [String : Any])!
            if(response.code == "227") {
                success(true, response)
            } else {
                success(false, response)
            }
            
        }) { (error) in
            failure(error)
        }
    }
}
