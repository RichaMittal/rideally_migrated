//
//  NeedRideRequestor.swift
//  rideally
//
//  Created by Sarav on 11/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper
import Foundation

class NeedRideRequest: BaseModel {
    var userId: String?
    var rideDate: String?
    var rideTime: String?
    var fromLocation: String?
    var fromLat: String?
    var fromLong: String?
    var toLocation: String?
    var toLat: String?
    var toLong: String?
    var poolType: String?
    
    var ridetype = "N"
    var pool_shared = "1" //hardcode to 1
    var pool_scope = "" //blank
    var pool_category = "9" //hardcode to 9
    var vehregno = "" //blank
    var ridemessage = "" //comments
    var pool_groupids = "" //blank
    var pool_taxi_booking = "0" //hardcode to 0
    var veh_capacity = "" //blank
    var distance = "" //blank
    var costType = "PK"
    var cost = "0"
    var duration = ""
    var pool_repeat_days = ""
    var via: String?
    var waypoints: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["user_id"]
        rideDate <- map["poolstartdate"]
        rideTime <- map["poolstarttime"]
        fromLocation <- map["from_location"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_lon"]
        toLocation <- map["to_location"]
        toLat <- map["to_lat"]
        toLong <- map["to_lon"]
        poolType <- map["pooltype"]
        
        ridetype <- map["ridetype"]
        pool_shared <- map["pool_shared"]
        pool_scope <- map["pool_scope"]
        pool_category <- map["pool_category"]
        vehregno <- map["vehregno"]
        ridemessage <- map["ridemessage"]
        pool_groupids <- map["pool_groupids"]
        pool_taxi_booking <- map["pool_taxi_booking"]
        veh_capacity <- map["veh_capacity"]
        distance <- map["distance"]
        costType <- map["cost_type"]
        
        cost <- map["cost"]
        duration <- map["duration"]
        pool_repeat_days <- map["pool_repeat_days"]
        via <- map["via"]
        waypoints <- map["waypoints"]
    }
}

class NeedRideResponse : BaseModel
{
    var data: [NeedRideDataResponse]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}

class NeedRideDataResponse : BaseModel
{
    var poolID: String?
    var distance: String?
    var duration: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        poolID <- map["pool_id"]
        distance <- map["distance"]
        duration <- map["duration"]
    }
}

class NeedRideRequestor: BaseRequestor {
    
    func createNeedaRide(_ requestObj: NeedRideRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<NeedRideRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.CREATE_NEED_A_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: NeedRideResponse = Mapper<NeedRideResponse>().map(JSON: result as! [String : Any])!
            if(response.data != nil){
                success(true, response)
            }
            else{
                success(false, response)
            }
        }) { (error) in
            failure(error)
        }
    }
}
