//
//  StaticPageRequestor.swift
//  rideally
//
//  Created by Raghunathan on 8/29/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class StaticPageRequest: BaseModel {
    var page_key: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        page_key <- map["page_key"]
    }
}

class StaticPageResponse: BaseModel
{
    var dataObj: [StaticPageData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class StaticPageData: BaseModel {
    var key : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        key <- map["content"]
    }
}

class getAppVersionResponse: BaseModel
{
    var dataObj: [getAppVersionData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class getAppVersionData: BaseModel {
    var versionName : String?
    var versionCode : String?
    var isUpdateMandatory : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        versionName <- map["version_name"]
        versionCode <- map["version_code"]
        isUpdateMandatory <- map["is_update_mandatory"]
    }
}

class StaticPageRequestor: BaseRequestor {

    func sendStaticPageRequest(_ requestObj: StaticPageRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<StaticPageRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.STATICPAGE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: StaticPageResponse = Mapper<StaticPageResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }

    func getAppVersionRequest(deviceType: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let params = ["device_type": deviceType]
        makePOSTRequestWithparameters(URLS.GET_APP_VERSION, parameters: params as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: getAppVersionResponse = Mapper<getAppVersionResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
        })
        { (error) in
            failure(error)
        }
    }
}
