//
//  NotificationReuestor.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/21/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationRequest: BaseModel {
    var user_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        user_id <- map["user_id"]
    }
}

class NotificationResponse: BaseModel
{
    var dataObj: [NotificationData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class NotificationData: BaseModel {
    var notificationMsg : String?
    var notificationId : String?
    var notificationStatus : String?
    var notificationType : String?
    var notifType : String?
    var poolId : String?
    var groupId : String?
    var groupName : String?
    var routeId : String?
    var sourceLoc : String?
    var destinationLoc : String?
    var wpLoc : String?
    var createdTime : String?
    var userName : String?
    var senderName : String?
    var tieUpStatus : String?
    var routeSource : String?
    var routeDest : String?
    var senderId : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        notificationId <- map["notification_id"]
        notificationMsg <- map["notification_message"]
        notificationStatus <- map["notification_status"]
        notificationType <- map["notification_type"]
        notifType <- map["notif_type"]
        poolId <- map["pool_id"]
        groupId <- map["group_id"]
        groupName <- map["group_name"]
        routeId <- map["croute_id"]
        sourceLoc <- map["source"]
        destinationLoc <- map["destination"]
        wpLoc <- map["wp_loc"]
        createdTime <- map["created_time"]
        userName <- map["user_name"]
        senderName <- map["sender_name"]
        tieUpStatus <- map["tieup_status"]
        routeSource <- map["route_src"]
        routeDest <- map["route_dest"]
        senderId <- map["sender_id"]
        //notification_id <- map["notification_id"]
        //ra_tm_rating <- map["ra_tm_rating"]
    }
}

class NotificationRequestor: BaseRequestor {
    
    func sendNotificationRequest(_ requestObj: NotificationRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<NotificationRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.GET_NOTIFICATIONS, parameters: parameters as [String : AnyObject], encoding: .default, success: { (result) in
            
            let response: NotificationResponse = Mapper<NotificationResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    func sendUpdateNotificationRequest(_ requestObj: UpdateNotificationRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<UpdateNotificationRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.UPDATE_NOTIFICATION, parameters: parameters as [String : AnyObject], encoding: .default, success: { (result) in
            
            let response: UpdateNotificationResponse = Mapper<UpdateNotificationResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    func sendDeleteNotificationRequest(_ requestObj: DeleteNotificationRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<DeleteNotificationRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.DELETE_NOTIFICATION, parameters: parameters as [String : AnyObject], encoding: .default, success: { (result) in
            
            let response: DeleteNotificationResponse = Mapper<DeleteNotificationResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
}

class UpdateNotificationRequest: BaseModel {
    var user_id: String?
    var notification_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        user_id <- map["user_id"]
        notification_id <- map["notification_id"]
    }
}

class UpdateNotificationResponse: BaseModel
{
    var dataObj: [UpdateNotificationData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class UpdateNotificationData: BaseModel {
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class DeleteNotificationRequest: BaseModel {
    var notification_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        notification_id <- map["notification_id"]
    }
}

class DeleteNotificationResponse: BaseModel
{
    var dataObj: [DeleteNotificationData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class DeleteNotificationData: BaseModel {
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

