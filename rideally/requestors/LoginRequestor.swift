//
//  LoginRequestor.swift
//  rideally
//
//  Created by Raghunathan on 8/9/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper
import Crashlytics

class LoginRequest: BaseModel {
    var email: String?
    var password: String?
    var device_id: String?
    var device_type: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        email <- map["email"]
        password <- map["password"]
        device_id <- map["device_id"]
        device_type <- map["device_type"]
    }
}

class LoginResponse: BaseModel
{
    var dataObj: [LoginData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        dataObj <- map["data"]
    }
}

class LoginData: BaseModel {
    
    var user_id: String?
    var unique_id: String?
    var fb_id: String?
    var email : String?
    var device_id : String?
    var user_state : String?
    var pool_search_data : String?
    var created_time : String?
    var updated_time: String?
    var pool_scope: String?
    var first_name: String?
    var last_name: String?
    var full_name: String?
    var facebook_url: String?
    var email_id: String?
    var secondary_email: String?
    var secondary_email_status: String?
    var dob: String?
    var age: String?
    var mobile_number: String?
    var share_mobile_number: String?
    var gender : String?
    var address : String?
    var marital_status: String?
    var city_name: String?
    var pin: String?
    var linkedin_url: String?
    var current_company: String?
    var current_designation: String?
    var govt_id: String?
    var govtidf_url: String?
    var govtidb_url: String?
    var officeid_url: String?
    var govt_fid_status : String?
    var govt_bid_status: String?
    var profilepic_url: String?
    var profilepic_status: String?
    var office_id_status: String?
    var home_address: String?
    var home_lat: String?
    var home_long: String?
    var work_address: String?
    var work_lat: String?
    var work_long: String?
    var invited_user_status: String?
    var vehicle_count: String?
    var loginTime: String?
    var route_request: String?
    var user_groups: String?
    var isOfficialEmail: String?
    var hideAnywhere: String?
    var  default_Vehicle_capacity:String?
    var  default_Vehicle_model:String?
    var  default_Vehicle_seqID:String?
    var  default_Vehicle_type:String?
    var  default_Vehicle_regNo:String?
    var userDetail : LoginUserDetail?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        unique_id <- map["unique_id"]
        fb_id <- map["fb_id"]
        email <- map["email"]
        device_id <- map["device_id"]
        user_state <- map["user_state"]
        pool_search_data <- map["pool_search_data"]
        created_time <- map["created_time"]
        updated_time <- map["updated_time"]
        pool_scope <- map["pool_scope"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        full_name <- map["full_name"]
        facebook_url <- map["facebook_url"]
        email_id <- map["email_id"]
        secondary_email <- map["secondary_email"]
        secondary_email_status <- map["secondary_email_status"]
        dob <- map["dob"]
        age <- map["age"]
        mobile_number <- map["mobile_number"]
        share_mobile_number <- map["share_mobile_number"]
        gender <- map["gender"]
        address <- map["address"]
        marital_status <- map["marital_status"]
        city_name <- map["city_name"]
        pin <- map["pin"]
        linkedin_url <- map["linkedin_url"]
        current_company <- map["current_company"]
        current_designation <- map["current_designation"]
        govt_id <- map["govt_id"]
        govtidf_url <- map["govtidf_url"]
        govtidb_url <- map["govtidb_url"]
        officeid_url <- map["officeid_url"]
        govt_fid_status <- map["govt_fid_status"]
        govt_bid_status <- map["govt_bid_status"]
        profilepic_url <- map["profilepic_url"]
        profilepic_status <- map["profilepic_status"]
        office_id_status <- map["office_id_status"]
        home_address <- map["home_address"]
        home_lat <- map["latitude"]
        home_long <- map["longitude"]
        work_address <- map["wp_address"]
        work_lat <- map["wp_latitude"]
        work_long <- map["wp_longitude"]
        invited_user_status <- map["invited_user_status"]
        vehicle_count <- map["vehicle_count"]
        loginTime <- map["loginTime"]
        route_request <- map["route_request"]
        user_groups <- map["user_groups"]
        isOfficialEmail <- map["is_official_email"]
        hideAnywhere <- map["hide_anywhere"]
        default_Vehicle_capacity <- map["default_Vehicle_capacity"]
        default_Vehicle_model <- map["default_Vehicle_model"]
        default_Vehicle_seqID <- map["default_Vehicle_seq_id"]
        default_Vehicle_type <- map["default_Vehicle_type"]
        default_Vehicle_regNo <- map["default_Vehicle_vehregno"]
        userDetail <- map["user_Detail"]
    }
}

class LoginUserDetail: BaseModel {
    
    var allRides: String?
    var allWorkplaces: String?
    var allRoutes: String?
    var offered: String?
    var needed: String?
    var joined: String?
    var booked: String?
    var sent: String?
    var received: String?
    var mygroup: String?
    var myroute: String?
    var myevent: String?
    var mytrip: String?
    var myWorkplace: String?
    var myPendingWorkplace: String?
    var mygroup_ride: String?
    var notification_received: String?
    var pointsDetail : PointsDetail?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        allRides <- map ["allrides"]
        allWorkplaces <- map ["allworkplace"]
        allRoutes <- map ["allroute"]
        offered <- map["offered"]
        needed <- map["needed"]
        joined <- map["joined"]
        booked <- map["booked"]
        sent <- map["sent"]
        received <- map["received"]
        mygroup <- map["mygroup"]
        myroute <- map["myroute"]
        myevent <- map["myevent"]
        mytrip <- map["mytrip"]
        myWorkplace <- map ["myworkplace"]
        myPendingWorkplace <- map ["mypendingworkplace"]
        mygroup_ride <- map["mygroup_ride"]
        notification_received <- map["notification_received"]
        pointsDetail <- map["points_information"]
    }
}

class PointsDetail: BaseModel
{
    var maximumallowedpoints: String?
    var pointsavailable: Int?
    var pointsearned: String?
    var pointsused: String?
    var bonusPoints: Int?
    var redeemablePoints: Int?
    var bonusPts: Int?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        maximumallowedpoints <- map["max_allowed_points"]
        pointsavailable <- map["points_available"]
        pointsearned <- map["points_earned"]
        pointsused <- map["points_used"]
        bonusPoints <- map["points_as_bonus"]
        redeemablePoints <- map["points_redeemable"]
        bonusPts <- map["points_bonus"]
    }
}

class FBLoginRequest: BaseModel {
    
    var device_id: String?
    var id: String?
    var firstName : String?
    var lastName : String?
    var link : String?
    var email: String?
    var gender: String?
    var birthday: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        device_id <- map["device_id"]
        id <- map["id"]
        firstName <- map["first_name"]
        lastName <- map["last_name"]
        link <- map["link"]
        email <- map["email"]
        gender <- map["gender"]
    }
}


class FBLoginResponse: BaseModel {
    
    var FBdataObj: [FBLoginData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        FBdataObj <- map["data"]
    }
}



class FBLoginData: BaseModel {
    
    var user_id: String?
    var unique_id: String?
    var fb_id: String?
    var email : String?
    var device_id : String?
    var user_state : String?
    var pool_search_data : String?
    var created_time : String?
    var updated_time: String?
    var pool_scope: String?
    var first_name: String?
    var last_name: String?
    var full_name: String?
    var facebook_url: String?
    var email_id: String?
    var secondary_email: String?
    var secondary_email_status: String?
    var dob: String?
    var age: String?
    var mobile_number: String?
    var share_mobile_number: String?
    var gender : String?
    var address : String?
    var marital_status: String?
    var city_name: String?
    var pin: String?
    var linkedin_url: String?
    var current_company: String?
    var current_designation: String?
    var govt_id: String?
    var govtidf_url: String?
    var govtidb_url: String?
    var officeid_url: String?
    var govt_fid_status : String?
    var govt_bid_status: String?
    var profilepic_url: String?
    var profilepic_status: String?
    var office_id_status: String?
    var home_address: String?
    var home_lat: String?
    var home_long: String?
    var work_address: String?
    var work_lat: String?
    var work_long: String?
    var invited_user_status: String?
    var vehicle_count: String?
    var loginTime: String?
    var route_request: String?
    var user_groups: String?
    var isOfficialEmail: String?
    var hideAnywhere: String?
    var  default_Vehicle_capacity:String?
    var  default_Vehicle_model:String?
    var  default_Vehicle_seqID:String?
    var  default_Vehicle_type:String?
    var  default_Vehicle_regNo:String?
    var FBuserDetail : LoginUserDetail?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        unique_id <- map["unique_id"]
        fb_id <- map["fb_id"]
        email <- map["email"]
        device_id <- map["device_id"]
        user_state <- map["user_state"]
        pool_search_data <- map["pool_search_data"]
        created_time <- map["created_time"]
        updated_time <- map["updated_time"]
        pool_scope <- map["pool_scope"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        full_name <- map["full_name"]
        facebook_url <- map["facebook_url"]
        email_id <- map["email_id"]
        secondary_email <- map["secondary_email"]
        secondary_email_status <- map["secondary_email_status"]
        dob <- map["dob"]
        age <- map["age"]
        mobile_number <- map["mobile_number"]
        share_mobile_number <- map["share_mobile_number"]
        gender <- map["gender"]
        address <- map["address"]
        marital_status <- map["marital_status"]
        city_name <- map["city_name"]
        pin <- map["pin"]
        linkedin_url <- map["linkedin_url"]
        current_company <- map["current_company"]
        current_designation <- map["current_designation"]
        govt_id <- map["govt_id"]
        govtidf_url <- map["govtidf_url"]
        govtidb_url <- map["govtidb_url"]
        officeid_url <- map["officeid_url"]
        govt_fid_status <- map["govt_fid_status"]
        govt_bid_status <- map["govt_bid_status"]
        profilepic_url <- map["profilepic_url"]
        profilepic_status <- map["profilepic_status"]
        office_id_status <- map["office_id_status"]
        home_address <- map["home_address"]
        home_lat <- map["latitude"]
        home_long <- map["longitude"]
        work_address <- map["wp_address"]
        work_lat <- map["wp_latitude"]
        work_long <- map["wp_longitude"]
        invited_user_status <- map["invited_user_status"]
        vehicle_count <- map["vehicle_count"]
        loginTime <- map["loginTime"]
        route_request <- map["route_request"]
        user_groups <- map["user_groups"]
        isOfficialEmail <- map["is_official_email"]
        hideAnywhere <- map["hide_anywhere"]
        default_Vehicle_capacity <- map["default_Vehicle_capacity"]
        default_Vehicle_model <- map["default_Vehicle_model"]
        default_Vehicle_seqID <- map["default_Vehicle_seq_id"]
        default_Vehicle_type <- map["default_Vehicle_type"]
        default_Vehicle_regNo <- map["default_Vehicle_vehregno"]
        FBuserDetail <- map["user_Detail"]
        
    }
}

class deviceTokenRequest: BaseModel {
    
    var user_id: String?
    var device_token: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        device_token <- map["device_token"]
    }
}


class deviceTokenResponse: BaseModel {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class LoginRequestor: BaseRequestor {
    
    func sendLoginRequest(_ requestObj: LoginRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<LoginRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.LOGIN, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: LoginResponse = Mapper<LoginResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    func sendFBLoginRequest(_ requestObj: FBLoginRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<FBLoginRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.FBLOGIN, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: FBLoginResponse = Mapper<FBLoginResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    func sendDeviceTokenRequest(_ requestObj: deviceTokenRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<deviceTokenRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.SEND_TOKEN, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: deviceTokenResponse = Mapper<deviceTokenResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    
    func logUserToCrashlytics()
    {
        //TODO:
        Crashlytics.sharedInstance().setUserEmail("richa.gupta@opcord.com")
        Crashlytics.sharedInstance().setUserIdentifier("")
        Crashlytics.sharedInstance().setUserName("")
    }
    
}
