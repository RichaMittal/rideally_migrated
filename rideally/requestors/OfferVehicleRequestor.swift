//
//  OfferVehicleRequestor.swift
//  rideally
//
//  Created by Sarav on 11/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class OfferVehicleRequest: BaseModel {
    var userId: String?
    var rideDate: String?
    var rideTime: String?
    var fromLocation: String?
    var fromLat: String?
    var fromLong: String?
    var toLocation: String?
    var toLat: String?
    var toLong: String?
    var poolType: String?
    
    var ridetype = "O"
    var pool_shared = "1" //hardcode to 1
    var pool_scope = "PUBLIC" //blank
    var pool_category = "9" //hardcode to 9
    var cost: String?
    var cost_type = "PK"
    var vehregno: String?
    var pool_repeat_days = ""
    var ridemessage: String? //comments
    var pool_groupids = "" //blank
    var pool_taxi_booking = "0" //hardcode to 0
    var veh_capacity: String?
    var distance: String?
    var duration: String?
    var via: String?
    var waypoints: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["user_id"]
        rideDate <- map["poolstartdate"]
        rideTime <- map["poolstarttime"]
        fromLocation <- map["from_location"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_lon"]
        toLocation <- map["to_location"]
        toLat <- map["to_lat"]
        toLong <- map["to_lon"]
        poolType <- map["pooltype"]
        
        ridetype <- map["ridetype"]
        pool_shared <- map["pool_shared"]
        pool_scope <- map["pool_scope"]
        pool_category <- map["pool_category"]
        vehregno <- map["vehregno"]
        ridemessage <- map["ridemessage"]
        pool_groupids <- map["pool_groupids"]
        pool_taxi_booking <- map["pool_taxi_booking"]
        veh_capacity <- map["veh_capacity"]
        distance <- map["distance"]
        cost <- map["cost"]
        cost_type <- map["cost_type"]
        duration <- map["duration"]
        pool_repeat_days <- map["pool_repeat_days"]
        via <- map["via"]
        waypoints <- map["waypoints"]
    }
}

//{"code":"571","message":"Success","data":[{"pool_id":"25715","distance":"9.4 km","duration":"27 mins"}]}
class OfferVehicleResponse : BaseModel
{
    var data: [OfferVehicleDataResponse]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}

class OfferVehicleDataResponse : BaseModel
{
    var poolID: String?
    var distance: String?
    var duration: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        poolID <- map["pool_id"]
        distance <- map["distance"]
        duration <- map["duration"]
    }
}

class JoinOrOfferRide : BaseModel
{
    var userID: String?
    var poolID: String?    
    var vehicleRegNo: String?
    var cost: String?
    var rideMsg: String?
    var vehicleCapacity: String?
    var key = "yes"
    var costType = "PK"
    var joiningPoint = ""
    var pickupPoint: String?
    var pickupPointLat: String?
    var pickupPointLong: String?
    var dropPoint: String?
    var dropPointLat: String?
    var dropPointLong: String?
    var reqCapacity = "1"
    var costMain = ""
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        poolID <- map["pool_id"]
        key <- map["key"]
        vehicleRegNo <- map["vehregno"]
        cost <- map["cost"]
        costType <- map["costtype"]
        joiningPoint <- map["joining_point"]
        rideMsg <- map["ride_message"]
        pickupPoint <- map["pickup_point"]
        pickupPointLat <- map["pickup_point_lat"]
        pickupPointLong <- map["pickup_point_long"]
        dropPoint <- map["drop_point"]
        dropPointLat <- map["drop_point_lat"]
        dropPointLong <- map["drop_point_long"]
        reqCapacity <- map["requested_capacity"]
        costMain <- map["cost_main"]
        vehicleCapacity <- map["vehicle_capacity"]
    }
    
}


class OfferVehicleRequestor: BaseRequestor {
    
    func createOfferVehicle(_ requestObj: OfferVehicleRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<OfferVehicleRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.CREATE_NEED_A_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: OfferVehicleResponse = Mapper<OfferVehicleResponse>().map(JSON: result as! [String : Any])!
            if(response.data != nil){
                success(true, response)
            }
            else{
                success(false, response)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func joinOrOfferRide(_ requestObj: JoinOrOfferRide, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<JoinOrOfferRide>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.JOIN_OR_OFFER_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: BaseModel = Mapper<BaseModel>().map(JSON: result as! [String : Any])!
            if(response.code == "73"){
                success(true, response)
            }
            else{
                success(false, response)
            }
        }) { (error) in
            failure(error)
        }
    }
}
