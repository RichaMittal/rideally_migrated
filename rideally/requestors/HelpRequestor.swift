//
//  HelpRequestor.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/17/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class HelpRequest: BaseModel {
    //var page_key: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        //page_key <- map["page_key"]
    }
}

class HelpResponse: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class HelpRequestor: BaseRequestor {
    
    func sendHelpRequest(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        //let parameters = Mapper<StaticPageRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.GET_FAQ, parameters: nil, encoding: .default, success: { (result) in
            
            let response: HelpResponse = Mapper<HelpResponse>().map(JSON: result as! [String: Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    
}
