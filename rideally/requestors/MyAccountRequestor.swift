//
//  MyAccountRequestor.swift
//  rideally
//
//  Created by Raghunathan on 10/19/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper


class GetUserPointsOnRecahargeRequest: BaseModel
{
    var userID: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        userID <- map["user_id"]
    }
}

class GetUserPointsOnRecahargeResponse: BaseModel
{
    var dataObj: PointData?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}


class PointData: BaseModel
{
    var maximumallowedpoints: String?
    var pointsavailable: Int?
    var pointsearned: String?
    var pointsused: String?
    var transactions: TransactionData?
    var earnedPoints: [EarnedData]?
    var usedPoints: [EarnedData]?
    var bonusPoints: Int?
    var redeemablePoints: Int?
    var bonusPts: Int?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        maximumallowedpoints <- map["max_allowed_points"]
        pointsavailable <- map["points_available"]
        pointsearned <- map["points_earned"]
        pointsused <- map["points_used"]
        transactions <- map["transactions"]
        earnedPoints <- map["earned"]
        usedPoints <- map["used"]
        bonusPoints <- map["points_as_bonus"]
        redeemablePoints <- map["points_redeemable"]
        bonusPts <- map["points_bonus"]
    }
}

class TransactionData: BaseModel
{
    var earnedPoints: [EarnedData]?
    //var pointsavailable: String?
    //var pointsearned: String?
    //var pointsused: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        earnedPoints <- map["earned"]
        //pointsavailable <- map["points_available"]
        //pointsearned <- map["points_earned"]
        //pointsused <- map["points_used"]
    }
}

class EarnedData: BaseModel
{
    var points: String?
    var poolStartDate: String?
    var poolStartTime: String?
    var transactionType: String?
    var transactionReason: String?
    var distance: String?
    var users: [UsersDetail]?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        points <- map["points"]
        poolStartDate <- map["pool_start_date"]
        poolStartTime <- map["pool_start_time"]
        transactionType <- map["txn_type"]
        transactionReason <- map["reason"]
        distance <- map["distance"]
        users <- map["users"]
    }
}

class UsersDetail: BaseModel
{
    var firstName: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        firstName <- map["first_name"]
    }
}

class UpdateTransactionOnRecahargeRequest: BaseModel
{
    var userID: String?
    var orderID: String?
    var transactionID: String?
    var transactionAmt: String?
    var mid: String?
    var responseCode: String?
    var currency: String?
    var gateWayname: String?
    var channelType: String?
    var validCheckSum: String?
    var responseMSG: String?
    var transactionFrom: String?
    var transactionDate: String?
    var bankTransactionID: String?
    var paymentMode: String?
    var bankName: String?
    var status: String?
    var rechargeAmount: String?
    var firstName: String?
    var emailID: String?
    var mobileNumber: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        userID <- map["USERID"]
        orderID <- map["ORDERID"]
        transactionID <- map["TXNID"]
        transactionAmt <- map["TXNAMOUNT"]
        mid <- map["MID"]
        responseCode <- map["RESPCODE"]
        currency <- map["CURRENCY"]
        gateWayname <- map["GATEWAYNAME"]
        channelType <- map["channel_type"]
        validCheckSum <- map["CHECKSUM_VALID"]
        responseMSG <- map["RESPMSG"]
        transactionFrom <- map["transaction_from"]
        transactionDate <- map["TXNDATE"]
        bankTransactionID <- map["BANKTXNID"]
        paymentMode <- map["PAYMENTMODE"]
        bankName <- map["BANKNAME"]
        status <- map["STATUS"]
        rechargeAmount <- map["RECHARGEAMOUNT"]
        firstName <- map["first_name"]
        emailID <- map["email_id"]
        mobileNumber <- map["mobile_no"]
    }
}


class GetRedeemPointsOnEncashRequest: BaseModel
{
    var userID: String?
    var firstName: String?
    var mobileNumber: String?
    var emailID: String?
    var transType: String?
    var transAmt: String?
    var redeemAmt: String?
    var redeemOpt: String?
    var cardType: String?
    var redeemAddress: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        userID <- map["user_id"]
        firstName <- map["first_name"]
        emailID <- map["email_id"]
        mobileNumber <- map["mobile_no"]
        transType <- map["txn_type"]
        transAmt <- map["txn_amount"]
        redeemAmt <- map["redeem_amount"]
        redeemOpt <- map["redeem_option"]
        cardType <- map["card_type"]
        redeemAddress <- map["redeem_address"]
    }
}

class RedeemPointsOnEncashResponse: BaseModel
{
    override func mapping(map: Map)
    {
        super.mapping(map: map)
    }
}


class MyAccountRequestor: BaseRequestor
{
    func sendGetUserPointsRequest(_ requestObj: GetUserPointsOnRecahargeRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<GetUserPointsOnRecahargeRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.GETUSERPOINTS, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: GetUserPointsOnRecahargeResponse = Mapper<GetUserPointsOnRecahargeResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            })
        { (error) in
            failure(error)
        }
    }
    
    
    func sendUpdateTransactionRequest(_ requestObj: UpdateTransactionOnRecahargeRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<UpdateTransactionOnRecahargeRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.UPDATETRANSACTION, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            success(true, result)
            })
        { (error) in
            failure(error)
        }
    }

    
    func sendRedeemPointsRequest(_ requestObj: GetRedeemPointsOnEncashRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<GetRedeemPointsOnEncashRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.GETREDEEMPOINTS, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            success(true, result)
            
            })
        { (error) in
            failure(error)
        }
    }

    func generateCheckSumRequest(_ requestObj: checkSumRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<checkSumRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(PAYTM_GENERATE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: checkSumResponse = Mapper<checkSumResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
}

class checkSumRequest: BaseModel
{
    var MID: String?
    var TXN_AMOUNT: String?
    var CUST_ID: String?
    var CHANNEL_ID: String?
    var INDUSTRY_TYPE_ID: String?
    var WEBSITE: String?
    var ORDER_ID: String?
    var REQUEST_TYPE: String?
    var CALLBACK_URL: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        MID <- map["MID"]
        TXN_AMOUNT <- map["TXN_AMOUNT"]
        CUST_ID <- map["CUST_ID"]
        CHANNEL_ID <- map["CHANNEL_ID"]
        INDUSTRY_TYPE_ID <- map["INDUSTRY_TYPE_ID"]
        WEBSITE <- map["WEBSITE"]
        ORDER_ID <- map["ORDER_ID"]
        REQUEST_TYPE <- map["REQUEST_TYPE"]
        CALLBACK_URL <- map["CALLBACK_URL"]
    }
}

class checkSumResponse: BaseModel
{
    var CHECKSUMHASH: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        CHECKSUMHASH <- map["CHECKSUMHASH"]
    }
}
