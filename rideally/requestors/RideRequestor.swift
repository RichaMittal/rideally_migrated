//
//  RideRequestor.swift
//  rideally
//
//  Created by Sarav on 01/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper



class GetRideDetailsRequest: BaseModel
{
    var userID: String?
    var userName: String?
    var userMobile: String?
    var gender: String?
    var currentLocation: String?
    var vehicleNumber: String?
    var vehicleModel: String?
    var vehicleType: String?
    var helpMobile: String?
    var helpMail: String?
    var sourceLocation: String?
    var destinationLocation: String?
    var url: String?
    var latitude: String?
    var longtitude: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        userName <- map["user_name"]
        userMobile <- map["user_number"]
        gender <- map["gender"]
        currentLocation <- map["current_location"]
        vehicleNumber <- map["vehicle_number"]
        vehicleModel <- map["veh_model"]
        vehicleType <- map["veh_type"]
        helpMobile <- map["help_number"]
        helpMail <- map["help_mail"]
        sourceLocation <- map["src_loc"]
        destinationLocation <- map["dest_loc"]
        url <- map["url"]
        latitude <- map["lat"]
        longtitude <- map["long"]
    }
}



class AllRidesRequest: BaseModel {
    var reqDate: String?
    var reqTime: String?
    var pooltype: String?
    var userId: String?
    var showRtype: String?
    var page: String?
    var searchType: String?
    var rideScope: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        reqDate <- map["reqDate"]
        reqTime <- map["reqTime"]
        pooltype <- map["pooltype"]
        userId <- map["userId"]
        showRtype <- map["showRtype"]
        page <- map["page"]
        searchType <- map["searchType"]
        rideScope <- map["rideScope"]
    }
}

class AllRidesResponse: BaseModel
{
    var totalRecords: Int?
    var pools: [Ride]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        totalRecords <- map["total_records"]
        pools <- map["pools"]
    }
}

class Ride: BaseModel
{
    var source: String?
    var destination: String?
    var dateTime: String?
    var cost: String?
    var userCost: String?
    var costType: String?
    var members: String?
    var seatsLeft: String?
    var poolID: String?
    
    var poolTaxiBooking: String?
    var poolJoinedMembers: String?
    var createdBy: String?
    var poolStatus: String?
    var poolShared: String?
    var poolCategory: String?
    var poolRideType: String?
    var requestedStatus: String?
    var rideType: String?
    
    var ownerProfilePicStatus: String?
    var ownerProfilePicUrl: String?
    var ownerFBID: String?
    var ownerName: String?
    var ownerEmail: String?
    var ownerUserId: String?
    var sourceID: String?
    var destID: String?
    
    var sourceLat: String?
    var sourceLong: String?
    var destLat: String?
    var destLong: String?
    
    var vehicleType: String?
    var vehicleRegNo: String?
    var userState: String?
    var startDate: String?
    var startTime: String?
    var finalCost: Int?
    var fullCost: Int?
    var poolScope: String?
    var via: String?
    var viaLat: String?
    var viaLon: String?
    var distance: String?
    var duration: String?
    var fullSource: String?
    var fullDest: String?
    var rideOwnerFBID: String?
    var rideOwnerUserID: String?
    var poolGroupInsStatus: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        rideOwnerFBID <- map["owner_fb_id"]
        rideOwnerUserID <- map["owner_id"]
        
        sourceLat <- map["src_lat"]
        sourceLong <- map["src_lon"]
        destLat <- map["dest_lat"]
        destLong <- map["dest_lon"]
        
        source <- map["src_loc_name"]
        destination <- map["dest_loc_name"]
        dateTime <- map["pool_sdate_time"]
        startDate <- map["pool_start_date"]
        startTime <- map["pool_start_time"]
        cost <- map["pool_cost"]
        userCost <- map["user_cost"]
        costType <- map["pool_cost_type"]
        members <- map["pool_joined_member"]
        seatsLeft <- map["pool_seat_left"]
        poolID <- map["pool_id"]
        
        poolTaxiBooking <- map["pool_taxi_booking"]
        poolJoinedMembers <- map["pool_joined_members"]
        createdBy <- map["created_by"]
        poolStatus <- map["pool_status"]
        poolShared <- map["pool_shared"]
        poolCategory <- map["pool_category"]
        poolRideType <- map["pool_ride_type"]
        requestedStatus <- map["requested_status"]
        rideType <- map["ride_type"]
        
        ownerProfilePicStatus <- map["owner_profilepic_status"]
        ownerProfilePicUrl <- map["owner_profilepic_url"]
        ownerFBID <- map["owner_fbid"]
        ownerUserId <- map["owner_userid"]
        ownerName <- map["owner_name"]
        ownerEmail <- map["owner_email_id"]
        sourceID <- map["src_id"]
        destID <- map["dest_id"]
        
        vehicleType <- map["vehicle_type"]
        vehicleRegNo <- map["vehicle_regno"]
        userState <- map["user_state"]
        finalCost <- map["final_cost"]
        fullCost <- map["full_cost"]
        poolScope <- map["pool_scope"]
        via <- map["via"]
        viaLat <- map["via_lat"]
        viaLon <- map["via_lon"]
        distance <- map["distance"]
        duration <- map["duration"]
        fullSource <- map["full_source_location"]
        fullDest <- map["full_destination_location"]
        poolGroupInsStatus <- map["pool_group_ins_status"]
    }
}

class MyRidesResponse: BaseModel
{
    var data: myRidesData?
    var dataOngoing: ongoingRidesData?
    var dataCompleted: completedRidesData?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
        dataOngoing <- map["data"]
        dataCompleted <- map["data"]
    }
}

class myRidesData: BaseModel {
    var myPoolRides: [MyRide]?
    var myTaxiRides: [MyRide]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        myPoolRides <- map["myallpoolrides"]
       // myTaxiRides <- map["booking"]
    }
}

class ongoingRidesData: BaseModel {
    var myPoolRides: [MyRide]?
    var myTaxiRides: [MyRide]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        myPoolRides <- map["myallpoolrides"]
    }
}

class completedRidesData: BaseModel {
    var myPoolRides: [MyRide]?
    var myTaxiRides: [MyRide]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        myPoolRides <- map["myallpoolrides"]
        myTaxiRides <- map["booking"]
    }
}

class MyRide: Ride{
    
    var poolDate: String?
    var poolTime: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        source <- map["source_location"]
        sourceLat <- map["slat"]
        sourceLong <- map["slon"]
        destination <- map["destination_location"]
        destLat <- map["dlat"]
        destLong <- map["dlon"]
        poolDate <- map["pool_start_date"]
        poolTime <- map["pool_start_time"]
        cost <- map["cost"]
        costType <- map["cost_type"]
        members <- map["pool_joined_members"]
        seatsLeft <- map["seat_left"]
        poolID <- map["pool_id"]
        
        poolTaxiBooking <- map["pool_taxi_booking"]
        createdBy <- map["created_by"]
        poolStatus <- map["pool_status"]
        poolRideType <- map["pool_type"]
        requestedStatus <- map["requested_status"]
        rideType <- map["ride_type"]
        viaLat <- map["vlat"]
        viaLon <- map["vlon"]
        fullSource <- map["full_source_location"]
        fullDest <- map["full_destination_location"]
    }
}

class RideDetail: BaseModel {
    
    var source: String?
    var sourceLat: String?
    var sourceLong: String?
    var destination: String?
    var destinationLat: String?
    var destinationLong: String?
    
    var startDate: String?
    var startTime: String?
    
    var startDateDisplay: String?
    var startTimeDisplay: String?
    
    var cost: String?
    var costType: String?
    var seatsLeft: String?
    var poolType: String?
    
    var distance: String?
    var distanceUnit: String?
    var duration: String?
    var durationUnit: String?
    
    var comments: String?
    
    var ownerName: String?
    var ownerCompany: String?
    var ownerEmail: String?
    var ownerPhone: String?
    var ownerImgUrl: String?
    var ownerImgStatus: String?
    var ownerScope: String?
    var ownerFBID: String?
    
    var poolUsers: [Member]?
    var poolJoinedMembers: String?
    var poolGroups: [Group]?
    
    var poolTaxiBooking: String?
    var poolID: String?
    var createdBy: String?
    var joinStatus: String?
    var requestedStatus: String?
    var rideType: String?
    
    var vehicleRegNo: String?
    var vehicleModel: String?
    var vehicleType: String?
    
    var poolCostType: String?
    var poolCategory: String?
    var poolShared: String?
    
    var sourceID: String?
    var destID: String?
    
    var userState: String?
    var currentStatus: String?
    var vehicleOwnerID: String?
    var points: String?
    
    var pickUpPoint: String?
    var dropPoint: String?
    
    var payTo: String?
    var youPay: String?
    var fullCost: Int?
    var txvtVehModel: String?
    var costUpdated: String?
    var couponId: String?
    var couponCode: String?
    var discountPrice: Int?
    var waypoints: String?
    var via: String?
    var officeRideType: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        pickUpPoint <- map["pickup_point"]
        dropPoint <- map["drop_point"]
        
        source <- map["source_location"]
        sourceLat <- map["source_latitude"]
        sourceLong <- map["source_longitude"]
        destination <- map["destination_location"]
        destinationLat <- map["destination_latitude"]
        destinationLong <- map["destination_longitude"]
        startDate <- map["pool_start_date"]
        startTime <- map["pool_start_time"]
        
        startDateDisplay <- map["pStratDate"]
        startTimeDisplay <- map["pStartTime"]
        
        cost <- map["pool_cost"]
        costType <- map["pool_cost_type"]
        seatsLeft <- map["seat_left"]
        poolType <- map["pool_type"]
        ownerScope <- map["owner_pool_scope"]
        
        distance <- map["distance"]
        duration <- map["duration"]
        
        poolUsers <- map["pool_users"]
        poolJoinedMembers <- map["pool_joined_member"]
        poolGroups <- map["pool_groups"]
        ownerFBID <- map["rideOwner_fbid"]
        
        comments <- map["ride_message"]
        ownerName <- map["full_name"]
        ownerCompany <- map["owner_company"]
        ownerEmail <- map["owner_email"]
        ownerPhone <- map["owner_mobile"]
        ownerImgUrl <- map["owner_profilepic_url"]
        ownerImgStatus <- map["owner_profilepic_status"]
        
        poolTaxiBooking <- map["pool_taxi_booking"]
        poolID <- map["pool_id"]
        createdBy <- map["created_by"]
        joinStatus <- map["join_status"]
        requestedStatus <- map["requested_status"]
        rideType <- map["ride_type"]
        
        vehicleRegNo <- map["vehicle_regno"]
        vehicleType <- map["vehicle_type"]
        vehicleModel <- map["vehicle_model"]
        
        poolCostType <- map["pool_cost_type"]
        poolCategory <- map["pool_category"]
        poolShared <- map["pool_shared"]
        
        userState <- map["user_state"]
        currentStatus <- map["current_status"]
        vehicleOwnerID <- map["veh_user_id"]
        points <- map["points"]
        payTo <- map["pay_to"]
        youPay <- map["you_pay"]
        fullCost <- map["full_cost"]
        txvtVehModel <- map["txvt_model"]
        costUpdated <- map["cost_updated"]
        couponId <- map["coupon_id"]
        couponCode <- map["coupon_code"]
        discountPrice <- map["discount_price"]
        via <- map["via"]
        waypoints <- map["waypoints"]
        officeRideType <- map["office_ride_type"]
    }
}

class Group: BaseModel {
    var key: String?
    var name: String?
    var type: String?
    var url_key: String?
    var id: String?
    var helpEmail: String?
    var helpNumber: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        key <- map["group_key"]
        id <- map["group_id"]
        name <- map["group_name"]
        type <- map["group_type"]
        url_key <- map["group_url_key"]
        helpEmail <- map["help_email"]
        helpNumber <- map["help_nember"]
    }
}
class Member: BaseModel
{
    var email: String?
    var mobile: String?
    var name: String?
    
    var joinStatus: String?
    var requestedSatus: String?
    var profilePic: String?
    var profilePicStatus: String?
    
    var memberUserID: String?
    var memberFBID: String?
    
    var dropLocation: String?
    var pickupLocation: String?
    var comments: String?
    
    var userState: String?
    var riderCost: String?
    
    var vehicleRegNo: String?
    var vehicleType: String?
    var dropLatitude: String?
    var dropLongitude: String?
    var dropPoint: String?
    var pickupLatitude: String?
    var pickupLongitude: String?
    var pickupPoint: String?
    
    
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        email <- map["email"]
        mobile <- map["mobile_number"]
        name <- map["full_name"]
        joinStatus <- map["join_status"]
        requestedSatus <- map["requested_status"]
        profilePic <- map["user_profilepic_url"]
        profilePicStatus <- map["user_profilepic_status"]
        memberUserID <- map["um_user_id"]
        memberFBID <- map["rideMember_fbid"]
        
        dropLocation <- map["drop_location"]
        pickupLocation <- map["pickup_location"]
        comments <- map["rider_ride_message"]
        
        userState <- map["user_state"]
        riderCost <- map["rider_cost"]
        vehicleRegNo <- map["vehicle_regno"]
        vehicleType <- map["vehicle_type"]
        
        dropLatitude <- map["drop_latitude"]
        dropLongitude <- map["drop_longitude"]
        dropPoint <- map["drop_point"]
        pickupLatitude <- map["pickup_latitude"]
        pickupLongitude <- map["pickup_longitude"]
        pickupPoint <- map["pickup_point"]
        
    }
}

class MemberDetailResponse : BaseModel
{
    var data: MemberDetailResponseUserInfo?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class MemberDetailResponseUserInfo : BaseModel
{
    var userInfo: MemberDetail?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userInfo <- map["userInfo"]
    }
}

class MemberDetail : BaseModel
{
    var profilePic: String?
    var profilePicStatus: String?
    var userState: String?
    var gender: String?
    var email: String?
    var mobile: String?
    var name: String?
    var fbID: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        email <- map["email"]
        mobile <- map["contact"]
        name <- map["name"]
        profilePic <- map["Profilepic_url"]
        profilePicStatus <- map["Profilepic_status"]
        userState <- map["UserState"]
        fbID <- map["fb_id"]
        gender <- map["gender"]
    }
}



class SearchRideRequest: BaseModel
{
    var userId: String?
    var reqDate: String?
    var reqTime: String?
    var startLoc: String?
    var fromLat: String?
    var fromLong: String?
    var endLoc: String?
    var toLat: String?
    var toLong: String?
    var poolType: String?
    var ridetype: String?
    var searchCategory = "0"
    var searchType: String?
    var searchRange = "5"
    var searchFrom: String?
    var groupID: String?
    var groupType: String?
    var rideScope: String?
    var poolScope: String?
    var recordSize = "50"
    var reqCurrentDate: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["userId"]
        reqDate <- map["reqDate"]
        reqTime <- map["reqTime"]
        startLoc <- map["startLoc"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_lon"]
        endLoc <- map["endLoc"]
        toLat <- map["to_lat"]
        toLong <- map["to_lon"]
        poolType <- map["pooltype"]
        ridetype <- map["ridetype"]
        searchCategory <- map["searchcategory"]
        searchType <- map["searchType"]
        searchRange <- map["searchRange"]
        searchFrom <- map["searchFrom"]
        groupID <- map["gid"]
        groupType <- map["group_type"]
        rideScope <- map["rideScope"]
        poolScope <- map["pool_scope"]
        recordSize <- map["recordSize"]
        reqCurrentDate <- map["reqCurrentDate"]
    }
}

class SearchRecurrentRideRequest: BaseModel
{
    var userId: String?
    var reqDate: String?
    var reqTime: String?
    var startLoc: String?
    var fromLat: String?
    var fromLong: String?
    var endLoc: String?
    var toLat: String?
    var toLong: String?
    var poolType: String?
    var ridetype = ""
    var searchCategory = "0"
    var searchType = "recurrent"
    var page: String?
    var recordSize = "20"
    var reqCurrentDate: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["userId"]
        reqDate <- map["reqDate"]
        reqTime <- map["reqTime"]
        startLoc <- map["startLoc"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_lon"]
        endLoc <- map["endLoc"]
        toLat <- map["to_lat"]
        toLong <- map["to_lon"]
        poolType <- map["pooltype"]
        ridetype <- map["ridetype"]
        searchCategory <- map["searchcategory"]
        searchType <- map["searchType"]
        page <- map["page"]
        recordSize <- map["recordSize"]
        reqCurrentDate <- map["reqCurrentDate"]
    }
}

class RideMembersResponse: BaseModel {
    var poolUsers: [Member]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        poolUsers <- map["pool_users"]
    }
}

class InviteMembersResponse: BaseModel {
    var data: [InviteResult]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class InviteResult: BaseModel{
    var msgCode: String?
    var email: String?
    var mobile: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        msgCode <- map["msg"]
        email <- map["email"]
        mobile <- map["mobile"]
    }
}

class recreateRideRequest: BaseModel
{
    var poolId: String?
    var userId: String?
    var datesSelected: String?
    var poolStartTime: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        poolId <- map["pool_id"]
        userId <- map["user_id"]
        datesSelected <- map["dates_selected"]
        poolStartTime <- map["pool_start_time"]
    }
}

class recreateRideResponse: BaseModel
{
    var data: [recreateRideData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class recreateRideData: BaseModel
{
    var poolId: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        poolId <- map["pool_id"]
    }
}

class setRatingRequest: BaseModel
{
    var poolId: String?
    var userId: String?
    var rateToUserIds: String?
    var ratings: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        poolId <- map["pool_id"]
        userId <- map["user_id"]
        rateToUserIds <- map["rate_to_user_id"]
        ratings <- map["rating"]
    }
}

class setRatingResponse: BaseModel
{
    var data: [recreateRideData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class RideRequestor: BaseRequestor {
    
    func getAllRides(_ requestObj: AllRidesRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<AllRidesRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.ALL_RIDES, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: AllRidesResponse = Mapper<AllRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func getMyRides(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["user_id": UserInfo.sharedInstance.userID]
        
        makePOSTRequestWithparameters(URLS.MY_RIDES, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: MyRidesResponse = Mapper<MyRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func getOngoingRides(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["user_id": UserInfo.sharedInstance.userID]
        
        makePOSTRequestWithparameters(URLS.RIDES_ONGOING, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: MyRidesResponse = Mapper<MyRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func getCompletedRides(_ key: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["user_id": UserInfo.sharedInstance.userID, "key": key]
        
        makePOSTRequestWithparameters(URLS.RIDES_COMPLETED, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: MyRidesResponse = Mapper<MyRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func getRideDetails(_ poolId: String, userId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id": userId]
        
        makePOSTRequestWithparameters(URLS.RIDE_DETAILS, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: RideDetail = Mapper<RideDetail>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func getTaxiDetails(_ poolId: String, userId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id": userId]
        
        makePOSTRequestWithparameters(URLS.TAXI_DETAILS, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: RideDetail = Mapper<RideDetail>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func searchRides(_ requestObj: SearchRideRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<SearchRideRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.ALL_RIDES, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: AllRidesResponse = Mapper<AllRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func searchRecurrentRides(_ requestObj: SearchRecurrentRideRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<SearchRecurrentRideRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.ALL_RIDES, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: AllRidesResponse = Mapper<AllRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func joinTaxiRide(_ poolId: String, userId: String, pickupPoint: String?, dropPoint: String?, comments: String?, pCost: String?, costMain: String?, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        var params = ["pool_id": poolId, "user_id": userId, "cost_type":"", "vehregno":"", "key":""]
        
        if(pickupPoint != nil){
            params["pickup_point"] = pickupPoint!
        }
        if(dropPoint != nil){
            params["drop_point"] = dropPoint!
        }
        if(comments != nil){
            params["ride_comment"] = comments!
        }
        if(dropPoint != nil){
            params["pcost"] = pCost!
        }
        if(comments != nil){
            params["cost_main"] = costMain!
        }
        makePOSTRequestWithparameters(URLS.JOIN_TAXI_RIDE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1701"){
                success(true, "")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func unJoinRide(_ poolId: String, userId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id": userId]
        makePOSTRequestWithparameters(URLS.UNJOIN_RIDE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "31" || responseObj.code == "33" || responseObj.code == "34"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func acceptRideRequest(_ poolId: String, userId: String, userType: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        //userType: from detailsPage,ridelist - friend, memberslist - join
        let params = ["pool_id": poolId, "user_id": userId, "requestType":"Accept", "userType":userType]
        makePOSTRequestWithparameters(URLS.RESPOND_TO_RIDE_REQUEST, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "21" || responseObj.code == "23" || responseObj.code == "121" || responseObj.code == "122"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func rejectRideRequest(_ poolId: String, userId: String, userType: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id": userId, "requestType":"Reject", "userType":userType]
        makePOSTRequestWithparameters(URLS.RESPOND_TO_RIDE_REQUEST, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "61" || responseObj.code == "63" || responseObj.code == "131" || responseObj.code == "132"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func cancelRequest(_ poolId: String, userId: String, cancelrequestType: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id": userId, "cancelRequestType":cancelrequestType]
        makePOSTRequestWithparameters(URLS.CANCEL_REQUEST, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "91" || responseObj.code == "92"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func cancelTaxiRide(_ poolId: String, userId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id":userId]
        makePOSTRequestWithparameters(URLS.CANCEL_TAXI_RIDE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1711"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func acceptTaxiRideRequest(_ poolId: String, userId: String, pCost: String, costMain: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        //userType: from detailsPage,ridelist - friend, memberslist - join
        let params = ["pool_id": poolId, "user_id": userId, "offer_id":"2", "pcost":pCost, "cost_main":costMain]
        makePOSTRequestWithparameters(URLS.ACCEPT_TAXIRIDE_REQUEST, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1721"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func rejectTaxiRideRequest(_ poolId: String, userId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id": userId]
        makePOSTRequestWithparameters(URLS.REJECT_TAXIRIDE_REQUEST, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1731"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func deleteRide(_ poolId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId]
        makePOSTRequestWithparameters(URLS.DELETE_RIDE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "51" || responseObj.code == "53"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func sendInvite(_ poolId: String, ownerId: String,  emailIDs: String?, mobileNOs: String?, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        var params = ["pool_id": poolId, "owner_id":ownerId]
        if let email = emailIDs{
            params["email_string"] = email
        }
        if let mobile = mobileNOs{
            params["mobile_string"] = mobile
        }
        
        makePOSTRequestWithparameters(URLS.INVITE_TO_RIDE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: InviteMembersResponse = Mapper<InviteMembersResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1711"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
            
        }) { (error) in
            
            failure(error)
        }
    }
    
    func getRideMembers(_ poolId: String, userId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id":userId, "detail_key":"1"]
        makePOSTRequestWithparameters(URLS.GET_RIDE_MEMBERS, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: RideMembersResponse = Mapper<RideMembersResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func updateRide(_ reqObj: UpdateRideRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<UpdateRideRequest>().toJSON(reqObj)
        makePOSTRequestWithparameters(URLS.UPDATE_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if((responseObj.code == "43")||(responseObj.code == "41")){
                success(true, "")
            }else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getMembersDetails(_ memberId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["user_id": memberId, "auth_user_id":UserInfo.sharedInstance.userID]
        makePOSTRequestWithparameters(URLS.GET_MEMBERS_DETAILS, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: MemberDetailResponse = Mapper<MemberDetailResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "415"){
                success(true, responseObj)
            }
            else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getVehicleConfig(_ configType: String, groupID: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        //configType-ride.. config_type =>workplace, group_id => group_id of the workplace
        //TODO:
        var params = ["config_type": configType]
        if(groupID != ""){
            params = ["config_type": configType, "group_id":groupID]
        }
        
        makePOSTRequestWithparameters(URLS.RIDE_VEHICLES_CONFIG, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: VehicleConfigResponse = Mapper<VehicleConfigResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "90909"){
                success(true, responseObj)
            }else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getSOSForRideDetail(_ reqObj: GetRideDetailsRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<GetRideDetailsRequest>().toJSON(reqObj)
        makePOSTRequestWithparameters(URLS.GETSOS, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            
            if(responseObj.code == "18384")
            {
                success(true, "")
            }
            else
            {
                success(false, responseObj)
            }
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getDefaultVehicle(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["userId": UserInfo.sharedInstance.userID]
        
        makePOSTRequestWithparameters(URLS.GET_DEFAULT_VEHICLE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: VehicleDefaultResponse = Mapper<VehicleDefaultResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "121")
            {
                UserInfo.sharedInstance.vehicleSeqID = (responseObj.data?.first?.vehicleSeqId)!
                UserInfo.sharedInstance.vehicleType = (responseObj.data?.first?.vehicleType)!
                UserInfo.sharedInstance.vehicleModel = (responseObj.data?.first?.vehicleModel)!
                UserInfo.sharedInstance.vehicleRegNo = (responseObj.data?.first?.vehicleRegistrationNumber)!
                UserInfo.sharedInstance.vehicleSeatCapacity = (responseObj.data?.first?.vehicleCapacity)!
                UserInfo.sharedInstance.vehicleKind = (responseObj.data?.first?.vehicleKind)!
                UserInfo.sharedInstance.vehicleOwnerName = (responseObj.data?.first?.vehicleOwnerName)!
                UserInfo.sharedInstance.insurerName = (responseObj.data?.first?.insurerName)!
                UserInfo.sharedInstance.insuredCmpnyName = (responseObj.data?.first?.insuredCompanyName)!
                UserInfo.sharedInstance.insuranceId = (responseObj.data?.first?.insuranceId)!
                UserInfo.sharedInstance.insuranceExpiryDate = (responseObj.data?.first?.insuranceExpiryDate)!
                UserInfo.sharedInstance.insUploadStatus = (responseObj.data?.first?.insuranceUploadStatus)!
                UserInfo.sharedInstance.insUrl = (responseObj.data?.first?.insuranceUrl)!
                UserInfo.sharedInstance.rcUploadStatus = (responseObj.data?.first?.rcUploadStatus)!
                UserInfo.sharedInstance.rcUrl = (responseObj.data?.first?.rcUrl)!
                success(true, responseObj)
            }else{
                success(false, "")
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func recreateRide(_ reqObj: recreateRideRequest,success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<recreateRideRequest>().toJSON(reqObj)
        
        makePOSTRequestWithparameters(URLS.RECREATE_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: recreateRideResponse = Mapper<recreateRideResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "111")
            {
                success(true, responseObj)
            }else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func setPoolRating(_ reqObj: setRatingRequest,success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<setRatingRequest>().toJSON(reqObj)
        
        makePOSTRequestWithparameters(URLS.SETPOOL_RATING, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: setRatingResponse = Mapper<setRatingResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1601")
            {
                success(true, responseObj)
            }else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
}

class VehicleDefaultResponse: BaseModel
{
    var data: [myVehicleData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class myVehicleData: BaseModel {
    
    
    var vehicleSeqId: String?
    var userId: String?
    var vehicleKind: String?
    var vehicleOwnerName: String?
    var vehicleType: String?
    var isDefault: String?
    var vehicleModel: String?
    var vehregnoSearch: String?
    var vehicleRegistrationNumber: String?
    var vehicleCapacity: String?
    var vehicleStatus: String?
    var insurerName: String?
    var insuredCompanyName: String?
    var insuranceId: String?
    var insuranceExpiryDate: String?
    var insuranceUrl: String?
    var rcUrl: String?
    var insuranceUploadStatus: String?
    var rcUploadStatus: String?
    var createdBy: String?
    var createdTime: String?
    var updatedBy: String?
    var updatedTime: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        vehicleSeqId <- map["vehicle_seq_id"]
        userId <- map["user_id"]
        vehicleKind <- map["vehicle_kind"]
        vehicleOwnerName <- map["vehicle_owner_name"]
        vehicleType <- map["vehicle_type"]
        isDefault <- map["is_default"]
        vehicleModel <- map["vehicle_model"]
        vehregnoSearch <- map["vehregno_search"]
        vehicleRegistrationNumber <- map["vehicle_registration_number"]
        vehicleCapacity <- map["vehicle_capacity"]
        vehicleStatus <- map["vehicle_status"]
        insurerName <- map["insurer_name"]
        insuredCompanyName <- map["insured_company_name"]
        insuranceId <- map["insurance_id"]
        insuranceExpiryDate <- map["insurance_expiry_date"]
        insuranceUrl <- map["insurance_url"]
        rcUrl <- map["rc_url"]
        insuranceUploadStatus <- map["insurance_upload_status"]
        rcUploadStatus <- map["rc_upload_status"]
        createdBy <- map["created_by"]
        createdTime <- map["created_time"]
        updatedBy <- map["updated_by"]
        updatedTime <- map["updated_time"]
    }
}

class VehicleConfigResponse: BaseModel {
    var car: String?
    var bike: String?
    var suv: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        car <- map["data.default_data.car.2"]
        bike <- map["data.default_data.bike.2"]
        suv <- map["data.default_data.suv.2"]
    }
}

class UpdateRideRequest: BaseModel {
    
    var userID: String?
    var poolID: String?
    var seatLeft: String?
    var vehregno: String?
    var pooltype: String?
    var cost_type: String?
    var cost: String?
    var poolstartdate: String?
    var poolstarttime: String?
    var from_location: String?
    var from_lat: String?
    var from_lon: String?
    var to_location: String?
    var to_lat: String?
    var to_lon: String?
    var rideMessage: String?
    var email: String?
    var poolStatus = "A"
    var rideType: String?
    var poolScope: String?
    var poolCategory: String?
    var poolShared: String?
    var groupIDs = ""
    var via: String?
    var via_lat: String?
    var via_lon: String?
    var duration: String?
    var distance: String?
    var waypoints: String?
    var flipLoc: String?
    var officeRideType : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["userid"]
        poolID <- map["poolID"]
        seatLeft <- map["seatleft"]
        vehregno <- map["vehregno"]
        pooltype <- map["pooltype"]
        cost_type <- map["cost_type"]
        cost <- map["cost"]
        poolstartdate <- map["poolstartdate"]
        poolstarttime <- map["poolstarttime"]
        from_location <- map["from_location"]
        from_lat <- map["from_lat"]
        from_lon <- map["from_lon"]
        to_location <- map["to_location"]
        to_lat <- map["to_lat"]
        to_lon <- map["to_lon"]
        rideMessage <- map["ridemessage"]
        email <- map["email"]
        poolStatus <- map["poolstatus"]
        rideType <- map["ridetype"]
        poolScope <- map["poolscope"]
        poolCategory <- map["pool_category"]
        poolShared <- map["pool_shared"]
        groupIDs <- map["groupids"]
        via <- map["via"]
        via_lat <- map["via_lat"]
        via_lon <- map["via_lon"]
        distance <- map["distance"]
        duration <- map["duration"]
        waypoints <- map["waypoints"]
        flipLoc <- map["flip_loc"]
        officeRideType <- map["office_ride_type"]
    }
}
