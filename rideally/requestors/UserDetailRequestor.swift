//
//  UserDetailRequestor.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/28/16.
//  Copyright © 2016 rideally. All rights reserved.
//


import UIKit
import ObjectMapper

class UserDetailRequest: BaseModel {
    var user_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        user_id <- map["user_id"]
    }
}

class UserDetailResponse: BaseModel
{
    var dataObj: [UserDetailData]?
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class UserDetailData: BaseModel {
    var unreadNotification : String?
    var allRides: String?
    var allWorkplaces: String?
    var allRoutes: String?
    var offered: String?
    var needed: String?
    var joined: String?
    var booked: String?
    var sent: String?
    var received: String?
    var mygroup: String?
    var myroute: String?
    var myevent: String?
    var mytrip: String?
    var myWorkplace: String?
    var myPendingWorkplace: String?
    var mygroup_ride: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        unreadNotification <- map["unread_notification"]
        allRides <- map ["allrides"]
        allWorkplaces <- map ["allworkplace"]
        allRoutes <- map ["allroute"]
        offered <- map["offered"]
        needed <- map["needed"]
        joined <- map["joined"]
        booked <- map["booked"]
        sent <- map["sent"]
        received <- map["received"]
        mygroup <- map["mygroup"]
        myroute <- map["myroute"]
        myevent <- map["myevent"]
        mytrip <- map["mytrip"]
        myWorkplace <- map ["myworkplace"]
        myPendingWorkplace <- map ["mypendingworkplace"]
        mygroup_ride <- map["mygroup_ride"]
    }
}


class UserDetailRequestor: BaseRequestor {
    
    func getUserTotalDetail(_ requestObj: UserDetailRequest, success: @escaping NetworkCompletionHandler, failure: NetworkFailureHandler){
        
        let parameters = Mapper<UserDetailRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.GET_USERDETAIL, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: UserDetailResponse = Mapper<UserDetailResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            
        }
    }
}
