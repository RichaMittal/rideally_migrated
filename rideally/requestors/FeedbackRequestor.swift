//
//  FeedbackRequestor.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/20/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class FeedbackRequest: BaseModel {
    var user_id: String?
    var name: String?
    var email: String?
    var subject: String?
    var feedbackMsg: String?
    var rating: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        user_id <- map["user_id"]
        name <- map["name"]
        email <- map["email"]
        subject <- map["subject"]
        feedbackMsg <- map["message"]
        rating <- map["rating"]
    }
}

class FeedbackResponse: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}


class FeedbackRequestor: BaseRequestor {
    
    func sendFeedbackRequest(_ requestObj: FeedbackRequest, success: @escaping NetworkCompletionHandler, failure: NetworkFailureHandler){
        
        let parameters = Mapper<FeedbackRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.SEND_FEEDBACK, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: FeedbackResponse = Mapper<FeedbackResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            
        }
    }
    
}

