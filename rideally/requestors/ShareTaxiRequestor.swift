//
//  ShareTaxiRequestor.swift
//  rideally
//
//  Created by Sarav on 22/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class ShareTaxiRequest: BaseModel{
    //user_id = 6007
    //from_location = get from textfield
    //from_lat = get from ""From"" location (google places)
    //from_lon = get from ""From"" location (google places)
    //to_location = get from textfield
    //to_lat = get from ""To"" location (google places)
    //to_lon = get from ""To"" location (google places)
    //goingtime = 09:00 AM (12 hr format)
    //returntime = 05:00 PM (12 hr format)
    //request_plantype = (8 or 9 or 10) Plan Id (value of the subscription selected, get from SERVICE URL//routes/getplandetail service)
    //home_location = get from session
    //work_place = get from session, if not then blank
    //request_type = Options available shoud be (A - (ANY) or M (when user Gender - Male)) or (A or F (when user Gender - FEMALE))
    //cmnt  = get from textfield"

    var userId: String?
    var fromLocation: String?
    var fromLat: String?
    var fromLong: String?
    var toLocation: String?
    var toLat: String?
    var toLong: String?
    
    var goingTime: String?
    var returnTime: String?
    var planType: String?
    var homeLocation: String = ""
    var workLocation: String = ""
    var requestType: String?
    var comment: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["user_id"]
        fromLocation <- map["from_location"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_lon"]
        toLocation <- map["to_location"]
        toLat <- map["to_lat"]
        toLong <- map["to_lon"]
        
        goingTime <- map["goingtime"]
        returnTime <- map["returntime"]
        planType <- map["request_plantype"]
        homeLocation <- map["home_location"]
        workLocation <- map["work_place"]
        requestType <- map["request_type"]
        comment <- map["cmnt"]
    }

}

class ShareTaxiResponse : BaseModel{
    
}

class SubscriptionResponse : BaseModel{
    var list: [SubscriptionItem]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        list <- map["data"]
    }
}

class SubscriptionItem: BaseModel{
    var cost: String?
    var vehicleType: String?
    var members: String?
    var planId: String?
    var planType: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        cost <- map["cost"]
        vehicleType <- map["vehicle_type"]
        members <- map["members"]
        planId <- map["plan_id"]
        planType <- map["plan_type"]
    }
}

class ShareTaxiRequestor: BaseRequestor {

    func getSubscriptions(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        makePOSTRequestWithparameters(URLS.GET_SUBSCRIPTIONS, parameters: nil, encoding: .default, success: { (result) in
            let response: SubscriptionResponse = Mapper<SubscriptionResponse>().map(JSON: result as! [String : Any])!
            if(response.list != nil){
                success(true, response)
            }
            else{
                success(false, response)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func createShareTaxi(_ requestObj: ShareTaxiRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<ShareTaxiRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.CREATE_SHARE_A_TAXI, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: ShareTaxiResponse = Mapper<ShareTaxiResponse>().map(JSON: result as! [String : Any])!
            if(response.code == "1071"){
                success(true, "Thanks for submitting your request. Our customer care will contact you within 2 working days for further process.")
            }
            else{
                success(false, response)
            }
        }) { (error) in
            failure(error)
        }
    }
}


//user_id = 6007
//from_location = get from textfield
//from_lat = get from ""From"" location (google places)
//from_lon = get from ""From"" location (google places)
//to_location = get from textfield
//to_lat = get from ""To"" location (google places)
//to_lon = get from ""To"" location (google places)
//goingtime = 09:00 AM (12 hr format)
//returntime = 05:00 PM (12 hr format)
//request_plantype = (8 or 9 or 10) Plan Id (value of the subscription selected, get from SERVICE URL//routes/getplandetail service)
//home_location = get from session
//work_place = get from session, if not then blank
//request_type = Options available shoud be (A - (ANY) or M (when user Gender - Male)) or (A or F (when user Gender - FEMALE))
//cmnt  = get from textfield"

//"{""code"":""1071"",""message"":""Success"",""data"":[{""request_id"":""677""}]}
//Message: ""Thanks for submitting your request. Our customer care will contact you within 2 working days for further process."""


//"{""code"":""1119"",""message"":""subscription details"",""data"":[{""plan_id"":""8"",""plan_type"":""weekly"",""cost"":""7.00"",""description"":"""",""vehicle_type"":""Sedan
//A\/c"",""vehicle_seats"":null,""status"":""2"",""members"":""4"",""additional_cst"":""7.50"",""created_date"":""0000-00-00 00:00:00"",""updated_date"":""0000-00-00 00:00:00""},
//{""plan_id"":""9"",""plan_type"":""weekly"",""cost"":""8.50"",""description"":"""",""vehicle_type"":""Sedan A\/c"",""vehicle_seats"":null,""status"":""2"",""members"":""3"",
//    ""additional_cst"":""8.50"",""created_date"":""0000-00-00 00:00:00"",""updated_date"":""0000-
//    00-00 00:00:00""},{""plan_id"":""10"",""plan_type"":""weekly"",""cost"":""11.00"",""description"":"""",""vehicle_type"":""Sedan A\/c"",
//        ""vehicle_seats"":null,""status"":""2"",""members"":""2"",""additional_cst"":""11.50"",""created_date"":""0000-00-00
//        00:00:00"",""updated_date"":""0000-00-00 00:00:00""}]}"
