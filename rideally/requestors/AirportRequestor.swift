//
//  AirportRequestor.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/28/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class AirportRequest: BaseModel{
    var userId: String?
    var rideDate: String?
    var rideTime: String?
    var fromLocation: String?
    var fromLat: String?
    var fromLong: String?
    var toLocation: String?
    var toLat: String?
    var toLong: String?
    var pool_shared: String?
    var ridecoment: String?
    var veh_type: String?
    var veh_seat: String?
    var distance: String?
    var duration: String?
    var final_cost: String?
    var main_cost: String?
    var base_cost: String?
    
    var poolType: String?
    var ridetype = "N"
    var pool_scope = "PUBLIC"
    var pool_category = "2"
    var pool_taxi_booking = "1"
    var min_distance = ""
    var fixed_cost = "0"
    var route_id = ""
    var pay_to = "1"
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["user_id"]
        rideDate <- map["ridestartdate"]
        rideTime <- map["ridestarttime"]
        fromLocation <- map["from_location"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_lon"]
        toLocation <- map["to_location"]
        toLat <- map["to_lat"]
        toLong <- map["to_lon"]
        poolType <- map["ride_pool_type"]
        
        ridetype <- map["ride_type"]
        pool_shared <- map["ride_shared"]
        pool_scope <- map["pool_scope"]
        pool_category <- map["ride_category"]
        ridecoment <- map["ridecoment"]
        pool_taxi_booking <- map["pool_taxi_booking"]
        veh_seat <- map["veh_seat"]
        min_distance <- map["min_distance"]
        final_cost <- map["final_cost"]
        main_cost <- map["main_cost"]
        base_cost <- map["base_cost"]
        fixed_cost <- map["fixed_cost"]
        veh_type <- map["veh_type"]
        route_id <- map["route_id"]
        distance <- map["distance"]
        duration <- map["duration"]
        pay_to <- map["pay_to"]
    }
}

class AirportResponse : BaseModel
{
    var data: [AirportDataResponse]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        data <- map["data"]
    }
}

class AirportDataResponse : BaseModel
{
    var poolID: String?
    var distance: String?
    var duration: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        poolID <- map["pool_id"]
        distance <- map["distance"]
        duration <- map["duration"]
    }
}

class couponExistsResponse: BaseModel {
    var dataObj: [couponExistsData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class couponExistsData: BaseModel {
    var coupon_id: String?
    var coupon_value: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        coupon_id <- map["coupon_id"]
        coupon_value <- map["coupon_value"]
    }
}

class couponUsageResponse: BaseModel {
    //var dataObj: [couponUsageData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        //dataObj <- map["data"]
    }
}

class applyCouponRequest: BaseModel {
    var user_id: String?
    var pool_id: String?
    var device_id: String?
    var coupon_code: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        user_id <- map["user_id"]
        pool_id <- map["pool_id"]
        device_id <- map["device_id"]
        coupon_code <- map["coupon_code"]
    }
}

class applyCouponResponse: BaseModel {
    var dataObj: [applyCouponData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class applyCouponData: BaseModel {
    var final_cost: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        final_cost <- map["cost"]
    }
}

class AirportRequestor: BaseRequestor {
    func createAirportRide(_ requestObj: AirportRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<AirportRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.CREATE_TAXIRIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let responseObj: AirportResponse = Mapper<AirportResponse>().map(JSON: result as! [String : Any])!
            if(responseObj.code == "171"){
                success(true, responseObj)
            }else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getDefaultLocation(_ requestObj: GetDefaultLocationRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<GetDefaultLocationRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.GET_DEFAULT_LOC, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: GetDefaultLocationResponse = Mapper<GetDefaultLocationResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
        }) { (error) in
            failure(error)
        }
    }
    
    func getPriceEstimationList(_ requestObj: GetPriceListRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<GetPriceListRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.GET_PRICE_LIST, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: GetPriceListResponse = Mapper<GetPriceListResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
        }) { (error) in
            failure(error)
        }
    }
    
    func checkCouponExists(_ couponCode: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["coupon_code": couponCode]
        
        makePOSTRequestWithparameters(URLS.CHECK_COUPON_EXISTS, parameters: params as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: couponExistsResponse = Mapper<couponExistsResponse>().map(JSON: result as! [String : Any])!
            if(response.code == "15191") {
                success(true, response)
            } else {
                success(false, response.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func checkCouponUsage(_ couponId: String, deviceId: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["coupon_id": couponId, "user_id": UserInfo.sharedInstance.userID, "device_id": deviceId]
        
        makePOSTRequestWithparameters(URLS.CHECK_COUPON_USAGE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: couponUsageResponse = Mapper<couponUsageResponse>().map(JSON: result as! [String : Any])!
            if(response.code == "16181") {
                success(true, response)
            } else {
                success(false, response.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func applyCouponCode(_ requestObj: applyCouponRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<applyCouponRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.APPLY_COUPON, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            let response: applyCouponResponse = Mapper<applyCouponResponse>().map(JSON: result as! [String : Any])!
            if(response.code == "15181") {
                success(true, response)
            } else {
                success(false, response.message)
            }
        }) { (error) in
            failure(error)
        }
    }
}
class GetDefaultLocationRequest: BaseModel {
    var loc_id: String?
    var offer_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        loc_id <- map["loc_id"]
        offer_id <- map["offer_id"]
    }
}

class GetDefaultLocationResponse: BaseModel
{
    var dataObj: [GetDefaultLocationData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class GetDefaultLocationData: BaseModel {
    var locName: String?
    var locLat: String?
    var locLong: String?
    override func mapping(map: Map) {
        super.mapping(map: map)
        locName <- map["location_name"]
        locLat <- map["latitude"]
        locLong <- map["longitude"]
    }
}

class GetPriceListRequest: BaseModel {
    var source: String?
    var destination: String?
    var distance: String?
    var duration: String?
    var date: String?
    var time: String?
    var offer_id = "2"
    var taxi_type: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        source <- map["source"]
        destination <- map["destination"]
        distance <- map["distance"]
        duration <- map["duration"]
        date <- map["date"]
        time <- map["time"]
        offer_id <- map["offer_id"]
        taxi_type <- map["taxi_type"]
    }
}

class GetPriceListResponse: BaseModel
{
    var dataObj: [GetPriceListData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class GetPriceListData: BaseModel
{
    var reducedRideDistance: Int?
    var rideDistance: Int?
    var discountPrivate: Int?
    var discountAny: String?
    var discountFemale: String?
    var discountMale: String?
    var vehiclesObj: [GetPriceListVehicles]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        reducedRideDistance <- map["reduced_ride_distance"]
        rideDistance <- map["ride_distance"]
        discountPrivate <- map["discount_private"]
        discountAny <- map["discount_shared_any"]
        discountFemale <- map["discount_shared_female"]
        discountMale <- map["discount_shared_male"]
        vehiclesObj <- map["vehicles"]
    }
}

class GetPriceListVehicles: BaseModel {
    var vehicleType: String?
    var vehicleTypeId: String?
    var seatCapacity: String?
    var priceMain: Int?
     var priceAny: Int?
     var priceFemale: Int?
     var priceMale: Int?
    var nightPrice: String?
    var basePrice: String?
    var pricePrivate: Int?
    var tollPrice: String?
    var vehTypeId: String?
    var fixedPrice: Int?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        vehicleType <- map["vehicle_type"]
        vehicleTypeId <- map["vehicle_type_id"]
        seatCapacity <- map["seat_capacity"]
        priceMain <- map["price_main"]
        priceAny <- map["price_shared_any"]
        priceFemale <- map["price_shared_female"]
        priceMale <- map["price_shared_male"]
        nightPrice <- map["night_price"]
        basePrice <- map["base_price"]
        pricePrivate <- map["price_private"]
        tollPrice <- map["toll_price"]
        vehTypeId <- map["vehicle_type_id"]
        fixedPrice <- map["txvt_fixed_price"]
    }
}
