//
//  OffersPromotionsRequestor.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 2/20/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class OffersPromotionsRequest: BaseModel {
    var user_id: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        user_id <- map["user_id"]
    }
}

class OffersPromotionsResponse: BaseModel
{
    var dataObj: [OffersPromotionsData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class OffersPromotionsData: BaseModel {
    var couponImgUrl : String?
    var couponCode : String?
    var couponDescription : String?
    var couponExpiryDate: String?
    var couponId: String?
    override func mapping(map: Map) {
        super.mapping(map: map)
        couponImgUrl <- map["coupon_img_url"]
        couponCode <- map["coupon_code"]
        couponDescription <- map["description"]
        couponExpiryDate <- map["expire_date"]
        couponExpiryDate <- map["expire_date"]
    }
}

class OffersPromotionsRequestor: BaseRequestor {
    
    func getOffersPromotions(_ requestObj: OffersPromotionsRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<OffersPromotionsRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.GET_OFFERS_PROMOTIONS, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: OffersPromotionsResponse = Mapper<OffersPromotionsResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    
}
