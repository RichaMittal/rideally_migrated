//
//  EmergencyContactRequestor.swift
//  rideally
//
//  Created by Raghunathan on 10/19/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper


class GetEmergencyContactRequest: BaseModel
{
    var userID: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        userID <- map["user_id"]
    }
}

class DeleteEmergencyContactRequest: BaseModel
{
    var userID: String?
    var mobileNumber: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        userID <- map["user_id"]
        mobileNumber <- map["mobile_number"]
    }
}

class DelteEmergencyContactResponse: BaseModel
{
    var data: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class GetEmergencyContactResponse: BaseModel
{
    var dataObj: [ContactData]?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}


class ContactData: BaseModel
{
    var firstPersonName: String?
    var firstPersonNumber: String?
    var firstPersonEmail: String?
    var secondPersonName : String?
    var secondPersonNumber : String?
    var secondPersonEmail: String?
    var defaultPerson: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        firstPersonName <- map["person_first_name"]
        firstPersonNumber <- map["sos_number"]
        firstPersonEmail <- map["sos_mail"]
        secondPersonName <- map["person_second_name"]
        secondPersonNumber <- map["sos_second_no"]
        secondPersonEmail <- map["sos_second_mail"]
        defaultPerson <- map["sos_default_person"]
    }
}



class SaveEmergencyContactRequest: BaseModel
{
    var userID: String?
    var firstPersonName: String?
    var firstPersonNumber: String?
    var firstPersonEmail: String?
    var secondPersonName : String?
    var secondPersonNumber : String?
    var secondPersonEmail: String?
    var defaultPerson: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        userID <- map["user_id"]
        firstPersonName <- map["person_first_name"]
        firstPersonNumber <- map["sos_mobile"]
        firstPersonEmail <- map["sos_mail"]
        secondPersonName <- map["person_second_name"]
        secondPersonNumber <- map["sos_second_no"]
        secondPersonEmail <- map["sos_second_mail"]
        defaultPerson <- map["sos_default_person"]
   
    }
}

class SaveEmergencyContactResponse: BaseModel
{
    override func mapping(map: Map)
    {
        super.mapping(map: map)
    }
}



class EmergencyContactRequestor: BaseRequestor
{
    
    func sendGetContactListRequest(_ requestObj: GetEmergencyContactRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<GetEmergencyContactRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.GETCONTACTLIST, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: GetEmergencyContactResponse = Mapper<GetEmergencyContactResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    func sendSaveContactListRequest(_ requestObj: SaveEmergencyContactRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<SaveEmergencyContactRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.SAVECONTACTLIST, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: SaveEmergencyContactResponse = Mapper<SaveEmergencyContactResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    func deleteContactListRequest(_ requestObj: DeleteEmergencyContactRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<DeleteEmergencyContactRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.DELETECONTACTLIST, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: DelteEmergencyContactResponse = Mapper<DelteEmergencyContactResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
}
