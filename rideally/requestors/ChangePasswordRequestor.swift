//
//  ChangePasswordRequestor.swift
//  rideally
//
//  Created by Raghunathan on 8/9/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

/*
 {"code":"460","message":"Password Change Successfull.","data":[]}
 {"code":"461","message":"Password Change Failed. Old Password Is Invalid.","data":[]}
 {"code":"462","message":"Both Passwords Are Not Identical.","data":[]}
 {"code":"463","message":"Invalid Password Length.","data":[]}
 {"code":"464","message":"Password Change Failed.","data":[]}
 */


class ChangePasswordRequest: BaseModel {
    var userID: String?
    var oldPassword: String?
    var newPassword: String?
    var confirmPassword: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        userID <- map["user_id"]
        oldPassword <- map["opassword"]
        newPassword <- map["npassword"]
        confirmPassword <- map["cpassword"]
    }
}

class ChangePasswordResponse: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}


class ChangePasswordRequestor: BaseRequestor {

    func sendChangePasswordRequest(_ requestObj: ChangePasswordRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<ChangePasswordRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.CHANGEPWD, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: ChangePasswordResponse = Mapper<ChangePasswordResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }

}
