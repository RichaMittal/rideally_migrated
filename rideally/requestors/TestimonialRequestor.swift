//
//  TestimonialRequestor.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/17/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class TestimonialRequest: BaseModel {
    //var page_key: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        //page_key <- map["page_key"]
    }
}

class TestimonialResponse: BaseModel
{
    var dataObj: TestimonialList?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class TestimonialList: BaseModel {
    var dataList: [TestimonialData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        dataList <- map["list"]
    }
}

class TestimonialData: BaseModel {
    var ra_tm_by : String?
    var ra_tm_detail : String?
    var ra_tm_rating : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        ra_tm_by <- map["ra_tm_by"]
        ra_tm_detail <- map["ra_tm_detail"]
        ra_tm_rating <- map["ra_tm_rating"]
    }
}

class TestimonialRequestor: BaseRequestor {
    
    func sendTestimonialRequest(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        //let parameters = Mapper<StaticPageRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.GET_TESTIMONIALS, parameters: nil, encoding: .default, success: { (result) in
            
            let response: TestimonialResponse = Mapper<TestimonialResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    
}
