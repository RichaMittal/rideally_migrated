//
//  WorkplaceRequestor.swift
//  rideally
//
//  Created by Sarav on 13/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class AddWPRequest : BaseModel{
    var userID: String?
    var officeName: String?
    var officeLocation: String?
    var officeLat: String?
    var officeLong: String?
    var officeKey: String?
    var officeDesc: String?
    var officeScope: String?
    var officeGroupType = "6"
    var emailID: String?
    var groupCategory: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        officeName <- map["office_name"]
        officeLocation <- map["office_loc"]
        officeLat <- map["office_lat"]
        officeLong <- map["office_long"]
        officeKey <- map["office_key"]
        officeDesc <- map["office_desc"]
        officeScope <- map["office_scope"]
        officeGroupType <- map["office_group_type"]
        emailID <- map["wp_email_id"]
        groupCategory <- map["group_category"]
    }
}

class UpdateWPRequest: BaseModel {
    var userID: String?
    var officeName: String?
    var officeID: String?
    var officeDesc: String?
    var officeScope: String?
    var officeType = "6"
    var officeKey: String?
    var fileTempGroup = ""
    var fileGroupExt = ""
    var officeUrlKey = ""
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        officeName <- map["office_name"]
        officeID <- map["office_id"]
        officeDesc <- map["office_desc"]
        officeScope <- map["office_scope"]
        officeType <- map["office_type"]
        officeKey <- map["office_key"]
        fileTempGroup <- map["file_temp_group"]
        fileGroupExt <- map["file_group_ext"]
        officeUrlKey <- map["office_url_key"]
    }
}

class WPJoinResponse : BaseModel{
    var data: userDetailCountArray?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class userDetailCountArray : BaseModel{
    var detailCount: userDetailCount?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        detailCount <- map["detailCount"]
    }
}

class userDetailCount : BaseModel{
    var unreadNotification : String?
    var allRides: String?
    var allWorkplaces: String?
    var allRoutes: String?
    var offered: String?
    var needed: String?
    var joined: String?
    var booked: String?
    var sent: String?
    var received: String?
    var mygroup: String?
    var myroute: String?
    var myevent: String?
    var mytrip: String?
    var myWorkplace: String?
    var myPendingWorkplace: String?
    var mygroup_ride: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        unreadNotification <- map["unread_notification"]
        allRides <- map ["allrides"]
        allWorkplaces <- map ["allworkplace"]
        allRoutes <- map ["allroute"]
        offered <- map["offered"]
        needed <- map["needed"]
        joined <- map["joined"]
        booked <- map["booked"]
        sent <- map["sent"]
        received <- map["received"]
        mygroup <- map["mygroup"]
        myroute <- map["myroute"]
        myevent <- map["myevent"]
        mytrip <- map["mytrip"]
        myWorkplace <- map ["myworkplace"]
        myPendingWorkplace <- map ["mypendingworkplace"]
        mygroup_ride <- map["mygroup_ride"]
    }
}

class WPSearchResponse : BaseModel{
    var data: [WorkPlace]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}


class WorkPlacesResponse : BaseModel{
    var data: WPData?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class WPData : BaseModel{
    var allOfcGroups: [WorkPlace]?
    var myOfcGroups: [WorkPlace]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        allOfcGroups <- map["all_office_groups"]
        myOfcGroups <- map["my_office_groups"]
    }
}

class WorkPlace : BaseModel{
    var wpName: String?
    var wpID: String?
    var wpLocation: String?
    var wpLat: String?
    var wpLong: String?
    var wpStatus: String?
    var wpMembers: String?
    var wpLogoURL: String?
    var wpDesc: String?
    
    var wpInitiator: String?
    var wpMembershipStatus: String?
    
    var wpScope: String?
    var wpTieUpStatus: String?
    var groupKey: String?
    var wpInvitedMembersCount: String?
    var wpPendingMembersCount: String?
    
    var wpDetailLocation: String?
    var wpDetailLat: String?
    var wpDetailLong: String?
    var wpDetailMembershipStatus: String?
    var wpDetailMembershipType: String?
    
    var wpSearchedName: String?
    var wpSearchedLocation: String?
    var wpSearchedLat: String?
    var wpSearchedLong: String?
    var wpInsuranceStatus :String?
    var wpHideAnywhere: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        wpName <- map["group_name"]
        wpDesc <- map["group_description"]
        wpID <- map["group_id"]
        wpStatus <- map["group_status"]
        wpLocation <- map["office_loc"]
        wpLat <- map["office_lat"]
        wpLong <- map["office_long"]
        wpMembers <- map["group_total_members"]
        wpLogoURL <- map["group_logo_url"]
        wpMembershipStatus <- map["agmship_status"]
        wpInitiator <- map["group_initiator"]
        wpScope <- map["group_scope"]
        wpTieUpStatus <- map["tieup_status"]
        groupKey <- map["group_key"]
        wpInvitedMembersCount <- map["group_invited_members"]
        wpPendingMembersCount <- map["group_pending_members"]
        wpInsuranceStatus <- map["insurance_status"]
        
        wpDetailLocation <- map["src_location"]
        wpDetailLat <- map["src_lat"]
        wpDetailLong <- map["src_lng"]
        wpDetailMembershipStatus <- map["gmship_status"]
        wpDetailMembershipType <- map["gmship_type"]
        
        wpSearchedName <- map["name"]
        wpSearchedLocation <- map["location"]
        wpSearchedLat <- map["latitude"]
        wpSearchedLong <- map["longitude"]
        wpHideAnywhere <- map["hide_anywhere"]
    }
}

class WPNewRideRequest: BaseModel {
    var userId: String?
    var fromLocation: String?
    var fromLat: String?
    var fromLong: String?
    var toLocation: String?
    var toLat: String?
    var toLong: String?
    var distance = ""
    var duration = ""
    var poolType: String?
    
    var selected_dates: String?
    var pool_return_date: String?
    var goingtime: String?
    var comingtime: String?
    var ridetype: String?
    var repeat_ride: String?
    var return_on = "0"
    var vehregno: String?
    var veh_capacity: String?
    var pool_shared: String?
    var cost_type = "PK"
    var cost: String?
    var pool_scope: String?
    var pool_category = "2"
    
    // TODO: New Added Params Need to verifi other apis to not affects.
    var created_from: String?
    var ridemessage: String?
    var via : String?
    var waypoints: String?
    var flip  : String?
    
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userId <- map["user_id"]
        fromLocation <- map["from_location"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_lon"]
        toLocation <- map["to_location"]
        toLat <- map["to_lat"]
        toLong <- map["to_lon"]
        distance <- map["distance"]
        duration <- map["duration"]
        poolType <- map["pooltype"]
        
        selected_dates <- map["selected_dates"]
        pool_return_date <- map["pool_return_date"]
        goingtime <- map["goingtime"]
        comingtime <- map["comingtime"]
        ridetype <- map["ridetype"]
        repeat_ride <- map["repeat_ride"]
        return_on <- map["return_on"]
        vehregno <- map["vehregno"]
        veh_capacity <- map["veh_capacity"]
        pool_shared <- map["pool_shared"]
        cost_type <- map["cost_type"]
        cost <- map["cost"]
        pool_scope <- map["pool_scope"]
        pool_category <- map["pool_category"]
        
        created_from <- map["created_from"]
        ridemessage <- map["ridemessage"]
        via <- map["via"]
        waypoints <- map["waypoints"]
        flip <- map["flip"]
        
    }
}

class WPSearchRequest: BaseModel {
    var wpName: String?
    var wpLocation: String?
    var wpLat: String?
    var wpLong: String?
    var userId: String?
    var category: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        wpName <- map["office_name"]
        wpLocation <- map["office_loc"]
        wpLat <- map["office_lat"]
        wpLong <- map["office_long"]
        userId <- map["user_id"]
        category <- map["category"]
    }
}

class WorkplaceRequestor: BaseRequestor {
    
    
    func addWPRide(_ requestObj: WPNewRideRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<WPNewRideRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.ADD_WORKPLACE_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: NeedRideResponse = Mapper<NeedRideResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1800"){
                success(true, responseObj)
            }else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func searchWorkplace(_ requestObj: WPSearchRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<WPSearchRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.SEARCH_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: WPSearchResponse = Mapper<WPSearchResponse>().map(JSON: response as! [String : Any])!
            if((responseObj.code == "1400") || (responseObj.code == "1401") || (responseObj.code == "1413")) {
                success(true, responseObj)
            }else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func verifyEmail(_ email: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["domain_email": email]
        makePOSTRequestWithparameters(URLS.VERIFY_WP_EMAIL, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1454"){
                success(true, responseObj.message)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func addWorkPlace(_ requestObj: AddWPRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<AddWPRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.ADD_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1411"){
                success(true, "Thank you for requesting your workplace. You would need to verify your corporate email-id to activate the workplace and share rides with colleagues.")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func updateWorkPlace(_ requestObj: UpdateWPRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<UpdateWPRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.UPDATE_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1421"){
                success(true, "")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
    func getMyWorkplaces(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id": UserInfo.sharedInstance.userID]
        makePOSTRequestWithparameters(URLS.GET_MY_WORKPLACES, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: WorkPlacesResponse = Mapper<WorkPlacesResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1400"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func getAllWorkPlace(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id": UserInfo.sharedInstance.userID]
        makePOSTRequestWithparameters(URLS.GET_ALL_WORKPLACES, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: WorkPlacesResponse = Mapper<WorkPlacesResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1900"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func addSecEmail(_ userID: String, email: String, groupKey: String, groupID: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id": userID, "sec_email":email, "group_key":groupKey, "group_id":groupID]
        makePOSTRequestWithparameters(URLS.ADD_SEC_EMAIL, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "2001"){
                success(true, responseObj.message)
            } else {
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func joinWorkPlace(_ groupID: String, groupScope: String, userMsg: String, tieupWpUser: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        var parameters = ["user_id": UserInfo.sharedInstance.userID, "group_id": groupID]
        if(userMsg.characters.count > 0){
            parameters["user_message"] = userMsg
        }
        if(tieupWpUser.characters.count > 0){
            parameters["tieup_wpuser"] = tieupWpUser
        }
        makePOSTRequestWithparameters(URLS.JOIN_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: WPJoinResponse = Mapper<WPJoinResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1271"){
                if(groupScope == "0" || tieupWpUser.characters.count > 0) {
                    success(true, "Thank you, You have successfully joined in this workplace.")
                } else {
                    success(true, "Join group request sent successfully.")
                }
                if(responseObj.data?.detailCount?.myWorkplace != nil && responseObj.data?.detailCount?.myWorkplace != "") {
                    UserInfo.sharedInstance.myWorkplaces = (responseObj.data?.detailCount?.myWorkplace)!
                }
                if(responseObj.data?.detailCount?.myPendingWorkplace != nil && responseObj.data?.detailCount?.myPendingWorkplace != "") {
                    UserInfo.sharedInstance.myPendingWorkplaces = (responseObj.data?.detailCount?.myPendingWorkplace)!
                }
            }else if(responseObj.code == "1274"){
                success(true, "Request sent already.")
            }else if(responseObj.code == "1275"){
                success(true, "Already a member of this workplace.")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    func unjoinWorkPlace(_ groupID: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id": UserInfo.sharedInstance.userID, "group_id": groupID]
        makePOSTRequestWithparameters(URLS.UNJOIN_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1291"){
                success(true, "UnJoin workplace request sent successfully.")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    func cancelJoinWorkPlace(_ groupID: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id": UserInfo.sharedInstance.userID, "group_id": groupID]
        makePOSTRequestWithparameters(URLS.CANCEL_JOIN_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1301"){
                success(true, "Cancelled request to join workplace successfully.")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func addAdminToWP(_ groupID: String, userIDs: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_ids": userIDs, "group_id": groupID, "admin_user_id":UserInfo.sharedInstance.userID]
        
        makePOSTRequestWithparameters(URLS.ADD_WORKPLACE_ADMIN, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1351"){
                success(true, "Request to make user as admin has been sent successfully.")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    func removeFromWP(_ groupID: String, userID: String, adminMsg: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id": userID, "group_id": groupID, "auth_user_id":UserInfo.sharedInstance.userID, "admin_message":adminMsg]
        
        makePOSTRequestWithparameters(URLS.REMOVE_FROM_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1321"){
                success(true, "Request to remove user from workplace has been sent successfully.")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func respondToJoinWP(_ groupID: String, userID: String, adminMsg: String, status: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id": userID, "gmship_id": groupID, "gmship_status":status, "admin_message":adminMsg]
        
        makePOSTRequestWithparameters(URLS.RESPOND_JOIN_TO_WORKPLACE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1261"){
                success(true, "Request to remove user from workplace has been sent successfully.")
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    
    func getWPMembers(_ poolId: String, groupID: String, distanceRange: String, homeLat: String, homeLon: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["pool_id": poolId, "user_id":UserInfo.sharedInstance.userID, "group_id":groupID, "distance_range":distanceRange, "latitude": homeLat, "longitude":homeLon]
        makePOSTRequestWithparameters(URLS.GET_WORKPLACE_MEMBERS, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: WPMembersResponse = Mapper<WPMembersResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj.data)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getWPRides(_ reqDate: String, reqTime: String, groupID: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["searchType": "generic", "userId":UserInfo.sharedInstance.userID, "rideScope":groupID, "group_type":"6", "records":"250", "pooltype":UserInfo.sharedInstance.gender == "M" ? "AM" : "AF", "reqTime":reqTime, "reqDate":reqDate]
        
        makePOSTRequestWithparameters(URLS.GET_WORKPLACE_RIDES, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: WPRidesResponse = Mapper<WPRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getWPDomains(_ groupID: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let params = ["group_id":groupID]
        
        makePOSTRequestWithparameters(URLS.GET_WORKPLACE_DOMAINS, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: WPDomainsResponse = Mapper<WPDomainsResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "14033"){
                success(true, responseObj.data)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func sendWPInvite(_ groupId: String, userId: String,  emailIDs: String?, mobileNOs: String?, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        var params = ["group_id": groupId, "user_id":userId]
        if let email = emailIDs{
            params["search_string"] = email
        }
        if let mobile = mobileNOs{
            params["mobile_string"] = mobile
        }
        
        makePOSTRequestWithparameters(URLS.INVITE_TO_WORKPLACE, parameters: params as [String : AnyObject]?, encoding: .default, success: { (response) in
            
            let responseObj: InviteMembersWPResponse = Mapper<InviteMembersWPResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1311"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
            
        }) { (error) in
            
            failure(error)
        }
    }
    
    func searchWPRides(_ requestObj: SearchRideRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<SearchRideRequest>().toJSON(requestObj)
        
        makePOSTRequestWithparameters(URLS.ALL_RIDES, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: WPRidesResponse = Mapper<WPRidesResponse>().map(JSON: response as! [String : Any])!
            success(true, responseObj)
        }) { (error) in
            failure(error)
        }
    }
    
    func getLastRide(_ success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["user_id":UserInfo.sharedInstance.userID]
        
        makePOSTRequestWithparameters(URLS.GET_USER_LAST_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: LastRideResponse = Mapper<LastRideResponse>().map(JSON: response as! [String : Any])!
            if((responseObj.data?.count)! > 0){
                success(true, responseObj)
            }
            else{
                success(false, responseObj)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func startRideByDriver(_ requestObj: TripRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        //requestObj.poolOwnerType = ""
        
        let parameters = Mapper<TripRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.DRIVER_START_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "18384"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func startRideByRider(_ requestObj: TripRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<TripRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.RIDER_START_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "18385"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func stopRideByDriver(_ requestObj: TripRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        //requestObj.poolOwnerType = "admin"
        
        let parameters = Mapper<TripRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.DRIVER_STOP_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "18386"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func stopRideByRider(_ requestObj: TripRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = Mapper<TripRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.RIDER_STOP_RIDE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: BaseModel = Mapper<BaseModel>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "18386"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
    
    func wpSearchByLike(_ officeName: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        let parameters = ["office_name": officeName]
        makePOSTRequestWithparameters(URLS.WPSEARCH_BYLIKE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (response) in
            let responseObj: WPSearchResponse = Mapper<WPSearchResponse>().map(JSON: response as! [String : Any])!
            if(responseObj.code == "1900"){
                success(true, responseObj)
            }else{
                success(false, responseObj.message)
            }
        }) { (error) in
            failure(error)
        }
    }
}

class TripRequest: BaseModel {
    var userID: String?
    var poolID: String?
    var fromLocation: String?
    var fromLat: String?
    var fromLong: String?
    var poolOwnerType: String?
    var toLocation: String?
    var toLat: String?
    var toLong: String?
    var startTime: String?
    
    var mobileNumber: String?
    var deviceID: String?
    var userName: String?
    
    var driverMobile: String?
    var driverUserID: String?
    var driverDeviceID: String?
    var userLocation: String?
    
    var distance: String?
    var duration: String?
    var points: String?
    
    var eta: String?
    
    var startLocation: String?
    var stopLocation: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        userID <- map["user_id"]
        poolID <- map["pool_id"]
        fromLocation <- map["from_location"]
        fromLat <- map["from_lat"]
        fromLong <- map["from_long"]
        poolOwnerType <- map["pool_owner_type"]
        toLocation <- map["to_location"]
        toLat <- map["to_lat"]
        toLong <- map["to_long"]
        startTime <- map["start_time"]
        
        mobileNumber <- map["mobile_number"]
        deviceID <- map["device_id"]
        userName <- map["user_name"]
        
        driverMobile <- map["driver_mob_no"]
        driverUserID <- map["driver_user_id"]
        driverDeviceID <- map["driver_device_id"]
        userLocation <- map["user_location"]
        
        distance <- map["distance"]
        duration <- map["duration"]
        points <- map["points"]
        
        eta <- map["eta"]
        startLocation <- map["StartLocation"]
        stopLocation <- map["StopLocation"]
    }
}

class InviteMembersWPResponse: BaseModel {
    var data: [InviteWPResult]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class InviteWPResult: BaseModel{
    var msgCode: String?
    var email: String?
    var mobile: String?
    var type: String?
    var result: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        msgCode <- map["resultcode"]
        email <- map["email"]
        mobile <- map["mobile"]
        type <- map["type"]
        result <- map["result"]
    }
}


class WPDomainsResponse : BaseModel
{
    var data: [WPDomainsResponseData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
class WPDomainsResponseData: BaseModel
{
    var domainName: String?
    override func mapping(map: Map) {
        super.mapping(map: map)
        domainName <- map["official_domain"]
    }
    
}

class WPMembersResponse: BaseModel {
    var data: WPMembersResponseData?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}
class WPMembersResponseData: BaseModel {
    var members: [WPMember]?
    var groupDetails: WorkPlace?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        members <- map["members"]
        groupDetails <- map["group"]
    }
}

class WPMember: BaseModel {
    var uID: String?
    var fname: String?
    var lname: String?
    var name: String?
    var email: String?
    var fbID: String?
    var membershipType: String?
    var userState: String?
    var mobile: String?
    var profilePicStatus: String?
    var profilePicUrl: String?
    var membershipStatus: String?
    var membershipId: String?
    var avgRating: String?
    var secEmailStatus: String?
    var homeLoc: String?
    var homeLat: String?
    var homeLon: String?
    var isActiveGoingUser: Int?
    var isActiveComingUser: Int?
    var memberDistance: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        uID <- map["user_id"]
        fname <- map["first_name"]
        lname <- map["last_name"]
        name <- map["name"]
        email <- map["email"]
        fbID <- map["fbId"]
        membershipId <- map["gmship_id"]
        membershipType <- map["gmship_type"]
        membershipStatus <- map["gmship_status"]
        userState <- map["user_state"]
        mobile <- map["mobile_number"]
        profilePicStatus <- map["profilepic_status"]
        profilePicUrl <- map["profilepic_url"]
        avgRating <- map["avg_rating"]
        secEmailStatus <- map["secondary_email_status"]
        homeLoc <- map["home_location"]
        homeLat <- map["home_lat"]
        homeLon <- map["home_lng"]
        isActiveGoingUser <- map["is_active_user"]
        isActiveComingUser <- map["is_active_coming_user"]
        memberDistance <- map["_distance"]
    }
}

class LastRideResponse: BaseModel {
    var data: [LastRideData]?
    override func mapping(map: Map) {
        super.mapping(map: map)
        data <- map["data"]
    }
}

class LastRideData: BaseModel {
    var vehRegNo: String?
    var rideType: String?
    var poolType: String?
    var capacity: String?
    var cost: String?
    var costType: String?
    var vehKind: String?
    var vehModel: String?
    var vehOwnerName: String?
    var vehSeqId: String?
    var vehType: String?
    var insurerName: String?
    var insuranceId: String?
    var insuredCmpnyName: String?
    var insuranceExpiryDate: String?
    var insuranceUrl: String?
    var rcUrl: String?
    var insuranceUploadStatus: String?
    var rcUploadStatus: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        vehKind <- map["vehicle_kind"]
        vehModel <- map["vehicle_model"]
        vehOwnerName <- map["vehicle_owner_name"]
        vehSeqId <- map["vehicle_seq_id"]
        vehType <- map["vehicle_type"]
        vehRegNo <- map["vehicle_registration_number"]
        capacity <- map["vehicle_capacity"]
        insurerName <- map["insurer_name"]
        insuranceId <- map["insurance_id"]
        insuredCmpnyName <- map["insured_company_name"]
        insuranceExpiryDate <- map["insurance_expiry_date"]
        insuranceUrl <- map["insurance_url"]
        rcUrl <- map["rc_url"]
        insuranceUploadStatus <- map["insurance_upload_status"]
        rcUploadStatus <- map["rc_upload_status"]
        rideType <- map["ride_type"]
        poolType <- map["pool_type"]
        cost <- map["cost"]
        costType <- map["cost_type"]
    }
}


class WPRidesResponse: BaseModel {
    var pools: WPRidesPools?
    override func mapping(map: Map) {
        super.mapping(map: map)
        pools <- map["pools"]
    }
}

class WPRidesPools: BaseModel {
    var officeGoing: [WPRide]?
    var homeGoing: [WPRide]?
    override func mapping(map: Map) {
        super.mapping(map: map)
        officeGoing <- map["office_going_rides"]
        homeGoing <- map["office_coming_rides"]
    }
}

class WPRide: Ride {
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        source <- map["src_loc_name"]
        destination <- map["dest_loc_name"]
        dateTime <- map["pool_sdate_time"]
        cost <- map["pool_cost"]
        userCost <- map["user_cost"]
        costType <- map["pool_cost_type"]
        members <- map["pool_joined_member"]
        seatsLeft <- map["pool_seat_left"]
        poolID <- map["pool_id"]
        
        poolTaxiBooking <- map["pool_taxi_booking"]
        //poolJoinedMembers <- map["pool_joined_members"]
        createdBy <- map["created_by"]
        poolStatus <- map["pool_status"]
        poolRideType <- map["pool_ride_type"]
        requestedStatus <- map["requested_status"]
        rideType <- map["ride_type"]
        
        ownerProfilePicStatus <- map["owner_profilepic_status"]
        ownerProfilePicUrl <- map["owner_profilepic_url"]
        ownerFBID <- map["owner_fb_id"]
        
        sourceID <- map["src_id"]
        destID <- map["dest_id"]
        
        vehicleType <- map["vehicle_type"]
        //userState <- map["user_state"]
    }
}

