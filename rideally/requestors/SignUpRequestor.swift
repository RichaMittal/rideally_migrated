//
//  SignUpRequestor.swift
//  rideally
//
//  Created by Raghunathan on 8/9/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class SignUpRequest: BaseModel {
    var user_first_name: String?
    var user_last_name: String?
    var user_email: String?
    var user_password: String?
    var user_gender: String?
    var user_mobile: String?
    var ref_code: String?
    var user_homeloc: String?
    var user_homeloc_lat: String?
    var user_homeloc_long: String?
    var device_id: String?
    var device_type: String?

    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_email <- map["user_email"]
        user_first_name <- map["user_first_name"]
        user_last_name <- map["user_last_name"]
        user_password <- map["user_password"]
        user_gender <- map["user_gender"]
        user_mobile <- map["user_mobile"]
        ref_code <- map["ref_code"]
        user_homeloc <- map["user_homeloc"]
        user_homeloc_lat <- map["user_homeloc_lat"]
        user_homeloc_long <- map["user_homeloc_long"]
        device_id <- map["device_id"]
        device_type <- map["device_type"]
    }
}


class SignUpResponse: BaseModel
{
    var dataObj: [SignupData]?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        dataObj <- map["data"]
    }
}

class SignupData: BaseModel {
    
    var user_id: String?
    var unique_id: String?
    var fb_id: String?
    var email : String?
    var device_id : String?
    var user_state : String?
    var pool_search_data : String?
    var created_time : String?
    var updated_time: String?
    var pool_scope: String?
    var first_name: String?
    var last_name: String?
    var full_name: String?
    var facebook_url: String?
    var email_id: String?
    var secondary_email: String?
    var secondary_email_status: String?
    var dob: String?
    var age: String?
    var mobile_number: String?
    var share_mobile_number: String?
    var gender : String?
    var address : String?
    var marital_status: String?
    var city_name: String?
    var pin: String?
    var linkedin_url: String?
    var current_company: String?
    var current_designation: String?
    var govt_id: String?
    var govtidf_url: String?
    var govtidb_url: String?
    var officeid_url: String?
    var govt_fid_status : String?
    var govt_bid_status: String?
    var profilepic_url: String?
    var profilepic_status: String?
    var office_id_status: String?
    var home_address: String?
    var home_lat: String?
    var home_long: String?
    var work_address : String?
    var invited_user_status: String?
    var vehicle_count: String?
    var loginTime: String?
    var route_request: String?
    var user_groups: String?
    var isOfficialEmail: String?
    var hideAnywhere: String?
    var  default_Vehicle_capacity:String?
    var  default_Vehicle_model:String?
    var  default_Vehicle_seqID:String?
    var  default_Vehicle_type:String?
    var  default_Vehicle_regNo:String?
    var userDetail: LoginUserDetail?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        user_id <- map["user_id"]
        unique_id <- map["unique_id"]
        fb_id <- map["fb_id"]
        email <- map["email"]
        device_id <- map["device_id"]
        user_state <- map["user_state"]
        pool_search_data <- map["pool_search_data"]
        created_time <- map["created_time"]
        updated_time <- map["updated_time"]
        pool_scope <- map["pool_scope"]
        first_name <- map["first_name"]
        last_name <- map["last_name"]
        full_name <- map["full_name"]
        facebook_url <- map["facebook_url"]
        email_id <- map["email_id"]
        secondary_email <- map["secondary_email"]
        secondary_email_status <- map["secondary_email_status"]
        dob <- map["dob"]
        age <- map["age"]
        mobile_number <- map["mobile_number"]
        share_mobile_number <- map["share_mobile_number"]
        gender <- map["gender"]
        address <- map["address"]
        marital_status <- map["marital_status"]
        city_name <- map["city_name"]
        pin <- map["pin"]
        linkedin_url <- map["linkedin_url"]
        current_company <- map["current_company"]
        current_designation <- map["current_designation"]
        govt_id <- map["govt_id"]
        govtidf_url <- map["govtidf_url"]
        govtidb_url <- map["govtidb_url"]
        officeid_url <- map["officeid_url"]
        govt_fid_status <- map["govt_fid_status"]
        govt_bid_status <- map["govt_bid_status"]
        profilepic_url <- map["profilepic_url"]
        profilepic_status <- map["profilepic_status"]
        office_id_status <- map["office_id_status"]
        home_address <- map["home_address"]
        home_lat <- map["latitude"]
        home_long <- map["longitude"]
        work_address <- map["wp_address"]
        invited_user_status <- map["invited_user_status"]
        vehicle_count <- map["vehicle_count"]
        loginTime <- map["loginTime"]
        route_request <- map["route_request"]
        user_groups <- map["user_groups"]
        isOfficialEmail <- map["is_official_email"]
        hideAnywhere <- map["hide_anywhere"]
        default_Vehicle_capacity <- map["default_Vehicle_capacity"]
        default_Vehicle_model <- map["default_Vehicle_model"]
        default_Vehicle_seqID <- map["default_Vehicle_seq_id"]
        default_Vehicle_type <- map["default_Vehicle_type"]
        default_Vehicle_regNo <- map["default_Vehicle_vehregno"]
        userDetail <- map["user_Detail"]
    }
}

class emailIDRequestModel: BaseModel {
    var user_email: String?
    var user_id: String?
    
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        user_email <- map["user_email"]
        user_id <- map["user_id"]
    }
}

class emailIDResponseModel: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class mobileNumberRequestModel: BaseModel {
    var user_mobile: String?
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        user_mobile <- map["user_mobile"]
    }
}

class mobileNumberResponseModel: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}

class referralCodeRequestModel: BaseModel {
    var ref_code: String?
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        ref_code <- map["ref_code"]
    }
}

class referralCodeResponseModel: BaseModel {
    var dataObj: [referralCodeData]?
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        dataObj <- map["data"]
    }
}

class referralCodeData: BaseModel {
    var firstName : String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        firstName <- map["first_name"]
    }
}

class applyRefCodeRequestModel: BaseModel {
    var coupon_code: String?
    var user_id: String?
    var device_id: String?
    var referred_by: String?
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        coupon_code <- map["coupon_code"]
        user_id <- map["user_id"]
        device_id <- map["device_id"]
        referred_by <- map["referred_by"]
    }
}

class applyRefCodeResponseModel: BaseModel {
    //var dataObj: [applyrefCodeData]?
    override func mapping(map: Map)
    {
        super.mapping(map: map)
        //dataObj <- map["data"]
    }
}
//
//class applyrefCodeData: BaseModel {
//    var firstName : String?
//    
//    override func mapping(map: Map) {
//        super.mapping(map)
//        firstName <- map["first_name"]
//    }
//}

class SignUpRequestor: BaseRequestor {
    
    func sendSignUpRequest(_ requestObj: SignUpRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        let parameters = Mapper<SignUpRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.SIGNUP, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: SignUpResponse = Mapper<SignUpResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    func isEmailIdalreadyexist(_ requestObj: emailIDRequestModel, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        let parameters = Mapper<emailIDRequestModel>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.ISEMAILIDEXIST, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: emailIDResponseModel = Mapper<emailIDResponseModel>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    func isMobilealreadyexist(_ requestObj: mobileNumberRequestModel, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        let parameters = Mapper<mobileNumberRequestModel>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.ISMOBILENUMBEREXIST, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: mobileNumberResponseModel = Mapper<mobileNumberResponseModel>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    
    func isReferralCodeValid(_ requestObj: referralCodeRequestModel, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        let parameters = Mapper<referralCodeRequestModel>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.ISREFERRALCODEVALID, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: referralCodeResponseModel = Mapper<referralCodeResponseModel>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
    
    func applyRefCode(_ requestObj: applyRefCodeRequestModel, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        let parameters = Mapper<applyRefCodeRequestModel>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.APPLYREFCODE, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: applyRefCodeResponseModel = Mapper<applyRefCodeResponseModel>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }
}
