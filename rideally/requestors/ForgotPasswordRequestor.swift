//
//  ForgotPasswordRequestor.swift
//  rideally
//
//  Created by Raghunathan on 8/9/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

/*
 {"code":"465","message":"New Password Email Sent Successfully.","data":[]}
 {"code":"466","message":"New Password Email Failed. Email Not Found.","data":[]}
 {"code":"467","message":"New Password Email Failed.","data":[]}
 */


class ForgotPasswordRequest: BaseModel {
    var email: String?
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        email <- map["email"]
    }
}

class ForgotPasswordResponse: BaseModel
{
    override func mapping(map: Map) {
        super.mapping(map: map)
    }
}


class ForgotPasswordRequestor: BaseRequestor {
    
    func sendForgotPasswordRequest(_ requestObj: ForgotPasswordRequest, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler){
        
        let parameters = Mapper<ForgotPasswordRequest>().toJSON(requestObj)
        makePOSTRequestWithparameters(URLS.FORGOTPWD, parameters: parameters as [String : AnyObject]?, encoding: .default, success: { (result) in
            
            let response: ForgotPasswordResponse = Mapper<ForgotPasswordResponse>().map(JSON: result as! [String : Any])!
            success(true, response)
            
            })
        { (error) in
            failure(error)
        }
    }

}
