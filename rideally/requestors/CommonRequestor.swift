//
//  CommonRequestor.swift
//  rideally
//
//  Created by Sarav on 17/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

class CommonRequestor: BaseRequestor {
    
    override func defaultHeaders() -> [String : String] {
        return ["Accept" : "application/json"]
    }
    
    func getDistanceDuration(_ source: String, destination: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        makeGETRequestWithparameters("http://maps.googleapis.com/maps/api/distancematrix/json?origins=\(source.replacingOccurrences(of: " ", with: ""))&destinations=\(destination.replacingOccurrences(of: " ", with: ""))&mode=driving&language=en-EN&sensor=false", parameters: nil, encoding: .default, success: { (response) in
            
            success(true, response)
            
        }) { (error) in
            failure(error)
        }
    }
    
    func getDirection(_ sourceStr: String, destinationStr: String, success: @escaping NetworkCompletionHandler, failure: @escaping NetworkFailureHandler)
    {
        makeGETRequestWithparameters("http://maps.googleapis.com/maps/api/distancematrix/json?origins=\(sourceStr.replacingOccurrences(of: " ", with: ""))&destinations=\(destinationStr.replacingOccurrences(of: " ", with: ""))&mode=driving&alternatives=true&sensor=false", parameters: nil, encoding: .default, success: { (response) in
            
            success(true, response)
            
        }) { (error) in
            failure(error)
        }

//        makeGETRequestWithparameters("https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true&sensor=false".stringByAddingPercentEncodingWithAllowedCharacters(CharacterSet.URLQueryAllowedCharacterSet())!, parameters: nil, encoding: .default, success: { (response) in
//                
//                success(success: true, object: response)
//                
//        }) { (error) in
//            failure(error)
//        }
    }
}
