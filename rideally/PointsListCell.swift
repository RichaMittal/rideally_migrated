//
//  PointsListCell.swift
//  rideally
//
//  Created by Raghunathan on 10/21/16.
//  Copyright © 2016 rideally. All rights reserved.
//


import UIKit

class PointsListCell: UITableViewCell
{
    
    var points = UILabel()
    var time =  UILabel()
    let descptext = UILabel()
    var personName = UILabel()
    var kmValue = UILabel()
    var selectedIndex: Int!
    var profilepicImageView = UIImageView()
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    var data: EarnedData!{
        didSet
        {
            updateCell()
        }
    }

    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        //self.layer.borderColor = Colors.GENERAL_BORDER_COLOR.CGColor
        //self.layer.borderWidth = 0.5
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        points.backgroundColor = UIColor.clear
        points.textColor = Colors.ORANGE_COLOR
        points.font = normalFontWithSize(15)
        points.textAlignment = NSTextAlignment.left
        points.lineBreakMode = .byWordWrapping
        points.translatesAutoresizingMaskIntoConstraints = false
        points.numberOfLines = 0
        self.addSubview(points)
        
        time.backgroundColor = UIColor.clear
        time.textColor = Colors.GRAY_COLOR
        time.font = normalFontWithSize(15)
        time.textAlignment = NSTextAlignment.right
        time.lineBreakMode = .byWordWrapping
        time.translatesAutoresizingMaskIntoConstraints = false
        time.numberOfLines = 0
        self.addSubview(time)
        
        descptext.backgroundColor = UIColor.clear
        descptext.textColor = Colors.GRAY_COLOR
        descptext.font = normalFontWithSize(15)
        descptext.textAlignment = NSTextAlignment.left
        descptext.lineBreakMode = .byWordWrapping
        descptext.translatesAutoresizingMaskIntoConstraints = false
        descptext.numberOfLines = 0
        self.addSubview(descptext)
        
        profilepicImageView.translatesAutoresizingMaskIntoConstraints = false
        profilepicImageView.image = UIImage(named: "m_profile")
        self.addSubview(profilepicImageView)
        profilepicImageView.isHidden = true
        
        personName.backgroundColor = UIColor.clear
        personName.textColor = Colors.GRAY_COLOR
        personName.font = normalFontWithSize(15)
        personName.textAlignment = NSTextAlignment.left
        personName.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(personName)
        personName.isHidden = true
        
        kmValue.backgroundColor = UIColor.clear
        kmValue.textColor = Colors.GREEN_COLOR
        kmValue.font = normalFontWithSize(15)
        kmValue.textAlignment = NSTextAlignment.center
        kmValue.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(kmValue)
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[point(30)]-5-[descptext(20)]-5-[pict(15)]-1-[name(20)]", options:[], metrics:nil, views:["point":points,"descptext":descptext,"pict":profilepicImageView,"name":personName]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[point]-padding-[kilo(100)]", options:[], metrics:["padding":(self.frame.size.width/5)-60], views:["point":points,"kilo":kmValue]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[descptext(250)]", options:[], metrics:nil, views:["descptext":descptext]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[pict(15)]", options:[], metrics:nil, views:["pict":profilepicImageView]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-40-[name]", options:[], metrics:nil, views:["name":personName]))
        
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[timeVal(50)]", options:[], metrics:nil, views:["timeVal":time]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[kilo(50)]", options:[], metrics:nil, views:["kilo":kmValue]))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[timeVal]-15-|", options:[], metrics:nil, views:["timeVal":time]))
        
        self.addConstraint(NSLayoutConstraint(item: time, attribute: .centerY, relatedBy: .equal, toItem: points, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: kmValue, attribute: .centerY, relatedBy: .equal, toItem: points, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
    }
    
    
    
    func updateCell() -> Void
    {
        if(data?.points != nil && data?.points != "0") {
            self.points.text = (data?.points)! + "  Points"
            if(data?.transactionType == "bonus") {
                self.descptext.text = (data?.transactionReason)!
            } else if(data?.transactionType == "recharge") {
                self.descptext.text = "Earned points from Recharge"
            } else if(data?.transactionType == "redeem") {
                self.descptext.text = "Redeemed Points"
            } else if(data?.transactionType == "ride") {
                if(selectedIndex == 0) {
                    self.descptext.text = "Earned from ride offer to"
                } else {
                    self.descptext.text = "Used for ride share with"
                }
            }
        } else {
            self.points.text =  "0  Points"
            if(data?.transactionType == "ride") {
                 if(selectedIndex == 0) {
                    self.descptext.text = "You have not earned from this ride"
                } else {
                     self.descptext.text = " You have not used any points in this ride."
                }
            } else {
                self.descptext.text = ""
            }
        }
        self.time.text = removeYearFromDate((data?.poolStartDate)!) + " at " + (data?.poolStartTime)!
        if(data?.distance != nil) {
            self.kmValue.text = (data?.distance)! + "  KM"
        }
        if(data?.users?.first != nil) {
            self.personName.text = data?.users?.first?.firstName
            self.profilepicImageView.isHidden = false
            self.personName.isHidden = false
        } else {
            self.personName.isHidden = true
            self.profilepicImageView.isHidden = true
        }
    }
}
