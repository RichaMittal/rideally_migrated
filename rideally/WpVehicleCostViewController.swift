//
//  WpVehicleCostViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 25/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class WpVehicleCostViewController: UIViewController {

    @IBOutlet weak var incrementButton: UIButton!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var decrementButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var seatLabel: UILabel!
    @IBOutlet weak var seatIncrement: UIButton!
    @IBOutlet weak var seatDecrement: UIButton!
    
    var vehicleConfig: VehicleConfigResponse!
    var vehicleDefaultDetails: VehicleDefaultResponse!
    var isControlerType: String!
    var cost = 0
    var seat = 0
    var maxSeat = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.vehicleName.text = vehicleDefaultDetails.data?.first?.vehicleType
        self.seatLabel.text = "2"
        self.priceLabel.text = "2"
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // self.vehicleConfig = object as! VehicleConfigResponse
        
        if (self.vehicleDefaultDetails.data?.first?.vehicleType == "Bike"){
            self.cost = Int(self.vehicleConfig.bike!)!
            self.maxSeat = 2
            self.seat = (self.vehicleDefaultDetails.data?.capacity)!
        }else if (self.vehicleDefaultDetails.data?.first?.vehicleType == "Car"){
            self.cost = Int(self.vehicleConfig.car!)!
            self.seat = (self.vehicleDefaultDetails.data?.capacity)!
            self.maxSeat = 4
        }else if (self.vehicleDefaultDetails.data?.first?.vehicleType == "Suv"){
            self.cost = Int(self.vehicleConfig.suv!)!
            self.seat = (self.vehicleDefaultDetails.data?.capacity)!
            self.maxSeat = 4
        }else{
            self.cost = 0
            self.seat = (self.vehicleDefaultDetails.data?.capacity)!
            self.maxSeat = 4
        }
        
        //let costStr = String(self.cost)
        //self.fare.text = costStr + "/ km"
        self.priceLabel.text = String(self.cost)
        
        
    }
    
    func checkVehicleTypeConditions(_ vehicleType : String, seat : Int)  {
        var messageString = ""
        
        if (vehicleType == "Bike"){
            if seat > 2 {
            messageString = "Oops! You cannot choose vehicle capacity more than 2."
            }else if seat < 2 {
              messageString = "Oops! You cannot choose vehicle capacity less than 2."
            }
            self.seat = 2

        }else if (vehicleType == "Car"){
            
            if seat > 5 {
                messageString = "Oops! You cannot choose vehicle capacity more than 5."
                self.seat = 5
            }else if seat < 2 {
                messageString = "Oops! You cannot choose vehicle capacity less than 2."
                self.seat = 2
            }
            
        }else if (vehicleType == "SUV"){
            
            if seat > 10 {
                messageString = "Oops! You cannot choose vehicle capacity more than 10."
                self.seat = 10
            }else if seat < 2 {
                messageString = "Oops! You cannot choose vehicle capacity less than 2."
                self.seat = 2
            }
            
        }else if (vehicleType == "Cab"){
            
            if seat > 15 {
                messageString = "Oops! You cannot choose vehicle capacity more than 15."
                self.seat = 15
            }else if seat < 2 {
                messageString = "Oops! You cannot choose vehicle capacity less than 2."
                self.seat = 2
            }
            
        }else if (vehicleType == "Auto"){
            
            if seat > 4 {
                messageString = "Oops! You cannot choose vehicle capacity more than 4."
                self.seat = 4
            }else if seat < 2 {
                messageString = "Oops! You cannot choose vehicle capacity less than 2."
                self.seat = 2
            }
            
        }else if (vehicleType == "Bus"){
            
            if seat > 25 {
                messageString = "Oops! You cannot choose vehicle capacity more than 25."
                self.seat = 25
            }else if seat < 2 {
                messageString = "Oops! You cannot choose vehicle capacity less than 2."
                self.seat = 2
            }
            
        }
        if messageString.characters.count > 0 {
            AlertController.showToastForInfo(messageString)
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func incrementButtonClicked(_ sender: AnyObject) {
        
        self.cost += 1
        self.priceLabel.text = String(cost)
        
    }
    
    @IBAction func decrementButtonClicked(_ sender: AnyObject) {
        cost -= 1
        if(cost <= 0){
            self.priceLabel.text = "0"
            cost = 0
            return
        }else{
            
        }
        self.priceLabel.text = String(cost)
        
    }
    
    @IBAction func removeCostView(_ sender: AnyObject) {
        
        self.dismiss(animated: true) {
            
            if (self.isControlerType == "Update") {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "kUpdateGetPriceNotification"),
                                                                          object: nil,
                                                                          userInfo: ["cost":self.cost,"vehicleConfig":self.vehicleConfig,"seat":self.seat])
                
            }else{
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kGetPriceNotification"),
                                                                      object: nil,
                                                                      userInfo: ["cost":self.cost,"vehicleConfig":self.vehicleConfig,"seat":self.seat])
            }
        }
    }
    
    @IBAction func addSeatuBtonClicked(_ sender: AnyObject) {
        
        
        self.seat += 1
        self.seatLabel.text = String(self.seat)
        self.checkVehicleTypeConditions((self.vehicleDefaultDetails.data?.first?.vehicleType)! , seat: self.seat)
        self.seatLabel.text = String(self.seat)
        
    }
    
    @IBAction func decrementSeatButtonClicked(_ sender: AnyObject) {
        self.seat -= 1
        self.seatLabel.text = String(self.seat)
        self.checkVehicleTypeConditions((self.vehicleDefaultDetails.data?.first?.vehicleType)! , seat: self.seat)
        self.seatLabel.text = String(self.seat)
    }
}
