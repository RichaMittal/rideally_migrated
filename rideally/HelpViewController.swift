//
//  HelpViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/17/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    
    var webContentView = UIWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Help"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        showIndicator("Getting Help..")
        let myWebView:UIWebView = UIWebView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        myWebView.loadRequest(URLRequest(url: URL(string: URLS.GET_FAQ)!))
        self.view.addSubview(myWebView)
        hideIndicator()
    }
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
}
