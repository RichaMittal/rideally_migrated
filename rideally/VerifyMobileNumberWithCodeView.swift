//
//  VerifyMobileNumberWithCodeView.swift
//  rideally
//
//  Created by Raghunathan on 8/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class VerifyMobileNumberWithCodeView: UIView,UITextFieldDelegate {

    var mobileNumberTextBox : RATextField!
    var smsCodeNumberTextBox : RATextField!
    var lblMobileCode = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createVerifyMobileNumberView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func getTextBoxViews(_ placeHolderString:String) -> RATextField
    {
        let textBoxInstance = RATextField()
        textBoxInstance.returnKeyType = UIReturnKeyType.next
        textBoxInstance.delegate = self
        textBoxInstance.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        textBoxInstance.placeholder = placeHolderString
        textBoxInstance.font = normalFontWithSize(15)!
        textBoxInstance.textColor = Colors.GRAY_COLOR
        return textBoxInstance
    }
    
    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(11)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GRAY_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.left
        return labelObject
    }

    
    func createVerifyMobileNumberView() -> Void {
        
        self.backgroundColor = Colors.WHITE_COLOR
        
        lblMobileCode.text = "+91"
        lblMobileCode.textColor = Colors.GRAY_COLOR
        lblMobileCode.font = boldFontWithSize(14)
        lblMobileCode.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lblMobileCode)
        
        mobileNumberTextBox = getTextBoxViews("ENTER MOBILE NUMBER")
        mobileNumberTextBox.text = UserInfo.sharedInstance.mobile
        
        let mobilenumberDescription = getLabelSubViews("Please check your number before verify")
        smsCodeNumberTextBox = getTextBoxViews("ENTER CODE HERE")
        smsCodeNumberTextBox.returnKeyType = UIReturnKeyType.done
        let codenumberDescription = getLabelSubViews("Please enter the code from received sms")

        mobileNumberTextBox.translatesAutoresizingMaskIntoConstraints = false
        mobilenumberDescription.translatesAutoresizingMaskIntoConstraints = false
        smsCodeNumberTextBox.translatesAutoresizingMaskIntoConstraints = false
        codenumberDescription.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(mobileNumberTextBox)
        self.addSubview(mobilenumberDescription)
        self.addSubview(smsCodeNumberTextBox)
        self.addSubview(codenumberDescription)
        
        let subViewsDict = ["lblmobilecode":lblMobileCode,"mobilenumber":mobileNumberTextBox,"mobiledesp":mobilenumberDescription,"codenumber":smsCodeNumberTextBox,"codedesp":codenumberDescription] as [String : Any]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[mobilenumber(30)]-2-[mobiledesp(15)]-10-[codenumber(==mobilenumber)]-2-[codedesp(==mobiledesp)]-10-|", options:[], metrics:nil, views: subViewsDict))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblmobilecode]-5-[mobilenumber]-10-|", options: [], metrics: nil, views: subViewsDict))
        
        self.addConstraint(NSLayoutConstraint(item: lblMobileCode, attribute: .centerY, relatedBy: .equal, toItem: mobileNumberTextBox, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[mobiledesp]-10-|", options: [], metrics: nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[codenumber]-10-|", options: [], metrics: nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[codedesp]-10-|", options: [], metrics: nil, views: subViewsDict))
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == mobileNumberTextBox)
        {
            smsCodeNumberTextBox.becomeFirstResponder()
        }
        else{
            smsCodeNumberTextBox.resignFirstResponder()
        }
        
        return true
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if  textField == mobileNumberTextBox
        {
            let lenghtValue = (textField.text?.characters.count)! + string.characters.count - range.length
            if lenghtValue > 10
            {
                return false
            }
            else
            {
                let aSet = CharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
                
            }
        }
        else
        {
            let aSet = CharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
        }
    }

}
