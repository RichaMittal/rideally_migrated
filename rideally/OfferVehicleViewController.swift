//
//  OfferVehicleViewController.swift
//  rideally
//
//  Created by Sarav on 06/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class OfferVehicleViewController: NeedRideViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let offerVehicleObj = OfferVehicleRequest()
    let joinOrOfferRideObj = JoinOrOfferRide()
    var vehicleConfig: VehicleConfigResponse!
    var isExistingRide:Bool = false
    var pickUpLoc = ""
    var pickUpLat = ""
    var pickUpLong = ""
    var dropLoc = ""
    var dropLat = ""
    var dropLong = ""
    var isWP = false
    var wpInsuranceStatus = ""
    var vehiclesList = [VehicleData]()
    let costOptions = ["Person", "KM"]
    var backFromSearchResultOfferData: OfferVehicleRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Offer a Vehicle"
        
        if(isEditingRide){
            self.title = "Update Ride"
        }
        getConfigData()
    }
    
    func getConfigData()
    {
        var configType = ""
        var groupId = ""
        if(isWP) {
            configType = "workplace"
            groupId = (rideDetailData?.ownerScope)!
        } else {
            configType = "ride"
            groupId = ""
        }
        RideRequestor().getVehicleConfig(configType, groupID: groupId, success: { (success, object) in
            self.vehicleConfig = object as! VehicleConfigResponse
            self.getVehicles()
        }) { (error) in
        }
    }
    
    func getVehicles()
    {
        let reqObj = GetALLVehicleListRequest()
        reqObj.userID = UserInfo.sharedInstance.userID
        
        showIndicator("Fetching vehicles data.")
        ProfileRequestor().getAddVehicleListData(reqObj, success: { (success, object) in
            hideIndicator()
            
            if let vehicles = (object as! GetALLVehicleListResponse).dataObj{
                self.vehiclesList = vehicles
                if(self.isEditingRide) {
                    if let type = self.rideDetailData?.vehicleType{
                        var maxAmt = 0
                        if(type.lowercased() == "car"){
                            maxAmt = Int(self.vehicleConfig.car!)!
                        } else if(type.lowercased() == "bike"){
                            maxAmt = Int(self.vehicleConfig.bike!)!
                        } else if(type.lowercased() == "suv"){
                            maxAmt = Int(self.vehicleConfig.suv!)!
                        } else {
                            maxAmt = 99999
                        }
                        
                        self.stepperAmt.maximum = Int32(maxAmt)
                    }
                    
                } else {
                    for vehicle in self.vehiclesList{
                        if(vehicle.isDefault == "1"){
                            self.txtVehicle.text = vehicle.RegNo
                            UserInfo.sharedInstance.vehicleSeqID = vehicle.vehicleSeqId!
                            UserInfo.sharedInstance.vehicleType = vehicle.vehicleType!
                            UserInfo.sharedInstance.vehicleModel = vehicle.vehicleModel!
                            UserInfo.sharedInstance.vehicleRegNo = vehicle.RegNo!
                            UserInfo.sharedInstance.vehicleSeatCapacity = vehicle.capacity!
                            UserInfo.sharedInstance.vehicleKind = vehicle.vehicleKind!
                            UserInfo.sharedInstance.vehicleOwnerName = vehicle.vehicleOwnerName!
                            UserInfo.sharedInstance.insurerName = (vehicle.insurerName)!
                            UserInfo.sharedInstance.insuredCmpnyName = (vehicle.insuredCmpnyName)!
                            UserInfo.sharedInstance.insuranceId = (vehicle.insuranceId)!
                            UserInfo.sharedInstance.insuranceExpiryDate = (vehicle.insuranceExpiryDate)!
                            UserInfo.sharedInstance.insUploadStatus = vehicle.insUploadStatus!
                            UserInfo.sharedInstance.insUrl = vehicle.insUrl!
                            UserInfo.sharedInstance.rcUploadStatus = vehicle.rcUploadStatus!
                            UserInfo.sharedInstance.rcUrl = vehicle.rcUrl!
                            if let capacity = vehicle.capacity{
                                self.stepperSeats.maximum = Int32(capacity)! - 1
                                self.stepperSeats.value = Int32(capacity)! - 1
                            }
                            
                            if let type = vehicle.vehicleType{
                                var maxAmt = 0
                                if(type.lowercased() == "car"){
                                    maxAmt = Int(self.vehicleConfig.car!)!
                                } else if(type.lowercased() == "bike"){
                                    maxAmt = Int(self.vehicleConfig.bike!)!
                                } else if(type.lowercased() == "suv"){
                                    maxAmt = Int(self.vehicleConfig.suv!)!
                                } else {
                                    maxAmt = 99999
                                }
                                
                                self.stepperAmt.maximum = Int32(maxAmt)
                                self.stepperAmt.value = 0
                            }
                        }
                    }
                }
                
                if(self.vehiclesList.count == 0){
                    self.txtVehicle.placeholder = "No vehicles available"
                    self.txtVehicle.isUserInteractionEnabled = false
                    self.stepperSeats.isUserInteractionEnabled = false
                }
                else{
                    
                    self.txtVehicle.isUserInteractionEnabled = true
                    self.stepperSeats.isUserInteractionEnabled = true
                }
            }
            
        }) { (error) in
            hideIndicator()
        }
        
    }
    
    let lblPickup = UILabel()
    let lblDrop = UILabel()
    
    func getLocationViewNew() -> SubView
    {
        let view = UIView()
        
        lblCaption.text = "Please share your travel details"
        lblCaption.font = boldFontWithSize(18)
        lblCaption.textColor = Colors.GRAY_COLOR
        lblCaption.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCaption)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.contentMode = .center
        iconSource.backgroundColor = Colors.GENERAL_BORDER_COLOR
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSource)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.text = "From (Select exact or nearby google places)"
        lblSource.font = boldFontWithSize(14)
        lblSource.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblSource)
        
        let iconPickup = UIImageView()
        iconPickup.image = UIImage(named: "source")
        iconPickup.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconPickup)
        
        lblPickup.textColor = Colors.GRAY_COLOR
        lblPickup.text = "Start location (Select exact or nearby google places)"
        lblPickup.font = boldFontWithSize(14)
        lblPickup.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(mapForPickup))
        lblPickup.isUserInteractionEnabled = true
        lblPickup.addGestureRecognizer(tapGestureFrom)
        view.addSubview(lblPickup)
        
        let iconSourceArrow = UIImageView()
        iconSourceArrow.image = UIImage(named: "rightArrow")
        iconSourceArrow.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSourceArrow)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.contentMode = .center
        iconDest.backgroundColor = Colors.GENERAL_BORDER_COLOR
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDest)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblDest.font = boldFontWithSize(14)
        lblDest.text = "To (Select exact or nearby google places)"
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDest)
        
        let iconDrop = UIImageView()
        iconDrop.image = UIImage(named: "dest")
        iconDrop.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDrop)
        
        lblDrop.textColor = Colors.GRAY_COLOR
        lblDrop.text = "End location (Select exact or nearby google places)"
        lblDrop.font = boldFontWithSize(14)
        lblDrop.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForDrop))
        lblDrop.isUserInteractionEnabled = true
        lblDrop.addGestureRecognizer(tapGestureTo)
        view.addSubview(lblDrop)
        
        let iconDestArrow = UIImageView()
        iconDestArrow.image = UIImage(named: "rightArrow")
        iconDestArrow.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDestArrow)
        
        view.addSubview(lblDest)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        let lblLine4 = UILabel()
        lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine4.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine4)
        
        let lblLine5 = UILabel()
        lblLine5.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine5.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine5)
        
        let viewsDict = ["caption":lblCaption, "line1":lblLine1, "isource":iconSource, "source":lblSource, "line2":lblLine2, "idest":iconDest, "dest":lblDest, "line3":lblLine3, "sarrow":iconSourceArrow, "darrow":iconDestArrow, "pickup":lblPickup, "drop":lblDrop, "line4":lblLine4, "line5":lblLine5, "ipickup":iconPickup, "idrop":iconDrop]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[caption(50)][line1(0.5)][source(40)][line2(0.5)][pickup(40)][line3(0.5)][dest(40)][line4(0.5)][drop(40)][line5(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[caption]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[isource(15)][source]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[ipickup(15)][pickup][sarrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[idest(15)][dest]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[idrop(15)][drop][darrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line5]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .height, relatedBy: .equal, toItem: lblSource, attribute: .height, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .height, relatedBy: .equal, toItem: lblDest, attribute: .height, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: iconPickup, attribute: .centerY, relatedBy: .equal, toItem: lblPickup, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDrop, attribute: .centerY, relatedBy: .equal, toItem: lblDrop, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: iconSourceArrow, attribute: .centerY, relatedBy: .equal, toItem: lblPickup, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDestArrow, attribute: .centerY, relatedBy: .equal, toItem: lblDrop, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 216)
        return sub
    }
    
    func mapForPickup()
    {
        let vc = LocationViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.onSelectionBlock = { (location) -> Void in
            if let loc = location as? [String: String]
            {
                self.lblPickup.text = loc["location"]
                self.joinOrOfferRideObj.pickupPoint = self.lblPickup.text!
                self.joinOrOfferRideObj.pickupPointLat = loc["lat"]!
                self.joinOrOfferRideObj.pickupPointLong = loc["long"]!
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func mapForDrop()
    {
        let vc = LocationViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.onSelectionBlock = { (location) -> Void in
            if let loc = location as? [String: String]
            {
                self.lblDrop.text = loc["location"]
                self.joinOrOfferRideObj.dropPoint = self.lblDrop.text!
                self.joinOrOfferRideObj.dropPointLat = loc["lat"]!
                self.joinOrOfferRideObj.dropPointLong = loc["long"]!
                
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func addViews() {
        
        if(isExistingRide){
            viewsArray.append(getLocationViewNew())
        }else{
            viewsArray.append(getLocationView())
        }
        
        viewsArray.append(getTravelWithView())
        viewsArray.append(getTimeView())
        
        viewsArray.append(getCostView())
        viewsArray.append(getVehicleSelectionView())
        
        viewsArray.append(getCommentView())
        
        addBottomView(getBottomView())
        
        btnSubmit.setTitle("SUBMIT", for: UIControlState())
        
        if(isExistingRide){
            btnSubmit.setTitle("SEND OFFER REQUEST", for: UIControlState())
            
            lblSource.text = rideDetailData?.source
            lblSource.isUserInteractionEnabled = false
            lblDest.text = rideDetailData?.destination
            lblDest.isUserInteractionEnabled = false
            
            lblPickup.text = pickUpLoc
            joinOrOfferRideObj.pickupPoint = pickUpLoc
            joinOrOfferRideObj.pickupPointLat = pickUpLat
            joinOrOfferRideObj.pickupPointLong = pickUpLong
            lblDrop.text = dropLoc
            joinOrOfferRideObj.dropPoint = dropLoc
            joinOrOfferRideObj.dropPointLat = dropLat
            joinOrOfferRideObj.dropPointLong = dropLong
            
            btnAny.isUserInteractionEnabled = false
            btnOnly.isUserInteractionEnabled = false
            
            if(rideDetailData?.poolType == "M"){
                btnOnly.setTitle("Only Male", for: UIControlState())
                btnOnly.isSelected = true
            }
            else if(rideDetailData?.poolType == "F"){
                btnOnly.setTitle("Only Female", for: UIControlState())
                btnOnly.isSelected = true
            }
            else{
                btnAny.isSelected = true
            }
            
            txtDate.isUserInteractionEnabled = false
            txtTime.isUserInteractionEnabled = false
            
            txtDate.text = prettyDateStringFromString(rideDetailData?.startDate, fromFormat: "dd-MMM-yyyy", toFormat: "dd-MMM-yyyy")
            txtTime.text = prettyDateStringFromString(rideDetailData?.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
            
        }
        
        if(isEditingRide){
            lblSource.text = rideDetailData?.source
            lblDest.text = rideDetailData?.destination
            
            if(rideDetailData?.poolType == "M"){
                btnOnly.setTitle("Only Male", for: UIControlState())
                btnOnly.isSelected = true
            }
            else if(rideDetailData?.poolType == "F"){
                btnOnly.setTitle("Only Female", for: UIControlState())
                btnOnly.isSelected = true
            }
            else{
                btnAny.isSelected = true
            }
            
            txtDate.text = prettyDateStringFromString(rideDetailData?.startDate, fromFormat: "dd-MMM-yyyy", toFormat: "dd-MMM-yyyy")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy"
            if let date = txtDate.text {
                datePicker.date = dateFormatter.date(from: date)!
            }
            
            txtTime.text = prettyDateStringFromString(rideDetailData?.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
            dateFormatter.dateFormat = "HH:mm"
            if let time = txtTime.text {
                timePicker.date = dateFormatter.date(from: time)!
            }
            txtCostOptions.text = "KM"
            if let amt = rideDetailData?.cost{
                stepperAmt.value = Int32(amt)!
                stepperAmt.countLabel.text = "\(amt)"
            }
            
            txtVehicle.text = rideDetailData?.vehicleRegNo
            if let cnt = rideDetailData?.seatsLeft{
                stepperSeats.value = Int32(cnt)!
                stepperSeats.countLabel.text = "\(cnt)"
            }
            
            txtComments.text = rideDetailData?.comments
            
            offerVehicleObj.fromLocation = rideDetailData?.source
            offerVehicleObj.fromLat = rideDetailData?.sourceLat
            offerVehicleObj.fromLong = rideDetailData?.sourceLong
            offerVehicleObj.toLocation = rideDetailData?.destination
            offerVehicleObj.toLat = rideDetailData?.destinationLat
            offerVehicleObj.toLong = rideDetailData?.destinationLong
        }
    }
    
    override func getTimeView() -> SubView {
        offerVehicleObj.rideDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        offerVehicleObj.rideTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        
        return super.getTimeView()
    }
    override func mapForFrom()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.offerVehicleObj.fromLocation = self.lblSource.text
                    self.offerVehicleObj.fromLat = loc["lat"]
                    self.offerVehicleObj.fromLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func mapForTo()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.offerVehicleObj.toLocation = self.lblDest.text
                    self.offerVehicleObj.toLat = loc["lat"]
                    self.offerVehicleObj.toLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    let pickerView = UIPickerView()
    let stepperAmt = PKYStepper()
    let txtCostOptions = RATextField()
    
    func getCostView() -> SubView
    {
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "I AM WILLING TO SHARE THE COST"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        stepperAmt.setBorderColor(Colors.GRAY_COLOR)
        stepperAmt.setBorderWidth(0.5)
        stepperAmt.countLabel.layer.borderWidth = 0.5
        stepperAmt.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperAmt.valueChangedCallback = { (stepper, count) -> Void in
            stepper?.countLabel.text = "\(count)"
        }
        
        stepperAmt.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Cost for a seat is exceeding its maximum.")
            }
        }
        
        stepperAmt.setLabelTextColor(Colors.GRAY_COLOR)
        stepperAmt.value = 0
        
        stepperAmt.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperAmt)
        
        let lblMsg = UILabel()
        lblMsg.text = "Please mention per ride cost only"
        lblMsg.font = boldFontWithSize(11)
        lblMsg.textColor = Colors.GRAY_COLOR
        lblMsg.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblMsg)
        
        let lblPer = UILabel()
        lblPer.text = "PER"
        lblPer.font = boldFontWithSize(14)
        lblPer.textColor = Colors.GRAY_COLOR
        lblPer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPer)
        
        txtCostOptions.layer.borderWidth = 0.5
        txtCostOptions.delegate = self
        txtCostOptions.font = boldFontWithSize(14)
        txtCostOptions.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtCostOptions.translatesAutoresizingMaskIntoConstraints = false
        txtCostOptions.text = "KM"
        txtCostOptions.isUserInteractionEnabled = false
        view.addSubview(txtCostOptions)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtCostOptions.inputAccessoryView = keyboardDoneButtonShow
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["title":lblTitle, "amount":stepperAmt, "msg":lblMsg, "per":lblPer, "persons":txtCostOptions, "line":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title(20)]-5-[amount(30)]-5-[msg(15)]-5-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[title]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[amount]-15-[per]-15-[persons(==amount)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[msg]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: lblPer, attribute: .centerY, relatedBy: .equal, toItem: stepperAmt, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: txtCostOptions, attribute: .centerY, relatedBy: .equal, toItem: stepperAmt, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: txtCostOptions, attribute: .height, relatedBy: .equal, toItem: stepperAmt, attribute: .height, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 86)
        return sub
    }
    
    let txtVehicle = RATextField()
    let stepperSeats = PKYStepper()
    func getVehicleSelectionView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "ADD YOUR VEHICLE"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtVehicle.layer.borderWidth = 0.5
        txtVehicle.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtVehicle.translatesAutoresizingMaskIntoConstraints = false
        txtVehicle.delegate = self
        txtVehicle.font = boldFontWithSize(14)
        txtVehicle.inputView = pickerView
        txtVehicle.placeholder = "Select your vehicle"
        view.addSubview(txtVehicle)
        let imgV = UIImageView(image: UIImage(named: "dropdown")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        txtVehicle.rightView = imgV
        txtVehicle.rightViewMode = .always
        txtVehicle.isUserInteractionEnabled = false
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtVehicle.inputAccessoryView = keyboardDoneButtonShow
        
        let btnAddVehicle = UIButton()
        btnAddVehicle.translatesAutoresizingMaskIntoConstraints = false
        btnAddVehicle.setTitle(" REGISTER A NEW VEHICLE", for: UIControlState())
        btnAddVehicle.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnAddVehicle.setImage(UIImage(named: "addV"), for: UIControlState())
        btnAddVehicle.contentHorizontalAlignment = .left
        btnAddVehicle.titleLabel?.font = boldFontWithSize(11)
        btnAddVehicle.addTarget(self, action: #selector(addNewVehicle), for: .touchDown)
        view.addSubview(btnAddVehicle)
        
        let lblTitle1 = UILabel()
        lblTitle1.text = "SEATS AVAILABLE"
        lblTitle1.font = boldFontWithSize(11)
        lblTitle1.textColor = Colors.GRAY_COLOR
        lblTitle1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle1)
        
        stepperSeats.setBorderColor(Colors.GRAY_COLOR)
        stepperSeats.setBorderWidth(0.5)
        stepperSeats.countLabel.layer.borderWidth = 0.5
        stepperSeats.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperSeats.valueChangedCallback = { (stepper, count) -> Void in
            self.stepperSeats.countLabel.text = "\(count)"
        }
        stepperSeats.setLabelTextColor(Colors.GRAY_COLOR)
        stepperSeats.value = 1
        stepperSeats.minimum = 1
        
        stepperSeats.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Seat selection is exceeding its maximum seat available.")
            }
        }
        
        stepperSeats.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperSeats)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["title":lblTitle, "vehicle":txtVehicle, "title1":lblTitle1, "stepper":stepperSeats, "line":lblLine1, "btn":btnAddVehicle]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title(20)]-5-[vehicle(30)]-3-[btn(20)]-5-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[title]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[vehicle]-20-[stepper(150)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[btn(200)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: stepperSeats, attribute: .top, relatedBy: .equal, toItem: txtVehicle, attribute: .top, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stepperSeats, attribute: .height, relatedBy: .equal, toItem: txtVehicle, attribute: .height, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: lblTitle1, attribute: .left, relatedBy: .equal, toItem: stepperSeats, attribute: .left, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblTitle1, attribute: .centerY, relatedBy: .equal, toItem: lblTitle, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 91)
        return sub
    }
    
    override func donePressed()
    {
        if(selectedTxtField == txtDate){
            offerVehicleObj.rideDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            selectedTxtField?.text = offerVehicleObj.rideDate
        }
        else if(selectedTxtField == txtTime){
            offerVehicleObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            selectedTxtField?.text = offerVehicleObj.rideTime
        }
        else if(selectedTxtField == txtCostOptions){
            offerVehicleObj.rideDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            selectedTxtField?.text = costOptions[pickerView.selectedRow(inComponent: 0)]
        }
        else if(selectedTxtField == txtVehicle){
            if(vehiclesList.count > 0){
                let vehicle = vehiclesList[pickerView.selectedRow(inComponent: 0)]
                selectedTxtField?.text = vehicle.RegNo
                UserInfo.sharedInstance.vehicleSeqID = vehicle.vehicleSeqId!
                UserInfo.sharedInstance.vehicleType = vehicle.vehicleType!
                UserInfo.sharedInstance.vehicleModel = vehicle.vehicleModel!
                UserInfo.sharedInstance.vehicleRegNo = vehicle.RegNo!
                UserInfo.sharedInstance.vehicleSeatCapacity = vehicle.capacity!
                UserInfo.sharedInstance.vehicleKind = vehicle.vehicleKind!
                UserInfo.sharedInstance.vehicleOwnerName = vehicle.vehicleOwnerName!
                UserInfo.sharedInstance.insurerName = vehicle.insurerName!
                UserInfo.sharedInstance.insuredCmpnyName = vehicle.insuredCmpnyName!
                UserInfo.sharedInstance.insuranceId = vehicle.insuranceId!
                UserInfo.sharedInstance.insuranceExpiryDate = vehicle.insuranceExpiryDate!
                UserInfo.sharedInstance.insUploadStatus = vehicle.insUploadStatus!
                UserInfo.sharedInstance.insUrl = vehicle.insUrl!
                UserInfo.sharedInstance.rcUploadStatus = vehicle.rcUploadStatus!
                UserInfo.sharedInstance.rcUrl = vehicle.rcUrl!
                sendDefaultVehicleRequest()
                if let capacity = vehicle.capacity{
                    self.stepperSeats.maximum = Int32(capacity)! - 1
                    self.stepperSeats.value = Int32(capacity)! - 1
                }
                
                if let type = vehicle.vehicleType{
                    var maxAmt = 0
                    if(type.lowercased() == "car"){
                        maxAmt = Int(self.vehicleConfig.car!)!
                    } else if(type.lowercased() == "bike"){
                        maxAmt = Int(self.vehicleConfig.bike!)!
                    } else if(type.lowercased() == "suv"){
                        maxAmt = Int(self.vehicleConfig.suv!)!
                    } else {
                        maxAmt = 99999
                    }
                    
                    self.stepperAmt.maximum = Int32(maxAmt)
                    self.stepperAmt.value = 0
                }
                
            }
        }
        selectedTxtField?.resignFirstResponder()
    }
    
    func sendDefaultVehicleRequest() -> Void {
        
        if  UserInfo.sharedInstance.userID.characters.count > 0 && UserInfo.sharedInstance.vehicleSeqID.characters.count > 0
        {
            let reqObj = SetDefaultVehicleRequest()
            showLoadingIndicator("Loading...")
            reqObj.userID = UserInfo.sharedInstance.userID
            reqObj.vehicleSeqID = UserInfo.sharedInstance.vehicleSeqID
            
            ProfileRequestor().sendSetDefaultVehicleRequest(reqObj, success:{ (success, object) in
                self.hideLoadingIndiactor()
                AlertController.showToastForInfo((object as! SetDefaultVehicleResponse).message!)
            }){ (error) in
                self.hideLoadingIndiactor()
                AlertController.showAlertForError(error)
            }
        }
        else
        {
            AlertController.showToastForError("Invalid UserID/VehicleSeqID provided in the Request")
        }
        
    }

    func addNewVehicle()
    {
        let vc = RegisterNewVehicleController()
        vc.isUpdateReqneedtobeSend = "0"
        vc.isSeatCapacityNeedToBeUpdated = "1"
        if(wpInsuranceStatus != "" && wpInsuranceStatus == "1") {
            vc.isWpInsuranceStatus = true
        }
        vc.onAddSuccessAction = {
            self.getVehicles()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateVehicle()
    {
        let vc = RegisterNewVehicleController()
        vc.isUpdateReqneedtobeSend = "1"
        vc.isSeatCapacityNeedToBeUpdated = "1"
        if(wpInsuranceStatus != "" && wpInsuranceStatus == "1") {
            vc.isWpInsuranceStatus = true
        }
        vc.onAddSuccessAction = {
            self.getVehicles()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(selectedTxtField == txtVehicle){
            return vehiclesList.count
        }
        else if(selectedTxtField == txtCostOptions){
            return costOptions.count
        }
        else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(selectedTxtField == txtVehicle){
            return vehiclesList[row].RegNo
        }
        else if(selectedTxtField == txtCostOptions){
            return costOptions[row]
        }
        else {
            return ""
        }
    }
    
    override func proceed() {
        if(isExistingRide){
            joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
            joinOrOfferRideObj.poolID = rideDetailData?.poolID
            joinOrOfferRideObj.vehicleRegNo = txtVehicle.text
            joinOrOfferRideObj.vehicleCapacity = self.stepperSeats.countLabel.text!
            if let cost = stepperAmt.countLabel.text{
                joinOrOfferRideObj.cost = cost
            }
            joinOrOfferRideObj.rideMsg = txtComments.text
            let todayDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.string(from: todayDate) as String
            if(joinOrOfferRideObj.vehicleRegNo != "" && wpInsuranceStatus == "1" && (UserInfo.sharedInstance.insuranceId == "")) {
                AlertController.showAlertFor("Offer a Vehicle", message: "Please provide vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                    self.updateVehicle()
                })
                return
            } else if(joinOrOfferRideObj.vehicleRegNo != "" && wpInsuranceStatus == "1" && UserInfo.sharedInstance.insuranceId != ""  && UserInfo.sharedInstance.insuranceExpiryDate.compare(date) == ComparisonResult.orderedAscending) {
                AlertController.showAlertFor("Offer a Vehicle", message: "Please update vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                    self.updateVehicle()
                })
                return
            }

            if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                if(joinOrOfferRideObj.vehicleRegNo != ""){
                    showIndicator("Offering vehicle to ride..")
                    OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                        hideIndicator()
                        
                        if(success == true){
                            AlertController.showAlertFor("Offer a Vehicle", message: "Thank you, Your request to offer the ride has been sent successfully.", okButtonTitle: "Ok", okAction: {
                                let poolID = (self.rideDetailData?.poolID)!
                                
                                showIndicator("Fetching Ride Details")
                                RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                    hideIndicator()
                                    if(success){
                                        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                        let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                       // let vc = RideDetailViewController()
                                        vc.hidesBottomBarWhenPushed = true
                                        vc.edgesForExtendedLayout = UIRectEdge()
                                        vc.data = object as? RideDetail
                                        vc.triggerFrom = "ride"
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }) { (error) in
                                    hideIndicator()
                                }
                                
                            })
                        }
                        else{
                            if((object as! BaseModel).code == "75") {
                                AlertController.showToastForError("Sorry, you will not be able to offer, as the vehicle capacity is less.")
                            } else if((object as! BaseModel).code == "711") {
                                AlertController.showToastForError("Vehicle is already with other ride in 30 mins duration.")
                            } else {
                                AlertController.showToastForError((object as! BaseModel).message!)
                            }
                        }
                        
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }
                else{
                    AlertController.showAlertFor("Offer a Vehicle", message: "Please provide vehicle details.")
                }
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
            }
        }
        else if(isEditingRide){
            let updateReqObj = UpdateRideRequest()
            updateReqObj.cost_type = rideDetailData?.costType
            updateReqObj.poolCategory = rideDetailData?.poolCategory
            updateReqObj.poolID = rideDetailData?.poolID
            updateReqObj.poolShared = rideDetailData?.poolShared
            updateReqObj.rideType = rideDetailData?.rideType
            updateReqObj.userID = rideDetailData?.createdBy
            updateReqObj.email = rideDetailData?.ownerEmail
            
            if(offerVehicleObj.fromLocation != nil){
                updateReqObj.from_location = offerVehicleObj.fromLocation
                updateReqObj.from_lat = offerVehicleObj.fromLat
                updateReqObj.from_lon = offerVehicleObj.fromLong
            }
            if(offerVehicleObj.toLocation != nil){
                updateReqObj.to_location = offerVehicleObj.toLocation
                updateReqObj.to_lat = offerVehicleObj.toLat
                updateReqObj.to_lon = offerVehicleObj.toLong
            }
            updateReqObj.pooltype = btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
            updateReqObj.poolstarttime = txtTime.text
            updateReqObj.poolstartdate = txtDate.text
            updateReqObj.rideMessage = txtComments.text
            updateReqObj.vehregno = txtVehicle.text!
            updateReqObj.seatLeft = stepperSeats.countLabel.text
            if let cost = stepperAmt.countLabel.text{
                updateReqObj.cost = cost
            }
            
            if((updateReqObj.from_location != nil) && (updateReqObj.to_location != nil)){
                if(updateReqObj.from_location != updateReqObj.to_location){
                    if((updateReqObj.poolstartdate != nil) && (updateReqObj.poolstarttime != nil)){
                        if(updateReqObj.vehregno != ""){
                            
                            CommonRequestor().getDistanceDuration(updateReqObj.from_location!, destination: updateReqObj.to_location!, success: { (success, object) in
                                    updateReqObj.duration = ""
                                    updateReqObj.distance = ""
                                    
                                    if let data = object as? NSDictionary{
                                        let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                                        let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                                        if((elements["status"] as! String) == "OK")
                                        {
                                            let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                            let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                        updateReqObj.duration = duration as? String
                                        updateReqObj.distance = distance as? String
                                    }
                                }
                                let sourceStr = "\(updateReqObj.from_lat!),\(updateReqObj.from_lon!)"
                                let destinationStr = "\(updateReqObj.to_lat!), \(updateReqObj.to_lon!)"
                                CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                                    updateReqObj.via = ""
                                    updateReqObj.waypoints = ""
                                    if let data = object as? NSDictionary{
                                        let routes = data["routes"] as! NSArray
                                        if routes.count == 0{
                                            return
                                        }
                                        let routesDict = routes[0] as! NSDictionary
                                        let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                                        let points = overview_polyline ["points"] as! NSString
                                        updateReqObj.via = routesDict ["summary"] as? String
                                        updateReqObj.waypoints = points as String
                                    }
                                    
                                    showIndicator("Updating Ride..")
                                    RideRequestor().updateRide(updateReqObj, success: { (success, object) in
                                        hideIndicator()
                                        
                                        if(success == true){
                                            let poolID = (self.rideDetailData?.poolID)!
                                            
                                            showIndicator("Fetching Ride Details")
                                            RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                                hideIndicator()
                                                if(success){
                                                    let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                                    let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                                    //let vc = RideDetailViewController()
                                                    vc.hidesBottomBarWhenPushed = true
                                                    vc.edgesForExtendedLayout = UIRectEdge()
                                                    vc.data = object as? RideDetail
                                                    vc.triggerFrom = "ride"
                                                    self.navigationController?.pushViewController(vc, animated: true)
                                                }
                                            }) { (error) in
                                                hideIndicator()
                                            }
                                        }
                                        else{
                                            AlertController.showToastForError((object as! BaseModel).message!)
                                        }
                                        
                                        }, failure: { (error) in
                                            hideIndicator()
                                    })
                                    }, failure: { (error) in
                                })
                                }, failure: { (error) in
                                    showIndicator("Updating Ride..")
                                    RideRequestor().updateRide(updateReqObj, success: { (success, object) in
                                        hideIndicator()
                                        
                                        if(success == true){
                                            let poolID = (self.rideDetailData?.poolID)!
                                            
                                            showIndicator("Fetching Ride Details")
                                            RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                                hideIndicator()
                                                if(success){
                                                    let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                                    let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                                    //let vc = RideDetailViewController()
                                                    vc.hidesBottomBarWhenPushed = true
                                                    vc.edgesForExtendedLayout = UIRectEdge()
                                                    vc.data = object as? RideDetail
                                                    vc.triggerFrom = "ride"
                                                    self.navigationController?.pushViewController(vc, animated: true)
                                                }
                                            }) { (error) in
                                                hideIndicator()
                                            }
                                        }
                                        else{
                                            AlertController.showToastForError((object as! BaseModel).message!)
                                        }
                                        
                                        }, failure: { (error) in
                                            hideIndicator()
                                    })
                            })
                        }
                        else{
                            AlertController.showAlertFor("Offer a Vehicle", message: "Please provide vehicle details.")
                        }
                    }
                    else{
                        AlertController.showAlertFor("Offer a Vehicle", message: "Please enter date and time for the Ride.")
                    }
                }
                else{
                    AlertController.showAlertFor("Offer a Vehicle", message: "Please choose different locations for source and destination.")
                }
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'From' and 'To' field from Google Places.")
            }
        }
        else{
            let searchReqObj = SearchRideRequest()
            searchReqObj.userId = UserInfo.sharedInstance.userID
            searchReqObj.reqDate = txtDate.text
            searchReqObj.reqTime = txtTime.text
            searchReqObj.poolType = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
            
            searchReqObj.startLoc = offerVehicleObj.fromLocation
            searchReqObj.fromLat = offerVehicleObj.fromLat
            searchReqObj.fromLong = offerVehicleObj.fromLong
            searchReqObj.endLoc = offerVehicleObj.toLocation
            searchReqObj.toLat = offerVehicleObj.toLat
            searchReqObj.toLong = offerVehicleObj.toLong
            searchReqObj.poolType = self.btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
            searchReqObj.searchType = "recurrent"
            searchReqObj.ridetype = ""
            var selectedDates = [String]()
            selectedDates.append(txtDate.text!)
            if(selectedDates.count > 0){
                searchReqObj.reqCurrentDate = selectedDates.joined(separator: ",")
            }
            
            offerVehicleObj.userId = UserInfo.sharedInstance.userID
            offerVehicleObj.poolType = btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
            offerVehicleObj.vehregno = txtVehicle.text!
            offerVehicleObj.veh_capacity = self.stepperSeats.countLabel.text!
            if let cost = stepperAmt.countLabel.text{
                offerVehicleObj.cost = cost
            }
            offerVehicleObj.ridemessage = txtComments.text
            
            if((offerVehicleObj.fromLocation != nil) && (offerVehicleObj.toLocation != nil)){
                if(offerVehicleObj.fromLocation != offerVehicleObj.toLocation){
                    if((offerVehicleObj.rideDate != nil) && (offerVehicleObj.rideTime != nil))
                    {
                        if(offerVehicleObj.vehregno != ""){
                            showIndicator("Searching for similair rides..")
                            RideRequestor().searchRides(searchReqObj, success: { (success, object) in
                                hideIndicator()
                                if(success){
                                    if((object as! AllRidesResponse).pools!.count > 0){
                                        let vc = SearchResultViewController()
                                        vc.hidesBottomBarWhenPushed = true
                                        vc.taxiRide = "0"
                                        vc.dataSource = (object as! AllRidesResponse).pools!
                                        vc.onAddNewBlock = {
                                            CommonRequestor().getDistanceDuration(self.offerVehicleObj.fromLocation!, destination: self.offerVehicleObj.toLocation!, success: { (success, object) in
                                                self.offerVehicleObj.duration = ""
                                                self.offerVehicleObj.distance = ""
                                                if let data = object as? NSDictionary{
                                                    let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                                                    let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                                                    if((elements["status"] as! String) == "OK")
                                                    {
                                                        let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                                        let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                                        self.offerVehicleObj.duration = duration as? String
                                                        self.offerVehicleObj.distance = distance as? String
                                                    }
                                                }
                                                let sourceStr = "\(self.offerVehicleObj.fromLat!),\(self.offerVehicleObj.fromLong!)"
                                                let destinationStr = "\(self.offerVehicleObj.toLat!), \(self.offerVehicleObj.toLong!)"
                                                CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                                                    self.offerVehicleObj.via = ""
                                                    self.offerVehicleObj.waypoints = ""
                                                    if let data = object as? NSDictionary{
                                                        let routes = data["routes"] as! NSArray
                                                        if routes.count == 0{
                                                            return
                                                        }
                                                        let routesDict = routes[0] as! NSDictionary
                                                        let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                                                        let points = overview_polyline ["points"] as! NSString
                                                        self.offerVehicleObj.via = routesDict ["summary"] as? String
                                                        self.offerVehicleObj.waypoints = points as String
                                                    }
                                                    self.createNewOfferVehicle()
                                                    }, failure: { (error) in
                                                        self.createNewOfferVehicle()
                                                })
                                                }, failure: { (error) in
                                                    self.createNewOfferVehicle()
                                            })
                                        }
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                    else{
                                        CommonRequestor().getDistanceDuration(self.offerVehicleObj.fromLocation!, destination: self.offerVehicleObj.toLocation!, success: { (success, object) in
                                            self.offerVehicleObj.duration = ""
                                            self.offerVehicleObj.distance = ""
                                                if let data = object as? NSDictionary{
                                                    let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                                                    let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                                                    if((elements["status"] as! String) == "OK")
                                                    {
                                                        let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                                        let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                                    self.offerVehicleObj.duration = duration as? String
                                                    self.offerVehicleObj.distance = distance as? String
                                                }
                                            }
                                            let sourceStr = "\(self.offerVehicleObj.fromLat!),\(self.offerVehicleObj.fromLong!)"
                                            let destinationStr = "\(self.offerVehicleObj.toLat!), \(self.offerVehicleObj.toLong!)"
                                            CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                                                self.offerVehicleObj.via = ""
                                                self.offerVehicleObj.waypoints = ""
                                                if let data = object as? NSDictionary{
                                                    let routes = data["routes"] as! NSArray
                                                    if routes.count == 0{
                                                        return
                                                    }
                                                    let routesDict = routes[0] as! NSDictionary
                                                    let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                                                    let points = overview_polyline ["points"] as! NSString
                                                    self.offerVehicleObj.via = routesDict ["summary"] as? String
                                                    self.offerVehicleObj.waypoints = points as String
                                                }
                                                self.createNewOfferVehicle()
                                                }, failure: { (error) in
                                                    self.createNewOfferVehicle()
                                            })
                                            }, failure: { (error) in
                                                self.createNewOfferVehicle()
                                        })
                                    }
                                }
                            }) { (error) in
                                hideIndicator()
                            }
                        }
                        else{
                            AlertController.showAlertFor("Offer a Vehicle", message: "Please provide vehicle details.")
                        }
                    }
                    else{
                        AlertController.showAlertFor("Offer a Vehicle", message: "Please enter date and time for the Ride.")
                    }
                }
                else{
                    AlertController.showAlertFor("Offer a Vehicle", message: "Please choose different locations for source and destination.")
                }
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'From' and 'To' field from Google Places.")
            }
        }
    }
    
    func createNewOfferVehicle()
    {
        showIndicator("Creating Offer a vehicle..")
        OfferVehicleRequestor().createOfferVehicle(offerVehicleObj, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Offer a Vehicle", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! OfferVehicleResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.triggerFrom = "ride"
                            vc.signupFlow = self.signupFlow
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! OfferVehicleResponse).code == "173") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! OfferVehicleResponse).message!)
                }
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
}
