//
//  MessageViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 25/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    var contactListArray : NSMutableArray = []
    var phoneMutArr : NSMutableArray = []
    var selectedPhoneMutArr : NSMutableArray = []
    var poolID: String?
    var responses: [InviteResult]?
    var triggerFrom = ""
    
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var messageTableView: UITableView!
    // cell reuse id (cells that scroll out of view can be reused)
    let cellReuseIdentifier = "Cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        saveBtn.isUserInteractionEnabled = false
        messageTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        messageTableView.delegate = self
        messageTableView.dataSource = self
        self.messageTableView.allowsMultipleSelection = true
        //self.showContact()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactListArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->  UITableViewCell
    {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell!
        if (self.contactListArray.count > 0)
        {
            cell.textLabel?.text = self.contactListArray[indexPath.row] as? String
            if cell.isSelected
            {
                cell.isSelected = false
                if cell.accessoryType == UITableViewCellAccessoryType.none
                {
                    cell.accessoryType = UITableViewCellAccessoryType.checkmark
                }
                else
                {
                    cell.accessoryType = UITableViewCellAccessoryType.none
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let cell = tableView.cellForRow(at: indexPath)
        let selectedPhone = self.phoneMutArr[indexPath.row]
        
        if cell!.isSelected
        {
            cell!.isSelected = false
            if cell!.accessoryType == UITableViewCellAccessoryType.none
            {
                cell!.accessoryType = UITableViewCellAccessoryType.checkmark
                self.selectedPhoneMutArr.add(selectedPhone)
            }
            else
            {
                cell!.accessoryType = UITableViewCellAccessoryType.none
                self.selectedPhoneMutArr.remove(selectedPhone)
            }
        }
        if(selectedPhoneMutArr.count != 0) {
            saveBtn.isUserInteractionEnabled = true
        } else {
            saveBtn.isUserInteractionEnabled = false
        }
    }
    
    @IBAction func backButtonClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareButtonClicked(_ sender: AnyObject) {
        let stringRepresentation = self.selectedPhoneMutArr.componentsJoined(by: ",")
        if(triggerFrom != "") {
            if(triggerFrom == "wpMap") {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "KInviteSendNotificationWpDetail"),
                                                                          object: nil,
                                                                          userInfo: ["phoneNumber":stringRepresentation,"poolId":poolID!])
            } else if(triggerFrom == "wpRide") {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "KInviteSendNotificationWpRideDetail"),
                                                                          object: nil,
                                                                          userInfo: ["phoneNumber":stringRepresentation,"poolId":poolID!])
            } else if(triggerFrom == "allRides") {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "KInviteSendNotificationallRides"),
                                                                          object: nil,
                                                                          userInfo: ["phoneNumber":stringRepresentation,"poolId":poolID!])
            } else if(triggerFrom == "createRide") {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "KInviteSendNotificationcreateRide"),
                                                                          object: nil,
                                                                          userInfo: ["phoneNumber":stringRepresentation,"poolId":poolID!])
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "KInviteSendNotificationDetail"),
                                                                          object: nil,
                                                                          userInfo: ["phoneNumber":stringRepresentation,"poolId":poolID!])
            }
        } else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "KInviteSendNotification"),
                                                                      object: nil,
                                                                      userInfo: ["phoneNumber":stringRepresentation,"poolId":poolID!])
        }
    }
}
