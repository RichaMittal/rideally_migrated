//
//  SearchModelViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 14/02/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps

class SearchModelViewController: UIViewController,GMSMapViewDelegate {
    
    @IBOutlet weak var listViewContainer: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var workDetails: UILabel!
    @IBOutlet weak var homeDetails: UILabel!
    var locationCoord:CLLocationCoordinate2D?
    
    var isControlerType: String!
    
    var placeSearch : String!
    let autoCompletedNotificationKey = "com.RideAlly.AutoCompletedNotificationKey"
    let updateTextFieldNotificationKey = "com.RideAlly.UpdateTextFieldNotificationKey"
    var isNoActive = true
    override func viewDidLoad() {
        super.viewDidLoad()
        // Hide Auto Complete list view. a
        self.listViewContainer.isHidden = true
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(SearchModelViewController.updateTextFieldNotification), name: NSNotification.Name(rawValue: updateTextFieldNotificationKey), object: nil)
        
        if self.isControlerType == "Update" {
         self.homeDetails.text = placeSearch
        }else{
        self.homeDetails.text = UserInfo.sharedInstance.homeAddress
        self.workDetails.text = UserInfo.sharedInstance.workAddress
        }
        searchTextField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonClicked(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {});
    }
    
    //MARK:- Text Field Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        
        if range.location == 0 {
            self.isNoActive = true
            self.listViewContainer.isHidden = true
            return true
        }
        
        let searchText = textField.text! + string
        if searchText.characters.count != 0 {
            self.isNoActive = false
            self.listViewContainer.isHidden = false
            self.findSearchResultsForText(searchText)
        }else{
            self.listViewContainer.isHidden = true
            self.isNoActive = true
        }
        return true
    }
    
    //MARK:- place search
    func findSearchResultsForText(_ searchText: String) {
        let characterSet = CharacterSet.letters
        let range = searchText.rangeOfCharacter(from: characterSet)
        if (range != nil) {
            // self.getPlacesForText(searchText)
            print(searchText, "textField Place Search")
            NotificationCenter.default.post(name: Notification.Name(rawValue: autoCompletedNotificationKey),
                                                                      object: nil,
                                                                      userInfo: ["searchText":searchText])
        }
    }
    
    //MARK:- Update TextField
    func updateTextFieldNotification (_ notification:Notification) -> Void {
        
        guard let userInfo = notification.userInfo else { return }
        if let placeName = userInfo["searchText"] as? String {
            self.placeSearch = placeName
        }
        
        var latitute = 0.0
        if let lat = userInfo["latitude"] as? Double {
            latitute = lat
        }
        var longitude = 0.0
        if let long = userInfo["longitute"] as? Double {
            longitude = long
        }
        self.locationCoord = CLLocationCoordinate2D(latitude: latitute, longitude: longitude)
        self.searchTextField.text = self.placeSearch;
        self.listViewContainer.isHidden = true
        
    }
    
    // MARK:- Segue Actions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            if segue.identifier == "chooseLocationSegue" {
                if let navController = segue.destination as? UINavigationController {
                    
                    if let chidVC = navController.topViewController as? ChoosLocationViewController {

                        if isControlerType == "Update" {
                            chidVC.locationAddress = self.placeSearch
                            chidVC.locationCoord = locationCoord
                            chidVC.isControlerType = "Update"
                        }else {
                            chidVC.isControlerType = ""
                        if isNoActive == true {
                            
                            let location = CLLocationCoordinate2D(latitude: Double(UserInfo.sharedInstance.homeLatValue)!, longitude: Double(UserInfo.sharedInstance.homeLongValue)!)
                        chidVC.locationAddress = UserInfo.sharedInstance.homeAddress
                        chidVC.locationCoord = location
                        }else {
                            chidVC.locationAddress = placeSearch
                            chidVC.locationCoord = self.locationCoord
                        }
                    }
                    }
                }
                
            } else {
                
            }
        }
    }
}
