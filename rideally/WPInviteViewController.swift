//
//  WPInviteViewController.swift
//  rideally
//
//  Created by Sarav on 28/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class WPInviteViewController: InviteViewController {

    var groupID: String?
    var wpresponses: [InviteWPResult]?

    
    override func sendRequest() {
        showIndicator("Sending Invites..")
        WorkplaceRequestor().sendWPInvite(groupID!, userId: ownerID!, emailIDs: "", mobileNOs: txtMobile.text, success: { (success, object) in
            hideIndicator()
            
            if(success){
                self.wpresponses = (object as! InviteMembersWPResponse).data
                let view = self.getInviteResposeView()
                (view.viewWithTag(33) as? UITableView)?.reloadData()
                
                let alertViewHolder =   AlertContentViewHolder()
                alertViewHolder.heightConstraintValue = 100
                alertViewHolder.view = view
                
                AlertController.showAlertFor("Invite Members", message: "", contentView: alertViewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: true, okAction: {
                    _ = self.navigationController?.popViewController(animated: true)
                    self.onCompletion?()
                    }, cancelButtonTitle: nil, cancelAction: nil)
            }

            
            }) { (error) in
                hideIndicator()
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wpresponses!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = normalFontWithSize(14)
        cell.textLabel?.numberOfLines = 0
        if (wpresponses![indexPath.row].type == "mobile"){
            cell.textLabel?.text = "\(wpresponses![indexPath.row].mobile!) - \(wpresponses![indexPath.row].result!)"
        }else{
            cell.textLabel?.text = "\(wpresponses![indexPath.row].email!) - \(wpresponses![indexPath.row].result!)"
        }
        return cell
    }
}
