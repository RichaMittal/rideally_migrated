//
//  VerifyMobileNumberView.swift
//  rideally
//
//  Created by Raghunathan on 8/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class VerifyMobileNumberView: UIView , UITextFieldDelegate {
    
    var mobileNumberTextBox = RATextField()
    var lblMobileCode = UILabel()
    var referralLabelText = UILabel()
    var referralTextBox = RATextField()
    var referralUserName = UILabel()
    var referralApplyText = UILabel()
    var referralInfoImage = UIImageView()
    var referredBy = ""
    var isInavlidCode = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createVerifyMobileNumberView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func createVerifyMobileNumberView() -> Void {
        
        self.backgroundColor = Colors.WHITE_COLOR
        
        let titleLabel = UILabel()
        titleLabel.font = normalFontWithSize(15)
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = Colors.GREEN_COLOR
        titleLabel.text="Verify Mobile Number"
        titleLabel.textAlignment = NSTextAlignment.left
        
        let lineSeperator = UIView()
        lineSeperator.backgroundColor = Colors.GREEN_COLOR
        
        lblMobileCode.text = "+91"
        lblMobileCode.textColor = Colors.GRAY_COLOR
        lblMobileCode.font = boldFontWithSize(14)
        lblMobileCode.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(lblMobileCode)
        
        mobileNumberTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        mobileNumberTextBox.font = normalFontWithSize(15)!
        mobileNumberTextBox.textColor = Colors.GRAY_COLOR
        mobileNumberTextBox.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberTextBox.returnKeyType = UIReturnKeyType.next
        mobileNumberTextBox.delegate = self
        mobileNumberTextBox.placeholder = "ENTER MOBILE NUMBER"
        
        let image = UIImage(named: "Infoicon_user")
        // frame1 = CGRectMake(0, 0, (image?.size.width)!, (image?.size.height)!)
        //referralInfoImage.frame = frame1
        referralInfoImage.image = image
        referralInfoImage.contentMode = UIViewContentMode.center
        let referralInfotapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(VerifyMobileNumberView.referralInfoImageTapped(_:)))
        referralInfoImage.isUserInteractionEnabled = true
        referralInfoImage.addGestureRecognizer(referralInfotapGestureRecognizer)
        
        referralLabelText.font = normalFontWithSize(15)
        referralLabelText.backgroundColor = UIColor.clear
        referralLabelText.isUserInteractionEnabled = true
        let myReferralAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myReferralString = NSMutableAttributedString(string: "Have a Referral Code?", attributes: myReferralAttribute)
        let myReferralRange = NSRange(location: 0, length: 21)
        myReferralString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myReferralRange)
        referralLabelText.attributedText = myReferralString
        let referralTapGesture = UITapGestureRecognizer(target:self, action:#selector(showTextField))
        referralTapGesture.numberOfTapsRequired = 1
        referralTapGesture.numberOfTouchesRequired = 1
        referralLabelText.addGestureRecognizer(referralTapGesture)
        
        
        referralApplyText.font = normalFontWithSize(15)
        referralApplyText.backgroundColor = UIColor.clear
        let myApplyAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myApplyString = NSMutableAttributedString(string: "APPLY", attributes: myApplyAttribute)
        let myApplyRange = NSRange(location: 0, length: 5)
        myApplyString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myApplyRange)
        referralApplyText.attributedText = myApplyString
        let referralApplyTapGesture = UITapGestureRecognizer(target:self, action:#selector(applyReferralCode))
        referralApplyTapGesture.numberOfTapsRequired = 1
        referralApplyTapGesture.numberOfTouchesRequired = 1
        referralApplyText.addGestureRecognizer(referralApplyTapGesture)
        
        referralTextBox.delegate = self
        referralTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        referralTextBox.placeholder = "REFERRAL CODE (OPTIONAL)"
        referralTextBox.font = normalFontWithSize(15)!
        referralTextBox.textColor = Colors.GRAY_COLOR
        referralTextBox.rightView = referralApplyText
        referralTextBox.rightViewMode = UITextFieldViewMode.always
        referralTextBox.returnKeyType = UIReturnKeyType.done
        referralTextBox.autocapitalizationType = .allCharacters
        referralTextBox.addTarget(self, action: #selector(VerifyMobileNumberView.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        referralUserName.font = normalFontWithSize(15)
        referralUserName.backgroundColor = UIColor.clear
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        lineSeperator.translatesAutoresizingMaskIntoConstraints = false
        referralLabelText.translatesAutoresizingMaskIntoConstraints = false
        referralTextBox.translatesAutoresizingMaskIntoConstraints = false
        referralUserName.translatesAutoresizingMaskIntoConstraints = false
        referralInfoImage.translatesAutoresizingMaskIntoConstraints = false
        referralApplyText.translatesAutoresizingMaskIntoConstraints = false
        
        self .addSubview(titleLabel)
        self.addSubview(lineSeperator)
        self.addSubview(mobileNumberTextBox)
        self.addSubview(referralLabelText)
        self.addSubview(referralTextBox)
        self.addSubview(referralUserName)
        self.addSubview(referralInfoImage)
        
        referralTextBox.isHidden = true
        referralApplyText.isHidden = true
        
        if(UserInfo.sharedInstance.refCode != "") {
            referralLabelText.isHidden = true
            referralInfoImage.isHidden = true
            referralTextBox.isHidden = false
            referralTextBox.isUserInteractionEnabled = false
            referralTextBox.text = UserInfo.sharedInstance.refCode
            referralApplyText.isHidden = false
            referralApplyText.text = "APPLIED"
            referralApplyText.textColor = Colors.GREEN_COLOR
            referralApplyText.isUserInteractionEnabled = false
        } else {
            referralLabelText.isHidden = false
            referralInfoImage.isHidden = false
        }
        
        let subViewsDict = ["title": titleLabel,"line":lineSeperator,"lblmobilecode":lblMobileCode,"mobilenumber":mobileNumberTextBox,"referralLabel":referralLabelText,"referralText":referralTextBox,"referralName":referralUserName,"referralInfo":referralInfoImage]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[title(20)]-1-[line(1)]-15-[mobilenumber(30)]-10-[referralLabel]-1-[referralText(30)]-2-[referralName]|", options:[], metrics:nil, views: subViewsDict))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[title]-10-|", options:[], metrics:nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options:[], metrics:nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblmobilecode]-5-[mobilenumber]-10-|", options: [], metrics: nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[referralLabel]-15-[referralInfo]-1-|", options: [], metrics: nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[referralText]-10-|", options: [], metrics: nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[referralName]-10-|", options: [], metrics: nil, views: subViewsDict))
        self.addConstraint(NSLayoutConstraint(item: lblMobileCode, attribute: .centerY, relatedBy: .equal, toItem: mobileNumberTextBox, attribute: .centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: referralInfoImage, attribute: .centerY, relatedBy: .equal, toItem: referralLabelText, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    func showTextField() {
        self.referralLabelText.isHidden = true
        self.referralTextBox.isHidden = false
        self.referralInfoImage.isHidden = true
    }
    func referralInfoImageTapped(_ imgObject: AnyObject) -> Void {
        AlertController.showToastForInfo("With the referral code, you will get bonus points once you creates a ride.")
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if(textField == referralTextBox) {
            textField.text = textField.text!.uppercased(with: Locale.current)
            self.isInavlidCode = false
            if(referralTextBox.text?.characters.count == 0) {
                self.referralUserName.isHidden = true
            } else {
                self.referralUserName.isHidden = false
                isReferralCodeValid()
            }
        }
    }
    
    func isReferralCodeValid () {
        let reqObj = referralCodeRequestModel()
        reqObj.ref_code = referralTextBox.text
        SignUpRequestor().isReferralCodeValid(reqObj, success:{(success, object) in
            if (object as! referralCodeResponseModel).code == "111" {
                self.isInavlidCode = false
                self.referralApplyText.isHidden = false
                self.referralApplyText.isUserInteractionEnabled = true
                self.referredBy = ((object as! referralCodeResponseModel).dataObj?.first?.firstName)!
                self.referralUserName.textColor = Colors.GREEN_COLOR
                self.referralUserName.text = (object as! referralCodeResponseModel).message!+" "+((object as! referralCodeResponseModel).dataObj?.first?.firstName)!
            } else {
                self.isInavlidCode = true
                self.referralUserName.textColor = Colors.RED_COLOR
                self.referralUserName.text = (object as! referralCodeResponseModel).message
            }
        }) { (error) in
        }
    }
    
    func applyReferralCode () {
        if(isInavlidCode) {
            AlertController.showToastForError("Please provide valid 'Referral Code'.")
        } else {
            mobileNumberTextBox.resignFirstResponder()
            let reqObj = applyRefCodeRequestModel()
            reqObj.coupon_code = referralTextBox.text
            reqObj.user_id = UserInfo.sharedInstance.userID
            reqObj.device_id = UIDevice.current.identifierForVendor?.uuidString
            reqObj.referred_by = self.referredBy
            showIndicator("Applying Referral Code...")
            SignUpRequestor().applyRefCode(reqObj, success:{(success, object) in
                hideIndicator()
                if (object as! applyRefCodeResponseModel).code == "101010" {
                    AlertController.showToastForInfo("Congratulations, Referral code applied successfully. You will get bonus points once you creates a ride.")
                    UserInfo.sharedInstance.refCode = self.referralTextBox.text!
                    self.referralTextBox.isUserInteractionEnabled = false
                    self.referralApplyText.text = "APPLIED"
                    self.referralApplyText.textColor = Colors.GREEN_COLOR
                    self.referralApplyText.isUserInteractionEnabled = false
                    self.referralUserName.isHidden = true
                } else {
                }
            }) { (error) in
                hideIndicator()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == mobileNumberTextBox)
        {
            mobileNumberTextBox.resignFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == mobileNumberTextBox) {
            let lenghtValue = (textField.text?.characters.count)! + string.characters.count - range.length
            if lenghtValue > 10
            {
                return false
            }
            else
            {
                let aSet = CharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
                
            }
        }
        return true
    }
}
