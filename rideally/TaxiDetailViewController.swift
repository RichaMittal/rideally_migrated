//
//  TaxiDetailViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 12/22/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import Kingfisher
import CoreLocation
import GoogleMaps
import GooglePlaces


class TaxiDetailViewController: BaseScrollViewController, CLLocationManagerDelegate {
    
    var data: RideDetail?
    
    lazy var bottomBtnsView: SubView = self.getBtnView()
    lazy var commentsView: SubView = self.getCommentsView()
    lazy var vehicleView: SubView = self.getVehicleView()
    
    var btnConstraints: [NSLayoutConstraint]?
    
    var onDeleteActionBlock: actionBlock?
    var isFirstLevel = false
    
    var taxiSourceID = ""
    var taxiDestID = ""
    var ISPROMOCODEAPPLIED = false
    var onSelectBackBlock: actionBlock?
    var signupFlow = false
    
    override func goBack()
    {
        if(isFirstLevel && !ISPROMOCODEAPPLIED){
            _ = self.navigationController?.popViewController(animated: true)
            _ = onSelectBackBlock?()
        } else if(signupFlow) {
            let ins = UIApplication.shared.delegate as! AppDelegate
            ins.gotoHome()
        } else {
           _ =  self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateRidesList"), object: nil)
        }
    }
    
    func comingFromPayment () {
        ISPROMOCODEAPPLIED = true
        updateRideDetails()
    }
    
    func updateRideDetails()
    {
        showIndicator("Updating Ride details..")
        
        RideRequestor().getTaxiDetails(data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                self.data = object as? RideDetail
                self.updateScreenWithNewData()
            }
        }) { (error) in
            hideIndicator()
            AlertController.showAlertForMessage("Failed to update ride deails. Try after sometime.")
        }
    }
    func updateScreenWithNewData()
    {
        setVisibleBtns(bottomBtnsView.view!)
        updateData()
    }
    
    func updateData()
    {
        lblName.text = "RideAlly Taxi Services"
        lblSource.text = data?.source
        lblDest.text = data?.destination
        
        lblPickUp.text = data?.pickUpPoint
        lblDrop.text = data?.dropPoint
        
        lblTravelWith.text = "Any"
        if(data?.poolType == "M"){
            lblTravelWith.text = "Only Male"
        }
        else if(data?.poolType == "F"){
            lblTravelWith.text = "Only Female"
        }
        
        lblTravelDate.text = data?.startDateDisplay
        lblTravelTime.text = data?.startTimeDisplay
        
        lblTravelDistance.text = (data?.distance)! + "\(" KMs")"
        if(data?.poolTaxiBooking == "1") {
            if(Int((data?.duration)!)! >= 60) {
                let hours = Int((data?.duration)!)! / 60
                let minutes = Int((data?.duration)!)! % 60
                var hrText = " hours "
                var minText = " mins"
                if(hours == 1) {
                    hrText = " hour "
                }
                if(minutes == 1) {
                    minText = " min"
                }
                lblTravelDuration.text = "\(hours) \(hrText)" + "\(minutes) \(minText)"
            } else {
                lblTravelDuration.text = (data?.duration)! + "\(" mins")"
            }
        } else {
            lblTravelDuration.text = data?.duration
        }
        
        lblCount.text = "-"
        
        if data?.poolTaxiBooking == "1"{
            lblCount.text = data?.poolJoinedMembers
        }
        else{
            if let cnt = data?.poolJoinedMembers{
                lblCount.text = "\(Int(cnt)! + 1)"
                
            }
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
        lblMembersTitle.addGestureRecognizer(tapGesture)
        viewsArray.append(vehicleView)
        
        if((data?.comments != nil) && (data?.comments != "")){
            viewsArray.append(commentsView)
        }
        
        lblComments.text = data?.comments
        
        if(data?.createdBy == UserInfo.sharedInstance.userID && data?.poolTaxiBooking == "0"){
            lblVerification.isHidden = false
            verificationView.isHidden = true
            if(data?.poolType == "J"){
                lblVerification.text = "Joined"
            }
            else{
                lblVerification.text = "Started By Me"
            }
        }
        else{
            lblVerification.isHidden = true
            verificationView.isHidden = false
            if(data?.userState == "5"){
                self.emailIcon.image = UIImage(named: "email_v")
                self.mobileIcon.image = UIImage(named: "phone_v")
            }
            else if(data?.userState == "4"){
                self.mobileIcon.image = UIImage(named: "phone_v")
            }
            else if(data?.userState == "3"){
                self.emailIcon.image = UIImage(named: "email_v")
            }
        }
    }
    
    func gotoMemberDetail()
    {
        showIndicator("Getting owner's detail..")
        RideRequestor().getMembersDetails((data?.createdBy)!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                let vc = MemberDetailViewController()
                vc.dataSource = (object as? MemberDetailResponse)?.data?.userInfo
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        
        title = "Ride Details"
        
        viewsArray.append(getProfileView())
        viewsArray.append(getLocationView())
        
        if(data?.poolShared == "0") {
            viewsArray.append(getPersonalRideDetailsView())
            //viewsArray.append(getMembersView())
        } else {
            viewsArray.append(getSharedRideDetailsView())
            //viewsArray.append(getMembersView())
        }
        viewsArray.append(vehicleView)
        if((data?.comments != nil) && (data?.comments != "")){
            viewsArray.append(commentsView)
        }
        
        addBottomView(bottomBtnsView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(comingFromPayment), name: NSNotification.Name(rawValue: "updateTaxiDetail"), object: nil)
    }
    
    let lblName = UILabel()
    let emailIcon = UIImageView()
    let mobileIcon = UIImageView()
    let verificationView = UIView()
    let lblVerification = UILabel()
    
    func getProfileView() -> SubView
    {
        let view = UIView()
        let imgProfilePic = UIImageView()
        imgProfilePic.translatesAutoresizingMaskIntoConstraints = false
        imgProfilePic.backgroundColor = UIColor.clear
        view.addSubview(imgProfilePic)
        
        if(data?.ownerImgStatus == "1"){
            if let url = data?.ownerImgUrl{
                imgProfilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
            }
            else{
                if let fbID = data?.ownerFBID{
                    imgProfilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            imgProfilePic.image = UIImage(named: "placeholder")
        }
        
        verificationView.backgroundColor = Colors.BLACK_COLOR
        verificationView.alpha = 0.8
        verificationView.translatesAutoresizingMaskIntoConstraints = false
        emailIcon.image = UIImage(named: "email")
        emailIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(emailIcon)
        mobileIcon.image = UIImage(named: "phone")
        mobileIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(mobileIcon)
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[email(15)]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mobile(15)]", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mobile]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[email]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        
        imgProfilePic.addSubview(verificationView)
        
        imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":verificationView]))
        imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(15)]|", options: [], metrics: nil, views: ["view":verificationView]))
        
        lblVerification.backgroundColor = Colors.BLACK_COLOR
        lblVerification.translatesAutoresizingMaskIntoConstraints = false
        lblVerification.alpha = 0.8
        lblVerification.textAlignment = .center
        lblVerification.textColor = Colors.WHITE_COLOR
        lblVerification.font = normalFontWithSize(9)
        lblVerification.isHidden = true
        imgProfilePic.addSubview(lblVerification)
        
        imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblVerification]|", options: [], metrics: nil, views: ["lblVerification":lblVerification]))
        imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lblVerification(15)]|", options: [], metrics: nil, views: ["lblVerification":lblVerification]))
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.text = "RideAlly Taxi Services"
        lblName.textColor = Colors.GREEN_COLOR
        lblName.textAlignment = .center
        lblName.font = normalFontWithSize(14)
        view.addSubview(lblName)
        
        /*imgProfilePic.userInteractionEnabled = true
         lblName.userInteractionEnabled = true
         
         let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(gotoMemberDetail))
         imgProfilePic.addGestureRecognizer(tapGesture1)
         let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(gotoMemberDetail))
         lblName.addGestureRecognizer(tapGesture2)*/
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(80)]-10-[lbl(20)]-10-|", options: [], metrics: nil, views: ["pic":imgProfilePic, "lbl":lblName]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblName]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[pic(60)]", options: [], metrics: nil, views: ["pic":imgProfilePic]))
        view.addConstraint(NSLayoutConstraint(item: imgProfilePic, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        
        if(data?.createdBy == UserInfo.sharedInstance.userID && data?.poolTaxiBooking == "0"){
            lblVerification.isHidden = false
            verificationView.isHidden = true
            if(data?.poolType == "J"){
                lblVerification.text = "Joined"
            }
            else{
                lblVerification.text = "Started By Me"
            }
        }
        else{
            lblVerification.isHidden = true
            verificationView.isHidden = false
            if(data?.userState == "5"){
                self.emailIcon.image = UIImage(named: "email_v")
                self.mobileIcon.image = UIImage(named: "phone_v")
            }
            else if(data?.userState == "4"){
                self.mobileIcon.image = UIImage(named: "phone_v")
            }
            else if(data?.userState == "3"){
                self.emailIcon.image = UIImage(named: "email_v")
            }
        }
        let subview = SubView()
        subview.view = view
        subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        subview.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 130)
        return subview
    }
    
    let lblSource = UILabel()
    let lblPickUp = UILabel()
    let lblDest = UILabel()
    let lblDrop = UILabel()
    
    func getLocationView() -> SubView
    {
        let view = UIView()
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconSource)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.text = data?.source
        lblSource.font = boldFontWithSize(14)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblSource)
        
        let lblPickupTitle = UILabel()
        lblPickupTitle.translatesAutoresizingMaskIntoConstraints = false
        lblPickupTitle.text = "START POINT"
        lblPickupTitle.font = normalFontWithSize(11)
        lblPickupTitle.textColor = Colors.LABEL_DULL_TXT_COLOR
        view.addSubview(lblPickupTitle)
        
        lblPickUp.translatesAutoresizingMaskIntoConstraints = false
        lblPickUp.font = boldFontWithSize(14)
        lblPickUp.textColor = Colors.GRAY_COLOR
        lblPickUp.numberOfLines = 0
        lblPickUp.lineBreakMode = .byWordWrapping
        view.addSubview(lblPickUp)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconDest)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = boldFontWithSize(14)
        lblDest.text = data?.destination
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDest)
        
        let lblDropTitle = UILabel()
        lblDropTitle.translatesAutoresizingMaskIntoConstraints = false
        lblDropTitle.text = "END POINT"
        lblDropTitle.font = normalFontWithSize(11)
        lblDropTitle.textColor = Colors.LABEL_DULL_TXT_COLOR
        view.addSubview(lblDropTitle)
        
        lblDrop.translatesAutoresizingMaskIntoConstraints = false
        lblDrop.font = boldFontWithSize(14)
        lblDrop.textColor = Colors.GRAY_COLOR
        lblDrop.numberOfLines = 0
        lblDrop.lineBreakMode = .byWordWrapping
        view.addSubview(lblDrop)
        
        lblPickUp.text = data?.pickUpPoint
        lblDrop.text = data?.dropPoint
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        let lblShowMap = UILabel()
        lblShowMap.text = "SHOW MAP"
        lblShowMap.font = normalFontWithSize(11)
        lblShowMap.textColor = Colors.GRAY_COLOR
        lblShowMap.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblShowMap)
        
        let iconMap = UIImageView()
        iconMap.image = UIImage(named: "map")
        iconMap.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconMap)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showDirection))
        lblShowMap.isUserInteractionEnabled = true
        lblShowMap.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(showDirection))
        iconMap.isUserInteractionEnabled = true
        iconMap.addGestureRecognizer(tapGesture1)
        
        let lblLine4 = UILabel()
        lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine4.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine4)
        
        var isPickUpPointVisible = true
        var viewHt: CGFloat = 245
        if(data?.poolTaxiBooking == "1"){
            isPickUpPointVisible = false
            viewHt = 120
            lblPickupTitle.isHidden = true
            lblPickUp.isHidden = true
            lblDropTitle.isHidden = true
            lblDrop.isHidden = true
        }
        if let pickupPoint = data?.pickUpPoint{
            if(pickupPoint == ""){
                isPickUpPointVisible = false
                viewHt = 120
                lblPickupTitle.isHidden = true
                lblPickUp.isHidden = true
                lblDropTitle.isHidden = true
                lblDrop.isHidden = true
            }
        }else{
            isPickUpPointVisible = false
            viewHt = 120
            lblPickupTitle.isHidden = true
            lblPickUp.isHidden = true
            lblDropTitle.isHidden = true
            lblDrop.isHidden = true
        }
        
        let viewsDict = ["isource":iconSource, "source":lblSource, "pickupt":lblPickupTitle, "pickup":lblPickUp, "line2":lblLine2, "idest":iconDest, "dest":lblDest, "dropt":lblDropTitle, "drop":lblDrop, "line3":lblLine3, "lblshowmap":lblShowMap, "imap":iconMap, "line4":lblLine4]
        
        if(isPickUpPointVisible == true){
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(40)]-5-[pickupt(15)][pickup(40)]-5-[line2(0.5)][dest(40)]-5-[dropt(15)][drop(40)]-5-[line3(0.5)][lblshowmap(30)][line4(0.5)]", options: [], metrics: nil, views: viewsDict))
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[pickupt]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[pickup]-5-|", options: [], metrics: nil, views: viewsDict))
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[dropt]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[drop]-5-|", options: [], metrics: nil, views: viewsDict))
        }else{
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(40)][line2(0.5)][dest(40)][line3(0.5)][lblshowmap(30)][line4(0.5)]", options: [], metrics: nil, views: viewsDict))
        }
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource(15)][source]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest(15)][dest]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblshowmap][imap(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
        
        
        view.addConstraint(NSLayoutConstraint(item: iconMap, attribute: .centerY, relatedBy: .equal, toItem: lblShowMap, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconMap, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: viewHt)
        return sub
    }
    
    func showDirection()
    {
        let vc = DirectionViewController()
        vc.fromLocation = data?.source
        vc.toLocation = data?.destination
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    let lblTravelWithTitle = UILabel()
    let lblTravelWith = UILabel()
    
    let lblTravelDateTitle = UILabel()
    let lblTravelDate = UILabel()
    
    let lblTravelTimeTitle = UILabel()
    let lblTravelTime = UILabel()
    
    let lblTravelDistanceTitle = UILabel()
    let lblTravelDistance = UILabel()
    
    let lblTravelDurationTitle = UILabel()
    let lblTravelDuration = UILabel()
    
    let lblTripCostTitle = UILabel()
    let lblTripCost = UILabel()
    
    func setLblProp(_ lbl: UILabel, isTitle: Bool, parentView: UIView)
    {
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = boldFontWithSize(14)
        lbl.textColor = Colors.GRAY_COLOR
        
        if(isTitle){
            lbl.font = normalFontWithSize(11)
            lbl.textColor = Colors.LABEL_DULL_TXT_COLOR
        }
        
        parentView.addSubview(lbl)
    }
    
    func showWPDetail()
    {
        //TODO:
        //data?.poolGroups?.first?.id
        let vc = WPDetailViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    let paymentType = UILabel()
    
    func getPersonalRideDetailsView() -> SubView
    {
        let view = UIView()
        
        setLblProp(lblTripCostTitle, isTitle: true, parentView: view)
        setLblProp(lblTripCost, isTitle: false, parentView: view)
        
        setLblProp(lblTravelDateTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelDate, isTitle: false, parentView: view)
        
        setLblProp(lblTravelTimeTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelTime, isTitle: false, parentView: view)
        
        setLblProp(lblTravelDistanceTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelDistance, isTitle: false, parentView: view)
        
        setLblProp(lblTravelDurationTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelDuration, isTitle: false, parentView: view)
        
        setLblProp(paymentType, isTitle: true, parentView: view)
        
        lblTravelDateTitle.text = "TRAVELLING ON"
        lblTravelTimeTitle.text = "RIDE TIME"
        
        lblTravelDate.text = data?.startDateDisplay
        lblTravelTime.text = data?.startTimeDisplay
        
        lblTravelDistanceTitle.text = "TRIP DISTANCE"
        lblTravelDistance.text = (data?.distance)! + "\(" KMs")"
        lblTravelDurationTitle.text = "TRIP DURATION"
        if(data?.poolTaxiBooking == "1") {
            if(Int((data?.duration)!)! >= 60) {
                let hours = Int((data?.duration)!)! / 60
                let minutes = Int((data?.duration)!)! % 60
                var hrText = " hours "
                var minText = " mins"
                if(hours == 1) {
                    hrText = " hour "
                }
                if(minutes == 1) {
                    minText = " min"
                }
                lblTravelDuration.text = "\(hours) \(hrText)" + "\(minutes) \(minText)"
            } else {
                lblTravelDuration.text = (data?.duration)! + "\(" mins")"
            }
        } else {
            lblTravelDuration.text = data?.duration
        }
        
        lblTripCostTitle.text = "TRIP COST : "
        lblTripCost.text = "\(RUPEE_SYMBOL) \(data?.youPay)"
        //lblTripCost.text = "\(RUPEE_SYMBOL) \(String(data?.youPay ?? ""))"
        
        if(data?.payTo == "1") {
            paymentType.text = "(Pay To Driver)"
        } else {
            paymentType.text = "(Pay Online Later)"
        }
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        let lblLine4 = UILabel()
        lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine4.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine4)
        
        let viewsDict = ["datetitle":lblTravelDateTitle, "date":lblTravelDate, "timetitle":lblTravelTimeTitle, "time":lblTravelTime ,"distancetitle":lblTravelDistanceTitle, "distance":lblTravelDistance, "durationtitle":lblTravelDurationTitle, "duration":lblTravelDuration, "tripcosttitle":lblTripCostTitle, "tripcost":lblTripCost, "paymentType":paymentType, "line2":lblLine2, "line3":lblLine3, "line4":lblLine4]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[datetitle(20)][date(20)]-3-[line2(0.5)]-3-[distancetitle(20)][distance(20)]-3-[line3(0.5)]-3-[tripcosttitle(20)]-3-[line4(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[datetitle][timetitle(==datetitle)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[date][time(==date)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblTravelTimeTitle, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDateTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblTravelTime, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDate, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[distancetitle][durationtitle(==distancetitle)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[distance][duration(==distance)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblTravelDurationTitle, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDistanceTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblTravelDuration, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDistance, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[tripcosttitle][tripcost]-5-[paymentType]", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblTripCost, attribute: .centerY, relatedBy: .equal, toItem: lblTripCostTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: paymentType, attribute: .centerY, relatedBy: .equal, toItem: lblTripCost, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 116)
        return sub
    }
    
    func getSharedRideDetailsView() -> SubView
    {
        let view = UIView()
        
        setLblProp(lblTravelWithTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelWith, isTitle: false, parentView: view)
        
        setLblProp(lblTravelDateTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelDate, isTitle: false, parentView: view)
        
        setLblProp(lblTravelTimeTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelTime, isTitle: false, parentView: view)
        
        setLblProp(lblTravelDistanceTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelDistance, isTitle: false, parentView: view)
        
        setLblProp(lblTravelDurationTitle, isTitle: true, parentView: view)
        setLblProp(lblTravelDuration, isTitle: false, parentView: view)
        
        setLblProp(lblTripCostTitle, isTitle: true, parentView: view)
        setLblProp(lblTripCost, isTitle: false, parentView: view)
        
        setLblProp(paymentType, isTitle: true, parentView: view)
        
        lblTravelWithTitle.text = "TRAVEL WITH"
        lblTravelWith.text = "Any"
        if(data?.poolType == "M"){
            lblTravelWith.text = "Only Male"
        }
        else if(data?.poolType == "F"){
            lblTravelWith.text = "Only Female"
        }
        
        lblTravelDateTitle.text = "TRAVELLING ON"
        lblTravelTimeTitle.text = "RIDE TIME"
        
        lblTravelDate.text = data?.startDateDisplay
        lblTravelTime.text = data?.startTimeDisplay
        
        lblTravelDistanceTitle.text = "TRIP DISTANCE"
        lblTravelDistance.text = (data?.distance)! + "\(" KMs")"
        lblTravelDurationTitle.text = "TRIP DURATION"
        if(data?.poolTaxiBooking == "1") {
            if let duration = data?.duration {
                if(duration != "") {
                    if(Int(duration)! >= 60) {
                        let hours = Int(duration)! / 60
                        let minutes = Int(duration)! % 60
                        var hrText = " hours "
                        var minText = " mins"
                        if(hours == 1) {
                            hrText = " hour "
                        }
                        if(minutes == 1) {
                            minText = " min"
                        }
                        lblTravelDuration.text = "\(hours) \(hrText)" + "\(minutes) \(minText)"
                    } else {
                        lblTravelDuration.text = duration + "\(" mins")"
                    }
                }
            }
        } else {
            lblTravelDuration.text = data?.duration
        }
        
        
        lblTripCostTitle.text = "TRIP COST : "
        var tripCost = ""
        for poolUsers in (data?.poolUsers)! {
            if(poolUsers.memberUserID == UserInfo.sharedInstance.userID && poolUsers.joinStatus == "J") {
                tripCost = poolUsers.riderCost ?? ""
                break
            } else {
                tripCost = String(data?.youPay ?? "")
            }
        }
        lblTripCost.text = "\(RUPEE_SYMBOL) \(tripCost)"
        //        if(data?.createdBy != UserInfo.sharedInstance.userID && data?.youPay != 0) {
        //            let youpay = data?.youPay ?? 4
        //            lblTripCost.text = "\(RUPEE_SYMBOL) \(youpay)"
        //        } else {
        //            lblTripCost.text = "\(RUPEE_SYMBOL) \(data?.cost ?? "")"
        //        }
        
        if(data?.payTo == "1") {
            paymentType.text = "(Pay To Driver)"
        } else {
            paymentType.text = "(Pay Online Later)"
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        let lblLine4 = UILabel()
        lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine4.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine4)
        
        let viewsDict = ["travelwithtitle":lblTravelWithTitle, "travelwith":lblTravelWith, "datetitle":lblTravelDateTitle, "date":lblTravelDate, "timetitle":lblTravelTimeTitle, "time":lblTravelTime ,"distancetitle":lblTravelDistanceTitle, "distance":lblTravelDistance, "durationtitle":lblTravelDurationTitle, "duration":lblTravelDuration, "tripcosttitle":lblTripCostTitle, "tripcost":lblTripCost, "paymentType":paymentType, "line1":lblLine1, "line2":lblLine2, "line3":lblLine3, "line4":lblLine4]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[travelwithtitle(20)][travelwith(20)]-3-[line1(0.5)]-3-[datetitle(20)][date(20)]-3-[line2(0.5)]-3-[distancetitle(20)][distance(20)]-3-[line3(0.5)]-3-[tripcosttitle(20)]-3-[line4(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[travelwithtitle]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[travelwith]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[datetitle][timetitle(==datetitle)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[date][time(==date)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblTravelTimeTitle, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDateTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblTravelTime, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDate, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[distancetitle][durationtitle(==distancetitle)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[distance][duration(==distance)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblTravelDurationTitle, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDistanceTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblTravelDuration, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDistance, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[tripcosttitle][tripcost]-5-[paymentType]", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblTripCost, attribute: .centerY, relatedBy: .equal, toItem: lblTripCostTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: paymentType, attribute: .centerY, relatedBy: .equal, toItem: lblTripCost, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 166)
        return sub
    }
    
    let lblMembersTitle = UILabel()
    let lblCount = UILabel()
    func getMembersView() -> SubView
    {
        let view = UIView()
        
        lblMembersTitle.textColor = Colors.GREEN_COLOR
        lblMembersTitle.text = "VIEW MEMBER(S)"
        lblMembersTitle.font = normalFontWithSize(11)
        lblMembersTitle.isUserInteractionEnabled = true
        lblMembersTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblMembersTitle)
        
        lblCount.textColor = Colors.GRAY_COLOR
        lblCount.isUserInteractionEnabled = true
        lblCount.text = "-"
        
        if data?.poolTaxiBooking == "1"{
            lblCount.text = data?.poolJoinedMembers
        }
        else{
            if let cnt = data?.poolJoinedMembers{
                lblCount.text = "\(Int(cnt)! + 1)"
            }
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
        lblMembersTitle.addGestureRecognizer(tapGesture)
        
        lblCount.textAlignment = .right
        lblCount.font = boldFontWithSize(14)
        lblCount.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCount)
        
        let lblLine = UILabel()
        lblLine.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl][cnt(25)]-5-|", options: [], metrics: nil, views: ["lbl":lblMembersTitle, "cnt":lblCount]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl][line(0.5)]|", options: [], metrics: nil, views: ["lbl":lblMembersTitle, "line":lblLine]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[cnt]|", options: [], metrics: nil, views: ["cnt":lblCount]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
        return sub
    }
    
    func showMembersList()
    {
        if((data?.joinStatus != nil && data?.joinStatus == "J") || (data?.createdBy == UserInfo.sharedInstance.userID)){
            let vc = MembersViewController()
            vc.membersList = data?.poolUsers
            vc.poolID = data?.poolID
            vc.poolTaxiBooking = data?.poolTaxiBooking
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            AlertController.showToastForError("Please join the ride to see the members.")
        }
    }
    
    let lblVehicleNoTitle = UILabel()
    let lblVehicleNo = UILabel()
    
    let lblVehicleSeatsTitle = UILabel()
    let lblVehicleSeats = UILabel()
    
    func getVehicleView() -> SubView
    {
        let view = UIView()
        
        setLblProp(lblVehicleNoTitle, isTitle: true, parentView: view)
        setLblProp(lblVehicleNo, isTitle: false, parentView: view)
        
        setLblProp(lblVehicleSeatsTitle, isTitle: true, parentView: view)
        setLblProp(lblVehicleSeats, isTitle: false, parentView: view)
        
        lblVehicleNoTitle.text = "VEHICLE"
        lblVehicleSeatsTitle.text = "SEAT(S) LEFT"
        lblVehicleNo.text = data?.txvtVehModel
        lblVehicleSeats.text = (data?.seatsLeft)! + "\(" Seat(s)")"
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["vehiclenotitle":lblVehicleNoTitle, "vehicleno":lblVehicleNo, "seatstitle":lblVehicleSeatsTitle, "seats":lblVehicleSeats, "line":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[vehiclenotitle(20)][vehicleno(20)]-3-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[vehiclenotitle][seatstitle(==vehiclenotitle)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[vehicleno][seats(==vehicleno)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblVehicleSeatsTitle, attribute: .centerY, relatedBy: .equal, toItem: lblVehicleNoTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblVehicleSeats, attribute: .centerY, relatedBy: .equal, toItem: lblVehicleNo, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 75)
        
        if(data?.poolShared == "0") {
            lblVehicleSeatsTitle.isHidden = true
            lblVehicleSeats.isHidden = true
        }
        return sub
    }
    
    let lblComments = UILabel()
    func getCommentsView() -> SubView
    {
        let view = UIView()
        
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = normalFontWithSize(11)
        lbl.text = "COMMENTS"
        lbl.textColor = Colors.LABEL_DULL_TXT_COLOR
        view.addSubview(lbl)
        
        lblComments.translatesAutoresizingMaskIntoConstraints = false
        lblComments.font = boldFontWithSize(14)
        lblComments.numberOfLines = 0
        lblComments.textColor = Colors.GRAY_COLOR
        lblComments.text = data?.comments
        view.addSubview(lblComments)
        
        let lblLine = UILabel()
        lblLine.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(20)][comments][line(0.5)]|", options: [], metrics: nil, views: ["lbl":lbl, "comments":lblComments,"line": lblLine]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lbl, "comments":lblComments]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[comments]-5-|", options: [], metrics: nil, views: ["lbl":lbl, "comments":lblComments]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine]))
        
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    //Invite, Edit, Delete, Cancel, Join, Unjoin, OfferVehicle
    
    let btnInvite = UIButton()
    let btnEdit = UIButton()
    let btnDelete = UIButton()
    let btnCancel = UIButton()
    let btnJoin = UIButton()
    let btnUnjoin = UIButton()
    let btnAccept = UIButton()
    let btnReject = UIButton()
    let btnOfferVehicle = UIButton()
    let btnPayNow = UIButton()
    
    func setUpBtn(_ btn: UIButton, imageName: String, title: String, parent: UIView)
    {
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.setImage(UIImage(named: imageName), for: UIControlState())
        btn.isHidden = true
        parent.addSubview(btn)
        
        parent.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]|", options: [], metrics: nil, views: ["btn":btn]))
    }
    func setUpBtnWithLabel(_ btn: UIButton, title: String, parent: UIView)
    {
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.setTitle(title, for: UIControlState())
        btn.titleLabel?.font = normalFontWithSize(15)
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        
        btn.isHidden = true
        parent.addSubview(btn)
        
        parent.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]|", options: [], metrics: nil, views: ["btn":btn]))
    }
    func getBtnView() -> SubView
    {
        let view = UIView()
        
        setUpBtn(btnInvite, imageName: "invite", title:"Invite", parent: view)
        btnInvite.addTarget(self, action: #selector(btnInvite_clicked), for: .touchDown)
        
        setUpBtn(btnEdit, imageName: "edit", title:"Edit", parent: view)
        btnEdit.addTarget(self, action: #selector(btnEdit_clicked), for: .touchDown)
        
        setUpBtn(btnDelete, imageName: "delete", title:"Delete", parent: view)
        btnDelete.addTarget(self, action: #selector(btnDelete_clicked), for: .touchDown)
        
        setUpBtn(btnCancel, imageName: "cancel", title:"Cancel", parent: view)
        btnCancel.addTarget(self, action: #selector(btnCancel_clicked), for: .touchDown)
        
        setUpBtn(btnJoin, imageName: "join", title:"Join", parent: view)
        btnJoin.addTarget(self, action: #selector(btnJoin_clicked), for: .touchDown)
        
        setUpBtn(btnUnjoin, imageName: "unjoin", title:"Unjoin", parent: view)
        btnUnjoin.addTarget(self, action: #selector(btnUnjoin_clicked), for: .touchDown)
        
        setUpBtn(btnAccept, imageName: "accept", title:"Accept", parent: view)
        btnAccept.addTarget(self, action: #selector(btnAccept_clicked), for: .touchDown)
        
        setUpBtn(btnReject, imageName: "reject", title:"Reject", parent: view)
        btnReject.addTarget(self, action: #selector(btnReject_clicked), for: .touchDown)
        
        setUpBtn(btnOfferVehicle, imageName: "offeravehicle", title:"Offer a Vehicle", parent: view)
        btnOfferVehicle.addTarget(self, action: #selector(btnOfferVehicle_clicked), for: .touchDown)
        
        setUpBtnWithLabel(btnPayNow, title:"PAY NOW", parent: view)
        btnPayNow.addTarget(self, action: #selector(btnPayNow_clicked), for: .touchDown)
        
        setVisibleBtns(view)
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80)
        return sub
    }
    
    func btnInvite_clicked()
    {
        let vc = InviteViewController()
        vc.poolID = self.data!.poolID
        vc.ownerID = self.data!.createdBy
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.onCompletion = {
            self.updateRideDetails()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func btnEdit_clicked()
    {
        if(self.data?.rideType == "O"){
            let vc = OfferVehicleViewController()
            vc.isEditingRide = true
            vc.rideDetailData = data
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(self.data?.rideType == "N"){
            let vc = NeedRideViewController()
            vc.isEditingRide = true
            vc.rideDetailData = data
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func btnDelete_clicked()
    {
        AlertController.showAlertFor("Delete Ride", message: "Would you really like to delete this ride?", okButtonTitle: "Yes", okAction: {
            
            showIndicator("Deleting Ride..")
            
            RideRequestor().deleteRide(self.data!.poolID!, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Delete Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        _ = self.navigationController?.popViewController(animated: true)
                        self.onDeleteActionBlock?()
                    })
                }
                else{
                    AlertController.showAlertFor("Delete Ride", message: object as? String)
                }
                
                }, failure: { (error) in
                    hideIndicator()
                    AlertController.showAlertForError(error)
            })
        }, cancelButtonTitle: "Later") {
            
        }
    }
    func btnCancel_clicked()
    {
        AlertController.showAlertFor("Cancel Taxi Ride", message: "Would you really like to cancel this taxi ride?", okButtonTitle: "Yes", okAction: {
            showIndicator("Cancelling Taxi Ride..")
            RideRequestor().cancelTaxiRide(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        _ = self.navigationController?.popViewController(animated: true)
                        self.onDeleteActionBlock?()
                    })
                }
                else{
                    AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
            }, cancelButtonTitle: "Later", cancelAction: {
        })
    }
    func btnAccept_clicked()
    {
        AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this invited ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Accepting ride request..")
            let youpay = self.data?.youPay ?? ""
            let fullcost = self.data?.fullCost ?? 4
            RideRequestor().acceptTaxiRideRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, pCost: String(youpay), costMain: String(fullcost), success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Accept Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateRideDetails()
                    })
                }else{
                    AlertController.showAlertFor("Accept Taxi Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    func btnReject_clicked()
    {
        AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Rejecting ride request..")
            RideRequestor().rejectTaxiRideRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Reject Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateRideDetails()
                    })
                }else{
                    AlertController.showAlertFor("Reject Taxi Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    func btnJoin_clicked()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            if(UserInfo.sharedInstance.shareMobileNumber == false){
                AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                    self.getPickUpDropLocation()
                    }, cancelButtonTitle: "LATER", cancelAction: {
                })
            }
            else{
                getPickUpDropLocation()
            }
        }
    }
    
    
    func getPickUpDropLocation()
    {
        let vc = RideDetailMapViewController()
        vc.sourceLong = data?.sourceLong
        vc.sourceLat = data?.sourceLat
        vc.sourceLocation = data?.source
        vc.destLong = data?.destinationLong
        vc.destLat = data?.destinationLat
        vc.destLocation = data?.destination
        vc.taxiSourceID = taxiSourceID
        vc.taxiDestID = taxiDestID
        vc.poolTaxiBooking = (data?.poolTaxiBooking)!
        vc.poolId = (data?.poolID)!
        vc.rideType = (data?.rideType)!
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.isWP = false
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            showIndicator("Joining Taxi Ride..")
            let youpay = self.data?.youPay ?? ""
            let fullcost = self.data?.fullCost ?? 4
            RideRequestor().joinTaxiRide(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, pickupPoint: pickUpLoc as? String, dropPoint: dropLoc as? String, comments: "", pCost: String(youpay), costMain: String(fullcost), success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Join Taxi Ride", message: "You have successfully joined this ride.", okButtonTitle: "Ok", okAction: {
                        self.updateRideDetails()
                    })
                }
                else{
                    AlertController.showAlertFor("Join Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnUnjoin_clicked()
    {
        AlertController.showAlertFor("Unjoin Ride", message: "Would you really like to unjoin from this ride?", okButtonTitle: "YES", okAction: {
            showIndicator("Unjoining the ride..")
            RideRequestor().unJoinRide(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Unjoin Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateRideDetails()
                    })
                }else{
                    AlertController.showAlertFor("Unjoin Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "LATER") {
        }
    }
    
    func btnOfferVehicle_clicked()
    {
        let vc = OfferVehicleViewController()
        vc.rideDetailData = data
        vc.isExistingRide = true
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnPayNow_clicked()
    {
        showIndicator("Fetching Payment Values")
        RideRequestor().getTaxiDetails((data?.poolID)!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let vc = PaymentViewController()
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.rideDetailData = object as? RideDetail
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func setVisibleBtns(_ view: UIView)
    {
        if(btnConstraints != nil){
            view.removeConstraints(btnConstraints!)
            btnConstraints = nil
        }
        
        btnInvite.isHidden = true
        btnEdit.isHidden = true
        btnDelete.isHidden = true
        btnCancel.isHidden = true
        btnJoin.isHidden = true
        btnUnjoin.isHidden = true
        btnAccept.isHidden = true
        btnReject.isHidden = true
        btnOfferVehicle.isHidden = true
        btnPayNow.isHidden = true
        
        let viewsDict = ["invite":btnInvite, "edit":btnEdit, "delete":btnDelete, "cancel":btnCancel, "join":btnJoin, "unjoin":btnUnjoin, "offerv":btnOfferVehicle, "accept":btnAccept, "reject":btnReject,"paynow":btnPayNow]
        
        if(data?.createdBy == UserInfo.sharedInstance.userID){
            if(data?.poolTaxiBooking == "0"){
                btnInvite.isHidden = false
                btnEdit.isHidden = false
                btnDelete.isHidden = false
                
                btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[edit(==invite)]-1-[delete(==invite)]|", options: [], metrics: nil, views: viewsDict)
            }
            else{
                btnCancel.isHidden = false
                if(data?.poolShared != "0") {
                    btnInvite.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[cancel(==invite)]|", options: [], metrics: nil, views: viewsDict)
                } else {
                    if(data?.payTo == "2") {
                        btnPayNow.isHidden = false
                        if(data?.costUpdated == "1") {
                            btnCancel.isHidden = true
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[paynow]|", options: [], metrics: nil, views: viewsDict)
                        } else {
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]-1-[paynow(==cancel)]|", options: [], metrics: nil, views: viewsDict)
                        }
                    } else {
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]|", options: [], metrics: nil, views: viewsDict)
                    }
                }
            }
        }
        else{
            if(data?.joinStatus != nil && data?.requestedStatus != nil){
                if(data?.joinStatus == "P" && data?.requestedStatus == "P"){
                    btnCancel.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]|", options: [], metrics: nil, views: viewsDict)
                }
                else if(data?.joinStatus == "J" && data?.requestedStatus == "A"){
                    btnInvite.isHidden = false
                    if(data?.poolTaxiBooking == "0"){
                        btnUnjoin.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[unjoin(==invite)]|", options: [], metrics: nil, views: viewsDict)
                    }
                    else{
                        btnCancel.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[cancel(==invite)]|", options: [], metrics: nil, views: viewsDict)
                    }
                }
                else if((data?.joinStatus == "F" || data?.joinStatus == "S") && data?.requestedStatus == "F"){
                    btnAccept.isHidden = false
                    btnReject.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[accept]-1-[reject(==accept)]|", options: [], metrics: nil, views: viewsDict)
                }
                else if((data?.joinStatus == "U" && data?.requestedStatus == "A") || (data?.joinStatus == "P" && data?.requestedStatus == "C") || (data?.joinStatus == "S" && data?.requestedStatus == "C") || (data?.joinStatus == "R" && data?.requestedStatus == "R") || (data?.joinStatus == "O" && data?.requestedStatus == "A") || (data?.joinStatus == "F" && data?.requestedStatus == "C")){
                    if(data?.rideType == "O"){
                        btnJoin.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                        
                    }
                    else{
                        if(data?.poolTaxiBooking == "0"){
                            btnJoin.isHidden = false
                            btnOfferVehicle.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]-1-[offerv(==join)]|", options: [], metrics: nil, views: viewsDict)
                            
                        }
                        else{
                            btnJoin.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                        }
                    }
                }
            }
            else{
                if(data?.rideType == "O"){
                    btnJoin.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                }
                else{
                    if(data?.poolTaxiBooking == "0"){
                        btnJoin.isHidden = false
                        btnOfferVehicle.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]-1-[offerv(==join)]|", options: [], metrics: nil, views: viewsDict)
                    }
                    else{
                        btnJoin.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                    }
                }
            }
        }
        view.addConstraints(btnConstraints!)
    }
}
