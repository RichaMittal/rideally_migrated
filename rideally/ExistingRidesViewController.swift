//
//  ExistingRidesViewController.swift
//  rideally
//
//  Created by Sarav on 05/11/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class ExistingRidesViewController: WPRidesViewController {

    var dataSource: [WPRide]?{
        didSet{
            
        }
    }
    var lastRideObj: LastRideData?

    override func hasTabBarItems() -> Bool
    {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Existing Rides"
        // Do any additional setup after loading the view.
    }
    
    let lblMsg = UILabel()
    override func addViews() {
        lblMsg.textColor = Colors.GRAY_COLOR
        lblMsg.font = normalFontWithSize(15)
        let nowText = "Add Ride"
        let combinedString = "You may join below mentioned existing rides or \(nowText)" as NSString
        let range = combinedString.range(of: nowText)
        let attributedString = NSMutableAttributedString(string: combinedString as String)
        
        let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1] as [String : Any]
        attributedString.addAttributes(attributes, range: range)
        
        lblMsg.attributedText = attributedString
        lblMsg.numberOfLines = 0
        lblMsg.translatesAutoresizingMaskIntoConstraints = false
        lblMsg.isUserInteractionEnabled = true
        lblMsg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createRide)))
        view.addSubview(lblMsg)
        
        tableView.separatorColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(WPRideCell.self, forCellReuseIdentifier: "WPRideCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        let viewsDict = ["lbl":lblMsg, "table":tableView]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl]-10-[table]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[table]-5-|", options: [], metrics: nil, views: viewsDict))
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource!.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WPRideCell") as! WPRideCell
        cell.data = dataSource![indexPath.row]
        cell.onUserActionBlock = { (selectedAction) -> Void in
            self.cell_btnAction(selectedAction as! String, indexPath: indexPath)
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showIndicator("Fetching Ride Details")
        let poolId: String = dataSource![indexPath.row].poolID!
        
        RideRequestor().getRideDetails(poolId, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                //let vc = RideDetailViewController()
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.isFirstLevel = true
                vc.data = object as? RideDetail
                if let poolScope = self.dataSource![indexPath.row].poolScope {
                    if poolScope == "PUBLIC" {
                        vc.triggerFrom = "ride"
                    } else {
                        vc.triggerFrom = "place"
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    override func getSelectedRide(_ indexPath: IndexPath) -> Ride
    {
        return dataSource![indexPath.row]
    }
}
