//
//  DropLocationMapViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 1/7/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GoogleMaps

class DropLocationMapViewController : UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, GMSMapViewDelegate {
    
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var mapView:GMSMapView!
    
    let sourceView = UIView()
    let destView = UIView()
    
    let sourceTextField = UITextField()
    let destTextField = UITextField()
    
    var mapSourcePinImageVIew = UIImageView()
    var mapDestPinImageVIew = UIImageView()
    
    var currentLocationView = UIView()
    var userMovedMap: Bool = false
    
    var locationButton: UIButton = UIButton(type: .System)
    
    let appDelegate: AppDelegate = sharedAppDelegate()!
    var locationButtonAction: VoidCompletionHandler?
    
    var shouldShowCancelButton = false
    
    var cancelButton = UIButton(type: .System)
    var currentLocationButton = UIButton(type: .Custom)
    var locationName: String = ""
    
    var onSelectionBlock: actionBlockWithParam?
    var onConfirmLocationsBlock: actionBlockWithParams?
    
    var jumpOneScreen: Bool = false
    
    var sourceLat: String?
    var sourceLong: String?
    var sourceLocation: String?
    var destLat: String?
    var destLong: String?
    var destLocation: String?
    
    var sourceConfirmed = false
    var destConfirmed = false
    
    var selectedTextField: UITextField?
    
    func goBack()
    {
        if(jumpOneScreen){
            let vcs = self.navigationController?.viewControllers
            let jumpToIndex = vcs!.count - 3
            
            if let vc = vcs![jumpToIndex] as? UIViewController{
                self.navigationController?.popToViewController(vc, animated: true)
            }
        }
        else{
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.Plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        
        // Do any additional setup after loading the view, typically from a nib.
        mapSourcePinImageVIew.image = UIImage(imageLiteral: "btn_set_location")
        
        title = "Choose Location"
        self.sourceTextField.delegate = self
        self.destTextField.delegate = self
        //setupTextfield()
        dropDownTextfield()
        self.setupGoogleMaps()
        sourceTextField.font = boldFontWithSize(15.0)
        destTextField.font = boldFontWithSize(15.0)
        destTextField.becomeFirstResponder()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.locationCoord == nil {
            self.getCurrentLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGoogleMaps () {
        
        let lat = Double(destLat!)!
        let lng = Double(destLong!)!
        let camera = GMSCameraPosition.cameraWithLatitude(lat, longitude: lng, zoom: 16)
        self.mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        
        self.mapView.delegate = self
        self.mapView.myLocationEnabled = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        self.view.sendSubviewToBack(self.mapView)
        let dict = ["mapView": mapView, "dest": destView]
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[dest(50)][mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[dest]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        
        // pin layout
        mapSourcePinImageVIew.translatesAutoresizingMaskIntoConstraints = false
        self.mapView.addSubview(self.mapSourcePinImageVIew)
        
        self.mapView.addConstraint(NSLayoutConstraint(item: self.mapSourcePinImageVIew, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        self.mapView.addConstraint(NSLayoutConstraint(item: self.mapSourcePinImageVIew, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: -(mapSourcePinImageVIew.image?.size.height)!/2.0 + 10))
        
        // button layout
        setupLocationButton()
        self.mapView.addSubview(self.locationButton)
        
        
        self.mapView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[btn]|", options: [], metrics: nil, views: ["btn":self.locationButton]))
        self.mapView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[btn(40)]|", options: [], metrics: nil, views: ["btn":self.locationButton]))
    }
    
    func setupLocationButton () {
        self.locationButton.setTitle("Save Drop Location", forState: UIControlState.Normal)
        self.locationButton.backgroundColor = Colors.GRAY_COLOR
        self.locationButton.setTitleColor(Colors.WHITE_COLOR, forState: .Normal)
        self.locationButton.titleLabel?.font = boldFontWithSize(16.0)
        self.locationButton.translatesAutoresizingMaskIntoConstraints = false
        self.locationButton.addTarget(self, action: #selector(selectLocationButtonTapped(_:)), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    
    func dropDownTextfield () {
        destView.translatesAutoresizingMaskIntoConstraints = false
        destView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(destView)
        
        let lblDest = UILabel()
        lblDest.text = "SELECT DROP LOCATION"
        lblDest.font = normalFontWithSize(12)
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        destView.addSubview(lblDest)
        
        let image = UIImage(named: "dest")
        let frame = CGRectMake(0, 0, (image?.size.width)!, (image?.size.height)!)
        let leftV1 = UIImageView(frame: frame)
        leftV1.image = image
        leftV1.contentMode = UIViewContentMode.Center
        
        destTextField.placeholder = "Enter your location"
        self.destTextField.leftView = leftV1
        self.destTextField.leftViewMode = UITextFieldViewMode.Always
        self.destTextField.clearButtonMode = UITextFieldViewMode.WhileEditing
        destTextField.translatesAutoresizingMaskIntoConstraints = false
        destTextField.backgroundColor = Colors.WHITE_COLOR
        
        destView.addSubview(destTextField)
        destView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[lbl(15)][txt(35)]|", options: [], metrics: nil, views: ["lbl":lblDest, "txt":destTextField]))
        destView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[lbl]|", options: [], metrics: nil, views: ["lbl":lblDest, "txt":destTextField]))
        destView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[txt]|", options: [], metrics: nil, views: ["lbl":lblDest, "txt":destTextField]))
        destTextField.text = destLocation
        
    }
    
    
    func getCurrentLocation () {
        // Ask for Authorisation from the User.
        //self.locationManager.requestAlwaysAuthorization()
        
//        if CLLocationManager.locationServicesEnabled() == true {
//            if CLLocationManager.authorizationStatus() == .Denied {
//                AlertController.showAlertForMessage("You have denied access to your location, please enable it in settings.")
//                return
//            }
//        }
        
        callLocationServiceEnablePopUp()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else {
            print("location service not enabled")
        }
    }
    
    
    func callLocationServiceEnablePopUp() -> Void
    {
        switch CLLocationManager.authorizationStatus()
        {
        case .AuthorizedWhenInUse, .Restricted, .Denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order to be notified, please open this app's settings and set location access to 'Always'.",
                preferredStyle: .Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .Default) { (action) in
                if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.presentViewController(alertController, animated: true, completion: nil)
            
            
        default:
            break
            
            
        }
    }

    
    // MARK:- CLLocationManager Delegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationCoord = manager.location!.coordinate
        self.moveMapToCoord(self.locationCoord)
        reverseGeocodeCoordinate(self.locationCoord)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("error::\(error)")
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            self.mapView.myLocationEnabled = true
        } else if status == .Denied {
            //AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
            callLocationServiceEnablePopUp()
        }
    }
    
    //MARK:- Text Field Delegates
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        selectedTextField = textField
        if textField == sourceTextField {
            sourceConfirmed = false
            let lat = Double(sourceLat!)!
            let lng = Double(sourceLong!)!
            self.moveMapToCoord(CLLocationCoordinate2DMake(lat, lng))
        }
        else if(textField == destTextField){
            destConfirmed = false
            let lat = Double(destLat!)!
            let lng = Double(destLong!)!
            self.moveMapToCoord(CLLocationCoordinate2DMake(lat, lng))
        }
        return false
    }
    
    func selectLocationButtonTapped (sender: AnyObject!) {
        if((sender as! UIButton).titleLabel?.text == "Save Drop Location"){
            let vc = RideDetailMapViewController()
            vc.sourceLong = self.sourceLong
            vc.sourceLat = self.sourceLat
            vc.sourceLocation = self.sourceLocation
            vc.destLong = self.destLong
            vc.destLat = self.destLat
            vc.destLocation = self.destLocation
            vc.onConfirmLocationsBlock = self.onConfirmLocationsBlock
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            goBack()
            onConfirmLocationsBlock?(sourceLocation, destLocation)
        }
        
    }
    
    func proceedWithSelectedLatLong (coord : CLLocationCoordinate2D) {
        goBack()
        onSelectionBlock?(["lat":"\(coord.latitude)", "long":"\(coord.longitude)", "location":destTextField.text!])
    }
    
    func getDataForCoord (coord : CLLocationCoordinate2D) {
    }
    
    func goToCurrentLocation (sender: UIButton) {
        if let location = mapView.myLocation {
            self.userMovedMap = true
            moveMapToCoord(location.coordinate)
        }
        
    }
    
    // MARK:- Google Map Delegates
    func mapView(mapView: GMSMapView, didTapAtCoordinate coordinate: CLLocationCoordinate2D) {
        /*self.locationCoord = coordinate
         self.moveMapToCoord(self.locationCoord)
         reverseGeocodeCoordinate(self.locationCoord)*/
    }
    
    func mapView(mapView: GMSMapView, willMove gesture: Bool) {
        self.userMovedMap = self.userMovedMap ? self.userMovedMap : gesture
    }
    
    func mapView(mapView: GMSMapView, idleAtCameraPosition position: GMSCameraPosition) {
        if self.userMovedMap == true {
            self.userMovedMap = false
            self.locationCoord = position.target
            self.moveMapToCoord(self.locationCoord)
            reverseGeocodeCoordinate(self.locationCoord)
        }
    }
    
    func moveMapToCoord(coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        self.locationCoord = coordinate
        let camera = self.mapView.camera
        let cameraPosition = GMSCameraPosition.cameraWithTarget(coord, zoom: camera.zoom)
        self.mapView.animateToCameraPosition(cameraPosition)
    }
    
    // MARK:- Reverse Geocode
    func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        
        let geocoder = GMSGeocoder()
        selectedTextField?.lock()
        
        geocoder.reverseGeocodeCoordinate(coord) { [weak self] response , error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]!
                self?.selectedTextField?.text = lines.joinWithSeparator("\n")
                self?.selectedTextField?.unlock()
                
                if(self?.selectedTextField == self?.sourceTextField){
                    self?.sourceLat = "\((self?.locationCoord?.latitude)!)"
                    self?.sourceLong = "\((self?.locationCoord?.longitude)!)"
                    self?.sourceLocation = self?.selectedTextField?.text
                }
                else if(self?.selectedTextField == self?.destTextField){
                    self?.destLat = "\((self?.locationCoord?.latitude)!)"
                    self?.destLong = "\((self?.locationCoord?.longitude)!)"
                    self?.destLocation = self?.selectedTextField?.text
                }
                
            }
        }
    }
}
