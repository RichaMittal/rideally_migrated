//
//  WPMembersViewController.swift
//  rideally
//
//  Created by Sarav on 29/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class WPMemberCell: UITableViewCell {
    var data: WPMember!{
        didSet{
            buildCell()
        }
    }
    var wpDetails: WorkPlace?
    var onUserActionBlock: actionBlockWithParams?
    
    let profilePic = UIImageView()
    let lblName = UILabel()
    let lblTrusted = UILabel()
    let lblRating = UILabel()
    let imgRating = UIImageView()
    let btnAccept = UIButton()
    let btnReject = UIButton()
    
    let verificationView = UIView()
    //    let emailIcon = UIImageView()
    //    let mobileIcon = UIImageView()
    let adminIcon = UIImageView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        profilePic.layer.cornerRadius = 20
        profilePic.layer.masksToBounds = true
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        profilePic.backgroundColor = UIColor.gray
        profilePic.isUserInteractionEnabled = true
        self.contentView.addSubview(profilePic)
        
        verificationView.backgroundColor = Colors.BLACK_COLOR
        verificationView.alpha = 0.8
        verificationView.translatesAutoresizingMaskIntoConstraints = false
        //        emailIcon.image = UIImage(named: "email")
        //        emailIcon.translatesAutoresizingMaskIntoConstraints = false
        //        verificationView.addSubview(emailIcon)
        //        mobileIcon.image = UIImage(named: "phone")
        //        mobileIcon.translatesAutoresizingMaskIntoConstraints = false
        //        verificationView.addSubview(mobileIcon)
        adminIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(adminIcon)
        
        //        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[email(15)]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        //        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[mobile(15)]", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        //        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[mobile]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        //        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[email]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[admin]|", options: [], metrics: nil, views: ["admin":adminIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[admin(15)]", options: [], metrics: nil, views: ["admin":adminIcon]))
        verificationView.addConstraint(NSLayoutConstraint(item: adminIcon, attribute: .centerX, relatedBy: .equal, toItem: verificationView, attribute: .centerX, multiplier: 1, constant: 0))
        
        profilePic.addSubview(verificationView)
        
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":verificationView]))
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(15)]|", options: [], metrics: nil, views: ["view":verificationView]))
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = boldFontWithSize(15)
        lblName.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblName)
        
        lblTrusted.translatesAutoresizingMaskIntoConstraints = false
        lblTrusted.font = boldFontWithSize(10)
        lblTrusted.textColor = Colors.ORANGE_TEXT_COLOR
        lblTrusted.isHidden = true
        //lblTrusted.backgroundColor = Colors.ORANGE_BG_COLOR
        self.contentView.addSubview(lblTrusted)
        
        lblRating.translatesAutoresizingMaskIntoConstraints = false
        lblRating.font = boldFontWithSize(12)
        lblRating.textColor = Colors.GRAY_COLOR
        lblRating.isHidden = true
        self.contentView.addSubview(lblRating)
        
        imgRating.image = UIImage(named: "Star_Filled")
        imgRating.translatesAutoresizingMaskIntoConstraints = false
        imgRating.isHidden = true
        self.contentView.addSubview(imgRating)
        
        btnAccept.translatesAutoresizingMaskIntoConstraints = false
        btnAccept.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnAccept.setTitle("Accept", for: UIControlState())
        btnAccept.titleLabel?.font = boldFontWithSize(13)
        self.contentView.addSubview(btnAccept)
        
        btnReject.translatesAutoresizingMaskIntoConstraints = false
        btnReject.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnReject.setTitle("Reject", for: UIControlState())
        btnReject.titleLabel?.font = boldFontWithSize(13)
        self.contentView.addSubview(btnReject)
        
        btnAccept.addTarget(self, action: #selector(btn_clicked(_:)), for: .touchDown)
        btnReject.addTarget(self, action: #selector(btn_clicked(_:)), for: .touchDown)
        
        let viewsDict = ["pic":profilePic, "name":lblName, "trusted":lblTrusted, "rating":lblRating, "irating":imgRating, "accept":btnAccept, "reject":btnReject]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[pic(40)]", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[name]-5-[trusted]-5-[rating]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-45-[accept(25)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-45-[reject(25)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[accept]-20-[reject]-15-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[pic(40)]-2-[name]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[rating]-2-[irating]", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblTrusted, attribute: .left, relatedBy: .equal, toItem: lblName, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblRating, attribute: .left, relatedBy: .equal, toItem: lblName, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: imgRating, attribute: .centerY, relatedBy: .equal, toItem: lblRating, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    func buildCell()
    {
        adminIcon.isHidden = true
        verificationView.isHidden = true
        if(data.membershipType == "1"){
            verificationView.isHidden = false
            adminIcon.isHidden = false
            adminIcon.image = UIImage(named: "Admin")
        }
        
        var name = ""
        if let fname = data.fname{
            if(fname != "") {
                name = fname
                name.replaceSubrange(name.startIndex...name.startIndex, with: String(name[name.startIndex]).capitalized)
            }
        }
        if let lname = data.lname{
            if(lname != "") {
                var lastName = lname
                lastName.replaceSubrange(lastName.startIndex...lastName.startIndex, with: String(lastName[lastName.startIndex]).capitalized)
                name = "\(name) \(lastName)"
            }
        }
        lblName.text = name
        
        if((data.userState == "4" && data.secEmailStatus == "2") || (data.userState == "5")) {
            lblTrusted.isHidden = false
            lblTrusted.text = "TRUSTED"
        } else {
            lblTrusted.isHidden = true
        }
        
        if(data.avgRating != "" && data.avgRating != nil) {
            lblRating.isHidden = false
            imgRating.isHidden = false
            var splitvalue = data.avgRating?.components(separatedBy: ".")
            if(splitvalue![1] == "00") {
                lblRating.text = splitvalue![0]
            } else {
                let numberOfPlaces = 1.0
                let multiplier = pow(10.0, numberOfPlaces)
                lblRating.text = String(round(Double(data.avgRating!)! * multiplier) / multiplier)
            }
        } else {
            lblRating.isHidden = true
            imgRating.isHidden = true
        }
        
        //        if(data.userState == "5"){
        //            emailIcon.image = UIImage(named: "email_v")
        //            mobileIcon.image = UIImage(named: "phone_v")
        //        }
        //        else if(data.userState == "4"){
        //            mobileIcon.image = UIImage(named: "phone_v")
        //        }
        //        else if(data.userState == "3"){
        //            emailIcon.image = UIImage(named: "email_v")
        //        }
        
        if(data?.profilePicStatus == "1"){
            if let url = data?.profilePicUrl{
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
            }
            else{
                if let fbID = data.fbID{
                    profilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            profilePic.image = UIImage(named: "placeholder")
        }
        
        btnAccept.isHidden = true
        btnReject.isHidden = true
        if(wpDetails?.wpDetailMembershipType == "1") {
            if(data.membershipStatus == "0"){
                btnAccept.isHidden = false
                btnReject.isHidden = false
                
                btnAccept.setTitle("ACCEPT", for: UIControlState())
                btnReject.setTitle("REJECT", for: UIControlState())
            } else if(data.membershipStatus == "1"){
                if(data.membershipType != "1"){
                    btnAccept.isHidden = false
                    btnReject.isHidden = false
                    
                    btnAccept.setTitle("MAKE ADMIN", for: UIControlState())
                    btnReject.setTitle("REMOVE", for: UIControlState())
                }
            }
        } else {
            btnAccept.isHidden = true
            btnReject.isHidden = true
        }
        setNeedsLayout()
    }
    
    func btn_clicked(_ btn: UIButton)
    {
        let str = btn.titleLabel?.text!
        if(str == "ACCEPT"){
            onUserActionBlock?("accept" as AnyObject?, data.uID as AnyObject?)
        } else if(str == "REJECT"){
            onUserActionBlock?("reject" as AnyObject?, data.uID as AnyObject?)
        } else if(str == "MAKE ADMIN"){
            onUserActionBlock?("makeadmin" as AnyObject?, data.uID as AnyObject?)
        } else if(str == "REMOVE"){
            onUserActionBlock?("remove" as AnyObject?, data.uID as AnyObject?)
        }
    }
}

class WPMembersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let table = UITableView()
    var membersList: [WPMember]?
    var joinedMembersList: NSDictionary?
    var wpDetails: WorkPlace?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.APP_BG
        title = "\(wpDetails?.wpName ?? "") \("Members")"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        var index = 0
        for member in membersList!{
            if(member.membershipStatus != "1" && member.membershipStatus != "0"){
                membersList?.remove(at: index)
            }
            else
            {
                index += 1
            }
        }
        table.translatesAutoresizingMaskIntoConstraints = false
        table.delegate = self
        table.dataSource = self
        table.register(WPMemberCell.self, forCellReuseIdentifier: "wpmembercell")
        //table.separatorColor = UIColor.clearColor()
        table.separatorColor = Colors.GRAY_COLOR
        table.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        table.tableFooterView = UIView()
        self.view.addSubview(table)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":table]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[table]-5-|", options: [], metrics: nil, views: ["table":table]))
    }
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return membersList!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "wpmembercell") as! WPMemberCell
        cell.wpDetails = wpDetails
        cell.data = membersList![indexPath.row]
        cell.onUserActionBlock = { (action, userID) -> Void in
            self.performAction(action as! String, indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showIndicator("Getting member's detail..")
        RideRequestor().getMembersDetails((membersList![indexPath.row]).uID!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                let vc = MemberDetailViewController()
                vc.dataSource = (object as? MemberDetailResponse)?.data?.userInfo
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func performAction(_ action: String, indexPath: IndexPath){
        let selectedData = membersList![indexPath.row]
        if(action == "accept"){
            showIndicator("Accepting request.")
            WorkplaceRequestor().respondToJoinWP(selectedData.membershipId!, userID: UserInfo.sharedInstance.userID, adminMsg: "", status: "1", success: { (success, object) in
                hideIndicator()
                if(success){
                    AlertController.showAlertFor("Accept Request", message: "Request to join workplace accepted successfully.", okButtonTitle: "Ok", okAction: {
                        self.updateMembers()
                    })
                }
                else{
                    
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }else if(action == "reject"){
            showIndicator("Rejecting request.")
            WorkplaceRequestor().respondToJoinWP(selectedData.membershipId!, userID: UserInfo.sharedInstance.userID, adminMsg: "", status: "2", success: { (success, object) in
                hideIndicator()
                if(success){
                    AlertController.showAlertFor("Reject Request", message: "Request to join workplace rejected successfully.", okButtonTitle: "Ok", okAction: {
                        self.updateMembers()
                    })
                }
                else{
                    
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }else if(action == "makeadmin"){
            showIndicator("Adding admin.")
            WorkplaceRequestor().addAdminToWP((wpDetails?.wpID)!, userIDs: selectedData.uID!, success: { (success, object) in
                hideIndicator()
                if(success){
                    AlertController.showAlertFor("Add Admin", message: "Admin added to workplace successfully.", okButtonTitle: "Ok", okAction: {
                        self.updateMembers()
                    })
                }
                else{
                    
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }else if(action == "remove"){
            showIndicator("Removing user.")
            WorkplaceRequestor().removeFromWP((wpDetails?.wpID)!, userID: selectedData.uID!, adminMsg: "", success: { (success, object) in
                hideIndicator()
                if(success){
                    AlertController.showAlertFor("Remove User", message: "User removed from workplace successfully.", okButtonTitle: "Ok", okAction: {
                        self.updateMembers()
                    })
                }
                else{
                    
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }
    }
    
    func updateMembers()
    {
        showIndicator("Getting members list..")
        WorkplaceRequestor().getWPMembers("", groupID: (wpDetails?.wpID)!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            if(success){
                self.membersList = (object as! WPMembersResponseData).members
                var index = 0
                for member in self.membersList!{
                    if(member.membershipStatus != "1" && member.membershipStatus != "0"){
                        self.membersList?.remove(at: index)
                    }
                    else
                    {
                        index += 1
                    }
                }
                self.wpDetails = (object as! WPMembersResponseData).groupDetails
                self.table.reloadData()
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
}
