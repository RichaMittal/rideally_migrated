//
//  MemberDetailViewController.swift
//  rideally
//
//  Created by Sarav on 01/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class MemberDetailViewController: UIViewController {
    
    var dataSource: MemberDetail?
    
    let imgProfilePic = UIImageView()
    let lblName = UILabel()
    let lblGender = UILabel()
    
    let lblLine1 = UILabel()
    let lblEmail = UILabel()
    let lblLine2 = UILabel()
    let lblMobile = UILabel()
    let lblLine3 = UILabel()
    
    let emailIcon = UIImageView()
    let mobileIcon = UIImageView()
    let verificationView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.APP_BG
        
        title = "Member Detail"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        
        imgProfilePic.translatesAutoresizingMaskIntoConstraints = false
        imgProfilePic.backgroundColor = UIColor.clear
        view.addSubview(imgProfilePic)
        
        verificationView.backgroundColor = Colors.BLACK_COLOR
        verificationView.alpha = 0.8
        verificationView.translatesAutoresizingMaskIntoConstraints = false
        emailIcon.image = UIImage(named: "email")
        emailIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(emailIcon)
        mobileIcon.image = UIImage(named: "phone")
        mobileIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(mobileIcon)
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[email(15)]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mobile(15)]", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mobile]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[email]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        
        imgProfilePic.addSubview(verificationView)
        
        imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":verificationView]))
        imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(15)]|", options: [], metrics: nil, views: ["view":verificationView]))
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.textColor = Colors.GREEN_COLOR
        lblName.textAlignment = .center
        lblName.font = normalFontWithSize(16)
        view.addSubview(lblName)
        
        lblGender.translatesAutoresizingMaskIntoConstraints = false
        lblGender.textColor = Colors.GRAY_COLOR
        lblGender.textAlignment = .center
        lblGender.font = normalFontWithSize(14)
        view.addSubview(lblGender)
        
        
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        view.addSubview(lblLine1)
        
        let lblEmailTitle = UILabel()
        lblEmailTitle.translatesAutoresizingMaskIntoConstraints = false
        lblEmailTitle.text = "EMAIL"
        lblEmailTitle.textColor = Colors.GENERAL_BORDER_COLOR
        lblEmailTitle.font = normalFontWithSize(14)
        view.addSubview(lblEmailTitle)
        
        lblEmail.translatesAutoresizingMaskIntoConstraints = false
        lblEmail.textColor = Colors.GRAY_COLOR
        lblEmail.font = normalFontWithSize(15)
        view.addSubview(lblEmail)
        
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        view.addSubview(lblLine2)
        
        let lblMobileTitle = UILabel()
        lblMobileTitle.translatesAutoresizingMaskIntoConstraints = false
        lblMobileTitle.text = "MOBILE"
        lblMobileTitle.textColor = Colors.GENERAL_BORDER_COLOR
        lblMobileTitle.font = normalFontWithSize(14)
        view.addSubview(lblMobileTitle)
        
        lblMobile.translatesAutoresizingMaskIntoConstraints = false
        lblMobile.textColor = Colors.GRAY_COLOR
        lblMobile.font = normalFontWithSize(15)
        view.addSubview(lblMobile)
        
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        view.addSubview(lblLine3)
        
        let viewsDict = ["pic":imgProfilePic, "name":lblName, "gender":lblGender, "line1":lblLine1, "emailtitle":lblEmailTitle, "email":lblEmail, "line2":lblLine2, "mobiletitle":lblMobileTitle, "mobile":lblMobile, "line3":lblLine3]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(80)]-10-[name(20)][gender(20)]-5-[line1(0.5)]-5-[emailtitle(15)]-5-[email(20)]-5-[line2(0.5)]-5-[mobiletitle(15)]-5-[mobile(20)]-5-[line3(0.5)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[name]-10-|", options: [], metrics: nil, views: ["name":lblName]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[gender]-10-|", options: [], metrics: nil, views: ["gender":lblGender]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[pic(60)]", options: [], metrics: nil, views: ["pic":imgProfilePic]))
        view.addConstraint(NSLayoutConstraint(item: imgProfilePic, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[emailtitle]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[email]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[mobiletitle]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[mobile]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        
        getDetails()
    }
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }

    func getDetails()
    {
        self.lblName.text = dataSource?.name
        if(dataSource?.profilePicStatus == "1"){
            if let url = dataSource?.profilePic{
                self.imgProfilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
            }
            else{
                if let fbID = dataSource?.fbID{
                    self.imgProfilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            self.imgProfilePic.image = UIImage(named: "placeholder")
        }
        
        if(dataSource?.gender == "F"){
            self.lblGender.text = "Female"
        }
        else{
            self.lblGender.text = "Male"
        }
        
        if(dataSource?.userState == "5"){
            self.emailIcon.image = UIImage(named: "email_v")
            self.mobileIcon.image = UIImage(named: "phone_v")
        }
        else if(dataSource?.userState == "4"){
            self.mobileIcon.image = UIImage(named: "phone_v")
        }
        else if(dataSource?.userState == "3"){
            self.emailIcon.image = UIImage(named: "email_v")
        }
        
        self.lblEmail.text = dataSource?.email
        self.lblMobile.text = dataSource?.mobile
    }
}
