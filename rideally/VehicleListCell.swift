//
//  VehicleListCell.swift
//  rideally
//
//  Created by Raghunathan on 9/2/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

protocol VehicleListCellDelegate: class
{
    func deleteButtonPressEvent(_ sender: UIButton)
    func updateButtonPresseEven()
    func isDefaultButtonPressedEventCalled()
}

class VehicleListCell: UITableViewCell
{
    let vehicleTypeImageView =  UIImageView()
    let vehicleTypeLabel = UILabel()
    let vehicleRegNoLabel = UILabel()
    let vehicleSeatInfoLabel = UILabel()
    let isDefaultVehicleLabel = UILabel()
    var updateLabel = UIButton()
    var deleteLabel = UIButton()
    let isDefaultVehicle = UIButton(type: .custom)
    //let seperatorLine = UIView()
    weak var cellDelegate:VehicleListCellDelegate?
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.backgroundColor = UIColor.clear
        buttonObject.contentHorizontalAlignment = .center
        buttonObject.titleLabel?.font = normalFontWithSize(15)
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    var data: VehicleData!{
        didSet
        {
            updateCell()
        }
    }

    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        vehicleTypeLabel.font = normalFontWithSize(13)
        vehicleRegNoLabel.font = normalFontWithSize(13)
        vehicleSeatInfoLabel.font = normalFontWithSize(13)
        
        
        isDefaultVehicleLabel.font = normalFontWithSize(11)
        isDefaultVehicleLabel.textColor = Colors.GRAY_COLOR
       // seperatorLine.backgroundColor = Colors.GRAY_COLOR
        
        self.updateLabel  =  getButtonSubViews("UPDATE")
        self.updateLabel.backgroundColor = UIColor.clear
        
        self.updateLabel.contentHorizontalAlignment = .right
        self.updateLabel.titleLabel?.font = normalFontWithSize(15)
        self.updateLabel.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())

        self.deleteLabel  =  getButtonSubViews("DELETE")
        self.deleteLabel.backgroundColor = UIColor.clear
        self.deleteLabel.contentHorizontalAlignment = .right
        self.deleteLabel.titleLabel?.font = normalFontWithSize(15)
        self.deleteLabel.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())

        
        isDefaultVehicle.addTarget(self, action: #selector(isDefaultButtonPressed(_:)), for: .touchUpInside)
        isDefaultVehicle.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())

        
        isDefaultVehicleLabel.text = "DEFAULT VEHICLE"

        vehicleTypeLabel.translatesAutoresizingMaskIntoConstraints = false
        vehicleRegNoLabel.translatesAutoresizingMaskIntoConstraints = false
        vehicleSeatInfoLabel.translatesAutoresizingMaskIntoConstraints = false
        isDefaultVehicleLabel.translatesAutoresizingMaskIntoConstraints = false
        updateLabel.translatesAutoresizingMaskIntoConstraints = false
        deleteLabel.translatesAutoresizingMaskIntoConstraints = false
        //seperatorLine.translatesAutoresizingMaskIntoConstraints = false
        vehicleTypeImageView.translatesAutoresizingMaskIntoConstraints = false
        isDefaultVehicle.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(vehicleTypeImageView)
        self.contentView.addSubview(vehicleTypeLabel)
        self.contentView.addSubview(vehicleRegNoLabel)
        self.contentView.addSubview(vehicleSeatInfoLabel)
        self.contentView.addSubview(isDefaultVehicle)
        self.contentView.addSubview(isDefaultVehicleLabel)
        self.contentView.addSubview(updateLabel)
        self.contentView.addSubview(deleteLabel)
       // self.contentView.addSubview(seperatorLine)
        

        //self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[sepLine]|", options: [], metrics: nil, views: ["sepLine":seperatorLine]))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-30-[vehicleTypeImg]-30-|", options: [], metrics: nil, views: ["vehicleTypeImg":vehicleTypeImageView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[vehicleTypeImg(40)]", options: [], metrics: nil, views: ["vehicleTypeImg":vehicleTypeImageView]))

        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[vehicleTypelbl(20)][vehicleRegNolbl(20)][vehicleSeatlbl(20)]-5-[vehicleIsDefault(15)]", options: [], metrics: nil, views: ["vehicleTypelbl":vehicleTypeLabel,"vehicleRegNolbl":vehicleRegNoLabel,"vehicleSeatlbl":vehicleSeatInfoLabel,"vehicleIsDefault":isDefaultVehicle]))//,"sepLine":seperatorLine   ///-9-[sepLine(0.5)]

        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[vehicleIsDefaultlbl(10)]-10-|", options: [], metrics: nil, views: ["vehicleIsDefaultlbl":isDefaultVehicleLabel]))

        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[vehicleTypelbl(200)]", options: [], metrics: nil, views: ["vehicleTypelbl":vehicleTypeLabel]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[vehicleRegNolbl(200)]", options: [], metrics: nil, views: ["vehicleRegNolbl":vehicleRegNoLabel]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[vehicleSeatlbl(200)]", options: [], metrics: nil, views: ["vehicleSeatlbl":vehicleSeatInfoLabel]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[vehicleIsDefault(15)]-5-[vehicleIsDefaultlbl(200)]", options: [], metrics: nil, views: ["vehicleIsDefault":isDefaultVehicle,"vehicleIsDefaultlbl":isDefaultVehicleLabel]))

        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[update(50)]-20-[delete(50)]-10-|", options: [], metrics: nil, views: ["update":updateLabel,"delete":deleteLabel]))
        
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[update(30)]-5-|", options: [], metrics: nil, views: ["update":updateLabel]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[delete(30)]-5-|", options: [], metrics: nil,
            views: ["delete":deleteLabel]))
    }
    
    func updateCell() ->  Void
    {
        var imgvalue : String?
        
        imgvalue = ""
        
        if  data.vehicleType == "Bike"
        {
            imgvalue = "bike_Big"
        }
        else if data.vehicleType == "Auto"
        {
            imgvalue = "auto_Big"
        }
        else if data.vehicleType == "Car"
        {
            imgvalue = "car_Big"
        }
        else if data.vehicleType == "Cab"
        {
            imgvalue = "cab_Big"
        }
        else if data.vehicleType == "Suv"
        {
            imgvalue = "suv_Big"
        }
        else if data.vehicleType == "Tempo"
        {
            imgvalue = "van_Big"
        }
        else if data.vehicleType == "Bus"
        {
            imgvalue = "bus_Big"
        }
        
        vehicleTypeImageView.image = UIImage(named: imgvalue!)
        vehicleTypeLabel.text = "\(data.vehicleType!): \(data.vehicleModel!)"
        vehicleRegNoLabel.text = data.RegNo
        
        let myNumber = NumberFormatter().number(from: data.capacity!)
        vehicleSeatInfoLabel.text = "\(myNumber!.int32Value - 1) Free Seats"
        
        isDefaultVehicle.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        isDefaultVehicle.isSelected = false
        
        if  data.isDefault == "1"
        {
            isDefaultVehicle.isSelected = true
            isDefaultVehicle.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
            saveDefaultVehicleDetails()
        }
    }
    
    func buttonPressed (_ sender: UIButton)
    {
        if  sender .isEqual(self.updateLabel) {
            saveDefaultVehicleDetails()
            self.cellDelegate?.updateButtonPresseEven()
        }
        else
        {
            UserInfo.sharedInstance.vehicleSeqID = data.vehicleSeqId!
            self.cellDelegate?.deleteButtonPressEvent(sender)
        }
    }
    
    func isDefaultButtonPressed(_ sender: UIButton) -> Void {
        
        sender.isSelected = !sender.isSelected
        
        isDefaultVehicle.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        
        if  sender.isSelected == true
        {
           isDefaultVehicle.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
            saveDefaultVehicleDetails()
            self.cellDelegate?.isDefaultButtonPressedEventCalled()
        }
    }
    
    func saveDefaultVehicleDetails() -> Void
    {
        UserInfo.sharedInstance.vehicleSeqID = data.vehicleSeqId!
        UserInfo.sharedInstance.vehicleType = data.vehicleType!
        UserInfo.sharedInstance.vehicleModel = data.vehicleModel!
        UserInfo.sharedInstance.vehicleRegNo = data.RegNo!
        UserInfo.sharedInstance.vehicleSeatCapacity = data.capacity!
        UserInfo.sharedInstance.vehicleKind = data.vehicleKind!
        UserInfo.sharedInstance.vehicleOwnerName = data.vehicleOwnerName!
        UserInfo.sharedInstance.insurerName = data.insurerName!
        UserInfo.sharedInstance.insuredCmpnyName = data.insuredCmpnyName!
        UserInfo.sharedInstance.insuranceId = data.insuranceId!
        UserInfo.sharedInstance.insuranceExpiryDate = data.insuranceExpiryDate!
        UserInfo.sharedInstance.insUploadStatus = data.insUploadStatus!
        UserInfo.sharedInstance.insUrl = data.insUrl!
        UserInfo.sharedInstance.rcUploadStatus = data.rcUploadStatus!
        UserInfo.sharedInstance.rcUrl = data.rcUrl!
    }
}
