//
//  WorkplaceCell.swift
//  rideally
//
//  Created by Sarav on 13/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import CoreGraphics
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class WorkplaceCell: UITableViewCell {
    
    let profilePic = UIImageView()
    let lblWPName = UILabel()
    let imgSource = UIImageView()
    let lblLocation = UILabel()
    
    let imgMembers = UIImageView()
    let lblMembersCnt = UILabel()
    let lblLine = UILabel()
    let lblRides = UILabel()
    let lblRidesCnt = UILabel()
    
    let btnAction = UIButton()
    let btnAdd = UIButton()
    
    let lblWPJoinStatus = UILabel()
    
    var onUserActionBlock: actionBlockWithParam?
    var newRideReqObj = WPNewRideRequest()
    
    var data: WorkPlace!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        lblWPName.font = boldFontWithSize(13)
        lblLocation.font = boldFontWithSize(13)
        
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        lblWPName.translatesAutoresizingMaskIntoConstraints = false
        lblLocation.translatesAutoresizingMaskIntoConstraints = false
        imgSource.translatesAutoresizingMaskIntoConstraints = false
        
        imgMembers.translatesAutoresizingMaskIntoConstraints = false
        lblMembersCnt.translatesAutoresizingMaskIntoConstraints = false
        lblLine.translatesAutoresizingMaskIntoConstraints = false
        lblRides.translatesAutoresizingMaskIntoConstraints = false
        lblRidesCnt.translatesAutoresizingMaskIntoConstraints = false
        
        btnAction.translatesAutoresizingMaskIntoConstraints = false
        btnAdd.translatesAutoresizingMaskIntoConstraints = false
        
        imgSource.image = UIImage(named: "source")
        
        self.contentView.addSubview(profilePic)
        self.contentView.addSubview(lblWPName)
        self.contentView.addSubview(lblLocation)
        self.contentView.addSubview(imgSource)
        self.contentView.addSubview(imgMembers)
        self.contentView.addSubview(lblMembersCnt)
        self.contentView.addSubview(lblLine)
        self.contentView.addSubview(lblRides)
        self.contentView.addSubview(lblRidesCnt)
        
        self.contentView.addSubview(btnAdd)
        self.contentView.addSubview(btnAction)
        
        profilePic.backgroundColor = UIColor.clear
        profilePic.layer.cornerRadius = 30
        profilePic.layer.masksToBounds = true
        
        profilePic.isUserInteractionEnabled = true
        lblWPJoinStatus.textColor = Colors.WHITE_COLOR
        lblWPJoinStatus.translatesAutoresizingMaskIntoConstraints = false
        lblWPJoinStatus.font = normalFontWithSize(8)
        lblWPJoinStatus.textAlignment = .center
        lblWPJoinStatus.backgroundColor = Colors.ORANGE_COLOR
        profilePic.addSubview(lblWPJoinStatus)
        
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblWPJoinStatus]))
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lbl(10)]|", options: [], metrics: nil, views: ["lbl":lblWPJoinStatus]))
        
        lblWPName.textColor = Colors.GRAY_COLOR
        lblLocation.textColor = Colors.GRAY_COLOR
        
        imgMembers.image = UIImage(named: "members_new")
        imgMembers.isUserInteractionEnabled = true
        lblMembersCnt.font = boldFontWithSize(12)
        lblMembersCnt.textColor = Colors.ORANGE_COLOR
        //        let tapMembersGesture = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
        //        lblMembers.addGestureRecognizer(tapMembersGesture)
        lblLine.backgroundColor = Colors.GRAY_COLOR
        lblRides.text = "Rides"
        lblRides.font = boldFontWithSize(13)
        lblRides.textColor = Colors.GREEN_COLOR
        lblRides.isUserInteractionEnabled = true
        lblRidesCnt.text = ""
        lblRidesCnt.font = boldFontWithSize(13)
        lblRidesCnt.textColor = Colors.ORANGE_COLOR
        
        let tapMembers = UITapGestureRecognizer(target: self, action: #selector(loadMembers))
        imgMembers.addGestureRecognizer(tapMembers)
        let tapRides = UITapGestureRecognizer(target: self, action: #selector(loadRides))
        lblRides.isUserInteractionEnabled = true
        lblRides.addGestureRecognizer(tapRides)
        
        btnAction.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnAction.titleLabel?.font = normalFontWithSize(13)
        btnAction.setTitle("INVITE", for: UIControlState())
        btnAction.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
        btnAction.addTarget(self, action: #selector(btnActionClicked), for: .touchDown)
        
        btnAdd.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnAdd.titleLabel?.font = normalFontWithSize(13)
        btnAdd.setImage(UIImage(named: "wpadd"), for: UIControlState())
        btnAdd.addTarget(self, action: #selector(addRide), for: .touchDown)
        
        let viewsDict = ["pic":profilePic, "name":lblWPName, "location":lblLocation, "sourceicon":imgSource, "members":imgMembers, "memberscnt":lblMembersCnt, "line":lblLine, "rides":lblRides, "ridescnt":lblRidesCnt, "btnadd":btnAdd, "btnaction":btnAction]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[pic(60)]-5-[name][btnadd(20)]-15-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[sourceicon(15)]-5-[location]-15-|", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[members]-2-[memberscnt]-5-[line(0.5)]-5-[rides]-3-[ridescnt]", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(60)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[name(20)][location(20)]-10-[members]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btnadd(20)]", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[memberscnt(20)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[rides(20)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[ridescnt(20)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line(15)]", options: [], metrics: nil, views: viewsDict))
        
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[btnaction(150)]-15-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraint(NSLayoutConstraint(item: btnAction, attribute: .centerY, relatedBy: .equal, toItem: imgMembers, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: btnAction, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: btnAdd, attribute: .centerY, relatedBy: .equal, toItem: lblWPName, attribute: .centerY, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: imgSource, attribute: .left, relatedBy: .equal, toItem: lblWPName, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: imgSource, attribute: .centerY, relatedBy: .equal, toItem: lblLocation, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: imgMembers, attribute: .left, relatedBy: .equal, toItem: lblWPName, attribute: .left, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblMembersCnt, attribute: .centerY, relatedBy: .equal, toItem: imgMembers, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblLine, attribute: .centerY, relatedBy: .equal, toItem: imgMembers, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblRides, attribute: .centerY, relatedBy: .equal, toItem: imgMembers, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblRidesCnt, attribute: .centerY, relatedBy: .equal, toItem: imgMembers, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    func loadMembers()
    {
        onUserActionBlock?("MEMBERS" as AnyObject?)
    }
    
    func loadRides()
    {
        onUserActionBlock?("RIDES" as AnyObject?)
    }
    
    func addRide()
    {
        onUserActionBlock?("ADDRIDE" as AnyObject?)
    }
    
    func btnActionClicked()
    {
        onUserActionBlock?(btnAction.titleLabel?.text! as AnyObject?)
    }
    
    func showPlaceholderPic()
    {
        if(data.wpScope == "1" || data.wpTieUpStatus == "1"){
            profilePic.image = UIImage(named: "wpPlaceholderL")
            //TODO: show lock
        }
        else{
            profilePic.image = UIImage(named: "wpPlaceholder")
        }
        let url = URL(string:SERVER+"static/groups/"+data.wpID!+".jpg")
        profilePic.kf.setImage(with: url, placeholder: profilePic.image, options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func buildCell()
    {
        if let url = data?.wpLogoURL{
            if(url != ""){
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                profilePic.layer.borderColor = Colors.GREEN_COLOR.cgColor
                profilePic.layer.borderWidth = 1.0
                if(data.wpScope == "1" || data.wpTieUpStatus == "1"){
                    //TODO: SHOWlock
                }
            }
            else{
                showPlaceholderPic()
            }
        }
        else{
            showPlaceholderPic()
        }
        
        lblWPJoinStatus.isHidden = false
        btnAdd.isHidden = false
        if(data.wpMembershipStatus == "0"){
            btnAdd.isHidden = true
            lblWPJoinStatus.text = "PENDING"
            if(data.wpScope == "1") {
                btnAction.isUserInteractionEnabled = false
                btnAction.titleLabel?.font = normalFontWithSize(9)
                btnAction.setTitleColor(Colors.ORANGE_COLOR, for: UIControlState())
                if(data.wpTieUpStatus == "1"){
                    btnAction.setTitle("AWAITING FOR EMAIL VERIFICATION", for: UIControlState())
                } else {
                    btnAction.setTitle("AWAITING FOR VERIFICATION", for: UIControlState())
                }
            } else {
                 btnAction.setTitle("CANCEL", for: UIControlState())
            }
        }
        else if(data.wpMembershipStatus == "1"){
            btnAction.isUserInteractionEnabled = true
            if(data.wpInitiator == UserInfo.sharedInstance.userID){
                lblWPJoinStatus.text = "OWNED"
            }
            else{
                lblWPJoinStatus.text = "JOINED"
            }
            btnAction.titleLabel?.font = normalFontWithSize(13)
            btnAction.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
            btnAction.setTitle("INVITE", for: UIControlState())
        }
        else{
            btnAction.isUserInteractionEnabled = true
            lblWPJoinStatus.isHidden = true
            btnAction.titleLabel?.font = normalFontWithSize(13)
            btnAction.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
            btnAction.setTitle("JOIN", for: UIControlState())
            btnAdd.isHidden = true
        }
        
        lblWPName.text = data.wpName
        lblLocation.text = data.wpLocation
        lblMembersCnt.text = data.wpMembers
    }
}

class AllWPCell: WorkplaceCell {
    
    override func buildCell()
    {
        btnAdd.isHidden = false
        
        if let url = data?.wpLogoURL{
            if(url != ""){
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                if(data.wpScope == "1" || data.wpTieUpStatus == "1"){
                    //TODO: SHOWlock
                }
            }
            else{
                showPlaceholderPic()
            }
        }
        else{
            showPlaceholderPic()
        }
        
        lblWPJoinStatus.isHidden = false
        if(data.wpMembershipStatus == "0"){
            lblWPJoinStatus.text = "PENDING"
            if(data.wpScope == "1") {
                btnAction.isUserInteractionEnabled = false
                btnAction.titleLabel?.font = normalFontWithSize(9)
                btnAction.setTitleColor(Colors.ORANGE_COLOR, for: UIControlState())
                if(data.wpTieUpStatus == "1"){
                    btnAction.setTitle("AWAITING FOR EMAIL VERIFICATION", for: UIControlState())
                } else {
                    btnAction.setTitle("AWAITING FOR VERIFICATION", for: UIControlState())
                }
            } else {
                btnAction.setTitle("CANCEL", for: UIControlState())
            }
        }
        else if(data.wpMembershipStatus == "1"){
            btnAction.isUserInteractionEnabled = true
            if(data.wpInitiator == UserInfo.sharedInstance.userID){
                lblWPJoinStatus.text = "OWNED"
            }
            else{
                lblWPJoinStatus.text = "JOINED"
            }
            btnAction.titleLabel?.font = normalFontWithSize(13)
            btnAction.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
            btnAction.setTitle("INVITE", for: UIControlState())
        }
        else{
            btnAction.isUserInteractionEnabled = true
            lblWPJoinStatus.isHidden = true
            btnAction.titleLabel?.font = normalFontWithSize(13)
            btnAction.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
            btnAction.setTitle("JOIN", for: UIControlState())
            if(data.wpScope == "0") {
                btnAdd.isHidden = false
            } else {
                btnAdd.isHidden = true
            }
        }
        
        lblWPName.text = data.wpName
        lblLocation.text = data.wpLocation
        lblMembersCnt.text = data.wpMembers
    }
}

class ExpandedWPCell: WorkplaceCell {
    
    let detailsView = UIView()
    
    let lblTitle = UILabel()
    let lblLoc1 = UILabel()
    let btnSwap = UIButton()
    let lblLoc2 = UILabel()
    
    let txtTime = RATextField()
    
    let btnDetail = UIButton()
    let btnCreate = UIButton()
    
    let timePicker = UIDatePicker()
    
    var onRideCreationDetail: actionBlockWithParam?
    var callUpdateVehicle: actionBlockWithStringParam?
    var onCreateRide: actionBlockWith3Params?
    
    var isStartLocWP = false
    
    override func buildCell()
    {
        super.buildCell()
        txtTime.text = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
    }
    
    override func commonInit()
    {
        super.commonInit()
        
        detailsView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(detailsView)
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":detailsView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(40)]|", options: [], metrics: nil, views: ["view":detailsView]))
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblLoc1.translatesAutoresizingMaskIntoConstraints = false
        lblLoc2.translatesAutoresizingMaskIntoConstraints = false
        btnSwap.translatesAutoresizingMaskIntoConstraints = false
        txtTime.translatesAutoresizingMaskIntoConstraints = false
        btnDetail.translatesAutoresizingMaskIntoConstraints = false
        btnCreate.translatesAutoresizingMaskIntoConstraints = false
        
        detailsView.addSubview(lblTitle)
        detailsView.addSubview(lblLoc1)
        detailsView.addSubview(btnSwap)
        detailsView.addSubview(lblLoc2)
        detailsView.addSubview(txtTime)
        detailsView.addSubview(btnDetail)
        detailsView.addSubview(btnCreate)
        
        lblTitle.text = "Time from"
        lblTitle.font = boldFontWithSize(13)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.textAlignment = .center
        lblLoc1.text = "Home"
        lblLoc1.font = boldFontWithSize(13)
        lblLoc1.textColor = Colors.GRAY_COLOR
        lblLoc1.textAlignment = .center
        lblLoc2.text = "Work"
        lblLoc2.font = boldFontWithSize(13)
        lblLoc2.textColor = Colors.GRAY_COLOR
        lblLoc2.textAlignment = .center
        
        txtTime.backgroundColor = Colors.WHITE_COLOR
        txtTime.font = boldFontWithSize(13)
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "NL")
        txtTime.inputView = timePicker
        timePicker.date = Date().addingTimeInterval(300)
        timePicker.addTarget(self, action: #selector(timePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.contentView.frame.size.width, height: 40))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        btnAdd.setImage(UIImage(named: "wpdelete"), for: UIControlState())
        
        btnSwap.setImage(UIImage(named: "hflip"), for: UIControlState())
        btnSwap.addTarget(self, action: #selector(swapLocations), for: .touchDown)
        
        btnDetail.setImage(UIImage(named: "wpsummary"), for: UIControlState())
        btnDetail.addTarget(self, action: #selector(showRideCreationVC), for: .touchDown)
        
        btnCreate.setImage(UIImage(named: "wpcreate"), for: UIControlState())
        btnCreate.addTarget(self, action: #selector(proceed), for: .touchDown)
        
        let viewsDict = ["title":lblTitle, "loc1":lblLoc1, "swap":btnSwap, "loc2":lblLoc2, "time":txtTime, "details":btnDetail, "create":btnCreate]
        
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title(60)][loc1(40)][swap(30)][loc2(40)]-5-[time(50)]-10-[details(30)]-10-[create(30)]", options: [], metrics: nil, views: viewsDict))
        
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title]-5-|", options: [], metrics: nil, views: viewsDict))
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[loc1]-5-|", options: [], metrics: nil, views: viewsDict))
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[swap]-5-|", options: [], metrics: nil, views: viewsDict))
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[loc2]-5-|", options: [], metrics: nil, views: viewsDict))
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[time]-5-|", options: [], metrics: nil, views: viewsDict))
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[details]-5-|", options: [], metrics: nil, views: viewsDict))
        detailsView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[create]-5-|", options: [], metrics: nil, views: viewsDict))
    }
    
    func showRideCreationVC()
    {
        onRideCreationDetail?(isStartLocWP as AnyObject?)
    }
    
    func proceed()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Workplace Tab",
            AnalyticsParameterItemName: "Share Time",
            AnalyticsParameterContentType:"Submit Button"
            ])
        if(UserInfo.sharedInstance.homeAddress != ""){
            if(txtTime.text?.characters.count <= 0){
                AlertController.showAlertFor("Create Ride", message: "Please choose a time for your travel.", okButtonTitle: "Ok", okAction: nil)
                return
            }
            newRideReqObj.userId = UserInfo.sharedInstance.userID
            newRideReqObj.selected_dates = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
            newRideReqObj.pool_scope = data.wpID
            if(data?.wpScope == "1") {
                newRideReqObj.pool_shared = "0"
            } else {
                newRideReqObj.pool_shared = "1"
            }
            newRideReqObj.fromLat = data.wpLat
            newRideReqObj.fromLong = data.wpLong
            newRideReqObj.fromLocation = data.wpLocation
            newRideReqObj.toLat = UserInfo.sharedInstance.homeLatValue
            newRideReqObj.toLong = UserInfo.sharedInstance.homeLongValue
            newRideReqObj.toLocation = UserInfo.sharedInstance.homeAddress
            if(isStartLocWP){
                newRideReqObj.comingtime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
                newRideReqObj.goingtime = ""
            }
            else{
                newRideReqObj.goingtime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
                newRideReqObj.comingtime = ""
            }
            
            //self.newRideReqObj.ridetype = "N"
            
            WorkplaceRequestor().getLastRide({ (success, object) in
                let lastRideObj = (object as! LastRideResponse).data?.first
                if(success == true){
                    self.newRideReqObj.ridetype = (lastRideObj?.rideType)!
                    self.newRideReqObj.poolType = lastRideObj?.poolType
                    if let vehRegNo = lastRideObj?.vehRegNo {
                        self.newRideReqObj.vehregno = vehRegNo
                        UserInfo.sharedInstance.vehicleSeqID = (lastRideObj?.vehSeqId)!
                        UserInfo.sharedInstance.vehicleType = (lastRideObj?.vehType)!
                        UserInfo.sharedInstance.vehicleModel = (lastRideObj?.vehModel)!
                        UserInfo.sharedInstance.vehicleRegNo = (lastRideObj?.vehRegNo)!
                        UserInfo.sharedInstance.vehicleSeatCapacity = (lastRideObj?.capacity)!
                        UserInfo.sharedInstance.vehicleKind = (lastRideObj?.vehKind)!
                        UserInfo.sharedInstance.vehicleOwnerName = (lastRideObj?.vehOwnerName)!
                        UserInfo.sharedInstance.insurerName = (lastRideObj?.insurerName)!
                        UserInfo.sharedInstance.insuredCmpnyName = (lastRideObj?.insuredCmpnyName)!
                        UserInfo.sharedInstance.insuranceId = (lastRideObj?.insuranceId)!
                        UserInfo.sharedInstance.insuranceExpiryDate = (lastRideObj?.insuranceExpiryDate)!
                        UserInfo.sharedInstance.insUploadStatus = (lastRideObj?.insuranceUploadStatus)!
                        UserInfo.sharedInstance.insUrl = (lastRideObj?.insuranceUrl)!
                        UserInfo.sharedInstance.rcUploadStatus = (lastRideObj?.rcUploadStatus)!
                        UserInfo.sharedInstance.rcUrl = (lastRideObj?.rcUrl)!
                    }
                    if let capacity = lastRideObj?.capacity {
                        self.newRideReqObj.veh_capacity = String(Int(capacity)!-1)
                    }
                    self.newRideReqObj.cost = lastRideObj?.cost
                    self.newRideReqObj.cost_type = (lastRideObj?.costType)!
                    self.checkInsurance("1")
                } else {
                    self.checkInsurance("2")
                }
                }, failure: { (error) in
                    self.checkInsurance("1")
            })
        }
        else{
            self.checkInsurance("2")
        }
    }
    
    func checkInsurance(_ callMethod: String) {
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: todayDate) as String
        if(newRideReqObj.vehregno != "" && newRideReqObj.vehregno != nil && data?.wpInsuranceStatus == "1" && (UserInfo.sharedInstance.insuranceId == "")) {
            AlertController.showAlertFor("Create Ride", message: "Please provide vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                self.updateVehicle()
            })
            //return
        } else if(newRideReqObj.vehregno != "" && newRideReqObj.vehregno != nil && data?.wpInsuranceStatus == "1" && UserInfo.sharedInstance.insuranceId != ""  && UserInfo.sharedInstance.insuranceExpiryDate.compare(date) == ComparisonResult.orderedAscending) {
            AlertController.showAlertFor("Create Ride", message: "Please update vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                self.updateVehicle()
            })
            //return
        } else {
            if(callMethod == "1") {
                self.getDistanceDuration()
            } else {
                showRideCreationVC()
            }
        }
    }
    
    func updateVehicle()
    {
        callUpdateVehicle?(data?.wpInsuranceStatus)
    }
    
    func getDistanceDuration()
    {
        CommonRequestor().getDistanceDuration(self.newRideReqObj.fromLocation!, destination: self.newRideReqObj.toLocation!, success: { (success, object) in
            self.newRideReqObj.duration = ""
            self.newRideReqObj.distance = ""
            if let data = object as? NSDictionary{
                let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                if((elements["status"] as! String) == "OK")
                {
                    let distance = ((elements["distance"] as! NSDictionary)["text"])!
                    let duration = ((elements["duration"] as! NSDictionary)["text"])!
                    self.newRideReqObj.duration = duration as! String
                    self.newRideReqObj.distance = distance as! String
                }
            }
            let sourceStr = "\(self.newRideReqObj.fromLat!),\(self.newRideReqObj.fromLong!)"
            let destinationStr = "\(self.newRideReqObj.toLat!), \(self.newRideReqObj.toLong!)"
            CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                self.newRideReqObj.via = ""
                self.newRideReqObj.waypoints = ""
                if let data = object as? NSDictionary{
                    let routes = data["routes"] as! NSArray
                    if routes.count == 0{
                        return
                    }
                    let routesDict = routes[0] as! NSDictionary
                    let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                    let points = overview_polyline ["points"] as! NSString
                    self.newRideReqObj.via = routesDict ["summary"] as? String
                    self.newRideReqObj.waypoints = points as String
                }
                self.searchForRides()
                }, failure: { (error) in
                    self.searchForRides()
            })
            }, failure: { (error) in
                self.searchForRides()
        })
    }
    
    func createRide()
    {
        showIndicator("Creating Ride..")
        newRideReqObj.userId = UserInfo.sharedInstance.userID
        WorkplaceRequestor().addWPRide(newRideReqObj, success: { (success, object) in
            hideIndicator()
            if(success){
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! NeedRideResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            self.onCreateRide?(nil, nil, object)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! NeedRideResponse).code == "1811") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! NeedRideResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func searchForRides()
    {
        let searchReqObj = SearchRideRequest()
        searchReqObj.userId = UserInfo.sharedInstance.userID
        searchReqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        searchReqObj.reqTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        searchReqObj.poolType = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        if(isStartLocWP){
            searchReqObj.fromLat = newRideReqObj.fromLat
            searchReqObj.fromLong = newRideReqObj.fromLong
            searchReqObj.startLoc = newRideReqObj.fromLocation
            searchReqObj.toLat = newRideReqObj.toLat
            searchReqObj.toLong = newRideReqObj.toLong
            searchReqObj.endLoc = newRideReqObj.toLocation
        } else {
            searchReqObj.fromLat = newRideReqObj.toLat
            searchReqObj.fromLong = newRideReqObj.toLong
            searchReqObj.startLoc = newRideReqObj.toLocation
            searchReqObj.toLat = newRideReqObj.fromLat
            searchReqObj.toLong = newRideReqObj.fromLong
            searchReqObj.endLoc = newRideReqObj.fromLocation
        }
        searchReqObj.groupID = data.wpID
        searchReqObj.rideScope = data.wpID
        searchReqObj.groupType = "6"
        searchReqObj.poolScope = data.wpID
        if let lastRideType = newRideReqObj.ridetype {
            searchReqObj.ridetype = lastRideType
        }
        searchReqObj.searchType = "generic_location"
        searchReqObj.searchFrom = "anywhere"
        
        showIndicator("Searching for similair rides.")
        
        WorkplaceRequestor().searchWPRides(searchReqObj, success: { (success, object) in
            hideIndicator()
            if(success){
                if(self.isStartLocWP){
                    if((object as! WPRidesResponse).pools?.homeGoing!.count > 0){
                        self.onCreateRide?(((object as! WPRidesResponse).pools?.homeGoing)!, self.newRideReqObj, nil)
                    }
                    else{
                        self.createRide()
                    }
                }
                else{
                    if((object as! WPRidesResponse).pools?.officeGoing!.count > 0){
                        self.onCreateRide?(((object as! WPRidesResponse).pools?.officeGoing)!, self.newRideReqObj, nil)
                    }
                    else{
                        self.createRide()
                    }
                }
            }
            }, failure: { (error) in
                hideIndicator()
                self.createRide()
        })
    }
    
    func swapLocations()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Workplace Tab",
            AnalyticsParameterItemName: "Share Time",
            AnalyticsParameterContentType:"Flip Button"
            ])
        if(isStartLocWP){
            isStartLocWP = false
        }
        else{
            isStartLocWP = true
        }
        let txt = lblLoc2.text
        lblLoc2.text = lblLoc1.text
        lblLoc1.text = txt
    }
    
    func timePicked()
    {
        
    }
    
    func donePressed()
    {
        txtTime.text = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        txtTime.resignFirstResponder()
    }
    
    
    
}
