//
//  MembersViewController.swift
//  rideally
//
//  Created by Sarav on 21/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import Kingfisher

class MemberCell : UITableViewCell{
    var data: Member!{
        didSet{
            buildCell()
        }
    }
    var poolTaxiBooking: String?
    var onUserActionBlock: actionBlockWithParams?
    
    let profilePic = UIImageView()
    let lblName = UILabel()
    let lblEmail = UILabel()
    let lblMobile = UILabel()
    
    let lblJoinPoint = UILabel()
    let lblJoinPointValue = UILabel()
    let lblDropPoint = UILabel()
    let lblDropPointValue = UILabel()
    let lblComment = UILabel()
    let lblCommentValue = UILabel()
    
    let lblVehicle = UILabel()
    let lblVehicleType = UILabel()
    let imgVehicle = UIImageView()
    
    let lblCost = UILabel()
    let lblCostValue = UILabel()
    
    let btnAccept = UIButton()
    let btnReject = UIButton()
    
    let verificationView = UIView()
    let emailIcon = UIImageView()
    let mobileIcon = UIImageView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        profilePic.backgroundColor = UIColor.gray
        profilePic.isUserInteractionEnabled = true
        self.contentView.addSubview(profilePic)
        
        verificationView.backgroundColor = Colors.BLACK_COLOR
        verificationView.alpha = 0.8
        verificationView.translatesAutoresizingMaskIntoConstraints = false
        emailIcon.image = UIImage(named: "email")
        emailIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(emailIcon)
        mobileIcon.image = UIImage(named: "phone")
        mobileIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(mobileIcon)
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[email(15)]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mobile(15)]", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mobile]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[email]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
        profilePic.addSubview(verificationView)
        
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":verificationView]))
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(15)]|", options: [], metrics: nil, views: ["view":verificationView]))
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = boldFontWithSize(15)
        lblName.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblName)
        
        lblEmail.translatesAutoresizingMaskIntoConstraints = false
        lblEmail.font = boldFontWithSize(10)
        lblEmail.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblEmail)
        
        lblMobile.translatesAutoresizingMaskIntoConstraints = false
        lblMobile.font = boldFontWithSize(10)
        lblMobile.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblMobile)
        
        lblVehicle.translatesAutoresizingMaskIntoConstraints = false
        lblVehicle.font = boldFontWithSize(11)
        lblVehicle.textColor = Colors.GREEN_COLOR
        self.contentView.addSubview(lblVehicle)
        
        imgVehicle.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(imgVehicle)
        
        lblVehicleType.translatesAutoresizingMaskIntoConstraints = false
        lblVehicleType.font = boldFontWithSize(10)
        lblVehicleType.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblVehicleType)
        
        lblCost.translatesAutoresizingMaskIntoConstraints = false
        lblCost.font = boldFontWithSize(11)
        lblCost.textColor = Colors.GREEN_COLOR
        self.contentView.addSubview(lblCost)

        lblCostValue.translatesAutoresizingMaskIntoConstraints = false
        lblCostValue.font = boldFontWithSize(10)
        lblCostValue.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblCostValue)
        
        lblJoinPoint.translatesAutoresizingMaskIntoConstraints = false
        lblJoinPoint.font = boldFontWithSize(14)
        lblJoinPoint.textColor = Colors.GREEN_COLOR
        lblJoinPoint.numberOfLines = 0
        self.contentView.addSubview(lblJoinPoint)
        
        lblJoinPointValue.translatesAutoresizingMaskIntoConstraints = false
        lblJoinPointValue.font = boldFontWithSize(13)
        lblJoinPointValue.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblJoinPointValue)
        
        lblDropPoint.translatesAutoresizingMaskIntoConstraints = false
        lblDropPoint.font = boldFontWithSize(14)
        lblDropPoint.textColor = Colors.GREEN_COLOR
        lblDropPoint.numberOfLines = 0
        self.contentView.addSubview(lblDropPoint)
        
        lblDropPointValue.translatesAutoresizingMaskIntoConstraints = false
        lblDropPointValue.font = boldFontWithSize(13)
        lblDropPointValue.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblDropPointValue)
        
        lblComment.translatesAutoresizingMaskIntoConstraints = false
        lblComment.font = boldFontWithSize(14)
        lblComment.textColor = Colors.GREEN_COLOR
        lblComment.numberOfLines = 0
        self.contentView.addSubview(lblComment)
        
        lblCommentValue.translatesAutoresizingMaskIntoConstraints = false
        lblCommentValue.font = boldFontWithSize(13)
        lblCommentValue.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblCommentValue)
        
        btnAccept.translatesAutoresizingMaskIntoConstraints = false
        btnAccept.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnAccept.setTitle("Accept", for: UIControlState())
        btnAccept.titleLabel?.font = boldFontWithSize(13)
        self.contentView.addSubview(btnAccept)
        
        btnReject.translatesAutoresizingMaskIntoConstraints = false
        btnReject.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnReject.setTitle("Reject", for: UIControlState())
        btnReject.titleLabel?.font = boldFontWithSize(13)
        self.contentView.addSubview(btnReject)
        
        btnAccept.addTarget(self, action: #selector(btn_clicked(_:)), for: .touchDown)
        btnReject.addTarget(self, action: #selector(btn_clicked(_:)), for: .touchDown)
        
        let viewsDict = ["pic":profilePic, "name":lblName, "email":lblEmail, "mobile":lblMobile, "accept":btnAccept, "reject":btnReject, "joinpoint":lblJoinPoint, "droppoint":lblDropPoint, "comment":lblComment,"joinpointv":lblJoinPointValue, "droppointv":lblDropPointValue, "commentv":lblCommentValue, "veh":lblVehicle, "vehtype":lblVehicleType, "imgveh":imgVehicle, "cost":lblCost, "costv":lblCostValue]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[pic(90)]-5-[joinpoint]-5-[droppoint]-5-[comment]", options: [], metrics: nil, views: viewsDict))
        
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[name]-5-[email]-5-[mobile]-5-[veh]-5-[cost]", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-75-[accept(25)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-75-[reject(25)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[accept]-20-[reject]-10-|", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[pic(100)]-10-[name]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[joinpoint(78)]-2-[joinpointv]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[droppoint(65)]-2-[droppointv]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[comment(60)]-2-[commentv]-5-|", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-115-[veh(40)]-2-[imgveh(15)]-2-[vehtype]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-115-[cost(35)]-2-[costv]-5-|", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblEmail, attribute: .left, relatedBy: .equal, toItem: lblName, attribute: .left, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblMobile, attribute: .left, relatedBy: .equal, toItem: lblName, attribute: .left, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblVehicle, attribute: .left, relatedBy: .equal, toItem: lblName, attribute: .left, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblJoinPointValue, attribute: .top, relatedBy: .equal, toItem: lblJoinPoint, attribute: .top, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblDropPointValue, attribute: .top, relatedBy: .equal, toItem: lblDropPoint, attribute: .top, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblCommentValue, attribute: .top, relatedBy: .equal, toItem: lblComment, attribute: .top, multiplier: 1, constant: 0))
        
         self.contentView.addConstraint(NSLayoutConstraint(item: imgVehicle, attribute: .top, relatedBy: .equal, toItem: lblVehicle, attribute: .top, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblVehicleType, attribute: .top, relatedBy: .equal, toItem: imgVehicle, attribute: .top, multiplier: 1, constant: 0))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblCostValue, attribute: .top, relatedBy: .equal, toItem: lblCost, attribute: .top, multiplier: 1, constant: 0))
    }
    
    func buildCell()
    {
        lblName.text = data.name
        lblEmail.text = data.email
        lblMobile.text = data.mobile

        if(data.userState == "5"){
            emailIcon.image = UIImage(named: "email_v")
            mobileIcon.image = UIImage(named: "phone_v")
        }
        else if(data.userState == "4"){
            mobileIcon.image = UIImage(named: "phone_v")
        }
        else if(data.userState == "3"){
            emailIcon.image = UIImage(named: "email_v")
        }
        
        if(data?.profilePicStatus == "1"){
            if let url = data?.profilePic{
                if(url != ""){
                    profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                }
                else{
                    profilePic.image = UIImage(named: "placeholder")
                }
            }
            else{
                if let fbID = data.memberFBID{
                    profilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            profilePic.image = UIImage(named: "placeholder")
        }
        
        lblJoinPoint.isHidden = true
        lblJoinPointValue.isHidden = true
        lblDropPoint.isHidden = true
        lblDropPointValue.isHidden = true
        lblComment.isHidden = true
        lblCommentValue.isHidden = true
        lblVehicle.isHidden = true
        lblVehicleType.isHidden = true
        imgVehicle.isHidden = true
        lblCost.isHidden = true
        lblCostValue.isHidden = true
        if(data.joinStatus != nil && (data.joinStatus == "J" || data.joinStatus == "P") && poolTaxiBooking != "1") {
            if(data.pickupLocation != nil){
                lblJoinPoint.isHidden = false
                lblJoinPointValue.isHidden = false
                lblJoinPoint.text = "Joining Point:"
                lblJoinPointValue.text = data.pickupLocation
            }
            if(data.dropLocation != nil){
                lblDropPoint.isHidden = false
                lblDropPointValue.isHidden = false
                lblDropPoint.text = "Drop Point:"
                lblDropPointValue.text = data.dropLocation
            }
            if(data.comments != nil && data.comments != ""){
                lblComment.isHidden = false
                lblCommentValue.isHidden = false
                lblComment.text = "Comment:"
                lblCommentValue.text = data.comments
            }
            if let vehRegNo = data.vehicleRegNo {
                lblVehicle.isHidden = false
                lblVehicleType.isHidden = false
                imgVehicle.isHidden = false
                lblVehicle.text = "Offering:"
                lblVehicleType.text = vehRegNo
                if(data.vehicleType!.lowercased() == "bike") {
                    imgVehicle.image = UIImage(named: "veh_bike")
                } else if(data.vehicleType!.lowercased() == "auto") {
                    imgVehicle.image = UIImage(named: "veh_auto")
                } else if(data.vehicleType!.lowercased() == "car") {
                    imgVehicle.image = UIImage(named: "veh_car")
                } else if(data.vehicleType!.lowercased() == "cab") {
                    imgVehicle.image = UIImage(named: "veh_cab")
                } else if(data.vehicleType!.lowercased() == "suv") {
                    imgVehicle.image = UIImage(named: "veh_suv")
                } else if(data.vehicleType!.lowercased() == "tempo") {
                    imgVehicle.image = UIImage(named: "veh_tempo")
                } else if(data.vehicleType!.lowercased() == "bus") {
                    imgVehicle.image = UIImage(named: "veh_bus")
                }
            }
            if(data.riderCost != "" && data.riderCost != "0") {
                lblCost.isHidden = false
                lblCostValue.isHidden = false
                lblCost.text = "Points:"
                lblCostValue.text = "\(data.riderCost!) / \("Km")"
            }
        }
        
        btnAccept.isHidden = true
        btnReject.isHidden = true
        
        if(data.joinStatus == "P"){
            btnAccept.isHidden = false
            btnReject.isHidden = false
            
            btnAccept.setTitle("Accept", for: UIControlState())
            btnReject.setTitle("Reject", for: UIControlState())
        }
        else if(data.joinStatus == "F" || data.joinStatus == "S"){
            btnReject.isHidden = false
            btnReject.setTitle("Cancel", for: UIControlState())
        }
        
        setNeedsLayout()
    }
    
    func btn_clicked(_ btn: UIButton)
    {
        let str = btn.titleLabel?.text!
        if(str == "Accept"){
            onUserActionBlock?("accept" as AnyObject?, data.memberUserID as AnyObject?)
        } else if(str == "Reject"){
            onUserActionBlock?("reject" as AnyObject?, data.memberUserID as AnyObject?)
        } else if(str == "Cancel"){
            onUserActionBlock?("cancel" as AnyObject?, data.memberUserID as AnyObject?)
        }
    }
}

class MembersViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource {
    
    let tableView = UITableView()
    var tableHeightConstraint: NSLayoutConstraint?
    
    var membersList: [Member]?
    var poolID: String?
    var poolTaxiBooking :String?
    var onSelectBackBlock: actionBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Member List"
        
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        
        viewsArray.append(getTableView())
    }
    
    override func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
        onSelectBackBlock?()
    }
    
    func getTableView() -> SubView
    {
        tableView.separatorColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(MemberCell.self, forCellReuseIdentifier: "membercell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        let subview = SubView()
        subview.view = tableView
        tableHeightConstraint = NSLayoutConstraint(item: tableView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 500)
        subview.padding = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        subview.heightConstraint = tableHeightConstraint!
        return subview
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return membersList!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var ht: CGFloat = 100
        if(membersList![indexPath.row].pickupLocation != nil){
            ht = ht + 20
        }
        if(membersList![indexPath.row].dropLocation != nil){
            ht = ht + 20
        }
        if(membersList![indexPath.row].comments != nil && membersList![indexPath.row].comments != ""){
            ht = ht + 20
        }
        return ht
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "membercell") as! MemberCell
        cell.poolTaxiBooking = poolTaxiBooking
        cell.data = membersList![indexPath.row]
        cell.onUserActionBlock = { (selectedAction, memberID) -> Void in
            self.cell_btnAction(selectedAction as! String, memberID: memberID as! String)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        showIndicator("Getting owner's detail..")
        RideRequestor().getMembersDetails((membersList![indexPath.row]).memberUserID!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                let vc = MemberDetailViewController()
                vc.dataSource = (object as? MemberDetailResponse)?.data?.userInfo
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row%2 == 0){
            cell.backgroundColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
        }
        else{
            cell.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
        }
    }
    
    func cell_btnAction(_ action: String, memberID: String)
    {
        if(action == "cancel"){
            AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel the invited request?", okButtonTitle: "Yes", okAction: {
                 showIndicator("cancelling invited request..")
                RideRequestor().cancelRequest(self.poolID!, userId: memberID, cancelrequestType: "FR", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Cancel Request", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateMembers()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Cancel Request", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
                }, cancelButtonTitle: "Later", cancelAction: {
            })
        }
        else if(action == "accept"){
            AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this member?", okButtonTitle: "Yes", okAction: {
                showIndicator("Accepting ride request..")
                RideRequestor().acceptRideRequest(self.poolID!, userId: memberID, userType: "Join", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Accept Request", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateMembers()
                        })
                    }else{
                        AlertController.showAlertFor("Accept Request", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "Cancel") {
            }
        }
        else if(action == "reject"){
            AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this member?", okButtonTitle: "Yes", okAction: {
                showIndicator("Rejecting ride request..")
                RideRequestor().rejectRideRequest(self.poolID!, userId: memberID, userType: "Join", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Reject Request", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateMembers()
                        })
                    }else{
                        AlertController.showAlertFor("Reject Request", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "Cancel") {
            }
        }
    }
    
    func updateMembers()
    {
        showIndicator("Getting members list..")
        RideRequestor().getRideMembers(self.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                self.membersList = (object as! RideMembersResponse).poolUsers
                self.tableView.reloadData()
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
}
