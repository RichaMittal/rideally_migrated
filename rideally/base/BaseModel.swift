//
//  BaseModel.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import Foundation
import ObjectMapper

class BaseModel: NSObject, Mappable {
    
    var code: String?
    var message: String?
    
    required override init() {
        //
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
    }
}

class ErrorResponse: BaseModel {
    var mcDescription: String?
    var mcTitle: String?
    
    override func mapping(map: Map) {
        mcDescription <- map["mcDescription"]
        mcTitle <- map["mcTitle"]
    }
}
