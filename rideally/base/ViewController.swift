//
//  ViewController.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import ObjectMapper

extension UIViewController {
    
    func adjustNavigationbarProperties () {
        
        let back = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action: nil)
        self.navigationItem.backBarButtonItem = back
        
        //        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_back"), style: .Plain, target: self, action: #selector(backButtonPressed(_:)))
    }
    
    func shoudlPopViewController () -> Bool {
        return true
    }
    
    func backButtonPressed (_ sender: AnyObject) {
        if self.shoudlPopViewController() {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
}

class ViewController: UIViewController, UINavigationControllerDelegate {
    
    var _CTImgView: UIImageView?
    var currentNavigationColor: UIColor?
    
    var dummyNavBarView = UIView()
    var showFakeNavigationBar = false {
        didSet {
            if showFakeNavigationBar {
                let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64)
                dummyNavBarView.backgroundColor = Colors.NAV_TINT_BG//UIColor.clearColor()
                dummyNavBarView.frame = frame
                self.view.addSubview(dummyNavBarView)
            } else {
                dummyNavBarView.removeFromSuperview()
            }
        }
    }
    
    func dismissCueTip()
    {
        _CTImgView?.removeFromSuperview()
        _CTImgView = nil
        saveCueTipResponse()
    }
    
    func saveCueTipResponse()
    {
        
    }
    
    func showCueTips(_ image: String)
    {
        _CTImgView = UIImageView(image: UIImage(named: image))
        _CTImgView?.frame = UIScreen.main.bounds  //self.view.bounds
        _CTImgView!.isUserInteractionEnabled = true
        
        //_CTImgView?.alpha = 0.5
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissCueTip))
        _CTImgView!.addGestureRecognizer(tapGesture)
        
        self.navigationController?.view.addSubview(_CTImgView!)
        self.navigationController?.view.bringSubview(toFront: _CTImgView!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.adjustNavigationbarProperties()
        
        //self.navigationController?.delegate = self
    }
    
    func showLoadingIndicator(_ message: String) {
        showIndicator(message)
    }
    
    func hideLoadingIndiactor() {
        hideIndicator()
    }
    
    //MARK:- Hiding Navigation Controller
    internal var hideNavigationController: Bool? = false {
        didSet {
            if hideNavigationController == true {
                self.hideNavController()
                setNavColor(UIColor.clear)
            } else {
                setNavColor(Colors.NAV_TINT_BG)
                self.automaticallyAdjustsScrollViewInsets = true
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let bgColor = self.currentNavigationColor {
            setNavColor(bgColor)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.networkStatusChanged(_:)), name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
        
        self.navigationController?.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let overlay = self.navigationController?.navigationBar.overlay {
            currentNavigationColor = overlay.backgroundColor
        } else {
            currentNavigationColor = self.navigationController?.navigationBar.backgroundColor
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: ReachabilityStatusChangedNotification), object: nil)
    }
    
    //MARK:- Reachability
    func networkStatusChanged(_ notification: Notification) {
        _ = notification.userInfo
       // print(userInfo ?? <#default value#>)
        
        let status = Reach().connectionStatus()
        switch status {
        case .unknown, .offline:
            handleInternetHasBecomeInactive()
        default:
            handleInternetIsAvailable()
        }
    }
    
    func handleInternetIsAvailable () {
        //
    }
    
    func handleInternetHasBecomeInactive () {
        AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
    }
    
    // MARK:- Navigation controller
    func hideNavController () {
        self.navigationController?.navigationBar.isTranslucent = true
        setNavColor(UIColor.clear)
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    //MARK:- scroll view delegate
    fileprivate let NAVBAR_CHANGE_POINT:CGFloat = 50.0
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if hideNavigationController == true {
            let offsetY = scrollView.contentOffset.y
            let color: UIColor
            if (offsetY > NAVBAR_CHANGE_POINT) {
                let alpha = min(1, 1-((NAVBAR_CHANGE_POINT+64 - offsetY)/64))
                color = Colors.NAV_TINT_BG.withAlphaComponent(alpha)
            } else {
                color = Colors.NAV_TINT_BG.withAlphaComponent(0)
            }
            self.currentNavigationColor = color
            setNavColor(color)
        }
    }
    
    func setNavColor (_ color: UIColor) {
        self.navigationController?.navigationBar.lt_setBackgroundColor(color)
        dummyNavBarView.backgroundColor = color
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
    }
    
    func getMyWP()
    {
        showIndicator("Loading Workplaces.")
        WorkplaceRequestor().getMyWorkplaces({ (success, object) in
            hideIndicator()
            let dataSource = (object as! WorkPlacesResponse).data!.myOfcGroups!
            WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: (dataSource.first?.wpID)!, success: { (success1, object1) in
                 WorkplaceRequestor().getWPMembers("", groupID: (dataSource.first?.wpID)!, distanceRange: "1", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, objectMembers) in
                hideIndicator()
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : RideMapViewController = storyboard.instantiateViewController(withIdentifier: "RideMapViewController") as! RideMapViewController
                vc.hidesBottomBarWhenPushed = true
                vc.data = dataSource.first
                vc.dataSourceToOfc = (object1 as! WPRidesResponse).pools?.officeGoing
                vc.dataSourceToHome = (object1 as! WPRidesResponse).pools?.homeGoing
                vc.wpMembers = (objectMembers as! WPMembersResponseData).members
                vc.isFrom = "login"
                self.navigationController?.pushViewController(vc, animated: true)
                 }) { (error) in
                    //hideIndicator()
                }
            }) { (error) in
                hideIndicator()
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func sendDeviceToken()
    {
        let reqObj = deviceTokenRequest()
        reqObj.user_id = UserInfo.sharedInstance.userID
        reqObj.device_token = UserInfo.sharedInstance.deviceToken
        LoginRequestor().sendDeviceTokenRequest(reqObj, success:{(success, object) in
            if (object as! deviceTokenResponse).code == "491" {
                //print("success")
            } else {
                //AlertController.showToastForError("error while updating token id")
                // print("error while updating token id")
            }
        }) { (error) in
        }
    }
    
    func getUserPoints() {
        let reqObj = GetUserPointsOnRecahargeRequest()
        reqObj.userID =  UserInfo.sharedInstance.userID
        
        MyAccountRequestor().sendGetUserPointsRequest(reqObj, success:{ (success, object) in
            if (object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable != nil
            {
                UserInfo.sharedInstance.maxAllowedPoints = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.maximumallowedpoints)!
                UserInfo.sharedInstance.availablePoints = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable)!
            }
            if (object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsearned != nil
            {
                UserInfo.sharedInstance.earnedPointsViaRecharge = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsearned)!
            }
        }){ (error) in
            AlertController.showAlertForError(error)
        }
    }
    
    func compareAppVersions() {
        StaticPageRequestor().getAppVersionRequest(deviceType: "2", success:{ (success, object) in
            if (object as! getAppVersionResponse).dataObj?.first?.versionName != nil && (object as! getAppVersionResponse).dataObj?.first?.versionName != "" && (object as! getAppVersionResponse).dataObj?.first?.versionCode != nil && (object as! getAppVersionResponse).dataObj?.first?.versionCode != ""
            {
                let dictionary = Bundle.main.infoDictionary!
                let version = dictionary["CFBundleShortVersionString"] as! String
                let build = dictionary["CFBundleVersion"] as! String
                print(version)
                print(build)
                var cancelBtn: String?
                if((object as! getAppVersionResponse).dataObj?.first?.isUpdateMandatory != nil && (object as! getAppVersionResponse).dataObj?.first?.isUpdateMandatory != "") {
                    if((object as! getAppVersionResponse).dataObj?.first?.isUpdateMandatory == "1") {
                        cancelBtn = nil
                    } else {
                        cancelBtn = "LATER"
                    }
                }
                if((object as! getAppVersionResponse).dataObj?.first?.versionName != version || (object as! getAppVersionResponse).dataObj?.first?.versionCode != build ) {
                    AlertController.showAlertFor("Update App", message: "Please update your app", okButtonTitle: "UPDATE", okAction: {
                        UIApplication.shared.openURL(NSURL(string: "itms://itunes.apple.com/us/app/rideally/id1143210362?mt=8")! as URL)
                    }, cancelButtonTitle: cancelBtn, cancelAction: nil)
                }
            }
        }){ (error) in
            AlertController.showAlertForError(error)
        }
    }
}
