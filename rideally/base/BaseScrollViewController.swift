//
//  BaseScrollViewController.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class SubView {
    var view: UIView?
    var heightConstraint: NSLayoutConstraint?
    ///bottom padding ignored if it is not the last view
    var padding = UIEdgeInsets.zero
    var showShadow = true
}

class BaseScrollViewController: ViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    var scrollView = UIScrollView()
    var contentView = UIView()
    var viewHasbeenLaid = false
    
    var bottomSubView: SubView?
    var topSubView: SubView?
    var actionBtn = UIButton(type: .system)
    var nRide : NSMutableArray? = NSMutableArray()
    var oRide : NSMutableArray? = NSMutableArray()
    
    func viewHasActionButtonBar() -> Bool
    {
        return false
    }
    
    ///array of `SubView` objects
    var viewsArray = [SubView]()
    
    
    func keyboardWillShow(_ notification:Notification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        
        self.scrollView.contentInset = contentInset
        keyBoardDidMoveUp(keyboardFrame.minY)
    }
    
    func keyboardWillHide(_ notification:Notification){
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = 0
        self.scrollView.contentInset = contentInset
    }
    
    func keyBoardDidMoveUp (_ keyboardMinY: CGFloat) {
        
    }
    
    func showMenu()
    {
        if let drawerVC = self.evo_drawerController {
            drawerVC.toggleLeftDrawerSide(animated: true, completion: nil)
            //drawerVC.toggleLeftDrawerSideAnimated(true, completion: nil)
        }
    }
    
    override func loadView() {
        super.loadView()
        
        view.backgroundColor = Colors.APP_BG
        scrollView.backgroundColor = Colors.APP_BG
        contentView.backgroundColor = Colors.APP_BG
        
        NotificationCenter.default.addObserver(self, selector: #selector(BaseScrollViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BaseScrollViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showShare), name: NSNotification.Name(rawValue: "shareButton"), object: nil)
    }
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func transparentNavBar() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        var image = UIImage(named: "Back-2")
        
        image = image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(goBack))
        
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = Colors.GREEN_COLOR
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(hasTabBarItems()){
            addTabbarRightOptions()
            getUserTotalDetail()
        }
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        
        // Do any additional setup after loading the view.
        scrollView.showsVerticalScrollIndicator = false
        scrollView.bounces = false
        scrollView.delegate = self
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(scrollView)
        scrollView.addSubview(contentView)
    }
    
    func hasTabBarItems() -> Bool
    {
        return false
    }
    
    func getUserTotalDetail() -> Void {
        let reqObj = UserDetailRequest()
        reqObj.user_id = UserInfo.sharedInstance.userID
        UserDetailRequestor().getUserTotalDetail(reqObj, success:{ (success, object) in
            if (object as! UserDetailResponse).code == "1331"
            {
                self.userDetailData = ((object as! UserDetailResponse).dataObj)!
                if(self.userDetailData.first?.allRides != nil && self.userDetailData.first?.allRides != "") {
                    UserInfo.sharedInstance.allRides = (self.userDetailData.first?.allRides)!
                }
                if(self.userDetailData.first?.allWorkplaces != nil && self.userDetailData.first?.allWorkplaces != "") {
                    UserInfo.sharedInstance.allWorkplaces = (self.userDetailData.first?.allWorkplaces)!
                }
                if(self.userDetailData.first?.myWorkplace != nil && self.userDetailData.first?.myWorkplace != "") {
                    UserInfo.sharedInstance.myWorkplaces = (self.userDetailData.first?.myWorkplace)!
                }
                if(self.userDetailData.first?.myPendingWorkplace != nil && self.userDetailData.first?.myPendingWorkplace != "") {
                    UserInfo.sharedInstance.myPendingWorkplaces = (self.userDetailData.first?.myPendingWorkplace)!
                }
                if(self.userDetailData.first?.offered != nil && self.userDetailData.first?.offered != "") {
                    UserInfo.sharedInstance.offeredRides = (self.userDetailData.first?.offered)!
                }
                if(self.userDetailData.first?.needed != nil && self.userDetailData.first?.needed != "") {
                    UserInfo.sharedInstance.neededRides = (self.userDetailData.first?.needed)!
                }
                if(self.userDetailData.first?.joined != nil && self.userDetailData.first?.joined != "") {
                    UserInfo.sharedInstance.joinedRides = (self.userDetailData.first?.joined)!
                }
                if(self.userDetailData.first?.booked != nil && self.userDetailData.first?.booked != "") {
                    UserInfo.sharedInstance.bookedRides = (self.userDetailData.first?.booked)!
                }
                if(self.userDetailData.first?.unreadNotification == "0") {
                    self.notificationCount.isHidden = true
                } else {
                    self.notificationCount.isHidden = false
                }
                if(self.userDetailData.first?.unreadNotification?.characters.count <= 2){
                    self.notificationCount.text = self.userDetailData.first?.unreadNotification
                    self.notificationCount.frame = CGRect(x: 12, y: -5, width: 15, height: 15)
                } else {
                    self.notificationCount.text = "99+"
                    self.notificationCount.frame = CGRect(x: 12, y: -5, width: 18, height: 18)
                }
                self.notificationCount.font = normalFontWithSize(10)
                self.notificationCount.textColor = Colors.WHITE_COLOR
                self.notificationCount.backgroundColor = Colors.RED_COLOR
                self.notificationCount.textAlignment = .center;
                self.notificationCount.layer.masksToBounds = true
                self.notificationCount.layer.cornerRadius = self.notificationCount.bounds.size.width/2
                self.notificationCount.layer.rasterizationScale = UIScreen.main.scale;
                self.notificationCount.layer.shouldRasterize = true;
                self.notificationCount.clipsToBounds = true;
                self.notificationCount.numberOfLines = 0
                self.notificationCount.lineBreakMode = NSLineBreakMode.byWordWrapping
                self.notificationCount.layoutIfNeeded()
                
            }
            else{
            }
        }){ (error) in
        }
    }
    
    var userDetailData = [UserDetailData]()
    let notificationCount = UILabel()
    
    func addTabbarRightOptions()
    {
        self.notificationCount.isHidden = true
        
        let view1 = UIView(frame: CGRect(x: 0,y: 0,width: 90,height: 30))
        let btnNotif = UIButton()
         if(ISWPRIDESSCREEN) {
            let membersCount = UILabel()
            if(WPDATA?.wpMembers?.characters.count <= 2){
                membersCount.text = WPDATA?.wpMembers
                membersCount.frame = CGRect(x: 12, y: -5, width: 15, height: 15)
            } else {
                membersCount.text = "99+"
                membersCount.frame = CGRect(x: 12, y: -5, width: 18, height: 18)
            }
            membersCount.font = normalFontWithSize(10)
            membersCount.textColor = Colors.WHITE_COLOR
            membersCount.backgroundColor = Colors.RED_COLOR
            membersCount.textAlignment = .center;
            membersCount.layer.masksToBounds = true
            membersCount.layer.cornerRadius = membersCount.bounds.size.width/2
            membersCount.layer.rasterizationScale = UIScreen.main.scale;
            membersCount.layer.shouldRasterize = true;
            membersCount.clipsToBounds = true;
            membersCount.numberOfLines = 0
            membersCount.lineBreakMode = NSLineBreakMode.byWordWrapping
            membersCount.layoutIfNeeded()
            
            let btnMembers = UIButton()
            btnMembers.setImage(UIImage(named: "t_members"), for: UIControlState())
            btnMembers.frame = CGRect(x: 0, y: 5, width: 30, height: 30)
            btnMembers.addTarget(self, action: #selector(showWpMembers), for: .touchDown)
            view1.addSubview(btnMembers)
            
            btnMembers.addSubview(membersCount)
            
            let btnSearch = UIButton()
            btnSearch.frame = CGRect(x: 30, y: 0, width: 30, height: 30)
            btnSearch.setImage(UIImage(named: "t_filter_white"), for: UIControlState())
            btnSearch.addTarget(self, action: #selector(showWpRidesSearch), for: .touchDown)
            view1.addSubview(btnSearch)
            
            if(ISWPMAPSCREEN) {
                let btnMapView = UIButton()
                btnMapView.setImage(UIImage(named: "t_listview"), for: UIControlState())
                btnMapView.frame = CGRect(x: 60, y: 0, width: 30, height: 30)
                btnMapView.addTarget(self, action: #selector(showWpListView), for: .touchDown)
                view1.addSubview(btnMapView)
            } else {
                let btnMapView = UIButton()
                btnMapView.setImage(UIImage(named: "t_mapview"), for: UIControlState())
                btnMapView.frame = CGRect(x: 60, y: 0, width: 30, height: 30)
                btnMapView.addTarget(self, action: #selector(showWpMapView), for: .touchDown)
                view1.addSubview(btnMapView)
            }
        } else {
            btnNotif.setImage(UIImage(named: "t_notif"), for: UIControlState())
            btnNotif.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            btnNotif.addTarget(self, action: #selector(showNotifications), for: .touchDown)
            view1.addSubview(btnNotif)
            
            let btnProfile = UIButton()
            btnProfile.frame = CGRect(x: 30, y: 0, width: 30, height: 30)
            btnProfile.setImage(UIImage(named: "t_profile"), for: UIControlState())
            btnProfile.addTarget(self, action: #selector(showProfile), for: .touchDown)
            view1.addSubview(btnProfile)
            
            let btnShare = UIButton()
            btnShare.setImage(UIImage(named: "t_share"), for: UIControlState())
            btnShare.frame = CGRect(x: 60, y: 0, width: 30, height: 30)
            btnShare.addTarget(self, action: #selector(showShare), for: .touchDown)
            view1.addSubview(btnShare)
        }
        let rightBarItem = UIBarButtonItem(customView: view1)
        self.navigationItem.rightBarButtonItem = rightBarItem
        
        btnNotif.addSubview(notificationCount)
    }
    
    func showProfile()
    {
        let vc = ProfileViewController()
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    func showNotifications()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = NotificationViewController()
            vc.onCloseBlock = {
                self.getUserTotalDetail()
            }
            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
        }
    }
    func showShare()
    {
        let refCode = "RAF"+UserInfo.sharedInstance.userID
        let webUrl = SERVER+"?ref_code="+refCode+"#login"
        let textToShare = "RideAlly has helped me to get Carpool partner. You may SignUp using my code "+refCode+" and get 200 free ride points on iOS/Android App using https://www.rideally.com/app or SignUp on web using "+webUrl+"."
        
        let objectsToShare = [textToShare]
        
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        let excludeActivities = [UIActivityType.airDrop,
                                 UIActivityType.print,
                                 UIActivityType.assignToContact,
                                 UIActivityType.saveToCameraRoll,
                                 UIActivityType.addToReadingList,
                                 UIActivityType.postToFlickr,
                                 UIActivityType.postToVimeo]
        
        activityVC.excludedActivityTypes = excludeActivities
        
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func showWpMembers () {
        showIndicator("Fetching members.")
        WorkplaceRequestor().getWPMembers("", groupID: (WPDATA?.wpID)!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = WPMembersViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.wpDetails = (object as! WPMembersResponseData).groupDetails
            vc.membersList = (object as! WPMembersResponseData).members
            self.navigationController?.pushViewController(vc, animated: true)
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func showWpListView () {
        ISWPMAPSCREEN = false
        let vc = WPRidesViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.data = WPDATA
        vc.dataSourceToOfc = DATASOURCETOOFC
        vc.dataSourceToHome = DATASOURCETOHOME
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showWpMapView () {
        ISWPMAPSCREEN = true
        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
        let vc : RideMapViewController = storyboard.instantiateViewController(withIdentifier: "RideMapViewController") as! RideMapViewController
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.data = WPDATA
        vc.wpMembers = WPMEMBERS
        vc.dataSourceToOfc = DATASOURCETOOFC
        vc.dataSourceToHome = DATASOURCETOHOME
        vc.isFrom = ISFROM!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showWpRidesSearch () {
        let vc = SearchRideViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.wpId = (WPDATA?.wpID)!
        vc.searchData = SEARCHREQUEST
        vc.searchResult = ISSEARCHRESULT
        vc.completionBlock = { (rides, reqObj) -> Void in
            if(ISWPMAPSCREEN) {
                var rowsCnt = 0
                if let list = (rides as! WPRidesPools).officeGoing{
                    rowsCnt = list.count
                }
                self.nRide?.removeAllObjects()
                self.oRide?.removeAllObjects()
                var rideDataObject: Ride!
                for index in 0..<rowsCnt{
                    rideDataObject  = (rides as! WPRidesPools).officeGoing![index]
                    if rideDataObject.poolRideType == "N" {
                        self.nRide! .add(rideDataObject)
                    }else{
                        self.oRide! .add(rideDataObject)
                    }
                }
                if(self.oRide?.count == 0) {
                    AlertController.showToastForInfo("No ride found.")
                } else {
                    AlertController.showToastForInfo("\((self.oRide?.count)!) \("ride(s) found.")")
                }
            }
            ISSEARCHRESULT = true
            SEARCHREQUEST = (reqObj as? SearchRideRequest)!
            SEARCHSOURCETOOFC = (rides as! WPRidesPools).officeGoing
            SEARCHSOURCETOHOME =  (rides as! WPRidesPools).homeGoing
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func actionBtnClicked()
    {
        print("actionBtnClicked")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !viewHasbeenLaid {
            viewHasbeenLaid = true
            setup()
            
            if viewHasActionButtonBar() {
                addBottomView(bottomButtonSubView())
            }
        }
    }
    
    func setup () {
        
        //view constraints
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[scrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["scrollView" : scrollView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[scrollView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["scrollView" : scrollView]))
        
        //scroll constraints
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[contentView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["contentView" : contentView]))
        scrollView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[contentView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["contentView" : contentView]))
        let contentWidthConstraint = NSLayoutConstraint(item: contentView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: self.scrollView, attribute: NSLayoutAttribute.width, multiplier: 1.0, constant: 0)
        scrollView.addConstraint(contentWidthConstraint)
        
        let array = viewsArray as NSArray
        
        array.enumerateObjects({ [weak self] (object, index, stop) -> Void in
            let subView = object as! SubView
            
            let currentView = subView.view!
            currentView.translatesAutoresizingMaskIntoConstraints = false
            
            let padding = subView.padding
            
            if subView.showShadow {
                currentView.addShadow(Colors.GENERAL_BORDER_COLOR, shadowRadius: 2.0)
            }
            
            //currentView.addBottomViewBorder(1.0, color: Colors.HJ_GENERAL_BORDER_COLOR, offset: CGPointZero)
            
            self?.contentView.addSubview(currentView)
            
            if let heightConstraint = subView.heightConstraint {
                self?.contentView.addConstraint(heightConstraint)
            }
            
            var previousSubView: SubView?
            if index > 0 {
                previousSubView = array[index-1] as? SubView
                let prevView = previousSubView?.view
                prevView?.translatesAutoresizingMaskIntoConstraints = false
                let dict = ["currView": currentView, "prevView": prevView!]
                self?.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[prevView]-\(padding.top)-[currView]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            } else {
                let dict = ["currView": currentView]
                self?.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(padding.top)-[currView]", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            }
            
            self?.contentView.addConstraint(NSLayoutConstraint(item: currentView, attribute: .left, relatedBy: .equal, toItem: self?.contentView, attribute: .left, multiplier: 1.0, constant: padding.left))
            self?.contentView.addConstraint(NSLayoutConstraint(item: currentView, attribute: .right, relatedBy: .equal, toItem: self?.contentView, attribute: .right, multiplier: 1.0, constant: -padding.right))
            
            if index == (self?.viewsArray.count)!-1 {
                let dict = ["currView": currentView]
                self?.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[currView]-\(padding.bottom)-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            }
        })
    }
    
    func relayoutSubview () {
        let array = self.contentView.subviews
        for view in array {
            view.removeFromSuperview()
        }
        
        let constraints = self.contentView.constraints
        self.contentView.removeConstraints(constraints)
        
        setup()
        
        if viewHasActionButtonBar() && actionBtn.superview == nil {
            addBottomView(bottomButtonSubView())
        } else {
            updateLayout()
        }
    }
    
    func updateLayout () {
        self.view.layoutIfNeeded()
    }
    
    func updateLayoutWithAnimation(_ animated: Bool) {
        if animated == true {
            UIView.animate(withDuration: 0.3, animations: { [weak self] () -> Void in
                self?.updateLayout()
                })
        } else {
            self.updateLayout()
        }
    }
    
    //MARK:- Adding top view
    func addTopView (_ topView: SubView?) {
        
        if let lastSubView = self.viewsArray.first, let currentSubView = topView {
            currentSubView.view?.isHidden = false
            var padding = lastSubView.padding
            padding.top += (currentSubView.padding.bottom + currentSubView.padding.top + (currentSubView.heightConstraint?.constant)!)
            lastSubView.padding = padding
            
            currentSubView.view?.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(currentSubView.view!)
            
            self.view.addConstraint(NSLayoutConstraint(item: currentSubView.view!, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: currentSubView.padding.left))
            self.view.addConstraint(NSLayoutConstraint(item: currentSubView.view!, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: -currentSubView.padding.right))
            self.view.addConstraint(NSLayoutConstraint(item: currentSubView.view!, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: -currentSubView.padding.top))
            self.view.addConstraint(currentSubView.heightConstraint!)
            
            topSubView = currentSubView
            
            relayoutSubview()
        }
    }
    
    func removeTopView () {
        if let lastSubView = self.viewsArray.first {
            if(topSubView != nil) {
                topSubView?.view?.isHidden = true
                
                var padding = lastSubView.padding
                padding.top = 0
                lastSubView.padding = padding
            }
        }
    }
    
    //MARK:- Adding bottom view
    func addBottomView (_ bottomView: SubView?) {
        
        if let lastSubView = self.viewsArray.last, let currentSubView = bottomView {
            var padding = lastSubView.padding
            padding.bottom += (currentSubView.padding.bottom + currentSubView.padding.top + (currentSubView.heightConstraint?.constant)!)
            lastSubView.padding = padding
            
            currentSubView.view?.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(currentSubView.view!)
            
            self.view.addConstraint(NSLayoutConstraint(item: currentSubView.view!, attribute: .left, relatedBy: .equal, toItem: self.view, attribute: .left, multiplier: 1.0, constant: currentSubView.padding.left))
            self.view.addConstraint(NSLayoutConstraint(item: currentSubView.view!, attribute: .right, relatedBy: .equal, toItem: self.view, attribute: .right, multiplier: 1.0, constant: -currentSubView.padding.right))
            self.view.addConstraint(NSLayoutConstraint(item: currentSubView.view!, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: -currentSubView.padding.bottom))
            self.view.addConstraint(currentSubView.heightConstraint!)
            
            bottomSubView = currentSubView
            
            relayoutSubview()
        }
    }
    
    func removeBottomView () {
        if let lastSubView = self.viewsArray.last {
            bottomSubView?.view?.isHidden = true
            
            var padding = lastSubView.padding
            padding.bottom -= (bottomSubView!.padding.bottom + bottomSubView!.padding.top + (bottomSubView!.heightConstraint?.constant)!)
            lastSubView.padding = padding
        }
    }
    
    func bottomButtonSubView () -> SubView {
        actionBtn.backgroundColor = Colors.BTN_BG_COLOR
        actionBtn.translatesAutoresizingMaskIntoConstraints = false
        actionBtn.addTarget(self, action: #selector(BaseScrollViewController.actionBtnClicked), for: .touchUpInside)
        actionBtn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        actionBtn.titleLabel?.font = boldFontWithSize(15)
        
        let sub = SubView()
        sub.view = actionBtn
        sub.padding = UIEdgeInsetsMake(10, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: sub.view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 50)
        
        return sub
    }
    
    func removeSubView (_ subView: SubView, completion: onSuccessBlock?) {
        let count = viewsArray.count
        self.viewsArray = self.viewsArray.filter({return $0 !== subView})
        if count != viewsArray.count {
            completion?(true as AnyObject?)
            self.relayoutSubview()
        } else {
            completion?(false as AnyObject?)
        }
    }
    
}
