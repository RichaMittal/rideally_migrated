//
//  BaseRequestor.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


let CUSTOM_ERROR_DOMAIN = "CUSTOM_ERROR_DOMAIN"
let CUSTOM_ERROR_CODE = -111
let CUSTOM_ERROR_INFO_KEY = "custom_description"

class BaseRequestor: NSObject {
    override init() {
        //
    }
    
    func errorForCustomResponse(_ message: String?) -> NSError? {
        var text: String?
        if message != nil && message != "" {
            text = message
        } else {
            text = ERROR_MESSAGE_STRING
        }
        let error = NSError(domain: CUSTOM_ERROR_DOMAIN, code: CUSTOM_ERROR_CODE, userInfo: [CUSTOM_ERROR_INFO_KEY : text!])
        return error
    }
    
    func defaultHeaders () -> [String: String] {
        return ["Accept" : "text/html"]
    }
    
    // Generic request
    func makeRequestWithparameters (_ method: HTTPMethod, urlString: String, parameters: [String : AnyObject]?, encoding: Alamofire.URLEncoding, success: NetworkSuccessHandler?, failure: NetworkFailureHandler?) {
        print("\nRequestParameters:>> \(parameters)")
        print("\nRequestURL:>> \(urlString)")
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            Alamofire.request(urlString, method: method, parameters: parameters, encoding: encoding, headers: defaultHeaders()).validate().responseJSON { (response) -> Void in
                print("\nRequestResponse:>> \(response)")
                let err = response.result.error
                if (err != nil) {
                    let response = (error: err!)
                    print(response)
                    let error = self.errorForCustomResponse(response.localizedDescription)
                    failure?(error)
                } else {
                    // check for access token. key name `accessToken`. Token related shit
                    let responseObject = response.result.value as? [String: AnyObject]
                    if let accessToken = responseObject?["accessToken"] {
                        print("access token >>>>> \(accessToken)")
                    }
                    
                    if let validToken = responseObject?["isValidToken"] as? Bool {
                        if validToken == false {
                            hideIndicator()
                            
                            var message = responseObject?["messageCodes"]?["mcDescription"] as? String
                            message = message == nil ? "Your token has expired." : message
                            
                            var title = responseObject?["messageCodes"]?["mcTitle"] as? String
                            title = title == nil ? "Authentication failed!" : title
                            
                            AlertController.showAlertFor(title, message: message, okButtonTitle: "Ok", okAction: {
                                logoutUserAndGoToHomeScreen()
                            })
                        } else {
                            if (responseObject != nil){
                                success?(responseObject!)
                            }else{
                                failure?(self.errorForCustomResponse("Something went wrong. Please try again later."))
                            }
                        }
                    } else {
                        if (responseObject != nil){
                            success?(responseObject!)
                        }else{
                            failure?(self.errorForCustomResponse("Something went wrong. Please try again later."))
                        }
                    }
                }
            }
        }
    }
    
    // make GET Request
    func makeGETRequestWithparameters (_ urlString: String, parameters: [String : AnyObject]?, encoding: Alamofire.URLEncoding, success: NetworkSuccessHandler?, failure: NetworkFailureHandler?) {
        
        self.makeRequestWithparameters(.get, urlString: urlString, parameters: parameters, encoding: encoding, success: success, failure: failure)
    }
    
    // make POST Request
    func makePOSTRequestWithparameters (_ urlString: String, parameters: [String : AnyObject]?, encoding: Alamofire.URLEncoding, success: NetworkSuccessHandler?, failure: NetworkFailureHandler?) {
        self.makeRequestWithparameters(.post, urlString: urlString, parameters: parameters, encoding: encoding, success: success, failure: failure)
    }
    
    // make PUT Request
    func makePUTRequestWithparameters (_ urlString: String, parameters: [String : AnyObject]?, encoding: Alamofire.URLEncoding, success: NetworkSuccessHandler?, failure: NetworkFailureHandler?) {
        self.makeRequestWithparameters(.put, urlString: urlString, parameters: parameters, encoding: encoding, success: success, failure: failure)
    }
    
    // make PUT Request
    func makeDELETERequestWithparameters (_ urlString: String, parameters: [String : AnyObject]?, encoding: Alamofire.URLEncoding, success: NetworkSuccessHandler?, failure: NetworkFailureHandler?) {
        self.makeRequestWithparameters(.delete, urlString: urlString, parameters: parameters, encoding: encoding, success: success, failure: failure)
    }
}
