//
//  ShareTaxiViewController.swift
//  rideally
//
//  Created by Sarav on 07/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class ShareTaxiViewController: NeedRideViewController {
    
    let reqObj = ShareTaxiRequest()
    
    lazy var subscriptionView: SubView = self.getSubscriptionView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Share a Taxi"
         self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func addViews() {
        
        viewsArray.append(getLocationView())
        viewsArray.append(getTravelWithView())
        viewsArray.append(getTimeView())
        
        viewsArray.append(subscriptionView)
        viewsArray.append(getCommentView())
        
        addBottomView(getBottomView())
    }
    
    override func mapForFrom()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.reqObj.fromLocation = self.lblSource.text
                    self.reqObj.fromLat = loc["lat"]
                    self.reqObj.fromLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func mapForTo()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.reqObj.toLocation = self.lblDest.text
                    self.reqObj.toLat = loc["lat"]
                    self.reqObj.toLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    override func getTimeView() -> SubView
    {
        let view = UIView()
        
        let lblDate = UILabel()
        lblDate.text = "START TIME"
        lblDate.textColor = Colors.GRAY_COLOR
        lblDate.font = boldFontWithSize(11)
        lblDate.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDate)
        
        txtDate.layer.borderWidth = 0.5
        txtDate.delegate = self
        txtDate.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtDate.translatesAutoresizingMaskIntoConstraints = false
        txtDate.font = boldFontWithSize(14)
        
        datePicker.datePickerMode = .time
        //datePicker.locale = NSLocale(localeIdentifier: "NL")
        txtDate.inputView = datePicker
        //datePicker.minimumDate = NSDate()
        datePicker.addTarget(self, action: #selector(datePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtDate.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "time")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        txtDate.rightView = imgV
        txtDate.rightViewMode = .always
        view.addSubview(txtDate)
        txtDate.placeholder = "Select Time"
        
        let lblTime = UILabel()
        lblTime.text = "RETURN TIME"
        lblTime.textColor = Colors.GRAY_COLOR
        lblTime.font = boldFontWithSize(11)
        lblTime.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTime)
        
        txtTime.layer.borderWidth = 0.5
        txtTime.delegate = self
        txtTime.font = boldFontWithSize(14)
        txtTime.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtTime.translatesAutoresizingMaskIntoConstraints = false
        txtTime.placeholder = "Select Time"
        
        timePicker.datePickerMode = .time
        //timePicker.locale = NSLocale(localeIdentifier: "NL")
        txtTime.inputView = timePicker
        //timePicker.minimumDate = NSDate()
        timePicker.addTarget(self, action: #selector(timePicked), for: .valueChanged)
        
        txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV1 = UIImageView(image: UIImage(named: "time")!)
        imgV1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV1.contentMode = .center
        
        txtTime.rightView = imgV1
        txtTime.rightViewMode = .always
        view.addSubview(txtTime)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbldate":lblDate, "txtdate":txtDate, "line":lblLine1, "lbltime":lblTime, "txttime":txtTime]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbldate(20)]-5-[txtdate(30)]-10-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbltime(20)]-5-[txttime(30)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbldate]-10-[lbltime(==lbldate)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txtdate]-10-[txttime(==txtdate)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
        
    }
    override func donePressed()
    {
        if(selectedTxtField == txtDate){
            reqObj.goingTime = prettyDateStringFromDate(datePicker.date, toFormat: "hh:mm a")
            selectedTxtField?.text = reqObj.goingTime
        }
        else if(selectedTxtField == txtTime){
            reqObj.returnTime = prettyDateStringFromDate(timePicker.date, toFormat: "hh:mm a")
            selectedTxtField?.text = reqObj.returnTime
        }
        selectedTxtField?.resignFirstResponder()
    }
    
    func getSubscriptionView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "PREFERRED SUBSCRIPTION"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        let lblSubscription = UILabel()
        //lblSubscription.layer.borderWidth = 0.5
        //lblSubscription.layer.borderColor = Colors.GENERAL_BORDER_COLOR.CGColor
        lblSubscription.translatesAutoresizingMaskIntoConstraints = false
        lblSubscription.font = boldFontWithSize(14)
        lblSubscription.text = "Select subscription here"
        lblSubscription.textColor = Colors.GRAY_COLOR
        lblSubscription.isUserInteractionEnabled = true
        view.addSubview(lblSubscription)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(gotoSubscriptionList))
        lblSubscription.addGestureRecognizer(tapGesture)
        
        let arrowImg = UIImageView(image: UIImage(named: "rightArrow")!)
        arrowImg.translatesAutoresizingMaskIntoConstraints = false
        lblSubscription.addSubview(arrowImg)
        
        lblSubscription.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[arrow(15)]-5-|", options: [], metrics: nil, views: ["arrow":arrowImg]))
        lblSubscription.addConstraint(NSLayoutConstraint(item: arrowImg, attribute: .centerY, relatedBy: .equal, toItem: lblSubscription, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        
        let view1 = UIView()
        view1.tag = 401
        view1.translatesAutoresizingMaskIntoConstraints = false
        view1.isHidden = true
        
        let lblVehicleTypeTitle = UILabel()
        setLblProp(lblVehicleTypeTitle, isTitle: true, parentView: view1)
        lblVehicleTypeTitle.text = "VEHICLE TYPE"
        
        let lblVehicleType = UILabel()
        setLblProp(lblVehicleType, isTitle: false, parentView: view1)
        lblVehicleType.text = "-"
        lblVehicleType.tag = 501
        
        let lblMemberTitle = UILabel()
        setLblProp(lblMemberTitle, isTitle: true, parentView: view1)
        lblMemberTitle.text = "MEMBER"
        
        let lblMember = UILabel()
        setLblProp(lblMember, isTitle: false, parentView: view1)
        lblMember.text = "-"
        lblMember.tag = 502
        
        let lblPriceTitle = UILabel()
        setLblProp(lblPriceTitle, isTitle: true, parentView: view1)
        lblPriceTitle.text = "PRICE"
        
        let lblPrice = UILabel()
        setLblProp(lblPrice, isTitle: false, parentView: view1)
        lblPrice.text = "-"
        lblPrice.tag = 503
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view1.addSubview(lblLine2)
        
        view.addSubview(view1)
        
        
        let viewsDict = ["type":lblVehicleTypeTitle, "typeval":lblVehicleType, "mem":lblMemberTitle, "memval":lblMember, "price":lblPriceTitle, "priceval":lblPrice, "line1":lblLine2]
        
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[type][mem(==type)][price(==type)]-5-|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[typeval][memval(==typeval)][priceval(==typeval)]-5-|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[type]-5-[typeval(==type)]-5-[line1(0.5)]|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mem]-5-[memval(==mem)]-5.5-|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[price]-5-[priceval(==price)]-5.5-|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: ["line1":lblLine2]))
        
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)][lblsub(20)]-5-[line(0.5)]-5-[subscriptionview(40)]", options: [], metrics: nil, views: ["lbl":lblTitle, "lblsub":lblSubscription, "line":lblLine1, "subscriptionview":view1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblsub(200)]", options: [], metrics: nil, views: ["lblsub":lblSubscription]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[subscriptionview]|", options: [], metrics: nil, views: ["subscriptionview":view1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 51)
        return sub
    }
    
    func setLblProp(_ lbl: UILabel, isTitle: Bool, parentView: UIView)
    {
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = boldFontWithSize(12)
        lbl.textColor = Colors.GRAY_COLOR
        if(isTitle){
            lbl.font = normalFontWithSize(8)
            lbl.textColor = UIColor.lightGray
        }
        
        parentView.addSubview(lbl)
    }
    
    override func getBottomView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        let myAttribute = [NSFontAttributeName: boldFontWithSize(8)!]
        let myString = NSMutableAttributedString(string: "By clicking on 'Submit request', you agree to our Share taxi - terms and conditions", attributes: myAttribute)
        let myRange = NSRange(location: 50, length: 33)
        myString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange)
        lblTitle.attributedText = myString
        let tapGesture = UITapGestureRecognizer(target:self, action:#selector(labelGesturePressed(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        lblTitle.addGestureRecognizer(tapGesture)
        //lblTitle.text = "By clicking on 'Submit request', you agree to our Share taxi - terms and conditions"
        //lblTitle.font = boldFontWithSize(8)
        lblTitle.textAlignment = .center
        //lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.isUserInteractionEnabled = true
        view.addSubview(lblTitle)
        
        let btn = UIButton()
        btn.setTitle("SUBMIT", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(proceed), for: .touchDown)
        view.addSubview(btn)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(20)][btn(40)]", options: [], metrics: nil, views: ["lbl":lblTitle,"btn":btn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btn]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    override func proceed()
    {
        reqObj.userId = UserInfo.sharedInstance.userID
        reqObj.requestType = btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
        
        if((reqObj.fromLocation != nil) && (reqObj.toLocation != nil))
        {
            if(reqObj.fromLocation != reqObj.toLocation){
                if((reqObj.goingTime != nil) && (reqObj.returnTime != nil))
                {
                    if(reqObj.goingTime != reqObj.returnTime){
                        if(reqObj.planType != nil){
                            showIndicator("Submitting Data..")
                            ShareTaxiRequestor().createShareTaxi(reqObj, success: { (success, object) in
                                hideIndicator()
                                if(success){
                                    AlertController.showAlertFor("Success", message: (object as! String), okButtonTitle: "Ok", okAction: {
                                        self.dismiss(animated: true, completion: nil)
                                    })
                                }
                                }, failure: { (error) in
                                    hideIndicator()
                            })
                        }
                        else{
                            AlertController.showAlertFor("ShareTaxi", message: "Please choose your preferred subscription.")
                        }
                    }
                    else{
                        AlertController.showAlertFor("ShareTaxi", message: "Please choose different time for 'Start' and 'Return'.")
                    }
                }
                else{
                    AlertController.showAlertFor("ShareTaxi", message: "Please choose time for 'Start' and 'Return'.")
                }
            }
            else{
                AlertController.showAlertFor("ShareTaxi", message: "Please choose different locations for source and destination.")
            }
        }
        else{
            AlertController.showAlertFor("ShareTaxi", message: "Please choose location in 'From' and 'To' field from Google Places.")
        }
    }
    
    func labelGesturePressed(_ sender:AnyObject)  {
        let vc = StaticPagesViewController()
        vc.staticPage = "sharedtaxi-T&C"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoSubscriptionList()
    {
        let vc = TaxiSubscriptionViewController()
        vc.onCompletionBlock = { (selectionSubscription) -> Void in
            if let obj = selectionSubscription as? SubscriptionItem{
                
                self.subscriptionView.view?.viewWithTag(401)?.isHidden = false
                self.subscriptionView.heightConstraint?.constant = 101
                
                (self.subscriptionView.view?.viewWithTag(501) as? UILabel)?.text = obj.vehicleType
                (self.subscriptionView.view?.viewWithTag(502) as? UILabel)?.text = obj.members
                (self.subscriptionView.view?.viewWithTag(503) as? UILabel)?.text = obj.cost
                
                self.reqObj.planType = obj.planId
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
