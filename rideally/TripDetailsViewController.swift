//
//  TripDetailsViewController.swift
//  rideally
//
//  Created by Sarav on 14/11/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class TripDetailCell: UITableViewCell, RateViewDelegate {
    
    let profilePic = UIImageView()
    let lblUserName = UILabel()
    let rateView = RateView()
    var rateValue : Int = 0
    var onUserActionBlock: actionBlockWithParam?
    
    var data: Member!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func rateView(_ rateView:RateView!, ratingDidChange rating: CFloat) {
        rateValue = Int(rating)
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        //self.clipsToBounds = true
        
        profilePic.layer.cornerRadius = 15
        profilePic.layer.masksToBounds = true
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(profilePic)
        
        rateView.notSelectedImage=UIImage(named: "Star_Empty")
        rateView.fullSelectedImage = UIImage(named: "Star_Filled")
        rateView.delegate = self
        rateView.rating = 0
        rateView.editable = true
        rateView.maxRating = 5
        rateView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(rateView)
        
        lblUserName.font = boldFontWithSize(15)
        lblUserName.textColor = Colors.GRAY_COLOR
        lblUserName.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblUserName.translatesAutoresizingMaskIntoConstraints = false
        //couponDescription.numberOfLines = 2
        self.contentView.addSubview(lblUserName)
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(30)]-5-[name]|", options: [], metrics: nil, views: ["pic":profilePic, "name":lblUserName]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[pic(30)]-10-[rating(150)]-10-|", options: [], metrics: nil, views: ["pic":profilePic, "rating":rateView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[name]-5-|", options: [], metrics: nil, views: ["name":lblUserName]))
        //self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[lblCost]-10-[stepperCost(150)]-10-|", options: [], metrics: nil, views: ["lblCost":lblCostTitle, "stepperCost":stepperCost]))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: profilePic, attribute: .centerY, relatedBy: .equal, toItem: rateView, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: rateView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 50))
        //self.contentView.addConstraint(NSLayoutConstraint(item: lblUserName, attribute: .Left, relatedBy: .Equal, toItem: profilePic, attribute: .Left, multiplier: 1, constant: 0))
        
    }
    
    func buildCell()
    {
        if(data.profilePicStatus == "1"){
            if let url = data.profilePic{
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
            }
            else{
                if let fbID = data.memberFBID{
                    profilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            profilePic.image = UIImage(named: "placeholder")
        }
        if let userName = data.name {
            lblUserName.text = userName
        }
        setNeedsLayout()
    }
}

class TripDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var startLocation: String?
    var stopLocation: String?
    var points: String?
    var distance: String?
    var poolId: String?
    var poolUsers = [Member]()
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Trip Detail"
        self.view.backgroundColor = Colors.APP_BG
        
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        //getPoolUsers()
        
        let lblPoints = UILabel()
        lblPoints.text = "\(points!) Points Earned"
        lblPoints.textColor = Colors.WHITE_COLOR
        lblPoints.textAlignment = .center
        lblPoints.backgroundColor = UIColor(red: 128.0/255.0, green: 0.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        lblPoints.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(lblPoints)
        
        let lblDistance = UILabel()
        lblDistance.textColor = Colors.WHITE_COLOR
        lblDistance.text = "\(distance!)"
        lblDistance.textAlignment = .center
        lblDistance.backgroundColor = UIColor(red: 215.0/255.0, green: 69.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        lblDistance.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(lblDistance)
        
        let detailView = UIView()
        detailView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(detailView)
        
        let lblTitle = UILabel()
        lblTitle.text = "Trip Complete"
        lblTitle.textAlignment = .center
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        detailView.addSubview(lblTitle)
        
        let lblSource = UILabel()
        lblSource.text = startLocation
        lblSource.textAlignment = .center
        lblSource.numberOfLines = 0
        lblSource.lineBreakMode = .byWordWrapping
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        detailView.addSubview(lblSource)
        
        let lblDest = UILabel()
        lblDest.text = stopLocation
        lblDest.textAlignment = .center
        lblDest.numberOfLines = 0
        lblDest.lineBreakMode = .byWordWrapping
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        detailView.addSubview(lblDest)
        
        let viewsDict1 = ["title":lblTitle, "source":lblSource, "dest":lblDest]
        detailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: viewsDict1))
        detailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title(30)]", options: [], metrics: nil, views: viewsDict1))
        detailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[source]-50-[dest(==source)]-10-|", options: [], metrics: nil, views: viewsDict1))
        detailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-40-[source]|", options: [], metrics: nil, views: viewsDict1))
        detailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-40-[dest]|", options: [], metrics: nil, views: viewsDict1))
        
        let btnDone = UIButton()
        btnDone.setTitle("DONE", for: UIControlState())
        btnDone.translatesAutoresizingMaskIntoConstraints = false
        btnDone.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnDone.backgroundColor = UIColor(red: 215.0/255.0, green: 69.0/255.0, blue: 46.0/255.0, alpha: 1.0)
        btnDone.addTarget(self, action: #selector(btnDoneClicked), for: .touchDown)
        self.view.addSubview(btnDone)
        
        let viewsDict = ["points":lblPoints, "distance":lblDistance, "view": detailView, "btn":btnDone]
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[points][distance(==points)]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[points(50)]", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[distance(50)]", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view]", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: viewsDict))
        
        self.view.addConstraint(NSLayoutConstraint(item: detailView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 0))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[btn]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]-10-|", options: [], metrics: nil, views: viewsDict))
        
    }
    
    func btnDoneClicked()
    {
        let vc = RideViewController()
        //vc.myRides = true
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
        //self.navigationController?.popToRootViewControllerAnimated(true)
       // NSNotificationCenter.defaultCenter().postNotificationName("updateRidesList", object: nil)
//        print("pool users count",poolUsers.count)
//        //        if poolUsers.count == 1 {
//        //            self.navigationController?.popToRootViewControllerAnimated(true)
//        //        } else {
//        let view = self.ratingView()
//        (view.viewWithTag(33) as? UITableView)?.reloadData()
//        
//        let alertViewHolder =   AlertContentViewHolder()
//        alertViewHolder.heightConstraintValue = 100
//        alertViewHolder.view = view
//        
//        AlertController.showAlertFor("Rate Your Co-rider", message: "", contentView: alertViewHolder, okButtonTitle: "Submit", willHaveAutoDismiss: true, okAction: {
//            self.setPoolRating()
//            }, cancelButtonTitle: "Skip", cancelAction: {
//                self.navigationController?.popToRootViewControllerAnimated(true)
//        })
       // }
    }
    
    func ratingView() -> UIView
    {
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        let table = UITableView()
        table.delegate = self
        table.tag = 33
        table.dataSource = self
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(TripDetailCell.self, forCellReuseIdentifier: "tripdetailcell")
        table.separatorColor = UIColor.clear
        view.addSubview(table)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table(100)]|", options: [], metrics: nil, views: ["table":table]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":table]))
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tripdetailcell") as! TripDetailCell
        cell.data = poolUsers[indexPath.row]
        return cell
    }
    
    func getPoolUsers() {
        RideRequestor().getRideMembers(poolId!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                self.poolUsers = (object as! RideMembersResponse).poolUsers!
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func setPoolRating () {
        let reqObj = setRatingRequest()
        reqObj.userId = UserInfo.sharedInstance.userID
        reqObj.poolId = poolId
        //reqObj.rateToUserIds
        //reqObj.ratings
        showIndicator("Submitting rating..")
        RideRequestor().setPoolRating(reqObj, success:{(success, object) in
            hideIndicator()
            if (success) {
                AlertController.showAlertFor("Rating", message: "Your ratings has been successfully submitted.", okAction: {
                    _ = self.navigationController?.popToRootViewController(animated: true)
                })
            } else {
                AlertController.showToastForError((object as! setRatingResponse).message!)
            }
        }) { (error) in
            hideIndicator()
        }
    }
}
