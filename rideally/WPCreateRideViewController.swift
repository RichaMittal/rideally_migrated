//
//  WPCreateRideViewController.swift
//  rideally
//
//  Created by Sarav on 08/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class WPCreateRideViewController: BaseScrollViewController, UIPickerViewDelegate, UIPickerViewDataSource, GMSMapViewDelegate
{
    var isStartLocWP = false
    var vehiclesList = [VehicleData]()
    var newRideReqObj = WPNewRideRequest()
    var wpData: WorkPlace?
    var lastRideType = ""
    var selectedDates = [String]()
    let btnAny = DLRadioButton()
    let btnOnly = DLRadioButton()
    
    func getVehicles()
    {
        let reqObj = GetALLVehicleListRequest()
        reqObj.userID = UserInfo.sharedInstance.userID
        
        showIndicator("Fetching vehicles data.")
        ProfileRequestor().getAddVehicleListData(reqObj, success: { (success, object) in
            hideIndicator()
            
            if let vehicles = (object as! GetALLVehicleListResponse).dataObj{
                self.vehiclesList = vehicles
                
                for vehicle in self.vehiclesList{
                    if(vehicle.isDefault == "1"){
                        self.txtVehicle.text = vehicle.RegNo
                        UserInfo.sharedInstance.vehicleSeqID = vehicle.vehicleSeqId!
                        UserInfo.sharedInstance.vehicleType = vehicle.vehicleType!
                        UserInfo.sharedInstance.vehicleModel = vehicle.vehicleModel!
                        UserInfo.sharedInstance.vehicleRegNo = vehicle.RegNo!
                        UserInfo.sharedInstance.vehicleSeatCapacity = vehicle.capacity!
                        UserInfo.sharedInstance.vehicleKind = vehicle.vehicleKind!
                        UserInfo.sharedInstance.vehicleOwnerName = vehicle.vehicleOwnerName!
                        UserInfo.sharedInstance.insurerName = (vehicle.insurerName)!
                        UserInfo.sharedInstance.insuredCmpnyName = (vehicle.insuredCmpnyName)!
                        UserInfo.sharedInstance.insuranceId = (vehicle.insuranceId)!
                        UserInfo.sharedInstance.insuranceExpiryDate = (vehicle.insuranceExpiryDate)!
                        UserInfo.sharedInstance.insUploadStatus = vehicle.insUploadStatus!
                        UserInfo.sharedInstance.insUrl = vehicle.insUrl!
                        UserInfo.sharedInstance.rcUploadStatus = vehicle.rcUploadStatus!
                        UserInfo.sharedInstance.rcUrl = vehicle.rcUrl!
                        if let capacity = vehicle.capacity{
                            self.stepperSeats.maximum = Int32(capacity)! - 1
                            self.stepperSeats.value = Int32(capacity)! - 1
                        }
                        
                        if let type = vehicle.vehicleType{
                            var maxAmt = 0
                            if(type.lowercased() == "car"){
                                maxAmt = Int(self.vehicleConfig.car!)!
                            } else if(type.lowercased() == "bike"){
                                maxAmt = Int(self.vehicleConfig.bike!)!
                            } else if(type.lowercased() == "suv"){
                                maxAmt = Int(self.vehicleConfig.suv!)!
                            } else {
                                maxAmt = 99999
                            }
                            
                            self.stepperAmt.maximum = Int32(maxAmt)
                            self.stepperAmt.value = 0
                        }
                    }
                }
                
                if(self.vehiclesList.count == 0){
                    self.txtVehicle.placeholder = "No vehicles available"
                    self.txtVehicle.isUserInteractionEnabled = false
                    self.stepperSeats.isUserInteractionEnabled = false
                }
                else{
                    
                    self.txtVehicle.isUserInteractionEnabled = true
                    self.stepperSeats.isUserInteractionEnabled = true
                }
            }
            
        }) { (error) in
            hideIndicator()
        }
        
    }
    
    func getConfigData()
    {
        showIndicator("Fetching vehicle config.")
        RideRequestor().getVehicleConfig("workplace", groupID: (wpData?.wpID)!, success: { (success, object) in
            hideIndicator()
            self.vehicleConfig = object as! VehicleConfigResponse
            self.getVehicles()
        }) { (error) in
            hideIndicator()
        }
    }
    
    lazy var costView: SubView = self.getCostView()
    lazy var vehicleView: SubView = self.getVehicleSelectionView()
    
    var vehicleConfig: VehicleConfigResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Create Ride"
        
        getConfigData()
        
        viewsArray.append(getSegment())
        viewsArray.append(getLocationView())
        viewsArray.append(getTravelWithView())
        viewsArray.append(getDateView())
        viewsArray.append(getTimeView())
        
        viewsArray.append(costView)
        viewsArray.append(vehicleView)
        
        viewsArray.append(getCommentView())
        
        addBottomView(getBottomView())
    }
    
    let segment = HMSegmentedControl()
    func getSegment() -> SubView
    {
        let view = UIView()
        segment.sectionTitles = ["Offer a Vehicle", "Need a Ride"]
        segment.backgroundColor = UIColor.clear
        segment.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.GREEN_COLOR]
        segment.selectedTitleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
        segment.selectionStyle = HMSegmentedControlSelectionStyleBox
        segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone
        segment.selectionIndicatorColor = Colors.GREEN_COLOR
        segment.selectionIndicatorBoxOpacity = 1.0
        segment.translatesAutoresizingMaskIntoConstraints = false
        segment.segmentEdgeInset = UIEdgeInsets.zero
        segment.layer.borderColor = Colors.GREEN_COLOR.cgColor
        segment.layer.borderWidth = 1
        
        segment.addTarget(self, action: #selector(rideTypeChanged), for: .valueChanged)
        view.addSubview(segment)
        newRideReqObj.ridetype = "O"
        let viewsDict = ["segment": segment]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[segment]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[segment]-20-|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    func rideTypeChanged()
    {
        if(segment.selectedSegmentIndex == 0){
            viewsArray.insert(costView, at: 5)
            viewsArray.insert(vehicleView, at: 6)
            newRideReqObj.ridetype = "O"
        }
        else{
            viewsArray.remove(at: 6)
            viewsArray.remove(at: 5)
            newRideReqObj.ridetype = "N"
        }
        relayoutSubview()
    }
    
    let lblSource = UILabel()
    let lblDest = UILabel()
    let view1 = UIView()
    let view2 = UIView()
    let iconSourceArrow = UIImageView()
    let iconDestArrow = UIImageView()
    
    func getLocationView() -> SubView
    {
        let view = UIView()
        
        view1.translatesAutoresizingMaskIntoConstraints = false
        view1.layer.borderWidth = 1
        view1.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        view1.addSubview(iconSource)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.text = "From (Select exact or nearby google places)"
        lblSource.font = boldFontWithSize(14)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(mapForFrom))
        lblSource.isUserInteractionEnabled = true
        lblSource.addGestureRecognizer(tapGestureFrom)
        view1.addSubview(lblSource)
        
        
        iconSourceArrow.image = UIImage(named: "rightArrow")
        iconSourceArrow.translatesAutoresizingMaskIntoConstraints = false
        view1.addSubview(iconSourceArrow)
        
        let btnFlip = UIButton()
        btnFlip.setImage(UIImage(named: "vflip"), for: UIControlState())
        btnFlip.translatesAutoresizingMaskIntoConstraints = false
        btnFlip.addTarget(self, action: #selector(swapDestinations), for: .touchDown)
        view.addSubview(btnFlip)
        
        view2.translatesAutoresizingMaskIntoConstraints = false
        view2.layer.borderWidth = 1
        view2.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        view2.addSubview(iconDest)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = boldFontWithSize(14)
        lblDest.text = "To (Select exact or nearby google places)"
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        
        iconDestArrow.image = UIImage(named: "rightArrow")
        iconDestArrow.translatesAutoresizingMaskIntoConstraints = false
        view2.addSubview(iconDestArrow)
        iconDestArrow.isHidden = true
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForTo))
        lblDest.isUserInteractionEnabled = true
        lblDest.addGestureRecognizer(tapGestureTo)
        view2.addSubview(lblDest)
        
        view.addSubview(view1)
        view.addSubview(view2)
        if(UserInfo.sharedInstance.homeAddress != ""){
            newRideReqObj.toLocation = UserInfo.sharedInstance.homeAddress
            newRideReqObj.toLat = UserInfo.sharedInstance.homeLatValue
            newRideReqObj.toLong = UserInfo.sharedInstance.homeLongValue
        }
        if(isStartLocWP){
            view1.backgroundColor = Colors.ODD_ROW_COLOR
            lblSource.text = wpData?.wpLocation != nil ? wpData?.wpLocation : wpData?.wpDetailLocation
            lblDest.text = "To (Select exact or nearby google places)"
            if(UserInfo.sharedInstance.homeAddress != ""){
                lblDest.text = UserInfo.sharedInstance.homeAddress
            }
            view2.backgroundColor = UIColor.clear
            lblDest.isUserInteractionEnabled = true
            lblSource.isUserInteractionEnabled = false
            iconDestArrow.isHidden = false
            iconSourceArrow.isHidden = true
        }
        else{
            view2.backgroundColor = Colors.ODD_ROW_COLOR
            lblDest.text = wpData?.wpLocation != nil ? wpData?.wpLocation : wpData?.wpDetailLocation
            lblSource.text = "From (Select exact or nearby google places)"
            if(UserInfo.sharedInstance.homeAddress != ""){
                lblSource.text = UserInfo.sharedInstance.homeAddress
            }
            view1.backgroundColor = UIColor.clear
            lblDest.isUserInteractionEnabled = false
            lblSource.isUserInteractionEnabled = true
            iconDestArrow.isHidden = true
            iconSourceArrow.isHidden = false
        }
        
        let viewsDict = ["view1":view1, "view2":view2, "isource":iconSource, "source":lblSource, "idest":iconDest, "dest":lblDest,"sarrow":iconSourceArrow, "darrow":iconDestArrow, "btnflip": btnFlip]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view1(40)]-5-[btnflip(30)]-5-[view2(40)]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: btnFlip, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view2]|", options: [], metrics: nil, views: viewsDict))
        
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource(15)]-5-[source][sarrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source]|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest(15)]-5-[dest][darrow(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dest]|", options: [], metrics: nil, views: viewsDict))
        
        view1.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view2.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        view1.addConstraint(NSLayoutConstraint(item: iconSourceArrow, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view2.addConstraint(NSLayoutConstraint(item: iconDestArrow, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(10, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 120)
        return sub
    }
    
    func mapForFrom()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.isFromWP = true
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.newRideReqObj.toLocation = self.lblSource.text
                    self.newRideReqObj.toLat = loc["lat"]
                    self.newRideReqObj.toLong = loc["long"]
                    
                    if(self.newRideReqObj.toLocation != UserInfo.sharedInstance.homeAddress){
                        AlertController.showAlertFor("Home Location", message: "Do you want to save \(self.newRideReqObj.toLocation!) as your Home location", okButtonTitle: "Change", okAction: {
                            self.saveHomeLocation(loc["location"]!, lat: loc["lat"]!, long: loc["long"]!)
                            }, cancelButtonTitle: "Cancel", cancelAction: {
                                _ = self.navigationController?.popViewController(animated: true)})
                    }
                }
            }
             vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func mapForTo()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.isFromWP = true
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.newRideReqObj.toLocation = self.lblDest.text
                    self.newRideReqObj.toLat = loc["lat"]
                    self.newRideReqObj.toLong = loc["long"]
                    
                    if(self.newRideReqObj.toLocation != UserInfo.sharedInstance.homeAddress){
                        AlertController.showAlertFor("Home Location", message: "Do you want to save \(self.newRideReqObj.toLocation!) as your Home location", okButtonTitle: "Change", okAction: {
                            self.saveHomeLocation(loc["location"]!, lat: loc["lat"]!, long: loc["long"]!)
                            }, cancelButtonTitle: "Cancel", cancelAction: {
                                _ = self.navigationController?.popViewController(animated: true)})
                    }
                }
            }
             vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func swapDestinations()
    {
        if(isStartLocWP){
            isStartLocWP = false
        }
        else{
            isStartLocWP = true
        }
        if(UserInfo.sharedInstance.homeAddress != ""){
            newRideReqObj.toLocation = UserInfo.sharedInstance.homeAddress
            newRideReqObj.toLat = UserInfo.sharedInstance.homeLatValue
            newRideReqObj.toLong = UserInfo.sharedInstance.homeLongValue
        }
        if(isStartLocWP){
            view1.backgroundColor = Colors.ODD_ROW_COLOR
            lblSource.text = wpData?.wpLocation != nil ? wpData?.wpLocation : wpData?.wpDetailLocation
            lblDest.text = "To (Select exact or nearby google places)"
            if(UserInfo.sharedInstance.homeAddress != ""){
                lblDest.text = UserInfo.sharedInstance.homeAddress
            }
            view2.backgroundColor = UIColor.clear
            lblDest.isUserInteractionEnabled = true
            lblSource.isUserInteractionEnabled = false
            iconDestArrow.isHidden = false
            iconSourceArrow.isHidden = true
        }
        else{
            view2.backgroundColor = Colors.ODD_ROW_COLOR
            lblDest.text = wpData?.wpLocation != nil ? wpData?.wpLocation : wpData?.wpDetailLocation
            lblSource.text = "From (Select exact or nearby google places)"
            if(UserInfo.sharedInstance.homeAddress != ""){
                lblSource.text = UserInfo.sharedInstance.homeAddress
            }
            view1.backgroundColor = UIColor.clear
            lblDest.isUserInteractionEnabled = false
            lblSource.isUserInteractionEnabled = true
            iconDestArrow.isHidden = true
            iconSourceArrow.isHidden = false
        }
    }
    
    func getTravelWithView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "TRAVEL WITH"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        btnAny.setTitle("Any", for: UIControlState())
        btnAny.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnAny.iconColor = Colors.GREEN_COLOR
        btnAny.indicatorColor = Colors.GREEN_COLOR
        btnAny.titleLabel?.font = boldFontWithSize(14)
        btnAny.translatesAutoresizingMaskIntoConstraints = false
        btnAny.isSelected = true
        view.addSubview(btnAny)
        
        if(UserInfo.sharedInstance.gender == "F"){
            btnOnly.setTitle("Only Female", for: UIControlState())
        }
        else{
            btnOnly.setTitle("Only Male", for: UIControlState())
        }
        btnOnly.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnOnly.iconColor = Colors.GREEN_COLOR
        btnOnly.indicatorColor = Colors.GREEN_COLOR
        btnOnly.titleLabel?.font = boldFontWithSize(14)
        btnOnly.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(btnOnly)
        
        btnAny.otherButtons = [btnOnly]
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[any(50)]-10-[only(100)]", options: [], metrics: nil, views: ["any":btnAny, "only":btnOnly, "line":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title(20)]-5-[any(25)]-4-[line(0.5)]", options: [], metrics: nil, views: ["any":btnAny, "title":lblTitle, "line":lblLine1]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[title]", options: [], metrics: nil, views: ["title":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        view.addConstraint(NSLayoutConstraint(item: btnOnly, attribute: .centerY, relatedBy: .equal, toItem: btnAny, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    var btn1: UIButton?
    var btn2: UIButton?
    var btn3: UIButton?
    var btn4: UIButton?
    var btn5: UIButton?
    var btn6: UIButton?
    var btn7: UIButton?
    
    func getDateView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "I WANT TO TRAVEL ON"
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.font = boldFontWithSize(11)
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        let lblMonth = UILabel()
        lblMonth.text = prettyDateStringFromDate(Date(), toFormat: "MMM yyyy")
        lblMonth.textColor = Colors.GREEN_COLOR
        lblMonth.font = boldFontWithSize(14)
        lblMonth.textAlignment = .center
        lblMonth.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblMonth)
        
        let datesList = getDates().0
        
        let datesView = UIView()
        datesView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(datesView)
        
        btn1 = configDateButton()
        btn1?.isSelected = true
        btn1!.setTitle(datesList[0].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
        btn1!.setTitle(datesList[0].replacingOccurrences(of: " ", with: "\n"), for: .selected)
        if(btn1!.isSelected == true){
            btn1!.backgroundColor = Colors.GREEN_COLOR
            btn1!.isSelected = true
        }
        datesView.addSubview(btn1!)
        
        btn2 = configDateButton()
        btn2!.setTitle(datesList[1].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
        btn2!.setTitle(datesList[1].replacingOccurrences(of: " ", with: "\n"), for: .selected)
        datesView.addSubview(btn2!)
        
        btn3 = configDateButton()
        btn3!.setTitle(datesList[2].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
        btn3!.setTitle(datesList[2].replacingOccurrences(of: " ", with: "\n"), for: .selected)
        datesView.addSubview(btn3!)
        
        btn4 = configDateButton()
        btn4!.setTitle(datesList[3].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
        btn4!.setTitle(datesList[3].replacingOccurrences(of: " ", with: "\n"), for: .selected)
        datesView.addSubview(btn4!)
        
        btn5 = configDateButton()
        btn5!.setTitle(datesList[4].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
        btn5!.setTitle(datesList[4].replacingOccurrences(of: " ", with: "\n"), for: .selected)
        datesView.addSubview(btn5!)
        
        btn6 = configDateButton()
        btn6!.setTitle(datesList[5].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
        btn6!.setTitle(datesList[5].replacingOccurrences(of: " ", with: "\n"), for: .selected)
        datesView.addSubview(btn6!)
        
        btn7 = configDateButton()
        btn7!.setTitle(datesList[6].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
        btn7!.setTitle(datesList[6].replacingOccurrences(of: " ", with: "\n"), for: .selected)
        datesView.addSubview(btn7!)
        
        let btnsDict = ["b1":btn1!,"b2":btn2!,"b3":btn3!,"b4":btn4!,"b5":btn5!,"b6":btn6!,"b7":btn7!]
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[b1]-5-[b2(==b1)]-5-[b3(==b1)]-5-[b4(==b1)]-5-[b5(==b1)]-5-[b6(==b1)]-5-[b7(==b1)]-5-|", options: [], metrics: nil, views: btnsDict))
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b1(50)]-5-|", options: [], metrics: nil, views: btnsDict))
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b2(50)]-5-|", options: [], metrics: nil, views: btnsDict))
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b3(50)]-5-|", options: [], metrics: nil, views: btnsDict))
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b4(50)]-5-|", options: [], metrics: nil, views: btnsDict))
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b5(50)]-5-|", options: [], metrics: nil, views: btnsDict))
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b6(50)]-5-|", options: [], metrics: nil, views: btnsDict))
        datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b7(50)]-5-|", options: [], metrics: nil, views: btnsDict))
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbltitle":lblTitle, "lblmonth":lblMonth, "dates":datesView, "line1":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbltitle(20)]-5-[lblmonth(20)]-5-[dates(60)]-5-[line1(0.5)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbltitle]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblmonth]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[dates]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 131)
        return sub
    }
    
    func getDates() -> ([String],[String])
    {
        var datesList = [String]()
        var datesObjList = [String]()
        
        var today = Date()
        
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        return (datesList, datesObjList)
    }
    
    func configDateButton() -> UIButton
    {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.borderWidth = 1
        btn.layer.borderColor = Colors.GREEN_COLOR.cgColor
        btn.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btn.setTitleColor(Colors.WHITE_COLOR, for: .selected)
        btn.addTarget(self, action: #selector(dateBtnClicked(_:)), for: .touchDown)
        
        btn.titleLabel?.textAlignment = .center
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.titleLabel?.font = boldFontWithSize(14)
        return btn
    }
    
    func dateBtnClicked(_ btn : UIButton)
    {
        if(btn.isSelected == true){
            btn.backgroundColor = Colors.WHITE_COLOR
            btn.isSelected = false
        }
        else{
            btn.isSelected = true
            btn.backgroundColor = Colors.GREEN_COLOR
        }
    }
    
    let timePicker = UIDatePicker()
    let txtStartTime = RATextField()
    
    func getTimeView() -> SubView
    {
        let view = UIView()
        
        let lblDate = UILabel()
        lblDate.text = "START TIME"
        lblDate.textColor = Colors.GRAY_COLOR
        lblDate.font = boldFontWithSize(11)
        lblDate.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDate)
        
        txtStartTime.layer.borderWidth = 0.5
        txtStartTime.delegate = self
        txtStartTime.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtStartTime.translatesAutoresizingMaskIntoConstraints = false
        txtStartTime.font = boldFontWithSize(14)
        
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "NL")
        txtStartTime.inputView = timePicker
        timePicker.date = Date().addingTimeInterval(300)
        txtStartTime.text = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtStartTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "time")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        txtStartTime.rightView = imgV
        txtStartTime.rightViewMode = .always
        view.addSubview(txtStartTime)
        txtStartTime.placeholder = "Select Time"
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbldate":lblDate, "txtdate":txtStartTime, "line":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbldate(20)]-5-[txtdate(30)]-10-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbldate]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txtdate(130)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
        
    }
    var selectedTxtField: UITextField?
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    func donePressed()
    {
        if(selectedTxtField == txtStartTime){
            selectedTxtField?.text = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        }
        else if(selectedTxtField == txtCostOptions){
            selectedTxtField?.text = ""
        }
        else if(selectedTxtField == txtVehicle){
            if(vehiclesList.count > 0){
                let vehicle = vehiclesList[pickerView.selectedRow(inComponent: 0)]
                selectedTxtField?.text = vehicle.RegNo
                UserInfo.sharedInstance.vehicleSeqID = vehicle.vehicleSeqId!
                UserInfo.sharedInstance.vehicleType = vehicle.vehicleType!
                UserInfo.sharedInstance.vehicleModel = vehicle.vehicleModel!
                UserInfo.sharedInstance.vehicleRegNo = vehicle.RegNo!
                UserInfo.sharedInstance.vehicleSeatCapacity = vehicle.capacity!
                UserInfo.sharedInstance.vehicleKind = vehicle.vehicleKind!
                UserInfo.sharedInstance.vehicleOwnerName = vehicle.vehicleOwnerName!
                UserInfo.sharedInstance.insurerName = vehicle.insurerName!
                UserInfo.sharedInstance.insuredCmpnyName = vehicle.insuredCmpnyName!
                UserInfo.sharedInstance.insuranceId = vehicle.insuranceId!
                UserInfo.sharedInstance.insuranceExpiryDate = vehicle.insuranceExpiryDate!
                UserInfo.sharedInstance.insUploadStatus = vehicle.insUploadStatus!
                UserInfo.sharedInstance.insUrl = vehicle.insUrl!
                UserInfo.sharedInstance.rcUploadStatus = vehicle.rcUploadStatus!
                UserInfo.sharedInstance.rcUrl = vehicle.rcUrl!
                sendDefaultVehicleRequest()
                if let capacity = vehicle.capacity{
                    self.stepperSeats.maximum = Int32(capacity)! - 1
                    self.stepperSeats.value = Int32(capacity)! - 1
                }
                
                if let type = vehicle.vehicleType{
                    var maxAmt = 0
                    if(type.lowercased() == "car"){
                        maxAmt = Int(self.vehicleConfig.car!)!
                    } else if(type.lowercased() == "bike"){
                        maxAmt = Int(self.vehicleConfig.bike!)!
                    } else if(type.lowercased() == "suv"){
                        maxAmt = Int(self.vehicleConfig.suv!)!
                    } else {
                        maxAmt = 99999
                    }
                    
                    self.stepperAmt.maximum = Int32(maxAmt)
                    self.stepperAmt.value = 0
                }
            }
        }
        selectedTxtField?.resignFirstResponder()
    }
    
    func sendDefaultVehicleRequest() -> Void {
        
        if  UserInfo.sharedInstance.userID.characters.count > 0 && UserInfo.sharedInstance.vehicleSeqID.characters.count > 0
        {
            let reqObj = SetDefaultVehicleRequest()
            showLoadingIndicator("Loading...")
            reqObj.userID = UserInfo.sharedInstance.userID
            reqObj.vehicleSeqID = UserInfo.sharedInstance.vehicleSeqID
            
            ProfileRequestor().sendSetDefaultVehicleRequest(reqObj, success:{ (success, object) in
                self.hideLoadingIndiactor()
                AlertController.showToastForInfo((object as! SetDefaultVehicleResponse).message!)
            }){ (error) in
                self.hideLoadingIndiactor()
                AlertController.showAlertForError(error)
            }
        }
        else
        {
            AlertController.showToastForError("Invalid UserID/VehicleSeqID provided in the Request")
        }
        
    }

    let pickerView = UIPickerView()
    let stepperAmt = PKYStepper()
    let txtCostOptions = RATextField()
    
    func getCostView() -> SubView
    {
        pickerView.delegate = self
        pickerView.dataSource = self
        
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "I AM WILLING TO SHARE THE COST"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        stepperAmt.setBorderColor(Colors.GRAY_COLOR)
        stepperAmt.setBorderWidth(0.5)
        stepperAmt.countLabel.layer.borderWidth = 0.5
        stepperAmt.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperAmt.valueChangedCallback = { (stepper, count) -> Void in
            stepper?.countLabel.text = "\(count)"
        }
        
        stepperAmt.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Cost for a seat is exceeding its maximum.")
            }
        }
        
        stepperAmt.setLabelTextColor(Colors.GRAY_COLOR)
        stepperAmt.value = 0
        
        stepperAmt.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperAmt)
        
        let lblMsg = UILabel()
        lblMsg.text = "Please mention per ride cost only"
        lblMsg.font = boldFontWithSize(11)
        lblMsg.textColor = Colors.GRAY_COLOR
        lblMsg.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblMsg)
        
        let lblPer = UILabel()
        lblPer.text = "PER"
        lblPer.font = boldFontWithSize(14)
        lblPer.textColor = Colors.GRAY_COLOR
        lblPer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPer)
        
        txtCostOptions.layer.borderWidth = 0.5
        txtCostOptions.delegate = self
        txtCostOptions.font = boldFontWithSize(14)
        txtCostOptions.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtCostOptions.translatesAutoresizingMaskIntoConstraints = false
        //txtCostOptions.inputView = pickerView
        txtCostOptions.text = "KM"
        txtCostOptions.isUserInteractionEnabled = false
        view.addSubview(txtCostOptions)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtCostOptions.inputAccessoryView = keyboardDoneButtonShow
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["title":lblTitle, "amount":stepperAmt, "msg":lblMsg, "per":lblPer, "persons":txtCostOptions, "line":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title(20)]-5-[amount(30)]-5-[msg(15)]-5-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[title]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[amount]-15-[per]-15-[persons(==amount)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[msg]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: lblPer, attribute: .centerY, relatedBy: .equal, toItem: stepperAmt, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: txtCostOptions, attribute: .centerY, relatedBy: .equal, toItem: stepperAmt, attribute: .centerY, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: txtCostOptions, attribute: .height, relatedBy: .equal, toItem: stepperAmt, attribute: .height, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 86)
        return sub
    }
    
    let txtVehicle = RATextField()
    let stepperSeats = PKYStepper()
    func getVehicleSelectionView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "ADD YOUR VEHICLE"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtVehicle.layer.borderWidth = 0.5
        txtVehicle.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        txtVehicle.translatesAutoresizingMaskIntoConstraints = false
        txtVehicle.delegate = self
        txtVehicle.font = boldFontWithSize(14)
        txtVehicle.inputView = pickerView
        txtVehicle.placeholder = "Select your vehicle"
        view.addSubview(txtVehicle)
        let imgV = UIImageView(image: UIImage(named: "dropdown")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        txtVehicle.rightView = imgV
        txtVehicle.rightViewMode = .always
        txtVehicle.isUserInteractionEnabled = false
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtVehicle.inputAccessoryView = keyboardDoneButtonShow
        
        let btnAddVehicle = UIButton()
        btnAddVehicle.translatesAutoresizingMaskIntoConstraints = false
        btnAddVehicle.setTitle(" REGISTER A NEW VEHICLE", for: UIControlState())
        btnAddVehicle.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnAddVehicle.setImage(UIImage(named: "addV"), for: UIControlState())
        btnAddVehicle.contentHorizontalAlignment = .left
        btnAddVehicle.titleLabel?.font = boldFontWithSize(11)
        btnAddVehicle.addTarget(self, action: #selector(addNewVehicle), for: .touchDown)
        view.addSubview(btnAddVehicle)
        
        let lblTitle1 = UILabel()
        lblTitle1.text = "SEATS AVAILABLE"
        lblTitle1.font = boldFontWithSize(11)
        lblTitle1.textColor = Colors.GRAY_COLOR
        lblTitle1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle1)
        
        stepperSeats.setBorderColor(Colors.GRAY_COLOR)
        stepperSeats.setBorderWidth(0.5)
        stepperSeats.countLabel.layer.borderWidth = 0.5
        stepperSeats.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperSeats.valueChangedCallback = { (stepper, count) -> Void in
            self.stepperSeats.countLabel.text = "\(count)"
        }
        stepperSeats.setLabelTextColor(Colors.GRAY_COLOR)
        stepperSeats.value = 1
        stepperSeats.minimum = 1
        
        stepperSeats.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Seat selection is exceeding its maximum seat available.")
            }
        }
        
        stepperSeats.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperSeats)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["title":lblTitle, "vehicle":txtVehicle, "title1":lblTitle1, "stepper":stepperSeats, "line":lblLine1, "btn":btnAddVehicle]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title(20)]-5-[vehicle(30)]-3-[btn(20)]-5-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[title]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[vehicle]-20-[stepper(150)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[btn(200)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: stepperSeats, attribute: .top, relatedBy: .equal, toItem: txtVehicle, attribute: .top, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stepperSeats, attribute: .height, relatedBy: .equal, toItem: txtVehicle, attribute: .height, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: lblTitle1, attribute: .left, relatedBy: .equal, toItem: stepperSeats, attribute: .left, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblTitle1, attribute: .centerY, relatedBy: .equal, toItem: lblTitle, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 91)
        return sub
    }
    
    func addNewVehicle()
    {
        let vc = RegisterNewVehicleController()
        vc.isUpdateReqneedtobeSend = "0"
        vc.isSeatCapacityNeedToBeUpdated = "1"
        if(wpData?.wpInsuranceStatus != "" && wpData?.wpInsuranceStatus != nil && wpData?.wpInsuranceStatus == "1") {
            vc.isWpInsuranceStatus = true
        }
        vc.onAddSuccessAction = {
            self.getVehicles()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateVehicle()
    {
        let vc = RegisterNewVehicleController()
        vc.isUpdateReqneedtobeSend = "1"
        vc.isSeatCapacityNeedToBeUpdated = "1"
        if(wpData?.wpInsuranceStatus != "" && wpData?.wpInsuranceStatus != nil && wpData?.wpInsuranceStatus == "1") {
            vc.isWpInsuranceStatus = true
        }
        vc.onAddSuccessAction = {
            self.getVehicles()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if(selectedTxtField == txtVehicle){
            return vehiclesList.count
        }
        else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(selectedTxtField == txtVehicle){
            return vehiclesList[row].RegNo
        }
        else {
            return ""
        }
    }
    
    let txtComments = RATextField()
    func getCommentView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "COMMENT"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtComments.font = boldFontWithSize(14)
        txtComments.placeholder = "Enter your comment here"
        txtComments.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(txtComments)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let viewsDict = ["lbl":lblTitle, "txt":txtComments, "line":lblLine1]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)][txt(40)]-2-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txt]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 10, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let btn = UIButton()
        btn.setTitle("SUBMIT", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(proceed), for: .touchDown)
        
        let sub = SubView()
        sub.view = btn
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: btn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func proceed()
    {
        newRideReqObj.fromLocation = wpData?.wpLocation != nil ? wpData?.wpLocation : wpData?.wpDetailLocation
        newRideReqObj.fromLat = wpData?.wpLat != nil ? wpData?.wpLat : wpData?.wpDetailLat
        newRideReqObj.fromLong = wpData?.wpLong != nil ? wpData?.wpLong : wpData?.wpDetailLong
        if(isStartLocWP){
            if(newRideReqObj.toLocation == nil){
                AlertController.showAlertFor("Create Ride", message: "Please choose location in 'To' field from Google Places", okButtonTitle: "Ok", okAction: nil)
                return
            }
        }
        else{
            if(newRideReqObj.fromLocation == nil){
                AlertController.showAlertFor("Create Ride", message: "Please choose location in 'From' field from Google Places", okButtonTitle: "Ok", okAction: nil)
                return
            }
        }
        
        let dates = getDates().1
        if(btn1?.isSelected == true){
            selectedDates.append(dates[0])
        }
        if(btn2?.isSelected == true){
            selectedDates.append(dates[1])
        }
        if(btn3?.isSelected == true){
            selectedDates.append(dates[2])
        }
        if(btn4?.isSelected == true){
            selectedDates.append(dates[3])
        }
        if(btn5?.isSelected == true){
            selectedDates.append(dates[4])
        }
        if(btn6?.isSelected == true){
            selectedDates.append(dates[5])
        }
        if(btn7?.isSelected == true){
            selectedDates.append(dates[6])
        }
        
        if(selectedDates.count > 0){
            newRideReqObj.selected_dates = selectedDates.joined(separator: ",")
        }
        else{
            AlertController.showAlertFor("Create Ride", message: "Please choose a date for your travel.", okButtonTitle: "Ok", okAction: nil)
            return
        }
        
        if(txtStartTime.text?.characters.count <= 0){
            AlertController.showAlertFor("Create Ride", message: "Please choose a time for your travel.", okButtonTitle: "Ok", okAction: nil)
            return
        }
        if(isStartLocWP){
            newRideReqObj.comingtime = txtStartTime.text!
            newRideReqObj.goingtime = ""
        }
        else{
            newRideReqObj.goingtime = txtStartTime.text!
            newRideReqObj.comingtime = ""
        }
        
        newRideReqObj.poolType = self.btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
        
        newRideReqObj.pool_scope = wpData?.wpID
        if(newRideReqObj.ridetype == "O"){
            if let vehNo = txtVehicle.text{
                newRideReqObj.vehregno = vehNo
                newRideReqObj.veh_capacity = stepperSeats.countLabel.text!
                newRideReqObj.cost = stepperAmt.countLabel.text!
            }
        }
        if(newRideReqObj.ridetype == "O"){
            let todayDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.string(from: todayDate) as String
            if(newRideReqObj.vehregno == ""){
                AlertController.showAlertFor("Create Ride", message: "Please provide vehicle details.", okButtonTitle: "Ok", okAction: nil)
                return
            } else if(newRideReqObj.vehregno != "" && wpData?.wpInsuranceStatus == "1" && (UserInfo.sharedInstance.insuranceId == "")) {
                AlertController.showAlertFor("Create Ride", message: "Please provide vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                    self.updateVehicle()
                })
                return
            } else if(newRideReqObj.vehregno != "" && wpData?.wpInsuranceStatus == "1" && UserInfo.sharedInstance.insuranceId != ""  && UserInfo.sharedInstance.insuranceExpiryDate.compare(date) == ComparisonResult.orderedAscending) {
                AlertController.showAlertFor("Create Ride", message: "Please update vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                    self.updateVehicle()
                })
                return
            }
        }
        
        if(wpData?.wpScope == "1") {
            newRideReqObj.pool_shared = "0"
        } else {
            newRideReqObj.pool_shared = "1"
        }
        
        CommonRequestor().getDistanceDuration(self.newRideReqObj.fromLocation!, destination: self.newRideReqObj.toLocation!, success: { (success, object) in
            self.newRideReqObj.duration = ""
            self.newRideReqObj.distance = ""
//            if let data = object as? NSDictionary{
//                let dict = ((((data["rows"  as NSObject] as! NSArray)[0])["elements" as NSObject] as! NSArray)[0] as! NSDictionary)
//                if((dict["status"] as! String) == "OK")
//                {
//                    let distance = ((dict["distance"] as! NSDictionary)["text"])!
//                    let duration = ((dict["duration"] as! NSDictionary)["text"])!
//                    self.newRideReqObj.duration = duration as! String
//                    self.newRideReqObj.distance = distance as! String
//                }
//            }
            let sourceStr = "\(self.newRideReqObj.fromLat!),\(self.newRideReqObj.fromLong!)"
            let destinationStr = "\(self.newRideReqObj.toLat!), \(self.newRideReqObj.toLong!)"
            CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                self.newRideReqObj.via = ""
                self.newRideReqObj.waypoints = ""
                if let data = object as? NSDictionary{
                    let routes = data["routes"] as! NSArray
                    if routes.count == 0{
                        return
                    }
                    let routesDict = routes[0] as! NSDictionary
                    let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                    let points = overview_polyline ["points"] as! NSString
                    self.newRideReqObj.via = routesDict ["summary"] as? String
                    self.newRideReqObj.waypoints = points as String
                }
                self.searchForRides()
                }, failure: { (error) in
                    self.searchForRides()
            })
            }, failure: { (error) in
                self.searchForRides()
        })
    }
    
    func createRide()
    {
        showIndicator("Creating Ride..")
        newRideReqObj.userId = UserInfo.sharedInstance.userID
        WorkplaceRequestor().addWPRide(newRideReqObj, success: { (success, object) in
            hideIndicator()
            if(success){
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! NeedRideResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.wpData = self.wpData
                            vc.triggerFrom = "place"
                            vc.inviteFlag = "wpMap"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! NeedRideResponse).code == "1811") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! NeedRideResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func searchForRides()
    {
        let searchReqObj = SearchRideRequest()
        searchReqObj.userId = UserInfo.sharedInstance.userID
        searchReqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        searchReqObj.reqTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        //searchReqObj.poolType = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        if(isStartLocWP){
            searchReqObj.fromLat = newRideReqObj.fromLat
            searchReqObj.fromLong = newRideReqObj.fromLong
            searchReqObj.startLoc = newRideReqObj.fromLocation
            searchReqObj.toLat = newRideReqObj.toLat
            searchReqObj.toLong = newRideReqObj.toLong
            searchReqObj.endLoc = newRideReqObj.toLocation
        } else {
            searchReqObj.fromLat = newRideReqObj.toLat
            searchReqObj.fromLong = newRideReqObj.toLong
            searchReqObj.startLoc = newRideReqObj.toLocation
            searchReqObj.toLat = newRideReqObj.fromLat
            searchReqObj.toLong = newRideReqObj.fromLong
            searchReqObj.endLoc = newRideReqObj.fromLocation
        }
        searchReqObj.groupID = wpData!.wpID
        searchReqObj.rideScope = wpData!.wpID
        searchReqObj.groupType = "6"
        searchReqObj.poolScope = wpData!.wpID
        searchReqObj.poolType = self.btnAny.isSelected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
        searchReqObj.searchType = "recurrent"
        searchReqObj.ridetype = ""
        searchReqObj.reqCurrentDate = selectedDates.joined(separator: ",")
        
        //searchReqObj.ridetype = newRideReqObj.ridetype!
        if(newRideReqObj.fromLocation != newRideReqObj.toLocation){
            showIndicator("Searching for similair rides.")
            
            WorkplaceRequestor().searchWPRides(searchReqObj, success: { (success, object) in
                hideIndicator()
                if(success){
                    if(self.isStartLocWP){
                        if((object as! WPRidesResponse).pools?.homeGoing!.count > 0){
                            self.gotoExistingRides(((object as! WPRidesResponse).pools?.homeGoing)!)
                        }
                        else{
                            self.createRide()
                        }
                    }
                    else{
                        if((object as! WPRidesResponse).pools?.officeGoing!.count > 0){
                            self.gotoExistingRides(((object as! WPRidesResponse).pools?.officeGoing)!)
                        }
                        else{
                            self.createRide()
                        }
                    }
                }
                }, failure: { (error) in
                    hideIndicator()
                    
            })
        } else {
            AlertController.showToastForError("Home location & workplace location should be different.")
        }
    }
    
    func gotoExistingRides(_ searchResults: [WPRide])
    {
        let vc = ExistingRidesViewController()
        vc.dataSource = searchResults
        vc.newRideReqObj = newRideReqObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func saveHomeLocation(_ loc: String,lat: String,long: String) {
        let reqObj = SetHomeLocationRequest()
        showIndicator("Loading...")
        reqObj.user_id = UserInfo.sharedInstance.userID
        reqObj.home_loc = loc
        reqObj.home_loc_lat = lat
        reqObj.home_loc_long = long
        
        ProfileRequestor().sendsetHomeLocationRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            
            if (object as! SetHomeLocationResponse).code == "8535"
            {
                
                if (object as! SetHomeLocationResponse).dataObj?.first != nil
                {
                    UserInfo.sharedInstance.homeAddress = loc
                    UserInfo.sharedInstance.homeLatValue = lat
                    UserInfo.sharedInstance.homeLongValue = long
                    _ = self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    AlertController.showToastForError("There is no User_ID found in the response.")
                }
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
}
