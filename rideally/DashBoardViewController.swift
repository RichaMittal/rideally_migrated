//
//  DashBoardViewController.swift
//  rideally
//
//  Created by Raghunathan on 9/7/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class DashBoardViewController: BaseScrollViewController
{
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.tabBarController?.tabBar.isTranslucent = false
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_menu"), style: .plain, target: self, action: #selector(showMenu))
        self.navigationItem.title = "Home"
        
        //checkIfUserUsesFBAccountForLoginORSignUP()
    }
    
    
    //TODO: This shouldn't be overrided, Since Dashborad screen is not yet done this is impelmented in this way.
    override func showMenu()
    {
        self.gotoHomeView()
    }
    
    func gotoHomeView() -> Void {
        let ins = UIApplication.shared.delegate as! AppDelegate
        ins.gotoHome()
    }
}
