//
//  ContactsListViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/11/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class ContactsListViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating{
    
    let table = UITableView()
    let responseTableView = UITableView()
    let rowHt: CGFloat = 40
    var nameMutArr : NSMutableArray = []
    var mobileMutArr : NSMutableArray = []
    var contactListArr : NSMutableArray = []
    var selectedPhoneMutArr : NSMutableArray = []
    var filteredContactListMutArr : NSMutableArray = []
    let btnSubmit = UIButton()
    var poolID = ""
    var triggerFrom = ""
    var isWp = false
    var responses: [InviteResult]?
    var wpresponses: [InviteWPResult]?
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        title = "Contacts List"
        viewsArray.append(getTableView())
        addBottomView(getBottomView())
    }
    
    override func goBack() {
        searchController.searchBar.isHidden = true
        self.dismiss(animated: true, completion: nil)
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func getTableView() ->SubView {
        let view = UIView()
        let mobileCount = mobileMutArr.count+1
        table.tableHeaderView = searchController.searchBar
        table.translatesAutoresizingMaskIntoConstraints = false
        table.delegate = self
        table.dataSource = self
        table.register(UITableViewCell.self, forCellReuseIdentifier: "contactlistcell")
        table.separatorColor = Colors.GRAY_COLOR
        table.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        table.tableFooterView = UIView()
        
        view.addSubview(table)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":table]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[table]-5-|", options: [], metrics: nil, views: ["table":table]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: CGFloat(mobileCount)*rowHt)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        btnSubmit.setTitle("SUBMIT", for: .normal)
        btnSubmit.backgroundColor = Colors.GREEN_COLOR
        btnSubmit.translatesAutoresizingMaskIntoConstraints = false
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: .normal)
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: .highlighted)
        btnSubmit.titleLabel?.font = boldFontWithSize(16)
        btnSubmit.isHidden = true
        btnSubmit.addTarget(self, action: #selector(submitClicked), for: .touchDown)
        view.addSubview(btnSubmit)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(40)]", options: [], metrics: nil, views: ["btn":btnSubmit]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btnSubmit]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func filterContentForSearchText(searchText: String) {
        filteredContactListMutArr.removeAllObjects()
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = NSMutableArray(array: (contactListArr as NSArray).filtered(using: searchPredicate))
        filteredContactListMutArr = array
        table.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == responseTableView) {
            if(isWp) {
                return (wpresponses?.count)!
            } else {
                return (responses?.count)!
            }
        } else {
            if searchController.isActive && searchController.searchBar.text != "" {
                return filteredContactListMutArr.count
            } else {
                return mobileMutArr.count
            }
        }
    }
    
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == responseTableView) {
            return UITableViewAutomaticDimension
        } else {
            return rowHt
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == responseTableView) {
            let cell = UITableViewCell()
            cell.textLabel?.font = normalFontWithSize(14)
            cell.textLabel?.numberOfLines = 0
            if(isWp) {
                if (wpresponses![indexPath.row].type == "mobile"){
                    cell.textLabel?.text = "\(wpresponses![indexPath.row].mobile!) - \(wpresponses![indexPath.row].result!)"
                }else{
                    cell.textLabel?.text = "\(wpresponses![indexPath.row].email!) - \(wpresponses![indexPath.row].result!)"
                }
            } else {
                if let mobile = responses![indexPath.row].mobile{
                    cell.textLabel?.text = "\(mobile) \(getMsgForCode(code: responses![indexPath.row].msgCode!))"
                }
                if let email = responses![indexPath.row].email{
                    cell.textLabel?.text = "\(email) \(getMsgForCode(code: responses![indexPath.row].msgCode!))"
                }
            }
            return cell
        } else {
            let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "contactlistcell")! as UITableViewCell
            if (self.contactListArr.count > 0 || filteredContactListMutArr.count > 0)
            {
                if searchController.isActive && searchController.searchBar.text != "" {
                    cell.textLabel?.text = self.filteredContactListMutArr[indexPath.row] as? String
                } else {
                    cell.textLabel?.text = self.contactListArr[indexPath.row] as? String
                }
                if cell.isSelected
                {
                    cell.isSelected = false
                    if cell.accessoryType == UITableViewCellAccessoryType.none
                    {
                        cell.accessoryType = UITableViewCellAccessoryType.checkmark
                    }
                    else
                    {
                        cell.accessoryType = UITableViewCellAccessoryType.none
                    }
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == table) {
            let cell = tableView.cellForRow(at: indexPath as IndexPath)
            var selectedPhone = ""
            if searchController.isActive && searchController.searchBar.text != "" {
                let splitValue = (filteredContactListMutArr[indexPath.row] as AnyObject).components(separatedBy: " ")
                let count = splitValue.count
                selectedPhone = splitValue[count-1]
            } else {
                selectedPhone = self.mobileMutArr[indexPath.row] as! String
            }
            
            if cell!.isSelected
            {
                cell!.isSelected = false
                if cell!.accessoryType == UITableViewCellAccessoryType.none
                {
                    cell!.accessoryType = UITableViewCellAccessoryType.checkmark
                    self.selectedPhoneMutArr.add(selectedPhone)
                }
                else
                {
                    cell!.accessoryType = UITableViewCellAccessoryType.none
                    self.selectedPhoneMutArr.remove(selectedPhone)
                }
            }
            if(selectedPhoneMutArr.count != 0) {
                btnSubmit.isHidden = false
            } else {
                btnSubmit.isHidden = true
            }
        }
    }
    
    func submitClicked() {
        let stringRepresentation = self.selectedPhoneMutArr.componentsJoined(by: ",")
        if(isWp) {
            sendWpInviteRequest(phoneNumber: stringRepresentation, poolId: poolID)
        } else {
            sendRequest(phoneNumber: stringRepresentation, poolId: poolID)
        }
    }
    
    func screenRedirection(phoneNumber: String) {
        searchController.searchBar.isHidden = true
                _ = self.navigationController?.popViewController(animated: true)
                if(triggerFrom != "") {
                    if(triggerFrom == "wpMap") {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KInviteSendNotificationWpDetail"), object: nil, userInfo: ["phoneNumber":phoneNumber,"poolId":poolID])
                    } else if(triggerFrom == "wpRide") {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KInviteSendNotificationWpRideDetail"), object: nil, userInfo: ["phoneNumber":phoneNumber,"poolId":poolID])
                    } else if(triggerFrom == "allRides") {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KInviteSendNotificationallRides"), object: nil, userInfo: ["phoneNumber":phoneNumber,"poolId":poolID])
                    } else if(triggerFrom == "createRide") {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KInviteSendNotificationcreateRide"), object: nil, userInfo: ["phoneNumber":phoneNumber,"poolId":poolID])
                    } else if(triggerFrom == "wpDetail") {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KInviteSendNotificationWpDetail"), object: nil, userInfo: ["phoneNumber":phoneNumber,"poolId":poolID])
                    } else {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KInviteSendNotificationDetail"), object: nil, userInfo: ["phoneNumber":phoneNumber,"poolId":poolID])
                    }
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "KInviteSendNotification"), object: nil, userInfo: ["phoneNumber":phoneNumber,"poolId":poolID])
                }
    }
    
    func sendRequest(phoneNumber: String, poolId : String)
    {
        showIndicator("Sending Invites..")
        RideRequestor().sendInvite(poolId, ownerId: UserInfo.sharedInstance.userID, emailIDs: "", mobileNOs: phoneNumber, success: { (success, object) in
            hideIndicator()
            if(success){
                self.responses = (object as! InviteMembersResponse).data
                //self.dismissViewControllerAnimated(true, completion: nil)
                
                let inviteView = self.getInviteResposeView()
                (inviteView.viewWithTag(33) as? UITableView)?.reloadData()
                
                let alertViewHolder =   AlertContentViewHolder()
                alertViewHolder.heightConstraintValue = 100
                alertViewHolder.view = inviteView
                
                AlertController.showAlertFor("Invite Members", message: "", contentView: alertViewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: true, okAction: {
                    self.screenRedirection(phoneNumber: phoneNumber)
                }, cancelButtonTitle: nil, cancelAction: nil)
            }
        }, failure: { (error) in
            hideIndicator()
        })
    }
    
    func getInviteResposeView() -> UIView
    {
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        responseTableView.delegate = self
        responseTableView.tag = 33
        responseTableView.dataSource = self
        responseTableView.translatesAutoresizingMaskIntoConstraints = false
        responseTableView.separatorColor = UIColor.clear
        view.addSubview(responseTableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table(100)]|", options: [], metrics: nil, views: ["table":responseTableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":responseTableView]))
        return view
    }
    
    func getMsgForCode(code: String) -> String
    {
        let dict = ["161":"for tieup workplace domain mismatch",
                    "157":"personal not filled",
                    "158":"gender not filled",
                    "160":"is not registered with rideally, however inivitation has been sent successfully.",
                    "151":"Sorry, this is only female ride, hence you cannot invite a male.",
                    "152":"Sorry, this is only male ride, hence you cannot invite a female.",
                    "141":"Your invitation has been sent successfully & Notification Send to user.",
                    "142":"Your invitation has been sent successfully but Error in notification sending.",
                    "154":"Sorry, user has already joined in this ride.",
                    "155":"User already invited, however invitation has been sent successfully.",
                    "156":"User has already sent a request to join this ride. ",
                    "153":"Oops! You are the initiator of this ride, hence you cannot invite yourself."]
        
        return dict[code]!
    }
    
    func sendWpInviteRequest(phoneNumber: String, poolId : String) {
        showIndicator("Sending Invites..")
        WorkplaceRequestor().sendWPInvite(poolId, userId: UserInfo.sharedInstance.userID, emailIDs: "", mobileNOs: phoneNumber, success: { (success, object) in
            hideIndicator()
            
            if(success){
                self.wpresponses = (object as! InviteMembersWPResponse).data
                let view = self.getInviteResposeView()
                (view.viewWithTag(33) as? UITableView)?.reloadData()
                
                let alertViewHolder =   AlertContentViewHolder()
                alertViewHolder.heightConstraintValue = 100
                alertViewHolder.view = view
                
                AlertController.showAlertFor("Invite Members", message: "", contentView: alertViewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: true, okAction: {
                    self.screenRedirection(phoneNumber: phoneNumber)
                }, cancelButtonTitle: nil, cancelAction: nil)
            }
            
            
        }) { (error) in
            hideIndicator()
        }
    }
}
