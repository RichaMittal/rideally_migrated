//
//  UpdateDetailsViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 01/04/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire

extension UpdateDetailsViewController: GMSMapViewDelegate,GMSPanoramaViewDelegate, CLLocationManagerDelegate,UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate,UITextViewDelegate {
    
}

class UpdateDetailsViewController: UIViewController {
    var updateData: RideDetail?
    var wpUpdateData: WorkPlace?
    var vehicleConfig: VehicleConfigResponse!
    var vehicleDefaultDetails: VehicleDefaultResponse!
    var updateReqObj = UpdateRideRequest()
    var isSelectedLocation: String!
    var isSelectedButton: String!
    
    var totalDistanceInMeters: UInt = 0
    var totalDistance: String!
    var totalDurationInSeconds: UInt = 0
    var totalDuration: String!
    
    @IBOutlet weak var commentTextField: UITextView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    
    @IBOutlet weak var anyPersonButton: UIButton!
    @IBOutlet weak var vehicleButton: UIButton!
    @IBOutlet weak var seatbutton: UIButton!
    @IBOutlet weak var fareButton: UIButton!
    
    @IBOutlet weak var anyPickerView: UIPickerView!
    @IBOutlet weak var dateTimePickerView: UIDatePicker!
    
    @IBOutlet weak var mapRideView: GMSMapView!
    
    @IBOutlet weak var anyView: UIView!
    @IBOutlet weak var dateTimeView: UIView!
    @IBOutlet weak var dateTimeHeight: NSLayoutConstraint!
    @IBOutlet weak var anyPersonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var updateViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var vehicleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var updateBottomBGView: UIView!
    @IBOutlet weak var vechicleBgView: UIView!
    var pickerData: [String] = [String]()
    
    
    @IBOutlet weak var sourceButton: UIButton!
    @IBOutlet weak var destinationButton: UIButton!
    @IBOutlet weak var viaLable: UILabel!
    
    var routePolylineIndex0: GMSPolyline!
    var routePolylineIndex1: GMSPolyline!
    var routePolylineIndex2: GMSPolyline!
    var routePolyline: GMSPolyline!
    
    var polylineMutArray : NSMutableArray? = NSMutableArray()
    var viaMutArray : NSMutableArray? = NSMutableArray()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.dateTimeView.isHidden = true
        self.anyView.isHidden = true
        self.dateTimeHeight.constant = 0.0
        self.anyPersonViewHeight.constant = 0.0
        //self.updateViewHeight.constant = 120.0
        
        self.vechicleBgView.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        self.vechicleBgView.layer.borderWidth = 1.0
        self.vechicleBgView.layer.masksToBounds = true
        
        self.updateBottomBGView.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        self.updateBottomBGView.layer.borderWidth = 1.0
        self.updateBottomBGView.layer.masksToBounds = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Update Ride"
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        self.commentTextField.delegate = self
        //self.navigationController!.navigationBar.hidden = true
        self.mapRideView.delegate = self
        if UserInfo.sharedInstance.gender == "M" {
            pickerData = ["Any", "Only Male"]
        }else if UserInfo.sharedInstance.gender == "F" {
            pickerData = ["Any", "Only Female"]
        }else{
            pickerData = ["Any","Only Male","Only Female"]
        }
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(getUpdatePriceNotification), name:NSNotification.Name(rawValue: "kUpdateGetPriceNotification"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateAddressNotification), name:NSNotification.Name(rawValue: "kUpdateAddressNotification"), object: nil)
        self.initallyUpdatedUIDetails()
        self.defaultValueSet()
        self.addOverlayToMapView((updateData?.sourceLat)!,sourceLang: (updateData?.sourceLong)!,distinationLat: (updateData?.destinationLat)!,distinationLang: (updateData?.destinationLong)!)
        getConfigData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func goBack() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func initallyUpdatedUIDetails(){
        if let source = updateData?.source {
            self.sourceButton .setTitle(source, for: UIControlState())
        }
        if let via = updateData?.via {
            self.viaLable.text = "VIA- " + via
        } else {
            self.viaLable.text = ""
        }
        if let destination = updateData?.destination {
            self.destinationButton .setTitle(destination, for: UIControlState())
        }
        if let time = updateData?.startTime {
            self.timeButton .setTitle(time, for: UIControlState())
        }
        if let date = updateData?.startDate {
            self.dateButton .setTitle(date, for: UIControlState())
        }
        if let seatsLeft = updateData?.seatsLeft {
            self.seatbutton .setTitle(seatsLeft, for: UIControlState())
        }
        if let cost = updateData?.cost{
            self.fareButton .setTitle(cost + " / km", for: UIControlState())
        }
        
        
        if updateData?.poolType == "A" {
            self.anyPersonButton .setTitle("Any", for: UIControlState())
        }else if updateData?.poolType == "F" {
            self.anyPersonButton .setTitle("Only Female", for: UIControlState())
        }else{
            self.anyPersonButton .setTitle("Only Male", for: UIControlState())
        }
        
        
        var vehicleImage : UIImage!
        
        if let vehicleType = updateData?.vehicleType{
            if(vehicleType.lowercased() == "bike") {
                vehicleImage = UIImage(named: "veh_bike")
            } else if(vehicleType.lowercased() == "auto") {
                vehicleImage = UIImage(named: "auto")
            } else if(vehicleType.lowercased() == "car") {
                vehicleImage = UIImage(named: "veh_car")
            } else if(vehicleType.lowercased() == "cab") {
                vehicleImage = UIImage(named: "cab")
            } else if(vehicleType.lowercased() == "suv") {
                vehicleImage = UIImage(named: "suv")
            } else if(vehicleType.lowercased() == "tempo") {
                vehicleImage = UIImage(named: "tempo")
            } else if(vehicleType.lowercased() == "bus") {
                vehicleImage = UIImage(named: "bus")
            }
            self.vehicleButton.setImage(vehicleImage, for: UIControlState())
            self.vehicleButton.setTitle(vehicleType.capitalized, for: UIControlState())
        }
        
        if(updateData?.rideType == "O" || updateData?.poolType == "O"){
            self.vechicleBgView.isHidden = false
            self.vehicleViewHeight.constant = 30
            self.updateViewHeight.constant = 120
        }else{
            self.vechicleBgView.isHidden = true
            //self.vehicleViewHeight.constant = 0
            //self.updateViewHeight.constant = 90
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dissmissViewController(_ sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    // MARK: - Vehicle Button Selected.
    @IBAction func vehicleButtonClicked(_ button: UIButton) {
        if(button.currentTitle == "Add vehicle"){
            let vc = RegisterNewVehicleController()
            vc.isFromWp = "1"
            vc.noVehicle = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = AddVehicleViewController()
            vc.isFromWp = "1"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: -  Any Person Picker View
    @IBAction func pickerDoneButtonClicked(_ sender: AnyObject) {
        self.dateTimeView.isHidden = true
        self.dateTimeHeight.constant = 0.0
        self.anyPersonViewHeight.constant = 0.0
        self.anyView.isHidden = true
        self.updateViewHeight.constant = 120
    }
    @IBAction func anyPersonButtonClicked(_ sender: AnyObject) {
        self.dateTimeHeight.constant = 0.0
        self.anyPersonViewHeight.constant = 150
        self.anyView.isHidden = false
        self.dateTimeView.isHidden = true
        self.updateViewHeight.constant = 280
    }
    
    // The number of columns of data
    func numberOfComponentsInPickerView(_ pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let valueSelected = pickerData[row] as String
        
        if (valueSelected == "Only Male"){
            updateReqObj.pooltype  = "M"
        }else if (valueSelected == "Only Female"){
            updateReqObj.pooltype  = "F"
        }else{
            updateReqObj.pooltype  = "A"
        }
        
        self.anyPersonButton.setTitle(valueSelected, for: UIControlState())
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        let titleData = pickerData[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: FONT_NAME.BOLD_FONT, size:  14.0)!])
        pickerLabel.textAlignment = .center
        pickerLabel.attributedText = myTitle
        return pickerLabel
    }
    
    // MARK: -  Date / Time Picker View
    @IBAction func dateButtonlClicked(_ sender: AnyObject) {
        self.dateTimeView.isHidden = false
        self.dateTimeHeight.constant = 150.0
        self.anyPersonViewHeight.constant = 0.0
        self.anyView.isHidden = true
        self.updateViewHeight.constant = 280
        
        self.dateTimePickerView.datePickerMode = UIDatePickerMode.date
        self.dateTimePickerView.tag = 1
        
    }
    
    @IBAction func timeButtonClicked(_ sender: AnyObject) {
        self.dateTimeView.isHidden = false
        self.dateTimeHeight.constant = 150.0
        self.anyPersonViewHeight.constant = 0.0
        self.anyView.isHidden = true
        self.updateViewHeight.constant = 280
        
        self.dateTimePickerView.datePickerMode = UIDatePickerMode.time
        self.dateTimePickerView.tag = 2
        
    }
    @IBAction func dateTimeDoneButtonClicked(_ sender: AnyObject) {
        self.dateTimeView.isHidden = true
        self.dateTimeHeight.constant = 0.0
        self.anyPersonViewHeight.constant = 0.0
        self.anyView.isHidden = true
        self.updateViewHeight.constant = 120
        
        
    }
    
    @IBAction func dateTimePickerSelected(_ picker: UIDatePicker) {
        if picker.tag == 2 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            let strDate = dateFormatter.string(from: dateTimePickerView.date)
            updateReqObj.poolstarttime = strDate
            
            self.timeButton .setTitle(strDate, for: UIControlState())
            
        }else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM d"
            let strDate = dateFormatter.string(from: dateTimePickerView.date)
            updateReqObj.poolstartdate = prettyDateStringFromDate(dateTimePickerView.date, toFormat: "dd-MMM-yyyy")
            //   self.selectedDateTime = datePickerView.date
            self.dateButton .setTitle(strDate, for: UIControlState())
        }
    }
    
    // MARK: -  Google Map View Update
    
    func addOverlayToMapView(_ sourceLat: String,sourceLang: String,distinationLat: String,distinationLang: String ){
        self.mapRideView.clear()
        
        showIndicator("Loading Map path...")
        let sourceStr = "\(sourceLat),\(sourceLang)"
        let destinationStr = "\(distinationLat), \(distinationLang)"
        
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        //print(directionURL," directionURL --------- ")
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                //  print(response)
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    self.addmarkers(legs)
                    self.calculateTotalDistanceAndDuration(legs)
                    self.addPolyLineUpdateViewWithEncodedStringInMap(routes as NSArray)
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }
    
    func calculateTotalDistanceAndDuration(_ legs : NSArray) {
        
        totalDistanceInMeters = 0
        totalDurationInSeconds = 0
        
          //for leg in legs {
            let legsDict = legs[0] as! NSDictionary
        totalDistanceInMeters += (legsDict["distance"] as! NSDictionary)["value"] as! UInt
        totalDurationInSeconds += (legsDict["duration"] as! NSDictionary)["value"] as! UInt
        //  }
        
        let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
        totalDistance = "\(distanceInKilometers) Km"
        
        self.updateReqObj.distance = String(totalDistance)
        let mins = totalDurationInSeconds / 60
        self.updateReqObj.duration = "\(distanceInKilometers) mins"
        
        
        let hours = mins / 60
        let days = hours / 24
        let remainingHours = hours % 24
        let remainingMins = mins % 60
        let remainingSecs = totalDurationInSeconds % 60
        totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
    }
    
    // MARK:- Google Map Direction Api Call
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    // MARK:- Add markers
    func addmarkers(_ legs : NSArray) {
        let legsStartLocDict = legs[0] as! NSDictionary
        let startLocationDictionary = legsStartLocDict["start_location"] as! NSDictionary
        self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
        let legsEndLocDict = legs[legs.count - 1] as! NSDictionary
        let endLocationDictionary = legsEndLocDict["end_location"] as! NSDictionary
        self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
        
        //self.mapRideView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
        
        let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
        let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
        let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
        let camera = self.mapRideView.camera(for: bounds, insets: UIEdgeInsets())!
        self.mapRideView.camera = camera
        
        self.originMarker = GMSMarker(position: self.originCoordinate)
        self.originMarker.map = self.mapRideView
        self.originMarker.isTappable = false
        self.originMarker.icon = UIImage(named:"marker_home_green")// GMSMarker.markerImageWithColor(UIColor.greenColor())
        self.originMarker.tracksInfoWindowChanges = true
        self.originMarker.title = "source"
        
        self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
        self.destinationMarker.map = self.mapRideView
        self.destinationMarker.isTappable = false
        self.destinationMarker.tracksInfoWindowChanges = true
        self.destinationMarker.icon = UIImage(named:"marker_wp_green")
        self.destinationMarker.title = "destination"
    }
    
    
    func addPolyLineUpdateViewWithEncodedStringInMap(_ routes: NSArray) {
        
        for index in 0..<routes.count{
            let routesDicts = routes[index] as! NSDictionary
            let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
            let points = overview_polyline ["points"] as! NSString
            let path = GMSMutablePath(fromEncodedPath: points as String)
            let routePolylineObje = GMSPolyline(path: path)
            let summary = routesDicts ["summary"] as! NSString
            
            if index == 0 {
                self.routePolylineIndex0 = routePolylineObje
            }else if index == 1{
                self.routePolylineIndex1 = routePolylineObje
            }else{
                self.routePolylineIndex2 = routePolylineObje
                updateReqObj.via = summary as String
                updateReqObj.waypoints = points as String
            }
            
            if index  == routes.count-1 {
                routePolylineObje.strokeColor = Colors.POLILINE_GREEN
            }else{
                routePolylineObje.strokeColor = Colors.POLILINE_GRAY
            }
            
            routePolylineObje.strokeWidth = 5.0
            routePolylineObje.isTappable = true
            routePolylineObje.title = String(index)
            //        self.routePolyline.geodesic = true
            //self.routePolyline.spans = [GMSStyleSpan(color: Colors.RED_COLOR)]
            self.polylineMutArray? .add(points)
            self.viaMutArray? .add(summary)
            routePolylineObje.map = mapRideView
            
            let bounds = GMSCoordinateBounds(path: path!)
            mapRideView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
        }
    }
    
    @objc(mapView:didTapOverlay:) func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        let selectedPolyline : GMSPolyline = overlay as! GMSPolyline
        let selectedIndex = Int(selectedPolyline.title!)
        let polylinePath  = self.polylineMutArray?[selectedIndex!] as! String
        let viaStr  = self.viaMutArray?[selectedIndex!] as! String
        updateReqObj.via = viaStr
        updateReqObj.waypoints = polylinePath
        
        print(polylinePath, "polylinePath selected value.")
        for index in 0..<(self.polylineMutArray?.count)! {
            if index == 0 {
                self.routePolylineIndex0.strokeColor = Colors.POLILINE_GRAY;
            }
            if index == 1 {
                self.routePolylineIndex1.strokeColor = Colors.POLILINE_GRAY
            }
            if index == 2  {
                self.routePolylineIndex2.strokeColor = Colors.POLILINE_GRAY
            }
        }
        selectedPolyline.strokeColor = Colors.POLILINE_GREEN
    }
    
    // MARK:- Segue Actions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "wpVehicleCostSegue" {
            let vc = segue.destination as! WpVehicleCostViewController
            vc.isControlerType = "Update"
            vc.vehicleConfig = self.vehicleConfig
            vc.vehicleDefaultDetails = self.vehicleDefaultDetails as VehicleDefaultResponse
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext //All objects and view are transparent
        }
        
        if segue.identifier == "searchSegue" {
            let vc = segue.destination as! SearchModelViewController
            vc.isControlerType = "Update"
            vc.placeSearch = self.isSelectedLocation
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext //All objects and view are transparent
        }
    }
    
    
    // MARK:- Update Ride.
    @IBAction func selectedUpdateButton(_ sender: AnyObject) {
        self.updateRide()
    }
    
    
    func  defaultValueSet(){
        
        updateReqObj.from_lon = updateData?.sourceLong
        updateReqObj.from_lat = updateData?.sourceLat
        updateReqObj.from_location = updateData?.source
        updateReqObj.to_location = updateData?.destination
        updateReqObj.to_lat = updateData?.destinationLat
        updateReqObj.to_lon = updateData?.destinationLong
        
        updateReqObj.cost_type = updateData?.costType
        updateReqObj.poolCategory = updateData?.poolCategory
        updateReqObj.poolID = updateData?.poolID
        updateReqObj.poolShared = updateData?.poolShared
        updateReqObj.poolstartdate = updateData?.startDate
        updateReqObj.poolstarttime = updateData?.startTime
        updateReqObj.pooltype = updateData?.poolType
        updateReqObj.rideMessage = updateData?.message
        updateReqObj.vehregno = updateData?.vehicleRegNo
        updateReqObj.userID = updateData?.createdBy
        updateReqObj.email = updateData?.ownerEmail
        
        
        updateReqObj.cost = updateData?.cost
        updateReqObj.rideType = updateData?.rideType
        updateReqObj.seatLeft = updateData?.seatsLeft
        updateReqObj.rideMessage = String(self.commentTextField.text!)
    }
    
    func updateRide() {
        
        if self.commentTextField.text != nil {
            updateReqObj.rideMessage = String(self.commentTextField.text!)
        }else{
            updateReqObj.rideMessage = ""
        }
        
        if(updateReqObj.from_location != updateReqObj.to_location){
            showIndicator("Updating Ride..")
            RideRequestor().updateRide(self.updateReqObj, success: { (success, object) in hideIndicator()
                if(success == true){
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "kUpdateRideDetailNotification"), object: nil)
                    _ = self.navigationController?.popViewController(animated: true)
                    
                    
                }else{ AlertController.showToastForError((object as! BaseModel).message!)  }
                }, failure: { (error) in hideIndicator() })
        }
    }
    
    @IBAction func sourceButtonClicked(_ button: UIButton) {
        
        self.isSelectedButton = "Source"
        self.isSelectedLocation = button.currentTitle
        let location = CLLocationCoordinate2D(latitude: Double((self.updateData?.sourceLat)!)!, longitude: Double((self.updateData?.sourceLong)!)!)
        self.loadSearchController(location)
    }
    
    @IBAction func destinationButtonClicked(_ button: UIButton) {
        
        self.isSelectedButton = "destination"
        self.isSelectedLocation = button.currentTitle
        let location = CLLocationCoordinate2D(latitude: Double((self.updateData?.destinationLat)!)!, longitude: Double((self.updateData?.destinationLong)!)!)
        self.loadSearchController(location)
    }
    
    func loadSearchController(_ location : CLLocationCoordinate2D) {
        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
        let vc : SearchModelViewController = storyboard.instantiateViewController(withIdentifier: "SearchModelViewController") as! SearchModelViewController
        vc.isControlerType = "Update"
        vc.placeSearch = self.isSelectedLocation
        vc.locationCoord = location
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext //All objects and view are transparent
        self.present(vc, animated: true, completion: nil)
    }
    
    
    //MARK:- Update AlertView
    func updateAddressNotification (_ notification:Notification) -> Void {
        
        self.dismiss(animated: true, completion: nil)
        
        var homeAddress = ""
        guard let userInfo = notification.userInfo else { return }
        if let placeName = userInfo["HomeAddress"] as? String {
            homeAddress = placeName
            if self.isSelectedButton  == "Source" {
                self.sourceButton .setTitle(homeAddress, for: UIControlState())
                updateReqObj.from_location! = homeAddress
            }else{
                self.destinationButton .setTitle(homeAddress, for: UIControlState())
                updateReqObj.to_location! = homeAddress
            }
        }
        
        var latitude = ""
        if let lat = userInfo["latitude"] as? Double {
            latitude  = String(format:"%f", lat)
            if self.isSelectedButton  == "Source" {
                updateReqObj.from_lat = latitude
            }else{
                updateReqObj.to_lat = latitude
            }
        }
        
        var longitute = ""
        if let long = userInfo["longitude"] as? Double {
            longitute = String(format:"%f", long)
            if self.isSelectedButton  == "Source" {
                updateReqObj.from_lon = longitute
            }else{
                updateReqObj.to_lon = longitute
            }
        }
        
        if self.isSelectedButton  == "Source" {
            self.addOverlayToMapView(latitude,sourceLang:longitute,distinationLat: (updateData?.destinationLat)!,distinationLang: (updateData?.destinationLong)!)
        } else {
            self.addOverlayToMapView((updateData?.sourceLat)!,sourceLang: (updateData?.sourceLong)!,distinationLat: latitude,distinationLang:longitute)
        }
        
    }
    
    //MARK:- Cost AlertView
    func getUpdatePriceNotification (_ notification:Notification) -> Void {
        guard let userInfo = notification.userInfo else { return }
        // var cost = 0
        if let costValue = userInfo["cost"] as? Int {
            //   cost = costValue
            updateReqObj.cost = String(costValue)
            self.fareButton.setTitle(String(costValue) + " / km", for: UIControlState())
            self.fareButton.isUserInteractionEnabled = true
        }
        
        if let seat = userInfo["seat"] as? Int {
            updateReqObj.seatLeft = String(seat)
            self.seatbutton.setTitle(String(seat), for: UIControlState())
        }
    }
    
    func getConfigData()
    {
        showIndicator("Fetching vehicle config.")
        var groupId = ""
        if let wpId = wpUpdateData?.wpID {
            groupId = wpId
        }
        RideRequestor().getVehicleConfig("ride", groupID: groupId, success: { (success, object) in
            hideIndicator()
            if (success) {
                self.vehicleConfig = object as! VehicleConfigResponse
            }
        }) { (error) in
            hideIndicator()
        }
    }
}
