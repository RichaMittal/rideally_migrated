//
//  SearchResultViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 10/21/16.
//  Copyright © 2016 rideally. All rights reserved.
//
import UIKit
import AddressBook
import AddressBookUI

class SearchResultViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource,ABPeoplePickerNavigationControllerDelegate {
    
    let lblTitle = UILabel()
    let btnNew = UIButton()
    let table = UITableView()
    var onAddNewBlock: actionBlock?
    var taxiRide = ""
    var searchReqObj: SearchRideRequest?
    var ridePoints = 0
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    var localPoolID = ""
    var localPoolScope = ""
    var dataSource = [Ride](){
        didSet{
            if(dataSource.count > 0){
                table.isHidden = false
                view.removeEmptyView()
                //tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt)
                table.reloadData()
            }
            else{
                //tableHeightConstraint?.constant = 0
                showEmptyView("Oops. no rides found for you search. Please Create new ride.")
            }

            //table.reloadData()
        }
    }
    func showEmptyView(_ msg: String)
    {
        table.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "ic_empty_cart", actionHandler: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Existing Rides"
         NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "KInviteSendNotification"),object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotification"), object: nil)
        lblTitle.isUserInteractionEnabled = true
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font = normalFontWithSize(15)
        lblTitle.numberOfLines = 0
        lblTitle.textColor = Colors.GRAY_COLOR
        var nowText = ""
        if(taxiRide == "0") {
            nowText = "Add Ride"
        } else {
            nowText = "Book a new taxi"
        }
        let combinedString = "You may join below mentioned existing rides or \(nowText)" as NSString
        let range = combinedString.range(of: nowText)
        let attributedString = NSMutableAttributedString(string: combinedString as String)
        let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1] as [String : Any]
        attributedString.addAttributes(attributes, range: range)
        self.lblTitle.attributedText = attributedString
        let tapGesture = UITapGestureRecognizer(target:self, action:#selector(btnNew_clicked(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        lblTitle.addGestureRecognizer(tapGesture)
        self.view.addSubview(lblTitle)
        
        table.delegate = self
        table.dataSource = self
        table.separatorColor = UIColor.clear
        table.register(RideCell.self, forCellReuseIdentifier: "ridecell")
        table.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(table)
        
        let viewsDict = ["lbl":lblTitle, "table":table]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(40)]-5-[table]-5-|", options: [], metrics: nil, views: viewsDict))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[table]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: viewsDict))
    }
    
    func btnNew_clicked(_ sender:AnyObject)
    {
        _ = self.navigationController?.popViewController(animated: true)
        onAddNewBlock?()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ridecell") as! RideCell
        cell.data = dataSource[indexPath.row]
        cell.onUserActionBlock = { (selectedAction) -> Void in
            self.cell_btnAction(selectedAction as! String, indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row%2 == 0){
            cell.backgroundColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
        }
        else{
            cell.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showIndicator("Fetching Ride Details")
        if(self.dataSource[indexPath.row].poolTaxiBooking == "1") {
            RideRequestor().getTaxiDetails(dataSource[indexPath.row].poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    let vc = TaxiDetailViewController()
                    if let sourceID = self.dataSource[indexPath.row].sourceID{
                        vc.taxiSourceID = sourceID
                    }
                    if let destID = self.dataSource[indexPath.row].destID{
                        vc.taxiDestID = destID
                    }
                    vc.isFirstLevel = true
                    vc.onDeleteActionBlock = {
                        self.updateSearchResultList()
                    }
                    vc.hidesBottomBarWhenPushed = true
                    vc.data = object as? RideDetail
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }) { (error) in
                hideIndicator()
            }
        } else {
            RideRequestor().getRideDetails(dataSource[indexPath.row].poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                    let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                    //let vc = RideDetailViewController()
                    if let sourceID = self.dataSource[indexPath.row].sourceID{
                        vc.taxiSourceID = sourceID
                    }
                    if let destID = self.dataSource[indexPath.row].destID{
                        vc.taxiDestID = destID
                    }
                    vc.isFirstLevel = true
                    vc.onDeleteActionBlock = {
                        self.updateSearchResultList()
                    }
                    vc.onUpdateSearchResultBlock = {
                        self.updateSearchResultList()
                    }
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    vc.data = object as? RideDetail
                    if let poolScope = self.dataSource[indexPath.row].poolScope {
                        if poolScope == "PUBLIC" {
                            vc.triggerFrom = "ride"
                        } else {
                            vc.triggerFrom = "place"
                        }
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }) { (error) in
                hideIndicator()
            }
        }
    }
    
    func getPickUpDropLocation(_ triggerFrom: String)
    {
        let joinOrOfferRideObj = JoinOrOfferRide()
        let vc = RideDetailMapViewController()
        vc.sourceLong = selectedRide?.sourceLong
        vc.sourceLat = selectedRide?.sourceLat
        vc.sourceLocation = selectedRide?.source
        vc.destLong = selectedRide?.destLong
        vc.destLat = selectedRide?.destLat
        vc.destLocation = selectedRide?.destination
        vc.taxiSourceID = selectedRide.sourceID!
        vc.taxiDestID = selectedRide.destID!
        vc.poolTaxiBooking = selectedRide.poolTaxiBooking!
        vc.poolId = selectedRide.poolID!
        vc.rideType = selectedRide.poolRideType!
        vc.triggerFrom = triggerFrom
        vc.wpInsuranceStatus =  String(selectedRide.poolGroupInsStatus!)
        
        if let poolScope = selectedRide.poolScope {
            if poolScope == "PUBLIC" {
                vc.isWP = false
            } else {
                vc.isWP = true
                vc.wpInsuranceStatus = (WPDATA?.wpInsuranceStatus)!
            }
        }
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            if(self.selectedRide.poolTaxiBooking == "1") {
                showIndicator("Joining Taxi Ride..")
                RideRequestor().joinTaxiRide(self.selectedRide!.poolID!, userId: UserInfo.sharedInstance.userID, pickupPoint: pickUpLoc as? String, dropPoint: dropLoc as? String, comments: "",pCost: String(self.selectedRide.finalCost!), costMain: String(self.selectedRide.fullCost!), success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Join Taxi Ride", message: "You have successfully joined this ride.", okButtonTitle: "Ok", okAction: {
                            self.updateTaxiDetails()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Join Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            } else {
                joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
                joinOrOfferRideObj.poolID = self.selectedRide.poolID!
                joinOrOfferRideObj.vehicleRegNo = ""
                joinOrOfferRideObj.vehicleCapacity = ""
                joinOrOfferRideObj.cost = "0"
                joinOrOfferRideObj.rideMsg = ""
                joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
                joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
                joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
                joinOrOfferRideObj.dropPoint = dropLoc as? String
                joinOrOfferRideObj.dropPointLat = dropLat as? String
                joinOrOfferRideObj.dropPointLong = dropLong as? String
                
                if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                    showIndicator("Joining Ride..")
                    OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                                self.updateRideDetails()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    
                }
                else{
                    AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
                }
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateRideDetails()
    {
        showIndicator("Fetching Ride Details")
        RideRequestor().getRideDetails(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                //let vc = RideDetailViewController()
                vc.taxiSourceID = self.selectedRide.sourceID!
                vc.taxiDestID = self.selectedRide.destID!
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = object as? RideDetail
                if let poolScope = self.selectedRide.poolScope {
                    if poolScope == "PUBLIC" {
                        vc.triggerFrom = "ride"
                    } else {
                        vc.triggerFrom = "place"
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func updateTaxiDetails()
    {
        showIndicator("Fetching Ride Details")
        RideRequestor().getTaxiDetails(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let vc = TaxiDetailViewController()
                vc.taxiSourceID = self.selectedRide.sourceID!
                vc.taxiDestID = self.selectedRide.destID!
                vc.hidesBottomBarWhenPushed = true
                vc.data = object as? RideDetail
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func joinRide() {
        if(UserInfo.sharedInstance.shareMobileNumber == false){
            AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                self.getPickUpDropLocation("joinRide")
                }, cancelButtonTitle: "LATER", cancelAction: {
            })
        }
        else{
            getPickUpDropLocation("joinRide")
        }
    }
    
    func acceptRide() {
        AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Accepting ride request..")
            RideRequestor().acceptRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Accept Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateSearchResultList()
                    })
                }else{
                    AlertController.showAlertFor("Accept Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            contactListArray.removeAllObjects()
            mobileMutArr.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject]{
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = localPoolID
            //vc.triggerFrom = "allRides"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    self.contactListArray.removeAllObjects()
                    self.mobileMutArr.removeAllObjects()
                    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                    for record:ABRecord in contactList as [AnyObject]{
                        let contactPerson: ABRecord = record
                        let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                        if(contactName != "") {
                            var personNumber = ""
                            let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                            if(phones != "")
                            {
                                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                {
                                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                    {
                                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                        personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                        let areaCode = personNumber.characters.count-10
                                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                        if(areaCode > 0) {
                                            personNumber = personNumber.substring(from: startIndex)
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            self.mobileMutArr.add(personNumber)
                            let nameString = "\(contactName) \(personNumber)"
                            self.contactListArray.add(nameString)
                        }
                    }
                    
                    let vc = ContactsListViewController()
                    vc.contactListArr = self.contactListArray
                    vc.mobileMutArr = self.mobileMutArr
                    vc.poolID = self.localPoolID
                    //vc.triggerFrom = "allRides"
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        //        AlertController.showAlertFor("Cannot Add Contact", message: "You must give the app permission to add the contact first.", okButtonTitle: "Settings", okAction: {
        //            self.openSettings()
        //            }, cancelButtonTitle: "OK", cancelAction: nil)
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObject()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg("", wpKey: "", poolID: localPoolID)
    }
    
    func grpMsg_clicked() {
        self.dismiss(animated: true, completion: nil)
        showIndicator("Fetching members.")
        WorkplaceRequestor().getWPMembers("", groupID: localPoolScope, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = InviteGroupMembersViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.wpDetails = (object as! WPMembersResponseData).groupDetails
            vc.membersList = (object as! WPMembersResponseData).members
            self.navigationController?.pushViewController(vc, animated: true)
        }, failure: { (error) in
            hideIndicator()
        })
    }

    var selectedRide: Ride!
    func cell_btnAction(_ action: String, indexPath: IndexPath)
    {
        selectedRide = dataSource[indexPath.row]
        //invite, joinride, cancel, reject, accept, offeravehicle
        if(action == "invite"){
            localPoolID = self.selectedRide.poolID!
            if let poolScope = self.selectedRide.poolScope {
                if poolScope == "PUBLIC" {
                    let inviteView = InviteNormalRideView()
                    let alertViewHolder =   AlertContentViewHolder()
                    alertViewHolder.heightConstraintValue = 95
                    alertViewHolder.view = inviteView
                    inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
                    inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
                    
                    AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
                } else {
                    localPoolScope = poolScope
                    let inviteView = InviteWPRideView()
                    let alertViewHolder =   AlertContentViewHolder()
                    alertViewHolder.heightConstraintValue = 95
                    alertViewHolder.view = inviteView
                    inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
                    inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
                    inviteView.groupMsgBtn.addTarget(self, action: #selector(grpMsg_clicked), for: .touchDown)
                    
                    AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
                }
            }
        } else if(action == "joinride"){
            if(selectedRide.poolTaxiBooking != "1" && selectedRide.cost != "0" && selectedRide.distance != "" && selectedRide.distance != "0") {
                ridePoints = Int(selectedRide.cost!)! * Int(round(Double(selectedRide.distance!)!))
                if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                    if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                        AlertController.showAlertFor("Join Ride", message: "\("SORRY! You cannot join this ride because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and join this ride.")", okButtonTitle: "RECHARGE", okAction: {
                            let vc = MyAccountViewController()
                            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                            }, cancelButtonTitle: "CANCEL", cancelAction: {})
                    } else {
                        joinRide()
                    }
                } else {
                    joinRide()
                }
            } else {
                joinRide()
            }
        } else if(action == "cancel"){
            if(selectedRide.poolTaxiBooking == "1"){
                //cancel taxi ride
                AlertController.showAlertFor("Cancel Taxi Ride", message: "Would you really like to cancel this taxi ride?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Cancelling Ride.")
                    RideRequestor().cancelTaxiRide(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateSearchResultList()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
            else{
                //cancel
                AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel this ride?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Cancelling Ride.")
                    //from memberslist - FR
                    //from ridedetail & ridelist = JR
                    RideRequestor().cancelRequest(self.selectedRide!.poolID!, userId: UserInfo.sharedInstance.userID, cancelrequestType: "JR", success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateSearchResultList()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
        }
        else if(action == "accept"){
            if(selectedRide.poolTaxiBooking == "1"){
                AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this invited ride request?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Accepting ride request..")
                    let youpay = self.selectedRide.finalCost ?? 4
                    let fullcost = self.selectedRide.fullCost ?? 4
                    RideRequestor().acceptTaxiRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, pCost: String(youpay), costMain: String(fullcost), success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Accept Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateSearchResultList()
                            })
                        }else{
                            AlertController.showAlertFor("Accept Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }, cancelButtonTitle: "Cancel") {
                }
            } else {
                if(selectedRide.poolTaxiBooking != "1" && selectedRide.cost != "0" && selectedRide.distance != "" && selectedRide.distance != "0") {
                    ridePoints = Int(selectedRide.cost!)! * Int(round(Double(selectedRide.distance!)!))
                    if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                        if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                            AlertController.showAlertFor("Accept Ride", message: "\("SORRY! You cannot accept this ride request because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and accept this ride request.")", okButtonTitle: "RECHARGE", okAction: {
                                let vc = MyAccountViewController()
                                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                                }, cancelButtonTitle: "CANCEL", cancelAction: {})
                        } else {
                            acceptRide()
                        }
                    } else {
                        acceptRide()
                    }
                } else {
                    acceptRide()
                }
            }
        }
        else if(action == "reject"){
            if(selectedRide.poolTaxiBooking == "1"){
                AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Rejecting ride request..")
                    RideRequestor().rejectTaxiRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Reject Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateSearchResultList()
                            })
                        }else{
                            AlertController.showAlertFor("Reject Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }, cancelButtonTitle: "Cancel") {
                }
                
            } else {
                AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Rejecting ride request..")
                    RideRequestor().rejectRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Reject Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateSearchResultList()
                            })
                        }else{
                            AlertController.showAlertFor("Reject Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }, cancelButtonTitle: "Cancel") {
                }
            }
        }
        else if(action == "offeravehicle"){
            self.getPickUpDropLocation("offerVehicle")
        }
        else if(action == "showmembers"){
            showIndicator("Getting members list..")
            RideRequestor().getRideMembers(dataSource[indexPath.row].poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    let vc = MembersViewController()
                    vc.hidesBottomBarWhenPushed = true
                    vc.poolID = self.selectedRide.poolID
                    vc.membersList = (object as! RideMembersResponse).poolUsers
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }
        else if(action == "unjoinride"){
            AlertController.showAlertFor("Unjoin Ride", message: "Would you really like to unjoin from this ride?", okButtonTitle: "YES", okAction: {
                showIndicator("Unjoining the ride..")
                RideRequestor().unJoinRide(self.dataSource[indexPath.row].poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Unjoin Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateSearchResultList()
                        })
                    }else{
                        AlertController.showAlertFor("Unjoin Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "LATER") {
            }
        }
    }
    
    func updateSearchResultList() {
        showIndicator("Updating.")
        RideRequestor().searchRides(self.searchReqObj!, success: { (success, object) in
            hideIndicator()
            if(success){
                    self.dataSource = (object as! AllRidesResponse).pools!
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
      self.updateSearchResultList()
    }
}
