//
//  WPRidesViewController.swift
//  rideally
//
//  Created by Sarav on 31/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class WPRideCell: RideCell {
    
}

class WPRidesViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource,ABPeoplePickerNavigationControllerDelegate {
    
    var data: WorkPlace?
    let segment = HMSegmentedControl()
    let tableView = UITableView()
    var dataSourceToOfc: [WPRide]?
    var dataSourceToHome: [WPRide]?
    var onSelectBackBlock: actionBlock?
    var onCompletion: actionBlock?
    var ridePoints = 0
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    var localPoolID = ""
    var localPoolScope = ""
    
    override func hasTabBarItems() -> Bool
    {
        ISWPRIDESSCREEN = true
        ISWPMAPSCREEN = false
        WPDATA = data
        DATASOURCETOOFC = dataSourceToOfc
        DATASOURCETOHOME = dataSourceToHome
        return true
    }
    
    override func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
        onSelectBackBlock?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.APP_BG
        title = "Rides"
        if(UserInfo.sharedInstance.availablePoints == 0 || UserInfo.sharedInstance.maxAllowedPoints == "") {
            getUserPoints()
        }
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
         NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "KInviteSendNotificationWpRideDetail"),object: nil )
         NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotificationWpRideDetail"), object: nil)
        addViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(ISSEARCHRESULT) {
            self.dataSourceToOfc = SEARCHSOURCETOOFC
            self.dataSourceToHome = SEARCHSOURCETOHOME
            if dataSourceToOfc?.count>0 {
                segment.selectedSegmentIndex = 0
            } else {
                segment.selectedSegmentIndex = 1
            }
        } else {
            self.dataSourceToOfc = DATASOURCETOOFC
            self.dataSourceToHome = DATASOURCETOHOME
        }
        self.tableView.reloadData()
    }
    
    func addViews()
    {
        let rideView = getRideView()
        self.view.addSubview(rideView)
        
        segment.sectionTitles = ["Home to Work", "Work to Home"]
        segment.backgroundColor = UIColor.clear
        segment.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.GREEN_COLOR]
        segment.selectedTitleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
        segment.selectionStyle = HMSegmentedControlSelectionStyleBox
        segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone
        segment.selectionIndicatorColor = Colors.GREEN_COLOR
        segment.selectionIndicatorBoxOpacity = 1.0
        segment.translatesAutoresizingMaskIntoConstraints = false
        segment.segmentEdgeInset = UIEdgeInsets.zero
        segment.selectedSegmentIndex = 0
        segment.layer.borderColor = Colors.GREEN_COLOR.cgColor
        segment.layer.borderWidth = 1
        segment.addTarget(self, action: #selector(rideTypeChanged), for: .valueChanged)
        view.addSubview(segment)
        
        tableView.separatorColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.register(WPRideCell.self, forCellReuseIdentifier: "WPRideCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        let viewsDict = ["segment": segment, "rideV":rideView, "table":tableView]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[rideV(80)]-10-[segment(40)]-10-[table]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[rideV]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[segment]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[table]-5-|", options: [], metrics: nil, views: viewsDict))
    }
    
    func rideTypeChanged()
    {
        if(segment.selectedSegmentIndex == 0){
        }
        else{
        }
        tableView.reloadData()
    }
    
    func removeEmptyView()
    {
        tableView.isHidden = false
        view.removeEmptyView()
    }
    
    func showEmptyView(_ msg: String)
    {
        tableView.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "ic_empty_cart", actionHandler: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowsCnt = 0
        if(tableView == self.tableView) {
        if(segment.selectedSegmentIndex == 0){
            if let list = dataSourceToOfc{
                rowsCnt = list.count
            }
        }
        else{
            if let list = dataSourceToHome{
                rowsCnt = list.count
            }
        }
        if(rowsCnt == 0){
            showEmptyView("No rides found.")
        }else{
            removeEmptyView()
        }
        }
        return rowsCnt
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 120
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "WPRideCell") as! WPRideCell
        if(segment.selectedSegmentIndex == 0){
            cell.data = dataSourceToOfc![indexPath.row]
        }
        else{
            cell.data = dataSourceToHome![indexPath.row]
        }
        cell.onUserActionBlock = { (selectedAction) -> Void in
            self.cell_btnAction(selectedAction as! String, indexPath: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showIndicator("Fetching Ride Details")
        var poolId: String = ""
        if(segment.selectedSegmentIndex == 0){
            poolId = dataSourceToOfc![indexPath.row].poolID!
        }
        else{
            poolId = dataSourceToHome![indexPath.row].poolID!
        }
        
        RideRequestor().getRideDetails(poolId, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                //let vc = RideDetailViewController()
                vc.onDeleteActionBlock = {
                    self.updateRidesList()
                }
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = object as? RideDetail
                vc.wpData = self.data
                vc.isFirstLevel = true
                vc.triggerFrom = "place"
                vc.inviteFlag = "wpMap"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
            if(indexPath.row%2 == 0){
                cell.backgroundColor = Colors.EVEN_ROW_COLOR
            }
            else{
                cell.backgroundColor = Colors.ODD_ROW_COLOR
            }
    }
    
    var isStartLocWP = false
    let lblTitle = UILabel()
    let lblLoc1 = UILabel()
    let btnSwap = UIButton()
    let lblLoc2 = UILabel()
    let txtTime = RATextField()
    let btnDetail = UIButton()
    let btnCreate = UIButton()
    let timePicker = UIDatePicker()
    let btnAdd = UIButton()
    let wpIcon = UIImageView()
    
    func showPlaceholderPic()
    {
        if(data!.wpScope == "1" || data!.wpTieUpStatus == "1"){
            wpIcon.image = UIImage(named: "wpPlaceholderL")
            //TODO: show lock
        }
        else{
            wpIcon.image = UIImage(named: "wpPlaceholder")
        }
        let url = URL(string:SERVER+"static/groups/"+(data?.wpID)!+".jpg")
        wpIcon.kf.setImage(with: url, placeholder: wpIcon.image, options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func getRideView() -> UIView
    {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let view1 = UIView()
        view1.backgroundColor = Colors.EVEN_ROW_COLOR
        view1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(view1)
        
        wpIcon.translatesAutoresizingMaskIntoConstraints = false
        wpIcon.backgroundColor = UIColor.clear
        wpIcon.layer.cornerRadius = 15
        wpIcon.layer.masksToBounds = true
        wpIcon.isUserInteractionEnabled = true
        
        if let url = data?.wpLogoURL{
            if(url != ""){
                wpIcon.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                if(data!.wpScope == "1" || data!.wpTieUpStatus == "1"){
                    //TODO: SHOWlock
                }
            }
            else{
                showPlaceholderPic()
            }
        }
        else{
            showPlaceholderPic()
        }
        view1.addSubview(wpIcon)
        
        let lblWPName = UILabel()
        lblWPName.translatesAutoresizingMaskIntoConstraints = false
        lblWPName.text = data!.wpName
        lblWPName.font = normalFontWithSize(14)
        view1.addSubview(lblWPName)
        
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[icon(30)]-5-[lbl]-5-|", options: [], metrics: nil, views: ["icon":wpIcon, "lbl":lblWPName]))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[icon(30)]-5-|", options: [], metrics: nil, views: ["icon":wpIcon, "lbl":lblWPName]))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(30)]-5-|", options: [], metrics: nil, views: ["icon":wpIcon, "lbl":lblWPName]))
        
        
        let tapWP = UITapGestureRecognizer(target: self, action: #selector(showWPDetail))
        view1.addGestureRecognizer(tapWP)
        
        
        let view2 = UIView()
        view2.backgroundColor = Colors.ODD_ROW_COLOR
        view2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(view2)
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblLoc1.translatesAutoresizingMaskIntoConstraints = false
        lblLoc2.translatesAutoresizingMaskIntoConstraints = false
        btnSwap.translatesAutoresizingMaskIntoConstraints = false
        txtTime.translatesAutoresizingMaskIntoConstraints = false
        btnDetail.translatesAutoresizingMaskIntoConstraints = false
        btnCreate.translatesAutoresizingMaskIntoConstraints = false
        
        view2.addSubview(lblTitle)
        view2.addSubview(lblLoc1)
        view2.addSubview(btnSwap)
        view2.addSubview(lblLoc2)
        view2.addSubview(txtTime)
        view2.addSubview(btnDetail)
        view2.addSubview(btnCreate)
        
        lblTitle.text = "Time from"
        lblTitle.font = boldFontWithSize(13)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.textAlignment = .center
        lblLoc1.text = "Home"
        lblLoc1.font = boldFontWithSize(13)
        lblLoc1.textColor = Colors.GRAY_COLOR
        lblLoc1.textAlignment = .center
        lblLoc2.text = "Work"
        lblLoc2.font = boldFontWithSize(13)
        lblLoc2.textColor = Colors.GRAY_COLOR
        lblLoc2.textAlignment = .center
        
        txtTime.backgroundColor = Colors.WHITE_COLOR
        txtTime.font = boldFontWithSize(13)
        txtTime.text = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "NL")
        txtTime.inputView = timePicker
        timePicker.date = Date().addingTimeInterval(300)
        timePicker.addTarget(self, action: #selector(timePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: 40))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        btnAdd.setImage(UIImage(named: "wpdelete"), for: UIControlState())
        
        btnSwap.setImage(UIImage(named: "hflip"), for: UIControlState())
        btnSwap.addTarget(self, action: #selector(swapLocations), for: .touchDown)
        
        btnDetail.setImage(UIImage(named: "wpsummary"), for: UIControlState())
        btnDetail.addTarget(self, action: #selector(showRideCreationVC), for: .touchDown)
        
        btnCreate.setImage(UIImage(named: "wpcreate"), for: UIControlState())
        btnCreate.addTarget(self, action: #selector(proceed), for: .touchDown)
        
        let viewsDict = ["title":lblTitle, "loc1":lblLoc1, "swap":btnSwap, "loc2":lblLoc2, "time":txtTime, "details":btnDetail, "create":btnCreate]
        
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title(60)][loc1(40)][swap(30)][loc2(40)]-5-[time(50)]-10-[details(30)]-10-[create(30)]", options: [], metrics: nil, views: viewsDict))
        
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title]-5-|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[loc1]-5-|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[swap]-5-|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[loc2]-5-|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[time]-5-|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[details]-5-|", options: [], metrics: nil, views: viewsDict))
        view2.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[create]-5-|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v1(40)][v2(40)]|", options: [], metrics: nil, views: ["v1":view1, "v2":view2]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v1]|", options: [], metrics: nil, views: ["v1":view1, "v2":view2]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v2]|", options: [], metrics: nil, views: ["v1":view1, "v2":view2]))
        
        return view
    }
    
    func showWPDetail()
    {
//        let vc = WPDetailViewController()
//        vc.hidesBottomBarWhenPushed = true
//        vc.edgesForExtendedLayout = .None
//        vc.data = data
//        self.navigationController?.pushViewController(vc, animated: true)
        
        showIndicator("Fetching Workplace Detail.")
        WorkplaceRequestor().getWPMembers("", groupID: data!.wpID!, distanceRange: "1", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = WPDetailViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.data = (object as! WPMembersResponseData).groupDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }) { (error) in
            hideIndicator()
        }
    }
    
    func updateRidesList()
    {
        showIndicator("Fetching Rides.")
        var groupId = ""
        if let wpId = data?.wpID {
            groupId = wpId
        } else {
            groupId = selectedRide.poolScope!
        }
        WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: groupId, success: { (success, object) in
            hideIndicator()
            self.dataSourceToOfc = (object as! WPRidesResponse).pools?.officeGoing
            self.dataSourceToHome = (object as! WPRidesResponse).pools?.homeGoing
            self.tableView.reloadData()
        }) { (error) in
            hideIndicator()
        }
    }
    
    func updateRideDetails()
    {
        showIndicator("Fetching Ride Details")
        RideRequestor().getRideDetails(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                //let vc = RideDetailViewController()
                vc.taxiSourceID = self.selectedRide.sourceID!
                vc.taxiDestID = self.selectedRide.destID!
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = object as? RideDetail
                vc.triggerFrom = "place"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    var selectedRide: Ride!
    func getSelectedRide(_ indexPath: IndexPath) -> Ride
    {
        if(segment.selectedSegmentIndex == 0){
            return dataSourceToOfc![indexPath.row]
        }
        else{
            return dataSourceToHome![indexPath.row]
        }
        
    }
    
    func joinRide() {
        if(UserInfo.sharedInstance.shareMobileNumber == false){
            AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                self.getPickUpDropLocation("joinRide")
                }, cancelButtonTitle: "LATER", cancelAction: {
            })
        }
        else{
            getPickUpDropLocation("joinRide")
        }
    }
    
    func acceptRide() {
        AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Accepting ride request..")
            RideRequestor().acceptRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, userType: "friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Accept Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateRidesList()
                    })
                }else{
                    AlertController.showAlertFor("Accept Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            contactListArray.removeAllObjects()
            mobileMutArr.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject]{
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = localPoolID
            vc.triggerFrom = "wpRide"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    self.contactListArray.removeAllObjects()
                    self.mobileMutArr.removeAllObjects()
                    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                    for record:ABRecord in contactList as [AnyObject]{
                        let contactPerson: ABRecord = record
                        let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                        if(contactName != "") {
                            var personNumber = ""
                            let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                            if(phones != "")
                            {
                                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                {
                                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                    {
                                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                        personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                        let areaCode = personNumber.characters.count-10
                                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                        if(areaCode > 0) {
                                            personNumber = personNumber.substring(from: startIndex)
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            self.mobileMutArr.add(personNumber)
                            let nameString = "\(contactName) \(personNumber)"
                            self.contactListArray.add(nameString)
                        }
                    }
                    
                    let vc = ContactsListViewController()
                    vc.contactListArr = self.contactListArray
                    vc.mobileMutArr = self.mobileMutArr
                    vc.poolID = self.localPoolID
                    vc.triggerFrom = "wpRide"
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        //        AlertController.showAlertFor("Cannot Add Contact", message: "You must give the app permission to add the contact first.", okButtonTitle: "Settings", okAction: {
        //            self.openSettings()
        //            }, cancelButtonTitle: "OK", cancelAction: nil)
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObject()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg("", wpKey: "", poolID: localPoolID)
    }
    
    func grpMsg_clicked() {
        self.dismiss(animated: true, completion: nil)
        showIndicator("Fetching members.")
        WorkplaceRequestor().getWPMembers("", groupID: localPoolScope, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = InviteGroupMembersViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.wpDetails = (object as! WPMembersResponseData).groupDetails
            vc.membersList = (object as! WPMembersResponseData).members
            self.navigationController?.pushViewController(vc, animated: true)
        }, failure: { (error) in
            hideIndicator()
        })
    }

    func cell_btnAction(_ action: String, indexPath: IndexPath)
    {
        selectedRide = getSelectedRide(indexPath)
        
        //invite, joinride, cancel, reject, accept, offeravehicle
        if(action == "invite"){
            localPoolID = self.selectedRide.poolID!
            localPoolScope = self.selectedRide.poolScope!
            
            let inviteView = InviteWPRideView()
            let alertViewHolder =   AlertContentViewHolder()
            alertViewHolder.heightConstraintValue = 95
            alertViewHolder.view = inviteView
            
            inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
            inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
            inviteView.groupMsgBtn.addTarget(self, action: #selector(grpMsg_clicked), for: .touchDown)
            
            AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
        }
        else if(action == "joinride"){
            if(selectedRide.poolTaxiBooking != "1" && selectedRide.cost != "0" && selectedRide.distance != "" && selectedRide.distance != "0") {
                ridePoints = Int(selectedRide.cost!)! * Int(round(Double(selectedRide.distance!)!))
                if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                    if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                        AlertController.showAlertFor("Join Ride", message: "\("SORRY! You cannot join this ride because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and join this ride.")", okButtonTitle: "RECHARGE", okAction: {
                            let vc = MyAccountViewController()
                            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                            }, cancelButtonTitle: "CANCEL", cancelAction: {})
                    } else {
                        joinRide()
                    }
                } else {
                    joinRide()
                }
            } else {
                joinRide()
            }
        }
        else if(action == "cancel"){
            if(selectedRide.poolTaxiBooking == "1"){
                //cancel taxi ride
                AlertController.showAlertFor("Cancel Taxi Ride", message: "Would you really like to cancel this taxi ride?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Cancelling Taxi Ride..")
                    RideRequestor().cancelTaxiRide(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateRidesList()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
            else{
                //cancel
                AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel this ride?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Cancelling Ride Request..")
                    //from memberslist - FR
                    //from ridedetail & ridelist = JR
                    RideRequestor().cancelRequest(self.selectedRide!.poolID!, userId: UserInfo.sharedInstance.userID, cancelrequestType: "JR", success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateRidesList()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
        }
        else if(action == "accept"){
            if(selectedRide.poolTaxiBooking != "1" && selectedRide.cost != "0" && selectedRide.distance != "" && selectedRide.distance != "0") {
                ridePoints = Int(selectedRide.cost!)! * Int(round(Double(selectedRide.distance!)!))
                if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                    if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                        AlertController.showAlertFor("Accept Ride", message: "\("SORRY! You cannot accept this ride request because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and accept this ride request.")", okButtonTitle: "RECHARGE", okAction: {
                            let vc = MyAccountViewController()
                            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                            }, cancelButtonTitle: "CANCEL", cancelAction: {})
                    } else {
                        acceptRide()
                    }
                } else {
                    acceptRide()
                }
            } else {
                acceptRide()
            }
        }
        else if(action == "reject"){
            AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
                showIndicator("Rejecting ride request..")
                RideRequestor().rejectRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, userType: "friend", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Reject Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateRidesList()
                        })
                    }else{
                        AlertController.showAlertFor("Reject Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "Cancel") {
            }
        }
        else if(action == "offeravehicle"){
            self.getPickUpDropLocation("offerVehicle")
        }
        else if(action == "showmembers"){
            showIndicator("Getting members list..")
            RideRequestor().getRideMembers(selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    let vc = MembersViewController()
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    vc.poolID = self.selectedRide.poolID
                    vc.membersList = (object as! RideMembersResponse).poolUsers
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }
        else if(action == "unjoinride"){
            AlertController.showAlertFor("Unjoin Ride", message: "Would you really like to unjoin from this ride?", okButtonTitle: "YES", okAction: {
                showIndicator("Unjoining the ride..")
                RideRequestor().unJoinRide(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Unjoin Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateRidesList()
                        })
                    }else{
                        AlertController.showAlertFor("Unjoin Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "LATER") {
            }
        }
    }
    
    func getPickUpDropLocation(_ triggerFrom: String)
    {
        let joinOrOfferRideObj = JoinOrOfferRide()
        let vc = RideDetailMapViewController()
        vc.sourceLong = selectedRide.sourceLong
        vc.sourceLat = selectedRide.sourceLat
        vc.sourceLocation = selectedRide.source
        vc.destLong = selectedRide.destLong
        vc.destLat = selectedRide.destLat
        vc.destLocation = selectedRide.destination
        vc.taxiSourceID = selectedRide.sourceID!
        vc.taxiDestID = selectedRide.destID!
        vc.poolTaxiBooking = selectedRide.poolTaxiBooking!
        vc.poolId = selectedRide.poolID!
        vc.rideType = selectedRide.poolRideType!
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.hidesBottomBarWhenPushed = true
        vc.triggerFrom = triggerFrom
        vc.isWP = true
        vc.wpInsuranceStatus = (data?.wpInsuranceStatus)!
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
            joinOrOfferRideObj.poolID = self.selectedRide.poolID!
            joinOrOfferRideObj.vehicleRegNo = ""
            joinOrOfferRideObj.vehicleCapacity = ""
            joinOrOfferRideObj.cost = "0"
            joinOrOfferRideObj.rideMsg = ""
            joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
            joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
            joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
            joinOrOfferRideObj.dropPoint = dropLoc as? String
            joinOrOfferRideObj.dropPointLat = dropLat as? String
            joinOrOfferRideObj.dropPointLong = dropLong as? String
            
            if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                showIndicator("Joining Ride..")
                OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                            self.updateRideDetails()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
                
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Workplaces.")
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showRideCreationVC()
    {
        let vc = WPCreateRideViewController()
        vc.isStartLocWP = isStartLocWP
        vc.wpData = data
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func swapLocations()
    {
        if(isStartLocWP){
            isStartLocWP = false
        }
        else{
            isStartLocWP = true
        }
        let txt = lblLoc2.text
        lblLoc2.text = lblLoc1.text
        lblLoc1.text = txt
    }
    
    var newRideReqObj = WPNewRideRequest()
    func proceed()
    {
        if(UserInfo.sharedInstance.homeAddress != ""){
            if(txtTime.text?.characters.count <= 0){
                AlertController.showAlertFor("Create Ride", message: "Please choose a time for your travel.", okButtonTitle: "Ok", okAction: nil)
                return
            }
            newRideReqObj.userId = UserInfo.sharedInstance.userID
            newRideReqObj.selected_dates = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
            //newRideReqObj.comingtime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            //newRideReqObj.poolType = UserInfo.sharedInstance.gender
            newRideReqObj.pool_scope = data!.wpID
            if(data?.wpScope == "1") {
                newRideReqObj.pool_shared = "0"
            } else {
                newRideReqObj.pool_shared = "1"
            }
            
            newRideReqObj.fromLocation = data!.wpLocation != nil ? data!.wpLocation : data!.wpDetailLocation
            newRideReqObj.fromLat = data!.wpLat != nil ? data!.wpLat : data!.wpDetailLat
            newRideReqObj.fromLong = data!.wpLong != nil ? data!.wpLong : data!.wpDetailLong
            newRideReqObj.toLat = UserInfo.sharedInstance.homeLatValue
            newRideReqObj.toLong = UserInfo.sharedInstance.homeLongValue
            newRideReqObj.toLocation = UserInfo.sharedInstance.homeAddress
            if(isStartLocWP){
                
                newRideReqObj.comingtime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
                newRideReqObj.goingtime = ""
            }
            else{
                newRideReqObj.goingtime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
                newRideReqObj.comingtime = ""
            }
            
            //self.newRideReqObj.ridetype = "N"
            
            WorkplaceRequestor().getLastRide({ (success, object) in
                let lastRideObj = (object as! LastRideResponse).data?.first
                if(success == true){
                    self.newRideReqObj.ridetype = (lastRideObj?.rideType)!
                    self.newRideReqObj.poolType = lastRideObj?.poolType
                    if let vehRegNo = lastRideObj?.vehRegNo {
                        self.newRideReqObj.vehregno = vehRegNo
                        UserInfo.sharedInstance.vehicleSeqID = (lastRideObj?.vehSeqId)!
                        UserInfo.sharedInstance.vehicleType = (lastRideObj?.vehType)!
                        UserInfo.sharedInstance.vehicleModel = (lastRideObj?.vehModel)!
                        UserInfo.sharedInstance.vehicleRegNo = (lastRideObj?.vehRegNo)!
                        UserInfo.sharedInstance.vehicleSeatCapacity = (lastRideObj?.capacity)!
                        UserInfo.sharedInstance.vehicleKind = (lastRideObj?.vehKind)!
                        UserInfo.sharedInstance.vehicleOwnerName = (lastRideObj?.vehOwnerName)!
                        UserInfo.sharedInstance.insurerName = (lastRideObj?.insurerName)!
                        UserInfo.sharedInstance.insuredCmpnyName = (lastRideObj?.insuredCmpnyName)!
                        UserInfo.sharedInstance.insuranceId = (lastRideObj?.insuranceId)!
                        UserInfo.sharedInstance.insuranceExpiryDate = (lastRideObj?.insuranceExpiryDate)!
                        UserInfo.sharedInstance.insUploadStatus = (lastRideObj?.insuranceUploadStatus)!
                        UserInfo.sharedInstance.insUrl = (lastRideObj?.insuranceUrl)!
                        UserInfo.sharedInstance.rcUploadStatus = (lastRideObj?.rcUploadStatus)!
                        UserInfo.sharedInstance.rcUrl = (lastRideObj?.rcUrl)!
                    }
                    if let capacity = lastRideObj?.capacity {
                        self.newRideReqObj.veh_capacity = String(Int(capacity)!-1)
                    }
                    self.newRideReqObj.cost = lastRideObj?.cost
                    self.newRideReqObj.cost_type = (lastRideObj?.costType)!
                    self.checkInsurance("1")
                } else {
                    self.checkInsurance("2")
                }
                }, failure: { (error) in
                    self.checkInsurance("1")
            })
        }
        else{
            self.checkInsurance("2")
        }
    }
    
    func checkInsurance(_ callMethod: String) {
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.string(from: todayDate) as String
        if(newRideReqObj.vehregno != "" && newRideReqObj.vehregno != nil && data?.wpInsuranceStatus == "1" && (UserInfo.sharedInstance.insuranceId == "")) {
            AlertController.showAlertFor("Create Ride", message: "Please provide vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                self.updateVehicle()
            })
            //return
        } else if(newRideReqObj.vehregno != "" && newRideReqObj.vehregno != nil && data?.wpInsuranceStatus == "1" && UserInfo.sharedInstance.insuranceId != ""  && UserInfo.sharedInstance.insuranceExpiryDate.compare(date) == ComparisonResult.orderedAscending) {
            AlertController.showAlertFor("Create Ride", message: "Please update vehicle insurance details to proceed futher.", okButtonTitle: "Ok", okAction: {
                self.updateVehicle()
            })
            //return
        } else {
            if(callMethod == "1") {
                self.getDistanceDuration()
            } else {
                showRideCreationVC()
            }
        }
    }

    func updateVehicle() {
        let vc = RegisterNewVehicleController()
        vc.isUpdateReqneedtobeSend = "1"
        vc.isSeatCapacityNeedToBeUpdated = "1"
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        if(data?.wpInsuranceStatus != "" && data?.wpInsuranceStatus != nil && data?.wpInsuranceStatus == "1") {
            vc.isWpInsuranceStatus = true
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getDistanceDuration()
    {
        CommonRequestor().getDistanceDuration(self.newRideReqObj.fromLocation!, destination: self.newRideReqObj.toLocation!, success: { (success, object) in
            self.newRideReqObj.duration = ""
            self.newRideReqObj.distance = ""
            if let data = object as? NSDictionary{
                let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                if((elements["status"] as! String) == "OK")
                {
                    let distance = ((elements["distance"] as! NSDictionary)["text"])!
                    let duration = ((elements["duration"] as! NSDictionary)["text"])!
                    self.newRideReqObj.duration = duration as! String
                    self.newRideReqObj.distance = distance as! String
                }
            }
            let sourceStr = "\(self.newRideReqObj.fromLat!),\(self.newRideReqObj.fromLong!)"
            let destinationStr = "\(self.newRideReqObj.toLat!), \(self.newRideReqObj.toLong!)"
            CommonRequestor().getDirection(sourceStr, destinationStr: destinationStr, success: { (success, object) in
                self.newRideReqObj.via = ""
                self.newRideReqObj.waypoints = ""
                if let data = object as? NSDictionary{
                    let routes = data["routes"] as! NSArray
                    if routes.count == 0{
                        return
                    }
                    let routesDict = routes[0] as! NSDictionary
                    let overview_polyline = routesDict["overview_polyline"] as! NSDictionary
                    let points = overview_polyline ["points"] as! NSString
                    self.newRideReqObj.via = routesDict ["summary"] as? String
                    self.newRideReqObj.waypoints = points as String
                }
                self.searchForRides()
                }, failure: { (error) in
                    self.searchForRides()
            })
            }, failure: { (error) in
                self.searchForRides()
        })
    }
    
    func createRide()
    {
        showIndicator("Creating Ride..")
        newRideReqObj.userId = UserInfo.sharedInstance.userID
        WorkplaceRequestor().addWPRide(newRideReqObj, success: { (success, object) in
            hideIndicator()
            if(success){
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! NeedRideResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.wpData = self.data
                            vc.triggerFrom = "place"
                            vc.inviteFlag = "wpMap"
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! NeedRideResponse).code == "1811") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! NeedRideResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func searchForRides()
    {
        let searchReqObj = SearchRideRequest()
        searchReqObj.userId = UserInfo.sharedInstance.userID
        searchReqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        searchReqObj.reqTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        searchReqObj.poolType = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        
        if(isStartLocWP){
            searchReqObj.fromLat = newRideReqObj.fromLat
            searchReqObj.fromLong = newRideReqObj.fromLong
            searchReqObj.startLoc = newRideReqObj.fromLocation
            searchReqObj.toLat = newRideReqObj.toLat
            searchReqObj.toLong = newRideReqObj.toLong
            searchReqObj.endLoc = newRideReqObj.toLocation
        } else {
            searchReqObj.fromLat = newRideReqObj.toLat
            searchReqObj.fromLong = newRideReqObj.toLong
            searchReqObj.startLoc = newRideReqObj.toLocation
            searchReqObj.toLat = newRideReqObj.fromLat
            searchReqObj.toLong = newRideReqObj.fromLong
            searchReqObj.endLoc = newRideReqObj.fromLocation
        }
        
        searchReqObj.groupID = data!.wpID
        searchReqObj.rideScope = data!.wpID
        searchReqObj.groupType = "6"
        searchReqObj.poolScope = data!.wpID
        if let lastRideType = newRideReqObj.ridetype {
            searchReqObj.ridetype = lastRideType
        }
        searchReqObj.searchType = "generic_location"
        searchReqObj.searchFrom = "anywhere"
        
        showIndicator("Searching for similair rides.")
        
        WorkplaceRequestor().searchWPRides(searchReqObj, success: { (success, object) in
            hideIndicator()
            if(success){
                if(self.isStartLocWP){
                    if((object as! WPRidesResponse).pools?.homeGoing!.count > 0){
                        self.gotoExistingRides(((object as! WPRidesResponse).pools?.homeGoing)!)
                    }
                    else{
                        self.createRide()
                    }
                }
                else{
                    if((object as! WPRidesResponse).pools?.officeGoing!.count > 0){
                        self.gotoExistingRides(((object as! WPRidesResponse).pools?.officeGoing)!)
                    }
                    else{
                        self.createRide()
                    }
                }
            }
            }, failure: { (error) in
                hideIndicator()
                self.createRide()
        })
    }
    
    func gotoExistingRides(_ searchResults: [WPRide])
    {
        let vc = ExistingRidesViewController()
        vc.dataSource = searchResults
        vc.newRideReqObj = newRideReqObj
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func timePicked()
    {
    }
    
    func donePressed()
    {
        txtTime.text = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        txtTime.resignFirstResponder()
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
       self.updateRidesList()
    }
}
