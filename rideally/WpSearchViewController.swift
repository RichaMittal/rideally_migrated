//
//  WpSearchViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/23/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class WpSearchViewController: BaseScrollViewController {
    
    var wpName = UITextField()
    var wpEmail = RATextField()
    var lblErrorMsg = UILabel()
    let btnCheckBox = UIButton(type: .custom)
    var wpData = WorkPlace()
    var domainsList = [WPDomainsResponseData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavBar()
    }
    
    override func goBack() {
        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
            let vc = RideOptionsViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    func addViews() {
        viewsArray.append(getTopView())
        addBottomView(getBottomView())
    }
    
    func getHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = boldFontWithSize(18)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.BLACK_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getSubHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(17)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GRAY_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GRAY_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getTopView() -> SubView
    {
        let view = UIView()
        
        let  lblHeading  =  getHeadingLabelSubViews("CONGRATS!")
        lblHeading.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblHeading)
        
        let  lblSubHeading  =  getSubHeadingLabelSubViews("SUBMIT AND ENJOY FREE RIDES")
        lblSubHeading.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblSubHeading)
        
        let wpImg = UIImageView(image: UIImage(named: "wp_gray"))
        
        wpName.layer.borderWidth = 0.5
        wpName.layer.borderColor = Colors.GREEN_COLOR .cgColor
        wpName.font = normalFontWithSize(15)!
        wpName.textColor = Colors.GRAY_COLOR
        wpName.translatesAutoresizingMaskIntoConstraints = false
        wpName.returnKeyType = UIReturnKeyType.next
        wpName.delegate = self
        wpName.placeholder = "Enter your workplace name"
        wpName.leftView = wpImg
        wpName.leftViewMode = .always
        view.addSubview(wpName)
        
        wpEmail.layer.borderWidth = 0.5
        wpEmail.layer.borderColor = Colors.GREEN_COLOR .cgColor
        wpEmail.font = normalFontWithSize(15)!
        wpEmail.textColor = Colors.GRAY_COLOR
        wpEmail.translatesAutoresizingMaskIntoConstraints = false
        wpEmail.returnKeyType = UIReturnKeyType.done
        wpEmail.delegate = self
        wpEmail.placeholder = "Enter workplace email address"
        wpEmail.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(wpEmail)
        
        btnCheckBox.addTarget(self, action: #selector(isCheckBoxPressed(_:)), for: .touchUpInside)
        btnCheckBox.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        btnCheckBox.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(btnCheckBox)
        
        let lblCheckBox = UILabel()
        lblCheckBox.text = "Check this check box if your coorporate email id is same as registered email id"
        lblCheckBox.textColor = Colors.GRAY_COLOR
        lblCheckBox.font = boldFontWithSize(15)
        lblCheckBox.numberOfLines = 0
        lblCheckBox.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCheckBox)
        
        lblErrorMsg.textColor = Colors.GRAY_COLOR
        lblErrorMsg.font = boldFontWithSize(15)
        lblErrorMsg.translatesAutoresizingMaskIntoConstraints = false
        lblErrorMsg.isHidden = true
        lblErrorMsg.numberOfLines = 0
        lblErrorMsg.isUserInteractionEnabled = true
        lblErrorMsg.textAlignment = .center
        let nowText = "Request new Workplace"
        let combinedString = "Oops. no workplace found for your search. Please \(nowText)" as NSString
        let range = combinedString.range(of: nowText)
        let attributedString = NSMutableAttributedString(string: combinedString as String)
        let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1] as [String : Any]
        attributedString.addAttributes(attributes, range: range)
        lblErrorMsg.attributedText = attributedString
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(requestWorkplace))
        lblErrorMsg.addGestureRecognizer(tapGesture)
        view.addSubview(lblErrorMsg)
        
        let viewsDict = ["lblHeading":lblHeading, "lblSubHeading":lblSubHeading,"wpName":wpName, "wpEmail":wpEmail, "btnCheckBox":btnCheckBox, "lblCheckBox":lblCheckBox, "lblErrorMsg":lblErrorMsg] as [String : Any]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-40-[lblHeading(20)]-3-[lblSubHeading(20)]-20-[wpName(40)]-20-[wpEmail(40)]-15-[btnCheckBox(15)]-20-[lblErrorMsg]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[wpName]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[wpEmail]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[btnCheckBox(15)]-3-[lblCheckBox]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblErrorMsg]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblHeading, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblSubHeading, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: btnCheckBox, attribute: .centerY, relatedBy: .equal, toItem: lblCheckBox, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 300)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let  termsOfuse =  getLabelSubViews("")
        termsOfuse.isUserInteractionEnabled = true
        let myAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myString = NSMutableAttributedString(string: "By clicking SUBMIT, you agree to our Terms of Use", attributes: myAttribute)
        let myRange = NSRange(location: 36, length: 13)
        myString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange)
        termsOfuse.attributedText = myString
        let tapGesture = UITapGestureRecognizer(target:self, action:#selector(termsOfUsePressed(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        termsOfuse.addGestureRecognizer(tapGesture)
        termsOfuse.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(termsOfuse)
        
        let btnSubmit = UIButton()
        btnSubmit.setTitle("SUBMIT", for: UIControlState())
        btnSubmit.backgroundColor = Colors.GREEN_COLOR
        btnSubmit.titleLabel?.font = boldFontWithSize(20)
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnSubmit.translatesAutoresizingMaskIntoConstraints = false
        btnSubmit.addTarget(self, action: #selector(joinWPs), for: .touchDown)
        view.addSubview(btnSubmit)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[termsOfUse(30)][btnSubmit(40)]", options: [], metrics: nil, views: ["termsOfUse":termsOfuse, "btnSubmit":btnSubmit]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[termsOfUse]|", options: [], metrics: nil, views: ["termsOfUse":termsOfuse]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnSubmit]|", options: [], metrics: nil, views: ["btnSubmit":btnSubmit]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 70)
        return sub
    }
    
    func isCheckBoxPressed(_ sender: UIButton) -> Void {
        
        sender.isSelected = !sender.isSelected
        
        btnCheckBox.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        wpEmail.text = ""
        if  sender.isSelected == true
        {
            btnCheckBox.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
            wpEmail.text = UserInfo.sharedInstance.email
        }
    }
    
    func termsOfUsePressed(_ sender:AnyObject)  {
        let vc = StaticPagesViewController()
        if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
            vc.touType = "specific"
        } else {
            vc.touType = "generic"
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func joinWPs()
    {
        if(self.wpName.text?.characters.count == 0 && self.wpEmail.text?.characters.count == 0) {
            AlertController.showToastForError("Please enter valid 'Workplace Name' and 'Company Email Id'.")
        } else if(self.wpName.text?.characters.count == 0) {
            AlertController.showToastForError("Please provide 'Workplace Name'.")
        } else if(self.wpEmail.text?.characters.count == 0) {
            AlertController.showToastForError("Please provide 'Company Email Id'.")
        } else if(self.wpEmail.text?.characters.count != 0 && !self.isValidEmailFormate(self.wpEmail.text!))
        {
            AlertController.showToastForError("Please enter valid 'Company Email Id'.")
        } else {
            checkDomainMatch()
        }
    }
    
    func checkDomainMatch() {
        //if(wpData.wpScope == "1"){
        if(wpData.wpTieUpStatus == "1"){
            if(UserInfo.sharedInstance.email != ""){
                WorkplaceRequestor().getWPDomains(wpData.wpID!, success: { (success, object) in
                    if(success){
                        self.domainsList = object as! [WPDomainsResponseData]
                        let mySecDomain = UserInfo.sharedInstance.secEmail.components(separatedBy: "@").last
                        let myPrimDomain = UserInfo.sharedInstance.email.components(separatedBy: "@").last
                        var domainMatch = false
                        for domain in self.domainsList{
                            if(domain.domainName == myPrimDomain || domain.domainName == mySecDomain){
                                domainMatch = true
                            }
                        }
                        if(domainMatch == true){
                            //check if email is verified else show popup
                            if(UserInfo.sharedInstance.isEmailIDVerified == true || UserInfo.sharedInstance.isSecEmailIDVerified == true){
                                self.sendJoinWPrequest(self.wpData)
                            }
                            else{
                                self.getSecEmail(self.wpData,flag: true)
                            }
                            
                        } else{
                            self.getSecEmail(self.wpData,flag: false)
                        }
                    }
                    }, failure: { (error) in
                        
                })
            }
            else{
                self.getSecEmail(wpData,flag: false)
            }
        } else {
            self.sendJoinWPrequest(wpData)
        }
        //        }
        //        else if(wpData.wpScope == "0"){
        //            self.sendJoinWPrequest(wpData)
        //        }
    }
    
    func isValidEmailFormate(_ stringValue:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: stringValue)
    }
    
    func getSecEmail(_ selectedWPdata: WorkPlace, flag: Bool)
    {
        let myDomain = self.wpEmail.text!.components(separatedBy: "@").last
        
        var domainMatch = false
        for domain in self.domainsList{
            if(domain.domainName == myDomain){
                domainMatch = true
            }
        }
        if(domainMatch == true){
            if(self.wpEmail.text == UserInfo.sharedInstance.email) {
                showIndicator("Sending verification link...")
                WorkplaceRequestor().addSecEmail(UserInfo.sharedInstance.userID, email: self.wpEmail.text!, groupKey:selectedWPdata.groupKey!, groupID: selectedWPdata.wpID!, success: { (success, object) in
                    hideIndicator()
                    if(success == true) {
                        UserInfo.sharedInstance.secEmail = self.wpEmail.text!
                        if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
                            UserInfo.sharedInstance.hideAnywhere = "1"
                        }
                        AlertController.showAlertFor("Email verification", message: "We have sent you a verification link to your email id (\(self.wpEmail.text!)). Please verify it to proceed further.", okAction: {
                            let ins = UIApplication.shared.delegate as! AppDelegate
                            if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                ins.gotoHome()
                            } else {
                                ins.gotoHome(1)
                            }
                        })
                    } else {
                        AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: {
                            //self.wpEmail.text = ""
                        })
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            } else {
                let reqObj = emailIDRequestModel()
                reqObj.user_email = self.wpEmail.text
                reqObj.user_id = UserInfo.sharedInstance.userID
                showIndicator("Loading...")
                SignUpRequestor().isEmailIdalreadyexist(reqObj, success:{ (success, object) in
                    
                    hideIndicator()
                    
                    if (object as! emailIDResponseModel).code == "1454"
                    {
                        showIndicator("Sending verification link...")
                        WorkplaceRequestor().addSecEmail(UserInfo.sharedInstance.userID, email: self.wpEmail.text!, groupKey:selectedWPdata.groupKey!, groupID: selectedWPdata.wpID!, success: { (success, object) in
                            hideIndicator()
                            if(success == true) {
                                UserInfo.sharedInstance.secEmail = self.wpEmail.text!
                                if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
                                    UserInfo.sharedInstance.hideAnywhere = "1"
                                }
                                AlertController.showAlertFor("Email verification", message: "We have sent you a verification link to your email id (\(self.wpEmail.text!)). Please verify it to proceed further.", okAction: {
                                    let ins = UIApplication.shared.delegate as! AppDelegate
                                    if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                        ins.gotoHome()
                                    } else {
                                        ins.gotoHome(1)
                                    }
                                })
                            } else {
                                AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: {
                                    //self.wpEmail.text = ""
                                })
                            }
                            }, failure: { (error) in
                                hideIndicator()
                        })
                    }
                    else
                    {
                        AlertController.showToastForError("This 'Email Id' already exists.")
                    }
                    
                }){ (error) in
                    hideIndicator()
                    AlertController.showAlertForError(error)
                }
            }
        } else {
            AlertController.showAlertFor("Error", message: "Sorry, we could not authorize your email id. Please check your email id (\(self.wpEmail.text!)) again or register again with correct email id or contact customer care at 080 4600 4600.", okButtonTitle: "Ok", okAction: {
                //self.wpEmail.text = ""
            })
        }
    }
    
    func sendJoinWPrequest(_ selectedWPdata: WorkPlace)
    {
        let tieup_wpuser: String?
        if(selectedWPdata.wpTieUpStatus == "1") {
            tieup_wpuser = "1"
        } else {
            tieup_wpuser = ""
        }
        showIndicator("Sending request to Join Workplace.")
        WorkplaceRequestor().joinWorkPlace(selectedWPdata.wpID!, groupScope: selectedWPdata.wpScope!, userMsg: "",tieupWpUser: tieup_wpuser!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
                    UserInfo.sharedInstance.hideAnywhere = "1"
                }
                AlertController.showAlertFor("Join Workplace", message: object as? String, okButtonTitle: "Ok", okAction: {
                    if(selectedWPdata.wpScope == "0" || selectedWPdata.wpTieUpStatus == "1") {
                        self.redirectToWpSummary()
                    } else {
                        let ins = UIApplication.shared.delegate as! AppDelegate
                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                            ins.gotoHome()
                        } else {
                            ins.gotoHome(1)
                        }
                    }
                })
            }
            else{
                AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: nil)
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func redirectToWpSummary() {
        WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: wpData.wpID!, success: { (success, object) in
            WorkplaceRequestor().getWPMembers("", groupID: self.wpData.wpID!, distanceRange: "1", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, objectMembers) in
                hideIndicator()
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : RideMapViewController = storyboard.instantiateViewController(withIdentifier: "RideMapViewController") as! RideMapViewController
                vc.hidesBottomBarWhenPushed = true
                vc.signupFlow = true
                vc.data = (objectMembers as! WPMembersResponseData).groupDetails
                vc.dataSourceToOfc = (object as! WPRidesResponse).pools?.officeGoing
                vc.dataSourceToHome = (object as! WPRidesResponse).pools?.homeGoing
                vc.wpMembers = (objectMembers as! WPMembersResponseData).members
                self.navigationController?.pushViewController(vc, animated: true)
            }) { (error) in
                //hideIndicator()
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func requestWorkplace()
    {
        let vc = AddWPViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.wpName = self.wpName.text
        vc.wpLoc = self.wpEmail.text
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if(textField == wpName) {
            let vc = WpListViewController()
            vc.onSelectionBlock = { (selectedWp, isSearchResult) -> Void in
                self.wpData = selectedWp as! WorkPlace
                if(isSearchResult != nil && isSearchResult != "" && isSearchResult == "true") {
                    self.wpName.text = self.wpData.wpSearchedName
                } else {
                    self.wpName.text = self.wpData.wpName
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
            wpName.resignFirstResponder()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
}
