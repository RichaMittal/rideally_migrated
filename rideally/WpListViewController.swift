//
//  WpListViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/23/17.
//  Copyright © 2017 rideally. All rights reserved.
//

//
//  WorkplaceViewController.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import Crashlytics
import Alamofire
import GoogleMaps
import FirebaseAnalytics

class WpListViewController: BaseScrollViewController, UITableViewDataSource, UITableViewDelegate {
    
    let tableView = UITableView()
    var tableHeightConstraint: NSLayoutConstraint?
    var tableTopConstraint: NSLayoutConstraint?
    let rowHt: CGFloat = 60
    let searchTxt = UITextField()
    var searchResult = false
    var allWp = [WorkPlace]()
    var onSelectionBlock: actionBlockWith2ParamsString?
    lazy var titleView: SubView = self.getTitleView()
    
    var dataSource = [WorkPlace](){
        didSet{
            if(dataSource.count > 0){
                tableView.isHidden = false
                view.removeEmptyView()
                tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt)
                selectedIndexPath = nil
                tableView.reloadData()
            }
            else{
                tableHeightConstraint?.constant = 0
                showEmptyView("You have not joined any Corporate groups to start sharing, you can join your company group by searching in \"All\" tab.")
                //viewsArray.append(errorView())
                //errorView()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.tabBarController?.tabBar.isTranslucent = false
        self.navigationItem.title = "Select Company"
        addViews()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        removeTopView()
        addTopView(titleView)
    }
    
    func addViews()
    {
        viewsArray.append(getTableView())
        getAllWP("normal")
    }
    
    func showEmptyView(_ msg: String)
    {
        tableView.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "ic_empty_cart", actionHandler: nil)
    }
    
    func getTitleView() -> SubView
    {
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR

        let image = UIImage(named: "search_gray")
        let frame = CGRect(x: 0, y: 0, width: (image?.size.width)!+10, height: (image?.size.height)!)
        let leftV = UIImageView(frame: frame)
        
        leftV.image = image
        leftV.contentMode = UIViewContentMode.center
        searchTxt.layer.borderColor = Colors.GREEN_COLOR.cgColor
        searchTxt.layer.borderWidth = 0.5
        searchTxt.layer.cornerRadius = 20
        searchTxt.clipsToBounds = true
        searchTxt.placeholder = "Search Your Company"
        searchTxt.font = normalFontWithSize(12)
        searchTxt.textColor = Colors.GRAY_COLOR
        searchTxt.translatesAutoresizingMaskIntoConstraints = false
        searchTxt.leftView = leftV
        searchTxt.leftViewMode = .always
        searchTxt.delegate = self
        //searchTxt.textAlignment = .Center
        searchTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        view.addSubview(searchTxt)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[searchTxt(30)]|", options: [], metrics: nil, views: ["searchTxt":searchTxt]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[searchTxt]-10-|", options: [], metrics: nil, views: ["searchTxt":searchTxt]))
        
        let subview = SubView()
        subview.view = view
        subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        subview.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 35)
        return subview
    }
    
    func getTableView() -> SubView
    {
        let view = UIView()
        
        //tableView.separatorColor = UIColor.clearColor()
        tableView.separatorColor = Colors.GRAY_COLOR
        tableView.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(WpListCell.self, forCellReuseIdentifier: "wplistcell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[table]-5-|", options: [], metrics: nil, views: ["table":tableView]))
        
        tableHeightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        let subview = SubView()
        subview.view = view
        subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        subview.heightConstraint = tableHeightConstraint!
        return subview
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    var selectedIndexPath: IndexPath?
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if((selectedIndexPath != nil) && (selectedIndexPath == indexPath))
            {
                return (rowHt + 40)
            }
            else{
                return rowHt
            }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: WpListCell?
        cell = tableView.dequeueReusableCell(withIdentifier: "wplistcell") as? WpListCell
        cell!.searchResult = searchResult
        cell!.data = dataSource[indexPath.row]
        cell!.onUserActionBlock = { (selectedAction) -> Void in
            self.performActionFor(selectedAction as! String, indexPath: indexPath)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.WHITE_COLOR
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedWp = dataSource[indexPath.row]
        _ = self.navigationController?.popViewController(animated: true)
        UserInfo.sharedInstance.touHideAnywhere = selectedWp.wpHideAnywhere!
        onSelectionBlock?(selectedWp, String(searchResult))
    }
    
    
    func performActionFor(_ userSelection: String, indexPath: IndexPath)
    {
    }
    
    func getAllWP(_ triggerFrom: String)
    {
        removeTopView()
        addTopView(titleView)
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Workplace Tab",
            AnalyticsParameterItemName: "Footer",
            AnalyticsParameterContentType:"All Workplace Button"
            ])
        selectedIndexPath = nil
        
        showIndicator("Loading Workplaces.")
        WorkplaceRequestor().getAllWorkPlace({ (success, object) in
            hideIndicator()
            self.allWp = (object as! WorkPlacesResponse).data!.allOfcGroups!
            self.dataSource = (object as! WorkPlacesResponse).data!.allOfcGroups!
        }) { (error) in
            hideIndicator()
        }
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if(textField == searchTxt) {
            if(searchTxt.text?.characters.count == 0) {
                self.searchResult = false
                self.dataSource = self.allWp
            } else {
                 wpSearchByLike()
            }
        }
    }
    
    func wpSearchByLike() {
        WorkplaceRequestor().wpSearchByLike(searchTxt.text!, success:{ (success, object) in
            if(success) {
                self.searchResult = true
                self.dataSource = (object as! WPSearchResponse).data!
            }
        }) { (error) in
            hideIndicator()
        }
    }
}
