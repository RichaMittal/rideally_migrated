//
//  PaymentViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 3/18/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class PaymentViewController: BaseScrollViewController, PGTransactionDelegate {
    public func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        AlertController.showAlertForMessage(ERROR_MESSAGE_STRING)
        self.dismiss(animated: true, completion: nil)
    }

    public func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        AlertController.showAlertForMessage(ERROR_MESSAGE_STRING)
        self.dismiss(animated: true, completion: nil)
    }

    
    let lblSource = UILabel()
    let lblDest = UILabel()
    var promoApplyText = UILabel()
    var promoTextBox = UITextField()
    var rideDetailData: RideDetail?
    var viewHt: CGFloat = 245
    let radioButtonInstance = DLRadioButton()
    var isInavlidCode = false
    var promoLabel = UILabel()
    var promoError = UILabel()
    var couponId = ""
    var finalCost = ""
    var btnTitle = ""
    let resultView = UIView()
    let resultLabel = UILabel()
    var payBtn = UIButton()
    var ISPROMOCODEAPPLIED = false
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        title = "Payment"
        viewsArray.append(rideDetailView())
        if(rideDetailData?.couponId != "" && rideDetailData?.couponId != nil) {
            viewsArray.append(pendingCostView())
        }
        viewsArray.append(amountView())
        createResultView()
        addBottomView(getBottomView())
        //viewsArray.append(pendingCostView())
    }
    
    override func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
        if(ISPROMOCODEAPPLIED){
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateTaxiDetail"), object: nil)
        }
    }
    
    func rideDetailView () -> SubView
    {
        let view = UIView()
        
        let lblFrom = UILabel()
        lblFrom.text = "FROM"
        lblFrom.font = normalFontWithSize(12)
        lblFrom.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblFrom)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.text = rideDetailData?.source
        lblSource.font = normalFontWithSize(12)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblSource)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let lblTo = UILabel()
        lblTo.font = normalFontWithSize(12)
        lblTo.text = "To"
        lblTo.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTo)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = normalFontWithSize(12)
        lblDest.text = rideDetailData?.destination
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDest)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        let lblDistance = UILabel()
        lblDistance.text = "Distance"
        lblDistance.font = normalFontWithSize(12)
        lblDistance.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDistance)
        
        let lblDistanceValue = UILabel()
        lblDistanceValue.textColor = Colors.GRAY_COLOR
        lblDistanceValue.font = normalFontWithSize(12)
        lblDistanceValue.text = (rideDetailData?.distance)!+" Km"
        lblDistanceValue.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDistanceValue)
        
        let lblLine4 = UILabel()
        lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine4.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine4)
        
        let lblAmount = UILabel()
        lblAmount.text = "Total Amount"
        lblAmount.font = normalFontWithSize(12)
        lblAmount.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblAmount)
        
        let lblAmountValue = UILabel()
        lblAmountValue.textColor = Colors.GRAY_COLOR
        lblAmountValue.font = normalFontWithSize(12)
        lblAmountValue.text = "\(RUPEE_SYMBOL) \(rideDetailData?.cost ?? "")"
        lblAmountValue.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblAmountValue)
        
        let lblLine5 = UILabel()
        lblLine5.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine5.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine5)
        
        let promoIcon = UIImageView()
        promoIcon.image = UIImage(named: "promocode")
        promoIcon.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(promoIcon)
        
        promoLabel.textColor = Colors.GREEN_COLOR
        promoLabel.font = normalFontWithSize(12)
        promoLabel.translatesAutoresizingMaskIntoConstraints = false
        promoLabel.text = "Have Promo Code?"
        promoLabel.isUserInteractionEnabled = true
        let promoLabelTapGesture = UITapGestureRecognizer(target:self, action:#selector(showTextField))
        promoLabelTapGesture.numberOfTapsRequired = 1
        promoLabelTapGesture.numberOfTouchesRequired = 1
        promoLabel.addGestureRecognizer(promoLabelTapGesture)
        view.addSubview(promoLabel)
        
        promoApplyText.font = normalFontWithSize(15)
        promoApplyText.backgroundColor = UIColor.clear
        let myApplyAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myApplyString = NSMutableAttributedString(string: "APPLY", attributes: myApplyAttribute)
        let myApplyRange = NSRange(location: 0, length: 5)
        myApplyString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myApplyRange)
        promoApplyText.attributedText = myApplyString
        let referralApplyTapGesture = UITapGestureRecognizer(target:self, action:#selector(applyReferralCode))
        referralApplyTapGesture.numberOfTapsRequired = 1
        referralApplyTapGesture.numberOfTouchesRequired = 1
        promoApplyText.translatesAutoresizingMaskIntoConstraints = false
        promoApplyText.addGestureRecognizer(referralApplyTapGesture)
        
        promoTextBox.delegate = self
        promoTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        promoTextBox.placeholder = "PROMO CODE"
        promoTextBox.font = normalFontWithSize(12)
        promoTextBox.textColor = Colors.GRAY_COLOR
        promoTextBox.rightView = promoApplyText
        promoTextBox.rightViewMode = UITextFieldViewMode.always
        promoTextBox.returnKeyType = UIReturnKeyType.done
        promoTextBox.autocapitalizationType = .allCharacters
        promoTextBox.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        promoTextBox.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(promoTextBox)
        
        promoError.textColor = Colors.RED_COLOR
        promoError.translatesAutoresizingMaskIntoConstraints = false
        promoError.font = normalFontWithSize(10)
        view.addSubview(promoError)
        
        promoLabel.isHidden = true
        promoTextBox.isHidden = true
        promoError.isHidden = true
        
        if(rideDetailData?.couponId != "" && rideDetailData?.couponId != nil) {
            promoTextBox.isHidden = false
            promoTextBox.text = rideDetailData?.couponCode
            promoTextBox.isUserInteractionEnabled = false
            promoApplyText.text = "APPLIED"
            promoApplyText.textColor = Colors.GREEN_COLOR
        } else {
            promoLabel.isHidden = false
        }
        self.resultView.backgroundColor = Colors.WHITE_COLOR
        self.resultView.isHidden = true
        self.resultView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.resultView)
        
        let viewsDict = ["lblFrom":lblFrom, "source":lblSource, "line2":lblLine2, "lblTo":lblTo, "dest":lblDest, "line3":lblLine3, "lblDistance":lblDistance, "lblDistanceV":lblDistanceValue, "line4":lblLine4, "lblAmount":lblAmount, "lblAmountV":lblAmountValue, "line5":lblLine5, "ipromo":promoIcon, "promoText":promoTextBox, "promolbl":promoLabel, "promoerror":promoError]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(40)][line2(0.5)][dest(40)][line3(0.5)][lblDistance(40)][line4(0.5)][lblAmount(40)][line5(0.5)][promolbl(40)][promoText(25)]-2-[promoerror]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblFrom(35)]-5-[source]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblTo(35)]-5-[dest]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblDistance][lblDistanceV(50)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblAmount][lblAmountV(50)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line5]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[ipromo(15)]-5-[promolbl]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[ipromo(15)]-5-[promoText]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[promoerror]-5-|", options: [], metrics: nil, views: viewsDict))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[resultView(230)]", options:[], metrics:nil, views: ["resultView": self.resultView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[resultView]-15-|", options:[], metrics:nil, views: ["resultView": self.resultView]))
        
        view.addConstraint(NSLayoutConstraint(item: lblDistanceValue, attribute: .centerY, relatedBy: .equal, toItem: lblDistance, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblDistanceValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        view.addConstraint(NSLayoutConstraint(item: lblAmountValue, attribute: .centerY, relatedBy: .equal, toItem: lblAmount, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblAmountValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        view.addConstraint(NSLayoutConstraint(item: promoIcon, attribute: .centerY, relatedBy: .equal, toItem: promoLabel, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: promoIcon, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        view.addConstraint(NSLayoutConstraint(item: promoIcon, attribute: .centerY, relatedBy: .equal, toItem: promoTextBox, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: promoIcon, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        view.addConstraint(NSLayoutConstraint(item: lblFrom, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblFrom, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        view.addConstraint(NSLayoutConstraint(item: lblTo, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblTo, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 206)
        return sub
    }
    
    func amountView () -> SubView
    {
        let view = UIView()
        
        let lblLine6 = UILabel()
        lblLine6.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine6.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine6)
        
        radioButtonInstance.isSelected = true
        radioButtonInstance.translatesAutoresizingMaskIntoConstraints = false
        radioButtonInstance.setTitleColor(Colors.BLACK_COLOR, for: UIControlState())
        radioButtonInstance.iconColor = Colors.GREEN_COLOR
        radioButtonInstance.indicatorColor = Colors.GREEN_COLOR
        radioButtonInstance.titleLabel?.font = boldFontWithSize(15)
        view.addSubview(radioButtonInstance)
        
        let imageView_payTM = UIImageView()
        imageView_payTM.translatesAutoresizingMaskIntoConstraints = false
        imageView_payTM.image = UIImage(named: "S_paytm")
        view.addSubview(imageView_payTM)
        
        let viewsDict = ["line6":lblLine6, "radiobtn":radioButtonInstance, "ipaytm":imageView_payTM]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line6(0.5)]-10-[radiobtn(15)]", options:[], metrics:nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[radiobtn]-5-[ipaytm(50)]", options:[], metrics:nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line6]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[ipaytm(20)]", options:[], metrics:nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: imageView_payTM, attribute: .centerY, relatedBy: .equal, toItem: radioButtonInstance, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20)
        return sub
    }
    
    func pendingCostView () -> SubView
    {
        let cost = self.finalCost != "" ? self.finalCost : "\(rideDetailData?.discountPrice ?? 4)"
        let view = UIView()
        
        let lblLine7 = UILabel()
        lblLine7.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine7.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine7)
        
        let lblPending = UILabel()
        lblPending.text = "Pending Amount"
        lblPending.font = normalFontWithSize(12)
        lblPending.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPending)
        
        let lblPenidngValue = UILabel()
        lblPenidngValue.textColor = Colors.GRAY_COLOR
        lblPenidngValue.font = normalFontWithSize(12)
        lblPenidngValue.text = "\(RUPEE_SYMBOL) \(cost)"
        lblPenidngValue.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPenidngValue)

        let viewsDict = ["line7":lblLine7, "lblPending":lblPending, "lblPendingV":lblPenidngValue]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[line7(0.5)][lblPending(40)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line7]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblPending][lblPendingV(50)]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraint(NSLayoutConstraint(item: lblPenidngValue, attribute: .centerY, relatedBy: .equal, toItem: lblPending, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblPenidngValue, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40.5)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        if(rideDetailData?.couponId != "" && rideDetailData?.couponId != nil) {
            btnTitle = "\("PAY") \(RUPEE_SYMBOL) \(rideDetailData?.discountPrice ?? 4)"
        } else {
            btnTitle = "\("PAY") \(RUPEE_SYMBOL) \(rideDetailData?.cost ?? "")"
        }
        payBtn.setTitle(btnTitle, for: UIControlState())
        payBtn.backgroundColor = Colors.GRAY_COLOR
        payBtn.translatesAutoresizingMaskIntoConstraints = false
        payBtn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        payBtn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        payBtn.addTarget(self, action: #selector(proceed), for: .touchDown)
        view.addSubview(payBtn)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(40)]", options: [], metrics: nil, views: ["btn":payBtn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":payBtn]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func showTextField () {
        if(rideDetailData?.costUpdated == "1") {
            promoTextBox.isHidden = false
            promoError.isHidden = false
            promoLabel.isHidden = true
        } else {
            AlertController.showAlertFor("Message", message: "You can apply promocode once you receive SMS, after completion of your ride.")
        }
    }
    func textFieldDidChange(_ textField: UITextField) {
        if(textField == promoTextBox) {
            textField.text = textField.text!.uppercased(with: Locale.current)
            self.isInavlidCode = false
            if(promoTextBox.text?.characters.count == 0) {
                self.promoError.isHidden = true
            } else {
                self.promoError.isHidden = false
                isPromoCodeValid()
            }
        }
    }
    
    func isPromoCodeValid () {
        AirportRequestor().checkCouponExists(promoTextBox.text!, success:{(success, object) in
            if (success) {
                self.isInavlidCode = false
                self.promoApplyText.isHidden = false
                self.promoApplyText.isUserInteractionEnabled = true
                //self.referredBy = ((object as! referralCodeResponseModel).dataObj?.first?.firstName)!
                self.promoError.textColor = Colors.GREEN_COLOR
                self.promoError.text = (object as AnyObject).message
                self.couponId = ((object as! couponExistsResponse).dataObj?.first?.coupon_id)!
            } else {
                self.isInavlidCode = true
                self.promoError.textColor = Colors.RED_COLOR
                self.promoError.text = object as? String
            }
        }) { (error) in
        }
    }
    
    func applyReferralCode () {
        if(isInavlidCode) {
            AlertController.showToastForError("Please provide valid 'Promo Code'.")
        } else {
            self.view.endEditing(true)
            var device_id = ""
            if(UserInfo.sharedInstance.deviceToken != "") {
                device_id = UserInfo.sharedInstance.deviceToken
            } else {
                device_id = (UIDevice.current.identifierForVendor?.uuidString)!
            }
            //reqObj.referred_by = self.referredBy
            showIndicator("Applying Referral Code...")
            AirportRequestor().checkCouponUsage(self.couponId, deviceId: device_id, success:{(success, object) in
                hideIndicator()
                if (success) {
                    let reqObj = applyCouponRequest()
                    reqObj.coupon_code = self.promoTextBox.text
                    reqObj.user_id = UserInfo.sharedInstance.userID
                    reqObj.device_id = device_id
                    reqObj.pool_id = self.rideDetailData?.poolID
                    AirportRequestor().applyCouponCode(reqObj, success:{(success, object) in
                        if(success) {
                            AlertController.showToastForInfo("Congratulations! Promocode has been applied successfully.")
                            // UserInfo.sharedInstance.refCode = self.referralTextBox.text!
                            self.promoTextBox.isUserInteractionEnabled = false
                            self.promoApplyText.text = "APPLIED"
                            self.promoApplyText.textColor = Colors.GREEN_COLOR
                            self.promoApplyText.isUserInteractionEnabled = false
                            self.promoError.isHidden = true
                            self.finalCost = "\((object as! applyCouponResponse).dataObj?.first?.final_cost ?? 4)"
                            self.btnTitle = "\("PAY") \(RUPEE_SYMBOL) \(self.finalCost)"
                            self.payBtn.setTitle(self.btnTitle, for: UIControlState())
                            self.ISPROMOCODEAPPLIED = true
                            //self.updateData()
                            //viewsArray.append(pendingView)
                        } else {
                            
                        }
                    }) { (error) in
                        //hideIndicator()
                    }
                } else {
                    AlertController.showToastForError("Promocode has been already used by you.")
                }
            }) { (error) in
                hideIndicator()
            }
        }
    }
    
    func updateData () {
        viewsArray.append(pendingCostView())
    }
    func proceed() {
        if(rideDetailData?.costUpdated == "1") {
            configPaytmMerchant()
        } else {
            AlertController.showAlertFor("Message", message: "You can proceed for payment once you receive SMS, after completion of your ride.")
        }
    }
    
    func createResultView() -> Void
    {
        
        self.resultView.layer.borderWidth = 1.0
        self.resultView.layer.borderColor = UIColor.lightGray.cgColor
        self.resultView.layer.cornerRadius = 5.0
        
        let tickIcon = UIImageView()
        tickIcon.image = UIImage(named: "Tickmark")
        tickIcon.translatesAutoresizingMaskIntoConstraints = false
        self.resultView.addSubview(tickIcon)
        
        
        let titleLabel = UILabel()
        titleLabel.text = "Payment Successful !"
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = boldFontWithSize(22)
        titleLabel.textColor = getColorByHex(0x617F23, alpha: 1.0)
        self.resultView.addSubview(titleLabel)
        
        resultLabel.textAlignment = NSTextAlignment.center
        resultLabel.translatesAutoresizingMaskIntoConstraints = false
        resultLabel.numberOfLines = 0
        resultLabel.clipsToBounds = true;
        resultLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        resultLabel.font = normalFontWithSize(17)
        resultLabel.textColor = Colors.GRAY_COLOR
        self.resultView.addSubview(resultLabel)
        
        let bottomLabel = UILabel()
        bottomLabel.text = "Thank you for doing business with RideAlly."
        bottomLabel.textAlignment = NSTextAlignment.center
        bottomLabel.translatesAutoresizingMaskIntoConstraints = false
        bottomLabel.font = normalFontWithSize(15)
        bottomLabel.textColor = Colors.GRAY_COLOR
        self.resultView.addSubview(bottomLabel)
        
        
        self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[tick(20)]-10-[titleLbl(30)]-20-[resultLbl(100)]-5-[bottomLbl(20)]", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel,"tick": tickIcon]))
        
        //self.resultView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[tick]-50-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel,"tick": tickIcon]))
        
        self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[titleLbl]-50-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel]))
        self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[resultLbl]-20-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel]))
        self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[bottomLbl]-20-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel]))
        
        self.resultView.addConstraint(NSLayoutConstraint(item: tickIcon, attribute: .centerX, relatedBy: .equal, toItem: self.resultView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
    }

    func configPaytmMerchant() -> Void
    {
     let discountprice = "\(rideDetailData?.discountPrice ?? 4)"
        let  pgMerchantConfig = PGMerchantConfiguration.default()
        pgMerchantConfig?.checksumGenerationURL = PAYTM_GENERATE
        pgMerchantConfig?.checksumValidationURL = PAYTM_VERIFY
        
        /*
         https://rideally.com/PaytmKit/generateChecksum.php
         https://rideally.com/PaytmKit/verifyChecksum.php
         
         Live Envi
         Merchant Detail:
         Website name       :WAP-   Hariprakashwap WEB-   Hariprakashweb
         MID                        : rideal18020254537437
         Merchant Key         : ZlQhHCGumzP8z2BN
         
         Industry_type_ID    : Retail110
         Channel_ID            : WEB
         WAP
         
         Merchant Dashboard: Login details.
         
                     URL:   https://gateway.paytm.in/PayTMSecured/app/auth/login
                     User name:  Hariprakash
                     Password:    Rideally#123
         
         */
        
        
        if(ENVIRONMENT == "LIVE")
        {
            let orderDict: [String:String] = [
                "MID" : "rideal18020254537437",
                "CHANNEL_ID" : "WAP",
                "INDUSTRY_TYPE_ID" : "Retail110",
                "WEBSITE" : "Hariprakashwap",
                "TXN_AMOUNT" : self.finalCost != "" ? self.finalCost : discountprice,
                "ORDER_ID" : (rideDetailData?.poolID)!+"-\(Int(arc4random_uniform(214128631) + 1))",
                "CUST_ID" : UserInfo.sharedInstance.userID,
                "THEME" : "merchant"
            ]
            
            
            let order: PGOrder = PGOrder(params: orderDict)
            let txnController: PGTransactionViewController = PGTransactionViewController(transactionFor: order)
            txnController.serverType =  eServerTypeProduction
            txnController.merchant = pgMerchantConfig
            txnController.delegate = self
            self.present(txnController, animated: true, completion: nil)
        }
        else
        {
            let orderDict: [String:String] = [
                "MID" : "RideAl10117568768303",
                "CHANNEL_ID" : "WAP",
                "INDUSTRY_TYPE_ID" : "Retail",
                "WEBSITE" : "RideAllywap",
                "TXN_AMOUNT" : self.finalCost != "" ? self.finalCost : discountprice,
                "ORDER_ID" : (rideDetailData?.poolID)!+"-\(Int(arc4random_uniform(214128631) + 1))",
                "CUST_ID" : UserInfo.sharedInstance.userID,
                "THEME" : "merchant"
            ]
            let order: PGOrder = PGOrder(params: orderDict)
            PGServerEnvironment.selectServerDialog(self.view, completionHandler: {(type: ServerType) -> Void in
                
                let txnController: PGTransactionViewController = PGTransactionViewController(transactionFor: order)
                
                if type != eServerTypeNone
                {
                    txnController.serverType =  type
                    txnController.merchant = pgMerchantConfig
                    txnController.delegate = self
                    self.present(txnController, animated: true, completion: nil)
                }
            })
        }
    }
    
    func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable: Any]!)
    {
        if  let respobj:NSDictionary = response as NSDictionary
        {
            let reqObj = UpdateTransactionOnRecahargeRequest()
            
            showIndicator("Loading...")
            
            reqObj.userID = UserInfo.sharedInstance.userID
            reqObj.channelType = "WAP"
            reqObj.bankName    =  respobj.value(forKey: "BANKNAME") as? String
            reqObj.bankTransactionID    = respobj.value(forKey: "BANKTXNID") as? String
            reqObj.currency    = respobj.value(forKey: "CURRENCY") as? String
            reqObj.gateWayname    = respobj.value(forKey: "GATEWAYNAME") as? String
            reqObj.validCheckSum    = respobj.value(forKey: "IS_CHECKSUM_VALID") as? String
            reqObj.mid    = respobj.value(forKey: "MID") as? String
            reqObj.orderID    = respobj.value(forKey: "ORDERID") as? String
            reqObj.paymentMode    = respobj.value(forKey: "PAYMENTMODE") as? String
            reqObj.responseCode    = respobj.value(forKey: "RESPCODE") as? String
            reqObj.responseMSG    = respobj.value(forKey: "RESPMSG") as? String
            reqObj.status    = respobj.value(forKey: "STATUS") as? String
            reqObj.transactionAmt   = respobj.value(forKey: "TXNAMOUNT") as? String
            reqObj.transactionDate    = respobj.value(forKey: "TXNDATE") as? String
            reqObj.transactionID    = respobj.value(forKey: "TXNID") as? String
            reqObj.transactionFrom = "2"
            //reqObj.rechargeAmount = self.selectedAmount as String
            reqObj.firstName = UserInfo.sharedInstance.firstName
            reqObj.emailID = UserInfo.sharedInstance.email
            reqObj.mobileNumber = UserInfo.sharedInstance.mobile
            
            
//            self.rechargeView.hidden = false
//            self.encashView.hidden = false
//            self.couponView.hidden = false
//            self.segmentEncash_Recharge.hidden = false
//            self.resultView.hidden = true
            
            
            MyAccountRequestor().sendUpdateTransactionRequest(reqObj, success:{ (success, object) in
                hideIndicator()
                
                if let data = object as? NSDictionary
                {
                    
                    if data.value(forKey: "code") as! NSString == "2992" || data.value(forKey: "code") as! NSString == "2990"
                    {
//                        if let transactionID = (data.objectForKey("data")) as? NSArray
//                        {
                            let message = "\("We have successfully received your payment of ")\(RUPEE_SYMBOL)\(reqObj.transactionAmt!)"
                            AlertController.showAlertFor("Payment", message: message, okButtonTitle: "Ok", okAction: {
                                self.updateTaxiDetails()
                            })
                            
//                            let transactionIDValue = transactionID[0] as! String
//                            
////                            self.rechargeView.hidden = true
////                            self.encashView.hidden = true
////                            self.couponView.hidden = true
////                            self.segmentEncash_Recharge.hidden = true
//                            self.resultView.hidden = false
//                            
//                            let fullString = NSMutableAttributedString(string: "We have received your payment of")
//                            let image1Attachment = NSTextAttachment()
//                            image1Attachment.image = UIImage(named: "rupee_gray.png")
//                            let image1String = NSAttributedString(attachment: image1Attachment)
//                            fullString.appendAttributedString(image1String)
//                            
//                            var boldString = "\(reqObj.transactionAmt!)"
//                            var range = NSMakeRange(0, "\(reqObj.transactionAmt!)".characters.count)
//                            fullString.appendAttributedString(self.attributedString(from: boldString, nonBoldRange: range))
//                            
//                            fullString.appendAttributedString(NSAttributedString(string:". Your reference ID for the transaction is"))
//                            
//                            boldString = " \(transactionIDValue)"
//                            range = NSMakeRange(0, "\(transactionIDValue)".characters.count)
//                            fullString.appendAttributedString(self.attributedString(from: boldString, nonBoldRange: range))
                            
                            //self.resultLabel.attributedText = fullString
                            
                            //self.resultLabel.text = "We have received your payment of \(reqObj.rechargeAmount!) cost. Your reference ID for the transaction is \(transactionIDValue)"
                        //}
                    }
                    else
                    {
                        if let transactionID = (data.object(forKey: "data")) as? NSArray
                        {
                            let transactionIDValue = transactionID[0] as! String
                            
                            AlertController.showAlertForMessage("There is some problem in transaction. Your reference ID for this transaction is \(transactionIDValue). Please try again")
                        }
                    }
                }
                
            }){ (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
//    
//    func didFailTransaction(_ controller: PGTransactionViewController!, error: NSError!, response: [AnyHashable: Any]!)
//    {
//        
//        AlertController.showAlertForMessage(ERROR_MESSAGE_STRING)
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    
//    func didCancelTransaction(_ controller: PGTransactionViewController!, error: NSError!, response: [AnyHashable: Any]!)
//    {
//        self.dismiss(animated: true, completion: nil)
//    }
    
    
    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable: Any]!)
    {
        //        if let value = response
        //        {
        //            print("CASTransaction resp is:",value)
        //        }
    }
    
    func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
        
        let fontSize = UIFont.systemFontSize
        
        let attrs = [
            NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize),
            NSForegroundColorAttributeName: UIColor.black
        ]
        
        let nonBoldAttribute = [
            NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize),
            NSForegroundColorAttributeName: UIColor.black
            //NSFontAttributeName: UIFont.systemFontOfSize(fontSize()),
        ]
        
        let attrStr = NSMutableAttributedString(string: string, attributes: attrs)
        
        if let range = nonBoldRange
        {
            attrStr.setAttributes(nonBoldAttribute, range: range)
        }
        
        return attrStr
    }
    
    func getColorByHex(_ rgbHexValue:UInt32, alpha:Double = 1.0) -> UIColor {
        let red = Double((rgbHexValue & 0xFF0000) >> 16) / 256.0
        let green = Double((rgbHexValue & 0xFF00) >> 8) / 256.0
        let blue = Double((rgbHexValue & 0xFF)) / 256.0
        
        return UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: CGFloat(alpha))
    }
    
    func updateTaxiDetails()
    {
        showIndicator("Fetching Ride Details")
        RideRequestor().getTaxiDetails((rideDetailData?.poolID)!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let vc = TaxiDetailViewController()
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = object as? RideDetail
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
