//
//  StaticPagesViewController.swift
//  rideally
//
//  Created by Raghunathan on 8/29/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit


class StaticPagesViewController: UIViewController {

    var webContentView = UIWebView()
    var staticPage: String?
    var touType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if(staticPage == "terms") {
            self.title = "Terms Of Use"
        } else if(staticPage == "terms-workplace") {
            self.title = "Terms Of Use - Honeywell"
        } else if(staticPage == "privacypolicy") {
            self.title = "Privacy Policy"
        } else if(staticPage == "blr-airport-tc") {
            self.title = "T&C For Airport"
        } else if(staticPage == "sharedtaxi-T&C") {
            self.title = "T&C For Share A Taxi"
        } else if(staticPage == "guidelines") {
            self.title = "User Guidelines"
        } else if(staticPage == "driverguidelines") {
            self.title = "Driver Guidelines"
        } else {
            self.title = ""
            if(touType != "" && touType == "specific") {
                self.navigationItem.title = "Terms Of Use"
            } else {
                self.navigationItem.title = "Terms Of Use"
            }
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = Colors.GREEN_COLOR
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        }
        self.view.backgroundColor = Colors.WHITE_COLOR
        self.navigationController!.navigationBar.topItem!.title = ""
    }

    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func loadView() {
        super.loadView()
        createWebContentView()
        getWebPageContent()
    }

    func createWebContentView() ->  Void {
        
        webContentView.translatesAutoresizingMaskIntoConstraints = false
        webContentView.backgroundColor = UIColor.clear
        
        let  webView = UIView()
        webView.addSubview(webContentView)
        
        self.view.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[webviewIns]|", options:[], metrics:nil, views: ["webviewIns": webView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webviewIns]|", options:[], metrics:nil, views: ["webviewIns": webView]))
        
        
        let subViewsDict = ["webview": webContentView]
        webView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[webview]|", options:[], metrics:nil, views: subViewsDict))
        webView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webview]|", options:[], metrics:nil, views: subViewsDict))
    }
    
    func getWebPageContent() -> Void
    {
        let reqObj = StaticPageRequest()
        showIndicator("Loading...")
        if(staticPage != nil && staticPage != "") {
            reqObj.page_key = staticPage
        } else {
            if(touType != "" && touType == "specific") {
                 reqObj.page_key = "terms-workplace"
            } else {
                reqObj.page_key = "terms"
            }
        }
        
        StaticPageRequestor().sendStaticPageRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! StaticPageResponse).code == "471"
            {
                if (object as! StaticPageResponse).dataObj?.first != nil
                {
                    
                    if (object as! StaticPageResponse).dataObj?.first?.key != nil
                    {
                        self.webContentView.loadHTMLString(((object as! StaticPageResponse).dataObj?.first?.key)!, baseURL: nil)
                    }
                }
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
}
