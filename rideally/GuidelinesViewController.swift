//
//  GuidelinesViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/18/16.
//  Copyright © 2016 rideally. All rights reserved.
//
import UIKit

class GuidelinesItem: NSObject {
    var title: String!
    
    init(title: String) {
        self.title = title
    }
}


class GuidelinesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let tableView = UITableView()
    var dataSource = [GuidelinesItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        createDataSource()
        setUpTableView()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        self.tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Guidelines"
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createDataSource() {
        
        let zero = GuidelinesItem(title: "Terms Of Use")
        let first = GuidelinesItem(title: "Privacy Policy")
        let second = GuidelinesItem(title: "Terms And Conditions For Airport")
        let third = GuidelinesItem(title: "Terms And Conditions For Share A Taxi")
        let fourth = GuidelinesItem(title: "User Guidelines")
        let fifth = GuidelinesItem(title: "Driver Guidelines")
        
        dataSource.append(zero)
        dataSource.append(first)
        dataSource.append(second)
        dataSource.append(third)
        dataSource.append(fourth)
        dataSource.append(fifth)
    }
    
    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.showsVerticalScrollIndicator = false
        //tableView.separatorColor = UIColor.clearColor()
        tableView.separatorColor = Colors.GRAY_COLOR
        tableView.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(tableView)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-14-[table]|", options: [], metrics: nil, views: ["table":tableView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "Cell"
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier)
        
        let object = self.dataSource[indexPath.row]
        cell.textLabel?.text = object.title
        cell.textLabel?.textColor = Colors.BLACK_COLOR
        cell.textLabel?.font = normalFontWithSize(16)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func onSelectionRow(_ indexPath: IndexPath)
    {
        if((indexPath.row >= 0) && (indexPath.row <= 5)){
            let vc = StaticPagesViewController()
            if(indexPath.row == 0) {
                if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                    vc.staticPage = "terms-workplace"
                } else {
                    vc.staticPage = "terms"
                }
            } else if(indexPath.row == 1) {
                vc.staticPage = "privacypolicy"
            } else if(indexPath.row == 2) {
                vc.staticPage = "blr-airport-tc"
            } else if(indexPath.row == 3) {
                vc.staticPage = "sharedtaxi-T&C"
            } else if(indexPath.row == 4) {
                vc.staticPage = "guidelines"
            } else if(indexPath.row == 5) {
                vc.staticPage = "driverguidelines"
            }
            //self.presentViewController(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        self.tableView(tableView, didUnhighlightRowAt: indexPath)
        //self.evo_drawerController?.toggleDrawerSide(.Left, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.backgroundColor = UIColor.lightGray
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            onSelectionRow(indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        UIView.animate(withDuration: 0.2, animations: {
            cell?.backgroundColor = UIColor.clear
        }) 
    }
}
