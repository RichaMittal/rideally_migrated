//
//  RideOptionsViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/17/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class RideOptionsViewController: BaseScrollViewController {
    
    var  loaderView : UIView!
    var activityView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createRideOptionsView()
        if AppDelegate.isIPhone5() {
            print("iphoen5")
        } else {
            print("iphoen6")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavBar()
    }
    
    override func goBack() {
        let vc = PoolOptionsViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        if AppDelegate.isIPhone5() {
            labelObject.font = boldFontWithSize(16)
        } else {
            labelObject.font = boldFontWithSize(18)
        }
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.BLACK_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        labelObject.lineBreakMode = NSLineBreakMode.byWordWrapping
        return labelObject
    }
    
    func getSubHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        if AppDelegate.isIPhone5() {
            labelObject.font = boldFontWithSize(14)
        } else {
            labelObject.font = boldFontWithSize(17)
        }
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GRAY_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getORLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = boldFontWithSize(20)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GREEN_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(22)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    func createRideOptionsView()->Void
    {
        let  lblHeading  =  getHeadingLabelSubViews("HOW WOULD YOU LIKE TO CAR/BIKE POOL?")
        let  lblSubHeading  =  getSubHeadingLabelSubViews("You can change your preferences later")
        let  btnTaker =  getButtonSubViews("As a Ride Taker")
        btnTaker.tag = 102
        let  lblTaker  =  getSubHeadingLabelSubViews("You can get rides at low cost")
        lblTaker.font = normalFontWithSize(15)
        let  lblOr        =  getORLabelSubViews("OR")
        let  btnGiver =  getButtonSubViews("As a Ride Giver")
        btnGiver.tag = 103
        let  lblGiver  =  getSubHeadingLabelSubViews("You can offer your vehicle to others")
        lblGiver.font = normalFontWithSize(15)
        
        let  rideOptionView = UIView()
        self.view.addSubview(rideOptionView)

        rideOptionView.addSubview(lblHeading)
        rideOptionView.addSubview(lblSubHeading)
        rideOptionView.addSubview(btnTaker)
        rideOptionView.addSubview(lblTaker)
        rideOptionView.addSubview(lblOr)
        rideOptionView.addSubview(btnGiver)
        rideOptionView.addSubview(lblGiver)
        
        
        rideOptionView.translatesAutoresizingMaskIntoConstraints = false
        lblHeading.translatesAutoresizingMaskIntoConstraints = false
        lblSubHeading.translatesAutoresizingMaskIntoConstraints = false
        btnTaker.translatesAutoresizingMaskIntoConstraints = false
        lblTaker.translatesAutoresizingMaskIntoConstraints = false
        lblOr.translatesAutoresizingMaskIntoConstraints = false
        btnGiver.translatesAutoresizingMaskIntoConstraints = false
        lblGiver.translatesAutoresizingMaskIntoConstraints = false
//        
//        lblHeading.adjustsFontSizeToFitWidth = true
//        lblHeading.minimumScaleFactor = 0.5
//        lblHeading.numberOfLines = 1
//        lblSubHeading.adjustsFontSizeToFitWidth = true
//        lblSubHeading.minimumScaleFactor = 0.5
//        lblSubHeading.numberOfLines = 1
//        lblTaker.adjustsFontSizeToFitWidth = true
//        lblTaker.minimumScaleFactor = 0.5
//        lblTaker.numberOfLines = 1
//        lblGiver.adjustsFontSizeToFitWidth = true
//        lblGiver.minimumScaleFactor = 0.5
//        lblGiver.numberOfLines = 1
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[loginOptView(350)]", options:[], metrics:nil, views: ["loginOptView": rideOptionView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[loginOptView]|", options:[], metrics:nil, views: ["loginOptView": rideOptionView]))
        
        self.view.addConstraint(NSLayoutConstraint(item: rideOptionView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        
        let subViewsDict = ["lblHeading":lblHeading,"lblSubHeading":lblSubHeading,"orValue":lblOr,"btnTaker":btnTaker, "lblTaker":lblTaker, "btnGiver":btnGiver, "lblGiver":lblGiver]
        rideOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[lblHeading(20)]-3-[lblSubHeading(20)]-20-[btnTaker(45)]-3-[lblTaker(20)]-25-[orValue(20)]-25-[btnGiver(45)]-3-[lblGiver(20)]", options:[], metrics:nil, views: subViewsDict))
        rideOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[btnTaker]-15-|", options:[], metrics:nil, views: subViewsDict))
        rideOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[btnGiver]-15-|", options:[], metrics:nil, views: subViewsDict))
        rideOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblHeading]-15-|", options:[], metrics:nil, views: subViewsDict))
        rideOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblSubHeading]-15-|", options:[], metrics:nil, views: subViewsDict))
        rideOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblTaker]-15-|", options:[], metrics:nil, views: subViewsDict))
        rideOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblGiver]-15-|", options:[], metrics:nil, views: subViewsDict))
        
        rideOptionView.addConstraint(NSLayoutConstraint(item: lblHeading, attribute: .centerX, relatedBy: .equal, toItem: rideOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        rideOptionView.addConstraint(NSLayoutConstraint(item: lblSubHeading, attribute: .centerX, relatedBy: .equal, toItem: rideOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        rideOptionView.addConstraint(NSLayoutConstraint(item: btnTaker, attribute: .centerX, relatedBy: .equal, toItem: rideOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        rideOptionView.addConstraint(NSLayoutConstraint(item: lblTaker, attribute: .centerX, relatedBy: .equal, toItem: rideOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        
        rideOptionView.addConstraint(NSLayoutConstraint(item: lblOr, attribute: .centerX, relatedBy: .equal, toItem: rideOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        rideOptionView.addConstraint(NSLayoutConstraint(item: btnGiver, attribute: .centerX, relatedBy: .equal, toItem: rideOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        rideOptionView.addConstraint(NSLayoutConstraint(item: lblGiver, attribute: .centerX, relatedBy: .equal, toItem: rideOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        //[rideOptionView.addConstraint(NSLayoutConstraint(item: rideOptionView, attribute: .Height, relatedBy: .Equal, toItem: rideOptionView, attribute: .Width, multiplier: 1, constant: 0))]
    }
    
    func buttonPressed (_ sender: AnyObject)
    {
        if   sender.tag == 102
        {
            UserInfo.sharedInstance.isRideTakerClicked = true
            if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1")
            {
                let vc = WpSearchViewController()
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                UserInfo.sharedInstance.isGroupOptionsIn = true
                let vc = GroupOptionsViewController()
                vc.btnTag = String(sender.tag)
                self.navigationController?.pushViewController(vc, animated: true)
            }
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Welcome Page",
                AnalyticsParameterItemName: "Before Login Home",
                AnalyticsParameterContentType:"Sign Up Button"
                ])
        }
        
        if sender.tag == 103
        {
            UserInfo.sharedInstance.isRideTakerClicked = false
             RideRequestor().getDefaultVehicle({ (success, object) in
                if(success) {
                    if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1")
                    {
                        let vc = WpSearchViewController()
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        UserInfo.sharedInstance.isGroupOptionsIn = true
                        let vc = GroupOptionsViewController()
                        vc.btnTag = String(sender.tag)
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    let vc = RegisterNewVehicleController()
                    vc.signupFlow = true
                    vc.btnTag = String(sender.tag)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
             }) { (error) in
                hideIndicator()
            }
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Welcome Page",
                AnalyticsParameterItemName: "Before Login Home",
                AnalyticsParameterContentType:"Login Button"
                ])
        }
    }  
}
