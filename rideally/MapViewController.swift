//
//  MapViewController.swift
//  rideally
//
//  Created by Sarav on 08/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource,GMSMapViewDelegate {

    let cell_height: CGFloat = 40.0
    let big_cell_height: CGFloat = 60.0
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var mapView:GMSMapView!
    let searchTextField = UITextField()
    var placeSearchResultsArray = [AnyObject]()
    var mapCenterPinImageVIew = UIImageView()
    
    var suggestionView = UIView()
    var currentLocationView = UIView()
    var promptTableView = UITableView()
    var tableHeightConstraint: NSLayoutConstraint?
    var userMovedMap: Bool = false
    
    var locationButton: UIButton = UIButton(type: .system)
    
    let appDelegate: AppDelegate = sharedAppDelegate()!
    var locationButtonAction: VoidCompletionHandler?
    
    var shouldShowCancelButton = false
    
    var cancelButton = UIButton(type: .system)
    var currentLocationButton = UIButton(type: .custom)
    var locationName: String = ""
    
    let NormalAddressCell = "NormalAddressCell"
    
    var onSelectionBlock: actionBlockWithParam?

    var jumpOneScreen: Bool = false
    var isFromWP: Bool = false
    
    func goBack()
    {
        if(jumpOneScreen && !isFromWP){
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
        }
        else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;

        
        // Do any additional setup after loading the view, typically from a nib.
        mapCenterPinImageVIew.image = UIImage(imageLiteralResourceName: "btn_set_location")
        
        title = "Choose Location"
        self.searchTextField.delegate = self
        setupTextfield()
        //setupCurrentLocationView()
        
//        if((UserInfo.sharedInstance.userLatitude != 0) && (UserInfo.sharedInstance.userLongitude != 0)) {
//            self.locationCoord = CLLocationCoordinate2DMake(UserInfo.sharedInstance.userLatitude, UserInfo.sharedInstance.userLongitude)
//            searchTextField.text = UserInfo.sharedInstance.locationName
//            locationName = UserInfo.sharedInstance.locationName
//        }
        
        self.setupGoogleMaps()
        searchTextField.font = boldFontWithSize(15.0)
        searchTextField.becomeFirstResponder()
        //setupLowerButtonButton()
    }
    
    func setupLowerButtonButton () {
        let currentLocationImage = UIImage(named: "ic_current_location")
        currentLocationButton.translatesAutoresizingMaskIntoConstraints = false
        currentLocationButton.setImage(currentLocationImage, for: UIControlState())
        currentLocationButton.addTarget(self, action: #selector(goToCurrentLocation(_:)), for: .touchUpInside)
        self.view.addSubview(currentLocationButton)
        
        let dict = ["currentLocationButton": currentLocationButton, "cancelButton": cancelButton]
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[currentLocationButton(60)]-16-|", options: [], metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[currentLocationButton(60)]-|", options: [], metrics: nil, views: dict))
        
        let dimension: CGFloat = 50
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
        cancelButton.backgroundColor = Colors.RED_COLOR
        cancelButton.setTitle("X", for: UIControlState())
        cancelButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        cancelButton.titleLabel?.font = boldFontWithSize(dimension/2.0)
        cancelButton.layer.cornerRadius = dimension/2.0
        cancelButton.isHidden = !shouldShowCancelButton
        self.view.addSubview(cancelButton)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[cancelButton(\(dimension))]-16-|", options: [], metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[cancelButton(\(dimension))]", options: [], metrics: nil, views: dict))
        
    }
    
    func cancelButtonAction () {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.locationCoord == nil {
            self.getCurrentLocation()
        }
        
        self.setupPromptView()
        //searchTextField.addShadow(Colors.BLACK_COLOR, shadowRadius: 2.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showPromptTextFieldAndSuggestion () {
        self.searchTextField.isHidden = false
        //self.searchTextField.alpha = 0
        self.suggestionView.alpha = 0
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.searchTextField.alpha = 1
            self.suggestionView.alpha = 1
        }) 
    }
    
    func setupPromptView () {
        currentLocationView.translatesAutoresizingMaskIntoConstraints = false
        currentLocationView.backgroundColor = Colors.WHITE_COLOR
        promptTableView.translatesAutoresizingMaskIntoConstraints = false
        promptTableView.delegate = self
        promptTableView.dataSource = self
        suggestionView.addSubview(currentLocationView)
        suggestionView.addSubview(promptTableView)
        
        // layout constraints
        let dict = ["currentLocationView" : currentLocationView, "promptTableView" : promptTableView]
        suggestionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[currentLocationView][promptTableView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        suggestionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[currentLocationView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        suggestionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[promptTableView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        suggestionView.addConstraint(NSLayoutConstraint(item: currentLocationView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: suggestionView, attribute: NSLayoutAttribute.width, multiplier: 1.0, constant: 0))
        suggestionView.addConstraint(NSLayoutConstraint(item: promptTableView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: suggestionView, attribute: NSLayoutAttribute.width, multiplier: 1.0, constant: 0))
        
        suggestionView.addConstraint(NSLayoutConstraint(item: currentLocationView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: 0))
        
        tableHeightConstraint = NSLayoutConstraint(item: promptTableView, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1.0, constant: 0)
        suggestionView.addConstraint(tableHeightConstraint!)
        
        suggestionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(suggestionView)
        
        self.view.addConstraint(NSLayoutConstraint(item: suggestionView, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: searchTextField, attribute: NSLayoutAttribute.bottom, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: suggestionView, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: searchTextField, attribute: NSLayoutAttribute.leading, multiplier: 1.0, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: suggestionView, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: searchTextField, attribute: NSLayoutAttribute.width, multiplier: 1.0, constant: 0))
        
        showPromptTextFieldAndSuggestion()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        suggestionView.roundCorners([.bottomLeft , .bottomRight], size: CGSize(width: 5.0, height: 5.0))
        
//        self.locationButton.layer.cornerRadius = CGRectGetHeight(self.locationButton.bounds)/2.0
//        searchTextField.addBottomBorder(0.5, color: Colors.GENERAL_BORDER_COLOR, offset: CGPointZero)
    }
    
    func setupGoogleMaps () {
        
        if let coord = self.locationCoord {
            let camera = GMSCameraPosition.camera(withLatitude: coord.latitude,
                                                              longitude: coord.longitude, zoom: 16)
            self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        } else {
            let camera = GMSCameraPosition.camera(withLatitude: 20.593684, longitude: 78.962880, zoom: 16)
            self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        }
        
        self.mapView.delegate = self
        //self.mapView.settings.myLocationButton = true
        self.mapView.isMyLocationEnabled = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        self.view.sendSubview(toBack: self.mapView)
        
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        searchTextField.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(searchTextField)
        
        let dict = ["mapView": mapView, "searchtxt":searchTextField] as [String : Any]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[searchtxt(40)][mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[searchtxt]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        // pin layout
        mapCenterPinImageVIew.translatesAutoresizingMaskIntoConstraints = false
        self.mapView.addSubview(self.mapCenterPinImageVIew)
        
        self.mapView.addConstraint(NSLayoutConstraint(item: self.mapCenterPinImageVIew, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 0))
        self.mapView.addConstraint(NSLayoutConstraint(item: self.mapCenterPinImageVIew, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: -(mapCenterPinImageVIew.image?.size.height)!/2.0 + 10))
        
        // button layout
        setupLocationButton()
        self.mapView.addSubview(self.locationButton)
        
        
        self.mapView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":self.locationButton]))
        self.mapView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]|", options: [], metrics: nil, views: ["btn":self.locationButton]))
        
//        self.mapView.addConstraint(NSLayoutConstraint(item: self.locationButton, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
//        self.mapView.addConstraint(NSLayoutConstraint(item: self.locationButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 10))
//        self.mapView.addConstraint(NSLayoutConstraint(item: self.locationButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 40))
    }
    
    func setupLocationButton () {
        self.locationButton.backgroundColor = Colors.GRAY_COLOR
        self.locationButton.setTitle("SAVE LOCATION", for: UIControlState())
        self.locationButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        self.locationButton.titleLabel?.font = boldFontWithSize(16.0)
        self.locationButton.translatesAutoresizingMaskIntoConstraints = false
        self.locationButton.addTarget(self, action: #selector(selectLocationButtonTapped(_:)), for: UIControlEvents.touchUpInside)
    }
    
    func setupTextfield () {
        let image = UIImage(named: "ic_location")
        let frame = CGRect(x: 0, y: 0, width: (image?.size.width)!, height: (image?.size.height)!)
        let leftV = UIImageView(frame: frame)
        leftV.image = image
        leftV.contentMode = UIViewContentMode.center
        
        searchTextField.placeholder = "Enter your location"
        self.searchTextField.leftView = leftV
        self.searchTextField.leftViewMode = UITextFieldViewMode.always
        self.searchTextField.clearButtonMode = UITextFieldViewMode.whileEditing
    }
    
    func getCurrentLocation () {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        
        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .denied {
                //                AlertController.showAlertForMessage("You have denied access to your location, please enable it in settings.")
                callLocationServiceEnablePopUp()
                return
            }
        }
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else {
            print("location service not enabled")
        }
    }
    
    func callLocationServiceEnablePopUp() -> Void
    {
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedWhenInUse, .restricted, .denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order to be notified, please open this app's settings and set location access to 'Always'.",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        default:
            break
            
            
        }
    }

    
    // MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationCoord = manager.location!.coordinate
        self.moveMapToCoord(self.locationCoord)
        reverseGeocodeCoordinate(self.locationCoord)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
        } else if status == .denied {
           // AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
            callLocationServiceEnablePopUp()
        }
    }
    
    //MARK:- Text Field Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        /*let incrementer = (string == "") ? -1 : 1  //such that search is made when exact 4 characters has been typed
         if ((textField.text?.characters.count)! + incrementer) >= 4 {
         let searchText = textField.text! + string
         self.findSearchResultsForText(searchText)
         }*/
        
        let searchText = textField.text! + string
        self.findSearchResultsForText(searchText)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        resetAddressTable()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
        }
        
        return true
    }
    
    //MARK:- place search
    func findSearchResultsForText(_ searchText: String) {
        let characterSet = CharacterSet.letters
        let range = searchText.rangeOfCharacter(from: characterSet)
        if (range != nil) {
            self.getPlacesForText(searchText)
        } else {
            self.getPincodesForText(searchText)
        }
    }
    
    func getPincodesForText(_ searchText: String) {
    }
    
    func getPlacesForText(_ searchText: String) {
        let placesClient = GMSPlacesClient()
        
        let filter = GMSAutocompleteFilter()
        filter.country = INDIA_COUNTRY_CODE
        filter.type = GMSPlacesAutocompleteTypeFilter.geocode
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: filter) { [weak self] (results, error:Error?) -> Void in
            self?.placeSearchResultsArray.removeAll()
            if results == nil {
                return
            }
            self?.placeSearchResultsArray = results! as [AnyObject]
            self?.presentAddressData()
        }
    }
    
    let MAX_ROWS = 5
    let section_height: CGFloat = 20
    func presentAddressData () {
        let count = self.placeSearchResultsArray.count > MAX_ROWS ? MAX_ROWS : self.placeSearchResultsArray.count
        let height_cell = cell_height
        let extra = count != 0 ? section_height : 0
        self.tableHeightConstraint?.constant = CGFloat(count) * height_cell + extra
        self.promptTableView.reloadData()
        self.suggestionView.layoutIfNeeded()
    }
    
    // MARK:- Table View Delegate and Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeSearchResultsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cell_height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellIdentifier: String?
        var text: String?
        let loadRecentAddressCell: Bool = false

            cellIdentifier = NormalAddressCell
            let object: GMSAutocompletePrediction = self.placeSearchResultsArray[indexPath.row] as! GMSAutocompletePrediction
            text = object.attributedFullText.string
        
        var cell = UITableViewCell()
        if loadRecentAddressCell == false {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier!)
            cell.textLabel?.text = text!
            cell.textLabel?.font = boldFontWithSize(13.0)
        }
//        else {
//            let addrCell = UITableViewCell()
//            return addrCell
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let object: GMSAutocompletePrediction = self.placeSearchResultsArray[indexPath.row] as! GMSAutocompletePrediction
            searchTextField.text = object.attributedFullText.string
            locationName = object.attributedFullText.string
            performDidSelectForGoogleAPI(object)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Select an address"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header:UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.font = boldFontWithSize(11)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section_height
    }
    
    //MARK:- Navigation to Master Service
    func performDidSelectForGoogleAPI (_ placeObject: GMSAutocompletePrediction) {
        let client = GMSPlacesClient()
        client.lookUpPlaceID(placeObject.placeID!) { [weak self] (place, error) -> Void in
            if let coord = place?.coordinate {
                self?.moveMapToCoord(coord)
            }
        }
        
        // added to reset tableView on selection
        searchTextField.resignFirstResponder()
        resetAddressTable()
    }
    
    func resetAddressTable () {
        self.placeSearchResultsArray.removeAll()
        presentAddressData()
    }
    
    func selectLocationButtonTapped (_ sender: AnyObject!) {
        
        if let completionHandler = locationButtonAction {
            completionHandler()
        } else {
            if let coord = self.locationCoord{
                proceedWithSelectedLatLong(coord)
            }
        }
    }
    
    func proceedWithSelectedLatLong (_ coord : CLLocationCoordinate2D) {
//        self.navigationController?.popViewControllerAnimated(true)
        goBack()
        onSelectionBlock?(["lat":"\(coord.latitude)", "long":"\(coord.longitude)", "location":searchTextField.text!])
    }
    
    func getDataForCoord (_ coord : CLLocationCoordinate2D) {
    }
    
    func goToCurrentLocation (_ sender: UIButton) {
        if let location = mapView.myLocation {
            self.userMovedMap = true
            moveMapToCoord(location.coordinate)
        }
        
    }
    
    // MARK:- Google Map Delegates
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        /*self.locationCoord = coordinate
         self.moveMapToCoord(self.locationCoord)
         reverseGeocodeCoordinate(self.locationCoord)*/
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.userMovedMap = self.userMovedMap ? self.userMovedMap : gesture
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if self.userMovedMap == true {
            self.userMovedMap = false
            self.locationCoord = position.target
            self.moveMapToCoord(self.locationCoord)
            reverseGeocodeCoordinate(self.locationCoord)
        }
    }
    
    func moveMapToCoord(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        self.locationCoord = coordinate
        let camera = self.mapView.camera
        let cameraPosition = GMSCameraPosition.camera(withTarget: coord, zoom: camera.zoom)
        self.mapView.animate(to: cameraPosition)
    }
    
    // MARK:- Reverse Geocode
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        let geocoder = GMSGeocoder()
        searchTextField.lock()
        geocoder.reverseGeocodeCoordinate(coord) { [weak self] response , error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]!
                self?.searchTextField.text = lines?.joined(separator: "\n")
                if let text = self?.searchTextField.text {
                    self?.locationName = text
                }
                self?.searchTextField.unlock()
            }
        }
    }

    
}
