//
//  MemberWindowInfoView.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/17/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class MemberWindowInfoView: UIView {

    @IBOutlet weak var profile: UIImageView!
    
    @IBOutlet weak var memberName: UILabel!
    @IBOutlet weak var homeAddress: UILabel!
    
    @IBOutlet weak var userDetailBtn: UIButton!
    @IBOutlet weak var rideWithMeBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profile.layer.cornerRadius = 25.0
        self.profile.layer.masksToBounds = true
    }
}
