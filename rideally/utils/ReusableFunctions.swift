//
//  ReusableFunctions.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import Foundation
import UIKit

func topMostController() -> UIViewController? {
    
    var presentedVC = UIApplication.shared.keyWindow?.rootViewController
    while let pVC = presentedVC?.presentedViewController
    {
        presentedVC = pVC
    }
    
    if presentedVC == nil {
        print("Error: You don't have any views set. You may be calling in viewdidload. Try viewdidappear.")
    }
    
    return presentedVC
}

func boldFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FONT_NAME.BOLD_FONT, size: size)
    return font
}

func normalFontWithSize (_ size: CGFloat) -> UIFont? {
    let font = UIFont(name: FONT_NAME.NORMAL_FONT, size: size)
    return font
}

func radians(_ degrees: Double) -> Double {
    return M_PI * degrees / 180.0
}

func sharedAppDelegate() -> AppDelegate? {
    return UIApplication.shared.delegate as? AppDelegate;
}

func isServerReachable () -> Bool {
    var reachable = false
    let status = Reach().connectionStatus()
    switch status {
    case .unknown, .offline:
        reachable = false
    default:
        reachable = true
    }
    
    return reachable
}

func showIndicator(_ message: String) {
    _ = EZLoadingActivity.show(message, disableUI: true)
}

func hideIndicator() {
    _ = EZLoadingActivity.hide()
}

func getSegmentControl (_ sectiontitles: [String]?, indexChangeBlock: IndexChangeBlock?) -> HMSegmentedControl {
    let segmentControl = HMSegmentedControl()
    segmentControl.sectionTitles = sectiontitles
    segmentControl.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(15)!]
    segmentControl.selectedTitleTextAttributes = [NSForegroundColorAttributeName: Colors.RED_COLOR]
    segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown
    segmentControl.selectionIndicatorColor = Colors.GENERAL_BORDER_COLOR
    segmentControl.borderColor = Colors.GENERAL_BORDER_COLOR
    segmentControl.borderType = .right
    segmentControl.verticalDividerColor = Colors.GENERAL_BORDER_COLOR
    if indexChangeBlock != nil {
        segmentControl.indexChangeBlock = indexChangeBlock
    }
    return segmentControl
}

func isValidEmail(_ testStr:String?) -> Bool {
    guard testStr != nil else {
        return false
    }
    print("validate emilId: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let result = emailTest.evaluate(with: testStr)
    return result
}

func isValidMobile(_ value: String?) -> Bool {
    guard value != nil else {
        return false
    }
    let PHONE_REGEX = "^\\d{10}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    let result =  phoneTest.evaluate(with: value)
    return result
}

func isValidOTP(_ value: String?) -> Bool {
    guard value != nil else {
        return false
    }
    let PHONE_REGEX = "^\\d{4}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    let result =  phoneTest.evaluate(with: value)
    return result
}

func isValidEmailFormate(_ stringValue:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: stringValue)
}

func isNumberOnly(_ stringValue:String) -> Bool {
    let invalidCharacters = CharacterSet.decimalDigits.inverted
    let rangeOne = stringValue.characters.startIndex
    let rangeTwo = stringValue.characters.endIndex
    let stringRange = Range(uncheckedBounds: (lower: rangeOne, upper: rangeTwo))
    return stringValue.rangeOfCharacter(from: invalidCharacters, options: [], range: stringRange) == nil
    //return stringValue.rangeOfCharacter(from: invalidCharacters, options: [], range: stringValue.characters.indices) == nil
}

func isAlphabetsOnly(_ stringValue:String) -> Bool {
    for chr in stringValue.characters {
        if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
            return false
        }
    }
    return true
}

//func print(items: Any..., separator: String = "", terminator: String = "") {
//    #if DEBUG
//        Swift.debugPrint(items, separator: separator, terminator: terminator)
//    #endif
//}

func doneAccessoryView (_ target: AnyObject) -> UIView {
    let accessoryView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 40))
    accessoryView.backgroundColor = UIColor.darkGray
    let btnDone = UIButton()
    btnDone.frame = CGRect(x: 5, y: 5, width: 50, height: 30)
    btnDone.setTitle("Done", for: UIControlState())
    btnDone.titleLabel?.font = boldFontWithSize(15)
    btnDone.addTarget(target, action: #selector(UIResponder.resignFirstResponder), for: .touchUpInside)
    accessoryView.addSubview(btnDone)
    
    return accessoryView
}

func prettyDateStringFromString(_ uglyString: String?, fromFormat: String?, toFormat: String?) -> String {
    guard uglyString != nil else {
        return ""
    }
    //print(uglyString)
    let fromDateFormat = fromFormat != nil ? fromFormat : "yyyy-MM-dd HH:mm:ss"
    let toDateFormat = toFormat != nil ? toFormat : "dd-MMM-yyyy | hh:mm a"
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = fromDateFormat
    guard let date = dateFormatter.date(from: uglyString!) else {
        return ""
    }
    
    dateFormatter.dateFormat = toDateFormat
    let prettyString = dateFormatter.string(from: date)
    
    return prettyString
}

func prettyDateStringFromDate(_ date: Date?, toFormat: String?) -> String {
    guard date != nil else {
        return ""
    }
    //print(uglyString)
    let toDateFormat = toFormat != nil ? toFormat : "dd-MMM-yyyy | hh:mm a"
    
    let dateFormatter = DateFormatter()
    
    dateFormatter.dateFormat = toDateFormat
    let prettyString = dateFormatter.string(from: date!)
    
    return prettyString
}

func getPriceString(_ value: CGFloat?) -> String
{
    guard let _ = value else {
        return "-"
    }
    return String(format: "%.2f", value!)
}

import CoreTelephony
func getNetworkConnectionType () -> String {
    let telephoneInfo = CTTelephonyNetworkInfo()
    let tech = telephoneInfo.currentRadioAccessTechnology
    return tech != nil ? tech! : "No Network"
}

func showAlertForAttributedText (_ text: String?, title: String?) {
    if let data = text?.data(using: String.Encoding.unicode) {
        do {
            let attributedText = try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            AlertController.showAlertForAttributedText(attributedText, title: title)
        } catch let error as NSError {
            print(error)
        }
    }
}


func imageNameForCurrentDevice(_ fileName: String) -> String
{
    let height = UIScreen.main.bounds.height
    
    var file = fileName
    
    switch height {
    case 480:
        break
    case 568:
        file = file + "-568"
        break
    case 667:
        file = file + "-667"
        break
    case 1104:
        break
    default:
        break
    }
    return file
}

func gotoAppStore () {
    UIApplication.shared.openURL(URL(string: "")!)
}

func removeYearFromDate(_ date: String) -> String
{
    let dateArray = date.components(separatedBy: " ")
    return "\(dateArray[0]) \(dateArray[1])"
}

func sendWhatsAppMsg(_ wpName: String, wpKey: String, poolID: String) {
    let name = UserInfo.sharedInstance.firstName
    var urlString = ""
    if(wpName != "") {
        urlString = "\(name) \("wants you to travel together with colleagues in secured place,") \(wpName)\(". Do join it at") \(SERVER)\("/workplace/")\(wpKey)"
    } else {
        urlString = name + " has invited you to join the ride on RideAlly. Please join ride at "+SERVER+"ride/ridedetail?pool_id=" + "\(poolID )"
    }
    let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
    let url  = URL(string: "whatsapp://send?text=\(urlStringEncoded!)")
    
    if UIApplication.shared.canOpenURL(url!) {
        UIApplication.shared.openURL(url!)
    }
}

func logoutUserAndGoToHomeScreen () {
    UserInfo.sharedInstance.logoutUser()
    
//    FBLogout()
//    GIDSignIn.sharedInstance().signOut()
//    GIDSignIn.sharedInstance().disconnect()
//    
//    sharedAppDelegate()?.checkForLoginVerification()
}

//import FBSDKLoginKit
//func FBLogout () {
//    let loginManager: FBSDKLoginManager = FBSDKLoginManager()
//    loginManager.logOut()
//}
