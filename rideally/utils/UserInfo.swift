//
//  UserInfo.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import UIKit


class UserInfo: NSObject {
    
    private static var __once: () = {
        //static.instanceMethod(for: loadFromDB())
           // static.instance = UserInfo()
           // static.instance!.loadFromDB()
        }()
    
    class var sharedInstance: UserInfo {
        struct Static {
            static var onceToken: Int = 0
            static var instance: UserInfo? = nil
        }
        //_ = UserInfo.__once
        Static.instance = UserInfo()
        Static.instance!.loadFromDB()
        return Static.instance!
    }
    
    
    var isLoggedIn: Bool = false{
        didSet{
            UserDefaults.standard.set(isLoggedIn, forKey: "logged")
            UserDefaults.standard.synchronize()
        }
    }

    
    var isProfilePicContent: Bool = false{
        didSet{
            UserDefaults.standard.set(isProfilePicContent, forKey: "isProfilePicContent")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isEmailIDVerified: Bool = false{
        didSet{
            UserDefaults.standard.set(isEmailIDVerified, forKey: "isEmailIDVerified")
            UserDefaults.standard.synchronize()
        }
    }

    var isProfilePicUpdatedSuccesfully: Bool = false{
        didSet{
            UserDefaults.standard.set(isProfilePicUpdatedSuccesfully, forKey: "isProfilePicUpdatedSuccesfully")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isSecEmailIDVerified: Bool = false{
        didSet{
            UserDefaults.standard.set(isSecEmailIDVerified, forKey: "isSecEmailIDVerified")
            UserDefaults.standard.synchronize()
        }
    }
    
    var secEmail: String = ""{
        didSet{
            UserDefaults.standard.set(secEmail, forKey: "secEmail")
            UserDefaults.standard.synchronize()
        }
    }
    
    var email: String = ""{
        didSet{
            UserDefaults.standard.set(email, forKey: "emailID")
            UserDefaults.standard.synchronize()
        }
    }

    var dob: String = ""{
        didSet{
            UserDefaults.standard.set(dob, forKey: "dob")
            UserDefaults.standard.synchronize()
        }
    }
    
    var mobile: String = ""{
        didSet{
            UserDefaults.standard.set(mobile, forKey: "mobile")
            UserDefaults.standard.synchronize()
        }
    }
    
    var userID: String = ""{
        didSet{
            UserDefaults.standard.set(userID, forKey: "userID")
            UserDefaults.standard.synchronize()
        }
    }
    
    var password: String = ""{
        didSet{
            UserDefaults.standard.set(password, forKey: "password")
            UserDefaults.standard.synchronize()
        }
    }
    
    var gender: String = ""{
        didSet{
            UserDefaults.standard.set(gender, forKey: "gender")
            UserDefaults.standard.synchronize()
        }
    }
    
    var firstName: String = ""{
        didSet{
            UserDefaults.standard.set(firstName, forKey: "firstname")
            UserDefaults.standard.synchronize()
        }
    }
    
    var lastName: String = ""{
        didSet{
            UserDefaults.standard.set(lastName, forKey: "lastname")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var fullName: String = ""{
        didSet{
            UserDefaults.standard.set(fullName, forKey: "fullName")
            UserDefaults.standard.synchronize()
        }
    }

    var refCode: String = ""{
        didSet{
            UserDefaults.standard.set(refCode, forKey: "refCode")
            UserDefaults.standard.synchronize()
        }
    }
    
    var userState: String = ""{
        didSet{
            UserDefaults.standard.set(userState, forKey: "userstate")
            UserDefaults.standard.synchronize()
        }
    }
    
    var profilepicStatus: String = ""{
        didSet{
            UserDefaults.standard.set(profilepicStatus, forKey: "profilepicStatus")
            UserDefaults.standard.synchronize()
        }
    }
    
    var profilepicUrl: String = ""{
        didSet{
            UserDefaults.standard.set(profilepicUrl, forKey: "profilepicUrl")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isOfficialEmail: String = ""{
        didSet{
            UserDefaults.standard.set(isOfficialEmail, forKey: "isOfficialEmail")
            UserDefaults.standard.synchronize()
        }
    }
    
    var hideAnywhere: String = ""{
        didSet{
            UserDefaults.standard.set(hideAnywhere, forKey: "hideAnywhere")
            UserDefaults.standard.synchronize()
        }
    }
    
    var touHideAnywhere: String = ""{
        didSet{
            UserDefaults.standard.set(touHideAnywhere, forKey: "touHideAnywhere")
            UserDefaults.standard.synchronize()
        }
    }
    
    var faceBookid: String = ""{
        didSet{
            UserDefaults.standard.set(faceBookid, forKey: "faceBookid")
            UserDefaults.standard.synchronize()
        }
    }
    
    var shareMobileNumber: Bool = false{
        didSet{
            UserDefaults.standard.set(shareMobileNumber, forKey: "sharemobilenumber")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var currentLocation: String = ""{
        didSet{
            UserDefaults.standard.set(currentLocation, forKey: "currentLocation")
            UserDefaults.standard.synchronize()
        }
    }
    
    var homeAddress: String = ""{
        didSet{
            UserDefaults.standard.set(homeAddress, forKey: "homeAddress")
            UserDefaults.standard.synchronize()
        }
    }
    
    var workAddress: String = ""{
        didSet{
            UserDefaults.standard.set(workAddress, forKey: "workAddress")
            UserDefaults.standard.synchronize()
        }
    }
    
    var workLatValue: String = ""{
        didSet{
            UserDefaults.standard.set(workLatValue, forKey: "workLatValue")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var workLongValue: String = ""{
        didSet{
            UserDefaults.standard.set(workLongValue, forKey: "workLongValue")
            UserDefaults.standard.synchronize()
        }
    }

    
    var homeLatValue: String = ""{
        didSet{
            UserDefaults.standard.set(homeLatValue, forKey: "homeLatValue")
            UserDefaults.standard.synchronize()
        }
    }

    var homeLongValue: String = ""{
        didSet{
            UserDefaults.standard.set(homeLongValue, forKey: "homeLongValue")
            UserDefaults.standard.synchronize()
        }
    }

    
    var defaultVehicleRegNo: String = ""{
        didSet{
            UserDefaults.standard.set(defaultVehicleRegNo, forKey: "defaultVehicleRegNo")
            UserDefaults.standard.synchronize()
        }
    }

    
    var vehicleSeqID: String = ""{
        didSet{
            UserDefaults.standard.set(vehicleSeqID, forKey: "vehicleSeqID")
            UserDefaults.standard.synchronize()
        }
    }

    
    var vehicleType: String = ""{
        didSet{
            UserDefaults.standard.set(vehicleType, forKey: "vehicleType")
            UserDefaults.standard.synchronize()
        }
    }

    
    var vehicleModel: String = ""{
        didSet{
            UserDefaults.standard.set(vehicleModel, forKey: "vehicleModel")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var vehicleRegNo: String = ""{
        didSet{
            UserDefaults.standard.set(vehicleRegNo, forKey: "vehicleRegNo")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var vehicleSeatCapacity: String = ""{
        didSet{
            UserDefaults.standard.set(vehicleSeatCapacity, forKey: "vehicleSeatCapacity")
            UserDefaults.standard.synchronize()
        }
    }
    
    var vehicleKind: String = ""{
        didSet{
            UserDefaults.standard.set(vehicleKind, forKey: "vehicleKind")
            UserDefaults.standard.synchronize()
        }
    }
    
    var vehicleOwnerName: String = ""{
        didSet{
            UserDefaults.standard.set(vehicleOwnerName, forKey: "vehicleOwnerName")
            UserDefaults.standard.synchronize()
        }
    }
    
    var insurerName: String = ""{
        didSet{
            UserDefaults.standard.set(insurerName, forKey: "insurerName")
            UserDefaults.standard.synchronize()
        }
    }
    
    var insuredCmpnyName: String = ""{
        didSet{
            UserDefaults.standard.set(insuredCmpnyName, forKey: "insuredCmpnyName")
            UserDefaults.standard.synchronize()
        }
    }
    
    var insuranceId: String = ""{
        didSet{
            UserDefaults.standard.set(insuranceId, forKey: "insuranceId")
            UserDefaults.standard.synchronize()
        }
    }

    var insuranceExpiryDate: String = ""{
        didSet{
            UserDefaults.standard.set(insuranceExpiryDate, forKey: "insuranceExpiryDate")
            UserDefaults.standard.synchronize()
        }
    }
    
    var insUploadStatus: String = ""{
        didSet{
            UserDefaults.standard.set(insUploadStatus, forKey: "insUploadStatus")
            UserDefaults.standard.synchronize()
        }
    }
    
    var insUrl: String = ""{
        didSet{
            UserDefaults.standard.set(insUrl, forKey: "insUrl")
            UserDefaults.standard.synchronize()
        }
    }
    
    var rcUploadStatus: String = ""{
        didSet{
            UserDefaults.standard.set(rcUploadStatus, forKey: "rcUploadStatus")
            UserDefaults.standard.synchronize()
        }
    }
    
    var rcUrl: String = ""{
        didSet{
            UserDefaults.standard.set(rcUrl, forKey: "rcUrl")
            UserDefaults.standard.synchronize()
        }
    }
    
    var earnedPointsViaRecharge: String = ""{
        didSet{
            UserDefaults.standard.set(earnedPointsViaRecharge, forKey: "earnedPointsViaRecharge")
            UserDefaults.standard.synchronize()
        }
    }

    
    var earnedPointsViaEncash: String = ""{
        didSet{
            UserDefaults.standard.set(earnedPointsViaEncash, forKey: "earnedPointsViaEncash")
            UserDefaults.standard.synchronize()
        }
    }
    
    
    var profilepicfileFormat: String = ""{
        didSet
        {
            UserDefaults.standard.set(profilepicfileFormat, forKey: "profilepicfileFormat")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isDefaultEmergencyNumberSelected: String = ""{
        didSet
        {
            UserDefaults.standard.set(isDefaultEmergencyNumberSelected, forKey: "isDefaultEmergencyNumberSelected")
            UserDefaults.standard.synchronize()
        }
    }

    var selectedEmergencyNumber: String = ""{
        didSet
        {
            UserDefaults.standard.set(selectedEmergencyNumber, forKey: "selectedEmergencyNumber")
            UserDefaults.standard.synchronize()
        }
    }
    
    var selectedEmergencyEmail: String = ""{
        didSet
        {
            UserDefaults.standard.set(selectedEmergencyEmail, forKey: "selectedEmergencyEmail")
            UserDefaults.standard.synchronize()
        }
    }
    
    var deviceToken: String = ""{
        didSet
        {
            UserDefaults.standard.set(deviceToken, forKey: "deviceToken")
            UserDefaults.standard.synchronize()
        }
    }
    
    var allRides: String = ""{
        didSet
        {
            UserDefaults.standard.set(allRides, forKey: "allRides")
            UserDefaults.standard.synchronize()
        }
    }
    
    var allWorkplaces: String = ""{
        didSet
        {
            UserDefaults.standard.set(allWorkplaces, forKey: "allWorkplaces")
            UserDefaults.standard.synchronize()
        }
    }
    
    var myWorkplaces: String = ""{
        didSet
        {
            UserDefaults.standard.set(myWorkplaces, forKey: "myWorkplaces")
            UserDefaults.standard.synchronize()
        }
    }
    
    var myPendingWorkplaces: String = ""{
        didSet
        {
            UserDefaults.standard.set(myPendingWorkplaces, forKey: "myPendingWorkplaces")
            UserDefaults.standard.synchronize()
        }
    }
    
    var offeredRides: String = ""{
        didSet
        {
            UserDefaults.standard.set(offeredRides, forKey: "offeredRides")
            UserDefaults.standard.synchronize()
        }
    }
    
    var neededRides: String = ""{
        didSet
        {
            UserDefaults.standard.set(neededRides, forKey: "neededRides")
            UserDefaults.standard.synchronize()
        }
    }
    
    var joinedRides: String = ""{
        didSet
        {
            UserDefaults.standard.set(joinedRides, forKey: "joinedRides")
            UserDefaults.standard.synchronize()
        }
    }
    
    var bookedRides: String = ""{
        didSet
        {
            UserDefaults.standard.set(bookedRides, forKey: "bookedRides")
            UserDefaults.standard.synchronize()
        }
    }
    
    var maxAllowedPoints: String = ""{
        didSet
        {
            UserDefaults.standard.set(maxAllowedPoints, forKey: "maxAllowedPoints")
            UserDefaults.standard.synchronize()
        }
    }
    
    var availablePoints: Int = 0{
        didSet
        {
            UserDefaults.standard.set(availablePoints, forKey: "availablePoints")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isRideOptionsIn: Bool = false{
        didSet{
            UserDefaults.standard.set(isRideOptionsIn, forKey: "rideOptions")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isGroupOptionsIn: Bool = false{
        didSet{
            UserDefaults.standard.set(isGroupOptionsIn, forKey: "groupOptions")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isRideTakerClicked: Bool = false{
        didSet{
            UserDefaults.standard.set(isRideTakerClicked, forKey: "rideTaker")
            UserDefaults.standard.synchronize()
        }
    }
    
    var isTouAgreed: Bool = false{
        didSet{
            UserDefaults.standard.set(isTouAgreed, forKey: "isTouAgreed")
            UserDefaults.standard.synchronize()
        }
    }
    
//    var allVehicleList: [AnyObject] = [ ]{
//        
//    }

    
    
    fileprivate func loadFromDB()
    {
        if let temp = UserDefaults.standard.object(forKey: "isSecEmailIDVerified") as? Bool{
            isSecEmailIDVerified = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "secEmail") as? String{
            secEmail = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "emailID") as? String{
            email = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "mobile") as? String{
            mobile = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "userID") as? String{
            userID = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "password") as? String{
            password = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "logged") as? Bool{
            isLoggedIn = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "isProfilePicContent") as? Bool{
            isProfilePicContent = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "isEmailIDVerified") as? Bool{
            isEmailIDVerified = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "isProfilePicUpdatedSuccesfully") as? Bool{
            isProfilePicUpdatedSuccesfully = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "sharemobilenumber") as? Bool{
            shareMobileNumber = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "gender") as? String{
            gender = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "firstname") as? String{
            firstName = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "lastname") as? String{
            lastName = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "userstate") as? String{
            userState = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "profilepicStatus") as? String{
            profilepicStatus = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "faceBookid") as? String{
            faceBookid = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "profilepicUrl") as? String{
            profilepicUrl = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "isOfficialEmail") as? String{
            isOfficialEmail = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "hideAnywhere") as? String{
            hideAnywhere = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "touHideAnywhere") as? String{
            touHideAnywhere = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "homeAddress") as? String{
            homeAddress = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "profilepicfileFormat") as? String{
            profilepicfileFormat = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "earnedPointsViaRecharge") as? String{
            earnedPointsViaRecharge = temp
        }
        
        
        if let temp = UserDefaults.standard.object(forKey: "earnedPointsViaEncash") as? String{
            earnedPointsViaEncash = temp
        }

        
        if let temp = UserDefaults.standard.object(forKey: "homeLatValue") as? String{
            homeLatValue = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "homeLongValue") as? String{
            homeLongValue = temp
        }

        
        if let temp = UserDefaults.standard.object(forKey: "workAddress") as? String{
            workAddress = temp
        }
        
        
        if let temp = UserDefaults.standard.object(forKey: "workLatValue") as? String{
            workLatValue = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "workLongValue") as? String{
            workLongValue = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "dob") as? String{
            dob = temp
        }
        if let temp = UserDefaults.standard.object(forKey: "defaultVehicleRegNo") as? String{
            defaultVehicleRegNo = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "currentLocation") as? String{
            currentLocation = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "fullName") as? String{
            fullName = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "refCode") as? String{
            refCode = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "isDefaultEmergencyNumberSelected") as? String
        {
            isDefaultEmergencyNumberSelected = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "selectedEmergencyNumber") as? String
        {
            selectedEmergencyNumber = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "selectedEmergencyEmail") as? String
        {
            selectedEmergencyEmail = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "vehicleSeqID") as? String{
            vehicleSeqID = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "vehicleType") as? String{
            vehicleType = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "vehicleModel") as? String{
            vehicleModel = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "vehicleRegNo") as? String{
            vehicleRegNo = temp
        }

        if let temp = UserDefaults.standard.object(forKey: "vehicleSeatCapacity") as? String{
            vehicleSeatCapacity = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "vehicleKind") as? String{
            vehicleKind = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "vehicleOwnerName") as? String{
            vehicleOwnerName = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "insurerName") as? String{
            insurerName = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "insuredCmpnyName") as? String{
            insuredCmpnyName = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "insuranceId") as? String{
            insuranceId = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "insuranceExpiryDate") as? String{
            insuranceExpiryDate = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "insUploadStatus") as? String{
            insUploadStatus = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "insUrl") as? String{
            insUrl = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "rcUploadStatus") as? String{
            rcUploadStatus = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "rcUrl") as? String{
            rcUrl = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "deviceToken") as? String{
            deviceToken = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "allRides") as? String{
            allRides = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "allWorkplaces") as? String{
            allWorkplaces = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "myWorkplaces") as? String{
            myWorkplaces = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "myPendingWorkplaces") as? String{
            myPendingWorkplaces = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "offeredRides") as? String{
            offeredRides = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "neededRides") as? String{
            neededRides = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "joinedRides") as? String{
            joinedRides = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "bookedRides") as? String{
            bookedRides = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "maxAllowedPoints") as? String{
            maxAllowedPoints = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "availablePoints") as? Int{
            availablePoints = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "rideOptions") as? Bool{
            isRideOptionsIn = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "groupOptions") as? Bool{
            isGroupOptionsIn = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "rideTaker") as? Bool{
            isRideTakerClicked = temp
        }
        
        if let temp = UserDefaults.standard.object(forKey: "isTouAgreed") as? Bool{
            isTouAgreed = temp
        }

    }
    
    func logoutUser()
    {
        isLoggedIn = false
        shareMobileNumber = false
        isProfilePicContent =  false
        isEmailIDVerified = false
        isProfilePicUpdatedSuccesfully = false
        isSecEmailIDVerified = false
        email = ""
        dob = ""
        mobile = ""
        userID = ""
        gender = ""
        password = ""
        firstName = ""
        lastName = ""
        fullName = ""
        userState = ""
        secEmail = ""
        isOfficialEmail = ""
        hideAnywhere = ""
        touHideAnywhere = ""
        allRides = ""
        allWorkplaces = ""
        myWorkplaces = ""
        myPendingWorkplaces = ""
        offeredRides = ""
        neededRides = ""
        joinedRides = ""
        bookedRides = ""
        refCode = ""
        profilepicStatus = ""
        profilepicUrl = ""
        faceBookid = ""
        currentLocation = ""
        homeAddress = ""
        homeLatValue = ""
        homeLongValue = ""
        workAddress = ""
        workLatValue = ""
        workLongValue = ""
        defaultVehicleRegNo = ""
        vehicleSeqID = ""
        vehicleType = ""
        vehicleModel = ""
        vehicleRegNo = ""
        vehicleSeatCapacity = ""
        vehicleKind = ""
        vehicleOwnerName = ""
        insurerName = ""
        insuredCmpnyName = ""
        insuranceId = ""
        insuranceExpiryDate = ""
        insUploadStatus = ""
        insUrl = ""
        rcUploadStatus = ""
        rcUrl = ""
        earnedPointsViaEncash = ""
        earnedPointsViaRecharge = ""
        profilepicfileFormat = ""
        deviceToken = ""
        isDefaultEmergencyNumberSelected = ""
        selectedEmergencyEmail = ""
        selectedEmergencyNumber = ""
        isRideOptionsIn = false
        isGroupOptionsIn = false
        isRideTakerClicked = false
        isTouAgreed = false
        maxAllowedPoints = ""
        availablePoints = 0
    }
}
