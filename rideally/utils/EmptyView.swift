//
//  EmptyView.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class EmptyView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    var imageView = UIImageView()
    var label = UILabel()
    
    var actionHandler: VoidCompletionHandler?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    var attributedText: NSAttributedString? {
        didSet {
            label.attributedText = attributedText
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func commonInit () {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(imageView)
        
        label.textAlignment = .center
        label.textColor = UIColor.gray
        label.font = normalFontWithSize(15)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        label.addGestureRecognizer(tap)
        
        self.addSubview(label)
        
        let dict = ["imageView": imageView, "label": label] as [String : Any]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[imageView]-[label]-|", options: [], metrics: nil, views: dict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[label]-|", options: [], metrics: nil, views: dict))
        self.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0))
    }
    
    func handleTap () {
        print("tap")
        actionHandler?()
    }

}
