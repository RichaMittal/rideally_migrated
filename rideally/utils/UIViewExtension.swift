//
//  UIViewExtension.swift
//  Feed Me
//
/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

extension UIView {
    
    func lock() {
        if let _ = viewWithTag(10) {
            //View is already locked
        } else {
            let lockView = lockStockView(Colors.WHITE_COLOR, style: .gray)
            //addSubview(lockView)
            
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 1.0
            }) 
        }
    }
    
    func lock(_ backgroundColor: UIColor, style: UIActivityIndicatorViewStyle) {
        if let _ = viewWithTag(10) {
            //View is already locked
        } else {
            let lockView = lockStockView(backgroundColor, style: style)
            //addSubview(lockView)
            
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 1.0
            }) 
        }
    }
    
    func lockStockView (_ backgroundColor: UIColor, style: UIActivityIndicatorViewStyle) -> UIView {
        let lockView = UIView(frame: bounds)
        lockView.backgroundColor = backgroundColor
        lockView.tag = 10
        lockView.alpha = 0.0
        let activity = UIActivityIndicatorView(activityIndicatorStyle: style)
        activity.hidesWhenStopped = true
        activity.tintColor = tintColor
        activity.center = lockView.center
        lockView.addSubview(activity)
        //lockView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        lockView.translatesAutoresizingMaskIntoConstraints = false
        activity.startAnimating()
        
        addSubview(lockView)
        
        let dict = ["lockView": lockView]
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lockView]|", options: [], metrics: nil, views: dict))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lockView]|", options: [], metrics: nil, views: dict))
        
        return lockView
    }
    
    func unlock() {
        if let lockView = viewWithTag(10) {
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 0.0
            }, completion: { finished in
                lockView.removeFromSuperview()
            }) 
        }
    }
    
    func fadeOut(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0.0
        }) 
    }
    
    func fadeIn(_ duration: TimeInterval) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        }) 
    }
    
    class func viewFromNibName(_ name: String) -> UIView? {
        let views = Bundle.main.loadNibNamed(name, owner: nil, options: nil)
        return views!.first as? UIView
    }
}
