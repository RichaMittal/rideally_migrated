//
//  Extensions.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import Foundation
//import Alamofire

//extension Request{
//    
//    public func validate() -> Self {
//        let acceptableStatusCodes: Range<Int> = 200..<500
//        let acceptableContentTypes: [String] = {
//            if let accept = request?.valueForHTTPHeaderField("Accept") {
//                return accept.componentsSeparatedByString(",")
//            }
//            
//            return ["*/*"]
//        }()
//        
//        return validate(statusCode: acceptableStatusCodes).validate(contentType: acceptableContentTypes)
//    }
//    
//}


private var BORDER_LAYER: UInt8 = 0
extension UIView {
    
    var borderLayer:CALayer? {
        get {
            return objc_getAssociatedObject(self, &BORDER_LAYER) as? CALayer
        }
        set {
            objc_setAssociatedObject(self, &BORDER_LAYER, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func roundCorners(_ corners: UIRectCorner, size: CGSize) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath
        self.layer.mask = shapeLayer
    }
    
    /// adds CALayer as bottom border
    func addBottomBorder(_ height: CGFloat, color: UIColor, offset: CGPoint) {
        self.borderLayer?.removeFromSuperlayer()
        
        let btmLayer = CALayer()
        let bounds = self.bounds
        let leftGap: CGFloat = offset.x
        let rightGap: CGFloat = offset.x + offset.y
        btmLayer.frame = CGRect(x: leftGap, y: bounds.maxY - height, width: bounds.width-rightGap, height: height)
        btmLayer.backgroundColor = color.cgColor
        self.borderLayer = btmLayer
        self.layer.addSublayer(self.borderLayer!)
    }
    
    func addTopBorder(_ height: CGFloat, color: UIColor, offset: CGPoint) {
        self.borderLayer?.removeFromSuperlayer()
        
        let btmLayer = CALayer()
        let bounds = self.bounds
        let leftGap: CGFloat = offset.x
        let rightGap: CGFloat = offset.x + offset.y
        btmLayer.frame = CGRect(x: leftGap, y: 0, width: bounds.width-rightGap, height: height)
        btmLayer.backgroundColor = color.cgColor
        self.borderLayer = btmLayer
        self.layer.addSublayer(self.borderLayer!)
    }
    
    func addShadow (_ color: UIColor, shadowRadius: CGFloat) {
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layer.shadowColor = color.cgColor
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    /// adds View as Bottom border
    func addBottomViewBorder (_ height: CGFloat, color: UIColor, offset: CGPoint) {
        let existingView = self.viewWithTag(-1001)
        existingView?.removeFromSuperview()
        
        let view = UIView()
        view.tag = -1001
        view.backgroundColor = color
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(offset.x))-[view]-(\(offset.y))-|", options: [], metrics: nil, views: ["view" : view]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(\(height))]|", options: [], metrics: nil, views: ["view" : view]))
    }
    
    func addTopViewBorder (_ height: CGFloat, color: UIColor, offset: CGPoint) {
        let existingView = self.viewWithTag(-1002)
        existingView?.removeFromSuperview()
        
        let view = UIView()
        view.tag = -1002
        view.backgroundColor = color
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(view)
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(offset.x))-[view]-(\(offset.y))-|", options: [], metrics: nil, views: ["view" : view]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[view(\(height))]", options: [], metrics: nil, views: ["view" : view]))
    }
}


private var OVERLAY_LAYER: UInt8 = 0
extension UINavigationBar {
    var overlay: UIView? {
        get {
            return objc_getAssociatedObject(self, &OVERLAY_LAYER) as? UIView
        }
        set {
            objc_setAssociatedObject(self, &OVERLAY_LAYER, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func lt_setBackgroundColor (_ bg_color: UIColor) {
        if self.overlay == nil {
            self.setBackgroundImage(UIImage(), for: .default)
            self.shadowImage = UIImage()
            let frame = CGRect(x: 0, y: -20, width: self.bounds.width, height: self.bounds.height+20)
            self.overlay = UIView(frame: frame)
            self.overlay?.isUserInteractionEnabled = false
            self.overlay?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.insertSubview(self.overlay!, at: 0)
        }
        self.overlay?.backgroundColor = bg_color
    }
    
    func lt_reset () {
        self.setBackgroundImage(nil, for: .default)
        self.overlay?.removeFromSuperview()
        self.overlay = nil
    }
}

extension UICollectionViewFlowLayout {
    func indexPathLastInSection (_ indexPath: IndexPath) -> Bool {
        guard let number = self.collectionView?.numberOfItems(inSection: indexPath.section) else {
            return false
        }
        let lastItem = number-1;
        return lastItem == indexPath.row
    }
    
    func indexPathInLastLine (_ indexPath: IndexPath) -> Bool {
        guard let number = self.collectionView?.numberOfItems(inSection: indexPath.section) else {
            return false
        }
        
        let lastItem = number-1;
        let lastIndexPath = IndexPath(item: lastItem, section: indexPath.section)
        let lastItemAttributes = self.layoutAttributesForItem(at: lastIndexPath)
        let thisItemAttributes = self.layoutAttributesForItem(at: indexPath)
        
        return lastItemAttributes?.frame.origin.y == thisItemAttributes?.frame.origin.y
    }
    
    func indexPathLastInLine (_ indexPath: IndexPath) -> Bool {
        let nextIndexPath = IndexPath(item: indexPath.row+1, section: indexPath.section)
        
        let thisItemAttributes = self.layoutAttributesForItem(at: indexPath)
        let nextItemAttributes = self.layoutAttributesForItem(at: nextIndexPath)
        
        return !(thisItemAttributes?.frame.origin.y == nextItemAttributes?.frame.origin.y)
    }
}

let placeHolderText = "Enter your message here"
extension UITextView {
    
    func setMessagePlaceHolder () {
        setMessagePlaceHolder (placeHolderText)
    }
    
    func setMessagePlaceHolder (_ placeHolder: String?) {
        self.text = placeHolder
        self.textColor = Colors.DISABLED_COLOR
    }
    
    func removeMessagePlaceHolder () {
        self.text = ""
        self.textColor = Colors.BLACK_COLOR
    }
}

extension UIViewController {
    //MARK:- Get content Height
    func getContentHeight () -> CGFloat {
        return 0.0
    }
}

extension CGFloat {
    mutating func twoPlaces () -> String {
        let value = (self*100).rounded()
        return String(format: "%.2f",value/100.0)
    }
}

private var EMPTY_VIEW: UInt8 = 0
extension UIView {
    var emptyView: UIView? {
        get {
            return objc_getAssociatedObject(self, &EMPTY_VIEW) as? UIView
        }
        set {
            objc_setAssociatedObject(self, &EMPTY_VIEW, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func addEmptyImageViewWithText (_ text: String?, attributedText: NSAttributedString?, imageNamed: String?, actionHandler: VoidCompletionHandler?) {
        if self.emptyView?.superview != nil {
            self.emptyView?.removeFromSuperview()
        }
        let _emptyView = EmptyView()
        _emptyView.actionHandler = actionHandler

        _emptyView.label.text = text
        if attributedText != nil {
            _emptyView.attributedText = attributedText
        }
        if imageNamed != nil {
            _emptyView.imageView.image = UIImage(named: imageNamed!)
        }
        
        _emptyView.translatesAutoresizingMaskIntoConstraints = false
        
        addEmptyView(_emptyView)
    }
    
    func addEmptyView (_ _emptyView: EmptyView) {
        self.addSubview(_emptyView)
        
        self.emptyView = _emptyView
        
        let dict = ["emptyView": _emptyView]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(>=8)-[emptyView]-(>=8)-|", options: [], metrics: nil, views: dict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(>=8)-[emptyView]-(>=8)-|", options: [], metrics: nil, views: dict))
        
        self.addConstraint(NSLayoutConstraint(item: _emptyView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: _emptyView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0))
    }
    
    func removeEmptyView () {
        self.emptyView?.removeFromSuperview()
    }
}


extension UIResponder {
    // Swift 1.2 finally supports static vars!. If you use 1.1 see:
    // http://stackoverflow.com/a/24924535/385979
    fileprivate weak static var _currentFirstResponder: UIResponder? = nil
    
    public class func currentFirstResponder() -> UIResponder? {
        UIResponder._currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(findFirstResponder(_:)), to: nil, from: nil, for: nil)
        return UIResponder._currentFirstResponder
    }
    
    internal func findFirstResponder(_ sender: AnyObject) {
        UIResponder._currentFirstResponder = self
    }
}

extension String {
    func trimmedText () -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}

class ActionBlockWrapper : NSObject {
    var block : actionBlockWithParam?
    init(block: actionBlockWithParam?) {
        self.block = block
    }
}

private var BUTTON_ACTION_HANDLER: UInt8 = 0
extension UIButton {
    
    func addTarget(_ handler: actionBlockWithParam?) {
        self.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        objc_setAssociatedObject(self, &BUTTON_ACTION_HANDLER, ActionBlockWrapper(block: handler), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func buttonPressed (_ sender: AnyObject) {
        let handler = objc_getAssociatedObject(self, &BUTTON_ACTION_HANDLER) as? ActionBlockWrapper
        handler?.block?(sender)
    }
}

extension Data {
    func hexadecimalStringDescription () -> String {
        let chars = (self as NSData).bytes.bindMemory(to: CChar.self, capacity: self.count)
        var prettyString = ""
        
        for i in 0..<self.count {
            prettyString += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        return prettyString
    }
}

