//
//  Constants.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import UIKit
import Foundation

struct API_KEYS {
    //static let GOOGLE_API_KEY: String = "AIzaSyDdc4gvbyC3kr97pWrweS1xcrNHMR-3b4o"//saravanan's
    static let GOOGLE_API_KEY: String = "AIzaSyB8K1bMvB2Q_di-TC5tQA3bOZ09HNn-PBw"
    static let FACEBOOK_APP_ID: String = ""
    static let FACEBOOK_SECRET_ID: String = ""
    static let AWS_API_KEY: String  = "us-east-1:483f9656-128c-4451-8696-acdab05ddea1"
    //static let AWS_API_KEY: String  = "us-west-2:26b43e1a-cf02-4467-bffd-72ad79a18db1"
    static let GA_TRACKING_ID: String = "UA-73996371-1"
}

let DEVICE_TYPE = "IOS"
let API_VERSION = "1.0"
let RESOLUTION_VALUE = "\(Int(UIScreen.main.scale))X"
let NAV_BAR_HEIGHT: CGFloat = 64.0
let Header_Height: CGFloat = 50.0

let ERROR_MESSAGE_STRING = "Something went wrong. Please try after some time."
let INTERNET_MESSAGE_STRING = "Your internet connection seems to be offline."

let INDIA_COUNTRY_CODE: String = "IN"

let TERMS_URL = ""
let TERMS_TITLE = "Terms and Conditions"

let S3BUCKETNAME = "s3rastage"
var ISFROM: String?
var ISWPRIDESSCREEN = false
var ISWPMAPSCREEN = false
var WPDATA: WorkPlace?
var WPMEMBERS: [WPMember]?
var DATASOURCETOOFC: [WPRide]?
var DATASOURCETOHOME: [WPRide]?
var ISSEARCHRESULT = false
var SEARCHSOURCETOOFC: [WPRide]?
var SEARCHSOURCETOHOME: [WPRide]?
var SEARCHREQUEST: SearchRideRequest?
var ISVEHICLEUPDATED = false
var APPLICATION_NAME: String? {
    return Bundle.main.infoDictionary?["CFBundleName"] as? String
}

let CURRENT_BUILD_VERSION = "1.0"


struct Colors {
    
    static let TABBAR_BG = UIColor(red: 86.0/255.0, green: 106.0/255.0, blue: 118.0/255.0, alpha: 1.0)
    
    static let NAV_TINT_BG = Colors.RED_COLOR
    static let NAV_TITLE = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
    
    static let APP_BG = UIColor.white
    
    static let WHITE_COLOR = UIColor.white
    static let BLACK_COLOR = UIColor.black
    static let ORANGE_TEXT_COLOR = UIColor(red: 218.0/255.0, green: 127.0/255.0, blue: 18.0/255.0, alpha: 1.0)
    static let ORANGE_BG_COLOR = UIColor(red: 236.0/255.0, green: 204.0/255.0, blue: 166.0/255.0, alpha: 1.0)
    static let RED_COLOR = UIColor(red: 240.0/255.0, green: 76.0/255.0, blue: 54.0/255.0, alpha: 1.0)
    
    static let GRAY_COLOR = UIColor(red: 86.0/255.0, green: 106.0/255.0, blue: 118.0/255.0, alpha: 1.0)
    static let GREEN_COLOR = UIColor(red: 33.0/255.0, green: 200.0/255.0, blue: 169.0/255.0, alpha: 1.0)
    static let BG_GREEN_COLOR = UIColor(red: 33.0/255.0, green: 167.0/255.0, blue: 169.0/255.0, alpha: 1.0)
    static let LIGHT_GREEN_COLOR = UIColor(red: 227.0/255.0, green: 249.0/255.0, blue: 145.0/255.0, alpha: 1.0)
    static let ORANGE_COLOR = UIColor(red: 230.0/255.0, green: 126.0/255.0, blue: 34.0/255.0, alpha: 1.0)
    
    static let GENERAL_BORDER_COLOR = UIColor.lightGray.withAlphaComponent(0.4)
    static let DISABLED_COLOR = UIColor.lightGray
    
    static let LABEL_DULL_TXT_COLOR = UIColor(red: 142.0/255.0, green: 141.0/255.0, blue: 141.0/255.0, alpha: 1.0)
    
    static let BTN_BG_COLOR = UIColor(red: 58.0/255.0, green: 75.0/255.0, blue: 87.0/255.0, alpha: 1.0)
    
    
    static let ODD_ROW_COLOR = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
    static let EVEN_ROW_COLOR = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
    
    //#03C9A9 //rgb(3,201,169)
    static let POLILINE_GRAY = UIColor(red: 180.0/255.0, green: 180.0/255.0, blue: 180.0/255.0, alpha: 1)
    static let POLILINE_GREEN = UIColor(red: 3.0/255.0, green: 201.0/255.0, blue: 169.0/255.0, alpha: 1)
    static let POLILINE_ORANGE = UIColor(red: 230.0/255.0, green: 126.0/255.0, blue: 34.0/255.0, alpha: 1.0)
    
    
}

let DOT_CHARACTER = "•"
let STAR_CHARACTER = "*"
let RUPEE_SYMBOL = "₹"

struct FONT_NAME {
    static let NORMAL_FONT = "Calibri"
    static let BOLD_FONT = "Calibri"
}
