//
//  Blocks.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

typealias actionBlock = VoidCompletionHandler
typealias actionBlockWithParam = (Any?) -> Void
typealias actionBlockWithStringParam = (String?) -> Void
typealias actionBlockWithParams = (Any?, Any?) -> Void
typealias actionBlockWith3Params = (Any?, Any?, Any?) -> Void
typealias actionBlockWith3ParamsString = (Any?, String?, Any?) -> Void
typealias actionBlockWith4ParamsString = (Any?, Any?, String?, String?) -> Void
typealias actionBlockWith2ParamsString = (Any?, String?) -> Void
typealias actionBlockWith6Params = (Any?, Any?, Any?, Any?, Any?, Any?) -> Void
typealias ListItemDidSelecthandler = (_ indexPath: IndexPath, _ dataObject: Any, _ identifier: String?) -> Void

typealias VoidCompletionHandler = () -> Void
typealias HeightUpdateHandler = (_ height: CGFloat) -> Void

typealias NetworkSuccessHandler = (Any?) -> Void
typealias NetworkFailureHandler = (NSError?) -> Void
typealias NetworkCompletionHandler = (_ success: Bool, _ object: Any?) -> Void


typealias onSuccessBlock = (Any?) -> Void
typealias onFailureBlock = (String?, Any?) -> Void

typealias NotificationFetchHandler = (UIBackgroundFetchResult) -> Void


