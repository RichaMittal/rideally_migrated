//
//  ApplicationUrls.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import Foundation


struct TERMS_ETC_URLS {
    static let WEB_BASE_URL = ""
    
    static let ABOUT_US_URL = WEB_BASE_URL + "aboutus/cust-app";
    static let FAQ_TERMS_URL = WEB_BASE_URL + "faq-and-terms/cust-app";
    static let PRIVACY_POLICY_URL = WEB_BASE_URL + "privacy-policy/cust-app";
    static let PAYMENT_TERMS_URL = FAQ_TERMS_URL + "#Terms";
}

var ENVIRONMENT: String{
    //return "LIVE"
    return "STAGE"
}

var SERVER: String {
    if(ENVIRONMENT == "LIVE"){
        return "https://rideally.com/"
    }
    else{
        return "http://prodtst.rideally.com/"
    }
}

var URLS_BASE: String {
    return SERVER + "services/"
}

var IMG_URLS_BASE: String {
    if(ENVIRONMENT == "LIVE"){
        return "http://s3rideallylive.s3.amazonaws.com/"
    }
    else{
        return "http://s3rastage.s3.amazonaws.com/"
    }
}

var WORKPLACE_URL_BASE: String {
    return SERVER + "workplace/"
}

var PAYTM_GENERATE: String {
    return SERVER + "PaytmKit/generateChecksum.php"
}

var PAYTM_VERIFY: String {
    return SERVER + "PaytmKit/verifyChecksum.php"
}

var SOS_USERLOC: String {
    return SERVER + "userlocation"
}

struct URLS {
    
    static var BASE: String {
        return URLS_BASE
    }
    
    static var IMG_URL: String{
        return IMG_URLS_BASE
    }
    
    static var ALL_RIDES: String {
        return URLS.BASE + "pool/search"
    }
    
    static var MY_RIDES: String {
        return URLS.BASE + "pool/getmyrides"
    }

    static var RIDE_DETAILS: String {
        return URLS.BASE + "pool/getdetails"
    }
    
    static var TAXI_DETAILS: String {
        return URLS.BASE + "pool/gettaxidetails"
    }

    static var CREATE_NEED_A_RIDE: String {
        return URLS.BASE + "anywheres/createridenew"
    }
    
    static var STATICPAGE: String
    {
        return URLS.BASE + "support/getstaticpage"
    }

    static var JOIN_OR_OFFER_RIDE: String {
        return URLS.BASE + "pool/pendingrequest"
    }
    
    static var LOGIN: String
    {
        return URLS.BASE + "user/login"
    }
    
    static var SIGNUP: String
    {
        return URLS.BASE + "user/signup"
    }
    
    static var FORGOTPWD: String
    {
        return URLS.BASE + "user/forgotpassword"
    }
    
    static var CHANGEPWD: String
    {
        return URLS.BASE + "user/changepassword"
    }

    static var ISEMAILIDEXIST: String
    {
        return URLS.BASE + "user/isemailexistcheck"
    }

    static var ISMOBILENUMBEREXIST: String
    {
        return URLS.BASE + "user/ismobileexistcheck"
    }
    
    static var ISREFERRALCODEVALID: String
    {
        return URLS.BASE + "user/checkrefcode"
    }
    static var APPLYREFCODE: String
    {
        return URLS.BASE + "user/addcouponusage"
    }
    
    static var VERIFYMOBILE: String
    {
        return URLS.BASE + "sms/verify"
    }
    
    static var RESENDCODE: String
    {
        return URLS.BASE + "sms/resendcode"
    }
    
    static var GETPROFILEDETAIL: String
    {
        return URLS.BASE + "user/getprofile"
    }
    
    static var FBLOGIN: String
    {
        return URLS.BASE + "user/fblogin"
    }
    
    static var EDITUSERPROFILE: String
    {
        return URLS.BASE + "user/profile"
    }

    static var SETHOMELOC: String
    {
        return URLS.BASE + "officegroups/sethomelocation"
    }

    static var SETWORKLOC: String
    {
        return URLS.BASE + "officegroups/setworkplacelocation"
    }

    static var GETCONTACTLIST: String
    {
        return URLS.BASE + "user/getsosmobmail"
    }

    static var SAVECONTACTLIST: String
    {
        return URLS.BASE + "user/sosmobmailadd"
    }
    
    static var DELETECONTACTLIST: String
    {
        return URLS.BASE + "user/deletesos"
    }

    static var GETUSERPOINTS: String
    {
        return URLS.BASE + "user/getuserpoints"
    }
    
    static var UPADATEPROFILEIMAGEURL: String
    {
        return URLS.BASE + "user/updateprofileimageurl"
    }
    
    static var RESENDEMAIL: String
    {
        return URLS.BASE + "user/resendemail"
    }

    static var ALL_VEHICLE: String
    {
        return URLS.BASE + "vehicle/getallvehicle"
    }

    static var GET_DEFAULT_VEHICLE: String
    {
        return URLS.BASE + "vehicle/getdefaultvehicle"
    }
    
    static var SET_DEFAULT_VEHICLE: String
    {
        return URLS.BASE + "vehicle/setdefaultvehicle"
    }

    static var ADD_VEHICLE: String
    {
        return URLS.BASE + "vehicle/addvehicle"
    }

    static var DELETE_VEHICLE: String
    {
        return URLS.BASE + "vehicle/deletevehicle"
    }

    static var UPDATE_VEHICLE: String
    {
        return URLS.BASE + "vehicle/updatevehicle"
    }
    
    static var ADD_VEHICLE_INSURANCE: String
    {
        return URLS.BASE + "vehicle/vehinsurance"
    }
    
    static var UPDATE_VEHICLE_INSURANCE: String
    {
        return URLS.BASE + "vehicle/updatevehinsurance"
    }
    
    static var GET_SUBSCRIPTIONS: String
    {
        return URLS.BASE + "routes/getplandetail"
    }
    
    static var CREATE_SHARE_A_TAXI: String
    {
        return URLS.BASE + "routes/routecreate"
    }
    
    
    static var GETSOS: String
    {
        return URLS.BASE + "pool/rideallysos"
    }
    
    static var UNJOIN_RIDE: String
    {
        return  URLS.BASE + "pool/unjoin"
    }
    static var JOIN_TAXI_RIDE: String
    {
        return  URLS.BASE + "taxiride/jointaxiride"
    }
    static var DELETE_RIDE: String
    {
        return  URLS.BASE + "pool/delete"
    }
    static var CANCEL_TAXI_RIDE: String
    {
        return  URLS.BASE + "taxiride/unjointaxiride"
    }
    static var CANCEL_REQUEST: String
    {
        return  URLS.BASE + "pool/cancelrequest"
    }
    static var RESPOND_TO_RIDE_REQUEST: String  //accept/Reject
    {
        return  URLS.BASE + "pool/acceptreject"
    }
    static var ACCEPT_TAXIRIDE_REQUEST: String
    {
        return  URLS.BASE + "taxiride/acceptfriendrequest"
    }
    static var REJECT_TAXIRIDE_REQUEST: String
    {
        return  URLS.BASE + "taxiride/rejectfriendrequest"
    }
    static var INVITE_TO_RIDE: String
    {
        return  URLS.BASE + "pool/inviteride"
    }
    static var GET_RIDE_MEMBERS: String
    {
        return  URLS.BASE + "pool/getdetails"
    }
    static var GET_MEMBERS_DETAILS: String
    {
        return  URLS.BASE + "user/getdetails"
    }
    static var UPDATE_RIDE: String
    {
        return  URLS.BASE + "pool/updatepool"
    }
    static var GET_USERDETAIL: String
    {
        return  URLS.BASE + "user/usertotaldetail"
    }
    static var ADD_SEC_EMAIL: String
    {
        return  URLS.BASE + "user/addsecemail"
    }
    static var VERIFY_WP_EMAIL: String
    {
        return  URLS.BASE + "user/isemailvalidwp"
    }
    static var ADD_WORKPLACE: String
    {
        return  URLS.BASE + "officegroups/addofficegroup"
    }
    static var UPDATE_WORKPLACE: String
    {
        return  URLS.BASE + "officegroups/updateofficegroup"
    }
    static var GET_ALL_WORKPLACES: String
    {
        return  URLS.BASE + "officegroups/getallofficegroup"
    }
    static var GET_MY_WORKPLACES: String
    {
        return  URLS.BASE + "officegroups/getmyofficegroup"
    }
    static var JOIN_WORKPLACE: String
    {
        return  URLS.BASE + "group/join"
    }
    static var UNJOIN_WORKPLACE: String
    {
        return  URLS.BASE + "group/unjoin"
    }
    static var CANCEL_JOIN_WORKPLACE: String
    {
        return  URLS.BASE + "group/canceljoin"
    }
    static var GET_WORKPLACE_MEMBERS: String
    {
        return  URLS.BASE + "group/getdetails"
    }
    static var GET_WORKPLACE_RIDES: String
    {
        return URLS.BASE + "pool/search"
    }
    static var GET_WORKPLACE_DOMAINS: String
    {
        return URLS.BASE + "officegroups/getdomaingroup"
    }
    static var INVITE_TO_WORKPLACE: String
    {
        return  URLS.BASE + "group/invite"
    }
    static var SEARCH_WORKPLACE: String
    {
        return URLS.BASE + "officegroups/getofficegroup"
    }
    static var ADD_WORKPLACE_RIDE: String
    {
        return URLS.BASE + "officegroups/createofcridenew"
    }
    static var GET_USER_LAST_RIDE: String
    {
        return URLS.BASE + "officegroups/getuserlastride"
    }
    
    static var ADD_WORKPLACE_ADMIN: String
    {
        return  URLS.BASE + "group/addadmintogroup"
    }
    static var RESPOND_JOIN_TO_WORKPLACE: String
    {
        return  URLS.BASE + "group/processjoining"
    }
    static var REMOVE_FROM_WORKPLACE: String
    {
        return  URLS.BASE + "group/remove"
    }
    
    static var GET_TESTIMONIALS: String
    {
        return  URLS.BASE + "support/gettestimonial"
    }
    static var GET_FAQ: String
    {
        return  URLS.BASE + "support/getfaqhtml"
    }
    static var SEND_FEEDBACK: String
    {
        return  URLS.BASE + "support/contactmessage"
    }
    static var GET_APP_VERSION: String
    {
        return URLS.BASE + "support/playstoreversion"
    }
    static var GET_NOTIFICATIONS: String
    {
        return  URLS.BASE + "notification/getnotification"
    }
    static var UPDATE_NOTIFICATION: String
    {
        return  URLS.BASE + "notification/updatestatusnoti"
    }
    static var DELETE_NOTIFICATION: String
    {
        return  URLS.BASE + "notification/delete"
    }
    static var CREATE_TAXIRIDE: String
    {
        return  URLS.BASE + "taxiride/create"
    }
    static var GET_DEFAULT_LOC: String
    {
        return  URLS.BASE + "taxiride/getdefaultlocation"
    }
    static var GET_PRICE_LIST: String
    {
        return  URLS.BASE + "taxiride/gettaxipricelist"
    }
    
    
    static var DRIVER_START_RIDE: String
    {
        return  URLS.BASE + "pool/poolstart"
    }
    static var RIDER_START_RIDE: String
    {
        return  URLS.BASE + "pool/boardingstart"
    }
    static var DRIVER_STOP_RIDE: String
    {
        return  URLS.BASE + "pool/driverend"
    }
    static var RIDER_STOP_RIDE: String
    {
        return  URLS.BASE + "pool/riderend"
    }

    static var UPDATETRANSACTION: String
    {
        return  URLS.BASE + "subscriptions/updatetransactoin"
    }

    static var GETREDEEMPOINTS: String
    {
        return  URLS.BASE + "subscriptions/redeem"
    }
    

    static var RIDE_ETA: String
    {
        return URLS.BASE + "pool/eta"
    }
    static var RIDE_VEHICLES_CONFIG: String
    {
        return URLS.BASE + "vehicle/getrideconfigdata"
    }
    static var GET_OFFERS_PROMOTIONS: String
    {
        return  URLS.BASE + "subscriptions/promotions"
    }
    static var SEND_TOKEN: String
    {
        return  URLS.BASE + "user/updatedevicetoken"
    }
    
    static var CHECK_COUPON_EXISTS: String
    {
        return URLS.BASE + "taxiride/checkcoupon"
    }
    static var CHECK_COUPON_USAGE: String
    {
        return  URLS.BASE + "taxiride/checkcouponusage"
    }
    static var APPLY_COUPON: String
    {
        return  URLS.BASE + "taxiride/applycoupon"
    }
    static var RIDES_COMPLETED: String
    {
        return  URLS.BASE + "pool/getmyrideshistory"
    }
    static var RIDES_ONGOING: String
    {
        return  URLS.BASE + "pool/getmyongoingrideshistory"
    }
    static var RECREATE_RIDE: String
    {
        return  URLS.BASE + "pool/recreateride"
    }
    static var SETPOOL_RATING: String
    {
        return  URLS.BASE + "pool/setpoolrating"
    }
    static var WPSEARCH_BYLIKE: String
    {
        return  URLS.BASE + "officegroups/getofficegroupbynamelike"
    }
}
