//
//  ChangePasswordViewController.swift
//  rideally
//
//  Created by Raghunathan on 8/7/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ChangePasswordViewController: UIViewController,UITextFieldDelegate {
    
    var currentPasswordTextBox: RATextField?
    var newPasswordTextBox: RATextField?
    var confirmPasswordTextBox  : RATextField?
    var currentPasswordLabel: UILabel!
    var newPasswordLabel: UILabel!
    var confirmPasswordLabel: UILabel!
    var yPost_New:  NSLayoutConstraint!
    var yPost_Confirm:  NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Change Password"
    }

    func getTextBoxViews(_ placeHolderString:String) -> RATextField
    {
        let textBoxInstance =  RATextField()
        textBoxInstance.returnKeyType = UIReturnKeyType.next
        textBoxInstance.delegate = self
        
        textBoxInstance.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        textBoxInstance.placeholder = placeHolderString
        textBoxInstance.isSecureTextEntry = true
        textBoxInstance.font = normalFontWithSize(15)!
        textBoxInstance.textColor = Colors.GRAY_COLOR
        return textBoxInstance
    }

    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = Colors.WHITE_COLOR
        createChangePasswordViews()
    }

    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.WHITE_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func createChangePasswordViews() ->  Void
    {
        currentPasswordLabel  =  getLabelSubViews("CURRENT PASSWORD")
        currentPasswordLabel.isHidden = true
        currentPasswordLabel.font = normalFontWithSize(12)
        currentPasswordLabel.textColor = Colors.GREEN_COLOR
        currentPasswordLabel.textAlignment = NSTextAlignment.left
        
        newPasswordLabel  =  getLabelSubViews("NEW PASSWORD")
        newPasswordLabel.isHidden = true
        newPasswordLabel.font = normalFontWithSize(12)
        newPasswordLabel.textColor = Colors.GREEN_COLOR
        newPasswordLabel.textAlignment = NSTextAlignment.left
        
        confirmPasswordLabel  =  getLabelSubViews("CONFIRM PASSWORD")
        confirmPasswordLabel.isHidden = true
        confirmPasswordLabel.font = normalFontWithSize(12)
        confirmPasswordLabel.textColor = Colors.GREEN_COLOR
        confirmPasswordLabel.textAlignment = NSTextAlignment.left
        
        currentPasswordTextBox = getTextBoxViews("CURRENT PASSWORD")
        newPasswordTextBox = getTextBoxViews("NEW PASSWORD")
        confirmPasswordTextBox = getTextBoxViews("CONFIRM PASSWORD")
        confirmPasswordTextBox!.returnKeyType = UIReturnKeyType.done
        
        let  changePasswordView = UIView()
        self.view.addSubview(changePasswordView)
        changePasswordView.addSubview(currentPasswordTextBox!)
        changePasswordView.addSubview(currentPasswordLabel!)
        changePasswordView.addSubview(newPasswordTextBox!)
        changePasswordView.addSubview(newPasswordLabel!)
        changePasswordView.addSubview(confirmPasswordTextBox!)
        changePasswordView.addSubview(confirmPasswordLabel!)
        
        changePasswordView.translatesAutoresizingMaskIntoConstraints = false
        currentPasswordTextBox!.translatesAutoresizingMaskIntoConstraints = false
        currentPasswordLabel!.translatesAutoresizingMaskIntoConstraints = false
        newPasswordTextBox!.translatesAutoresizingMaskIntoConstraints = false
        newPasswordLabel!.translatesAutoresizingMaskIntoConstraints = false
        confirmPasswordTextBox!.translatesAutoresizingMaskIntoConstraints = false
        confirmPasswordLabel!.translatesAutoresizingMaskIntoConstraints = false
        
        let btn = UIButton()
        btn.setTitle("SUBMIT", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(submitButtonPressed(_:)), for: .touchDown)
        self.view.addSubview(btn)
        
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[changepasswordview(200)]", options:[] , metrics:nil , views:["changepasswordview": changePasswordView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btnSubmit(40)]|", options:[], metrics:nil, views: ["btnSubmit":btn]))

        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[changepasswordview]|", options:[] , metrics:nil , views:["changepasswordview": changePasswordView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnSubmit]|", options:[], metrics:nil, views: ["btnSubmit":btn]))

        
        let subViewsDict = ["currentPassword": currentPasswordTextBox!,"newPassword":newPasswordTextBox!,"confirmPassword":confirmPasswordTextBox!, "currentPasswordLabel":currentPasswordLabel] as [String : Any]
        
        changePasswordView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[currentPasswordLabel][currentPassword(30)]-20-[newPassword(==currentPassword)]-20-[confirmPassword(==currentPassword)]", options:[] , metrics:nil , views:subViewsDict))
        
        changePasswordView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[currentPasswordLabel]-10-|", options:[] , metrics:nil , views:["currentPasswordLabel": currentPasswordLabel]))
        
        changePasswordView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[currentPassword]-10-|", options:[] , metrics:nil , views:["currentPassword": currentPasswordTextBox!]))
        
        changePasswordView.addConstraint(NSLayoutConstraint(item:currentPasswordTextBox!, attribute: .left, relatedBy: .equal, toItem: currentPasswordLabel, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item:currentPasswordTextBox!, attribute: .width, relatedBy: .equal, toItem: currentPasswordLabel, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        changePasswordView.addConstraint(NSLayoutConstraint(item:newPasswordTextBox!, attribute: .left, relatedBy: .equal, toItem: currentPasswordTextBox!, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item:newPasswordTextBox!, attribute: .width, relatedBy: .equal, toItem: currentPasswordTextBox!, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        changePasswordView.addConstraint(NSLayoutConstraint(item:confirmPasswordTextBox!, attribute: .left, relatedBy: .equal, toItem: currentPasswordTextBox!, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item:confirmPasswordTextBox!, attribute: .width, relatedBy: .equal, toItem: currentPasswordTextBox!, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        changePasswordView.addConstraint(NSLayoutConstraint(item: currentPasswordLabel!, attribute: .left, relatedBy: .equal, toItem: currentPasswordTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item: currentPasswordLabel!, attribute: .width, relatedBy: .equal, toItem: currentPasswordTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item: currentPasswordLabel!, attribute: .height, relatedBy: .equal, toItem: currentPasswordTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        changePasswordView.addConstraint(NSLayoutConstraint(item: newPasswordLabel!, attribute: .left, relatedBy: .equal, toItem: newPasswordTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item: newPasswordLabel!, attribute: .width, relatedBy: .equal, toItem: newPasswordTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item: newPasswordLabel!, attribute: .height, relatedBy: .equal, toItem: newPasswordTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        changePasswordView.addConstraint(NSLayoutConstraint(item: confirmPasswordLabel!, attribute: .left, relatedBy: .equal, toItem: confirmPasswordTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item: confirmPasswordLabel!, attribute: .width, relatedBy: .equal, toItem: confirmPasswordTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        changePasswordView.addConstraint(NSLayoutConstraint(item: confirmPasswordLabel!, attribute: .height, relatedBy: .equal, toItem: confirmPasswordTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        yPost_New = NSLayoutConstraint(item: newPasswordLabel!, attribute: .top, relatedBy: .equal, toItem:currentPasswordTextBox, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        changePasswordView.addConstraint(yPost_New)
        
        yPost_Confirm = NSLayoutConstraint(item: confirmPasswordLabel!, attribute: .top, relatedBy: .equal, toItem:newPasswordTextBox, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        changePasswordView.addConstraint(yPost_Confirm)
    }
    
    
    func submitButtonPressed(_ Sender:UIButton) -> Void {
        callFunctionWhenDoneButtonisPressed()
    }
    
    
    func handleValidation() ->  String{
        
        if  currentPasswordTextBox!.text?.characters.count == 0 && newPasswordTextBox!.text?.characters.count == 0 && confirmPasswordTextBox!.text?.characters.count == 0
        {
            return "Please provide information in all the fields."
        }
        else if currentPasswordTextBox!.text?.characters.count == 0
        {
            return "Please provide 'Current Password'."
        }
        else if currentPasswordTextBox!.text != UserInfo.sharedInstance.password
        {
            return "Current password is incorrect."
        }
        else if newPasswordTextBox!.text?.characters.count == 0
        {
            return "Please provide 'New Password'."
        }
        else if newPasswordTextBox!.text?.characters.count < 6 || newPasswordTextBox!.text?.characters.count > 32
        {
            return "Please provide 'New Password' between 6 and 32 characters."
        }
        else if currentPasswordTextBox!.text == newPasswordTextBox!.text
        {
            return "New Password' and 'Current Password' should be different."
        }
        else if confirmPasswordTextBox!.text?.characters.count == 0
        {
            return "Please provide 'Confirm Password'."
        }
        else if confirmPasswordTextBox!.text?.characters.count < 6 || confirmPasswordTextBox!.text?.characters.count > 32
        {
            return "Please provide 'Confirm Password' between 6 and 32 characters."
        }
        else if newPasswordTextBox!.text != confirmPasswordTextBox!.text
        {
            return "New Password' and 'Confirm Password' must match."
        }

        return ""
    }
    
    func callFunctionWhenDoneButtonisPressed() -> Void {
        
        if  handleValidation().characters.count  == 0
        {
            sendChangePasswordRequest()
        }
        else
        {
            AlertController.showToastForError(handleValidation())
        }
    }

    
    func sendChangePasswordRequest() -> Void {

        showIndicator("Loading...")
        let reqObj = ChangePasswordRequest()
        reqObj.userID = UserInfo.sharedInstance.userID
        reqObj.oldPassword = currentPasswordTextBox!.text
        reqObj.newPassword = newPasswordTextBox!.text
        reqObj.confirmPassword = confirmPasswordTextBox!.text
        
        ChangePasswordRequestor().sendChangePasswordRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! ChangePasswordResponse).code == "460"
            {
                AlertController.showAlertFor("RideAlly", message: "Your password is changed successfully", okButtonTitle: "Ok", okAction: {
                    _ = self.navigationController?.popViewController(animated: true)
                })
            }
            else if (object as! ChangePasswordResponse).code == "461"
            {
                AlertController.showAlertFor("RideAlly", message: "Current password is incorrect")
            }
            else if (object as! ChangePasswordResponse).code == "462"
            {
                AlertController.showAlertFor("RideAlly", message: "New Password' and 'Confirm Password' must match")
            }
            else if (object as! ChangePasswordResponse).code == "463"
            {
                AlertController.showAlertFor("RideAlly", message: "Please provide 'Password' between 6 and 32 characters")
            }
            else if (object as! ChangePasswordResponse).code == "464"
            {
                AlertController.showAlertFor("RideAlly", message: "Sorry, there is some technical problem while changing password, please try later")
            }

        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == currentPasswordTextBox)
        {
            let placeHolder = NSAttributedString(string: "CURRENT PASSWORD", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            currentPasswordTextBox!.attributedPlaceholder = placeHolder
            currentPasswordLabel .isHidden = true
            newPasswordTextBox!.becomeFirstResponder()
        }
        else if (textField == newPasswordTextBox)
        {
            yPost_New.constant = 0
            let placeHolder = NSAttributedString(string: "NEW PASSWORD", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            newPasswordTextBox!.attributedPlaceholder = placeHolder
            newPasswordLabel .isHidden = true
            confirmPasswordTextBox!.becomeFirstResponder()
        }
        else if (textField == confirmPasswordTextBox)
        {
            yPost_Confirm.constant = 0
            let placeHolder = NSAttributedString(string: "CONFIRM PASSWORD", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            confirmPasswordTextBox!.attributedPlaceholder = placeHolder
            confirmPasswordLabel .isHidden = true
            callFunctionWhenDoneButtonisPressed()
            confirmPasswordTextBox!.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField === currentPasswordTextBox)
        {
            currentPasswordTextBox!.placeholder = ""
            currentPasswordLabel.isHidden = false
        }
        else if (textField === newPasswordTextBox)
        {
            yPost_New.constant = 25;
            newPasswordTextBox!.placeholder = ""
            newPasswordLabel.isHidden = false
        }
        else if (textField === confirmPasswordTextBox)
        {
            yPost_Confirm.constant = 25;
            confirmPasswordTextBox!.placeholder = ""
            confirmPasswordLabel.isHidden = false
        }
    }

}
