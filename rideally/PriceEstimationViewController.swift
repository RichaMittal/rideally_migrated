//
//  PriceEstimationViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 10/17/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class PrivatePriceEstimationCell: UITableViewCell {
    
    let lblVehicleType = UILabel()
    let lblPrice = UILabel()
    let priceRB = DLRadioButton()
    var data: GetPriceListVehicles!{
        didSet{
            buildCell()
        }
    }
    
    var onRBSelection: actionBlock?

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .gray
        
        lblVehicleType.text = "VEHICLE TYPE"
        lblVehicleType.translatesAutoresizingMaskIntoConstraints = false
        lblPrice.text = "PRICE"
        lblPrice.translatesAutoresizingMaskIntoConstraints = false
        
        lblVehicleType.font = normalFontWithSize(10)
        lblPrice.font = normalFontWithSize(10)
        
        lblVehicleType.textColor = Colors.GRAY_COLOR
        lblPrice.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblVehicleType)
        self.contentView.addSubview(lblPrice)
        
        priceRB.isHidden = true
        priceRB.iconColor = Colors.GREEN_COLOR
        priceRB.indicatorColor = Colors.GREEN_COLOR
        priceRB.translatesAutoresizingMaskIntoConstraints = false
        lblPrice.addSubview(priceRB)
        
        
        
        let viewsDict = ["type":lblVehicleType, "price":lblPrice,"rb":priceRB]
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[type][rb(15)]-5-[price]-10-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[type]|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[price]|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[rb(15)]", options: [], metrics: nil, views: viewsDict))
         self.contentView.addConstraint(NSLayoutConstraint(item: priceRB, attribute: .centerY, relatedBy: .equal, toItem: lblPrice, attribute: .centerY, multiplier: 1, constant: 0))
        

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(selected){
            priceRB.isSelected = true
            onRBSelection?()
        }
        else{
            priceRB.isSelected = false
        }
        
    }
    
    func buildCell()
    {
        priceRB.isHidden = false
        lblVehicleType.font = normalFontWithSize(12)
        lblPrice.font = normalFontWithSize(12)
        lblVehicleType.text = data.vehicleType
        lblPrice.text = "\(RUPEE_SYMBOL) \(data.pricePrivate!)"
        
        setNeedsLayout()
    }
}

class SharedPriceEstimationCell: UITableViewCell {
    
    let lblVehicleType = UILabel()
    
    let lblAny = UILabel()
    let lblOnly = UILabel()
    let priceRBAny = DLRadioButton()
    let priceRBOnly = DLRadioButton()
    var isHeading = true
    var baseData: GetPriceListData?{
        didSet{
            buildCell()
        }
    }
    var data: GetPriceListVehicles!{
        didSet{
            buildCell()
        }
    }
    
    var onRBSelection: actionBlockWithParam?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .gray
        
        lblVehicleType.text = "VEHICLE TYPE"
        lblVehicleType.translatesAutoresizingMaskIntoConstraints = false
        lblAny.text = "ANY"
        lblAny.numberOfLines = 0
        //lblAny.textAlignment = .Center
        lblAny.translatesAutoresizingMaskIntoConstraints = false
        
        lblOnly.text = UserInfo.sharedInstance.gender == "M" ? "ONLY MALE" : "ONLY FEMALE"
        lblOnly.numberOfLines = 0
        //lblOnly.textAlignment = .Center
        lblOnly.translatesAutoresizingMaskIntoConstraints = false
        
        lblVehicleType.font = normalFontWithSize(10)
        lblAny.font = normalFontWithSize(10)
        lblOnly.font = normalFontWithSize(10)
        
        lblVehicleType.textColor = Colors.GRAY_COLOR
        lblAny.textColor = Colors.GRAY_COLOR
        lblOnly.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblVehicleType)
        self.contentView.addSubview(lblAny)
        self.contentView.addSubview(lblOnly)
        
        priceRBAny.isHidden = true
        priceRBAny.iconColor = Colors.GREEN_COLOR
        priceRBAny.indicatorColor = Colors.GREEN_COLOR
        priceRBAny.translatesAutoresizingMaskIntoConstraints = false
        priceRBAny.addTarget(self, action: #selector(priceSelected(_:)), for: .touchDown)
        self.contentView.addSubview(priceRBAny)

        priceRBOnly.isHidden = true
        priceRBOnly.iconColor = Colors.GREEN_COLOR
        priceRBOnly.indicatorColor = Colors.GREEN_COLOR
        priceRBOnly.translatesAutoresizingMaskIntoConstraints = false
        priceRBOnly.addTarget(self, action: #selector(priceSelected(_:)), for: .touchDown)
        self.contentView.addSubview(priceRBOnly)
        
        let viewsDict = ["type":lblVehicleType, "lblany":lblAny,"rb":priceRBAny, "lblonly":lblOnly,"rbo":priceRBOnly]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[type(100)]-10-[rb(15)]-5-[lblany]-10-[rbo(15)]-5-[lblonly(==lblany)]-10-|", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[type]|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lblany]|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[rb(15)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraint(NSLayoutConstraint(item: priceRBAny, attribute: .centerY, relatedBy: .equal, toItem: lblAny, attribute: .centerY, multiplier: 1, constant: 0))

        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lblonly]|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[rbo(15)]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraint(NSLayoutConstraint(item: priceRBOnly, attribute: .centerY, relatedBy: .equal, toItem: lblOnly, attribute: .centerY, multiplier: 1, constant: 0))

    }
    
    func priceSelected(_ btn: DLRadioButton)
    {
        if(btn == priceRBOnly){
            priceRBOnly.isSelected = true
            priceRBAny.isSelected = false
            if(UserInfo.sharedInstance.gender == "M"){
                onRBSelection?("m" as AnyObject?)
            }
            else{
                onRBSelection?("f" as AnyObject?)
            }
        }else{
            priceRBAny.isSelected = true
            priceRBOnly.isSelected = false
            onRBSelection?("" as AnyObject?)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(!selected){
            priceRBAny.isSelected = false
            priceRBOnly.isSelected = false
        }
    }
    
    func buildCell()
    {
        if(isHeading) {
            if(baseData != nil){
                if let disc = baseData?.discountAny{
                    lblAny.text = "ANY \n(\(disc)% Discount)"
                }
                
                if(UserInfo.sharedInstance.gender == "M"){
                    if let disc = baseData?.discountMale{
                        lblOnly.text = "ONLY MALE \n(\(disc)% Discount)"
                    }
                }
                else if(UserInfo.sharedInstance.gender == "F"){
                    if let disc = baseData?.discountFemale{
                        lblOnly.text = "ONLY FEMALE \n(\(disc)% Discount)"
                    }
                }
            }
        }
        else{
            priceRBAny.isHidden = false
            priceRBOnly.isHidden = false
            
            lblVehicleType.font = normalFontWithSize(12)
            lblAny.font = normalFontWithSize(12)
            lblOnly.font = normalFontWithSize(12)
            lblVehicleType.text = data.vehicleType
            lblAny.text = "\(RUPEE_SYMBOL) \(data.priceAny!)"
            if(UserInfo.sharedInstance.gender == "M"){
                lblOnly.text = "\(RUPEE_SYMBOL) \(data.priceMale!)"
            }
            else{
                lblOnly.text = "\(RUPEE_SYMBOL) \(data.priceFemale!)"
            }
            lblAny.textAlignment = .left
            lblOnly.textAlignment = .left
        }
        setNeedsLayout()
    }
}

class PriceEstimationViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource {
    var source :String?
    var destination :String?
    var distance :String?
    var duration :String?
    var date :String?
    var time :String?
    var taxiType :String?
    let priceListObj = GetPriceListRequest()
    var onCompletionBlock: actionBlockWith2ParamsString?
    var list: [GetPriceListVehicles]?
    
    lazy var tableView: SubView = self.getTableView()
    lazy var personalPriceDetailView: SubView = self.getPersonalPriceDetailView()
    lazy var sharedPriceDetailView: SubView = self.getSharedPriceDetailView()
    
    var baseData: GetPriceListData?
    var selectedItem: GetPriceListVehicles?
    var genderType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(taxiType == "P"){
            title = "Personal Price Estimate"
        }else{
            title = "Shared Price Estimate"
        }
        
        viewsArray.append(tableView)
        getPriceEstimationList()
        viewsArray.append(getMiddleView())
        addBottomView(getBottomView())
    }
    
    let table = UITableView()
    func getTableView() -> SubView
    {
        table.delegate = self
        table.dataSource = self
        table.isScrollEnabled = false
        table.separatorColor = UIColor.clear
        if(taxiType == "S") {
            table.register(SharedPriceEstimationCell.self, forCellReuseIdentifier: "sharedpriceestimationcell")
        } else {
        table.register(PrivatePriceEstimationCell.self, forCellReuseIdentifier: "privatepriceestimationcell")
        }
        
        let sub = SubView()
        sub.view = table
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: table, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    let lblDistance = UILabel()
    
    func getPersonalPriceDetailView() -> SubView
    {
        let view = UIView()
        view.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
        
        let lblPriceDetail = UILabel()
        lblPriceDetail.text = "PRICE DETAILS"
        lblPriceDetail.textColor = Colors.GREEN_COLOR
        lblPriceDetail.font = boldFontWithSize(14)
        lblPriceDetail.textAlignment = .center
        lblPriceDetail.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPriceDetail)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GREEN_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let lblDistanceTitle = UILabel()
        lblDistanceTitle.text = "Distance as per google"
        lblDistanceTitle.textColor = Colors.GRAY_COLOR
        lblDistanceTitle.font = boldFontWithSize(12)
        lblDistanceTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDistanceTitle)
        
        lblDistance.textColor = Colors.GRAY_COLOR
        lblDistance.font = boldFontWithSize(12)
        lblDistance.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDistance)

        let lblUpToFiftyTitle = UILabel()
        lblUpToFiftyTitle.text = "Price up to 50 Km"
        lblUpToFiftyTitle.textColor = Colors.GRAY_COLOR
        lblUpToFiftyTitle.font = boldFontWithSize(12)
        lblUpToFiftyTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblUpToFiftyTitle)
        
        lblUpToFifty.textColor = Colors.GRAY_COLOR
        lblUpToFifty.font = boldFontWithSize(12)
        lblUpToFifty.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblUpToFifty)

        let lblAfterFiftyTitle = UILabel()
        lblAfterFiftyTitle.text = "Price after 50 Km"
        lblAfterFiftyTitle.textColor = Colors.GRAY_COLOR
        lblAfterFiftyTitle.font = boldFontWithSize(12)
        lblAfterFiftyTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblAfterFiftyTitle)
        
        lblAfterFifty.textColor = Colors.GRAY_COLOR
        lblAfterFifty.font = boldFontWithSize(12)
        lblAfterFifty.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblAfterFifty)

        let lblEstimatedPriceTitle = UILabel()
        lblEstimatedPriceTitle.text = "Estimated Price"
        lblEstimatedPriceTitle.textColor = Colors.GRAY_COLOR
        lblEstimatedPriceTitle.font = boldFontWithSize(12)
        lblEstimatedPriceTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblEstimatedPriceTitle)
        
        lblEstimatedPrice.textColor = Colors.GRAY_COLOR
        lblEstimatedPrice.font = boldFontWithSize(12)
        lblEstimatedPrice.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblEstimatedPrice)
        
        let viewsDict = ["lblPriceDetail":lblPriceDetail, "line1":lblLine1, "lblDistanceTitle":lblDistanceTitle, "lblDistance":lblDistance, "lblUpToFiftyTitle":lblUpToFiftyTitle, "lblUpToFifty":lblUpToFifty, "lblAfterFiftyTitle":lblAfterFiftyTitle, "lblAfterFifty":lblAfterFifty, "lblEstimatedPriceTitle":lblEstimatedPriceTitle, "lblEstimatedPrice":lblEstimatedPrice]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lblPriceDetail]-3-[line1(0.5)]-5-[lblDistanceTitle]-5-[lblUpToFiftyTitle]-5-[lblAfterFiftyTitle]-5-[lblEstimatedPriceTitle]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblPriceDetail]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblDistanceTitle][lblDistance(==lblDistanceTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
         view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblUpToFiftyTitle][lblUpToFifty(==lblUpToFiftyTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
         view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblAfterFiftyTitle][lblAfterFifty(==lblAfterFiftyTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
         view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblEstimatedPriceTitle][lblEstimatedPrice(==lblEstimatedPriceTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: lblDistance, attribute: .centerY, relatedBy: .equal, toItem: lblDistanceTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblUpToFifty, attribute: .centerY, relatedBy: .equal, toItem: lblUpToFiftyTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblAfterFifty, attribute: .centerY, relatedBy: .equal, toItem: lblAfterFiftyTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblEstimatedPrice, attribute: .centerY, relatedBy: .equal, toItem: lblEstimatedPriceTitle, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(20, 30, 0, 30)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100)
        return sub
    }

    let lblDiscountTitle = UILabel()
    let lblUpToFifty = UILabel()
    let lblAfterFifty = UILabel()
    let lblPrice = UILabel()
    let lblDiscount = UILabel()
    let lblEstimatedPrice = UILabel()

    func getSharedPriceDetailView() -> SubView
    {
        let view = UIView()
        view.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5

        let lblPriceDetail = UILabel()
        lblPriceDetail.text = "PRICE DETAILS"
        lblPriceDetail.textColor = Colors.GREEN_COLOR
        lblPriceDetail.font = boldFontWithSize(14)
        lblPriceDetail.textAlignment = NSTextAlignment.center
        lblPriceDetail.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPriceDetail)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GREEN_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let lblDistanceTitle = UILabel()
        lblDistanceTitle.text = "Distance as per google"
        lblDistanceTitle.textColor = Colors.GRAY_COLOR
        lblDistanceTitle.font = boldFontWithSize(12)
        lblDistanceTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDistanceTitle)
        
        lblDistance.textColor = Colors.GRAY_COLOR
        lblDistance.font = boldFontWithSize(12)
        lblDistance.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDistance)
        
        let lblUpToFiftyTitle = UILabel()
        lblUpToFiftyTitle.text = "Price up to 50 Km"
        lblUpToFiftyTitle.textColor = Colors.GRAY_COLOR
        lblUpToFiftyTitle.font = boldFontWithSize(12)
        lblUpToFiftyTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblUpToFiftyTitle)
        
        lblUpToFifty.textColor = Colors.GRAY_COLOR
        lblUpToFifty.font = boldFontWithSize(12)
        lblUpToFifty.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblUpToFifty)
        
        let lblAfterFiftyTitle = UILabel()
        lblAfterFiftyTitle.text = "Price after 50 Km"
        lblAfterFiftyTitle.textColor = Colors.GRAY_COLOR
        lblAfterFiftyTitle.font = boldFontWithSize(12)
        lblAfterFiftyTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblAfterFiftyTitle)
        
        lblAfterFifty.textColor = Colors.GRAY_COLOR
        lblAfterFifty.font = boldFontWithSize(12)
        lblAfterFifty.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblAfterFifty)
        
        let lblPriceTitle = UILabel()
        lblPriceTitle.text = "Price"
        lblPriceTitle.textColor = Colors.GRAY_COLOR
        lblPriceTitle.font = boldFontWithSize(12)
        lblPriceTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPriceTitle)
        
        lblPrice.textColor = Colors.GRAY_COLOR
        lblPrice.font = boldFontWithSize(12)
        lblPrice.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblPrice)
        
        lblDiscountTitle.textColor = Colors.GRAY_COLOR
        lblDiscountTitle.font = boldFontWithSize(12)
        lblDiscountTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDiscountTitle)
        
        lblDiscount.textColor = Colors.GRAY_COLOR
        lblDiscount.font = boldFontWithSize(12)
        lblDiscount.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblDiscount)
        
        let lblEstimatedPriceTitle = UILabel()
        lblEstimatedPriceTitle.text = "Estimated Price"
        lblEstimatedPriceTitle.textColor = Colors.GRAY_COLOR
        lblEstimatedPriceTitle.font = boldFontWithSize(12)
        lblEstimatedPriceTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblEstimatedPriceTitle)
        
        lblEstimatedPrice.textColor = Colors.GRAY_COLOR
        lblEstimatedPrice.font = boldFontWithSize(12)
        lblEstimatedPrice.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblEstimatedPrice)
        
        let viewsDict = ["lblPriceDetail":lblPriceDetail, "line1":lblLine1, "lblDistanceTitle":lblDistanceTitle, "lblDistance":lblDistance, "lblUpToFiftyTitle":lblUpToFiftyTitle, "lblUpToFifty":lblUpToFifty, "lblAfterFiftyTitle":lblAfterFiftyTitle, "lblAfterFifty":lblAfterFifty, "lblPriceTitle":lblPriceTitle, "lblPrice":lblPrice, "lblDiscountTitle":lblDiscountTitle, "lblDiscount":lblDiscount, "lblEstimatedPriceTitle":lblEstimatedPriceTitle, "lblEstimatedPrice":lblEstimatedPrice]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lblPriceDetail]-3-[line1(0.5)]-5-[lblDistanceTitle]-5-[lblUpToFiftyTitle]-5-[lblAfterFiftyTitle]-5-[lblPriceTitle]-5-[lblDiscountTitle]-5-[lblEstimatedPriceTitle]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblPriceDetail]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblDistanceTitle][lblDistance(==lblDistanceTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblUpToFiftyTitle][lblUpToFifty(==lblUpToFiftyTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblAfterFiftyTitle][lblAfterFifty(==lblAfterFiftyTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblPriceTitle][lblPrice(==lblPriceTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblDiscountTitle][lblDiscount(==lblDiscountTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblEstimatedPriceTitle][lblEstimatedPrice(==lblEstimatedPriceTitle)]-10-|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: lblDistance, attribute: .centerY, relatedBy: .equal, toItem: lblDistanceTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblUpToFifty, attribute: .centerY, relatedBy: .equal, toItem: lblUpToFiftyTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblAfterFifty, attribute: .centerY, relatedBy: .equal, toItem: lblAfterFiftyTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblPrice, attribute: .centerY, relatedBy: .equal, toItem: lblPriceTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblDiscount, attribute: .centerY, relatedBy: .equal, toItem: lblDiscountTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblEstimatedPrice, attribute: .centerY, relatedBy: .equal, toItem: lblEstimatedPriceTitle, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(20, 30, 0, 30)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 140)
        return sub
    }

    func getMiddleView() -> SubView
    {
        let view = UIView()
        
        let img1 = UIImageView()
        img1.image = UIImage(named: "app_small")
        img1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(img1)
        
        let img2 = UIImageView()
        img2.image = UIImage(named: "app_small")
        img2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(img2)
        
        let lbl1 = UILabel()
        lbl1.text = "Above price includes toll & night charges"
        lbl1.textColor = Colors.BLACK_COLOR
        lbl1.font = boldFontWithSize(11)
        lbl1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lbl1)
        
        let lbl2 = UILabel()
        lbl2.text = "Exact ride distnace and cost will be shared with you post ride"
        lbl2.textColor = Colors.BLACK_COLOR
        lbl2.font = boldFontWithSize(11)
        lbl2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lbl2)
    
        if(taxiType == "S") {
            
            let img3 = UIImageView()
            img3.image = UIImage(named: "app_small")
            img3.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(img3)
            
            let lbl3 = UILabel()
            lbl3.text = "You will pay 45% lesser for shared taxi if one more person joins"
            lbl3.textColor = Colors.BLACK_COLOR
            lbl3.font = boldFontWithSize(11)
            lbl3.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lbl3)

             view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl1(30)][lbl2(30)][lbl3(30)]", options: [], metrics: nil, views: ["lbl1":lbl1,"lbl2":lbl2,"lbl3":lbl3]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[img3(15)]-3-[lbl3]-5-|", options: [], metrics: nil, views: ["img3":img3,"lbl3":lbl3]))
            view.addConstraint(NSLayoutConstraint(item: img3, attribute: .centerY, relatedBy: .equal, toItem: lbl3, attribute: .centerY, multiplier: 1, constant: 0))
        } else {
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl1(30)][lbl2(30)]", options: [], metrics: nil, views: ["lbl1":lbl1,"lbl2":lbl2]))
        }
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[img2(15)]-3-[lbl2]-5-|", options: [], metrics: nil, views: ["img2":img2,"lbl2":lbl2]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[img1(15)]-3-[lbl1]-5-|", options: [], metrics: nil, views: ["img1":img1,"lbl1":lbl1]))
        
        view.addConstraint(NSLayoutConstraint(item: img2, attribute: .centerY, relatedBy: .equal, toItem: lbl2, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: img1, attribute: .centerY, relatedBy: .equal, toItem: lbl1, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(20, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 80)
        return sub
    }

    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let btn = UIButton()
        btn.setTitle("SAVE", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(proceed), for: .touchDown)
        view.addSubview(btn)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(40)]", options: [], metrics: nil, views: ["btn":btn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btn]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func proceed()
    {
        _ = self.navigationController?.popViewController(animated: true)
        onCompletionBlock?(selectedItem,genderType)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let cnt = list?.count{
            return cnt+1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if(taxiType == "S") {
            cell = tableView.dequeueReusableCell(withIdentifier: "sharedpriceestimationcell") as! SharedPriceEstimationCell
            if(indexPath.row != 0){
                (cell as? SharedPriceEstimationCell)?.isHeading = false
                (cell as? SharedPriceEstimationCell)?.data = list![indexPath.row-1]
                (cell as? SharedPriceEstimationCell)?.onRBSelection = { (type) -> Void in
                    
                    if(self.viewsArray.count < 3){
                        self.viewsArray.insert(self.sharedPriceDetailView, at: 1)
                        self.relayoutSubview()
                    }
                    self.selectedItem = self.list![indexPath.row-1]
                    self.genderType = type as? String
                    self.setSharedPriceDetailsValue(type as! String)
                    self.table.selectRow(at: indexPath, animated: true, scrollPosition: .none)
                }
            }
            else{
                (cell as? SharedPriceEstimationCell)?.baseData = baseData
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "privatepriceestimationcell") as! PrivatePriceEstimationCell
            if(indexPath.row != 0){
                (cell as? PrivatePriceEstimationCell)?.data = list![indexPath.row-1]
                (cell as? PrivatePriceEstimationCell)?.onRBSelection = {
                    
                    if(self.viewsArray.count < 3){
                        self.viewsArray.insert(self.personalPriceDetailView, at: 1)
                        self.relayoutSubview()
                    }
                    self.selectedItem = self.list![indexPath.row-1]
                    self.setPrivatePriceDetailsValue()
                }
            }
        }
        
        cell!.selectionStyle = .none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func setSharedPriceDetailsValue(_ gender: String) {
        
        lblDistance.text = "\(": ") \((baseData?.rideDistance!)!) \(" Kms")"
        
        if(selectedItem != nil){
            
            lblUpToFifty.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.fixedPrice!)"
            let basePrice = (selectedItem!.basePrice! as NSString).integerValue
            lblAfterFifty.text = "\(": ") \(RUPEE_SYMBOL) \(basePrice)\(" / Km")"
            lblPrice.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.priceMain!)"

            if(gender == ""){
                lblDiscountTitle.text = "\((baseData?.discountAny!)!)\("% Discount")"
                 lblDiscount.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.priceMain! - selectedItem!.priceAny!)"
                lblEstimatedPrice.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.priceAny!)"
            }
            else if(gender == "m"){
                lblDiscountTitle.text = "\((baseData?.discountMale!)!)\("% Discount")"
                 lblDiscount.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.priceMain! - selectedItem!.priceMale!)"
                lblEstimatedPrice.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.priceMale!)"
            }
            else if(gender == "f"){
                lblDiscountTitle.text = "\((baseData?.discountFemale!)!)\("% Discount")"
                 lblDiscount.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.priceMain! - selectedItem!.priceFemale!)"
                lblEstimatedPrice.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.priceFemale!)"
            }
        }
    }
    
    func setPrivatePriceDetailsValue() {
        
        lblDistance.text = "\(": ") \((baseData?.rideDistance!)!) \(" Kms")"
        
        if(selectedItem != nil){
            lblUpToFifty.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.fixedPrice!)"
            let basePrice = (selectedItem!.basePrice! as NSString).integerValue
            lblAfterFifty.text = "\(": ") \(RUPEE_SYMBOL) \(basePrice)\(" / Km")"
            lblEstimatedPrice.text = "\(": ") \(RUPEE_SYMBOL) \(selectedItem!.pricePrivate!)"
        }
    }
    
    
    func getPriceEstimationList()
    {
        self.priceListObj.source = self.source
        self.priceListObj.destination = self.destination
        self.priceListObj.distance = self.distance
        self.priceListObj.duration = self.duration
        self.priceListObj.date = self.date
        self.priceListObj.time = self.time
        self.priceListObj.taxi_type = taxiType
        showIndicator("Getting price estimation..")
        AirportRequestor().getPriceEstimationList(self.priceListObj, success: { (success, object) in
            hideIndicator()
            if(success){
                self.list = (object as! GetPriceListResponse).dataObj?.first?.vehiclesObj
                self.baseData = (object as! GetPriceListResponse).dataObj?.first
                if let cnt = self.list?.count{
                    self.tableView.heightConstraint?.constant = 30 * CGFloat(cnt+1)
                    self.table.reloadData()
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
}
