//
//  LeftViewController.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import UIKit

class LeftItem: NSObject {
    var title: String!
    var imageName: String!
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
    
    func selectedImageName () -> String {
        return imageName + "_a"
    }
}


class LeftViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var imageButtonView  = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.GREEN_COLOR
        createProfileView()
        NotificationCenter.default.addObserver(self, selector: #selector(callAirportTab), name: NSNotification.Name(rawValue: "airportTab"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(callRideTab), name: NSNotification.Name(rawValue: "rideTab"), object: nil)
    }
    
    func callAirportTab(_ notification: Notification){
        let navC = self.evo_drawerController?.centerViewController as? UITabBarController
        navC?.selectedIndex = 2
        self.evo_drawerController?.closeDrawer(animated: true, completion: nil)
    }
    
    func callRideTab(_ notification: Notification){
        let navC = self.evo_drawerController?.centerViewController as? UITabBarController
        navC?.selectedIndex = 0
        self.evo_drawerController?.closeDrawer(animated: true, completion: nil)
        NotificationCenter.default.post(name: Notification.Name(rawValue: "myTaxiRides"), object: nil)
    }
    
    func createProfileView() -> Void
    {
        imageButtonView.translatesAutoresizingMaskIntoConstraints = false
        imageButtonView.layer.masksToBounds = false
        imageButtonView.layer.cornerRadius = 30
        imageButtonView.clipsToBounds = true
        self.view.addSubview(imageButtonView)
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(LeftViewController.imageItemTapped))
        imageButtonView.isUserInteractionEnabled = true
        imageButtonView.addGestureRecognizer(tapGestureRecognizer)
        
        let profileName  = UILabel()
        profileName.text = UserInfo.sharedInstance.fullName
        profileName.translatesAutoresizingMaskIntoConstraints = false
        profileName.textColor = Colors.WHITE_COLOR
        profileName.numberOfLines = 0
        profileName.tag = 1000
        profileName.lineBreakMode = NSLineBreakMode.byWordWrapping
        profileName.textAlignment = NSTextAlignment.left
        profileName.font = normalFontWithSize(15)
        self.view.addSubview(profileName)
        let tapGestureRecognizer1 = UITapGestureRecognizer(target:self, action:#selector(LeftViewController.imageItemTapped))
        profileName.isUserInteractionEnabled = true
        profileName.addGestureRecognizer(tapGestureRecognizer1)
        
        let profileEmailID  = UILabel()
        profileEmailID.translatesAutoresizingMaskIntoConstraints = false
        profileEmailID.textColor = Colors.WHITE_COLOR
        profileEmailID.numberOfLines = 0
        profileEmailID.lineBreakMode = NSLineBreakMode.byWordWrapping
        profileEmailID.text = UserInfo.sharedInstance.email
        profileEmailID.font = normalFontWithSize(15)
        self.view.addSubview(profileEmailID)
        let tapGestureRecognizer2 = UITapGestureRecognizer(target:self, action:#selector(LeftViewController.imageItemTapped))
        profileEmailID.isUserInteractionEnabled = true
        profileEmailID.addGestureRecognizer(tapGestureRecognizer2)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-25-[imagebuttonview(60)]", options:[], metrics:nil, views: ["imagebuttonview": imageButtonView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-40-[profilename(20)][profileemailid(20)]", options:[], metrics:nil, views: ["profilename":profileName,"profileemailid":profileEmailID]))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[imagebuttonview(60)]-5-[profilename]-10-|", options:[], metrics:nil, views: ["imagebuttonview": imageButtonView,"profilename":profileName]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[profileemailid]", options:[], metrics:nil, views: ["profileemailid":profileEmailID]))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createDataSource()
        setUpTableView()
        (self.view.viewWithTag(1000) as? UILabel)?.text =  UserInfo.sharedInstance.fullName
        
        if(UserInfo.sharedInstance.profilepicStatus == "1"){
            if UserInfo.sharedInstance.profilepicUrl != ""{
                imageButtonView.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(UserInfo.sharedInstance.profilepicUrl)"))
            }
            else{
                if UserInfo.sharedInstance.faceBookid != ""{
                    imageButtonView.kf.setImage(with: URL(string: "https://graph.facebook.com/\(UserInfo.sharedInstance.faceBookid)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            imageButtonView.image = UIImage(named: "ProfileImage_S")
        }
    }
    
    let tableView = UITableView()
    var dataSource = [LeftItem]()
    
    func showAnywhere() {
        dataSource.removeAll()
        
        let m_ride = LeftItem(title: "Anywhere", imageName: "m_anywhere_green")
        let m_workplace = LeftItem(title: "Workplace", imageName: "m_workplace_big")
        let m_airport = LeftItem(title: "Airport", imageName: "m_airport_big")
        let m_myrides = LeftItem(title: "My Rides", imageName: "m_myride_green")
        let m_sharetaxi = LeftItem(title: "Share a Taxi", imageName: "m_share_taxi_green")
        let m_profile = LeftItem(title: "Profile", imageName: "m_profile_big")
        let m_offer = LeftItem(title: "Offers & Promotions", imageName: "m_offer_big")
        let m_emergencycontacts = LeftItem(title: "Emergency Contact", imageName: "m_emergencycontacts_big")
        let m_myaccount = LeftItem(title: "MY Accounts", imageName: "m_myaccount_big")
        
        let m_guidelines = LeftItem(title: "Guidelines", imageName: "m_guidelines_big")
        let m_testimonials = LeftItem(title: "Testimonial", imageName: "m_testimonials_big")
        let m_help = LeftItem(title: "Help", imageName: "m_help_big")
        let m_contactus = LeftItem(title: "Contact Us", imageName: "m_contactus_big")
        let m_logout = LeftItem(title: "Logout", imageName: "m_logout_big")
        
        dataSource.append(m_ride)
        dataSource.append(m_workplace)
        dataSource.append(m_airport)
        dataSource.append(m_myrides)
        dataSource.append(m_sharetaxi)
        dataSource.append(m_profile)
        dataSource.append(m_offer)
        dataSource.append(m_emergencycontacts)
        dataSource.append(m_myaccount)
        dataSource.append(m_guidelines)
        dataSource.append(m_testimonials)
        dataSource.append(m_help)
        dataSource.append(m_contactus)
        dataSource.append(m_logout)
        
        tableView.reloadData()
    }
    
    func hideAnywhere() {
        dataSource.removeAll()
        
        let m_workplace = LeftItem(title: "Workplace", imageName: "m_workplace_big")
        let m_airport = LeftItem(title: "Airport", imageName: "m_airport_big")
        let m_myrides = LeftItem(title: "My Rides", imageName: "m_myride_green")
        let m_sharetaxi = LeftItem(title: "Share a Taxi", imageName: "m_share_taxi_green")
        let m_profile = LeftItem(title: "Profile", imageName: "m_profile_big")
        let m_offer = LeftItem(title: "Offers & Promotions", imageName: "m_offer_big")
        let m_emergencycontacts = LeftItem(title: "Emergency Contact", imageName: "m_emergencycontacts_big")
        let m_myaccount = LeftItem(title: "MY Transactions", imageName: "m_transaction")
        let m_guidelines = LeftItem(title: "Guidelines", imageName: "m_guidelines_big")
        let m_testimonials = LeftItem(title: "Testimonial", imageName: "m_testimonials_big")
        let m_help = LeftItem(title: "Help", imageName: "m_help_big")
        let m_contactus = LeftItem(title: "Contact Us", imageName: "m_contactus_big")
        let m_logout = LeftItem(title: "Logout", imageName: "m_logout_big")
        
        dataSource.append(m_workplace)
        dataSource.append(m_airport)
        dataSource.append(m_myrides)
        dataSource.append(m_sharetaxi)
        dataSource.append(m_profile)
        dataSource.append(m_offer)
        dataSource.append(m_emergencycontacts)
        dataSource.append(m_myaccount)
        dataSource.append(m_guidelines)
        dataSource.append(m_testimonials)
        dataSource.append(m_help)
        dataSource.append(m_contactus)
        dataSource.append(m_logout)
        
        tableView.reloadData()
    }
    
    func createDataSource() {
        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
            hideAnywhere()
        } else {
            showAnywhere()
        }
    }
    
    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = false
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorColor = UIColor.clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(tableView)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[table]|", options: [], metrics: nil, views: ["table":tableView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier: String = "Cell"
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier)
        
        let object = self.dataSource[indexPath.row]
        cell.textLabel?.text = object.title
        cell.textLabel?.textColor = Colors.GREEN_COLOR
        cell.textLabel?.font = normalFontWithSize(16)
        cell.imageView?.image = UIImage(named: object.imageName)
        if((indexPath.row == 0) || (indexPath.row == 1) || (indexPath.row == 2)){
            cell.selectionStyle = .default
            
            let bgColorView = UIView()
            bgColorView.backgroundColor = getColorByHex(0xDDF8F2, alpha: 1.0)
            cell.selectedBackgroundView = bgColorView
            
        }else{
            cell.selectionStyle = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
            onSelectionRowHide(indexPath)
        } else {
            onSelectionRow(indexPath)
        }
    }
    
    func onSelectionRowHide(_ indexPath: IndexPath)
    {
        if(indexPath.row == 0){
            let navC = self.evo_drawerController?.centerViewController as? UITabBarController
            navC?.selectedIndex = indexPath.row
        } else if(indexPath.row == 1){
            let navC = self.evo_drawerController?.centerViewController as? UITabBarController
            navC?.selectedIndex = indexPath.row
        }
            //        else if(indexPath.row == 5){
            //            let navC = self.evo_drawerController?.centerViewController as? UITabBarController
            //            navC?.selectedIndex = indexPath.row
            //        }
        else
        {
            //            if(indexPath.row == 0){
            //                let vc = DashBoardViewController()
            //                let ins = UIApplication.sharedApplication().delegate as! AppDelegate
            //                ins.changeRootViewControllerWithViewController(UINavigationController(rootViewController: vc))
            //            }
            if(indexPath.row == 2) {
                let vc = RideViewController()
                vc.myRides = true
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 3){
                let vc = ShareTaxiViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 4){
                let vc = ProfileViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 5){
                if !isServerReachable() {
                    AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
                } else {
                    let vc = OffersPromotionsViewController()
                    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }
            else if(indexPath.row == 6){
                let vc = EmergencyContactViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 7){
                let vc = PointsTableViewController()
                vc.myAccounts = false
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 8){
                let vc = GuidelinesViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 9){
                if !isServerReachable() {
                    AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
                } else {
                    let vc = TestimonialViewController()
                    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }
            else if(indexPath.row == 10){
                if !isServerReachable() {
                    AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
                } else {
                    let vc = HelpViewController()
                    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }
            else if(indexPath.row == 11){
                let vc = FeedbackViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 12){
                
                AlertController.showAlertFor("Logout", message: "Are you sure you want to logout", okButtonTitle: "Logout", okAction: {
                    UserInfo.sharedInstance.logoutUser()
                    let ins = UIApplication.shared.delegate as! AppDelegate
                    ins.changeRootViewControllerWithViewController(UINavigationController(rootViewController: LoginOptionViewController()))
                    }, cancelButtonTitle: "Cancel", cancelAction: nil)
            }
        }
        
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
    }
    
    func onSelectionRow(_ indexPath: IndexPath)
    {
        if(indexPath.row == 0){
            let navC = self.evo_drawerController?.centerViewController as? UITabBarController
            navC?.selectedIndex = indexPath.row
        } else if(indexPath.row == 1){
            let navC = self.evo_drawerController?.centerViewController as? UITabBarController
            navC?.selectedIndex = indexPath.row
        }else if(indexPath.row == 2){
            let navC = self.evo_drawerController?.centerViewController as? UITabBarController
            navC?.selectedIndex = indexPath.row
        }
            //        else if(indexPath.row == 5){
            //            let navC = self.evo_drawerController?.centerViewController as? UITabBarController
            //            navC?.selectedIndex = indexPath.row
            //        }
        else
        {
            //            if(indexPath.row == 0){
            //                let vc = DashBoardViewController()
            //                let ins = UIApplication.sharedApplication().delegate as! AppDelegate
            //                ins.changeRootViewControllerWithViewController(UINavigationController(rootViewController: vc))
            //            }
            if(indexPath.row == 3) {
                let vc = RideViewController()
                vc.myRides = true
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 4){
                let vc = ShareTaxiViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 5){
                let vc = ProfileViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 6){
                if !isServerReachable() {
                    AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
                } else {
                    let vc = OffersPromotionsViewController()
                    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }
            else if(indexPath.row == 7){
                let vc = EmergencyContactViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 8){
                let vc = MyAccountViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 9){
                let vc = GuidelinesViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 10){
                if !isServerReachable() {
                    AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
                } else {
                    let vc = TestimonialViewController()
                    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }
            else if(indexPath.row == 11){
                if !isServerReachable() {
                    AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
                } else {
                    let vc = HelpViewController()
                    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }
            else if(indexPath.row == 12){
                let vc = FeedbackViewController()
                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
            }
            else if(indexPath.row == 13){
                
                AlertController.showAlertFor("Logout", message: "Are you sure you want to logout", okButtonTitle: "Logout", okAction: {
                    UserInfo.sharedInstance.logoutUser()
                    let ins = UIApplication.shared.delegate as! AppDelegate
                    ins.changeRootViewControllerWithViewController(UINavigationController(rootViewController: LoginOptionViewController()))
                    }, cancelButtonTitle: "Cancel", cancelAction: nil)
            }
        }
        
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
    }

    //    func tableView(tableView: UITableView, didHighlightRowAtIndexPath indexPath: NSIndexPath) {
    //        if((indexPath.row == 0) || (indexPath.row == 1) || (indexPath.row == 2)){
    //            let cell = tableView.cellForRowAtIndexPath(indexPath)
    //            cell?.backgroundColor = Colors.LIGHT_GREEN_COLOR
    //        }
    //        //onSelectionRow(indexPath)
    //    }
    
    //    func tableView(tableView: UITableView, didUnhighlightRowAtIndexPath indexPath: NSIndexPath) {
    //        let cell = tableView.cellForRowAtIndexPath(indexPath)
    //        UIView.animateWithDuration(0.2) {
    //            cell?.backgroundColor = UIColor.clearColor()
    //        }
    //    }
    
    func imageItemTapped(_ sender:AnyObject) -> Void {
        let vc = ProfileViewController()
        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
    }
    
    
    func getColorByHex(_ rgbHexValue:UInt32, alpha:Double = 1.0) -> UIColor {
        let red = Double((rgbHexValue & 0xFF0000) >> 16) / 256.0
        let green = Double((rgbHexValue & 0xFF00) >> 8) / 256.0
        let blue = Double((rgbHexValue & 0xFF)) / 256.0
        
        return UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: CGFloat(alpha))
    }
    
    
}
