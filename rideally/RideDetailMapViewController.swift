//
//  RideDetailMapViewController.swift
//  rideally
//
//  Created by Sarav on 10/11/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import GoogleMaps

class RideDetailMapViewController : UIViewController, CLLocationManagerDelegate, UITextFieldDelegate, GMSMapViewDelegate {
    
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var mapView:GMSMapView!
    
    let sourceView = UIView()
    let destView = UIView()
    
    let sourceTextField = UITextField()
    let destTextField = UITextField()
    
    var mapSourcePinImageVIew = UIImageView()
    var mapDestPinImageVIew = UIImageView()
    
    var currentLocationView = UIView()
    var userMovedMap: Bool = false
    
    var locationButton: UIButton = UIButton(type: .system)
    
    let appDelegate: AppDelegate = sharedAppDelegate()!
    var locationButtonAction: VoidCompletionHandler?
    
    var shouldShowCancelButton = false
    
    var cancelButton = UIButton(type: .system)
    var currentLocationButton = UIButton(type: .custom)
    var locationName: String = ""
    
    var onSelectionBlock: actionBlockWithParam?
    var onConfirmLocationsBlock: actionBlockWith6Params?
    
    var jumpOneScreen: Bool = false
    
    var sourceLat: String?
    var sourceLong: String?
    var sourceLocation: String?
    var destLat: String?
    var destLong: String?
    var destLocation: String?
    var taxiSourceID = ""
    var taxiDestID = ""
    var poolTaxiBooking = ""
    var triggerFrom = ""
    var poolId = ""
    var selectedTextField: UITextField?
    var rideType = ""
    var isWP = false
    var wpInsuranceStatus = ""
    func goBack()
    {
        if(jumpOneScreen){
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
        }
        else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        
        // Do any additional setup after loading the view, typically from a nib.
        mapSourcePinImageVIew.image = UIImage(imageLiteralResourceName: "map_G_HW_15x15")
        mapDestPinImageVIew.image = UIImage(imageLiteralResourceName: "map_G_HW_15x15")
        
//        if(UserInfo.sharedInstance.profilepicStatus == "1"){
//            if UserInfo.sharedInstance.profilepicUrl != ""{
//                mapSourcePinImageVIew.kf_setImageWithURL(NSURL(string: "\(URLS.IMG_URL)\(UserInfo.sharedInstance.profilepicUrl)"))
//                mapDestPinImageVIew.kf_setImageWithURL(NSURL(string: "\(URLS.IMG_URL)\(UserInfo.sharedInstance.profilepicUrl)"))
//            }
//            else{
//                if UserInfo.sharedInstance.faceBookid != ""{
//                    mapSourcePinImageVIew.kf_setImageWithURL(NSURL(string: "https://graph.facebook.com/\(UserInfo.sharedInstance.faceBookid)/picture?type=large&return_ssl_resources=1"))
//                    mapDestPinImageVIew.kf_setImageWithURL(NSURL(string: "https://graph.facebook.com/\(UserInfo.sharedInstance.faceBookid)/picture?type=large&return_ssl_resources=1"))
//                }
//            }
//        }
//        else{
//            mapSourcePinImageVIew.image = UIImage(named: "placeholder")
//            mapDestPinImageVIew.image = UIImage(named: "placeholder")
//        }
        
        if(triggerFrom != "" && triggerFrom == "joinRide") {
            title = "Choose Pickup & Drop Location"
        } else {
            title = "Choose Start & End Point"
        }
        self.sourceTextField.delegate = self
        self.destTextField.delegate = self
        setupTextfield()
        //pickUpTextfield()
        self.setupGoogleMaps()
        sourceTextField.font = boldFontWithSize(15.0)
        destTextField.font = boldFontWithSize(15.0)
        //sourceTextField.becomeFirstResponder()
        selectedTextField = nil
        mapSourcePinImageVIew.isHidden = true
        mapDestPinImageVIew.isHidden = true
        showIndicator("Getting directions..")
        getDirections()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.locationCoord == nil {
            self.getCurrentLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGoogleMaps () {
        
        let lat = Double(sourceLat!)!
        let lng = Double(sourceLong!)!
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        self.moveMapToCoord(CLLocationCoordinate2DMake(lat, lng))
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        self.view.sendSubview(toBack: self.mapView)
        
        if(poolTaxiBooking == "1") {
            if(taxiSourceID == "1281") {
                let dict = ["mapView": mapView as NSObject, "dest": destView]
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dest(50)][mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                //self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[source]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dest]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                
                mapDestPinImageVIew.translatesAutoresizingMaskIntoConstraints = false
                self.mapView.addSubview(self.mapDestPinImageVIew)
                
                self.mapView.addConstraint(NSLayoutConstraint(item: self.mapDestPinImageVIew, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 0))
                self.mapView.addConstraint(NSLayoutConstraint(item: self.mapDestPinImageVIew, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: -(mapDestPinImageVIew.image?.size.height)!/2.0 + 10))
                sourceView.isHidden = true
            } else if(taxiDestID == "1281") {
                let dict = ["mapView": mapView as NSObject, "source":sourceView]
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(50)][mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[source]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                //self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[dest]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
                
                mapSourcePinImageVIew.translatesAutoresizingMaskIntoConstraints = false
                self.mapView.addSubview(self.mapSourcePinImageVIew)
                
                self.mapView.addConstraint(NSLayoutConstraint(item: self.mapSourcePinImageVIew, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 0))
                self.mapView.addConstraint(NSLayoutConstraint(item: self.mapSourcePinImageVIew, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: -(mapSourcePinImageVIew.image?.size.height)!/2.0 + 10))
                destView.isHidden = true
            }
        }else {
            let dict = ["mapView": mapView as NSObject, "source":sourceView, "dest": destView]
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(50)][dest(50)][mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[source]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dest]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            
            mapSourcePinImageVIew.translatesAutoresizingMaskIntoConstraints = false
            self.mapView.addSubview(self.mapSourcePinImageVIew)
            
            self.mapView.addConstraint(NSLayoutConstraint(item: self.mapSourcePinImageVIew, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 0))
            self.mapView.addConstraint(NSLayoutConstraint(item: self.mapSourcePinImageVIew, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: -(mapSourcePinImageVIew.image?.size.height)!/2.0 + 10))
            
            mapDestPinImageVIew.translatesAutoresizingMaskIntoConstraints = false
            self.mapView.addSubview(self.mapDestPinImageVIew)
            
            self.mapView.addConstraint(NSLayoutConstraint(item: self.mapDestPinImageVIew, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerX, multiplier: 1.0, constant: 0))
            self.mapView.addConstraint(NSLayoutConstraint(item: self.mapDestPinImageVIew, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self.mapView, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: -(mapDestPinImageVIew.image?.size.height)!/2.0 + 10))
            sourceView.isHidden = false
            destView.isHidden = false
        }
        
        setupLocationButton()
        self.mapView.addSubview(self.locationButton)
        
        
        self.mapView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":self.locationButton]))
        self.mapView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]|", options: [], metrics: nil, views: ["btn":self.locationButton]))
        
        //        self.mapView.addConstraint(NSLayoutConstraint(item: self.locationButton, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        //        self.mapView.addConstraint(NSLayoutConstraint(item: self.locationButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 10))
        //        self.mapView.addConstraint(NSLayoutConstraint(item: self.locationButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1.0, constant: 40))
    }
    
    func setupLocationButton () {
        if(triggerFrom == "joinRide" || poolTaxiBooking == "1") {
            self.locationButton.setTitle("SEND REQUEST", for: UIControlState())
        } else {
            self.locationButton.setTitle("SUBMIT", for: UIControlState())
        }
        self.locationButton.backgroundColor = Colors.GRAY_COLOR
        self.locationButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        self.locationButton.titleLabel?.font = boldFontWithSize(16.0)
        self.locationButton.translatesAutoresizingMaskIntoConstraints = false
        self.locationButton.addTarget(self, action: #selector(selectLocationButtonTapped(_:)), for: UIControlEvents.touchUpInside)
    }
    
    func setupTextfield () {
        
        sourceView.translatesAutoresizingMaskIntoConstraints = false
        sourceView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(sourceView)
        
        let lblSource = UILabel()
        if(triggerFrom == "joinRide") {
            lblSource.text = "SELECT PICKUP LOCATION"
        } else {
            lblSource.text = "SELECT START POINT"
        }
        lblSource.font = normalFontWithSize(12)
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        sourceView.addSubview(lblSource)
        
        let imageSrc = UIImage(named: "source")
        let frameSrc = CGRect(x: 0, y: 0, width: (imageSrc?.size.width)!, height: (imageSrc?.size.height)!)
        let leftV = UIImageView(frame: frameSrc)
        leftV.image = imageSrc
        leftV.contentMode = UIViewContentMode.center
        
        sourceTextField.placeholder = "Enter your location"
        self.sourceTextField.leftView = leftV
        self.sourceTextField.leftViewMode = UITextFieldViewMode.always
        self.sourceTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        sourceTextField.translatesAutoresizingMaskIntoConstraints = false
        sourceTextField.backgroundColor = Colors.WHITE_COLOR
        
        let imageDest = UIImage(named: "dest")
        let frameDest = CGRect(x: 0, y: 0, width: (imageDest?.size.width)!, height: (imageDest?.size.height)!)
        let leftV1 = UIImageView(frame: frameDest)
        leftV1.image = imageDest
        leftV1.contentMode = UIViewContentMode.center
        
        sourceView.addSubview(sourceTextField)
        sourceView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(15)][txt(35)]|", options: [], metrics: nil, views: ["lbl":lblSource, "txt":sourceTextField]))
        sourceView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]|", options: [], metrics: nil, views: ["lbl":lblSource, "txt":sourceTextField]))
        sourceView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txt]|", options: [], metrics: nil, views: ["lbl":lblSource, "txt":sourceTextField]))
        
        destView.translatesAutoresizingMaskIntoConstraints = false
        destView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(destView)
        
        let lblDest = UILabel()
        if(triggerFrom == "joinRide") {
            lblDest.text = "SELECT DROP LOCATION"
        } else {
            lblDest.text = "SELECT END POINT"
        }
        lblDest.font = normalFontWithSize(12)
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        destView.addSubview(lblDest)
        
        destTextField.placeholder = "Enter your location"
        self.destTextField.leftView = leftV1
        self.destTextField.leftViewMode = UITextFieldViewMode.always
        self.destTextField.clearButtonMode = UITextFieldViewMode.whileEditing
        destTextField.translatesAutoresizingMaskIntoConstraints = false
        destTextField.backgroundColor = Colors.WHITE_COLOR
        
        destView.addSubview(destTextField)
        destView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(15)][txt(35)]|", options: [], metrics: nil, views: ["lbl":lblDest, "txt":destTextField]))
        destView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]|", options: [], metrics: nil, views: ["lbl":lblDest, "txt":destTextField]))
        destView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txt]|", options: [], metrics: nil, views: ["lbl":lblDest, "txt":destTextField]))
        
        sourceTextField.text = sourceLocation
        destTextField.text = destLocation
        
    }
    
    func getCurrentLocation () {
        // Ask for Authorisation from the User.
        //self.locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .denied {
                AlertController.showAlertForMessage("You have denied access to your location, please enable it in settings.")
                return
            }
        }
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else {
            print("location service not enabled")
        }
    }
    
    
    // MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationCoord = manager.location!.coordinate
        self.moveMapToCoord(self.locationCoord)
        reverseGeocodeCoordinate(self.locationCoord)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
        } else if status == .denied {
            AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
        }
    }
    
    //MARK:- Text Field Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTextField = textField
        if textField == sourceTextField {
            let lat = Double(sourceLat!)!
            let lng = Double(sourceLong!)!
            if(triggerFrom == "joinRide" || poolTaxiBooking == "1") {
                self.locationButton.setTitle("Save Pick Up Location", for: UIControlState())
            } else {
                self.locationButton.setTitle("Save Start Point", for: UIControlState())
            }
            mapSourcePinImageVIew.isHidden = false
            self.moveMapToCoord(CLLocationCoordinate2DMake(lat, lng))
        }
        else if(textField == destTextField){
            let lat = Double(destLat!)!
            let lng = Double(destLong!)!
            if(triggerFrom == "joinRide" || poolTaxiBooking == "1") {
                self.locationButton.setTitle("Save Drop Location", for: UIControlState())
            } else {
                self.locationButton.setTitle("Save End Point", for: UIControlState())
            }
            mapDestPinImageVIew.isHidden = false
            self.moveMapToCoord(CLLocationCoordinate2DMake(lat, lng))
        }
        return false
    }
    
    func selectLocationButtonTapped (_ sender: AnyObject!) {
        if(poolTaxiBooking == "1") {
            if((sender as! UIButton).titleLabel?.text == "Save Pick Up Location"){
                mapSourcePinImageVIew.translatesAutoresizingMaskIntoConstraints = true
                selectedTextField = nil
                (sender as! UIButton).setTitle("SEND REQUEST", for: UIControlState())
            }
            if((sender as! UIButton).titleLabel?.text == "Save Drop Location"){
                mapDestPinImageVIew.translatesAutoresizingMaskIntoConstraints = true
                selectedTextField = nil
                (sender as! UIButton).setTitle("SEND REQUEST", for: UIControlState())
            }
            if((sender as! UIButton).titleLabel?.text == "SEND REQUEST") {
                goBack()
                onConfirmLocationsBlock?(sourceLocation as AnyObject?, sourceLat as AnyObject?, sourceLong as AnyObject?, destLocation as AnyObject?, destLat as AnyObject?, destLong as AnyObject?)
            }
        } else {
            if((sender as! UIButton).titleLabel?.text == "Save Pick Up Location" || (sender as! UIButton).titleLabel?.text == "Save Start Point"){
                mapSourcePinImageVIew.translatesAutoresizingMaskIntoConstraints = true
                destTextField.becomeFirstResponder()
            } else if((sender as! UIButton).titleLabel?.text == "Save Drop Location" || (sender as! UIButton).titleLabel?.text == "Save End Point"){
                mapDestPinImageVIew.translatesAutoresizingMaskIntoConstraints = true
                selectedTextField = nil
                if(triggerFrom == "joinRide") {
                    (sender as! UIButton).setTitle("SEND REQUEST", for: UIControlState())
                } else {
                    (sender as! UIButton).setTitle("SUBMIT", for: UIControlState())
                }
            } else if((sender as! UIButton).titleLabel?.text == "SEND REQUEST" || (sender as! UIButton).titleLabel?.text == "SUBMIT") {
                if(triggerFrom == "joinRide") {
                    goBack()
                    onConfirmLocationsBlock?(sourceLocation as AnyObject?, sourceLat as AnyObject?, sourceLong as AnyObject?, destLocation as AnyObject?, destLat as AnyObject?, destLong as AnyObject?)
                } else {
                    showIndicator("Processing...")
                    RideRequestor().getRideDetails(poolId, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let vc = OfferVehicleViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.rideDetailData = object as? RideDetail
                            vc.isExistingRide = true
                            vc.pickUpLoc = self.sourceLocation!
                            vc.pickUpLat = self.sourceLat!
                            vc.pickUpLong = self.sourceLong!
                            vc.dropLoc = self.destLocation!
                            vc.dropLat = self.destLat!
                            vc.dropLong = self.destLong!
                            vc.isWP = self.isWP
                            vc.wpInsuranceStatus = self.wpInsuranceStatus
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                    
                }
            }
        }
    }
    
    func proceedWithSelectedLatLong (_ coord : CLLocationCoordinate2D) {
        goBack()
        onSelectionBlock?(["lat":"\(coord.latitude)", "long":"\(coord.longitude)", "location":sourceTextField.text!])
    }
    
    func getDataForCoord (_ coord : CLLocationCoordinate2D) {
    }
    
    func goToCurrentLocation (_ sender: UIButton) {
        if let location = mapView.myLocation {
            self.userMovedMap = true
            moveMapToCoord(location.coordinate)
        }
        
    }
    
    // MARK:- Google Map Delegates
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        /*self.locationCoord = coordinate
         self.moveMapToCoord(self.locationCoord)
         reverseGeocodeCoordinate(self.locationCoord)*/
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.userMovedMap = self.userMovedMap ? self.userMovedMap : gesture
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if self.userMovedMap == true {
            self.userMovedMap = false
            self.locationCoord = position.target
            self.moveMapToCoord(self.locationCoord)
            reverseGeocodeCoordinate(self.locationCoord)
        }
    }
    
    func moveMapToCoord(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        self.locationCoord = coordinate
        let camera = self.mapView.camera
        let cameraPosition = GMSCameraPosition.camera(withTarget: coord, zoom: camera.zoom)
        self.mapView.animate(to: cameraPosition)
    }
    
    // MARK:- Reverse Geocode
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        
        let geocoder = GMSGeocoder()
        selectedTextField?.lock()
        
        geocoder.reverseGeocodeCoordinate(coord) { [weak self] response , error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]!
                self?.selectedTextField?.text = lines?.joined(separator: "\n")
                self?.selectedTextField?.unlock()
                
                if(self?.selectedTextField == self?.sourceTextField){
                    self?.sourceLat = "\((self?.locationCoord?.latitude)!)"
                    self?.sourceLong = "\((self?.locationCoord?.longitude)!)"
                    self?.sourceLocation = self?.selectedTextField?.text
                }
                else if(self?.selectedTextField == self?.destTextField){
                    self?.destLat = "\((self?.locationCoord?.latitude)!)"
                    self?.destLong = "\((self?.locationCoord?.longitude)!)"
                    self?.destLocation = self?.selectedTextField?.text
                }
                
            }
        }
    }
    
    var selectedRoute: Dictionary<NSObject, AnyObject>!
    var overviewPolyline: Dictionary<NSObject, AnyObject>!
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var routePolyline: GMSPolyline!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    
    func getDirections()
    {
        var url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceLocation!)&destination=\(destLocation!)&mode=driving"
        //        url = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        DispatchQueue.main.async(execute: { () -> Void in
            let directionsData = try? Data(contentsOf: URL(string: url)!)
            
            do{
                if(directionsData != nil) {
                    let dictionary: Dictionary<NSObject, AnyObject> = try JSONSerialization.jsonObject(with: directionsData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<NSObject, AnyObject>
                    
                    let status = dictionary["status" as NSObject] as! String
                    
                    if status == "OK" {
                        self.selectedRoute = (dictionary["routes" as NSObject] as! Array<Dictionary<NSObject, AnyObject>>)[0]
                        self.overviewPolyline = self.selectedRoute["overview_polyline" as NSObject] as! Dictionary<NSObject, AnyObject>
                        
                        let legs = self.selectedRoute["legs" as NSObject] as! Array<Dictionary<NSObject, AnyObject>>
                        
                        let startLocationDictionary = legs[0]["start_location" as NSObject] as! Dictionary<NSObject, AnyObject>
                        self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat" as NSObject] as! Double, startLocationDictionary["lng" as NSObject] as! Double)
                        
                        let endLocationDictionary = legs[legs.count - 1]["end_location" as NSObject] as! Dictionary<NSObject, AnyObject>
                        self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat" as NSObject] as! Double, endLocationDictionary["lng" as NSObject] as! Double)
                    }
                    
                    //self.mapView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
                    let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
                    let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
                    let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
                    let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
                    self.mapView.camera = camera

                    self.originMarker = GMSMarker(position: self.originCoordinate)
                    self.originMarker.map = self.mapView
                    if self.rideType == "O" {
                        self.originMarker.icon = UIImage(named:"marker_ride_orange")
                    } else {
                        self.originMarker.icon = UIImage(named:"marker_profile_orange")
                    }
                    self.originMarker.title = self.sourceLocation
                    
                    self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
                    self.destinationMarker.map = self.mapView
                    if self.rideType == "O" {
                        self.destinationMarker.icon = UIImage(named:"marker_ride_green")
                    } else {
                        self.destinationMarker.icon = UIImage(named:"marker_profile_green")
                    }
                    self.destinationMarker.title = self.destLocation
                    
                    let route = self.overviewPolyline["points" as NSObject] as! String
                    
                    let path: GMSPath = GMSPath(fromEncodedPath: route)!
                    self.routePolyline = GMSPolyline(path: path)
                    self.routePolyline.strokeColor = Colors.GREEN_COLOR
                    self.routePolyline.strokeWidth = 5.0
                    self.routePolyline.map = self.mapView
                }
            }
            catch{
                
            }
            hideIndicator()
        })
    }
    
    
}
