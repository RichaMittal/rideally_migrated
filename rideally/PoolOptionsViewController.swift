//
//  PoolOptionsViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/17/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class PoolOptionsViewController: BaseScrollViewController {
    
    var  loaderView : UIView!
    var activityView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createPoolOptionsView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavBar()
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
    }
    
    func getHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        if AppDelegate.isIPhone5() {
            labelObject.font = boldFontWithSize(16)
        } else {
            labelObject.font = boldFontWithSize(18)
        }
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.BLACK_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getSubHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        if AppDelegate.isIPhone5() {
            labelObject.font = boldFontWithSize(14)
        } else {
            labelObject.font = boldFontWithSize(17)
        }
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GRAY_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getORLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = boldFontWithSize(20)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GREEN_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(22)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    func createPoolOptionsView()->Void
    {
        let  lblHeading  =  getHeadingLabelSubViews("WHAT WOULD YOU LIKE TO DO?")
        let  lblSubHeading  =  getSubHeadingLabelSubViews("You can change your preferences later")
        let  btnPool =  getButtonSubViews("Wish to Carpool or Bikepool")
        btnPool.tag = 100
        let  lblPool  =  getSubHeadingLabelSubViews("You may take ride or give ride on personal vehicles")
        lblPool.font = normalFontWithSize(15)
        let  lblOr        =  getORLabelSubViews("OR")
        let  btnTaxi =  getButtonSubViews("Wish to book Airport Cab")
        btnTaxi.tag = 101
        let  lblTaxi  =  getSubHeadingLabelSubViews("Get cabs at Bengaluru only")
        lblTaxi.font = normalFontWithSize(15)
        
        let  poolOptionView = UIView()
        self.view.addSubview(poolOptionView)
        
        poolOptionView.addSubview(lblHeading)
        poolOptionView.addSubview(lblSubHeading)
        poolOptionView.addSubview(btnPool)
        poolOptionView.addSubview(lblPool)
        poolOptionView.addSubview(lblOr)
        poolOptionView.addSubview(btnTaxi)
        poolOptionView.addSubview(lblTaxi)
        
        poolOptionView.translatesAutoresizingMaskIntoConstraints = false
        lblHeading.translatesAutoresizingMaskIntoConstraints = false
        lblSubHeading.translatesAutoresizingMaskIntoConstraints = false
        btnPool.translatesAutoresizingMaskIntoConstraints = false
        lblPool.translatesAutoresizingMaskIntoConstraints = false
        lblOr.translatesAutoresizingMaskIntoConstraints = false
        btnTaxi.translatesAutoresizingMaskIntoConstraints = false
        lblTaxi.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[loginOptView(350)]", options:[], metrics:nil, views: ["loginOptView": poolOptionView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[loginOptView]|", options:[], metrics:nil, views: ["loginOptView": poolOptionView]))
        
        self.view.addConstraint(NSLayoutConstraint(item: poolOptionView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        
        let subViewsDict = ["lblHeading":lblHeading,"lblSubHeading":lblSubHeading,"orValue":lblOr,"btnPool":btnPool, "lblPool":lblPool,"btnTaxi":btnTaxi, "lblTaxi":lblTaxi]
        poolOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[lblHeading(20)]-3-[lblSubHeading(20)]-20-[btnPool(45)]-3-[lblPool(20)]-25-[orValue(20)]-25-[btnTaxi(45)]-3-[lblTaxi(20)]", options:[], metrics:nil, views: subViewsDict))
        poolOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[btnPool]-15-|", options:[], metrics:nil, views: subViewsDict))
        poolOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[btnTaxi]-15-|", options:[], metrics:nil, views: subViewsDict))
        poolOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblHeading]-15-|", options:[], metrics:nil, views: subViewsDict))
        poolOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblSubHeading]-15-|", options:[], metrics:nil, views: subViewsDict))
        //poolOptionView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-15-[lblPool]-15-|", options:[], metrics:nil, views: subViewsDict))
        poolOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblTaxi]-15-|", options:[], metrics:nil, views: subViewsDict))
        
        poolOptionView.addConstraint(NSLayoutConstraint(item: lblHeading, attribute: .centerX, relatedBy: .equal, toItem: poolOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        poolOptionView.addConstraint(NSLayoutConstraint(item: lblSubHeading, attribute: .centerX, relatedBy: .equal, toItem: poolOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        poolOptionView.addConstraint(NSLayoutConstraint(item: btnPool, attribute: .centerX, relatedBy: .equal, toItem: poolOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        poolOptionView.addConstraint(NSLayoutConstraint(item: lblPool, attribute: .centerX, relatedBy: .equal, toItem: poolOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        poolOptionView.addConstraint(NSLayoutConstraint(item: lblOr, attribute: .centerX, relatedBy: .equal, toItem: poolOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        poolOptionView.addConstraint(NSLayoutConstraint(item: btnTaxi, attribute: .centerX, relatedBy: .equal, toItem: poolOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        poolOptionView.addConstraint(NSLayoutConstraint(item: lblTaxi, attribute: .centerX, relatedBy: .equal, toItem: poolOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
    }
    
    func buttonPressed (_ sender: AnyObject)
    {
        if   sender.tag == 100
        {
            UserInfo.sharedInstance.isRideOptionsIn = true
            let vc = RideOptionsViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Welcome Page",
                AnalyticsParameterItemName: "Before Login Home",
                AnalyticsParameterContentType:"Sign Up Button"
                ])
        }
        
        if sender.tag == 101
        {
            let vc = AirportViewController()
            vc.signupFlow = true
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            //let ins = UIApplication.sharedApplication().delegate as! AppDelegate
            //ins.gotoHome(2)
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Welcome Page",
                AnalyticsParameterItemName: "Before Login Home",
                AnalyticsParameterContentType:"Login Button"
                ])
        }
    }
}
