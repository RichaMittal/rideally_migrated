//
//  UserDetailViewController.swift
//  rideally
//
//  Created by Raghunathan on 8/30/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import AWSS3
import AWSCore
import MobileCoreServices
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class UserDetailViewController: BaseScrollViewController,UIImagePickerControllerDelegate {
    
    var birthdayValue: String?
    var  selectedTxtField: UITextField?
    var  imageView = UIImageView()
    let  infoIcon = UIImageView()
    let  birthDayIcon = UITextField()
    var  birthdayPickerView = UIDatePicker()
    let  headerValueLabel = UILabel()
    var  firstNametxtBox = UITextField()
    var  lastNametxtBox = UITextField()
    let  emailLabelValue =  UILabel()
    let  mobileNumberLabelValue = UILabel()
    
    var birthdayLabelValue = UITextField() //UILabel()
    
    let genderLabelValue =  UILabel()
    let ismobileNumberShared = UIButton()
    
    let uploadRequest = AWSS3TransferManagerUploadRequest()
    var uploadCompletionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Personal Details"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title:"Edit" , style:.plain, target: self, action:#selector((saveButtonClicked(_:))))
        
        viewsArray.append(createTitleView())
        viewsArray.append(createEmailBGView())
        
        if  UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "4"
        {
            viewsArray.append(createVerifyButtonSub())
        }
        
        viewsArray.append(createMobileNumberSubView())
        viewsArray.append(createBirthdaySubview())
        viewsArray.append(createGenderSubView())
        addBottomView(createBottomSubView())
    }
    
    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.BLACK_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.left
        return labelObject
    }
    
    
    func createTitleView() -> SubView {
        
        let  headerView = UIView()
        
        //imageView.image = UIImage(named: "ProfileImage_S")
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(UserDetailViewController.profileImageTapped))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        let uploadImgBtn = UIButton()
        uploadImgBtn.setImage(UIImage(named:"upload_pic"), for: UIControlState())
        uploadImgBtn.translatesAutoresizingMaskIntoConstraints = false
        uploadImgBtn.backgroundColor = Colors.GREEN_COLOR
        uploadImgBtn.layer.cornerRadius = 15
        uploadImgBtn.contentMode = .center
        uploadImgBtn.addTarget(self, action: #selector(profileImageTapped), for: .touchDown)
        imageView.addSubview(uploadImgBtn)
        
        imageView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[img(30)]-10-|", options: [], metrics: nil, views: ["img":uploadImgBtn]))
        imageView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[img(30)]-20-|", options: [], metrics: nil, views: ["img":uploadImgBtn]))
        headerView.addSubview(imageView)
        
        if(UserInfo.sharedInstance.profilepicStatus == "1"){
            if UserInfo.sharedInstance.profilepicUrl != ""{
                imageView.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(UserInfo.sharedInstance.profilepicUrl)"))
            }
            else{
                if UserInfo.sharedInstance.faceBookid != ""{
                    imageView.kf.setImage(with: URL(string: "https://graph.facebook.com/\(UserInfo.sharedInstance.faceBookid)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            imageView.image = UIImage(named: "ProfileImage_S")
        }
        
        headerValueLabel.text = UserInfo.sharedInstance.fullName
        headerValueLabel.font = boldFontWithSize(20)
        headerValueLabel.textColor = Colors.BLACK_COLOR
        headerValueLabel.textAlignment = NSTextAlignment.center
        headerValueLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(headerValueLabel)
        
        firstNametxtBox.isHidden = true
        firstNametxtBox.delegate=self
        firstNametxtBox.text = UserInfo.sharedInstance.firstName
        firstNametxtBox.returnKeyType=UIReturnKeyType.next
        firstNametxtBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        firstNametxtBox.translatesAutoresizingMaskIntoConstraints = false
        firstNametxtBox.font = boldFontWithSize(14)
        firstNametxtBox.placeholder = "First Name"
        headerView.addSubview(firstNametxtBox)
        
        lastNametxtBox.isHidden = true
        lastNametxtBox.delegate=self
        lastNametxtBox.text = UserInfo.sharedInstance.lastName
        lastNametxtBox.returnKeyType=UIReturnKeyType.done
        lastNametxtBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        lastNametxtBox.translatesAutoresizingMaskIntoConstraints = false
        lastNametxtBox.font = boldFontWithSize(14)
        lastNametxtBox.placeholder = "Last Name"
        headerView.addSubview(lastNametxtBox)
        
        
        let sepLine = UIView()
        sepLine.translatesAutoresizingMaskIntoConstraints = false
        sepLine.backgroundColor = Colors.GRAY_COLOR
        headerView.addSubview(sepLine)
        
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[imgView(120)][headerLbl]|", options:[], metrics:nil, views: ["headerLbl": headerValueLabel,"imgView": imageView]))
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[headertxt(20)]-10-[headerLasttxt(==headertxt)]", options:[], metrics:nil, views: ["headertxt": firstNametxtBox,"headerLasttxt": lastNametxtBox]))
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-125-[headertxt]-10-|", options:[], metrics:nil, views: ["headertxt": firstNametxtBox]))
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-125-[headerLasttxt]-10-|", options:[], metrics:nil, views: ["headerLasttxt": lastNametxtBox]))
        
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[imgView]|", options:[], metrics:nil, views: ["headerLbl": headerValueLabel,"imgView": imageView]))
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[headerLbl]|", options:[], metrics:nil, views: ["headerLbl": headerValueLabel,"imgView": imageView]))
        
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[sep(0.5)]-0.5-|", options:[], metrics:nil, views: ["sep": sepLine]))
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[sep]|", options:[], metrics:nil, views: ["sep": sepLine]))
        
        
        let headerViewSubViewInstance = SubView()
        headerViewSubViewInstance.view = headerView
        headerViewSubViewInstance.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        headerViewSubViewInstance.heightConstraint = NSLayoutConstraint(item: headerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 150)
        
        return headerViewSubViewInstance
    }
    
    
    func createEmailBGView() -> SubView {
        
        let  emailView = UIView()
        
        let titleLabelValue = getLabelSubViews("EMAIL")
        titleLabelValue.textColor = UIColor.gray
        emailView.addSubview(titleLabelValue)
        
        emailLabelValue.text = UserInfo.sharedInstance.email
        emailLabelValue.font = normalFontWithSize(15)
        emailView.addSubview(emailLabelValue)
        
        let sepLine = UIView()
        sepLine.translatesAutoresizingMaskIntoConstraints = false
        sepLine.backgroundColor = Colors.GRAY_COLOR
        emailView.addSubview(sepLine)
        
        
        emailLabelValue.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        
        
        let subViewsDict = ["emailLabelVal":emailLabelValue,"titleLabelVal":titleLabelValue]
        
        emailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)]-5-[emailLabelVal]-5-|", options:[], metrics:nil, views: subViewsDict))
        
        emailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[titleLabelVal]|", options:[], metrics:nil, views: subViewsDict))
        emailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[emailLabelVal]|", options:[], metrics:nil, views: subViewsDict))
        
        emailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[sep(0.5)]-0.5-|", options:[], metrics:nil, views: ["sep": sepLine]))
        emailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[sep]|", options:[], metrics:nil, views: ["sep": sepLine]))
        
        
        let emailViewSubViewInstance = SubView()
        emailViewSubViewInstance.view = emailView
        emailViewSubViewInstance.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        emailViewSubViewInstance.heightConstraint = NSLayoutConstraint(item: emailView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 60)
        
        return emailViewSubViewInstance
    }
    
    
    func createVerifyButtonSub() ->  SubView
    {
        
        let verifyEmailView = UIView()
        
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(20)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.translatesAutoresizingMaskIntoConstraints = false
        buttonObject.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        buttonObject.setTitle("Verify Email", for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        verifyEmailView.addSubview(buttonObject)
        
        
        let sepLine = UIView()
        sepLine.translatesAutoresizingMaskIntoConstraints = false
        sepLine.backgroundColor = Colors.GRAY_COLOR
        verifyEmailView.addSubview(sepLine)
        
        verifyEmailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[btnObj]-10-|", options:[], metrics:nil, views: ["btnObj":buttonObject]))
        verifyEmailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[btnObj]-15-|", options:[], metrics:nil, views: ["btnObj":buttonObject]))
        
        
        verifyEmailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[sep(0.5)]-0.5-|", options:[], metrics:nil, views: ["sep": sepLine]))
        verifyEmailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[sep]|", options:[], metrics:nil, views: ["sep": sepLine]))
        
        
        let emailViewSubViewInstance = SubView()
        emailViewSubViewInstance.view = verifyEmailView
        emailViewSubViewInstance.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        emailViewSubViewInstance.heightConstraint = NSLayoutConstraint(item: verifyEmailView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 60)
        
        return emailViewSubViewInstance
    }
    
    
    func createMobileNumberSubView() ->  SubView{
        
        UserInfo.sharedInstance.shareMobileNumber = false
        
        let  mobileNumberView = UIView()
        let tapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(mobileNumberLabelGesturePressed(_:)))
        mobileNumberView.addGestureRecognizer(tapGesture)
        
        
        let titleLabelValue = getLabelSubViews("PHONE")
        titleLabelValue.textColor = UIColor.gray
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberView.addSubview(titleLabelValue)
        
        let phoneIcon = UIImageView()
        phoneIcon.image = UIImage(named:"VerifiedPhonenum")
        phoneIcon.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberView.addSubview(phoneIcon)
        
        
        let mobilenumberIndex = getLabelSubViews("+91")
        mobilenumberIndex.textColor = UIColor.black
        mobilenumberIndex.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberView.addSubview(mobilenumberIndex)
        
        
        mobileNumberLabelValue.text = UserInfo.sharedInstance.mobile
        mobileNumberLabelValue.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberLabelValue.font = normalFontWithSize(15)
        mobileNumberLabelValue.isUserInteractionEnabled = true
        mobileNumberView.addSubview(mobileNumberLabelValue)
        
        ismobileNumberShared.translatesAutoresizingMaskIntoConstraints = false
        ismobileNumberShared.addTarget(self, action: #selector(ismobileNumberSharedPressed(_:)), for: .touchUpInside)
        ismobileNumberShared.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        mobileNumberView.addSubview(ismobileNumberShared)
        
        let ismobilenumbersharedLabel = getLabelSubViews("Wish to share the mobile number with your fellow riders")
        ismobilenumbersharedLabel.textColor = Colors.GRAY_COLOR
        ismobilenumbersharedLabel.numberOfLines = 0
        ismobilenumbersharedLabel.lineBreakMode = .byWordWrapping
        ismobilenumbersharedLabel.font=normalFontWithSize(12)
        ismobilenumbersharedLabel.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberView.addSubview(ismobilenumbersharedLabel)
        
        
        infoIcon.image = UIImage(named:"Infoicon_user")
        infoIcon.tag = 100
        infoIcon.contentMode = UIViewContentMode.center
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(UserDetailViewController.infoImageTapped(_:)))
        infoIcon.isUserInteractionEnabled = true
        infoIcon.addGestureRecognizer(tapGestureRecognizer)
        infoIcon.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberView.addSubview(infoIcon)
        
        
        let sepLine = UIView()
        sepLine.translatesAutoresizingMaskIntoConstraints = false
        sepLine.backgroundColor = Colors.GRAY_COLOR
        mobileNumberView.addSubview(sepLine)
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[title]-5-[mobileIndex]-5-[ismobileNumberShared(20)]-10-|", options:[], metrics:nil, views: ["title":titleLabelValue,"mobilenum":mobileNumberLabelValue,"ismobileNumberShared":ismobileNumberShared,"mobileIndex":mobilenumberIndex]))
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[title(100)]", options:[], metrics:nil, views: ["title":titleLabelValue,"mobilenum":mobileNumberLabelValue,"ismobileNumberShared":ismobileNumberShared]))
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[mobileIndex(25)][mobilenum(100)]", options:[], metrics:nil, views: ["title":titleLabelValue,"mobilenum":mobileNumberLabelValue,"ismobileNumberShared":ismobileNumberShared,"mobileIndex":mobilenumberIndex]))
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[mobilenum(25)]-30-|", options:[], metrics:nil, views: ["mobilenum":mobileNumberLabelValue]))
        
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[ismobileNumberShared(20)]-5-[ismobilenumbersharedLabel]-25-|", options:[], metrics:nil, views: ["title":titleLabelValue,"mobilenum":mobileNumberLabelValue,"ismobileNumberShared":ismobileNumberShared,"ismobilenumbersharedLabel":ismobilenumbersharedLabel]))
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[ismobilenumbersharedLabel(25)]-5-|", options:[], metrics:nil, views: ["ismobilenumbersharedLabel":ismobilenumbersharedLabel]))
        
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[phicon(15)]-10-|", options:[], metrics:nil, views: ["phicon":phoneIcon]))
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[phicon(15)]", options:[], metrics:nil, views: ["phicon":phoneIcon]))
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[phicon(15)]-10-|", options:[], metrics:nil, views: ["phicon":infoIcon]))
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-50-[phicon(15)]", options:[], metrics:nil, views: ["phicon":infoIcon]))
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[sep(0.5)]-0.5-|", options:[], metrics:nil, views: ["sep": sepLine]))
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[sep]|", options:[], metrics:nil, views: ["sep": sepLine]))
        
        
        let mobileNumberViewSubViewInstance = SubView()
        mobileNumberViewSubViewInstance.view = mobileNumberView
        mobileNumberViewSubViewInstance.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        mobileNumberViewSubViewInstance.heightConstraint = NSLayoutConstraint(item: mobileNumberView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 80)
        
        return mobileNumberViewSubViewInstance
    }
    
    
    func createBirthdaySubview() -> SubView {
        
        let  birthdayView = UIView()
        
        let titleLabelValue = getLabelSubViews("Date of Birth")
        titleLabelValue.textColor = UIColor.gray
        birthdayView.addSubview(titleLabelValue)
        
        
        if UserInfo.sharedInstance.dob.characters.count > 0 && UserInfo.sharedInstance.dob != "0000-00-00"
        {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let dateObj = formatter.date(from: UserInfo.sharedInstance.dob)
            formatter.dateFormat = "dd-MMM-yyyy"
            self.birthdayLabelValue.text = (formatter.string(from: dateObj!))
            
            formatter.dateFormat = "yyyy-MM-dd"
            self.birthdayValue = (formatter.string(from: dateObj!))
        }
        
        birthdayLabelValue.font = normalFontWithSize(15)
        birthdayLabelValue.placeholder = "DD-MMM-YYYY"
        birthdayLabelValue.delegate=self
        birthdayView.addSubview(birthdayLabelValue)
        
        
        birthdayPickerView.addTarget(self, action:#selector(birthDayPickerValueChanged(_:)), for: UIControlEvents.valueChanged)
        birthdayPickerView.datePickerMode = .date
        birthdayPickerView.maximumDate = (Calendar.current as NSCalendar).date(byAdding: .year, value: -14, to: Date(), options: [])
        birthdayLabelValue.inputView = birthdayPickerView
        birthdayLabelValue.isUserInteractionEnabled = false
        
        birthDayIcon.backgroundColor = UIColor(patternImage: UIImage(named: "CalendarIcon")!)
        birthDayIcon.tintColor = UIColor.clear
        birthDayIcon.isUserInteractionEnabled = false
        birthDayIcon.delegate=self
        birthDayIcon.inputView = birthdayPickerView
        birthDayIcon.translatesAutoresizingMaskIntoConstraints = false
        birthdayView.addSubview(birthDayIcon)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        
        birthDayIcon.inputAccessoryView = keyboardDoneButtonShow
        birthdayLabelValue.inputAccessoryView = keyboardDoneButtonShow
        
        let sepLine = UIView()
        sepLine.translatesAutoresizingMaskIntoConstraints = false
        sepLine.backgroundColor = Colors.GRAY_COLOR
        birthdayView.addSubview(sepLine)
        
        
        birthdayLabelValue.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        
        let subViewsDict = ["birthdayLabelVal":birthdayLabelValue,"titleLabelVal":titleLabelValue] as [String : Any]
        
        birthdayView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)]-5-[birthdayLabelVal]-5-|", options:[], metrics:nil, views: subViewsDict))
        
        birthdayView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[titleLabelVal]|", options:[], metrics:nil, views: subViewsDict))
        birthdayView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[birthdayLabelVal]|", options:[], metrics:nil, views: subViewsDict))
        
        birthdayView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-25-[img(15)]", options:[], metrics:nil, views: ["img":birthDayIcon]))
        birthdayView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[img(15)]-10-|", options:[], metrics:nil, views: ["img":birthDayIcon]))
        
        
        
        birthdayView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[sep(0.5)]-0.5-|", options:[], metrics:nil, views: ["sep": sepLine]))
        birthdayView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[sep]|", options:[], metrics:nil, views: ["sep": sepLine]))
        
        
        let emailViewSubViewInstance = SubView()
        emailViewSubViewInstance.view = birthdayView
        emailViewSubViewInstance.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        emailViewSubViewInstance.heightConstraint = NSLayoutConstraint(item: birthdayView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 60)
        
        return emailViewSubViewInstance
    }
    
    
    func createGenderSubView() -> SubView {
        let  genderView = UIView()
        
        let titleLabelValue = getLabelSubViews("I AM")
        titleLabelValue.textColor = UIColor.gray
        genderView.addSubview(titleLabelValue)
        
        
        genderLabelValue.text = (UserInfo.sharedInstance.gender == "M" ? "Male" : "Female")
        genderLabelValue.font = normalFontWithSize(15)
        genderView.addSubview(genderLabelValue)
        
        
        let sepLine = UIView()
        sepLine.translatesAutoresizingMaskIntoConstraints = false
        sepLine.backgroundColor = Colors.GRAY_COLOR
        genderView.addSubview(sepLine)
        
        
        genderLabelValue.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        
        let subViewsDict = ["genderLabelVal":genderLabelValue,"titleLabelVal":titleLabelValue]
        
        genderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)]-5-[genderLabelVal]-5-|", options:[], metrics:nil, views: subViewsDict))
        
        genderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[titleLabelVal]|", options:[], metrics:nil, views: subViewsDict))
        genderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[genderLabelVal]|", options:[], metrics:nil, views: subViewsDict))
        
        genderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[sep(0.5)]-0.5-|", options:[], metrics:nil, views: ["sep": sepLine]))
        genderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[sep]|", options:[], metrics:nil, views: ["sep": sepLine]))
        
        
        let genderViewSubViewInstance = SubView()
        genderViewSubViewInstance.view = genderView
        genderViewSubViewInstance.padding = UIEdgeInsetsMake(0, 0, 5, 0)
        genderViewSubViewInstance.heightConstraint = NSLayoutConstraint(item: genderView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 60)
        
        return genderViewSubViewInstance
    }
    
    
    func createBottomSubView() -> SubView {
        
        let view = UIView()
        
        let btnLogout = UIButton()
        btnLogout.setTitle("LOGOUT", for: UIControlState())
        btnLogout.backgroundColor = Colors.GRAY_COLOR
        btnLogout.titleLabel?.textAlignment = NSTextAlignment.center
        btnLogout.translatesAutoresizingMaskIntoConstraints = false
        btnLogout.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnLogout.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btnLogout.addTarget(self, action: #selector(footerButtonPressEvent(_:)), for: .touchDown)
        view.addSubview(btnLogout)
        
        let btnChangePassword = UIButton()
        btnChangePassword.setTitle("CHANGE PASSWORD", for: UIControlState())
        btnChangePassword.backgroundColor = Colors.GRAY_COLOR
        btnChangePassword.titleLabel?.textAlignment = NSTextAlignment.center
        btnChangePassword.translatesAutoresizingMaskIntoConstraints = false
        btnChangePassword.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnChangePassword.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btnChangePassword.addTarget(self, action: #selector(footerButtonPressEvent(_:)), for: .touchDown)
        view.addSubview(btnChangePassword)
        
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnLog]-1-[btnCP(==btnLog)]|", options: [], metrics: nil, views: ["btnLog":btnLogout,"btnCP":btnChangePassword]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btnLog]|", options: [], metrics: nil, views: ["btnLog":btnLogout,"btnCP":btnChangePassword]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btnCP]|", options: [], metrics: nil, views: ["btnLog":btnLogout,"btnCP":btnChangePassword]))
        
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    
    func footerButtonPressEvent(_ sender: UIButton) -> Void {
        
        if  sender.titleLabel?.text == "CHANGE PASSWORD"
        {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Personal Detail Page",
                AnalyticsParameterContentType:"Change Password Button"
                ])
            self.navigationController?.pushViewController(ChangePasswordViewController(), animated: true)
        }
        else
        {
            AlertController.showAlertFor("Logout", message: "Are you sure you want to logout", okButtonTitle: "Logout", okAction: {
                let ins = UIApplication.shared.delegate as! AppDelegate
                UserInfo.sharedInstance.logoutUser()
                ins.changeRootViewControllerWithViewController(UINavigationController(rootViewController: LoginOptionViewController()))
                }, cancelButtonTitle: "Cancel", cancelAction: nil)
        }
    }
    
    func setBoolPropertyToObject(_ isVal:Bool) -> Void
    {
        firstNametxtBox.isHidden = !isVal
        lastNametxtBox.isHidden = !isVal
        headerValueLabel.isHidden = isVal
    }
    
    
    func mobileNumberLabelGesturePressed(_ sender:AnyObject) -> Void {
        AlertController.showToastForInfo("If you wish to change your mobile number, please contact our customer care at 080 4600 4600")
    }
    
    
    func containsOnlyLetters(_ input: String) -> Bool
    {
        for chr in input.characters
        {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    
    
    func calcAge(_ birthday:String?) -> Int
    {
        var age: Int = 0
        if let bday = birthday
        {
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "dd/MMM/yyyy"
            let birthdayDate = dateFormater.date(from: bday)
            let calendar: Calendar! = Calendar(identifier: Calendar.Identifier.gregorian)
            //let now: Date! = Date()
            let calcAge = calendar.dateComponents([.year], from: birthdayDate!)
            //let calcAge = calendar.dateComponents(.year, from: birthdayDate!, to: now, options: [])
            age = calcAge.year!
        }
        return age
    }
    
    func saveButtonClicked(_ sender:UIBarButtonItem) -> Void {
        
        var  isVal = false
        
        if  sender.title == "Edit"
        {
            self.birthDayIcon.isUserInteractionEnabled = true
            self.birthdayLabelValue.isUserInteractionEnabled = true
            
            sender.title = "Save"
            isVal = true
            setBoolPropertyToObject(isVal)
        }
        else if sender.title == "Save"
        {
            self.birthDayIcon.isUserInteractionEnabled = false
            self.birthdayLabelValue.isUserInteractionEnabled = false
            
            if let name = self.firstNametxtBox.text
            {
                
                if self.firstNametxtBox.text?.characters.count == 0
                {
                    AlertController.showToastForError("First Name' field cannot be left blank")
                }
                else if !containsOnlyLetters(name)
                {
                    AlertController.showToastForError("Please provide valid 'First Name'")
                }
                else if self.firstNametxtBox.text?.characters.count < 2 || self.firstNametxtBox.text?.characters.count > 40
                {
                    AlertController.showToastForError("Please provide 'First Name' between 2 and 40 characters")
                }
                else if self.firstNametxtBox.text?.characters.count < 2 || self.firstNametxtBox.text?.characters.count > 40
                {
                    AlertController.showToastForError("Please provide 'First Name' between 2 and 40 characters")
                }
                else if self.lastNametxtBox.text?.characters.count != 0 && !containsOnlyLetters(lastNametxtBox.text!)
                {
                    AlertController.showToastForError("Please provide valid 'Last Name'")
                }
                else if  self.lastNametxtBox.text?.characters.count != 0  && (self.lastNametxtBox.text?.characters.count < 2 || self.lastNametxtBox.text?.characters.count > 40)
                {
                    AlertController.showToastForError("Please provide 'Last Name' between 2 and 40 characters")
                }
                else if self.birthdayLabelValue.text == ""
                {
                    sender.title = "Edit"
                    setBoolPropertyToObject(isVal)
                    let reqObj = EditProfileRequest()
                    reqObj.userID = UserInfo.sharedInstance.userID
                    reqObj.userFirstName = self.firstNametxtBox.text
                    reqObj.userLastName = ""
                    
                    if let lastname = self.lastNametxtBox.text
                    {
                        reqObj.userLastName = lastname
                    }
                    
                    showLoadingIndicator("Loading...")
                    reqObj.userDOB = ""
                    reqObj.userEmail = self.emailLabelValue.text
                    reqObj.userGender = UserInfo.sharedInstance.gender
                    reqObj.userMobile = self.mobileNumberLabelValue.text
                    reqObj.userShareNumber = String(UserInfo.sharedInstance.shareMobileNumber)
                    reqObj.userCity = ""
                    reqObj.userAddress = UserInfo.sharedInstance.homeAddress
                    reqObj.userGovtIDType = ""
                    reqObj.userView_f = "f"
                    reqObj.userView_b = "b"
                    reqObj.userView_o = "office"
                    reqObj.userView_p = "profile"
                    
                    ProfileRequestor().sendEditUserProfileRequest(reqObj, success:{ (success, object) in
                        
                        self.hideLoadingIndiactor()
                        UserInfo.sharedInstance.fullName = self.firstNametxtBox.text! + " " + self.lastNametxtBox.text!
                        self.headerValueLabel.text = UserInfo.sharedInstance.fullName
                        
                        if (object as! EditProfileResponse).code == "415"
                        {
                            AlertController.showToastForInfo("Profile updated successfully.")
                        }
                        else if (object as! EditProfileResponse).code != "415"
                        {
                            AlertController.showToastForError( (object as! EditProfileResponse).message!)
                        }
                    }){ (error) in
                        self.hideLoadingIndiactor()
                        AlertController.showAlertForError(error)
                    }
                }
                else if self.birthdayLabelValue.text != ""
                {
                    if calcAge(self.birthdayLabelValue.text) < 13
                    {
                        AlertController.showToastForError("Sorry, your age must be 13 years or above to join RideAlly.")
                        self.birthDayIcon.isUserInteractionEnabled = true
                        self.birthdayLabelValue.isUserInteractionEnabled = true
                    }
                    else
                    {
                        sender.title = "Edit"
                        setBoolPropertyToObject(isVal)
                        let reqObj = EditProfileRequest()
                        reqObj.userID = UserInfo.sharedInstance.userID
                        reqObj.userFirstName = self.firstNametxtBox.text
                        reqObj.userLastName = ""
                        
                        if let lastname = self.lastNametxtBox.text
                        {
                            reqObj.userLastName = lastname
                        }
                        
                        
                        if let check = self.birthdayValue
                        {
                            showLoadingIndicator("Loading...")
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd"
                            let dateObj = formatter.date(from: check)
                            
                            if let dateObject = dateObj
                            {
                                reqObj.userDOB = (formatter.string(from: dateObject))
                            }
                            
                            reqObj.userEmail = self.emailLabelValue.text
                            reqObj.userGender = UserInfo.sharedInstance.gender
                            reqObj.userMobile = self.mobileNumberLabelValue.text
                            reqObj.userShareNumber = String(UserInfo.sharedInstance.shareMobileNumber)
                            reqObj.userCity = ""
                            reqObj.userAddress = UserInfo.sharedInstance.homeAddress
                            reqObj.userGovtIDType = ""
                            reqObj.userView_f = "f"
                            reqObj.userView_b = "b"
                            reqObj.userView_o = "office"
                            reqObj.userView_p = "profile"
                            
                            ProfileRequestor().sendEditUserProfileRequest(reqObj, success:{ (success, object) in
                                self.hideLoadingIndiactor()
                                
                                UserInfo.sharedInstance.fullName = self.firstNametxtBox.text! + " " + self.lastNametxtBox.text!
                                self.headerValueLabel.text = UserInfo.sharedInstance.fullName
                                
                                
                                if (object as! EditProfileResponse).code == "415"
                                {
                                    AlertController.showToastForInfo("Profile updated successfully.")
                                }
                                else if (object as! EditProfileResponse).code != "415"
                                {
                                    AlertController.showToastForError( (object as! EditProfileResponse).message!)
                                }
                            }){ (error) in
                                self.hideLoadingIndiactor()
                                AlertController.showAlertForError(error)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func birthDayPickerValueChanged(_ datePickerObject: UIDatePicker) -> Void
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        birthdayLabelValue.text = formatter.string(from: datePickerObject.date)
        
        formatter.dateFormat = "yyyy-MM-dd"
        self.birthdayValue = formatter.string(from: datePickerObject.date)
    }
    
    func infoImageTapped(_ imgObject: UITapGestureRecognizer) -> Void
    {
        
        if  imgObject.view?.tag == 100
        {
            AlertController.showAlertFor("Why Mobile Number?", message: "Mobile number is needed to notify you about your rides, ride members, groups etc. It will be shared with your fellow riders only when you join / offer any ride.")
        }
        else if imgObject.view?.tag == 101
        {
        }
    }
    
    
    func buttonPressed() -> Void
    {
        let reqObj = ResendEmailRequest()
        showLoadingIndicator("Loading...")
        reqObj.userID = UserInfo.sharedInstance.userID
        
        
        ProfileRequestor().sendVerifyEmailRequest(reqObj, success:{ (success, object) in
            self.hideLoadingIndiactor()
            
            if (object as! ResendEmailResponse).code == "1099"
            {
                AlertController.showToastForInfo("A verification link has been sent to your Email Inbox. Please do check SPAM folder as well. Please verify your Email ID.")
            }
            else
            {
                AlertController.showToastForError((object as! ResendEmailResponse).message!)
            }
            
        }){ (error) in
            self.hideLoadingIndiactor()
            AlertController.showAlertForError(error)
        }
    }
    
    
    func sendUpdateProfilrPicURL() -> Void
    {
        let reqObj = UpdateProfilePictureURLRequest()
        showLoadingIndicator("Loading...")
        reqObj.userID = UserInfo.sharedInstance.userID
        reqObj.profilePicURL = self.uploadRequest?.key
        
        ProfileRequestor().sendUpdateProfileImageURLRequest(reqObj, success:{ (success, object) in
            
            self.hideLoadingIndiactor()
            UserInfo.sharedInstance.isProfilePicUpdatedSuccesfully = false
            
            if (object as! UpdateProfilePictureURLResponse).code == "431"
            {
                /*let localStorePath = "\(UserInfo.sharedInstance.userID)".stringByAppendingString("_image.png")
                 let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
                 var  imagePath = documentsURL.absoluteString
                 imagePath = imagePath.stringByReplacingOccurrencesOfString("/Documents/", withString:"/tmp/\(localStorePath)")
                 imagePath = imagePath.stringByReplacingOccurrencesOfString("file:///", withString:"/")*/
                
                var updatedImagePath = (self.uploadRequest?.body)?.absoluteString
                updatedImagePath = updatedImagePath?.replacingOccurrences(of: "file:///", with:"/")
                let image = UIImage(contentsOfFile:updatedImagePath!)
                
                if image == nil
                {
                    self.imageView.image = UIImage(named: "ProfileImage_S")
                }
                else
                {
                    //UserInfo.sharedInstance.isProfilePicUpdatedSuccesfully = true
                    self.imageView.image = image
                }
                UserInfo.sharedInstance.profilepicStatus = "1"
                UserInfo.sharedInstance.profilepicUrl = (self.uploadRequest?.key!)!
                
                AlertController.showToastForInfo("Your profile pic has been uploaded successfully.")
            }
            else
            {
                AlertController.showToastForError((object as! UpdateProfilePictureURLResponse).message!)
            }
            
        }){ (error) in
            self.hideLoadingIndiactor()
            AlertController.showAlertForError(error)
        }
    }
    
    
    func profileImageTapped() -> Void {
        
        //let vc = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.mediaTypes = [kUTTypeImage as String]
        
//        let capturePhoto = UIAlertAction(title: "Capture Photo", style: .Default, handler: { (action) in
//            imgPicker.sourceType = .Camera
//            self.presentViewController(imgPicker, animated: true, completion: nil)
//        })
//        vc.addAction(capturePhoto)
//        
//        let photoLibrary = UIAlertAction(title: "Photo Library", style: .Default, handler: { (action) in
            imgPicker.sourceType = .photoLibrary
            self.present(imgPicker, animated: true, completion: nil)
//        })
//        vc.addAction(photoLibrary)
//        
//        let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) in
//            
//        })
//        vc.addAction(actionCancel)
        
        //presentViewController(vc, animated: true, completion: nil)
    }
    
    func presentImagePickerView() -> Void {
        let imgPicker = UIImagePickerController()
        imgPicker.delegate = self
        imgPicker.sourceType = .photoLibrary
        imgPicker.mediaTypes = [kUTTypeImage as String]
        self.present(imgPicker, animated: true, completion: nil)
    }
    
    func donePressed() -> Void {
        if(selectedTxtField == birthdayLabelValue){
            selectedTxtField?.text = prettyDateStringFromDate(birthdayPickerView.date, toFormat: "dd-MMM-yyyy")
            self.birthdayValue = prettyDateStringFromDate(birthdayPickerView.date, toFormat: "yyyy-MM-dd")
        }
        selectedTxtField!.resignFirstResponder()
    }
    
    func ismobileNumberSharedPressed(_ sender:UIButton) -> Void
    {
        
        sender.isSelected = !sender.isSelected
        
        ismobileNumberShared.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        UserInfo.sharedInstance.shareMobileNumber = false
        
        if  sender.isSelected == true
        {
            ismobileNumberShared.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
            UserInfo.sharedInstance.shareMobileNumber = true
        }
    }
    
    //Uitextfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField === firstNametxtBox)
        {
            lastNametxtBox.becomeFirstResponder()
        }
        else if (textField === lastNametxtBox)
        {
            lastNametxtBox.resignFirstResponder()
        }
        
        return true
    }
    
    
    func imageType(_ imgData : Data) -> String
    {
        var c = [UInt8](repeating: 0, count: 1)
        (imgData as NSData).getBytes(&c, length: 1)
        
        let ext : String
        
        switch (c[0]) {
        case 0xFF:
            
            ext = "jpg"
            
        case 0x89:
            
            ext = "png"
        case 0x47:
            
            ext = "gif"
        case 0x49, 0x4D :
            ext = "tiff"
        default:
            ext = ""
        }
        
        return ext
    }
    
    
    //Uiimage Picker view delegate
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let tempImage:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        //self.imageView.image = tempImage
        
        
        let assertvalue:URL = info[UIImagePickerControllerReferenceURL] as! URL
        let fileExtension:CFString =  assertvalue.pathExtension as CFString
        let unmanagedFileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, nil)
        let fileUTI = unmanagedFileUTI!.takeRetainedValue()
        
        
        var extValue = ""
        
        if (UTTypeConformsTo(fileUTI, kUTTypeJPEG))
        {
            extValue = "jpeg"
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypePNG))
        {
            extValue = "png"
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypeTIFF))
        {
            extValue = "tiff"
        }
        
        
        
        let bucketName = S3BUCKETNAME
        let imagePath = "profilepictures/raw_"
        let imageName = "_profile.\(extValue)"
        
        
        UserInfo.sharedInstance.profilepicfileFormat = extValue
        let localStorePath = "\(UserInfo.sharedInstance.userID)" + "_image.\(extValue)"
        
        // create a local image that we can use to upload to s3
        let path = NSTemporaryDirectory() + localStorePath
        
        if extValue == "jpeg" {
            let imageData = UIImageJPEGRepresentation(tempImage, 5.0)
            try? imageData?.write(to: URL(fileURLWithPath: path), options: [.atomic])
        }
        else
        {
            let imageData = UIImagePNGRepresentation(tempImage)
            try? imageData?.write(to: URL(fileURLWithPath: path), options: [.atomic])
        }
        
        // once the image is saved we can use the path to create a local fileurl
        let  urlPath = URL.init(fileURLWithPath: path)
        // next we set up the S3 upload request manager
        self.uploadRequest?.bucket = bucketName
        // I want this image to be public to anyone to view it so I'm setting it to Public Read
        self.uploadRequest?.acl = AWSS3ObjectCannedACL.publicRead
        // set the image's name that will be used on the s3 server. I am also creating a folder to place the image in
        self.uploadRequest?.key = imagePath+UserInfo.sharedInstance.userID+imageName
        // set the content type
        self.uploadRequest?.contentType = "image/\(extValue)"
        self.uploadRequest?.body = urlPath
        
        //print("path before upload",path,(self.uploadRequest.body).absoluteString)
        
        let express = AWSS3TransferUtilityUploadExpression()
        express.progressBlock = {(task: AWSS3TransferUtilityTask, progress:Progress) in
            DispatchQueue.main.async {
                
            }
//            DispatchQueue.main.asynchronously(execute: {
//                self.showLoadingIndicator("Uploading...")
//            })
        }
        
        self.uploadCompletionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                self.hideLoadingIndiactor()
                
                if ((error) != nil)
                {
                    AlertController.showToastForError("Sorry, there is some technical error, please try later")
                }
                else
                {
                    self.sendUpdateProfilrPicURL()
                }
            })
        }
        
        
        let transferUtility = AWSS3TransferUtility.default()
        
        transferUtility.uploadFile((self.uploadRequest?.body)!, bucket: (self.uploadRequest?.bucket!)!, key: (self.uploadRequest?.key!)!, contentType:(self.uploadRequest?.contentType!)!, expression: express, completionHandler: self.uploadCompletionHandler).continueWith { (task) -> AnyObject! in
            
            if let error = task.error
            {
                if error._domain == AWSS3TransferManagerErrorDomain as String
                {
                    if let errorCode = AWSS3TransferManagerErrorType(rawValue: error._code)
                    {
                        switch (errorCode)
                        {
                        case .cancelled, .paused:
                            DispatchQueue.main.async {
                                
                            }
//                            DispatchQueue.main.asynchronously(execute: { () -> Void in
//                                
//                            })
                            break;
                        default:
                            AlertController.showToastForError("Sorry, there is some technical error, please try later.")
                            break;
                        }
                    }
                    else
                    {   AlertController.showToastForError("Sorry, there is some technical error, please try later.")
                    }
                }
                else
                {
                    AlertController.showToastForError("Sorry, there is some technical error, please try later.")
                }
            }
//            if let exception = task.description as [AnyObject]
//            {
//                AlertController.showToastForError(exception.description)
//            }
//            if let _ = task.result
//            {
//                print("Upload Starting!")
//            }
            
            return nil;
        }
        
        
        self.dismiss(animated: true, completion: nil)
    }
}
