//
//  GroupOptionsViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/17/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class GroupOptionsViewController: BaseScrollViewController {
    
    var  loaderView : UIView!
    var activityView: UIActivityIndicatorView!
    var btnTag = ""
    var vehicleDefaultDetails: VehicleDefaultResponse!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createGroupOptionsView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavBar()
    }
    
    override func goBack() {
        let vc = RideOptionsViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        if AppDelegate.isIPhone5() {
            labelObject.font = boldFontWithSize(16)
        } else {
            labelObject.font = boldFontWithSize(18)
        }
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.BLACK_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        labelObject.lineBreakMode = NSLineBreakMode.byWordWrapping
        return labelObject
    }
    
    func getSubHeadingLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        if AppDelegate.isIPhone5() {
            labelObject.font = boldFontWithSize(14)
        } else {
            labelObject.font = boldFontWithSize(17)
        }
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GRAY_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        labelObject.lineBreakMode = NSLineBreakMode.byWordWrapping
        labelObject.numberOfLines = 2
        return labelObject
    }
    
    func getORLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = boldFontWithSize(20)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.GREEN_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(22)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    func createGroupOptionsView()->Void
    {
        let  lblHeading  =  getHeadingLabelSubViews("TRAVEL WITH COLLEAGUES OR ANYONE")
        //let  lblSubHeading  =  getSubHeadingLabelSubViews("Knowing you will help RideAlly to find best\n commute partners for you to Ride with")
        let  btnWp =  getButtonSubViews("Workplace")
        btnWp.tag = 104
        let  lblWp  =  getSubHeadingLabelSubViews("Join this to carpool or bikepool with colleagues only from/to your workplace")
        lblWp.font = normalFontWithSize(15)
        let  lblOr        =  getORLabelSubViews("OR")
        let  btnAnywhere =  getButtonSubViews("Anywhere")
        btnAnywhere.tag = 105
        let  lblAnywhere  =  getSubHeadingLabelSubViews("Join this to carpool or bikepool with all trusted persons from anywhere to anywhere")
        lblAnywhere.font = normalFontWithSize(15)
        
        let  groupOptionView = UIView()
        self.view.addSubview(groupOptionView)
        
        groupOptionView.addSubview(lblHeading)
        //groupOptionView.addSubview(lblSubHeading)
        groupOptionView.addSubview(btnWp)
        groupOptionView.addSubview(lblWp)
        groupOptionView.addSubview(lblOr)
        groupOptionView.addSubview(btnAnywhere)
        groupOptionView.addSubview(lblAnywhere)
        
        groupOptionView.translatesAutoresizingMaskIntoConstraints = false
        lblHeading.translatesAutoresizingMaskIntoConstraints = false
        //lblSubHeading.translatesAutoresizingMaskIntoConstraints = false
        btnWp.translatesAutoresizingMaskIntoConstraints = false
        lblWp.translatesAutoresizingMaskIntoConstraints = false
        lblOr.translatesAutoresizingMaskIntoConstraints = false
        btnAnywhere.translatesAutoresizingMaskIntoConstraints = false
        lblAnywhere.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[loginOptView(350)]", options:[], metrics:nil, views: ["loginOptView": groupOptionView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[loginOptView]|", options:[], metrics:nil, views: ["loginOptView": groupOptionView]))
        
        self.view.addConstraint(NSLayoutConstraint(item: groupOptionView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        
        let subViewsDict = ["lblHeading":lblHeading,"orValue":lblOr,"btnWp":btnWp, "lblWp":lblWp, "btnAnywhere":btnAnywhere, "lblAnyhere":lblAnywhere]
        groupOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[lblHeading(20)]-20-[btnWp(45)]-3-[lblWp]-25-[orValue(20)]-25-[btnAnywhere(45)]-3-[lblAnyhere]", options:[], metrics:nil, views: subViewsDict))
        groupOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[btnWp]-15-|", options:[], metrics:nil, views: subViewsDict))
        groupOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[btnAnywhere]-15-|", options:[], metrics:nil, views: subViewsDict))
        groupOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblHeading]-15-|", options:[], metrics:nil, views: subViewsDict))
        //groupOptionView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-15-[lblSubHeading]-15-|", options:[], metrics:nil, views: subViewsDict))
        groupOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblWp]-15-|", options:[], metrics:nil, views: subViewsDict))
        groupOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[lblAnyhere]-15-|", options:[], metrics:nil, views: subViewsDict))
        
        groupOptionView.addConstraint(NSLayoutConstraint(item: lblHeading, attribute: .centerX, relatedBy: .equal, toItem: groupOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        // groupOptionView.addConstraint(NSLayoutConstraint(item: lblSubHeading, attribute: .CenterX, relatedBy: .Equal, toItem: groupOptionView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        groupOptionView.addConstraint(NSLayoutConstraint(item: btnWp, attribute: .centerX, relatedBy: .equal, toItem: groupOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        groupOptionView.addConstraint(NSLayoutConstraint(item: lblWp, attribute: .centerX, relatedBy: .equal, toItem: groupOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        groupOptionView.addConstraint(NSLayoutConstraint(item: lblOr, attribute: .centerX, relatedBy: .equal, toItem: groupOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        groupOptionView.addConstraint(NSLayoutConstraint(item: btnAnywhere, attribute: .centerX, relatedBy: .equal, toItem: groupOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        groupOptionView.addConstraint(NSLayoutConstraint(item: lblAnywhere, attribute: .centerX, relatedBy: .equal, toItem: groupOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
    }
    
    func buttonPressed (_ sender: AnyObject)
    {
        if   sender.tag == 104
        {
            let vc = WpSearchViewController()
            self.navigationController?.pushViewController(vc, animated: true)
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Welcome Page",
                AnalyticsParameterItemName: "After Login Home",
                AnalyticsParameterContentType:"Workplace Button"
                ])
        }
        
        if sender.tag == 105
        {
            getWebPageContent()
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Welcome Page",
                AnalyticsParameterItemName: "After Login Home",
                AnalyticsParameterContentType:"Anywhere Button"
                ])
        }
    }
    
    func getWebPageContent() -> Void
    {
        let reqObj = StaticPageRequest()
        showIndicator("Loading...")
        reqObj.page_key = "terms"
        
        StaticPageRequestor().sendStaticPageRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            if (object as! StaticPageResponse).code == "471"
            {
                if (object as! StaticPageResponse).dataObj?.first != nil
                {
                    if (object as! StaticPageResponse).dataObj?.first?.key != nil
                    {
                        let ss = ((object as! StaticPageResponse).dataObj?.first?.key)?.data(using: String.Encoding.unicode, allowLossyConversion: true)
                        let attrStr = try! NSAttributedString(data: ss!, options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                            documentAttributes: nil)
                        //                        let viewHolder = AlertContentViewHolder()
                        //                        viewHolder.heightConstraintValue = 95
                        //                        let view = UIView()
                        //                        view.translatesAutoresizingMaskIntoConstraints = false
                        //                        view.backgroundColor = Colors.WHITE_COLOR
                        //
                        //                        let lblInfo = UILabel()
                        //                        lblInfo.numberOfLines = 0
                        //                        lblInfo.lineBreakMode = .ByWordWrapping
                        //                        lblInfo.translatesAutoresizingMaskIntoConstraints = false
                        //                        lblInfo.font = normalFontWithSize(13)
                        //                        lblInfo.attributedText = attrStr
                        //                        view.addSubview(lblInfo)
                        //
                        //                        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-10-[lbl]|", options: [], metrics: nil, views: ["lbl":lblInfo]))
                        //                        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblInfo]))
                        //                        viewHolder.view = view
                        //AlertController.showAlertFor("Terms Of Use", message: "", contentView: viewHolder, okButtonTitle: "ACCEPT", willHaveAutoDismiss: true, okAction: {
                        if(UserInfo.sharedInstance.isTouAgreed) {
                            self.redirectScreen()
                        } else {
                            AlertController.showAlertFor("Terms Of Use", message: attrStr.string, okButtonTitle: "AGREE", okAction: {
                                UserInfo.sharedInstance.isTouAgreed = true
                                self.redirectScreen()
                                }, cancelButtonTitle: "DISAGREE", cancelAction: {
                                    UserInfo.sharedInstance.isTouAgreed = false
                            })
                        }
                    }
                }
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
    
    func redirectScreen() {
        if(self.btnTag == "102" || UserInfo.sharedInstance.isRideTakerClicked) {
            let vc = CreateRideMapViewController()
            vc.btnTag = self.btnTag
            vc.signupFlow = true
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = CreateRideMapViewController()
            vc.btnTag = self.btnTag
            vc.signupFlow = true
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
