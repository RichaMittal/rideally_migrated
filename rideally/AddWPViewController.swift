//
//  AddWPViewController.swift
//  rideally
//
//  Created by Sarav on 24/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class AddWPViewController: BaseScrollViewController {
    
    var onExistingWPsBlock: actionBlockWithParams?
    var wpName: String?
    var wpLoc: String?
    var wpLat: String?
    var wpLong: String?
    let txtWPname = UITextField()
    let lblLocation = UILabel()
    
    let lblWPURL = UILabel()
    let txtWPUID = UITextField()
    let txtEmail = UITextField()
    
    let btnPrivate = DLRadioButton()
    let btnPublic = DLRadioButton()
    
    let txtComments = UITextField()
    
    let reqObj = AddWPRequest()
    //let pickerView = UIPickerView()
//    let txtCategory = UITextField()
//    var pickerDataSource :Array = ["Workplace", "Residential"]
    var selectedTxtField: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Request Workplace"
        
        self.viewsArray.append(getView())
        self.addBottomView(getBottomView())
    }
    
    func getBottomView() -> SubView
    {
        let btn = UIButton()
        btn.setTitle("REQUEST WORKPLACE", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(proceed), for: .touchDown)
        
        let sub = SubView()
        sub.view = btn
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: btn, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    
    func getView() -> SubView
    {
        let view = UIView()
        
        /*let lblCategory = UILabel()
        lblCategory.text = "CATEGORY"
        lblCategory.font = boldFontWithSize(10)
        lblCategory.textColor = Colors.GRAY_COLOR
        lblCategory.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCategory)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        txtCategory.delegate=self
        txtCategory.translatesAutoresizingMaskIntoConstraints = false
        txtCategory.text = "Workplace"
        txtCategory.placeholder = "Select"
        txtCategory.inputView = pickerView
        txtCategory.font = boldFontWithSize(14)
        view.addSubview(txtCategory)
        let imgV = UIImageView(image: UIImage(named: "dropdown")!)
        imgV.frame = CGRectMake(0, 0, 30, 30)
        imgV.contentMode = .Center
        txtCategory.rightView = imgV
        txtCategory.rightViewMode = .Always
        
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRectMake(0, 0,  self.view.frame.size.width, self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .BlackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.Done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtCategory.inputAccessoryView = keyboardDoneButtonShow*/

//        let lblLine = UILabel()
//        lblLine.backgroundColor = Colors.GENERAL_BORDER_COLOR
//        lblLine.translatesAutoresizingMaskIntoConstraints = false
//        view.addSubview(lblLine)
        
        let iconwpname = UIImageView()
        iconwpname.image = UIImage(named: "work_loc")
        iconwpname.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconwpname)
        
        if(wpName?.characters.count>0) {
            txtWPname.text = wpName
        }
        txtWPname.textColor = Colors.GRAY_COLOR
        txtWPname.placeholder = "Workplace name"
        txtWPname.font = boldFontWithSize(15)
        txtWPname.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(txtWPname)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        let iconlocation = UIImageView()
        iconlocation.image = UIImage(named: "dest")
        iconlocation.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(iconlocation)
        
        lblLocation.textColor = Colors.GRAY_COLOR
        lblLocation.font = boldFontWithSize(15)
        if(wpLoc?.characters.count>0) {
            lblLocation.text = wpLoc
            reqObj.officeLocation = wpLoc
            reqObj.officeLat = wpLat
            reqObj.officeLong = wpLong
        } else {
            lblLocation.text = "Workplace location (Select exact or nearby google places)"
        }
        lblLocation.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForTo))
        lblLocation.isUserInteractionEnabled = true
        lblLocation.addGestureRecognizer(tapGestureTo)
        view.addSubview(lblLocation)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine2)
        
        let lblURLTitle = UILabel()
        lblURLTitle.text = "WORKPLACE URL"
        lblURLTitle.font = boldFontWithSize(10)
        lblURLTitle.textColor = Colors.GRAY_COLOR
        lblURLTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblURLTitle)
        
        lblWPURL.textColor = Colors.GRAY_COLOR
        lblWPURL.font = boldFontWithSize(15)
        lblWPURL.text = WORKPLACE_URL_BASE
        lblWPURL.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblWPURL)
        
        txtWPUID.textColor = Colors.GRAY_COLOR
        txtWPUID.placeholder = "Enter any Unique Id"
        txtWPUID.font = boldFontWithSize(15)
        txtWPUID.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(txtWPUID)
        
        let lblLine3 = UILabel()
        lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine3.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine3)
        
        txtEmail.textColor = Colors.GRAY_COLOR
        txtEmail.placeholder = "Enter your corporate email Id"
        txtEmail.font = boldFontWithSize(15)
        txtEmail.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(txtEmail)
        
        let lblLine4 = UILabel()
        lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine4.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine4)
        
        let lblUIDtxt = UILabel()
        lblUIDtxt.text = "Unique Id should contain alphanumeric, '-' and '_' only."
        lblUIDtxt.font = boldFontWithSize(10)
        lblUIDtxt.textColor = Colors.GRAY_COLOR
        lblUIDtxt.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblUIDtxt)
        
        let lblLine5 = UILabel()
        lblLine5.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine5.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine5)
        
        let lblCommentsTitle = UILabel()
        lblCommentsTitle.text = "COMMENT"
        lblCommentsTitle.font = boldFontWithSize(10)
        lblCommentsTitle.textColor = Colors.GRAY_COLOR
        lblCommentsTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCommentsTitle)
        
        txtComments.font = boldFontWithSize(15)
        txtComments.placeholder = "Enter your comment here"
        txtComments.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(txtComments)
        
        let lblLine6 = UILabel()
        lblLine6.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine6.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine6)
        
        txtWPname.delegate = self
        txtWPname.returnKeyType = .next
        txtEmail.delegate = self
        txtEmail.returnKeyType = .next
        txtWPUID.delegate = self
        txtWPUID.returnKeyType = .next
        txtComments.delegate = self
        txtComments.returnKeyType = .done
        
        
        //let viewsDict = ["lblCategory":lblCategory, "txtCategory":txtCategory, "line":lblLine, "iwpname":iconwpname, "wpname":txtWPname, "line1":lblLine1, "ilocation":iconlocation, "location":lblLocation, "line2":lblLine2, "lblurltitle":lblURLTitle, "lblurl":lblWPURL, "txtuid":txtWPUID, "line3":lblLine3, "lbluid":lblUIDtxt, "line4":lblLine4, "txtemail": txtEmail, "line5":lblLine5, "lblcomments":lblCommentsTitle, "txtcomments":txtComments, "line6":lblLine6]
        let viewsDict = ["iwpname":iconwpname, "wpname":txtWPname, "line1":lblLine1, "ilocation":iconlocation, "location":lblLocation, "line2":lblLine2, "lblurltitle":lblURLTitle, "lblurl":lblWPURL, "txtuid":txtWPUID, "line3":lblLine3, "lbluid":lblUIDtxt, "line4":lblLine4, "txtemail": txtEmail, "line5":lblLine5, "lblcomments":lblCommentsTitle, "txtcomments":txtComments, "line6":lblLine6]
        //view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[lblCategory(20)]-5-[txtCategory(30)][line(0.5)][wpname(45)][line1(0.5)][location(45)][line2(0.5)]-5-[txtemail(40)][line3(0.5)]-5-[lblurltitle(20)][lblurl(20)][txtuid(40)][line4(0.5)]-5-[lbluid(20)]-5-[line5(0.5)]-5-[lblcomments(20)]-5-[txtcomments(45)][line6(0.5)]", options: [], metrics: nil, views: viewsDict))
         view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[wpname(45)][line1(0.5)][location(45)][line2(0.5)]-5-[txtemail(40)][line3(0.5)]-5-[lblurltitle(20)][lblurl(20)][txtuid(40)][line4(0.5)]-5-[lbluid(20)]-5-[line5(0.5)]-5-[lblcomments(20)]-5-[txtcomments(45)][line6(0.5)]", options: [], metrics: nil, views: viewsDict))
        //view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[lblCategory]-5-|", options: [], metrics: nil, views: viewsDict))
        //view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-15-[txtCategory]-5-|", options: [], metrics: nil, views: viewsDict))
         //view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[line]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[iwpname(15)]-5-[wpname]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[ilocation(15)]-5-[location]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line5]|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line6]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblurltitle]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblurl]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txtuid]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txtemail]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbluid]-5-|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblcomments]-5-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txtcomments]-5-|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraint(NSLayoutConstraint(item: iconwpname, attribute: .centerY, relatedBy: .equal, toItem: txtWPname, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconlocation, attribute: .centerY, relatedBy: .equal, toItem: lblLocation, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 360)
        return sub
    }
    
    func proceed()
    {
        reqObj.userID = UserInfo.sharedInstance.userID
        reqObj.officeName = txtWPname.text
        reqObj.officeScope = "1"
        reqObj.officeKey = txtWPUID.text
        reqObj.officeDesc = txtComments.text
        reqObj.emailID = txtEmail.text
        
        if(reqObj.officeName?.characters.count > 0){
            if(reqObj.officeLocation != nil){
                if((reqObj.emailID != nil) && (reqObj.emailID?.characters.count > 0)){
                    showIndicator("Verifying Email.")
                    WorkplaceRequestor().verifyEmail(reqObj.emailID!, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            UserInfo.sharedInstance.secEmail = self.reqObj.emailID!
                            if(self.reqObj.officeKey?.characters.count > 0){
                                if(self.wpName?.characters.count > 0) {
                                    self.requestWP()
                                } else {
                                    self.searchWP()
                                }
                            }
                            else{
                                AlertController.showAlertFor("Request Workplace", message: "Please enter workplace unique id to proceed.")
                            }
                        }
                        else{
                            AlertController.showAlertFor("Request Workplace", message: "Please provide a valid workplace email Id.")
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }
                else{
                    AlertController.showAlertFor("Request Workplace", message: "Please enter corporate email to proceed.")
                }
            }
            else{
                AlertController.showAlertFor("Request Workplace", message: "Please choose workplace location from Google Places.")
            }
        }
        else{
            AlertController.showAlertFor("Request Workplace", message: "Please enter workplace name to proceed.")
        }
    }
    
    func searchWP()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Add Workplace Page",
            AnalyticsParameterContentType:"Submit Button"
            ])
        let searchReqObj = WPSearchRequest()
        searchReqObj.wpName = reqObj.officeName
        searchReqObj.wpLocation = reqObj.officeLocation
        searchReqObj.wpLat = reqObj.officeLat
        searchReqObj.wpLong = reqObj.officeLong
        searchReqObj.userId = UserInfo.sharedInstance.userID
        searchReqObj.category = ""
        
        showIndicator("Searching Workplaces.")
        WorkplaceRequestor().searchWorkplace(searchReqObj, success: { (success, object) in
            hideIndicator()
            if(success == true){
                _ = self.navigationController?.popViewController(animated: true)
                self.onExistingWPsBlock?((object as! WPSearchResponse).data, self.reqObj)
            }
            else{
                self.requestWP()
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func requestWP()
    {
        showIndicator("Requesting for workplace..")
        WorkplaceRequestor().addWorkPlace(self.reqObj, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Request Workplace", message: object as? String, okButtonTitle: "Ok", okAction: {
                    _ = self.navigationController?.popViewController(animated: true)
                })
            }
            else{
                AlertController.showAlertFor("Request Workplace", message: object as? String)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtWPUID){
            let text: NSString = (textField.text ?? "") as NSString
            let resultString = text.replacingCharacters(in: range, with: string)
            updateURL(resultString)
        }
        return true
    }
    
    func updateURL(_ url: String)
    {
        lblWPURL.text = WORKPLACE_URL_BASE+"\(url)"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == txtWPname){
            txtEmail.becomeFirstResponder()
        }
        else if(textField == txtEmail){
            txtWPUID.becomeFirstResponder()
        }
        else if(textField == txtWPUID){
            txtComments.becomeFirstResponder()
        }
        else if(textField == txtComments){
            txtComments.resignFirstResponder()
        }
        return true
    }
    
    func mapForTo()
    {
        let vc = LocationViewController()
        vc.onSelectionBlock = { (location) -> Void in
            if let loc = location as? [String: String]
            {
                self.lblLocation.text = loc["location"]
                self.reqObj.officeLocation = self.lblLocation.text
                self.reqObj.officeLat = loc["lat"]
                self.reqObj.officeLong = loc["long"]
            }
        }
         vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
//    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
//        return 1
//    }
//    
//    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return pickerDataSource.count;
//    }
//    
//    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return pickerDataSource[row]
//    }
//    
//    
//    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
//    {
//        txtCategory.text = pickerDataSource[row]
//        //setDefaultValueForSeatBasedOnVehicleType()
//    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
//        if  textField.text == "Workplace"
//        {
//            self.txtCategory.text = pickerDataSource[0]
//        }
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        selectedTxtField = nil
    }
    
//    func donePressed() -> Void {
//        if(selectedTxtField?.text == "Residential") {
//            txtEmail.hidden = true
//            reqObj.groupCategory = "6"
//        } else {
//             reqObj.groupCategory = "7"
//        }
//        selectedTxtField!.resignFirstResponder()
//    }

}
