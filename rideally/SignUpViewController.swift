//
//  SignUpViewController.swift
//  rideally
//
//  Created by Raghunathan on 8/7/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SignUpViewController: BaseScrollViewController {
    
    var firstNameTextBox  : RATextField!
    var lastNameTextBox   : RATextField!
    var emailTextBox      : RATextField!
    var passwordTextBox   : RATextField!
    var mobilenumberTextBox : RATextField!
    var referralTextBox : RATextField!
    var referralLabelText = UILabel()
    var referralUserName = UILabel()
    var radioButtonForFemale :  DLRadioButton!
    var radioButtonForMale : DLRadioButton!
    var  loaderView : UIView!
    var activityView: UIActivityIndicatorView!
    var isInvalidCode = false
    var firstNameLabel : UILabel!
    var lastNameLabel : UILabel!
    var emailLabel : UILabel!
    var passwordLabel : UILabel!
    var mobileNumberLabel : UILabel!
    var refCodeLabel : UILabel!
    var yPost_firstName: NSLayoutConstraint!
    var yPost_lastName: NSLayoutConstraint!
    var yPost_email: NSLayoutConstraint!
    var yPost_password: NSLayoutConstraint!
    var yPost_mobilenumber: NSLayoutConstraint!
    var yPost_refCode: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden=true
        setBGImageForView()
        viewsArray.append(createSignUpViewSubviews())
        loadActivityLoader()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = Colors.GREEN_COLOR
        }
        
    }
    
    
    func loadActivityLoader() -> Void
    {
        self.loaderView = UIView()
        self.loaderView.backgroundColor = UIColor.black
        self.loaderView.isHidden = true
        self.loaderView.layer.borderWidth = 1.0
        self.loaderView.layer.borderColor = UIColor.white.cgColor
        self.loaderView.layer.cornerRadius = 5.0
        self.loaderView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.loaderView)
        
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[loaderView(35)]", options:[], metrics:nil, views: ["loaderView": self.loaderView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[loaderView(100)]", options:[], metrics:nil, views: ["loaderView": self.loaderView]))
        self.view.addConstraint(NSLayoutConstraint(item: self.loaderView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.loaderView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        
        let titleVlaue = UILabel()
        titleVlaue.text = "Loading..."
        titleVlaue.textAlignment = NSTextAlignment.left
        titleVlaue.font = boldFontWithSize(15)
        titleVlaue.textColor = UIColor.white
        titleVlaue.translatesAutoresizingMaskIntoConstraints = false
        self.loaderView.addSubview(titleVlaue)
        
        
        activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityView.hidesWhenStopped = true;
        activityView.translatesAutoresizingMaskIntoConstraints = false
        self.loaderView.addSubview(activityView)
        
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-1-[titleVlaue]-1-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[activityView]-5-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[activityView(25)]-5-[titleVlaue]-5-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
    }
    
    
    func setBGImageForView() -> Void {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "LoginBG.png")
        self.contentView.addSubview(backgroundImage)
    }
    
    
    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.WHITE_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(20)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    func getFbButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(20)
        //buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }

    
    func getTextBoxViews(_ placeHolderString:String) -> RATextField
    {
        let textBoxInstance = RATextField()
        textBoxInstance.returnKeyType = UIReturnKeyType.next
        textBoxInstance.delegate = self
        textBoxInstance.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        
        let placeHolder = NSAttributedString(string: placeHolderString, attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
        textBoxInstance.attributedPlaceholder = placeHolder
        
        textBoxInstance.font = normalFontWithSize(15)!
        textBoxInstance.textColor = Colors.WHITE_COLOR
        return textBoxInstance
    }
    
    func getRadioButtonView(_ titleValue:String) ->  DLRadioButton{
        let radioButtonInstance = DLRadioButton()
        radioButtonInstance.setTitle(titleValue, for: UIControlState())
        radioButtonInstance.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        radioButtonInstance.iconColor = Colors.GREEN_COLOR
        radioButtonInstance.indicatorColor = Colors.GREEN_COLOR
        radioButtonInstance.titleLabel?.font = boldFontWithSize(12)
        return radioButtonInstance
    }
    
    func createSignUpViewSubviews() ->  SubView{
        
        let  facebookDescriptioLabelText  =  getLabelSubViews("We will not Post on your behalf")
        let  orLabelText        =  getLabelSubViews("OR")
        let  loginDescriptionLabelText  =  getLabelSubViews("Use your Email ID or Mobile Number")
        let  facebookButtonView =  getFbButtonSubViews("")
        //facebookButtonView.setBackgroundImage(UIImage(named:"LoginFacebook"), forState:UIControlState.Normal)
        facebookButtonView.setImage(UIImage(named:"LoginFacebook"), for: UIControlState())
        facebookButtonView.contentMode = .center
        facebookButtonView.tag = 99
        firstNameTextBox = getTextBoxViews("FIRST NAME")
        lastNameTextBox = getTextBoxViews("LAST NAME")
        emailTextBox = getTextBoxViews("EMAIL")
        passwordTextBox = getTextBoxViews("PASSWORD")
        passwordTextBox.isSecureTextEntry = true
        
        let image = UIImage(named: "infoIcon")
        let frame = CGRect(x: 0, y: 0, width: (image?.size.width)!, height: (image?.size.height)!)
        let infoImage = UIImageView(frame: frame)
        infoImage.image = image
        infoImage.contentMode = UIViewContentMode.center
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(SignUpViewController.infoImageTapped(_:)))
        infoImage.isUserInteractionEnabled = true
        infoImage.addGestureRecognizer(tapGestureRecognizer)
        
        let mobileNumberIndex = getLabelSubViews("+91")
        
        mobilenumberTextBox = getTextBoxViews("MOBILE NUMBER")
        mobilenumberTextBox.rightView = infoImage
        mobilenumberTextBox.rightViewMode = UITextFieldViewMode.always
        
        radioButtonForFemale = getRadioButtonView("Female")
        radioButtonForMale = getRadioButtonView("Male")
        
        radioButtonForFemale.otherButtons = [radioButtonForMale]
        
        referralLabelText.font = normalFontWithSize(15)
        referralLabelText.backgroundColor = UIColor.clear
        referralLabelText.isUserInteractionEnabled = true
        let myReferralAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
        let myReferralString = NSMutableAttributedString(string: "Have a Referral Code?", attributes: myReferralAttribute)
        let myReferralRange = NSRange(location: 0, length: 21)
        myReferralString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myReferralRange)
        referralLabelText.attributedText = myReferralString
        let referralTapGesture = UITapGestureRecognizer(target:self, action:#selector(showTextField))
        referralTapGesture.numberOfTapsRequired = 1
        referralTapGesture.numberOfTouchesRequired = 1
        referralLabelText.addGestureRecognizer(referralTapGesture)
        
        let referralInfoImage = UIImageView(frame: frame)
        referralInfoImage.image = image
        referralInfoImage.contentMode = UIViewContentMode.center
        let referralInfotapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(SignUpViewController.referralInfoImageTapped(_:)))
        referralInfoImage.isUserInteractionEnabled = true
        referralInfoImage.addGestureRecognizer(referralInfotapGestureRecognizer)
        
        referralTextBox = getTextBoxViews("REFERRAL CODE (OPTIONAL)")
        referralTextBox.rightView = referralInfoImage
        referralTextBox.rightViewMode = UITextFieldViewMode.always
        referralTextBox.returnKeyType = UIReturnKeyType.done
        referralTextBox.addTarget(self, action: #selector(SignUpViewController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        
        referralUserName.font = normalFontWithSize(15)
        referralUserName.backgroundColor = UIColor.clear
        
//        let  termsOfuse =  getLabelSubViews("")
//        termsOfuse.userInteractionEnabled = true
//        let myAttribute = [NSFontAttributeName: normalFontWithSize(15)!]
//        let myString = NSMutableAttributedString(string: "By clicking Sign Up, you agree to our Terms of Use", attributes: myAttribute)
//        let myRange = NSRange(location: 37, length: 13)
//        myString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange)
//        termsOfuse.attributedText = myString
//        let tapGesture = UITapGestureRecognizer(target:self, action:#selector(termsOfUsePressed(_:)))
//        tapGesture.numberOfTapsRequired = 1
//        tapGesture.numberOfTouchesRequired = 1
//        termsOfuse.addGestureRecognizer(tapGesture)
        
        let  signUpButtonView  =  getButtonSubViews("SIGN UP")
        signUpButtonView.tag = 100
        
        firstNameLabel  =  getLabelSubViews("FIRST NAME")
        firstNameLabel.isHidden = true
        firstNameLabel.font = normalFontWithSize(12)
        firstNameLabel.textColor = Colors.GREEN_COLOR
        firstNameLabel.textAlignment = NSTextAlignment.left
        
        lastNameLabel  =  getLabelSubViews("LAST NAME")
        lastNameLabel.isHidden = true
        lastNameLabel.font = normalFontWithSize(12)
        lastNameLabel.textColor = Colors.GREEN_COLOR
        lastNameLabel.textAlignment = NSTextAlignment.left
        
        emailLabel  =  getLabelSubViews("EMAIL")
        emailLabel.isHidden = true
        emailLabel.font = normalFontWithSize(12)
        emailLabel.textColor = Colors.GREEN_COLOR
        emailLabel.textAlignment = NSTextAlignment.left
        
        passwordLabel  =  getLabelSubViews("PASSWORD")
        passwordLabel.isHidden = true
        passwordLabel.font = normalFontWithSize(12)
        passwordLabel.textColor = Colors.GREEN_COLOR
        passwordLabel.textAlignment = NSTextAlignment.left
        
        mobileNumberLabel  =  getLabelSubViews("MOBILE NUMBER")
        mobileNumberLabel.isHidden = true
        mobileNumberLabel.font = normalFontWithSize(12)
        mobileNumberLabel.textColor = Colors.GREEN_COLOR
        mobileNumberLabel.textAlignment = NSTextAlignment.left
        
        refCodeLabel  =  getLabelSubViews("REFERRAL CODE (OPTIONAL)")
        refCodeLabel.isHidden = true
        refCodeLabel.font = normalFontWithSize(12)
        refCodeLabel.textColor = Colors.GREEN_COLOR
        refCodeLabel.textAlignment = NSTextAlignment.left
        
        let  signInLabelText = getLabelSubViews("")
        signInLabelText.isUserInteractionEnabled = true
        let myAttribute1 = [NSFontAttributeName: normalFontWithSize(15)!]
        let myString1 = NSMutableAttributedString(string: "Already have a RideAlly account? LOGIN", attributes: myAttribute1)
        let myRange1 = NSRange(location: 33, length: 5)
        myString1.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange1)
        signInLabelText.attributedText = myString1
        let tapGesture1 = UITapGestureRecognizer(target:self, action:#selector(labelGesturePressed(_:)))
        tapGesture1.numberOfTapsRequired = 1
        tapGesture1.numberOfTouchesRequired = 1
        signInLabelText.addGestureRecognizer(tapGesture1)
        
        
        let  signUpView = UIView()
        signUpView.addSubview(facebookButtonView)
        signUpView.addSubview(facebookDescriptioLabelText)
        signUpView.addSubview(orLabelText)
        signUpView.addSubview(loginDescriptionLabelText)
        signUpView.addSubview(firstNameTextBox)
        signUpView.addSubview(firstNameLabel)
        signUpView.addSubview(lastNameTextBox)
        signUpView.addSubview(lastNameLabel)
        signUpView.addSubview(emailTextBox)
        signUpView.addSubview(emailLabel)
        signUpView.addSubview(passwordTextBox)
        signUpView.addSubview(passwordLabel)
        signUpView.addSubview(mobilenumberTextBox)
        signUpView.addSubview(mobileNumberLabel)
        signUpView.addSubview(mobileNumberIndex)
        signUpView.addSubview(radioButtonForFemale)
        signUpView.addSubview(radioButtonForMale)
        signUpView.addSubview(signUpButtonView)
        signUpView.addSubview(signInLabelText)
        signUpView.addSubview(referralLabelText)
        signUpView.addSubview(referralTextBox)
        signUpView.addSubview(refCodeLabel)
        signUpView.addSubview(referralUserName)
        //signUpView.addSubview(termsOfuse)
        
        facebookButtonView.translatesAutoresizingMaskIntoConstraints = false
        facebookDescriptioLabelText.translatesAutoresizingMaskIntoConstraints = false
        orLabelText.translatesAutoresizingMaskIntoConstraints = false
        loginDescriptionLabelText.translatesAutoresizingMaskIntoConstraints = false
        firstNameTextBox.translatesAutoresizingMaskIntoConstraints = false
        firstNameLabel.translatesAutoresizingMaskIntoConstraints = false
        lastNameTextBox.translatesAutoresizingMaskIntoConstraints = false
        lastNameLabel.translatesAutoresizingMaskIntoConstraints = false
        emailTextBox.translatesAutoresizingMaskIntoConstraints = false
        emailLabel.translatesAutoresizingMaskIntoConstraints = false
        passwordTextBox.translatesAutoresizingMaskIntoConstraints = false
        passwordLabel.translatesAutoresizingMaskIntoConstraints = false
        radioButtonForFemale.translatesAutoresizingMaskIntoConstraints = false
        radioButtonForMale.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberIndex.translatesAutoresizingMaskIntoConstraints = false
        mobilenumberTextBox.translatesAutoresizingMaskIntoConstraints = false
        mobileNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        signUpButtonView.translatesAutoresizingMaskIntoConstraints = false
        signInLabelText.translatesAutoresizingMaskIntoConstraints = false
        referralLabelText.translatesAutoresizingMaskIntoConstraints = false
        referralTextBox.translatesAutoresizingMaskIntoConstraints = false
        refCodeLabel.translatesAutoresizingMaskIntoConstraints = false
        referralUserName.translatesAutoresizingMaskIntoConstraints = false
        //termsOfuse.translatesAutoresizingMaskIntoConstraints=false
        
        referralLabelText.isHidden = false
        referralTextBox.isHidden = true
        
        let subViewsDict = ["facebookButton": facebookButtonView,"facebookDespt":facebookDescriptioLabelText,"orValue":orLabelText,"loginDesp":loginDescriptionLabelText,"firstName":firstNameTextBox,"lastName":lastNameTextBox,"email":emailTextBox,"password":passwordTextBox,"male":radioButtonForMale,"female":radioButtonForFemale,"mobileNumber":mobilenumberTextBox,"nextbutton":signUpButtonView,"signIN":signInLabelText,"mobileNumberIndex":mobileNumberIndex, "referralLabel":referralLabelText,"referralText":referralTextBox,"referralName":referralUserName]
        
        signUpView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[facebookButton(40)]-10-[facebookDespt(10)]-10-[orValue(10)]-10-[loginDesp(10)]-10-[firstName(30)]-10-[email(==firstName)]-10-[password(==firstName)]-10-[female(==firstName)]-10-[mobileNumberIndex(==firstName)]-5-[referralLabel]-1-[referralText]-5-[referralName]-5-[nextbutton(==facebookButton)]-5-[signIN(==facebookButton)]", options:[], metrics:nil, views: subViewsDict))
        
        signUpView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[facebookButton]-15-|", options:[], metrics:nil, views: subViewsDict))
        
        //signUpView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[termsofuse]-5-|", options:[], metrics:nil, views: subViewsDict))
        
        signUpView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[radiobuttonFemale(75)]-5-[radiobuttonMale(==radiobuttonFemale)]", options: [], metrics: nil, views: ["radiobuttonFemale":radioButtonForFemale, "radiobuttonMale":radioButtonForMale]))
        
        signUpView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[firstName]-15-[lastName(==firstName)]-5-|", options: [], metrics: nil, views: subViewsDict))
        signUpView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[email]-5-|", options: [], metrics: nil, views: subViewsDict))
        signUpView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[mobileNumberIndex(30)][mobileNumber]-5-|", options: [], metrics: nil, views: subViewsDict))
        
        signUpView.addConstraint(NSLayoutConstraint(item: firstNameLabel, attribute: .left, relatedBy: .equal, toItem: firstNameTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: firstNameLabel, attribute: .width, relatedBy: .equal, toItem: firstNameTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: firstNameLabel, attribute: .height, relatedBy: .equal, toItem: firstNameTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        
        signUpView.addConstraint(NSLayoutConstraint(item: facebookButtonView, attribute: .centerX, relatedBy: .equal, toItem: signUpView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: facebookDescriptioLabelText, attribute: .centerX, relatedBy: .equal, toItem: signUpView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: orLabelText, attribute: .centerX, relatedBy: .equal, toItem: signUpView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: loginDescriptionLabelText, attribute: .centerX, relatedBy: .equal, toItem: signUpView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: lastNameTextBox, attribute: .centerY, relatedBy: .equal, toItem: firstNameTextBox, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: lastNameTextBox, attribute: .height, relatedBy: .equal, toItem: firstNameTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: radioButtonForMale, attribute: .centerY, relatedBy: .equal, toItem: radioButtonForFemale, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: lastNameLabel, attribute: .left, relatedBy: .equal, toItem: lastNameTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: lastNameLabel, attribute: .width, relatedBy: .equal, toItem: lastNameTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: lastNameLabel, attribute: .height, relatedBy: .equal, toItem: lastNameTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item:passwordTextBox, attribute: .left, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: passwordTextBox, attribute: .width, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: passwordLabel, attribute: .left, relatedBy: .equal, toItem: passwordTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: passwordLabel, attribute: .width, relatedBy: .equal, toItem: passwordTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: passwordLabel, attribute: .height, relatedBy: .equal, toItem: passwordTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item:referralLabelText, attribute: .left, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: referralLabelText, attribute: .width, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        signUpView.addConstraint(NSLayoutConstraint(item:referralTextBox, attribute: .left, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: referralTextBox, attribute: .width, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: refCodeLabel, attribute: .left, relatedBy: .equal, toItem: referralTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: refCodeLabel, attribute: .width, relatedBy: .equal, toItem: referralTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: refCodeLabel, attribute: .height, relatedBy: .equal, toItem: referralTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item:referralUserName, attribute: .left, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: referralUserName, attribute: .width, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        signUpView.addConstraint(NSLayoutConstraint(item:mobileNumberIndex, attribute: .left, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item:mobilenumberTextBox, attribute: .centerY, relatedBy: .equal, toItem: mobileNumberIndex, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: mobileNumberLabel, attribute: .left, relatedBy: .equal, toItem: mobilenumberTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: mobileNumberLabel, attribute: .width, relatedBy: .equal, toItem: mobilenumberTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        //signUpView.addConstraint(NSLayoutConstraint(item: mobileNumberLabel, attribute: .Height, relatedBy: .Equal, toItem: mobilenumberTextBox, attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0))
        
//        signUpView.addConstraint(NSLayoutConstraint(item: mobileNumberLabel, attribute: .Left, relatedBy: .Equal, toItem: mobilenumberTextBox, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0))
//        signUpView.addConstraint(NSLayoutConstraint(item: mobileNumberLabel, attribute: .Width, relatedBy: .Equal, toItem: mobilenumberTextBox, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: mobileNumberIndex, attribute: .height, relatedBy: .equal, toItem: mobilenumberTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        signUpView.addConstraint(NSLayoutConstraint(item: mobilenumberTextBox, attribute: .height, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        signUpView.addConstraint(NSLayoutConstraint(item:signUpButtonView, attribute: .left, relatedBy: .equal, toItem:facebookButtonView, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: signUpButtonView, attribute: .width, relatedBy: .equal, toItem:facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: emailLabel, attribute: .left, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: emailLabel, attribute: .width, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: emailLabel, attribute: .height, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: signInLabelText, attribute: .centerX, relatedBy: .equal, toItem: signUpView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        signUpView.addConstraint(NSLayoutConstraint(item: signInLabelText, attribute: .width, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        //signUpView.addConstraint(NSLayoutConstraint(item: termsOfuse, attribute: .CenterX, relatedBy: .Equal, toItem: signUpView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        //signUpView.addConstraint(NSLayoutConstraint(item: termsOfuse, attribute: .Width, relatedBy: .Equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0))
        
        yPost_firstName = NSLayoutConstraint(item: firstNameLabel, attribute: .top, relatedBy: .equal, toItem:orLabelText , attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        signUpView.addConstraint(yPost_firstName)
        
        yPost_lastName = NSLayoutConstraint(item: lastNameLabel, attribute: .top, relatedBy: .equal, toItem:orLabelText , attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        signUpView.addConstraint(yPost_lastName)
        
        yPost_email = NSLayoutConstraint(item: emailLabel, attribute: .top, relatedBy: .equal, toItem:firstNameTextBox , attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        signUpView.addConstraint(yPost_email)
        
        
        yPost_password = NSLayoutConstraint(item: passwordLabel, attribute: .top, relatedBy: .equal, toItem:emailTextBox, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        signUpView.addConstraint(yPost_password)
        
        yPost_mobilenumber = NSLayoutConstraint(item: mobileNumberLabel, attribute: .top, relatedBy: .equal, toItem:radioButtonForMale!, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        signUpView.addConstraint(yPost_mobilenumber)
        
        yPost_refCode = NSLayoutConstraint(item: refCodeLabel, attribute: .top, relatedBy: .equal, toItem:mobilenumberTextBox!, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        signUpView.addConstraint(yPost_refCode)
        
        let screenSize: CGRect = UIScreen.main.bounds
        let originY = screenSize.height - (screenSize.height * 0.90)
        
        let signUPSubViewInstance = SubView()
        signUPSubViewInstance.view = signUpView
        signUPSubViewInstance.padding = UIEdgeInsetsMake(originY, 0, 0, 0)
        signUPSubViewInstance.heightConstraint = NSLayoutConstraint(item: signUpView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 500)
        
        return signUPSubViewInstance
    }
    
    func showTextField () {
        referralTextBox.isHidden = false
        referralLabelText.isHidden = true
    }
    func buttonPressed (_ sender: AnyObject)
    {
        if sender.tag != 99 {
            
            if handleValidation().characters.count == 0
            {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"SignUp Page",
                    AnalyticsParameterItemName: "First Page",
                    AnalyticsParameterContentType:"Next Button"
                    ])
                //                let reqObj = emailIDRequestModel()
                //                reqObj.user_email = emailTextBox.text
                //                showLoadingIndicator("Loading...")
                //
                //                SignUpRequestor().isEmailIdalreadyexist(reqObj, success:{ (success, object) in
                //
                //                    self.hideLoadingIndiactor()
                //
                //                    if (object as! emailIDResponseModel).code == "1454"
                //                    {
                //                        let reqObj = mobileNumberRequestModel()
                //                        reqObj.user_mobile =  self.mobilenumberTextBox.text!
                //                        self.showLoadingIndicator("Loading...")
                //
                //                        SignUpRequestor().isMobilealreadyexist(reqObj, success:{ (success, object) in
                //
                //                            self.hideLoadingIndiactor()
                //
                //                            if (object as! mobileNumberResponseModel).code == "1456"
                //                            {
                //                                UserInfo.sharedInstance.firstName = self.firstNameTextBox.text!
                //                                UserInfo.sharedInstance.lastName = self.lastNameTextBox.text!
                //                                UserInfo.sharedInstance.email = self.emailTextBox.text!
                //                                UserInfo.sharedInstance.password = self.passwordTextBox.text!
                //                                UserInfo.sharedInstance.mobile =  self.mobilenumberTextBox.text!
                //                                UserInfo.sharedInstance.gender = self.selectedGender()
                //                                UserInfo.sharedInstance.refCode = self.referralTextBox.text!
                //                                self.navigationController?.pushViewController(SignUpMapViewController(), animated: true)
                //                            }
                //                            else
                //                            {
                //                                AlertController.showToastForError("This 'Mobile Number' already exists.")
                //                            }
                //
                //                        }){ (error) in
                //                            self.hideLoadingIndiactor()
                //                            AlertController.showAlertForError(error)
                //                        }
                //                    }
                //                    else
                //                    {
                //                        AlertController.showToastForError("This 'Email Id' already exists.")
                //                    }
                //
                //                }){ (error) in
                //                    self.hideLoadingIndiactor()
                //                    AlertController.showAlertForError(error)
                //                }
                
                
                
                
                
                
                let  reqObj = SignUpRequest()
                reqObj.user_first_name = self.firstNameTextBox.text
                reqObj.user_last_name = self.lastNameTextBox.text
                reqObj.user_email = self.emailTextBox.text
                reqObj.user_gender = self.selectedGender()
                reqObj.user_mobile = self.mobilenumberTextBox.text
                reqObj.user_password =  self.passwordTextBox.text
                reqObj.ref_code = self.referralTextBox.text
                reqObj.device_id = UIDevice.current.identifierForVendor?.uuidString
                reqObj.device_type =  "2"
                
                showIndicator("Loading...")
                
                SignUpRequestor().sendSignUpRequest(reqObj, success:{ (success, object) in
                    hideIndicator()
                    
                    if (object as! SignUpResponse).code == "450"
                    {
                        if (object as! SignUpResponse).dataObj?.first != nil
                        {
                            if (object as! SignUpResponse).dataObj?.first?.user_state != nil
                            {
                                if (object as! SignUpResponse).dataObj?.first?.user_state?.characters.count > 0
                                {
                                    
                                    if (object as! SignUpResponse).dataObj?.first?.user_id?.characters.count > 0
                                    {
                                        
                                        AlertController.showAlertFor("RideAlly", message: "Welcome \(self.firstNameTextBox.text!) ! you are successfully registered with RideAlly.", okButtonTitle: "Ok", okAction: {
                                            UserInfo.sharedInstance.userID = ((object as! SignUpResponse).dataObj?.first?.user_id)!
                                            
                                            UserInfo.sharedInstance.firstName = ((object as! SignUpResponse).dataObj?.first?.first_name)!
                                            UserInfo.sharedInstance.lastName = ((object as! SignUpResponse).dataObj?.first?.last_name)!
                                            UserInfo.sharedInstance.email = ((object as! SignUpResponse).dataObj?.first?.email)!
                                            UserInfo.sharedInstance.mobile = ((object as! SignUpResponse).dataObj?.first?.mobile_number)!
                                            UserInfo.sharedInstance.gender = ((object as! SignUpResponse).dataObj?.first?.gender)!
                                            //UserInfo.sharedInstance.refCode = ((object as! SignUpResponse).dataObj?.first?.)!
                                            
                                            UserInfo.sharedInstance.userState = ((object as! SignUpResponse).dataObj?.first?.user_state)!
                                            
                                            UserInfo.sharedInstance.profilepicStatus = ((object as! SignUpResponse).dataObj?.first?.profilepic_status)!
                                            
                                            
                                            UserInfo.sharedInstance.isEmailIDVerified = false
                                            if (((object as! SignUpResponse).dataObj?.first?.user_state) == "3" || ((object as! SignUpResponse).dataObj?.first?.user_state) == "5")
                                            {
                                                UserInfo.sharedInstance.isEmailIDVerified = true
                                            }
                                            
                                            
                                            if ((object as! SignUpResponse).dataObj?.first?.profilepic_status) == "0"
                                            {
                                                UserInfo.sharedInstance.profilepicUrl = ""
                                            }
                                            else
                                            {
                                                UserInfo.sharedInstance.profilepicUrl = ((object as!SignUpResponse).dataObj?.first?.profilepic_url)!
                                            }
                                            
                                            UserInfo.sharedInstance.faceBookid = ((object as! SignUpResponse).dataObj?.first?.fb_id)!
                                            UserInfo.sharedInstance.fullName = ((object as! SignUpResponse).dataObj?.first?.full_name)!
                                            UserInfo.sharedInstance.gender = ((object as! SignUpResponse).dataObj?.first?.gender)!
                                            UserInfo.sharedInstance.isOfficialEmail = ((object as! SignUpResponse).dataObj?.first?.isOfficialEmail)!
                                            
                                            if((object as! SignUpResponse).dataObj?.first?.home_address != nil) {
                                                UserInfo.sharedInstance.homeAddress = ((object as! SignUpResponse).dataObj?.first?.home_address)!
                                                UserInfo.sharedInstance.homeLatValue = ((object as! SignUpResponse).dataObj?.first?.home_lat)!
                                                UserInfo.sharedInstance.homeLongValue = ((object as! SignUpResponse).dataObj?.first?.home_long)!
                                            } else {
                                                UserInfo.sharedInstance.homeAddress = ""
                                                UserInfo.sharedInstance.homeLatValue = ""
                                                UserInfo.sharedInstance.homeLongValue = ""
                                            }
                                            
                                            UserInfo.sharedInstance.workAddress = ""
                                            UserInfo.sharedInstance.vehicleSeqID = ""
                                            UserInfo.sharedInstance.vehicleModel = ""
                                            UserInfo.sharedInstance.vehicleRegNo = ""
                                            UserInfo.sharedInstance.vehicleSeatCapacity = ""
                                            
                                            
                                            if ((object as! SignUpResponse).dataObj?.first?.work_address)?.characters.count > 0
                                            {
                                                UserInfo.sharedInstance.workAddress = ((object as! SignUpResponse).dataObj?.first?.work_address)!
                                            }
                                            
                                            if ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_seqID)?.characters.count > 0
                                            {
                                                UserInfo.sharedInstance.vehicleSeqID = ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_seqID)!
                                            }
                                            
                                            if ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_model)?.characters.count > 0
                                            {
                                                UserInfo.sharedInstance.vehicleModel = ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_model)!
                                            }
                                            
                                            
                                            if ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_regNo)?.characters.count > 0
                                            {
                                                UserInfo.sharedInstance.vehicleRegNo = ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_regNo)!
                                            }
                                            
                                            
                                            if ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_capacity)?.characters.count > 0
                                            {
                                                UserInfo.sharedInstance.vehicleSeatCapacity = ((object as! SignUpResponse).dataObj?.first?.default_Vehicle_capacity)!
                                            }
                                            
                                            UserInfo.sharedInstance.dob = ((object as! SignUpResponse).dataObj?.first?.dob)!
                                            UserInfo.sharedInstance.mobile = ((object as! SignUpResponse).dataObj?.first?.mobile_number)!
                                            UserInfo.sharedInstance.email = ((object as! SignUpResponse).dataObj?.first?.email)!
                                            UserInfo.sharedInstance.fullName = ((object as! SignUpResponse).dataObj?.first?.full_name)!
                                            
                                            var boolVal = true
                                            if ((object as! SignUpResponse).dataObj?.first?.share_mobile_number)! == "0"
                                            {
                                                boolVal = false
                                            }
                                            
                                            UserInfo.sharedInstance.shareMobileNumber = boolVal
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.allRides != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.allRides != "") {
                                                UserInfo.sharedInstance.allRides = ((object as! SignUpResponse).dataObj?.first?.userDetail?.allRides)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.allWorkplaces != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.allWorkplaces != "") {
                                                UserInfo.sharedInstance.allWorkplaces = ((object as! SignUpResponse).dataObj?.first?.userDetail?.allWorkplaces)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.myWorkplace != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.myWorkplace != "") {
                                                UserInfo.sharedInstance.myWorkplaces = ((object as! SignUpResponse).dataObj?.first?.userDetail?.myWorkplace)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.myPendingWorkplace != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.myPendingWorkplace != "") {
                                                UserInfo.sharedInstance.myPendingWorkplaces = ((object as! SignUpResponse).dataObj?.first?.userDetail?.myPendingWorkplace)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.offered != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.offered != "") {
                                                UserInfo.sharedInstance.offeredRides = ((object as! SignUpResponse).dataObj?.first?.userDetail?.offered)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.needed != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.needed != "") {
                                                UserInfo.sharedInstance.neededRides = ((object as! SignUpResponse).dataObj?.first?.userDetail?.needed)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.joined != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.joined != "") {
                                                UserInfo.sharedInstance.joinedRides = ((object as! SignUpResponse).dataObj?.first?.userDetail?.joined)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.booked != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.booked != "") {
                                                UserInfo.sharedInstance.bookedRides = ((object as! SignUpResponse).dataObj?.first?.userDetail?.booked)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.pointsDetail?.maximumallowedpoints != nil && (object as! SignUpResponse).dataObj?.first?.userDetail?.pointsDetail?.maximumallowedpoints != "") {
                                                UserInfo.sharedInstance.maxAllowedPoints = ((object as! SignUpResponse).dataObj?.first?.userDetail?.pointsDetail?.maximumallowedpoints)!
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.userDetail?.pointsDetail?.pointsavailable != nil && String(describing: (object as! SignUpResponse).dataObj?.first?.userDetail?.pointsDetail?.pointsavailable) != "") {
                                                UserInfo.sharedInstance.availablePoints = (((object as! SignUpResponse).dataObj?.first?.userDetail?.pointsDetail?.pointsavailable)!)
                                            }
                                            if((object as! SignUpResponse).dataObj?.first?.hideAnywhere != nil && (object as! SignUpResponse).dataObj?.first?.hideAnywhere != "") {
                                                UserInfo.sharedInstance.hideAnywhere = ((object as! SignUpResponse).dataObj?.first?.hideAnywhere)!
                                            }
                                            self.sendDeviceToken()
                                            UserInfo.sharedInstance.isLoggedIn = true
                                            
                                            if  UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3" {
                                                AlertController.showToastForInfo("Please verify 'Mobile Number' to proceed further.")
                                                let vc = MobileVerificationViewController()
                                                vc.hidesBottomBarWhenPushed = true
                                                vc.edgesForExtendedLayout = UIRectEdge()
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            } else {
                                                let myRides = Int(UserInfo.sharedInstance.offeredRides)! + Int(UserInfo.sharedInstance.neededRides)! + Int(UserInfo.sharedInstance.joinedRides)! + Int(UserInfo.sharedInstance.bookedRides)!
                                                let ins = UIApplication.shared.delegate as! AppDelegate
                                                if(myRides > 0) { // || UserInfo.sharedInstance.isOfficialEmail == "0"
                                                    let vc = RideViewController()
                                                    vc.hidesBottomBarWhenPushed = true
                                                    vc.edgesForExtendedLayout = UIRectEdge()
                                                    self.navigationController?.pushViewController(vc, animated: true)
                                                } else if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                                                    if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0) {
                                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                                            ins.gotoHome()
                                                        } else {
                                                            ins.gotoHome(1)
                                                        }
                                                    } else if(Int(UserInfo.sharedInstance.myWorkplaces) == 1){
                                                        self.getMyWP()
                                                    } else {
                                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                                            ins.gotoHome()
                                                        } else {
                                                            ins.gotoHome(1)
                                                        }
                                                    }
                                                }
                                            }
                                        })
                                        
                                    }
                                    else
                                    {
                                        AlertController.showToastForError("There is no User_ID found in the response.")
                                    }
                                    
                                }
                                //}
                            }
                        }
                    }
                    else if (object as! SignUpResponse).code == "454"
                    {
                        AlertController.showToastForError("This 'Email Id' already exists.")
                    }
                    else if (object as! SignUpResponse).code == "456"
                    {
                        AlertController.showToastForError("This 'Mobile Number' already exists.")
                    }
                    })
                { (error) in
                    hideIndicator()
                    AlertController.showAlertForError(error)
                }
            }
            else
            {
                AlertController.showToastForError(handleValidation())
            }
            
        }
        else if sender.tag == 99 {
            facebookButtonPressedEvent()
        }
        
    }
    
    
    func facebookButtonPressedEvent() -> Void {
        let fbLogin = FBSDKLoginManager()
        fbLogin.logOut()
        
        fbLogin.logIn(withReadPermissions: ["public_profile","email"], from: self, handler: { (result, error) -> Void in
            
            self.view.bringSubview(toFront: self.loaderView)
            self.loaderView.isHidden = false
            self.activityView.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            
            if (error == nil)
            {
                
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if(fbloginresult.isCancelled)
                {
                    self.view.isUserInteractionEnabled = true
                    self.loaderView.isHidden = true
                    self.activityView.stopAnimating()
                }
                else
                {
                    self.getDetailsAndLogin()
                }
            }
            else
            {
                self.view.isUserInteractionEnabled = true
                self.loaderView.isHidden = true
                self.activityView.stopAnimating()
            }
        })
        
    }
    
    func getDetailsAndLogin() -> Void{
        
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,gender,name,first_name,last_name, email,link,birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                
                if (error == nil)
                {
                    let resultDict = result as! NSDictionary
                    let reqObj = FBLoginRequest()
                    reqObj.device_id = UIDevice.current.identifierForVendor?.uuidString
                    reqObj.id = resultDict.value(forKey: "id") as? String
                    reqObj.email = resultDict.value(forKey: "email") as? String
                    reqObj.firstName = resultDict.value(forKey: "first_name") as? String
                    reqObj.lastName = resultDict.value(forKey: "last_name") as? String
                    reqObj.link = resultDict.value(forKey: "link") as? String
                    reqObj.gender = resultDict.value(forKey: "gender") as? String
                    
                    LoginRequestor().sendFBLoginRequest(reqObj, success:{ (success, object) in
                        if ((object as! FBLoginResponse).FBdataObj?.first?.user_id)?.characters.count > 0
                        {
                            self.view.isUserInteractionEnabled = true
                            self.loaderView.isHidden = true
                            self.activityView.stopAnimating()
                            UserInfo.sharedInstance.userID = ((object as! FBLoginResponse).FBdataObj?.first?.user_id)!
                            UserInfo.sharedInstance.userState = ((object as! FBLoginResponse).FBdataObj?.first?.user_state)!
                            UserInfo.sharedInstance.profilepicStatus = ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_status)!
                            UserInfo.sharedInstance.fullName = ((object as! FBLoginResponse).FBdataObj?.first?.full_name)!
                            UserInfo.sharedInstance.firstName = ((object as! FBLoginResponse).FBdataObj?.first?.first_name)!
                            UserInfo.sharedInstance.lastName = ((object as! FBLoginResponse).FBdataObj?.first?.last_name)!
                            UserInfo.sharedInstance.gender = ((object as! FBLoginResponse).FBdataObj?.first?.gender)!
                            UserInfo.sharedInstance.isOfficialEmail = ((object as! FBLoginResponse).FBdataObj?.first?.isOfficialEmail)!
                            if((object as! FBLoginResponse).FBdataObj?.first?.work_address != nil) {
                                UserInfo.sharedInstance.workAddress = ((object as! FBLoginResponse).FBdataObj?.first?.work_address)!
                                UserInfo.sharedInstance.workLatValue = ((object as! FBLoginResponse).FBdataObj?.first?.work_lat)!
                                UserInfo.sharedInstance.workLongValue = ((object as! FBLoginResponse).FBdataObj?.first?.work_long)!
                            } else {
                                UserInfo.sharedInstance.workAddress = ""
                                UserInfo.sharedInstance.workLatValue = ""
                                UserInfo.sharedInstance.workLongValue = ""
                            }
                            
                            if((object as! FBLoginResponse).FBdataObj?.first?.home_address != nil) {
                                UserInfo.sharedInstance.homeAddress = ((object as! FBLoginResponse).FBdataObj?.first?.home_address)!
                                UserInfo.sharedInstance.homeLatValue = ((object as! FBLoginResponse).FBdataObj?.first?.home_lat)!
                                UserInfo.sharedInstance.homeLongValue = ((object as! FBLoginResponse).FBdataObj?.first?.home_long)!
                            } else {
                                UserInfo.sharedInstance.homeAddress = ""
                                UserInfo.sharedInstance.homeLatValue = ""
                                UserInfo.sharedInstance.homeLongValue = ""
                            }
                            
                            UserInfo.sharedInstance.dob = ((object as! FBLoginResponse).FBdataObj?.first?.dob)!
                            UserInfo.sharedInstance.faceBookid = ((object as! FBLoginResponse).FBdataObj?.first?.fb_id)!
                            UserInfo.sharedInstance.email = ((object as! FBLoginResponse).FBdataObj?.first?.email)!
                            UserInfo.sharedInstance.mobile = ((object as! FBLoginResponse).FBdataObj?.first?.mobile_number)!
                            UserInfo.sharedInstance.secEmail = ""
                            if((object as! FBLoginResponse).FBdataObj?.first?.secondary_email != nil) {
                                UserInfo.sharedInstance.secEmail = ((object as! FBLoginResponse).FBdataObj?.first?.secondary_email)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.secondary_email_status == "2") {
                                UserInfo.sharedInstance.isSecEmailIDVerified = true
                            } else {
                                UserInfo.sharedInstance.isSecEmailIDVerified = false
                            }
                            
                            UserInfo.sharedInstance.isEmailIDVerified = false
                            if (((object as! FBLoginResponse).FBdataObj?.first?.user_state) == "3" || ((object as! FBLoginResponse).FBdataObj?.first?.user_state) == "5")
                            {
                                UserInfo.sharedInstance.isEmailIDVerified = true
                            }
                            
                            UserInfo.sharedInstance.vehicleSeqID = ""
                            UserInfo.sharedInstance.vehicleModel = ""
                            UserInfo.sharedInstance.vehicleRegNo = ""
                            UserInfo.sharedInstance.vehicleSeatCapacity = ""
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.work_address)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.workAddress = ((object as! FBLoginResponse).FBdataObj?.first?.work_address)!
                            }
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_seqID)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeqID = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_seqID)!
                            }
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_model)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleModel = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_model)!
                            }
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_regNo)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleRegNo = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_regNo)!
                            }
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_capacity)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeatCapacity = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_capacity)!
                            }
                            
                            var boolVal = true
                            if ((object as! FBLoginResponse).FBdataObj?.first?.share_mobile_number)! == "0"
                            {
                                boolVal = false
                            }
                            UserInfo.sharedInstance.shareMobileNumber = boolVal
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_status)! == "0"
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                            }
                            else
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                                if ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_url)?.characters.count > 0
                                {
                                    UserInfo.sharedInstance.profilepicUrl = ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_url)!
                                }
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides != "") {
                                UserInfo.sharedInstance.allRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces != "") {
                                UserInfo.sharedInstance.allWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace != "") {
                                UserInfo.sharedInstance.myWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace != "") {
                                UserInfo.sharedInstance.myPendingWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered != "") {
                                UserInfo.sharedInstance.offeredRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed != "") {
                                UserInfo.sharedInstance.neededRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined != "") {
                                UserInfo.sharedInstance.joinedRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked != "") {
                                UserInfo.sharedInstance.bookedRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints != "") {
                                UserInfo.sharedInstance.maxAllowedPoints = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable != nil && String(describing: (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable) != "") {
                                UserInfo.sharedInstance.availablePoints = (((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable)!)
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere != nil && (object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere != "") {
                                UserInfo.sharedInstance.hideAnywhere = ((object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere)!
                            }
                            self.sendDeviceToken()
                            UserInfo.sharedInstance.isLoggedIn = true
                            
                            if (object as! FBLoginResponse).code == "411" && (UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3") {
                                hideIndicator()
                                AlertController.showAlertFor("RideAlly", message: "Welcome \(UserInfo.sharedInstance.firstName) ! you are successfully registered with RideAlly.", okButtonTitle: "Ok", okAction: {
                                    AlertController.showToastForInfo("Please provide 'Mobile Number' to proceed further.")
                                    let vc = MobileVerificationViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                })
                            } else if UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3" {
                                hideIndicator()
                                if(UserInfo.sharedInstance.mobile == "") {
                                    AlertController.showToastForInfo("Please provide 'Mobile Number' to proceed further.")
                                } else {
                                    AlertController.showToastForInfo("Please verify 'Mobile Number' to proceed further.")
                                }
                                let vc = MobileVerificationViewController()
                                vc.hidesBottomBarWhenPushed = true
                                vc.edgesForExtendedLayout = UIRectEdge()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else if(UserInfo.sharedInstance.homeAddress == "") {
                                hideIndicator()
                                AlertController.showToastForInfo("Please provide 'Home Location' to proceed further.")
                                let vc = SignUpMapViewController()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else {
                                hideIndicator()
                                let myRides = Int(UserInfo.sharedInstance.offeredRides)! + Int(UserInfo.sharedInstance.neededRides)! + Int(UserInfo.sharedInstance.joinedRides)! + Int(UserInfo.sharedInstance.bookedRides)!
                                let ins = UIApplication.shared.delegate as! AppDelegate
                                if(myRides > 0) { // || UserInfo.sharedInstance.isOfficialEmail == "0"
                                    let vc = RideViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                                    if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0) {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    } else if(Int(UserInfo.sharedInstance.myWorkplaces) == 1){
                                        self.getMyWP()
                                    } else {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    }
                                } else {
                                    let vc = PoolOptionsViewController()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        else
                        {
                            AlertController.showToastForError("User_ID is not there in the response.")
                        }
                        
                        
                    }){ (error) in
                        hideIndicator()
                        AlertController.showAlertForError(error)
                    }
                }
                else
                {
                    hideIndicator()
                    AlertController.showToastForError("Sorry, either 'Email Id' or 'Password' is incorrect.")
                }
            })
        }
    }
    
    
    func selectedGender() -> String {
        
        if  radioButtonForMale.isSelected == true
        {
            return "M"
        }
        
        return "F"
    }
    
    func isGenderSelected() -> Bool{
        return (radioButtonForMale.isSelected || radioButtonForFemale.isSelected)
    }
    
    func labelGesturePressed(_ sender:AnyObject)  {
        self.navigationController?.pushViewController(LoginViewController(), animated: true)
    }
    
//    func termsOfUsePressed(sender:AnyObject)  {
//        self.navigationController?.pushViewController(StaticPagesViewController(), animated: true)
//    }
    
    func handleValidation() -> String {
        
        if  firstNameTextBox.text?.characters.count == 0 && emailTextBox.text?.characters.count == 0 && passwordTextBox.text?.characters.count == 0 && mobilenumberTextBox.text?.characters.count == 0 && !isGenderSelected()
        {
            return "Please provide 'First Name'."
        }
        else if firstNameTextBox.text?.characters.count == 0
        {
            return "Please provide 'First Name'."
        }
        else if !isAlphabetsOnly(firstNameTextBox.text!)
        {
            return "Please enter only characters in 'First Name'."
        }
        else if !isAlphabetsOnly(lastNameTextBox.text!)
        {
            return "Please enter only characters in 'Last Name'."
        }
        else if emailTextBox.text?.characters.count == 0
        {
            return "Please provide 'Email Id'."
        }
        else if !isValidEmailFormate(emailTextBox.text!)
        {
            return "Please enter valid 'Email Id'."
        }
        else if passwordTextBox.text?.characters.count == 0
        {
            return "Please provide 'Password'."
        }
        else if passwordTextBox.text?.characters.count < 6 || passwordTextBox.text?.characters.count > 32
        {
            return "Please provide 'Password' between 6 and 32 characters."
        }
        else if !isGenderSelected()
        {
            return "Please select 'Gender'."
        }
        else if mobilenumberTextBox.text?.characters.count == 0
        {
            return "Please provide 'Mobile No'."
        }
        else if mobilenumberTextBox.text?.characters.count < 10
        {
            return "Please provide valid 'Mobile No'."
        }
        else if isInvalidCode
        {
            return "Please provide valid 'Referral Code'."
        }
        return ""
    }
    
    
    func infoImageTapped(_ imgObject: AnyObject) -> Void {
        AlertController.showAlertFor("Why Mobile?", message: "It is needed to notify you about your rides, ride members, groups etc. It will be shared with your fellow riders only when you join / offer any ride.")
    }
    
    func referralInfoImageTapped(_ imgObject: AnyObject) -> Void {
        AlertController.showAlertFor("Referral Code?", message: "With the referral code, you will get bonus points once you creates a ride.")
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == firstNameTextBox)
        {
            yPost_firstName.constant = 0
            let placeHolder = NSAttributedString(string: "FIRST NAME", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            firstNameTextBox.attributedPlaceholder = placeHolder
            firstNameLabel .isHidden = true
            lastNameTextBox.becomeFirstResponder()
        }
        else if (textField == lastNameTextBox)
        {
            yPost_lastName.constant = 0
            let placeHolder = NSAttributedString(string: "LAST NAME", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            lastNameTextBox.attributedPlaceholder = placeHolder
            lastNameLabel .isHidden = true
            emailTextBox.becomeFirstResponder()
        }
        else if (textField == emailTextBox)
        {
            yPost_email.constant = 0
            let placeHolder = NSAttributedString(string: "EMAIL", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            emailTextBox.attributedPlaceholder = placeHolder
            emailLabel .isHidden = true
            passwordTextBox.becomeFirstResponder()
        }
        else if (textField == passwordTextBox)
        {
            yPost_password.constant = 0
            let placeHolder = NSAttributedString(string: "PASSWORD", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            passwordTextBox.attributedPlaceholder = placeHolder
            passwordLabel .isHidden = true
            mobilenumberTextBox.becomeFirstResponder()
        }
        else if (textField == mobilenumberTextBox)
        {
            yPost_mobilenumber.constant = 0
            let placeHolder = NSAttributedString(string: "MOBILE NUMBER", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            mobilenumberTextBox.attributedPlaceholder = placeHolder
            mobileNumberLabel .isHidden = true
            referralTextBox.becomeFirstResponder()
        }
        else if (textField == referralTextBox)
        {
            yPost_refCode.constant = 0
            let placeHolder = NSAttributedString(string: "REFERRAL CODE (OPTIONAL)", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            referralTextBox.attributedPlaceholder = placeHolder
            refCodeLabel .isHidden = true
            referralTextBox.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField === firstNameTextBox)
        {
            yPost_firstName.constant = 25;
            firstNameTextBox.placeholder = ""
            firstNameLabel.isHidden = false
        }
        else if (textField === lastNameTextBox)
        {
            yPost_lastName.constant = 25;
            lastNameTextBox.placeholder = ""
            lastNameLabel.isHidden = false
        }
        else if (textField === emailTextBox)
        {
            yPost_email.constant = 25;
            emailTextBox.placeholder = ""
            emailLabel.isHidden = false
        }
        else if (textField === passwordTextBox)
        {
            yPost_password.constant = 25;
            passwordTextBox.placeholder = ""
            passwordLabel.isHidden = false
        }
        else if (textField === mobilenumberTextBox)
        {
            yPost_mobilenumber.constant = 25;
            mobilenumberTextBox.placeholder = ""
            mobileNumberLabel.isHidden = false
        }
        else if (textField === referralTextBox)
        {
            yPost_refCode.constant = 25;
            referralTextBox.placeholder = ""
            refCodeLabel.isHidden = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        let lenghtValue = (textField.text?.characters.count)! + string.characters.count - range.length
        
        if  textField == mobilenumberTextBox
        {
            
            if lenghtValue > 10
            {
                return false
            }
            else
            {
                let aSet = CharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
            }
        }
        
        return true
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if(textField == referralTextBox) {
            self.isInvalidCode = false
            if(referralTextBox.text?.characters.count == 0) {
                self.referralUserName.isHidden = true
            } else {
                self.referralUserName.isHidden = false
                isReferralCodeValid()
            }
        }
    }
    
    func isReferralCodeValid () {
        let reqObj = referralCodeRequestModel()
        reqObj.ref_code = referralTextBox.text
        SignUpRequestor().isReferralCodeValid(reqObj, success:{(success, object) in
            if (object as! referralCodeResponseModel).code == "111" {
                self.isInvalidCode = false
                self.referralUserName.textColor = Colors.GREEN_COLOR
                self.referralUserName.text = (object as! referralCodeResponseModel).message!+" "+((object as! referralCodeResponseModel).dataObj?.first?.firstName)!
            } else {
                self.isInvalidCode = true
                self.referralUserName.textColor = Colors.RED_COLOR
                self.referralUserName.text = (object as! referralCodeResponseModel).message
            }
        }) { (error) in
        }
    }
}
