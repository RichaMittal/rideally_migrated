//
//  TaxiSubscriptionViewController.swift
//  rideally
//
//  Created by Sarav on 22/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class SubscriptionCell: UITableViewCell {
    
    let lblVehicleType = UILabel()
    let lblMember = UILabel()
    let lblPrice = UILabel()
    let priceRB = DLRadioButton()

    var data: SubscriptionItem!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .gray
        
        lblVehicleType.text = "VEHICLE TYPE"
        lblVehicleType.translatesAutoresizingMaskIntoConstraints = false
        lblMember.text = "MEMBERS"
        lblMember.translatesAutoresizingMaskIntoConstraints = false
        lblPrice.text = "PRICE"
        lblPrice.translatesAutoresizingMaskIntoConstraints = false
        
        lblVehicleType.font = normalFontWithSize(10)
        lblMember.font = normalFontWithSize(10)
        lblPrice.font = normalFontWithSize(10)
        
        lblVehicleType.textColor = Colors.GRAY_COLOR
        lblMember.textColor = Colors.GRAY_COLOR
        lblPrice.textColor = Colors.GRAY_COLOR

        lblVehicleType.textAlignment = .center
        lblMember.textAlignment = .center
        lblPrice.textAlignment = .center
        
        self.contentView.addSubview(lblVehicleType)
        self.contentView.addSubview(lblMember)
        self.contentView.addSubview(lblPrice)
        
        priceRB.isHidden = true
        priceRB.iconColor = Colors.GREEN_COLOR
        priceRB.indicatorColor = Colors.GREEN_COLOR
        priceRB.translatesAutoresizingMaskIntoConstraints = false
        lblPrice.addSubview(priceRB)
        
        lblPrice.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[rb(15)]", options: [], metrics: nil, views: ["rb":priceRB]))
        lblPrice.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[rb(15)]", options: [], metrics: nil, views: ["rb":priceRB]))
        lblPrice.addConstraint(NSLayoutConstraint(item: priceRB, attribute: .centerY, relatedBy: .equal, toItem: lblPrice, attribute: .centerY, multiplier: 1, constant: 0))
        

        lblVehicleType.layer.borderWidth = 1
        lblVehicleType.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        lblMember.layer.borderWidth = 1
        lblMember.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        lblPrice.layer.borderWidth = 1
        lblPrice.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        
//        self.contentView.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        
        
        let viewsDict = ["type":lblVehicleType, "member":lblMember, "price":lblPrice]
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[type][member(==type)][price(==type)]-10-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[type]|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[member]|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[price]|", options: [], metrics: nil, views: viewsDict))
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(selected){
            priceRB.isSelected = true
        }
        else{
            priceRB.isSelected = false
        }
    }
    
    func buildCell()
    {
        priceRB.isHidden = false
        lblVehicleType.font = normalFontWithSize(12)
        lblMember.font = normalFontWithSize(12)
        lblPrice.font = normalFontWithSize(12)

        lblVehicleType.text = data.vehicleType
        lblMember.text = data.members
        lblPrice.text = "\(RUPEE_SYMBOL) \(data.cost!)/km"
        
        setNeedsLayout()
    }
}

class TaxiSubscriptionViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource {

    var onCompletionBlock: actionBlockWithParam?
    var list: [SubscriptionItem]?
    
    lazy var tableView: SubView = self.getTableView()
    
    var selectedItem: SubscriptionItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Preferred Subscription"
        
        viewsArray.append(tableView)
        addBottomView(getBottomView())
        
        getSubscriptions()
    }
    
    let table = UITableView()
    func getTableView() -> SubView
    {
        table.delegate = self
        table.dataSource = self
        table.separatorColor = UIColor.clear
        table.register(SubscriptionCell.self, forCellReuseIdentifier: "subscriptioncell")
        
        let sub = SubView()
        sub.view = table
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: table, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 120)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let btn = UIButton()
        btn.setTitle("SAVE", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(proceed), for: .touchDown)
        view.addSubview(btn)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(40)]", options: [], metrics: nil, views: ["btn":btn]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btn]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func proceed()
    {        
        _ = self.navigationController?.popViewController(animated: true)
        onCompletionBlock?(selectedItem)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let cnt = list?.count{
            return cnt+1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subscriptioncell") as! SubscriptionCell
        if(indexPath.row != 0){
            cell.data = list![indexPath.row-1]
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row != 0){
            selectedItem = list![indexPath.row-1]
        }
    }
    
    func getSubscriptions()
    {
        showIndicator("Getting subscriptions..")
        ShareTaxiRequestor().getSubscriptions({ (success, object) in
            hideIndicator()
            if(success){
                self.list = (object as! SubscriptionResponse).list
                if let cnt = self.list?.count{
                    self.tableView.heightConstraint?.constant = 30 * CGFloat(cnt+1)
                    self.table.reloadData()
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
}
