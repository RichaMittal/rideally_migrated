//
//  TestimonialViewController.swift
//  rideally
//
//  Created by Sarav on 16/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class TestimonialCell: UITableViewCell {
    
    let profilePic = UIImageView()
    let lblName = UILabel()
    let lblComment = UILabel()
    //let lblRating = UILabel()
    
    var data: TestimonialData!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        
        /*profilePic.translatesAutoresizingMaskIntoConstraints = false
        profilePic.backgroundColor = UIColor.grayColor()
        self.contentView.addSubview(profilePic)*/
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = boldFontWithSize(15)
        lblName.textColor = Colors.GREEN_COLOR
        lblName.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblName.numberOfLines = 0
        self.contentView.addSubview(lblName)
        
        lblComment.translatesAutoresizingMaskIntoConstraints = false
        lblComment.font = boldFontWithSize(15)
        lblComment.textColor = Colors.GRAY_COLOR
        lblComment.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblComment.numberOfLines = 0
        self.contentView.addSubview(lblComment)
        
        /*lblRating.translatesAutoresizingMaskIntoConstraints = false
        lblRating.font = boldFontWithSize(15)
        lblRating.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblRating)*/
        
        let viewsDict = ["name":lblName, "comment":lblComment]
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[comment]-5-[name]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[comment]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[name]-5-|", options: [], metrics: nil, views: viewsDict))
    }
    
    func buildCell()
    {
        //profilePic.image = UIImage(named: "placeholder")
        lblName.text = data.ra_tm_by
        lblComment.text = data.ra_tm_detail!.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        //lblRating.text = data.ra_tm_rating
        
        setNeedsLayout()
    }
}

class TestimonialViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource {
    
    var testimonialList = [TestimonialData]()
    let table = UITableView()
    
    override func viewDidLoad() {
        getTestimonials()
        super.viewDidLoad()
        title = "Testimonials"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        table.delegate = self
        table.dataSource = self
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 40
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorColor = UIColor.clear
        table.register(TestimonialCell.self, forCellReuseIdentifier: "testimonialcell")
        self.view.addSubview(table)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views:["table":table]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-64-[table]|", options: [], metrics: nil, views:["table":table]))
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testimonialList.count
    }
    
    /*func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let ht: CGFloat = 100
        return ht
    }
    */
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row%2 == 0){
            cell.backgroundColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
        }
        else{
            cell.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "testimonialcell") as! TestimonialCell
            cell.data = testimonialList[indexPath.row]
        return cell
    }
    
    func getTestimonials()
    {
        showIndicator("Getting Testimonials..")
        TestimonialRequestor().sendTestimonialRequest({ (success, object) in
            hideIndicator()
            if (object as! TestimonialResponse).code == "474" {
                self.testimonialList = ((object as! TestimonialResponse).dataObj?.dataList)!
                    self.table.reloadData()
            }
        }) { (error) in
            hideIndicator()
        }
    }
}
