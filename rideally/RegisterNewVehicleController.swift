//
//  RegisterNewVehicleController.swift
//  rideally
//
//  Created by Raghunathan on 9/3/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RegisterNewVehicleController: BaseScrollViewController,UIPickerViewDelegate,UIPickerViewDataSource{
    
    var isUpdateReqneedtobeSend : String?
    var isSeatCapacityNeedToBeUpdated : String?
    var isFromWp: String?
    var noVehicle: Bool?
    var selectedTxtField: UITextField?
    let addVehicleObj = AddVehicleRequest()
    let pickerView = UIPickerView()
    let txtType = UITextField()
    let txtModel = UITextField()
    let txtRegNo = UITextField()
    let txtName = UITextField()
    var pickerDataSource = [String]()
    var personalPickerDataSource :Array = ["Bike", "Car", "Suv"]
    var commercialPickerDataSource :Array = ["Auto", "Bus", "Cab", "Suv", "Tempo"]
    let btnPersonal = DLRadioButton()
    let btnCommercial = DLRadioButton()
    var onAddSuccessAction: actionBlock?
    var signupFlow = false
    var btnTag = ""
    var isWpInsuranceStatus = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            self.title = "Update Vehicles"
        }
        else
        {
            self.title = "Add Vehicles"
        }
        if(signupFlow == true) {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = Colors.GREEN_COLOR
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        }
        if  self.isUpdateReqneedtobeSend == "1"
        {
            if(UserInfo.sharedInstance.vehicleKind == "") {
                if(UserInfo.sharedInstance.vehicleType.lowercased() == "auto" || UserInfo.sharedInstance.vehicleType.lowercased() == "bus" || UserInfo.sharedInstance.vehicleType.lowercased() == "cab" || UserInfo.sharedInstance.vehicleType.lowercased() == "tempo") {
                    UserInfo.sharedInstance.vehicleKind = "commercial"
                    pickerDataSource = commercialPickerDataSource
                } else {
                    UserInfo.sharedInstance.vehicleKind = "personal"
                    pickerDataSource = personalPickerDataSource
                }
            } else {
                if(UserInfo.sharedInstance.vehicleKind != "" && UserInfo.sharedInstance.vehicleKind == "commercial") {
                    pickerDataSource = commercialPickerDataSource
                } else {
                    pickerDataSource = personalPickerDataSource
                }
            }
        } else {
            pickerDataSource = personalPickerDataSource
        }
        viewsArray.append(getUsageOfVehicle())
        viewsArray.append(getVehicleOwnerName())
        viewsArray.append(getVehicleType())
        viewsArray.append(getVehicleModel())
        viewsArray.append(getVehicleRegNo())
        viewsArray.append(getNoSeats())
        if(signupFlow) {
            addBottomView(getSignUpFlowBottomView())
        } else {
            addBottomView(getBottomView())
        }
    }
    
    func getUsageOfVehicle() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "USAGE OF VEHICLE"
        lblTitle.font = boldFontWithSize(11)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        btnPersonal.setTitle("Personal", for: UIControlState())
        btnPersonal.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnPersonal.iconColor = Colors.GREEN_COLOR
        btnPersonal.indicatorColor = Colors.GREEN_COLOR
        btnPersonal.titleLabel?.font = boldFontWithSize(14)
        btnPersonal.translatesAutoresizingMaskIntoConstraints = false
        btnPersonal.isSelected = true
        btnPersonal.addTarget(self, action: #selector(usageTypeChanged(_:)), for: .touchDown)
        view.addSubview(btnPersonal)
        
        btnCommercial.setTitle("Commercial", for: UIControlState())
        btnCommercial.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        btnCommercial.iconColor = Colors.GREEN_COLOR
        btnCommercial.indicatorColor = Colors.GREEN_COLOR
        btnCommercial.titleLabel?.font = boldFontWithSize(14)
        btnCommercial.translatesAutoresizingMaskIntoConstraints = false
        btnCommercial.addTarget(self, action: #selector(usageTypeChanged(_:)), for: .touchDown)
        view.addSubview(btnCommercial)
        
        btnPersonal.otherButtons = [btnCommercial]
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            if(UserInfo.sharedInstance.vehicleKind != "" && UserInfo.sharedInstance.vehicleKind == "commercial") {
                btnCommercial.isSelected = true
            } else {
                btnPersonal.isSelected = true
            }
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[btnP(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "btnP":btnPersonal, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[btnP(80)]-10-[btnC(100)]", options: [], metrics: nil, views: ["btnP":btnPersonal, "btnC":btnCommercial]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        view.addConstraint(NSLayoutConstraint(item: btnCommercial, attribute: .centerY, relatedBy: .equal, toItem: btnPersonal, attribute: .centerY, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    func getVehicleOwnerName() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "VEHICLE OWNER NAME"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtName.delegate=self
        txtName.translatesAutoresizingMaskIntoConstraints = false
        txtName.font = boldFontWithSize(14)
        txtName.placeholder = "Enter Vehicle Onwer Name"
        txtName.returnKeyType = UIReturnKeyType.next
        view.addSubview(txtName)
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            txtName.text = UserInfo.sharedInstance.vehicleOwnerName
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtName, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtName]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
        
    }
    
    func getVehicleType() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "VEHICLE TYPE"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        txtType.delegate=self
        txtType.translatesAutoresizingMaskIntoConstraints = false
        txtType.text = "Vehicle Type"
        txtType.inputView = pickerView
        txtType.font = boldFontWithSize(14)
        txtType.placeholder = "Select"
        view.addSubview(txtType)
        let imgV = UIImageView(image: UIImage(named: "dropdown")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        txtType.rightView = imgV
        txtType.rightViewMode = .always
        
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtType.inputAccessoryView = keyboardDoneButtonShow
        
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            let first = String(UserInfo.sharedInstance.vehicleType.characters.prefix(1)).capitalized
            let other = String(UserInfo.sharedInstance.vehicleType.characters.dropFirst()).lowercased()
            txtType.text = first + other
            pickerView.selectRow(pickerDataSource.index(of: first + other)!, inComponent: 0, animated: true)
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtType, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtType]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    
    func getVehicleModel() -> SubView
    {
        
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "VEHICLE MODEL"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtModel.delegate=self
        txtModel.translatesAutoresizingMaskIntoConstraints = false
        txtModel.font = boldFontWithSize(14)
        txtModel.placeholder = "Enter Vehicle Model"
        txtModel.returnKeyType = UIReturnKeyType.next
        view.addSubview(txtModel)
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            txtModel.text = UserInfo.sharedInstance.vehicleModel
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtModel, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtModel]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    
    func getVehicleRegNo() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "REGISTRATION NUMBER"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        txtRegNo.delegate=self
        txtRegNo.translatesAutoresizingMaskIntoConstraints = false
        txtRegNo.font = boldFontWithSize(14)
        txtRegNo.placeholder = "xx-00-x/xx-0000/0000"
        txtRegNo.returnKeyType = UIReturnKeyType.done
        view.addSubview(txtRegNo)
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            txtRegNo.text = UserInfo.sharedInstance.vehicleRegNo
        }
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":txtRegNo, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt]-5-|", options: [], metrics: nil, views: ["txt":txtRegNo]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    
    let stepper = PKYStepper()
    func getNoSeats() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "SEATS"
        lblTitle.font = boldFontWithSize(10)
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        stepper.setBorderColor(Colors.GRAY_COLOR)
        stepper.setBorderWidth(0.5)
        stepper.countLabel.layer.borderWidth = 0.5
        stepper.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        
        stepper.valueChangedCallback = { (stepper, count) -> Void in
            
            if  self.txtType.text == "Bike" &&  count > 2
            {
                AlertController.showToastForError("Seat selection is exceeding its maximum capacity.")
            }
            else if  self.txtType.text == "Auto" &&  count > 4
            {
                AlertController.showToastForError("Seat selection is exceeding its maximum capacity.")
            }
            else if  self.txtType.text == "Car" &&  count > 5
            {
                AlertController.showToastForError("Seat selection is exceeding its maximum capacity.")
            }
            else if  self.txtType.text == "Cab" &&  count > 7
            {
                AlertController.showToastForError("Seat selection is exceeding its maximum capacity.")
            }
            else if  self.txtType.text == "Suv" &&  count > 10
            {
                AlertController.showToastForError("Seat selection is exceeding its maximum capacity.")
            }
            else if  self.txtType.text == "Tempo" &&  count > 25
            {
                AlertController.showToastForError("Seat selection is exceeding its maximum capacity.")
            }
            else if  self.txtType.text == "Bus" &&  count > 60
            {
                AlertController.showToastForError("Seat selection is exceeding its maximum capacity.")
            }
            else
            {
                stepper?.countLabel.text = "\(count)"
            }
            
        }
        
        stepper.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepper)
        
        setDefaultValueForSeatBasedOnVehicleType()
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            stepper.countLabel.text = "\(UserInfo.sharedInstance.vehicleSeatCapacity)"
            stepper.value = getIntValueForString()
        }
        
        
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblLine1)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)]-5-[txt(30)]-10-[line(0.5)]", options: [], metrics: nil, views: ["lbl":lblTitle, "txt":stepper, "line":lblLine1]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[txt(200)]", options: [], metrics: nil, views: ["txt":stepper]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine1]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 71)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "By adding vehicle, I delcare that I am 18 years above."
        lblTitle.backgroundColor = UIColor.clear
        lblTitle.font = boldFontWithSize(8)
        lblTitle.textAlignment = .center
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        let btnInsurance = UIButton()
        btnInsurance.backgroundColor = Colors.WHITE_COLOR
        btnInsurance.translatesAutoresizingMaskIntoConstraints = false
        btnInsurance.setTitle(" Update your Insurance and RC", for: UIControlState())
        btnInsurance.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnInsurance.setImage(UIImage(named: "edit_green"), for: UIControlState())
        btnInsurance.contentHorizontalAlignment = .left
        btnInsurance.titleLabel?.font = boldFontWithSize(14)
        btnInsurance.addTarget(self, action: #selector(updateInsurance), for: .touchDown)
        view.addSubview(btnInsurance)
        
        let btn = UIButton()
        btn.setTitle("ADD", for: UIControlState())
        btn.backgroundColor = Colors.GREEN_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.setTitleColor(Colors.WHITE_COLOR, for: .highlighted)
        btn.titleLabel?.font = boldFontWithSize(16)
        btn.addTarget(self, action: #selector(proceed(_:)), for: .touchDown)
        view.addSubview(btn)
        
        if  self.isUpdateReqneedtobeSend == "1"
        {
            lblTitle.isHidden = true
            btnInsurance.isHidden = false
//            if(UserInfo.sharedInstance.insuranceId != "" || isWpInsuranceStatus) {
//                btn.setTitle("NEXT", forState: .Normal)
//            } else {
                btn.setTitle("UPDATE", for: UIControlState())
            //}
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btnInsurance(20)]-10-[btn(40)]", options: [], metrics: nil, views: ["btnInsurance":btnInsurance, "btn":btn]))
             view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[btnInsurance]|", options: [], metrics: nil, views: ["btnInsurance":btnInsurance]))
        }
        else
        {
            lblTitle.isHidden = false
            btnInsurance.isHidden = true
            if(isWpInsuranceStatus) {
                 btn.setTitle("NEXT", for: UIControlState())
            } else {
                btn.setTitle("ADD", for: UIControlState())
            }
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(30)][btn(40)]", options: [], metrics: nil, views: ["lbl":lblTitle,"btn":btn]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        }
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btn]))
        
        
        let sub = SubView()
        sub.view = view
        
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 70)
        return sub
    }
    
    func getSignUpFlowBottomView() -> SubView
    {
        let view = UIView()
        
        let lblTitle = UILabel()
        lblTitle.text = "By adding vehicle, I delcare that I am 18 years above."
        lblTitle.backgroundColor = UIColor.clear
        lblTitle.font = boldFontWithSize(8)
        lblTitle.textAlignment = .center
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        let btnSkip = UIButton()
        btnSkip.setTitle("SKIP", for: UIControlState())
        btnSkip.backgroundColor = Colors.GRAY_COLOR
        btnSkip.translatesAutoresizingMaskIntoConstraints = false
        btnSkip.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnSkip.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btnSkip.addTarget(self, action: #selector(skipClicked), for: .touchDown)
        view.addSubview(btnSkip)
        
        let btn = UIButton()
        btn.setTitle("ADD", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(proceed(_:)), for: .touchDown)
        view.addSubview(btn)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(20)][btnSkip(40)]", options: [], metrics: nil, views: ["lbl":lblTitle,"btnSkip":btnSkip]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnSkip]-1-[btn(==btnSkip)]|", options: [], metrics: nil, views: ["btnSkip":btnSkip, "btn":btn]))
        view.addConstraint(NSLayoutConstraint(item: btn, attribute: .centerY, relatedBy: .equal, toItem: btnSkip, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: btn, attribute: .height, relatedBy: .equal, toItem: btnSkip, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
        return sub
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        txtType.text = pickerDataSource[row]
        setDefaultValueForSeatBasedOnVehicleType()
    }
    
    
    func getIntValueForString() -> Int32{
        
        if  stepper.countLabel.text?.characters.count > 0
        {
            let myNumber = NumberFormatter().number(from: stepper.countLabel.text!)
            return myNumber!.int32Value
        }
        if(btnPersonal.isSelected == true) {
            return 2
        } else {
            return 4
        }
    }
    
    func proceed(_ sender: UIButton)
    {
        
        //print("the new val is: ",txtType.text)
        let val = getIntValueForString()
        
        if txtName.text?.characters.count == 0 && txtType.text == "Vehicle Type" && txtRegNo.text?.characters.count == 0 && (stepper.countLabel.text == "0" || stepper.countLabel.text == nil)
        {
            AlertController.showToastForError("Please provide information in all the fields.")
        }
        else if txtName.text?.characters.count == 0
        {
            AlertController.showToastForError("Please provide 'Vehicle Owner Name'.")
        }
        else if txtType.text?.characters.count == 0 || txtType.text == "Vehicle Type"
        {
            AlertController.showToastForError("Please choose 'Vehicle Type'.")
        }
        else if txtRegNo.text?.characters.count == 0
        {
            AlertController.showToastForError("Please provide 'Vehicle Registration Number'.")
        }
        else if txtRegNo.text?.characters.count < 5
        {
            AlertController.showToastForError("Please provide valid 'Vehicle Registration Number'.")
        }
        else if stepper.countLabel.text?.characters.count == 0
        {
            AlertController.showToastForError("Please provide number of seats available in the vehicle within the 'Capacity' field.")
        }
        else if val == 0
        {
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity cannot be 0.")
        }
        else if  txtType.text == "Bike" &&  val > 2 {
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity more than 2.")
        }
        else if  txtType.text == "Auto" &&  val > 4 {
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity more than 4.")
        }
        else if  txtType.text == "Car" &&  val > 5{
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity more than 5.")
        }
        else if  txtType.text == "Cab" &&  val > 7{
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity more than 7.")
        }
        else if  txtType.text == "Suv" &&  val > 10{
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity more than 10.")
        }
        else if  txtType.text == "Tempo" &&  val > 25{
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity more than 25.")
        }
        else if  txtType.text == "Bus" &&  val > 60{
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity more than 60.")
        }
        else if (txtType.text == "Bike" || txtType.text == "Auto" || txtType.text == "Car" ||  txtType.text == "Cab" || txtType.text == "Suv" || txtType.text == "Tempo" || txtType.text == "Bus") && val < 2
        {
            AlertController.showToastForError("Oops! You cannot choose vehicle capacity less than 2.")
        }
        else
        {
            if  sender.titleLabel?.text == "UPDATE"{
                sendUpdateVehicleRequest()
            } else if sender.titleLabel?.text == "NEXT"{
                updateInsurance()
            } else
            {
                sendAddVehicleRequest()
            }
        }
    }
    
    func updateInsurance() {
        let vc = VehicleInsuranceViewController()
        vc.vehicleCapacity = stepper.countLabel.text!
        vc.vehicleModel = txtModel.text!
        vc.vehicleRegNo = txtRegNo.text!
        vc.vehicleType = txtType.text!
        vc.vehicleKind = btnPersonal.isSelected == true ? "personal" : "commercial"
        vc.vehicleOwnerName = txtName.text!
        vc.isUpdateReqneedtobeSend = isUpdateReqneedtobeSend
        if (self.isFromWp != nil && self.isFromWp == "1") {
            vc.isFromWp = "1"
        }
        if(noVehicle != nil && noVehicle == true) {
            vc.noVehicle = noVehicle
        }
        vc.onAddSuccessAction = { () -> Void in
            self.onAddSuccessAction?()
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func selectedUsageType() -> String {
        
        if  btnPersonal.isSelected == true
        {
            return "personal"
        }
        
        return "commercial"
    }
    
    func sendUpdateVehicleRequest() -> Void {
        
        if  selectedUsageType() == UserInfo.sharedInstance.vehicleKind && txtName.text == UserInfo.sharedInstance.vehicleOwnerName && txtType.text == UserInfo.sharedInstance.vehicleType && txtModel.text == UserInfo.sharedInstance.vehicleModel && txtRegNo.text == UserInfo.sharedInstance.vehicleRegNo && stepper.countLabel.text == UserInfo.sharedInstance.vehicleSeatCapacity
        {
            AlertController.showToastForError("Oops! It seems that you have not modified anything.")
        }
        else
        {
            if  UserInfo.sharedInstance.userID.characters.count > 0
            {
                let updateVehicleObj = UpdateVehicleRequest()
                
                updateVehicleObj.userID = UserInfo.sharedInstance.userID
                updateVehicleObj.vehicleSeqID = UserInfo.sharedInstance.vehicleSeqID
                updateVehicleObj.vehicleCapacity = stepper.countLabel.text
                updateVehicleObj.vehicleModel = txtModel.text
                updateVehicleObj.vehicleRegNo = txtRegNo.text
                updateVehicleObj.vehicleType = txtType.text
                updateVehicleObj.vehicleKind = btnPersonal.isSelected == true ? "personal" : "commercial"
                updateVehicleObj.vehicleOwnerName = txtName.text
                
                showLoadingIndicator("Loading...")
                
                ProfileRequestor().updateVehicleListData(updateVehicleObj, success: { (success, object) in
                    
                    self.hideLoadingIndiactor()
                    
                    if (object as! UpdateVehicleResponse).code == "223"
                    {
                        AlertController.showAlertFor("RideAlly", message:"Your vehicle information is successfully updated." , okButtonTitle: "Ok", okAction:
                            {
                                if self.isFromWp != nil && self.isFromWp == "1"{
                                    self.backTwo()
                                    ISVEHICLEUPDATED = true
                                }else{
                                    _ = self.navigationController?.popViewController(animated: true)
                                    self.onAddSuccessAction?()
                                }
                        })
                    } else if((object as! UpdateVehicleResponse).code == "221") {
                        AlertController.showToastForError("Sorry, you are not able to update vehicle information if there are any active rides.")
                    } else
                    {
                        AlertController.showToastForError((object as! UpdateVehicleResponse).message!)
                    }
                    
                }) { (error) in
                    self.hideLoadingIndiactor()
                    AlertController.showAlertForError(error)
                }
                
            }
            else
            {
                AlertController.showToastForError("Please provide a valid UserID.")
            }
        }
    }
    
    func sendAddVehicleRequest() -> Void {
        
        if  UserInfo.sharedInstance.userID.characters.count > 0 {
            
            addVehicleObj.userID = UserInfo.sharedInstance.userID
            addVehicleObj.vehicleCapacity = stepper.countLabel.text
            addVehicleObj.vehicleModel = txtModel.text
            addVehicleObj.vehicleRegNo = txtRegNo.text
            addVehicleObj.vehicleType = txtType.text
            addVehicleObj.vehicleKind = btnPersonal.isSelected == true ? "personal" : "commercial"
            addVehicleObj.vehicleOwnerName = txtName.text
            
            showLoadingIndicator("Loading...")
            
            ProfileRequestor().addVehicle(addVehicleObj, success: { (success, object) in
                
                self.hideLoadingIndiactor()
                
                if (object as! AddVehicleResponse).code == "200"
                {
                    AlertController.showAlertFor("RideAlly", message:"Thank you! Your vehicle information is saved successfully." , okButtonTitle: "Ok", okAction:
                        {
                            if (self.signupFlow == true) {
                                if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1")
                                {
                                    let vc = WpSearchViewController()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else {
                                    let vc = GroupOptionsViewController()
                                    vc.btnTag = self.btnTag
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            } else if self.isFromWp != nil && self.isFromWp == "1"{
                                self.backTwo()
                                ISVEHICLEUPDATED = true
                            }else{
                                _ = self.navigationController?.popViewController(animated: true)
                                self.onAddSuccessAction?()
                            }
                    })
                }
                else
                {
                    AlertController.showToastForError((object as! AddVehicleResponse).message!)
                }
                
            }) { (error) in
                self.hideLoadingIndiactor()
                AlertController.showAlertForError(error)
            }
        }
        else
        {
            AlertController.showToastForError("Please provide a valid UserID.")
        }
    }
    
    func backTwo() {
        //        let dashboardVC = navigationController!.viewControllers.filter { $0 is RideMapViewController }.first!
        //        navigationController!.popToViewController(dashboardVC, animated: true)
        
        if(noVehicle != nil && noVehicle == true) {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }
    }
    
    func donePressed() -> Void {
        selectedTxtField!.resignFirstResponder()
    }
    
    func skipClicked() {
        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1")
        {
            let vc = WpSearchViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = GroupOptionsViewController()
            vc.btnTag = self.btnTag
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func setDefaultValueForSeatBasedOnVehicleType() ->  Void
    {
        if  self.txtType.text == "Bike"
        {
            stepper.countLabel.text = "2"
        }
        else if  self.txtType.text == "Auto"
        {
            stepper.countLabel.text = "4"
        }
        else if  self.txtType.text == "Car"
        {
            stepper.countLabel.text = "5"
        }
        else if  self.txtType.text == "Cab"
        {
            stepper.countLabel.text = "7"
        }
        else if  self.txtType.text == "Suv"
        {
            stepper.countLabel.text = "10"
        }
        else if  self.txtType.text == "Tempo"
        {
            stepper.countLabel.text = "25"
        }
        else if  self.txtType.text == "Bus"
        {
            stepper.countLabel.text = "60"
        }
        
        if  self.txtType.text != "Vehicle Type"
        {
            stepper.value = self.getIntValueForString()
        }
    }
    
    //Uitextfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if  textField.text == "Vehicle Type"
        {
            self.txtType.text = pickerDataSource[0]
            if(btnPersonal.isSelected == true) {
                stepper.countLabel.text = "2"
            } else {
                stepper.countLabel.text = "4"
            }
            stepper.value = self.getIntValueForString()
        }
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        selectedTxtField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField === txtName)
        {
            txtType.becomeFirstResponder()
        } else if (textField === txtType)
        {
            txtModel.becomeFirstResponder()
        } else if (textField === txtModel)
        {
            txtRegNo.becomeFirstResponder()
        } else if (textField === txtRegNo)
        {
            txtRegNo.resignFirstResponder()
        }
        
        return true
    }
    
    func usageTypeChanged(_ btn: DLRadioButton) {
        if(btn == btnPersonal) {
            pickerDataSource = personalPickerDataSource
        } else {
            pickerDataSource = commercialPickerDataSource
        }
        txtType.text = "Vehicle Type"
        stepper.countLabel.text = ""
    }
}
