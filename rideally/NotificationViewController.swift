//
//  NotificationViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/21/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    let notificationImage = UIImageView()
    let infoView = UIView()
    let lblName = UILabel()
    let lblNew = UILabel()
    let lblMsg = UILabel()
    let lblSource = UILabel()
    let lblDest = UILabel()
    let lblTime = UILabel()
    let imgSource = UIImageView()
    let imgDestination = UIImageView()
    let imgTime = UIImageView()
    let threeDotButton = UIButton()
    var onUserActionBlock: actionBlock?
    
    var data: NotificationData!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        
        notificationImage.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(notificationImage)
        
        infoView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(infoView)
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[pic(40)][info]|", options: [], metrics: nil, views: ["pic":notificationImage, "info":infoView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[info]-5-|", options: [], metrics: nil, views: ["info":infoView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(40)]", options: [], metrics: nil, views: ["pic":notificationImage]))
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = boldFontWithSize(15)
        lblName.textColor = Colors.GREEN_COLOR
        lblName.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblName.numberOfLines = 0
        infoView.addSubview(lblName)
        
        lblNew.translatesAutoresizingMaskIntoConstraints = false
        lblNew.font = boldFontWithSize(15)
        lblNew.textColor = Colors.ORANGE_TEXT_COLOR
        lblNew.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblNew.numberOfLines = 0
        lblNew.backgroundColor = Colors.ORANGE_BG_COLOR
        infoView.addSubview(lblNew)
        
        threeDotButton.setImage(UIImage(named: "three_dot"), for: UIControlState())
        threeDotButton.translatesAutoresizingMaskIntoConstraints = false
        threeDotButton.addTarget(self, action: #selector(btn_clicked), for: .touchDown)
        infoView.addSubview(threeDotButton)
        
        lblMsg.translatesAutoresizingMaskIntoConstraints = false
        lblMsg.font = boldFontWithSize(12)
        lblMsg.textColor = Colors.GRAY_COLOR
        lblMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblMsg.numberOfLines = 0
        infoView.addSubview(lblMsg)
        
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        lblSource.font = boldFontWithSize(12)
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblSource.numberOfLines = 0
        infoView.addSubview(lblSource)
        
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        lblDest.font = boldFontWithSize(12)
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblDest.numberOfLines = 0
        infoView.addSubview(lblDest)
        
        lblTime.translatesAutoresizingMaskIntoConstraints = false
        lblTime.font = boldFontWithSize(10)
        lblTime.textColor = Colors.GRAY_COLOR
        lblTime.lineBreakMode = NSLineBreakMode.byWordWrapping
        lblTime.numberOfLines = 0
        infoView.addSubview(lblTime)
        
        imgSource.translatesAutoresizingMaskIntoConstraints = false
        infoView.addSubview(imgSource)
        
        imgDestination.translatesAutoresizingMaskIntoConstraints = false
        infoView.addSubview(imgDestination)
        
        imgTime.translatesAutoresizingMaskIntoConstraints = false
        infoView.addSubview(imgTime)
        
        let viewsDict = ["name":lblName,"new":lblNew,"msg":lblMsg, "source":lblSource,"dest":lblDest, "time":lblTime,"imgsource":imgSource, "imgdest":imgDestination, "imgtime":imgTime, "delete":threeDotButton]
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[name(15)]-5-[msg]-5-[source(15)]-5-[dest(15)]-5-[time(15)]-5-|", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[name]-10-[new(30)]", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[delete(20)]-5-|", options: [], metrics: nil, views: viewsDict))
        
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[msg]-5-|", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[imgsource(15)]-5-[source]-5-|", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[imgdest(15)]-5-[dest]-5-|", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[imgtime(15)]-5-[time]-5-|", options: [], metrics: nil, views: viewsDict))
        
        infoView.addConstraint(NSLayoutConstraint(item: lblNew, attribute: .centerY, relatedBy: .equal, toItem: lblName, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: lblNew, attribute: .height, relatedBy: .equal, toItem: lblName, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: imgSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: imgDestination, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: imgTime, attribute: .centerY, relatedBy: .equal, toItem: lblTime, attribute: .centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: threeDotButton, attribute: .centerY, relatedBy: .equal, toItem: lblNew, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: threeDotButton, attribute: .height, relatedBy: .equal, toItem: lblNew, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
    }
    
    func buildCell()
    {
        notificationImage.image = UIImage(named: "notif_wp")
        var ifOwner = 0
        if(data.senderName != nil) {
            var splitvalue = data.senderName?.components(separatedBy: " ")
            if((data.notificationMsg!.range(of: splitvalue![0])) != nil) {
                ifOwner = 1
            }
        }
        lblNew.text = "New"
        lblMsg.text = data.notificationMsg!.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
        lblSource.text = data.sourceLoc
        lblDest.text = data.destinationLoc
        lblTime.text = "2 days ago"
        imgSource.image = UIImage(named: "source")
        imgDestination.image = UIImage(named: "dest")
        imgTime.image = UIImage(named: "time")
        setNeedsLayout()
        
        if let time = data.createdTime {
            if time != "" {
                let timeInterval = calculateTimeInterval(time)
                lblTime.text = timeInterval
            }
        }
        if(data.notificationStatus != nil && data.notificationStatus == "Read") {
            lblNew.isHidden = true;
        } else {
            lblNew.isHidden = false;
        }
        if(data.senderName != nil && ifOwner == 0 && data.senderId != UserInfo.sharedInstance.userID) {
            lblName.text = data.senderName
        } else {
            lblName.text = "You"
        }
        if(data.groupId != nil && (data.poolId == nil || data.poolId == "0")) {
            notificationImage.image = UIImage(named: "notif_wp")
            imgSource.image = UIImage(named: "work_loc")
            lblSource.text = data.groupName
            imgDestination.image = UIImage(named:"source")
            lblDest.text = data.wpLoc
        }
            /*else if(notificationList[indexPath.row].routeId != nil && (notificationList[indexPath.row].poolId == nil || notificationList[indexPath.row].poolId == "0")) {
             cell.notificationImage.image = UIImage(named: "notif_route")
             if(notificationList[indexPath.row].routeSource != nil) {
             
             } else {
             
             }
         }*/ else {
            notificationImage.image = UIImage(named: "notif_ride")
            if(data.sourceLoc == nil) {
                imgSource.isHidden = true
                lblSource.isHidden = true
            } else {
                imgSource.isHidden = false
                lblSource.isHidden = false
            }
            if(data.destinationLoc == nil) {
                imgDestination.isHidden = true
                lblDest.isHidden = true
            } else {
                imgDestination.isHidden = false
                lblDest.isHidden = false
            }
        }
    }
    
    func btn_clicked(){
        onUserActionBlock?()
    }
    
    func calculateTimeInterval(_ createdTime: String?) -> String {
        var timeText = createdTime
        var minutesStr: String?
        let formatter =  DateFormatter()
        formatter.dateFormat = "dd MMM yyyy hh:mm a"
        var timeString = Date()
        timeString = formatter.date(from: createdTime!)!
        formatter.dateFormat = "hh:mm a"
        let interval = Int((timeString.timeIntervalSinceNow)*(-1)/60)
        if (interval == 0) {
            timeText = "Just Now";
        } else if (interval > 0 && interval < 60) {
            minutesStr = interval == 1 ? "minute" : "minutes";
            timeText = String(format:"%u %@ ago", interval,minutesStr!);
        } else if (interval > 60 && interval < 24*60) {
            minutesStr = interval < 2*60 ? "hour" : "hours";
            timeText = String(format:"%u %@ ago", interval/60,minutesStr!);
        } else if (interval > 24*60 && interval < 7*24*60) {
            minutesStr = interval < 2*24*60 ? "day" : "days";
            timeText = String(format:"%u %@ ago", interval/60/24,minutesStr!);
        } else if (interval > 7*24*60) {
            minutesStr = interval < 2*7*24*60 ? "week" : "weeks";
            timeText = String(format:"%u %@ ago", interval/60/24/7,minutesStr!);
        }
        return timeText!
    }
}

class NotificationViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource {
    
    var notificationList = [NotificationData]()
    let table = UITableView()
    var onCloseBlock: actionBlock?
    var tableHeightConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        getNotifications()
        super.viewDidLoad()
        title = "Notification"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        table.delegate = self
        table.dataSource = self
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 40
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorColor = UIColor.clear
        table.register(NotificationCell.self, forCellReuseIdentifier: "notificationcell")
        self.view.addSubview(table)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views:["table":table]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-64-[table]|", options: [], metrics: nil, views:["table":table]))
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: {
            self.onCloseBlock?()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationList.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row%2 == 0){
            cell.backgroundColor = UIColor(red: 237.0/255.0, green: 237.0/255.0, blue: 237.0/255.0, alpha: 1)
        }
        else{
            cell.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationcell") as! NotificationCell
        cell.data = notificationList[indexPath.row]
        cell.onUserActionBlock = { () -> Void in
            print("notification id",self.notificationList[indexPath.row].notificationId!)
            self.deleteButtonPressed(self.notificationList[indexPath.row].notificationId!)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(notificationList[indexPath.row].notificationStatus != nil && notificationList[indexPath.row].notificationStatus != "Read") {
            let reqObj = UpdateNotificationRequest()
            reqObj.user_id = UserInfo.sharedInstance.userID
            reqObj.notification_id = notificationList[indexPath.row].notificationId
            //showIndicator("Getting Notifications..")
            NotificationRequestor().sendUpdateNotificationRequest(reqObj, success:{(success, object) in
                hideIndicator()
                if (object as! UpdateNotificationResponse).code == "13411" {
                    self.table.reloadData()
                }
            }) { (error) in
                hideIndicator()
            }
        }
        let notification_type = notificationList[indexPath.row].notificationType
        let notif_type = notificationList[indexPath.row].notifType
        if(notification_type == "Join"
            || notification_type == "Invitefriend"
            || notification_type == "Update"
            || notification_type == "extrarequestreject"
            || notification_type == "Delete"
            || notification_type == "Accept"
            || notification_type == "Reject"
            || notification_type == "offercanceled"
            || notification_type == "canceljoinrequest"
            || notification_type == "FR"
            || notification_type == "JR"
            || notification_type == "groupridecreate") {
            
            if(notif_type == "Join Request" && ((notificationList[indexPath.row].notificationMsg?.contains("likes to join"))! || (notificationList[indexPath.row].notificationMsg?.contains("offered to join"))!)) {
                showIndicator("Getting Ride Members..")
                RideRequestor().getRideMembers(notificationList[indexPath.row].poolId!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success){
                        let vc = MembersViewController()
                        vc.onSelectBackBlock = {
                            self.getNotifications()
                        }
                        vc.poolID = self.notificationList[indexPath.row].poolId
                        vc.membersList = (object as! RideMembersResponse).poolUsers
                        vc.hidesBottomBarWhenPushed = true
                        vc.edgesForExtendedLayout = UIRectEdge()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            } else {
                showIndicator("Getting Ride Details..")
                RideRequestor().getRideDetails(notificationList[indexPath.row].poolId!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success){
                        if (object as! RideDetail).poolID != nil {
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.isFirstLevel = true
                            vc.triggerFrom = "ride"
                            vc.data = object as? RideDetail
                            vc.onSelectBackBlock = {
                                self.getNotifications()
                            }
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                        else {
                            AlertController.showAlertFor("Ride", message: "Sorry! This Ride does not exist.", okButtonTitle: "Ok", okAction: {
                                let ins = UIApplication.shared.delegate as! AppDelegate
                                ins.gotoHome()
                            })
                        }
                    }
                }) { (error) in
                    hideIndicator()
                }
            }
        } else if(notification_type == "taxiinvitation"
            || notification_type == "booktaxi"
            || notification_type == "taxiconfirmbook"
            || notification_type == "taxiconfirmbookdriver"
            || notification_type == "taxiinvitationaccept"
            || notification_type == "taxiinvitationreject"
            || notification_type == "taxicancel"
            || notification_type == "taxijoin"
            || notification_type == "taxinewmemberjoin"
            || notification_type == "taxireminder") {
            showIndicator("Getting Ride Details..")
            RideRequestor().getTaxiDetails(notificationList[indexPath.row].poolId!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    if((object as! RideDetail).joinStatus != nil && (object as! RideDetail).joinStatus?.lowercased() == "u"){
                        AlertController.showAlertFor("Taxi Ride", message: "Sorry! This Ride does not exist.", okButtonTitle: "Ok", okAction: {
                            let ins = UIApplication.shared.delegate as! AppDelegate
                            ins.gotoHome()
                        })
                    } else {
                        let vc = TaxiDetailViewController()
                        vc.isFirstLevel = true
                        vc.data = object as? RideDetail
                        vc.onSelectBackBlock = {
                            self.getNotifications()
                        }
                        vc.hidesBottomBarWhenPushed = true
                        vc.edgesForExtendedLayout = UIRectEdge()
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }) { (error) in
                hideIndicator()
            }
        } else if(notification_type == "Friendrequestaccept"
            || notification_type == "Friendrequestreject"
            || notification_type == "newmemberjoin"
            || notification_type == "taxiunjoin"
            || notification_type == "taximemberunjoin"
            || notification_type == "memberunjoin"
            || notification_type == "Unjoin") {
            showIndicator("Getting Ride Members..")
            RideRequestor().getRideMembers(notificationList[indexPath.row].poolId!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    let vc = MembersViewController()
                    vc.onSelectBackBlock = {
                        self.getNotifications()
                    }
                    vc.poolID = self.notificationList[indexPath.row].poolId
                    vc.membersList = (object as! RideMembersResponse).poolUsers
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        } else if(notification_type == "group_add/invite") {
            showIndicator("Fetching Rides.")
            WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: notificationList[indexPath.row].groupId!, success: { (success, objectRides) in
                WorkplaceRequestor().getWPMembers("", groupID: self.notificationList[indexPath.row].groupId!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
                    hideIndicator()
                    let vc = WPRidesViewController()
                    vc.onSelectBackBlock = {
                        self.getNotifications()
                    }
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    vc.data = (object as! WPMembersResponseData).groupDetails
                    vc.dataSourceToOfc = (objectRides as! WPRidesResponse).pools?.officeGoing
                    vc.dataSourceToHome = (objectRides as! WPRidesResponse).pools?.homeGoing
                    self.navigationController?.pushViewController(vc, animated: true)
                }) { (error) in
                    hideIndicator()
                }
            }) { (error) in
                hideIndicator()
            }
        } else if(notification_type == "grouprequest_canceladmin" || notification_type == "groupmember_unjoinadmin" || notification_type == "group_joinadmin" || notification_type == "groupmember_removeadmin") {
            showIndicator("Fetching Workplace Detail.")
            WorkplaceRequestor().getWPMembers("", groupID: self.notificationList[indexPath.row].groupId!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
                hideIndicator()
                let vc = WPDetailViewController()
                vc.onSelectBackBlock = {
                    self.getNotifications()
                }
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = (object as! WPMembersResponseData).groupDetails
                self.navigationController?.pushViewController(vc, animated: true)
            }) { (error) in
                hideIndicator()
            }
        } else if(notification_type == "offers") {
            let reqObj = OffersPromotionsRequest()
            reqObj.user_id = UserInfo.sharedInstance.userID
            showIndicator("Getting Offers & Promotions..")
            OffersPromotionsRequestor().getOffersPromotions(reqObj, success:{(success, object) in
                hideIndicator()
                if (object as! OffersPromotionsResponse).code == "2999" {
                    let vc = OffersPromotionsViewController()
                    vc.onSelectBackBlock = {
                        self.getNotifications()
                    }
                    vc.offersPromotionsData = (object as! OffersPromotionsResponse).dataObj!
                    vc.comingFrom = "notification"
                    vc.hidesBottomBarWhenPushed = true
                    //vc.edgesForExtendedLayout = .None
                    self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                }
            }) { (error) in
                hideIndicator()
            }
        }
    }
    
    /*func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     return true
     }
     
     func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if (editingStyle == UITableViewCellEditingStyle.Delete) {
     let reqObj = DeleteNotificationRequest()
     reqObj.notification_id = notificationList[indexPath.row].notificationId
     showIndicator("Deleting..")
     NotificationRequestor().sendDeleteNotificationRequest(reqObj, success:{(success, object) in
     hideIndicator()
     if (object as! DeleteNotificationResponse).code == "1341" {
     self.getNotifications()
     }
     }) { (error) in
     hideIndicator()
     }
     
     }
     }*/
    func showEmptyView(_ msg: String)
    {
        self.table.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "ic_empty_cart", actionHandler: nil)
    }
    
    func getNotifications()
    {
        let reqObj = NotificationRequest()
        reqObj.user_id = UserInfo.sharedInstance.userID
        showIndicator("Getting Notifications..")
        NotificationRequestor().sendNotificationRequest(reqObj, success:{(success, object) in
            hideIndicator()
            if (object as! NotificationResponse).code == "4911" {
                self.notificationList = ((object as! NotificationResponse).dataObj)!
                self.table.reloadData()
            } else {
                self.tableHeightConstraint?.constant = 0
                self.showEmptyView("You do not have any notification right now")
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func deleteButtonPressed(_ notificationId: String) -> Void
    {
        let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteContact = UIAlertAction(title: "Delete", style: .default, handler: { (action) in
            let reqObj = DeleteNotificationRequest()
            reqObj.notification_id = notificationId
            showIndicator("Deleting..")
            NotificationRequestor().sendDeleteNotificationRequest(reqObj, success:{(success, object) in
                hideIndicator()
                if (object as! DeleteNotificationResponse).code == "1341" {
                    self.getNotifications()
                }
            }) { (error) in
                hideIndicator()
            }
        })
        vc.addAction(deleteContact)
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        })
        vc.addAction(actionCancel)
        
        present(vc, animated: true, completion: nil)
    }
    
}

