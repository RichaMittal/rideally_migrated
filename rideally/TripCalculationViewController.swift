//
//  TripCalculationViewController.swift
//  rideally
//
//  Created by Sarav on 10/11/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import FirebaseAnalytics
import AddressBook
import AddressBookUI
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class TripCalculationViewController: UIViewController, CLLocationManagerDelegate,ABPeoplePickerNavigationControllerDelegate {
    
    let btnStop = UIButton()
    var data: RideDetail?
    var isSOSButtonPressed = false
    var addressBookController = ABPeoplePickerNavigationController()
    var onSelectBackBlock: actionBlock?
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
        onSelectBackBlock?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Trip Calculation"
        self.view.backgroundColor = Colors.APP_BG
        // Do any additional setup after loading the view.
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;
        
        let imgBG = UIImageView()
        imgBG.backgroundColor = Colors.RED_COLOR
        imgBG.translatesAutoresizingMaskIntoConstraints = false
        imgBG.image = UIImage(named: "tripBG")
        imgBG.isUserInteractionEnabled = true
        self.view.addSubview(imgBG)
        
        let btnSOS = UIButton()
        btnSOS.translatesAutoresizingMaskIntoConstraints = false
        btnSOS.setImage(UIImage(named: "sos"), for: UIControlState())
        btnSOS.addTarget(self, action: #selector(btnSOS_clicked), for: .touchDown)
        imgBG.addSubview(btnSOS)
        
        imgBG.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[btnsos(30)]-15-|", options: [], metrics: nil, views: ["btnsos":btnSOS]))
        imgBG.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[btnsos(30)]", options: [], metrics: nil, views: ["btnsos":btnSOS]))
        
        btnStop.translatesAutoresizingMaskIntoConstraints = false
        btnStop.setTitle("STOP RIDE", for: UIControlState())
        btnStop.backgroundColor = Colors.BTN_BG_COLOR
        btnStop.addTarget(self, action: #selector(stopRide), for: .touchDown)
        self.view.addSubview(btnStop)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[img][btn(40)]|", options: [], metrics: nil, views: ["btn":btnStop, "img":imgBG]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btnStop]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[img]|", options: [], metrics: nil, views: ["img":imgBG]))
    }
    
    var tripStopped = false
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        manager.stopUpdatingLocation()
        
        hideIndicator()
        
        if  (isSOSButtonPressed == true)
        {
            self.tripStopped = true
            isSOSButtonPressed = false
            let geocoder = GMSGeocoder()
            
            geocoder.reverseGeocodeCoordinate(locValue) { [weak self] response , error in
                
                if let address = response?.firstResult() {
                    
                    let lines = address.lines as [String]!
                    self?.sendSOSRequest("\(address.coordinate.latitude)","\(address.coordinate.longitude)",(lines?.joined(separator: ","))!)
                }
                
            }
        }
        
        if(tripStopped == false){
            tripStopped = true
            let reqObj = TripRequest()
            
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(locValue) { [weak self] response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines as [String]!
                    
                    reqObj.userID = UserInfo.sharedInstance.userID
                    reqObj.poolID = self?.data?.poolID
                    
                    reqObj.mobileNumber = UserInfo.sharedInstance.mobile
                    reqObj.distance = ""
                    
                    reqObj.startLocation = self?.data?.source
                    reqObj.stopLocation = lines?.joined(separator: ",")
                    reqObj.userName = UserInfo.sharedInstance.fullName
                    reqObj.deviceID = ""
                    reqObj.eta = ""
                    reqObj.userLocation = lines?.joined(separator: ",")
                    reqObj.toLocation = lines?.joined(separator: ",")
                    reqObj.toLat = "\(locValue.latitude)"
                    reqObj.toLong = "\(locValue.longitude)"
                    
                    reqObj.driverMobile = self?.data?.ownerPhone
                    reqObj.driverDeviceID = ""
                    reqObj.driverUserID = self?.data?.vehicleOwnerID
                    reqObj.duration = ""
                    reqObj.points = ""
                    
                    CommonRequestor().getDistanceDuration(reqObj.startLocation!, destination: reqObj.stopLocation!, success: { (success, object) in
                        if let data = object as? NSDictionary{
                            let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                            let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                            reqObj.duration = ""
                            reqObj.distance = ""
                            reqObj.eta = ""
                            reqObj.points = ""
                                if((elements["status"] as! String) == "OK")
                                {
                                    let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                let distanceMtrs = (((elements["distance"] as! NSDictionary)["value"]) as! Int)/1000
                                let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                reqObj.duration = duration as? String
                                reqObj.eta = duration as? String
                                reqObj.distance = distance as? String
                                reqObj.points = "\(distanceMtrs * Int((self!.data!.cost!))!)"
                            }
                        }
                        self!.stopRideRequest(reqObj)
                        
                        }, failure: { (error) in
                            self!.stopRideRequest(reqObj)
                    })
                }
            }
        }
    }
    
    func stopRideRequest(_ reqObj: TripRequest)
    {
        if(self.data?.vehicleOwnerID == UserInfo.sharedInstance.userID){
            if(self.data?.vehicleOwnerID == self.data?.createdBy) {
                reqObj.poolOwnerType = "admin"
            } else {
                reqObj.poolOwnerType = ""
            }
            WorkplaceRequestor().stopRideByDriver(reqObj, success: { (success, object) in
                let vc = TripDetailsViewController()
                vc.startLocation = reqObj.startLocation
                vc.stopLocation = reqObj.stopLocation
                vc.points = reqObj.points
                vc.distance = reqObj.distance
                vc.poolId = reqObj.poolID
                self.navigationController?.pushViewController(vc, animated: true)
                }, failure: { (error) in
            })
        }
        else{
            WorkplaceRequestor().stopRideByRider(reqObj, success: { (success, object) in
                let vc = TripDetailsViewController()
                vc.startLocation = reqObj.startLocation
                vc.stopLocation = reqObj.stopLocation
                vc.points = reqObj.points
                vc.distance = reqObj.distance
                vc.poolId = reqObj.poolID
                self.navigationController?.pushViewController(vc, animated: true)
                }, failure: { (error) in
            })
        }
    }
    
    let locationManager = CLLocationManager()
    func stopRide()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Start Ride Activity",
            AnalyticsParameterItemName: "Popup",
            AnalyticsParameterContentType:"End Ride"
            ])
        self.locationManager.requestWhenInUseAuthorization()
        
        AlertController.showAlertFor("Stop Ride", message: "Would you really like to stop the ride?", okButtonTitle: "STOP", okAction: {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Start Ride Activity",
                AnalyticsParameterItemName: "Ok Button",
                AnalyticsParameterContentType:"End Ride"
                ])
            showIndicator("Fetching current location..")
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                self.tripStopped = false
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                self.locationManager.startUpdatingLocation()
            }
            else{
                hideIndicator()
                AlertController.showAlertFor("Enable Location Services", message: "Enable location services from Settings > Privacy for the application to proceed.")
            }
            
        }, cancelButtonTitle: "Cancel") {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Start Ride Activity",
                AnalyticsParameterItemName: "Cancel Button",
                AnalyticsParameterContentType:"End Ride"
                ])
        }
    }
    
    func btnSOS_clicked() {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            if(data?.vehicleOwnerID == nil) {
                AlertController.showAlertFor("SOS", message: "It will be enabled when ride is having vehicle.", okAction: {})
            } else {
                if  ((data?.poolGroups?.count > 0 && data?.poolGroups?.first?.helpNumber != nil && data?.poolGroups?.first?.helpNumber != "") || (UserInfo.sharedInstance.selectedEmergencyNumber.characters.count > 0))
                {
                    self.isSOSButtonPressed = true
                    self.locationManager.requestWhenInUseAuthorization()
                    
                    if CLLocationManager.locationServicesEnabled()
                    {
                        self.locationManager.delegate = self
                        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                        self.locationManager.startUpdatingLocation()
                    }
                }
                else
                {
                    callAddressBookForPressedButtonObject()
                }
            }
        }
    }
    
    func sendSOSRequest(_ value :String...) -> Void
    {
        showIndicator("Sending Email & Sms...")
        let reqObject =  GetRideDetailsRequest()
        
        reqObject.userID = UserInfo.sharedInstance.userID
        reqObject.userName = UserInfo.sharedInstance.fullName
        reqObject.userMobile = UserInfo.sharedInstance.mobile
        reqObject.gender = UserInfo.sharedInstance.gender == "M" ? "His" : "Her"
        reqObject.currentLocation = value[2]
        
        
        reqObject.vehicleNumber = ""
        if  let regNo = data?.vehicleRegNo
        {
            reqObject.vehicleNumber = regNo
        }
        
        reqObject.vehicleModel = ""
        if  let regNo = data?.vehicleModel
        {
            reqObject.vehicleModel = regNo
        }
        
        reqObject.vehicleType = ""
        if  let regNo = data?.vehicleType
        {
            reqObject.vehicleType = regNo
        }
        
        let poolID : NSString!
        poolID = data?.poolID as NSString!
        if(data?.poolGroups?.count > 0 && data?.poolGroups?.first?.helpNumber != nil && data?.poolGroups?.first?.helpNumber != "") {
            reqObject.helpMobile = data?.poolGroups?.first?.helpNumber
            reqObject.helpMail = data?.poolGroups?.first?.helpEmail
        } else {
            reqObject.helpMobile = ""
            reqObject.helpMail = ""
        }
        reqObject.sourceLocation = data?.source
        reqObject.destinationLocation =  data?.destination
        reqObject.url = SOS_USERLOC+"?pool_id=\(poolID)&lat=\(value[0])&long=\(value[1])"
        reqObject.latitude = value[0]
        reqObject.longtitude = value[1]
        
        RideRequestor().getSOSForRideDetail(reqObject, success:{ (success, object) in
            hideIndicator()
            if(success == true) {
                AlertController.showAlertFor("Alert", message: "RideAlly has sent Email & Sms to your SOS contacts successfully.\n Would you like to initiate emergency call?", okButtonTitle: "YES", okAction: {
                    var contactNo = ""
                    if(self.data?.poolGroups?.count > 0 && self.data?.poolGroups?.first?.helpNumber != nil && self.data?.poolGroups?.first?.helpNumber != "") {
                        contactNo = (self.data?.poolGroups?.first?.helpNumber)!
                    } else {
                        contactNo = UserInfo.sharedInstance.selectedEmergencyNumber
                    }
                    if let url = URL(string: "tel://\(contactNo)"), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(url)
                    }
                }, cancelButtonTitle: "Later") {
                }
            } else {
                AlertController.showAlertForMessage((object as AnyObject).message)
            }
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            self.addressBookController.peoplePickerDelegate = self
            self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                "phoneNumbers.@count > 0", argumentArray: nil)
            self.present(self.addressBookController, animated: true, completion: nil)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
    }
    
    func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController)
    {
        
        self.addressBookController.dismiss(animated: true, completion: nil)
    }
    
    var fullName = ""
    var contactNumber = ""
    var emailId = ""
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord)
    {
        if let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String
        {
            fullName = first
            
            if let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String
            {
                fullName = first+last
            }
        }
        
        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
        let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
        if(phones != "")
        {
            if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
            {
                if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                {
                    let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                    var personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                    let areaCode = personNumber.characters.count-10
                    let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                    if(areaCode > 0) {
                        personNumber = personNumber.substring(from: startIndex)
                    }
                    contactNumber = personNumber
                }
            }
        }
        
        let unmanagedPhones1 = ABRecordCopyValue(person, kABPersonEmailProperty)
        let phones1 = Unmanaged.fromOpaque((unmanagedPhones1?.toOpaque())!).takeUnretainedValue() as NSString
        if(phones1 != "")
        {
            if let unmanagedPhone1 = ABMultiValueCopyValueAtIndex(phones1, 0)
            {
                if  let phone1: String = Unmanaged.fromOpaque(unmanagedPhone1.toOpaque()).takeUnretainedValue() as NSObject as? String
                {
                    emailId = phone1
                }
            } else {
                emailId = ""
            }
        }
        if(contactNumber != "") {
            sendSaveContactListRequest()
        }
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    //print("Just authorized")
                    self.addressBookController.peoplePickerDelegate = self
                    self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                        "phoneNumbers.@count > 0", argumentArray: nil)
                    self.present(self.addressBookController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func sendSaveContactListRequest() -> Void
    {
        let reqObj = SaveEmergencyContactRequest()
        showIndicator("Loading...")
        
        reqObj.userID = UserInfo.sharedInstance.userID
        
        reqObj.firstPersonName = ""
        if fullName != ""
        {
            reqObj.firstPersonName = fullName
        }
        
        reqObj.secondPersonName = nil
        reqObj.firstPersonNumber = ""
        if contactNumber != ""
        {
            let stringArray = contactNumber.components(separatedBy: CharacterSet.decimalDigits.inverted)
            var firstPersonNumber = NSArray(array: stringArray).componentsJoined(by: "")
            let areaCode = firstPersonNumber.characters.count-10
            let startIndex = firstPersonNumber.characters.index(firstPersonNumber.startIndex, offsetBy: areaCode)
            if(areaCode > 0) {
                firstPersonNumber = firstPersonNumber.substring(from: startIndex)
            }
            reqObj.firstPersonNumber = firstPersonNumber
        }
        
        reqObj.secondPersonNumber = nil
        reqObj.firstPersonEmail = ""
        if  emailId != ""
        {
            reqObj.firstPersonEmail = emailId
        }
        
        reqObj.secondPersonEmail = ""
        reqObj.defaultPerson = "1"
        
        EmergencyContactRequestor().sendSaveContactListRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! SaveEmergencyContactResponse).code == "4600"
            {
                UserInfo.sharedInstance.selectedEmergencyNumber = reqObj.firstPersonNumber!
                UserInfo.sharedInstance.selectedEmergencyEmail = reqObj.firstPersonEmail!
                self.btnSOS_clicked()
            }
            else
            {
                AlertController.showToastForError("Record Not Found.")
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
}
