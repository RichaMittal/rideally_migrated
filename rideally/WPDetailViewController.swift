//
//  WPDetailViewController.swift
//  rideally
//
//  Created by Sarav on 28/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI

class WPDetailViewController: UIViewController, ABPeoplePickerNavigationControllerDelegate {
    
    var data: WorkPlace?
    var domainsList = [WPDomainsResponseData]()
    var onBtnActionBlock: actionBlock?
    let profilePic = UIImageView()
    var onSelectBackBlock: actionBlock?
    var wpresponses: [InviteWPResult]?
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    
    func showPlaceholderPic()
    {
        if(data?.wpScope == "1" || data?.wpTieUpStatus == "1"){
            profilePic.image = UIImage(named: "wpPlaceholderL")
        }
        else{
            profilePic.image = UIImage(named: "wpPlaceholder")
        }
        let url = URL(string:SERVER+"static/groups/"+(data?.wpID)!+".jpg")
        profilePic.kf.setImage(with: url, placeholder: profilePic.image, options: nil, progressBlock: nil, completionHandler: nil)
        //profilePic.kf_setImageWithURL(url, placeholderImage: profilePic.image, optionsInfo: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
        onSelectBackBlock?()
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.WHITE_COLOR
        title = "Workplace Details"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        // NSNotificationCenter.defaultCenter().removeObserver(self, name:"KInviteSendNotification",object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotification"), object: nil)
        
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        
        if let url = data?.wpLogoURL{
            if(url != ""){
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
            }
            else{
                showPlaceholderPic()
            }
        }
        else{
            showPlaceholderPic()
        }
        self.view.addSubview(profilePic)
        
        let lblWPname = UILabel()
        lblWPname.translatesAutoresizingMaskIntoConstraints = false
        lblWPname.text = data?.wpName
        if(data?.wpScope == "1" || data?.wpTieUpStatus == "1"){
            lblWPname.text = "\((data?.wpName)!)(Private)"
        }
        lblWPname.font = boldFontWithSize(15)
        lblWPname.textColor = Colors.GRAY_COLOR
        lblWPname.textAlignment = .center
        self.view.addSubview(lblWPname)
        
        let lblLine1 = getLineLabel()
        self.view.addSubview(lblLine1)
        
        let locView = UIView()
        locView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(locView)
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        locView.addSubview(iconSource)
        let lblLocation = UILabel()
        lblLocation.textColor = Colors.GRAY_COLOR
        lblLocation.text = data?.wpLocation != nil ? data?.wpLocation : data?.wpDetailLocation
        lblLocation.font = boldFontWithSize(14)
        lblLocation.numberOfLines = 0
        lblLocation.lineBreakMode = .byWordWrapping
        lblLocation.translatesAutoresizingMaskIntoConstraints = false
        locView.addSubview(lblLocation)
        let iconMap = UIImageView()
        iconMap.image = UIImage(named: "map")
        iconMap.translatesAutoresizingMaskIntoConstraints = false
        locView.addSubview(iconMap)
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(showDirection))
        iconMap.isUserInteractionEnabled = true
        iconMap.addGestureRecognizer(tapGesture1)
        
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[iS(15)]-5-[lbl]-5-[iM(30)]|", options: [], metrics: nil, views: ["iS":iconSource, "lbl":lblLocation, "iM":iconMap]))
        
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[iS(15)]", options: [], metrics: nil, views: ["iS":iconSource, "lbl":lblLocation, "iM":iconMap]))
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[iM(30)]", options: [], metrics: nil, views: ["iS":iconSource, "lbl":lblLocation, "iM":iconMap]))
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["iS":
            iconSource, "lbl":lblLocation, "iM":iconMap]))
        locView.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblLocation, attribute: .centerY, multiplier: 1, constant: 0))
        
        
        let lblLine2 = getLineLabel()
        self.view.addSubview(lblLine2)
        
        let urlView = UIView()
        urlView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(urlView)
        
        let lblURLTitle = UILabel()
        lblURLTitle.text = "WORKPLACE URL"
        lblURLTitle.font = boldFontWithSize(13)
        lblURLTitle.textColor = Colors.GRAY_COLOR
        lblURLTitle.translatesAutoresizingMaskIntoConstraints = false
        urlView.addSubview(lblURLTitle)
        
        let lblWPURL = UILabel()
        lblWPURL.textColor = Colors.GRAY_COLOR
        lblWPURL.font = boldFontWithSize(15)
        lblWPURL.text = WORKPLACE_URL_BASE + (data?.groupKey)!
        lblWPURL.translatesAutoresizingMaskIntoConstraints = false
        urlView.addSubview(lblWPURL)
        urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title(20)][lbl(20)]|", options: [], metrics: nil, views: ["title":lblURLTitle, "lbl":lblWPURL]))
        urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: ["title":lblURLTitle, "lbl":lblWPURL]))
        urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["title":lblURLTitle, "lbl":lblWPURL]))
        
        
        let lblLine6 = getLineLabel()
        self.view.addSubview(lblLine6)
        
        let descView = UIView()
        descView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(descView)
        let lblDesctitle = UILabel()
        lblDesctitle.translatesAutoresizingMaskIntoConstraints = false
        lblDesctitle.text = "WORKPLACE DESCRIPTION"
        lblDesctitle.font = boldFontWithSize(13)
        lblDesctitle.textColor = Colors.GRAY_COLOR
        descView.addSubview(lblDesctitle)
        let lblDesc = UILabel()
        lblDesc.translatesAutoresizingMaskIntoConstraints = false
        lblDesc.text = data?.wpDesc
        lblDesc.font = boldFontWithSize(14)
        lblDesc.textColor = Colors.BLACK_COLOR
        descView.addSubview(lblDesc)
        descView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title(25)][desc]|", options: [], metrics: nil, views: ["title":lblDesctitle, "desc":lblDesc]))
        descView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: ["title":lblDesctitle]))
        descView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[desc]|", options: [], metrics: nil, views: ["desc":lblDesc]))
        
        if(data?.wpDesc != "") {
            descView.isHidden = false
        } else {
            descView.isHidden = true
        }
        let lblLine3 = getLineLabel()
        self.view.addSubview(lblLine3)
        
        let btnMembers = UIButton()
        btnMembers.setTitle("VIEW MEMBER(S)", for: UIControlState())
        btnMembers.translatesAutoresizingMaskIntoConstraints = false
        btnMembers.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnMembers.titleLabel?.font = boldFontWithSize(14)
        btnMembers.contentHorizontalAlignment = .left
        btnMembers.addTarget(self, action: #selector(showMembers), for: .touchDown)
        self.view.addSubview(btnMembers)
        
        let lblLine4 = getLineLabel()
        self.view.addSubview(lblLine4)
        
        let btnRides = UIButton()
        btnRides.setTitle("SHOW RIDE(S)", for: UIControlState())
        btnRides.translatesAutoresizingMaskIntoConstraints = false
        btnRides.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnRides.titleLabel?.font = boldFontWithSize(14)
        btnRides.contentHorizontalAlignment = .left
        btnRides.addTarget(self, action: #selector(showRides), for: .touchDown)
        self.view.addSubview(btnRides)
        
        let lblLine5 = getLineLabel()
        self.view.addSubview(lblLine5)
        
        let viewsDict = ["imgV":profilePic, "lblname":lblWPname, "l1":lblLine1, "locV":locView, "l2":lblLine2, "urlV":urlView, "l6":lblLine6, "descV":descView, "l3":lblLine3, "btnMem":btnMembers, "l4":lblLine4, "btnRides":btnRides, "l5":lblLine5]
        if(data?.wpDesc != "") {
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[imgV(100)]-10-[lblname(30)]-10-[l1(0.5)]-5-[locV(40)]-5-[l2(0.5)]-5-[urlV(50)]-5-[l6(0.5)]-5-[descV(50)]-5-[l3(0.5)]-5-[btnMem(30)]-5-[l4(0.5)]-5-[btnRides(30)]-5-[l5(0.5)]", options: [], metrics: nil, views: viewsDict))
        } else {
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[imgV(100)]-10-[lblname(30)]-10-[l1(0.5)]-5-[locV(40)]-5-[l2(0.5)]-5-[urlV(50)]-5-[l6(0.5)]-5-[btnMem(30)]-5-[l4(0.5)]-5-[btnRides(30)]-5-[l5(0.5)]", options: [], metrics: nil, views: viewsDict))
        }
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imgV(100)]", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblname]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraint(NSLayoutConstraint(item: profilePic, attribute: .centerX, relatedBy: .equal, toItem: lblWPname, attribute: .centerX, multiplier: 1, constant: 0))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l1]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[locV]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l2]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[urlV]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l6]|", options: [], metrics: nil, views: viewsDict))
        if(data?.wpDesc != "") {
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[descV]-5-|", options: [], metrics: nil, views: viewsDict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l3]|", options: [], metrics: nil, views: viewsDict))
        }
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[btnMem]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l4]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[btnRides]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l5]|", options: [], metrics: nil, views: viewsDict))
        
        
        let btnView = getBtnView()
        btnView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(btnView)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btns]|", options: [], metrics: nil, views: ["btns":btnView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btns(40)]|", options: [], metrics: nil, views: ["btns":btnView]))
        
    }
    
    func showDirection()
    {
        let vc = WPMapViewController()
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.data = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showMembers()
    {
        let wpMembershipStatus = data!.wpMembershipStatus != nil ? data!.wpMembershipStatus : data!.wpDetailMembershipStatus
        if((data!.wpScope == "1" || data!.wpTieUpStatus == "1") && (wpMembershipStatus != "1")){
            AlertController.showAlertFor("Workplace Members", message: "You may need to join the workplace to see members list.", okButtonTitle: "Ok", okAction: nil)
        } else{
            showIndicator("Fetching members.")
            WorkplaceRequestor().getWPMembers("", groupID: data!.wpID!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
                hideIndicator()
                let vc = WPMembersViewController()
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.wpDetails = (object as! WPMembersResponseData).groupDetails
                vc.membersList = (object as! WPMembersResponseData).members
                self.navigationController?.pushViewController(vc, animated: true)
                }, failure: { (error) in
                    hideIndicator()
            })
        }
    }
    
    func showRides()
    {
        let wpMembershipStatus = data!.wpMembershipStatus != nil ? data!.wpMembershipStatus : data!.wpDetailMembershipStatus
        if((data!.wpScope == "1" || data!.wpTieUpStatus == "1") && (wpMembershipStatus != "1")){
            AlertController.showAlertFor("Workplace Members", message: "You may need to join the workplace to see rides list.", okButtonTitle: "Ok", okAction: nil)
        } else {
            showIndicator("Fetching Rides.")
            WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: data!.wpID!, success: { (success, object) in
                
                hideIndicator()
                let vc = WPRidesViewController()
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = self.data
                vc.dataSourceToOfc = (object as! WPRidesResponse).pools?.officeGoing
                vc.dataSourceToHome = (object as! WPRidesResponse).pools?.homeGoing
                self.navigationController?.pushViewController(vc, animated: true)
                
            }) { (error) in
                hideIndicator()
            }
        }
    }
    
    func getLineLabel() -> UILabel
    {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.backgroundColor = Colors.GENERAL_BORDER_COLOR
        return lbl
    }
    
    var btnConstraints: [NSLayoutConstraint]?
    
    func setUpBtn(_ btn: UIButton, imageName: String, title: String, parent: UIView)
    {
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Colors.GRAY_COLOR
        //btn.setImage(UIImage(named: imageName), forState: .Normal)
        btn.setTitle(title, for: UIControlState())
        btn.isHidden = true
        parent.addSubview(btn)
        
        parent.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn]|", options: [], metrics: nil, views: ["btn":btn]))
    }
    
    let btnInvite = UIButton()
    let btnCancel = UIButton()
    let btnJoin = UIButton()
    let btnUnjoin = UIButton()
    let btnUpdate = UIButton()
    func getBtnView() -> UIView
    {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        setUpBtn(btnInvite, imageName: "invite", title:"Invite", parent: view)
        btnInvite.addTarget(self, action: #selector(btnInvite_clicked), for: .touchDown)
        
        setUpBtn(btnUpdate, imageName: "edit", title:"Update", parent: view)
        btnUpdate.addTarget(self, action: #selector(btnEdit_clicked), for: .touchDown)
        
        setUpBtn(btnCancel, imageName: "cancel", title:"Cancel", parent: view)
        btnCancel.addTarget(self, action: #selector(btnCancel_clicked), for: .touchDown)
        
        setUpBtn(btnJoin, imageName: "join", title:"Join", parent: view)
        btnJoin.addTarget(self, action: #selector(btnJoin_clicked), for: .touchDown)
        
        setUpBtn(btnUnjoin, imageName: "unjoin", title:"Unjoin", parent: view)
        btnUnjoin.addTarget(self, action: #selector(btnUnjoin_clicked), for: .touchDown)
        
        setVisibleBtns(view)
        
        return view
    }
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            contactListArray.removeAllObjects()
            mobileMutArr.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject]{
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = (data?.wpID)!
            vc.triggerFrom = "wpDetail"
            vc.isWp = true
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    self.contactListArray.removeAllObjects()
                    self.mobileMutArr.removeAllObjects()
                    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                    for record:ABRecord in contactList as [AnyObject]{
                        let contactPerson: ABRecord = record
                        let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                        if(contactName != "") {
                            var personNumber = ""
                            let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                            if(phones != "")
                            {
                                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                {
                                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                    {
                                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                        personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                        let areaCode = personNumber.characters.count-10
                                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                        if(areaCode > 0) {
                                            personNumber = personNumber.substring(from: startIndex)
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            self.mobileMutArr.add(personNumber)
                            let nameString = "\(contactName) \(personNumber)"
                            self.contactListArray.add(nameString)
                        }
                    }
                    
                    let vc = ContactsListViewController()
                    vc.contactListArr = self.contactListArray
                    vc.mobileMutArr = self.mobileMutArr
                    vc.poolID = (self.data?.wpID)!
                    vc.triggerFrom = "wpDetail"
                    vc.isWp = true
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        //        AlertController.showAlertFor("Cannot Add Contact", message: "You must give the app permission to add the contact first.", okButtonTitle: "Settings", okAction: {
        //            self.openSettings()
        //            }, cancelButtonTitle: "OK", cancelAction: nil)
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObject()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg((data?.wpName)!, wpKey: (data?.groupKey)!, poolID: (data?.wpID)!)
    }
    
    func btnInvite_clicked()
    {
        let inviteView = InviteNormalRideView()
        let alertViewHolder =   AlertContentViewHolder()
        alertViewHolder.heightConstraintValue = 95
        alertViewHolder.view = inviteView
        
        inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
        inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
        
        AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
    }

    func btnEdit_clicked()
    {
        //TODO:Admin can update only 2 fields (Wp Url & Wp description). Please find respective Api and Params on following location..
        let vc = EditWPViewController()
        vc.data = self.data
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnCancel_clicked()
    {
        showIndicator("Cancelling request.")
        WorkplaceRequestor().cancelJoinWorkPlace(data!.wpID!, success: { (success, object) in
            hideIndicator()
            if(success){
                AlertController.showAlertFor("Cancel Request", message: "You have cancelled the request successfully.", okButtonTitle: "Ok", okAction: {
                    _ = self.navigationController?.popToRootViewController(animated: true)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "updateWpsList"), object: nil)
                    self.onBtnActionBlock?()
                })
            }
            else{
                AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: nil)
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    let txtEmailID = RATextField()
    func btnJoin_clicked()
    {
        //if(data!.wpScope == "1"){
            if(data!.wpTieUpStatus == "1"){
                if(UserInfo.sharedInstance.email != ""){
                    WorkplaceRequestor().getWPDomains(data!.wpID!, success: { (success, object) in
                        if(success){
                            self.domainsList = object as! [WPDomainsResponseData]
                            let mySecDomain = UserInfo.sharedInstance.secEmail.components(separatedBy: "@").last
                            let myPrimDomain = UserInfo.sharedInstance.email.components(separatedBy: "@").last
                            var domainMatch = false
                            for domain in self.domainsList{
                                if(domain.domainName == myPrimDomain || domain.domainName == mySecDomain){
                                    domainMatch = true
                                }
                            }
                            if(domainMatch == true){
                                //check if email is verified else show popup
                                if(UserInfo.sharedInstance.isEmailIDVerified == true || UserInfo.sharedInstance.isSecEmailIDVerified == true){
                                    self.sendJoinWPrequest(self.data!)
                                }
                                else{
                                    self.getSecEmail(self.data!,flag: true)
                                }
                                
                            } else{
                                self.getSecEmail(self.data!,flag: false)
                            }
                        }
                        }, failure: { (error) in
                            
                    })
                }
                else{
                    self.getSecEmail(self.data!,flag: false)
                }
            } else {
                self.sendJoinWPrequest(self.data!)
            }
//        }
//        else if(data!.wpScope == "0"){
//            self.sendJoinWPrequest(self.data!)
//        }
    }
    
    func isValidEmailFormate(_ stringValue:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: stringValue)
    }
    
    func getSecEmail(_ selectedWPdata: WorkPlace, flag: Bool)
    {
        var okButtonTitle = ""
        let viewHolder = AlertContentViewHolder()
        viewHolder.heightConstraintValue = 95
        
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        self.txtEmailID.translatesAutoresizingMaskIntoConstraints = false
        self.txtEmailID.placeholder = "ENTER COMPANY EMAIL ID"
        self.txtEmailID.font = normalFontWithSize(14)
        self.txtEmailID.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        view.addSubview(self.txtEmailID)
        let lblInfo = UILabel()
        if(flag == true) {
            if(UserInfo.sharedInstance.secEmail != "") {
                self.txtEmailID.text = UserInfo.sharedInstance.secEmail
            } else {
                self.txtEmailID.text = UserInfo.sharedInstance.email
            }
            lblInfo.text = "We are waiting for your email verification. If you didnt receive email yet, you can click on resend verification link below."
            okButtonTitle = "Resend Link"
        } else {
            lblInfo.text = "You may need to provide official email ID to access this workplace. Please provide email id to proceed further."
            okButtonTitle = "Submit"
        }
        lblInfo.numberOfLines = 0
        lblInfo.lineBreakMode = .byWordWrapping
        lblInfo.translatesAutoresizingMaskIntoConstraints = false
        lblInfo.font = normalFontWithSize(12)
        view.addSubview(lblInfo)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[lbl]-10-[txt(30)]|", options: [], metrics: nil, views: ["txt":self.txtEmailID, "lbl":lblInfo]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txt]-10-|", options: [], metrics: nil, views: ["txt":self.txtEmailID, "lbl":lblInfo]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["txt":self.txtEmailID, "lbl":lblInfo]))
        
        viewHolder.view = view
        
        AlertController.showAlertFor("Email verification", message: "", contentView: viewHolder, okButtonTitle: okButtonTitle, willHaveAutoDismiss: false, okAction: {
            if(self.txtEmailID.text?.characters.count == 0) {
                AlertController.showToastForError("Please provide 'Company Email Id'.")
            } else if(self.txtEmailID.text?.characters.count != 0 && !self.isValidEmailFormate(self.txtEmailID.text!))
            {
                AlertController.showToastForError("Please enter valid 'Company Email Id'.")
            } else {
                let myDomain = self.txtEmailID.text!.components(separatedBy: "@").last
                
                var domainMatch = false
                for domain in self.domainsList{
                    if(domain.domainName == myDomain){
                        domainMatch = true
                    }
                }
                if(domainMatch == true){
                    if(self.txtEmailID.text == UserInfo.sharedInstance.email) {
                        self.dismiss(animated: true, completion: {
                            showIndicator("Sending verification link...")
                            WorkplaceRequestor().addSecEmail(UserInfo.sharedInstance.userID, email: self.txtEmailID.text!, groupKey:selectedWPdata.groupKey!, groupID: selectedWPdata.wpID!, success: { (success, object) in
                                hideIndicator()
                                if(success == true) {
                                    UserInfo.sharedInstance.secEmail = self.txtEmailID.text!
                                    AlertController.showAlertFor("Email verification", message: "We have sent you a verification link to your email id (\(self.txtEmailID.text!)). Please verify it to proceed further.", okAction: {
                                        self.txtEmailID.text = ""
                                    })
                                } else {
                                    AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: {
                                        self.txtEmailID.text = ""
                                    })
                                }
                                }, failure: { (error) in
                                    hideIndicator()
                            })
                        })
                    } else {
                        let reqObj = emailIDRequestModel()
                        reqObj.user_email = self.txtEmailID.text
                        reqObj.user_id = UserInfo.sharedInstance.userID
                        showIndicator("Loading...")
                        SignUpRequestor().isEmailIdalreadyexist(reqObj, success:{ (success, object) in
                            
                            hideIndicator()
                            
                            if (object as! emailIDResponseModel).code == "1454"
                            {
                                self.dismiss(animated: true, completion: {
                                    showIndicator("Sending verification link...")
                                    WorkplaceRequestor().addSecEmail(UserInfo.sharedInstance.userID, email: self.txtEmailID.text!, groupKey:selectedWPdata.groupKey!, groupID: selectedWPdata.wpID!, success: { (success, object) in
                                        hideIndicator()
                                        if(success == true) {
                                            UserInfo.sharedInstance.secEmail = self.txtEmailID.text!
                                            AlertController.showAlertFor("Email verification", message: "We have sent you a verification link to your email id (\(self.txtEmailID.text!)). Please verify it to proceed further.", okAction: {
                                                self.txtEmailID.text = ""
                                            })
                                        } else {
                                            AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: {
                                                self.txtEmailID.text = ""
                                            })
                                        }
                                        }, failure: { (error) in
                                            hideIndicator()
                                    })
                                })
                            }
                            else
                            {
                                AlertController.showToastForError("This 'Email Id' already exists.")
                            }
                            
                        }){ (error) in
                            hideIndicator()
                            AlertController.showAlertForError(error)
                        }
                    }
                } else {
                    self.dismiss(animated: true, completion: {
                        AlertController.showAlertFor("Error", message: "Sorry, we could not authorize your \(selectedWPdata.wpName!) email id. Please check your email id (\(self.txtEmailID.text!)) again or register again with correct email id or contact customer care at 080 4600 4600.", okButtonTitle: "Ok", okAction: {
                            self.txtEmailID.text = ""
                        })
                    })
                }
            }
            }, cancelButtonTitle: "Cancel", cancelAction: {
                self.dismiss(animated: true, completion: nil)
                self.txtEmailID.text = ""
        })
    }
    
    func sendJoinWPrequest(_ selectedWPdata: WorkPlace)
    {
        let tieup_wpuser: String?
        if(selectedWPdata.wpTieUpStatus == "1") {
            tieup_wpuser = "1"
        } else {
            tieup_wpuser = ""
        }
        showIndicator("Sending request to Join Workplace.")
        WorkplaceRequestor().joinWorkPlace(selectedWPdata.wpID!, groupScope: selectedWPdata.wpScope!, userMsg: "",tieupWpUser: tieup_wpuser!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Join Workplace", message: object as? String, okButtonTitle: "Ok", okAction: {
                    if(selectedWPdata.wpScope == "0" || selectedWPdata.wpTieUpStatus == "1") {
                        _ = self.navigationController?.popViewController(animated: true)
//                        showIndicator("Fetching Rides.")
//                        WorkplaceRequestor().getWPRides(prettyDateStringFromDate(NSDate(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(NSDate(), toFormat: "hh:mm a"), groupID: (self.data?.wpID)!, success: { (success, objectRides) in
//                            hideIndicator()
//                            
//                            let vc = WPRidesViewController()
//                            vc.hidesBottomBarWhenPushed = true
//                            vc.edgesForExtendedLayout = .None
//                            vc.jumpOneScreen = true
//                            vc.data = self.data
//                            vc.dataSourceToOfc = (objectRides as! WPRidesResponse).pools?.officeGoing
//                            vc.dataSourceToHome = (objectRides as! WPRidesResponse).pools?.homeGoing
//                            self.navigationController?.pushViewController(vc, animated: true)
//                        }) { (error) in
//                            hideIndicator()
//                        }
                    } else {
                        _ = self.navigationController?.popToRootViewController(animated: true)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateWpsList"), object: nil)
                    }
                })
            }
            else{
                AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: nil)
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func btnUnjoin_clicked()
    {
        var localHideAnywhere = "0"
        if(UserInfo.sharedInstance.hideAnywhere != "") {
            localHideAnywhere = UserInfo.sharedInstance.hideAnywhere
        }
        showIndicator("Unjoining workplace..")
        WorkplaceRequestor().unjoinWorkPlace(data!.wpID!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Unjoin", message: "You have left the group successfully.", okButtonTitle: "Ok", okAction: {
                    let reqObj = ProfileDetailsRequest()
                    reqObj.userID = UserInfo.sharedInstance.userID
                    showIndicator("Loading Workplaces.")
                    ProfileRequestor().profileDetailRequest(reqObj, success:{ (success, object) in
                        hideIndicator()
                        if (object as! ProfileDetailsResponse).code == "415"
                        {
                            if((object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere != nil && (object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere != "") {
                                UserInfo.sharedInstance.hideAnywhere = ((object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere)!
                                if(localHideAnywhere != UserInfo.sharedInstance.hideAnywhere) {
                                    let ins = UIApplication.shared.delegate as! AppDelegate
                                    if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                        ins.gotoHome()
                                    } else {
                                        ins.gotoHome(1)
                                    }
                                } else {
                                    _ = self.navigationController?.popToRootViewController(animated: true)
                                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateWpsList"), object: nil)
                                    self.onBtnActionBlock?()
                                }
                            }
                        } else {
                        }
                    }){ (error) in
                        hideIndicator()
                        AlertController.showAlertForError(error)
                    }
                })
            }
            else{
                
            }
            
        }) { (error) in
            hideIndicator()
        }
    }
    
    func setVisibleBtns(_ view: UIView)
    {
        if(btnConstraints != nil){
            view.removeConstraints(btnConstraints!)
            btnConstraints = nil
        }
        
        btnInvite.isHidden = true
        btnUpdate.isHidden = true
        btnCancel.isHidden = true
        btnJoin.isHidden = true
        btnUnjoin.isHidden = true
        
        let viewsDict = ["invite":btnInvite, "cancel":btnCancel, "join":btnJoin, "unjoin":btnUnjoin, "update":btnUpdate]
        let wpMembershipStatus = data!.wpMembershipStatus != nil ? data!.wpMembershipStatus : data!.wpDetailMembershipStatus
        if(wpMembershipStatus == "0"){
            btnCancel.isHidden = false
            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]|", options: [], metrics: nil, views: viewsDict)
        }
        else if(wpMembershipStatus == "1"){
            if(data?.wpInitiator == UserInfo.sharedInstance.userID){
                btnInvite.isHidden = false
                btnUpdate.isHidden = false
                btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[update(==invite)]|", options: [], metrics: nil, views: viewsDict)
            }
            else{
                btnInvite.isHidden = false
                btnUnjoin.isHidden = false
                btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[unjoin(==invite)]|", options: [], metrics: nil, views: viewsDict)
            }
        }
        else{
            btnJoin.isHidden = false
            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
        }
        
        view.addConstraints(btnConstraints!)
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
     
    }
}
