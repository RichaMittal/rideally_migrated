//
//  PointsTableViewController.swift
//  rideally
//
//  Created by Raghunathan on 10/19/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class PointsTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
    let tableView = UITableView()
    let  segmentPoints = HMSegmentedControl()
    var tableHeightConstraint: NSLayoutConstraint?
    var tableTopConstraint: NSLayoutConstraint?
    let rowHt: CGFloat = 100
    var data = PointData()
    var myAccounts = false
    var transactionData: GetUserPointsOnRecahargeResponse?{
        didSet{
            self.data = (transactionData?.dataObj)!
            self.dataSource = (transactionData?.dataObj?.earnedPoints)!
        }
    }
    var dataSource = [EarnedData]()
        {
        didSet
        {
            if(dataSource.count > 0)
            {
                tableView.isHidden = false
                view.removeEmptyView()
                tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt)
                tableView.reloadData()
            }
            else{
                showEmptyView("No Data Found")
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Points Table"
        self.view.backgroundColor = Colors.WHITE_COLOR
        
        createPointsTableView()
        if(myAccounts) {
            getEarnedPoints()
        } else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
            getTransactionData()
        }
        self.tableView.tableFooterView = UIView()
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func createPointsTableView() -> Void
    {
        self.segmentPoints.backgroundColor = UIColor.clear
        self.segmentPoints.sectionTitles = ["Earned Points", "Used Points"]
        self.segmentPoints.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.GREEN_COLOR]
        self.segmentPoints.selectedTitleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
        self.segmentPoints.selectionStyle = HMSegmentedControlSelectionStyleBox
        self.segmentPoints.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone
        self.segmentPoints.selectionIndicatorColor = Colors.GREEN_COLOR
        self.segmentPoints.selectionIndicatorBoxOpacity = 1.0
        self.segmentPoints.translatesAutoresizingMaskIntoConstraints = false
        self.segmentPoints.segmentEdgeInset = UIEdgeInsets.zero
        self.segmentPoints.selectedSegmentIndex = 0
        segmentPoints.layer.borderColor = Colors.GREEN_COLOR.cgColor
        segmentPoints.layer.borderWidth = 1
        self.segmentPoints.addTarget(self, action: #selector(segmentTypeChanged), for: .valueChanged)
        self.view.addSubview(self.segmentPoints)
        
        //tableView.separatorColor = UIColor.clearColor()
        tableView.separatorColor = Colors.GRAY_COLOR
        tableView.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = true
        tableView.register(PointsListCell.self, forCellReuseIdentifier: "PointsListCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        
        //heigh reduce of segment
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-80-[segPoint(30)]-5-[table]|", options:[], metrics:nil, views: ["segPoint": self.segmentPoints,"table":tableView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-40-[segPoint]-40-|", options:[], metrics:nil, views: ["segPoint": self.segmentPoints]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
        
        tableHeightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHt
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PointsListCell") as! PointsListCell
        cell.selectedIndex = self.segmentPoints.selectedSegmentIndex
        cell.data = dataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.WHITE_COLOR
    }
    
    func showEmptyView(_ msg: String)
    {
        tableHeightConstraint?.constant = 0
        tableView.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "", actionHandler: nil)
    }
    
    func segmentTypeChanged() -> Void
    {
        if (self.segmentPoints.selectedSegmentIndex == 0)
        {
            getEarnedPoints()
        }
        else
        {
            getUsedPoints()
        }
        
        self.tableView.reloadData()
    }
    
    func getEarnedPoints()
    {
        self.dataSource = data.earnedPoints!
    }
    
    func getUsedPoints()
    {
        self.dataSource = data.usedPoints!
    }
    
    func getTransactionData() {
        let reqObj = GetUserPointsOnRecahargeRequest()
        showIndicator("Loading...")
        reqObj.userID =  UserInfo.sharedInstance.userID
        
        MyAccountRequestor().sendGetUserPointsRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            //let vc = PointsTableViewController()
            self.transactionData = object as? GetUserPointsOnRecahargeResponse
            //self.navigationController?.pushViewController(vc, animated: true)
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
}
