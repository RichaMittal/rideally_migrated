    //
    //  RideDetailViewController.swift
    //  rideally
    //
    //  Created by Sarav on 15/08/16.
    //  Copyright © 2016 rideally. All rights reserved.
    //
    
    import UIKit
    import Kingfisher
    import CoreLocation
    import GoogleMaps
    import GooglePlaces
    import AddressBook
    import AddressBookUI
    import FirebaseAnalytics
    
    class RideDetailViewController: BaseScrollViewController, CLLocationManagerDelegate,ABPeoplePickerNavigationControllerDelegate {
        
        var data: RideDetail?
        var wpData: WorkPlace?
        lazy var bottomBtnsView: SubView = self.getBtnView()
        lazy var commentsView: SubView = self.getCommentsView()
        lazy var vehicleView: SubView = self.getVehicleView()
        var addressBookController = ABPeoplePickerNavigationController()
        var btnConstraints: [NSLayoutConstraint]?
        var isSOSButtonPressed = false
        var isFirstLevel = false
        var taxiSourceID = ""
        var taxiDestID = ""
        var onDeleteActionBlock: actionBlock?
        var onSelectBackBlock: actionBlock?
        var onUpdateSearchResultBlock: actionBlock?
        var fromSearchesult = false
        let btnSOS = UIButton()
        
        override func goBack()
        {
            if(isFirstLevel){
                _ = self.navigationController?.popViewController(animated: true)
                onSelectBackBlock?()
                if(fromSearchesult) {
                    fromSearchesult = false
                    onUpdateSearchResultBlock?()
                }
            }
            else{
                _ = self.navigationController?.popToRootViewController(animated: true)
                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateRidesList"), object: nil)
            }
        }
        
        func updateRideDetails()
        {
            fromSearchesult = true
            showIndicator("Updating Ride details..")
            
            RideRequestor().getRideDetails(data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    self.data = object as? RideDetail
                    self.updateScreenWithNewData()
                }
            }) { (error) in
                hideIndicator()
                AlertController.showAlertForMessage("Failed to update ride deails. Try after sometime.")
            }
        }
        func updateScreenWithNewData()
        {
            setVisibleBtns(bottomBtnsView.view!)
            updateData()
        }
        
        func updateData()
        {
            lblName.text = data?.ownerName
            lblSource.text = data?.source
            lblDest.text = data?.destination
            
            lblPickUp.text = data?.pickUpPoint
            lblDrop.text = data?.dropPoint
            
            lblRideBelongTo.text = "-"
            if let scope = data?.ownerScope{
                if((scope != "") && (scope == "PUBLIC")){
                    lblRideBelongTo.text = "Public"
                }
                else{
                    lblRideBelongTo.text = data?.poolGroups?.first?.name
                }
            }
            
            lblTravelWith.text = "Any"
            if(data?.poolType == "M"){
                lblTravelWith.text = "Only Male"
            }
            else if(data?.poolType == "F"){
                lblTravelWith.text = "Only Female"
            }
            
            lblTravelDate.text = data?.startDateDisplay
            lblTravelTime.text = data?.startTimeDisplay
            
            lblTravelDistance.text = (data?.distance)! + "\(" KMs")"
            if(data?.poolTaxiBooking == "1") {
                if(Int((data?.duration)!)! >= 60) {
                    let hours = Int((data?.duration)!)! / 60
                    let minutes = Int((data?.duration)!)! % 60
                    var hrText = " hours "
                    var minText = " mins"
                    if(hours == 1) {
                        hrText = " hour "
                    }
                    if(minutes == 1) {
                        minText = " min"
                    }
                    lblTravelDuration.text = "\(hours) \(hrText)" + "\(minutes) \(minText)"
                } else {
                    lblTravelDuration.text = (data?.duration)! + "\(" mins")"
                }
            } else {
                lblTravelDuration.text = data?.duration
            }
            
            lblCount.text = "-"
            if let gesture = lblMembersTitle.gestureRecognizers?.first{
                lblMembersTitle.removeGestureRecognizer(gesture)
            }
            
            if data?.poolTaxiBooking == "1"{
                lblCount.text = data?.poolJoinedMembers
            }
            else{
                if let cnt = data?.poolJoinedMembers{
                    lblCount.text = "\(Int(cnt)! + 1)"
                    
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
                    lblMembersTitle.addGestureRecognizer(tapGesture)
                }
            }
            
            if(data?.rideType != "N" || ((data?.vehicleRegNo != nil) && (data?.vehicleRegNo != ""))){
                //if((data?.vehicleRegNo != nil) && (data?.vehicleRegNo != "")){
                viewsArray.append(vehicleView)
                //}
            }
            
            if((data?.comments != nil) && (data?.comments != "")){
                viewsArray.append(commentsView)
            }
            
            lblComments.text = data?.comments
            
            if(data?.createdBy == UserInfo.sharedInstance.userID && data?.poolTaxiBooking == "0"){
                lblVerification.isHidden = false
                verificationView.isHidden = true
                if(data?.poolType == "J"){
                    lblVerification.text = "Joined"
                }
                else{
                    lblVerification.text = "Started By Me"
                }
            }
            else{
                lblVerification.isHidden = true
                verificationView.isHidden = false
                if(data?.userState == "5"){
                    self.emailIcon.image = UIImage(named: "email_v")
                    self.mobileIcon.image = UIImage(named: "phone_v")
                }
                else if(data?.userState == "4"){
                    self.mobileIcon.image = UIImage(named: "phone_v")
                }
                else if(data?.userState == "3"){
                    self.emailIcon.image = UIImage(named: "email_v")
                }
            }
            
            if(data?.poolTaxiBooking != "1"){
                if let status = data?.currentStatus
                {
                    bottomBtnsView.heightConstraint?.constant = 39
                    if(status.lowercased() == "c"){
                        btnStart.setTitle("RIDE COMPLETED!", for: UIControlState())
                    }
                    else if((status.lowercased() == "s") || (status.lowercased() == "b")){
                        btnStart.setTitle("TRACK RIDE", for: UIControlState())
                        btnStart.removeTarget(self, action: #selector(startRide), for: .touchDown)
                        btnStart.addTarget(self, action: #selector(trackRide), for: .touchDown)
                    }
                }
                else{
                    bottomBtnsView.heightConstraint?.constant = 80
                    btnStart.isHidden = true
                    
                    //if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
                    if(data?.currentStatus?.lowercased() != "c"){
                        btnStart.isHidden = false
                        if(data?.vehicleOwnerID == UserInfo.sharedInstance.userID && data?.vehicleRegNo != nil){
                            btnStart.setTitle("START RIDE", for: UIControlState())
                            btnStart.addTarget(self, action: #selector(startRide), for: .touchDown)
                        } else {
                            if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
                                btnStart.setTitle("TRACK RIDE", for: UIControlState())
                                btnStart.addTarget(self, action: #selector(trackRide), for: .touchDown)
                            } else {
                                btnStart.isHidden = true
                            }
                        }
                    }
                    //}
                }
            }
        }
        
        func gotoMemberDetail()
        {
            showIndicator("Getting owner's detail..")
            RideRequestor().getMembersDetails((data?.createdBy)!, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    let vc = MemberDetailViewController()
                    vc.dataSource = (object as? MemberDetailResponse)?.data?.userInfo
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }) { (error) in
                hideIndicator()
            }
        }
        
        override func viewWillAppear(_ animated: Bool) {
            
            super.viewWillAppear(animated)
            self.isSOSButtonPressed = false
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
            let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
            self.navigationItem.leftBarButtonItem = newBackButton;
            title = "Ride Details"
            
            viewsArray.append(getProfileView())
            viewsArray.append(getLocationView())
            viewsArray.append(getRideDetailsView())
            
            viewsArray.append(getMembersView())
            
            if(data?.rideType != "N" || ((data?.vehicleRegNo != nil) && (data?.vehicleRegNo != ""))){
                //if((data?.vehicleRegNo != nil) && (data?.vehicleRegNo != "")){
                viewsArray.append(vehicleView)
                //}
            }
            
            if((data?.comments != nil) && (data?.comments != "")){
                viewsArray.append(commentsView)
            }
            
            addBottomView(bottomBtnsView)
        }
        
        let lblName = UILabel()
        let emailIcon = UIImageView()
        let mobileIcon = UIImageView()
        let verificationView = UIView()
        let lblVerification = UILabel()
        
        func getProfileView() -> SubView
        {
            let view = UIView()
            let imgProfilePic = UIImageView()
            imgProfilePic.translatesAutoresizingMaskIntoConstraints = false
            imgProfilePic.backgroundColor = UIColor.clear
            view.addSubview(imgProfilePic)
            
            if(data?.ownerImgStatus == "1"){
                if let url = data?.ownerImgUrl{
                    imgProfilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                    //imgProfilePic.kf_setImageWithURL(URL(string: "\(URLS.IMG_URL)\(url)"))
                }
                else{
                    if let fbID = data?.ownerFBID{
                        imgProfilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))

                        //imgProfilePic.kf_setImageWithURL(URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))
                    }
                }
            }
            else{
                imgProfilePic.image = UIImage(named: "placeholder")
            }
            
            verificationView.backgroundColor = Colors.BLACK_COLOR
            verificationView.alpha = 0.8
            verificationView.translatesAutoresizingMaskIntoConstraints = false
            emailIcon.image = UIImage(named: "email")
            emailIcon.translatesAutoresizingMaskIntoConstraints = false
            verificationView.addSubview(emailIcon)
            mobileIcon.image = UIImage(named: "phone")
            mobileIcon.translatesAutoresizingMaskIntoConstraints = false
            verificationView.addSubview(mobileIcon)
            verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[email(15)]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
            verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mobile(15)]", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
            verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mobile]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
            verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[email]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
            
            imgProfilePic.addSubview(verificationView)
            
            imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":verificationView]))
            imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(15)]|", options: [], metrics: nil, views: ["view":verificationView]))
            
            lblVerification.backgroundColor = Colors.BLACK_COLOR
            lblVerification.translatesAutoresizingMaskIntoConstraints = false
            lblVerification.alpha = 0.8
            lblVerification.textAlignment = .center
            lblVerification.textColor = Colors.WHITE_COLOR
            lblVerification.font = normalFontWithSize(9)
            lblVerification.isHidden = true
            imgProfilePic.addSubview(lblVerification)
            
            imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblVerification]|", options: [], metrics: nil, views: ["lblVerification":lblVerification]))
            imgProfilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lblVerification(15)]|", options: [], metrics: nil, views: ["lblVerification":lblVerification]))
            
            lblName.translatesAutoresizingMaskIntoConstraints = false
            lblName.text = data?.ownerName
            lblName.textColor = Colors.GREEN_COLOR
            lblName.textAlignment = .center
            lblName.font = normalFontWithSize(14)
            view.addSubview(lblName)
            
            imgProfilePic.isUserInteractionEnabled = true
            lblName.isUserInteractionEnabled = true
            
            let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(gotoMemberDetail))
            imgProfilePic.addGestureRecognizer(tapGesture1)
            let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(gotoMemberDetail))
            lblName.addGestureRecognizer(tapGesture2)
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(80)]-10-[lbl(20)]-10-|", options: [], metrics: nil, views: ["pic":imgProfilePic, "lbl":lblName]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblName]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[pic(60)]", options: [], metrics: nil, views: ["pic":imgProfilePic]))
            view.addConstraint(NSLayoutConstraint(item: imgProfilePic, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0))
            
            if(data?.createdBy == UserInfo.sharedInstance.userID && data?.poolTaxiBooking == "0"){
                lblVerification.isHidden = false
                verificationView.isHidden = true
                if(data?.poolType == "J"){
                    lblVerification.text = "Joined"
                }
                else{
                    lblVerification.text = "Started By Me"
                }
            }
            else{
                lblVerification.isHidden = true
                verificationView.isHidden = false
                if(data?.userState == "5"){
                    self.emailIcon.image = UIImage(named: "email_v")
                    self.mobileIcon.image = UIImage(named: "phone_v")
                }
                else if(data?.userState == "4"){
                    self.mobileIcon.image = UIImage(named: "phone_v")
                }
                else if(data?.userState == "3"){
                    self.emailIcon.image = UIImage(named: "email_v")
                }
            }
            btnSOS.translatesAutoresizingMaskIntoConstraints = false
            btnSOS.backgroundColor = UIColor.red
            btnSOS.titleLabel?.font = boldFontWithSize(15)
            btnSOS.setTitle("SOS", for: UIControlState())
            btnSOS.setTitleColor(UIColor.white, for: UIControlState())
            btnSOS.layer.cornerRadius = 15
            btnSOS.addTarget(self, action: #selector(btnSOS_clicked), for: .touchDown)
            view.addSubview(btnSOS)
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[btnsos(30)]-15-|", options: [], metrics: nil, views: ["btnsos":btnSOS]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[btnsos(30)]", options: [], metrics: nil, views: ["btnsos":btnSOS]))
            
            btnSOS.isHidden = true
            if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
                if(data?.currentStatus?.lowercased() != "c"){
                    //if(data?.vehicleRegNo != nil){
                        btnSOS.isHidden = false
                    //}
                }
            }
            
            let subview = SubView()
            subview.view = view
            subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            subview.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 130)
            return subview
        }
        
        func btnSOS_clicked()
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                if  (UserInfo.sharedInstance.selectedEmergencyNumber.characters.count > 0)
                {
                    self.isSOSButtonPressed = true
                    self.tripStarted = true
                    self.locationManager.requestWhenInUseAuthorization()
                    
                    if CLLocationManager.locationServicesEnabled()
                    {
                        
                        self.locationManager.delegate = self
                        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                        self.locationManager.startUpdatingLocation()
                    }
                }
                else
                {
                    callAddressBookForPressedButtonObject()
                }
            }
        }
        
        let lblSource = UILabel()
        let lblPickUp = UILabel()
        let lblDest = UILabel()
        let lblDrop = UILabel()
        
        func getLocationView() -> SubView
        {
            let view = UIView()
            
            let iconSource = UIImageView()
            iconSource.image = UIImage(named: "source")
            iconSource.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(iconSource)
            
            lblSource.textColor = Colors.GRAY_COLOR
            lblSource.text = data?.source
            lblSource.font = boldFontWithSize(14)
            lblSource.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblSource)
            
            let lblPickupTitle = UILabel()
            lblPickupTitle.translatesAutoresizingMaskIntoConstraints = false
            lblPickupTitle.text = "START POINT"
            lblPickupTitle.font = normalFontWithSize(11)
            lblPickupTitle.textColor = Colors.LABEL_DULL_TXT_COLOR
            view.addSubview(lblPickupTitle)
            
            lblPickUp.translatesAutoresizingMaskIntoConstraints = false
            lblPickUp.font = boldFontWithSize(14)
            lblPickUp.textColor = Colors.GRAY_COLOR
            lblPickUp.numberOfLines = 0
            lblPickUp.lineBreakMode = .byWordWrapping
            view.addSubview(lblPickUp)
            
            let lblLine2 = UILabel()
            lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine2.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine2)
            
            let iconDest = UIImageView()
            iconDest.image = UIImage(named: "dest")
            iconDest.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(iconDest)
            
            lblDest.textColor = Colors.GRAY_COLOR
            lblDest.font = boldFontWithSize(14)
            lblDest.text = data?.destination
            lblDest.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblDest)
            
            let lblDropTitle = UILabel()
            lblDropTitle.translatesAutoresizingMaskIntoConstraints = false
            lblDropTitle.text = "END POINT"
            lblDropTitle.font = normalFontWithSize(11)
            lblDropTitle.textColor = Colors.LABEL_DULL_TXT_COLOR
            view.addSubview(lblDropTitle)
            
            lblDrop.translatesAutoresizingMaskIntoConstraints = false
            lblDrop.font = boldFontWithSize(14)
            lblDrop.textColor = Colors.GRAY_COLOR
            lblDrop.numberOfLines = 0
            lblDrop.lineBreakMode = .byWordWrapping
            view.addSubview(lblDrop)
            
            lblPickUp.text = data?.pickUpPoint
            lblDrop.text = data?.dropPoint
            
            let lblLine3 = UILabel()
            lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine3.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine3)
            
            let lblShowMap = UILabel()
            lblShowMap.text = "SHOW MAP"
            lblShowMap.font = normalFontWithSize(11)
            lblShowMap.textColor = Colors.GRAY_COLOR
            lblShowMap.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblShowMap)
            
            let iconMap = UIImageView()
            iconMap.image = UIImage(named: "map")
            iconMap.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(iconMap)
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showDirection))
            lblShowMap.isUserInteractionEnabled = true
            lblShowMap.addGestureRecognizer(tapGesture)
            
            let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(showDirection))
            iconMap.isUserInteractionEnabled = true
            iconMap.addGestureRecognizer(tapGesture1)
            
            let lblLine4 = UILabel()
            lblLine4.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine4.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine4)
            
            var isPickUpPointVisible = true
            var viewHt: CGFloat = 245
            if(data?.poolTaxiBooking == "1"){
                isPickUpPointVisible = false
                viewHt = 120
                lblPickupTitle.isHidden = true
                lblPickUp.isHidden = true
                lblDropTitle.isHidden = true
                lblDrop.isHidden = true
            }
            if let pickupPoint = data?.pickUpPoint{
                if(pickupPoint == ""){
                    isPickUpPointVisible = false
                    viewHt = 120
                    lblPickupTitle.isHidden = true
                    lblPickUp.isHidden = true
                    lblDropTitle.isHidden = true
                    lblDrop.isHidden = true
                }
            }else{
                isPickUpPointVisible = false
                viewHt = 120
                lblPickupTitle.isHidden = true
                lblPickUp.isHidden = true
                lblDropTitle.isHidden = true
                lblDrop.isHidden = true
            }
            
            let viewsDict = ["isource":iconSource, "source":lblSource, "pickupt":lblPickupTitle, "pickup":lblPickUp, "line2":lblLine2, "idest":iconDest, "dest":lblDest, "dropt":lblDropTitle, "drop":lblDrop, "line3":lblLine3, "lblshowmap":lblShowMap, "imap":iconMap, "line4":lblLine4]
            
            if(isPickUpPointVisible == true){
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(40)]-5-[pickupt(15)][pickup(40)]-5-[line2(0.5)][dest(40)]-5-[dropt(15)][drop(40)]-5-[line3(0.5)][lblshowmap(30)][line4(0.5)]", options: [], metrics: nil, views: viewsDict))
                
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[pickupt]-5-|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[pickup]-5-|", options: [], metrics: nil, views: viewsDict))
                
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[dropt]-5-|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[drop]-5-|", options: [], metrics: nil, views: viewsDict))
            }else{
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(40)][line2(0.5)][dest(40)][line3(0.5)][lblshowmap(30)][line4(0.5)]", options: [], metrics: nil, views: viewsDict))
            }
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource(15)][source]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest(15)][dest]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblshowmap][imap(15)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line4]|", options: [], metrics: nil, views: viewsDict))
            
            
            view.addConstraint(NSLayoutConstraint(item: iconMap, attribute: .centerY, relatedBy: .equal, toItem: lblShowMap, attribute: .centerY, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: iconMap, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
            
            view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
            view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
            
            let sub = SubView()
            sub.view = view
            sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: viewHt)
            return sub
        }
        
        func showDirection()
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                let vc = DirectionViewController()
                vc.fromLocation = data?.source
                vc.toLocation = data?.destination
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        let lblRideBelongToTitle = UILabel()
        let lblRideBelongTo = UILabel()
        
        let lblTravelWithTitle = UILabel()
        let lblTravelWith = UILabel()
        
        let lblTravelDateTitle = UILabel()
        let lblTravelDate = UILabel()
        
        let lblTravelTimeTitle = UILabel()
        let lblTravelTime = UILabel()
        
        let lblTravelDistanceTitle = UILabel()
        let lblTravelDistance = UILabel()
        
        let lblTravelDurationTitle = UILabel()
        let lblTravelDuration = UILabel()
        
        func setLblProp(_ lbl: UILabel, isTitle: Bool, parentView: UIView)
        {
            lbl.translatesAutoresizingMaskIntoConstraints = false
            lbl.font = boldFontWithSize(14)
            lbl.textColor = Colors.GRAY_COLOR
            
            if(isTitle){
                lbl.font = normalFontWithSize(11)
                lbl.textColor = Colors.LABEL_DULL_TXT_COLOR
            }
            
            parentView.addSubview(lbl)
        }
        
        func showWPDetail()
        {
            showIndicator("Fetching Rides.")
            WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: (data?.poolGroups?.first?.id)!, success: { (success, objectRides) in
                WorkplaceRequestor().getWPMembers("", groupID: (self.data?.poolGroups?.first?.id)!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
                    hideIndicator()
                    let vc = WPRidesViewController()
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    vc.data = (object as! WPMembersResponseData).groupDetails
                    vc.dataSourceToOfc = (objectRides as! WPRidesResponse).pools?.officeGoing
                    vc.dataSourceToHome = (objectRides as! WPRidesResponse).pools?.homeGoing
                    self.navigationController?.pushViewController(vc, animated: true)
                }) { (error) in
                    hideIndicator()
                }
            }) { (error) in
                hideIndicator()
            }
        }
        
        func getRideDetailsView() -> SubView
        {
            let view = UIView()
            
            setLblProp(lblRideBelongToTitle, isTitle: true, parentView: view)
            setLblProp(lblRideBelongTo, isTitle: false, parentView: view)
            
            setLblProp(lblTravelWithTitle, isTitle: true, parentView: view)
            setLblProp(lblTravelWith, isTitle: false, parentView: view)
            
            setLblProp(lblTravelDateTitle, isTitle: true, parentView: view)
            setLblProp(lblTravelDate, isTitle: false, parentView: view)
            
            setLblProp(lblTravelTimeTitle, isTitle: true, parentView: view)
            setLblProp(lblTravelTime, isTitle: false, parentView: view)
            
            setLblProp(lblTravelDistanceTitle, isTitle: true, parentView: view)
            setLblProp(lblTravelDistance, isTitle: false, parentView: view)
            
            setLblProp(lblTravelDurationTitle, isTitle: true, parentView: view)
            setLblProp(lblTravelDuration, isTitle: false, parentView: view)
            
            lblRideBelongToTitle.text = "RIDE BELONG TO"
            lblRideBelongTo.text = "-"
            if let scope = data?.ownerScope{
                if((scope != "") && (scope == "PUBLIC")){
                    lblRideBelongTo.text = "Public"
                }
                else{
                    lblRideBelongTo.text = data?.poolGroups?.first?.name
                    lblRideBelongTo.gestureRecognizers?.removeAll()
                    lblRideBelongTo.isUserInteractionEnabled = true
                    let tapWP = UITapGestureRecognizer(target: self, action: #selector(showWPDetail))
                    lblRideBelongTo.addGestureRecognizer(tapWP)
                }
            }
            
            lblTravelWithTitle.text = "TRAVEL WITH"
            lblTravelWith.text = "Any"
            if(data?.poolType == "M"){
                lblTravelWith.text = "Only Male"
            }
            else if(data?.poolType == "F"){
                lblTravelWith.text = "Only Female"
            }
            
            lblTravelDateTitle.text = "TRAVELLING ON"
            lblTravelTimeTitle.text = "RIDE TIME"
            
            //lblTravelDate.text = data?.startDate
            //lblTravelTime.text = data?.startTime
            
            lblTravelDate.text = data?.startDateDisplay
            lblTravelTime.text = data?.startTimeDisplay
            
            lblTravelDistanceTitle.text = "TRIP DISTANCE"
            lblTravelDistance.text = (data?.distance)! + "\(" KMs")"
            lblTravelDurationTitle.text = "TRIP DURATION"
            if(data?.poolTaxiBooking == "1") {
                if(Int((data?.duration)!)! >= 60) {
                    let hours = Int((data?.duration)!)! / 60
                    let minutes = Int((data?.duration)!)! % 60
                    var hrText = " hours "
                    var minText = " mins"
                    if(hours == 1) {
                        hrText = " hour "
                    }
                    if(minutes == 1) {
                        minText = " min"
                    }
                    lblTravelDuration.text = "\(hours) \(hrText)" + "\(minutes) \(minText)"
                } else {
                    lblTravelDuration.text = (data?.duration)! + "\(" mins")"
                }
            } else {
                lblTravelDuration.text = data?.duration
            }
            
            let lblLine1 = UILabel()
            lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine1.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine1)
            
            let lblLine2 = UILabel()
            lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine2.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine2)
            
            let lblLine3 = UILabel()
            lblLine3.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine3.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine3)
            
            let viewsDict = ["belongtotitle":lblRideBelongToTitle, "belongto":lblRideBelongTo, "travelwithtitle":lblTravelWithTitle, "travelwith":lblTravelWith, "datetitle":lblTravelDateTitle, "date":lblTravelDate, "timetitle":lblTravelTimeTitle, "time":lblTravelTime ,"distancetitle":lblTravelDistanceTitle, "distance":lblTravelDistance, "durationtitle":lblTravelDurationTitle, "duration":lblTravelDuration, "line1":lblLine1, "line2":lblLine2, "line3":lblLine3]
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[belongtotitle(20)][belongto(20)]-3-[line1(0.5)]-3-[datetitle(20)][date(20)]-3-[line2(0.5)]-3-[distancetitle(20)][distance(20)]-3-[line3(0.5)]", options: [], metrics: nil, views: viewsDict))
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[belongtotitle][travelwithtitle(==belongtotitle)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[belongto][travelwith(==belongto)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
            view.addConstraint(NSLayoutConstraint(item: lblTravelWithTitle, attribute: .centerY, relatedBy: .equal, toItem: lblRideBelongToTitle, attribute: .centerY, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: lblTravelWith, attribute: .centerY, relatedBy: .equal, toItem: lblRideBelongTo, attribute: .centerY, multiplier: 1, constant: 0))
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[datetitle][timetitle(==datetitle)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[date][time(==date)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: viewsDict))
            view.addConstraint(NSLayoutConstraint(item: lblTravelTimeTitle, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDateTitle, attribute: .centerY, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: lblTravelTime, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDate, attribute: .centerY, multiplier: 1, constant: 0))
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[distancetitle][durationtitle(==distancetitle)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[distance][duration(==distance)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line3]|", options: [], metrics: nil, views: viewsDict))
            view.addConstraint(NSLayoutConstraint(item: lblTravelDurationTitle, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDistanceTitle, attribute: .centerY, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: lblTravelDuration, attribute: .centerY, relatedBy: .equal, toItem: lblTravelDistance, attribute: .centerY, multiplier: 1, constant: 0))
            
            let sub = SubView()
            sub.view = view
            sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 140)
            return sub
        }
        
        let lblMembersTitle = UILabel()
        let lblCount = UILabel()
        func getMembersView() -> SubView
        {
            let view = UIView()
            
            lblMembersTitle.textColor = Colors.GREEN_COLOR
            lblMembersTitle.text = "VIEW MEMBER(S)"
            lblMembersTitle.font = normalFontWithSize(11)
            lblMembersTitle.isUserInteractionEnabled = true
            lblMembersTitle.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblMembersTitle)
            
            lblCount.textColor = Colors.GRAY_COLOR
            lblCount.isUserInteractionEnabled = true
            lblCount.text = "-"
            if let gesture = lblMembersTitle.gestureRecognizers?.first{
                lblMembersTitle.removeGestureRecognizer(gesture)
            }
            
            if data?.poolTaxiBooking == "1"{
                lblCount.text = data?.poolJoinedMembers
            }
            else{
                if let cnt = data?.poolJoinedMembers{
                    lblCount.text = "\(Int(cnt)! + 1)"
                }
            }
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
            lblMembersTitle.addGestureRecognizer(tapGesture)
            
            lblCount.textAlignment = .right
            lblCount.font = boldFontWithSize(14)
            lblCount.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblCount)
            
            let lblLine = UILabel()
            lblLine.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine)
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl][cnt(25)]-5-|", options: [], metrics: nil, views: ["lbl":lblMembersTitle, "cnt":lblCount]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl][line(0.5)]|", options: [], metrics: nil, views: ["lbl":lblMembersTitle, "line":lblLine]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[cnt]|", options: [], metrics: nil, views: ["cnt":lblCount]))
            
            let sub = SubView()
            sub.view = view
            sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
            return sub
        }
        
        func showMembersList()
        {
            if((data?.joinStatus != nil && data?.joinStatus == "J") || (data?.createdBy == UserInfo.sharedInstance.userID)){
                let vc = MembersViewController()
                vc.membersList = data?.poolUsers
                vc.poolID = data?.poolID
                vc.poolTaxiBooking = data?.poolTaxiBooking
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                AlertController.showToastForError("Please join the ride to see the members.")
            }
        }
        
        let lblEstimatedCost = UILabel()
        let lblVehicleNoTitle = UILabel()
        let lblVehicleNo = UILabel()
        
        let lblVehicleSeatsTitle = UILabel()
        let lblVehicleSeats = UILabel()
        
        func getVehicleView() -> SubView
        {
            let view = UIView()
            
            setLblProp(lblEstimatedCost, isTitle: false, parentView: view)
            
            setLblProp(lblVehicleNoTitle, isTitle: true, parentView: view)
            setLblProp(lblVehicleNo, isTitle: false, parentView: view)
            
            setLblProp(lblVehicleSeatsTitle, isTitle: true, parentView: view)
            setLblProp(lblVehicleSeats, isTitle: false, parentView: view)
            
            lblEstimatedCost.text = "ESTIMATED TRIP COST:  \(data!.points!) Points/seat"
            lblVehicleNoTitle.text = "VEHICLE"
            lblVehicleSeatsTitle.text = "SEATS LEFT"
            
            lblVehicleNo.text = data?.vehicleRegNo
            lblVehicleSeats.text = data?.seatsLeft
            
            let lblLine1 = UILabel()
            lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine1.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine1)
            
            let viewsDict = ["cost":lblEstimatedCost, "vehiclenotitle":lblVehicleNoTitle, "vehicleno":lblVehicleNo, "seatstitle":lblVehicleSeatsTitle, "seats":lblVehicleSeats, "line":lblLine1]
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[cost(20)][vehiclenotitle(20)][vehicleno(20)]-3-[line(0.5)]", options: [], metrics: nil, views: viewsDict))
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[cost]|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[vehiclenotitle][seatstitle(==vehiclenotitle)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[vehicleno][seats(==vehicleno)]-5-|", options: [], metrics: nil, views: viewsDict))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: viewsDict))
            view.addConstraint(NSLayoutConstraint(item: lblVehicleSeatsTitle, attribute: .centerY, relatedBy: .equal, toItem: lblVehicleNoTitle, attribute: .centerY, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: lblVehicleSeats, attribute: .centerY, relatedBy: .equal, toItem: lblVehicleNo, attribute: .centerY, multiplier: 1, constant: 0))
            
            let sub = SubView()
            sub.view = view
            sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 75)
            return sub
        }
        
        let lblComments = UILabel()
        func getCommentsView() -> SubView
        {
            let view = UIView()
            
            let lbl = UILabel()
            lbl.translatesAutoresizingMaskIntoConstraints = false
            lbl.font = normalFontWithSize(11)
            lbl.text = "COMMENTS"
            lbl.textColor = Colors.LABEL_DULL_TXT_COLOR
            view.addSubview(lbl)
            
            lblComments.translatesAutoresizingMaskIntoConstraints = false
            lblComments.font = boldFontWithSize(14)
            lblComments.numberOfLines = 0
            lblComments.textColor = Colors.GRAY_COLOR
            lblComments.text = data?.comments
            view.addSubview(lblComments)
            
            let lblLine = UILabel()
            lblLine.backgroundColor = Colors.GENERAL_BORDER_COLOR
            lblLine.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(lblLine)
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lbl(20)][comments][line(0.5)]|", options: [], metrics: nil, views: ["lbl":lbl, "comments":lblComments,"line": lblLine]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["lbl":lbl, "comments":lblComments]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[comments]-5-|", options: [], metrics: nil, views: ["lbl":lbl, "comments":lblComments]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options: [], metrics: nil, views: ["line":lblLine]))
            
            
            let sub = SubView()
            sub.view = view
            sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60)
            return sub
        }
        
        //Invite, Edit, Delete, Cancel, Join, Unjoin, OfferVehicle
        
        let btnInvite = UIButton()
        let btnEdit = UIButton()
        let btnDelete = UIButton()
        let btnCancel = UIButton()
        let btnJoin = UIButton()
        let btnUnjoin = UIButton()
        let btnAccept = UIButton()
        let btnReject = UIButton()
        let btnOfferVehicle = UIButton()
        let btnStart = UIButton()
        
        func setUpBtn(_ btn: UIButton, imageName: String, title: String, parent: UIView)
        {
            btn.translatesAutoresizingMaskIntoConstraints = false
            btn.backgroundColor = Colors.GRAY_COLOR
            btn.setImage(UIImage(named: imageName), for: UIControlState())
            btn.isHidden = true
            parent.addSubview(btn)
            
            parent.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]|", options: [], metrics: nil, views: ["btn":btn]))
        }
        
        func getBtnView() -> SubView
        {
            let view = UIView()
            
            setUpBtn(btnInvite, imageName: "invite", title:"Invite", parent: view)
            btnInvite.addTarget(self, action: #selector(btnInvite_clicked), for: .touchDown)
            
            setUpBtn(btnEdit, imageName: "edit", title:"Edit", parent: view)
            btnEdit.addTarget(self, action: #selector(btnEdit_clicked), for: .touchDown)
            
            setUpBtn(btnDelete, imageName: "delete", title:"Delete", parent: view)
            btnDelete.addTarget(self, action: #selector(btnDelete_clicked), for: .touchDown)
            
            setUpBtn(btnCancel, imageName: "cancel", title:"Cancel", parent: view)
            btnCancel.addTarget(self, action: #selector(btnCancel_clicked), for: .touchDown)
            
            setUpBtn(btnJoin, imageName: "join", title:"Join", parent: view)
            btnJoin.addTarget(self, action: #selector(btnJoin_clicked), for: .touchDown)
            
            setUpBtn(btnUnjoin, imageName: "unjoin", title:"Unjoin", parent: view)
            btnUnjoin.addTarget(self, action: #selector(btnUnjoin_clicked), for: .touchDown)
            
            setUpBtn(btnAccept, imageName: "accept", title:"Accept", parent: view)
            btnAccept.addTarget(self, action: #selector(btnAccept_clicked), for: .touchDown)
            
            setUpBtn(btnReject, imageName: "reject", title:"Reject", parent: view)
            btnReject.addTarget(self, action: #selector(btnReject_clicked), for: .touchDown)
            
            setUpBtn(btnOfferVehicle, imageName: "offeravehicle", title:"Offer a Vehicle", parent: view)
            btnOfferVehicle.addTarget(self, action: #selector(btnOfferVehicle_clicked), for: .touchDown)
            
            setVisibleBtns(view)
            var viewHt: CGFloat = 80
            
            if(data?.poolTaxiBooking != "1"){
                btnStart.translatesAutoresizingMaskIntoConstraints = false
                btnStart.backgroundColor = Colors.GRAY_COLOR
                
                if let status = data?.currentStatus
                {
                    viewHt = 39
                    if(status.lowercased() == "c"){
                        btnStart.setTitle("RIDE COMPLETED!", for: UIControlState())
                    }
                    else if((status.lowercased() == "s") || (status.lowercased() == "b")){
                        btnStart.setTitle("TRACK RIDE", for: UIControlState())
                        btnStart.removeTarget(self, action: #selector(startRide), for: .touchDown)
                        btnStart.addTarget(self, action: #selector(trackRide), for: .touchDown)
                    }
                }
                else{
                    btnStart.isHidden = true
                    //Optional("26 Nov 2016") Optional("7:34 AM")
                    
                    //if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
                    if(data?.currentStatus?.lowercased() != "c"){
                        btnStart.isHidden = false
                        if(data?.vehicleOwnerID == UserInfo.sharedInstance.userID && data?.vehicleRegNo != nil){
                            btnStart.setTitle("START RIDE", for: UIControlState())
                            btnStart.addTarget(self, action: #selector(startRide), for: .touchDown)
                        } else {
                            if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
                                btnStart.setTitle("TRACK RIDE", for: UIControlState())
                                btnStart.addTarget(self, action: #selector(trackRide), for: .touchDown)
                            } else {
                                btnStart.isHidden = true
                            }
                        }
                    }
                }
                //}
                
                view.addSubview(btnStart)
                
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(39)]", options: [], metrics: nil, views: ["btn":btnStart]))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btnStart]))
            }
            
            let sub = SubView()
            sub.view = view
            sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
            sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: viewHt)
            return sub
        }
        
        func trackRide()
        {
            if let status = data?.currentStatus
            {
                if((status.lowercased() == "s") || (status.lowercased() == "b")){
                    let vc = TripCalculationViewController()
                    vc.data = self.data
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                if(data?.vehicleOwnerID == nil) {
                    AlertController.showAlertFor("Track Ride", message: "It will be enabled when ride is having vehicle.", okAction: {})
                } else {
                    AlertController.showAlertFor("Track Ride", message: "Please wait for vehicle to reach your place.", okAction: {})
                }
            }
        }
        
        var tripStarted = false
        
        
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            let locValue:CLLocationCoordinate2D = manager.location!.coordinate
            manager.stopUpdatingLocation()
            
            hideIndicator()
            
            if  (isSOSButtonPressed == true)
            {
                isSOSButtonPressed = false
                let geocoder = GMSGeocoder()
                
                geocoder.reverseGeocodeCoordinate(locValue) { [weak self] response , error in
                    
                    if let address = response?.firstResult() {
                        
                        let lines = address.lines as [String]!
                        self?.sendSOSRequest("\(address.coordinate.latitude)","\(address.coordinate.longitude)",(lines?.joined(separator: ","))!)
                    }
                    
                }
            }
            
            
            
            if(tripStarted == false){
                tripStarted = true
                let reqObj = TripRequest()
                let geocoder = GMSGeocoder()
                geocoder.reverseGeocodeCoordinate(locValue) { [weak self] response , error in
                    if let address = response?.firstResult() {
                        
                        let lines = address.lines as [String]!
                        reqObj.userID = UserInfo.sharedInstance.userID
                        reqObj.poolID = self?.data?.poolID
                        reqObj.fromLocation = lines?.joined(separator: ",")
                        reqObj.fromLat = "\(locValue.latitude)"
                        reqObj.fromLong = "\(locValue.longitude)"
                        reqObj.toLocation = self?.data?.destination
                        reqObj.startTime = prettyDateStringFromDate(NSDate() as Date, toFormat: "hh:mm a")
                        
                        if(self?.data?.vehicleOwnerID == UserInfo.sharedInstance.userID){
                            if(self?.data?.vehicleOwnerID == self?.data?.createdBy) {
                                reqObj.poolOwnerType = "admin"
                            } else {
                                reqObj.poolOwnerType = ""
                            }
                            WorkplaceRequestor().startRideByDriver(reqObj, success: { (success, object) in
                                if(success == true){
                                    self?.gotoTripCalculationVC()
                                }else{
                                    AlertController.showAlertFor("Start Ride", message: object as? String)
                                }
                                }, failure: { (error) in
                            })
                        }
                        else{
                            reqObj.mobileNumber = UserInfo.sharedInstance.mobile
                            reqObj.deviceID = ""
                            reqObj.userName = UserInfo.sharedInstance.fullName
                            WorkplaceRequestor().startRideByRider(reqObj, success: { (success, object) in
                                if(success == true){
                                    self?.gotoTripCalculationVC()
                                }
                                else{
                                    AlertController.showAlertFor("Start Ride", message: object as? String)
                                }
                                }, failure: { (error) in
                            })
                        }
                    }
                    
                }
            }
        }
        
        func gotoTripCalculationVC()
        {
            let vc = TripCalculationViewController()
            vc.data = self.data
            vc.onSelectBackBlock = {
                self.updateRideDetails()
            }
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let locationManager = CLLocationManager()
        func startRide()
        {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Start Ride Activity",
                AnalyticsParameterItemName: "Button",
                AnalyticsParameterContentType:"Start Ride"
                ])
            let todayDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM yyyy"
            var date = dateFormatter.string(from: todayDate) as String
            if(date.hasPrefix("0")) {
                date.remove(at: date.startIndex)
            }
            if(date.contains(data!.startDate!)){
                dateFormatter.dateFormat = "hh:mm a"
                //dateFormatter.timeZone = NSTimeZone(name: "IST")
                var rideTime = dateFormatter.date(from: data!.startTime!)
                rideTime = Date(timeInterval: -600, since: rideTime!)
                let strCurrentTime = dateFormatter.string(from: Date())
                let currentTime = dateFormatter.date(from: strCurrentTime)
                if(currentTime!.compare(rideTime!) == ComparisonResult.orderedDescending){
                    self.locationManager.requestWhenInUseAuthorization()
                    AlertController.showAlertFor("Start Ride", message: "Would you like to start now?", okButtonTitle: "Start", okAction: {
                        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                            AnalyticsParameterItemID:"Start Ride Activity",
                            AnalyticsParameterItemName: "Ok Button",
                            AnalyticsParameterContentType:"Start Ride"
                            ])
                        showIndicator("Fetching current location..")
                        //TODO: check in all files for below condition.
                        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                            self.tripStarted = false
                            self.locationManager.delegate = self
                            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                            self.locationManager.startUpdatingLocation()
                        }
                        else{
                            hideIndicator()
                            AlertController.showAlertFor("Enable Location Services", message: "Enable location services from Settings > Privacy for the application to proceed.")
                        }
                    }, cancelButtonTitle: "Cancel") {
                        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                            AnalyticsParameterItemID:"Start Ride Activity",
                            AnalyticsParameterItemName: "Cancel Button",
                            AnalyticsParameterContentType:"Start Ride"
                            ])
                    }
                } else {
                    AlertController.showAlertFor("Start Ride", message: "Ride can only start before 10 minutes or update your start time.", okAction: {})
                }
            } else {
                AlertController.showAlertFor("Start Ride", message: "Ride can only start before 10 minutes or update your start time.", okAction: {})
            }
        }
        
        func btnInvite_clicked()
        {
            let vc = InviteViewController()
            vc.poolID = self.data!.poolID
            vc.ownerID = self.data!.createdBy
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.onCompletion = {
                self.updateRideDetails()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        func btnEdit_clicked()
        {
            if(self.data?.rideType == "O"){
                let vc = OfferVehicleViewController()
                vc.isEditingRide = true
                vc.rideDetailData = data
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if(self.data?.rideType == "N"){
                let vc = NeedRideViewController()
                vc.isEditingRide = true
                vc.rideDetailData = data
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        func btnDelete_clicked()
        {
            AlertController.showAlertFor("Delete Ride", message: "Would you really like to delete this ride?", okButtonTitle: "Yes", okAction: {
                
                showIndicator("Deleting Ride..")
                
                RideRequestor().deleteRide(self.data!.poolID!, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Delete Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            _ = self.navigationController?.popViewController(animated: true)
                            self.onDeleteActionBlock?()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Delete Ride", message: object as? String)
                    }
                    
                    }, failure: { (error) in
                        hideIndicator()
                        AlertController.showAlertForError(error)
                })
            }, cancelButtonTitle: "Later") {
                
            }
        }
        func btnCancel_clicked()
        {
            if(data?.poolTaxiBooking == "1"){
                //cancel taxi ride
                AlertController.showAlertFor("Cancel Taxi Ride", message: "Would you really like to cancel this taxi ride?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Cancelling Taxi Ride..")
                    RideRequestor().cancelTaxiRide(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                //self.updateRideDetails()
                                _ = self.navigationController?.popViewController(animated: true)
                                self.onDeleteActionBlock?()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
            else{
                AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel this ride?", okButtonTitle: "Yes", okAction: {
                    //from memberslist - FR
                    //from ridedetail & ridelist = JR
                    showIndicator("Cancelling Ride.")
                    RideRequestor().cancelRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, cancelrequestType: "JR", success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateRideDetails()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
        }
        func btnAccept_clicked()
        {
            AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this ride request?", okButtonTitle: "Yes", okAction: {
                showIndicator("Accepting ride request..")
                RideRequestor().acceptRideRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Accept Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateRideDetails()
                        })
                    }else{
                        AlertController.showAlertFor("Accept Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "Cancel") {
            }
        }
        func btnReject_clicked()
        {
            AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
                showIndicator("Rejecting ride request..")
                RideRequestor().rejectRideRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Reject Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateRideDetails()
                        })
                    }else{
                        AlertController.showAlertFor("Reject Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "Cancel") {
            }
        }
        func btnJoin_clicked()
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                if(UserInfo.sharedInstance.shareMobileNumber == false){
                    AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                        self.getPickUpDropLocation("joinRide")
                        }, cancelButtonTitle: "LATER", cancelAction: {
                    })
                }
                else{
                    getPickUpDropLocation("joinRide")
                }
            }
        }
        
        func btnUnjoin_clicked()
        {
            AlertController.showAlertFor("Unjoin Ride", message: "Would you really like to unjoin from this ride?", okButtonTitle: "YES", okAction: {
                showIndicator("Unjoining the ride..")
                RideRequestor().unJoinRide(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Unjoin Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.updateRideDetails()
                        })
                    }else{
                        AlertController.showAlertFor("Unjoin Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "LATER") {
            }
        }
        
        func btnOfferVehicle_clicked()
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                self.getPickUpDropLocation("offerVehicle")
            }
        }
        
        func getPickUpDropLocation(_ triggerFrom: String)
        {
            let joinOrOfferRideObj = JoinOrOfferRide()
            let vc = RideDetailMapViewController()
            vc.sourceLong = data?.sourceLong
            vc.sourceLat = data?.sourceLat
            vc.sourceLocation = data?.source
            vc.destLong = data?.destinationLong
            vc.destLat = data?.destinationLat
            vc.destLocation = data?.destination
            vc.poolTaxiBooking = (data?.poolTaxiBooking)!
            vc.poolId = (data?.poolID)!
            vc.rideType = (data?.rideType)!
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.hidesBottomBarWhenPushed = true
            vc.triggerFrom = triggerFrom
            vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
                joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
                joinOrOfferRideObj.poolID = self.data!.poolID!
                joinOrOfferRideObj.vehicleRegNo = ""
                joinOrOfferRideObj.vehicleCapacity = ""
                joinOrOfferRideObj.cost = "0"
                joinOrOfferRideObj.rideMsg = ""
                joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
                joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
                joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
                joinOrOfferRideObj.dropPoint = dropLoc as? String
                joinOrOfferRideObj.dropPointLat = dropLat as? String
                joinOrOfferRideObj.dropPointLong = dropLong as? String
                
                if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                    showIndicator("Joining Ride..")
                    OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                                self.updateRideDetails()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    
                }
                else{
                    AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
                }
            }
            
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        func setVisibleBtns(_ view: UIView)
        {
            if(btnConstraints != nil){
                view.removeConstraints(btnConstraints!)
                btnConstraints = nil
            }
            
            btnInvite.isHidden = true
            btnEdit.isHidden = true
            btnDelete.isHidden = true
            btnCancel.isHidden = true
            btnJoin.isHidden = true
            btnUnjoin.isHidden = true
            btnAccept.isHidden = true
            btnReject.isHidden = true
            btnOfferVehicle.isHidden = true
            
            if let status = data?.currentStatus
            {
                if((status.lowercased() == "s") || (status.lowercased() == "b") || (status.lowercased() == "c")){
                    return
                }
            }
            
            let viewsDict = ["invite":btnInvite, "edit":btnEdit, "delete":btnDelete, "cancel":btnCancel, "join":btnJoin, "unjoin":btnUnjoin, "offerv":btnOfferVehicle, "accept":btnAccept, "reject":btnReject]
            
            if(data?.createdBy == UserInfo.sharedInstance.userID){
                if(data?.poolTaxiBooking == "0"){
                    //INIVTE, EDIT, DELETE
                    btnInvite.isHidden = false
                    btnEdit.isHidden = false
                    btnDelete.isHidden = false
                    
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[edit(==invite)]-1-[delete(==invite)]|", options: [], metrics: nil, views: viewsDict)
                }
                else{
                    //INVITE, CANCEL
                    btnCancel.isHidden = false
                    if(data?.poolShared != "0") {
                        btnInvite.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[cancel(==invite)]|", options: [], metrics: nil, views: viewsDict)
                    } else {
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]|", options: [], metrics: nil, views: viewsDict)
                    }
                }
            }
            else{
                if(data?.joinStatus != nil && data?.requestedStatus != nil){
                    if(data?.joinStatus == "P" && data?.requestedStatus == "P"){
                        btnCancel.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]|", options: [], metrics: nil, views: viewsDict)
                    }
                    else if(data?.joinStatus == "J" && data?.requestedStatus == "A"){
                        //INVITE
                        btnInvite.isHidden = false
                        if(data?.poolTaxiBooking == "0"){
                            //UNJOIN
                            btnUnjoin.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[unjoin(==invite)]|", options: [], metrics: nil, views: viewsDict)
                        }
                        else{
                            //CANCEL
                            btnCancel.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[cancel(==invite)]|", options: [], metrics: nil, views: viewsDict)
                        }
                    }
                    else if(data?.joinStatus == "F" && data?.requestedStatus == "F"){
                        //btnInvite.hidden = false
                        btnAccept.isHidden = false
                        btnReject.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[accept]-1-[reject(==accept)]|", options: [], metrics: nil, views: viewsDict)
                    }
                    else if((data?.joinStatus == "U" && data?.requestedStatus == "A") || (data?.joinStatus == "P" && data?.requestedStatus == "C") || (data?.joinStatus == "S" && data?.requestedStatus == "C") || (data?.joinStatus == "R" && data?.requestedStatus == "R") || (data?.joinStatus == "O" && data?.requestedStatus == "A") || (data?.joinStatus == "F" && data?.requestedStatus == "C")){
                        if(data?.rideType == "O"){
                            btnJoin.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                            
                        }
                        else{
                            if(data?.poolTaxiBooking == "0"){
                                btnJoin.isHidden = false
                                btnOfferVehicle.isHidden = false
                                btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]-1-[offerv(==join)]|", options: [], metrics: nil, views: viewsDict)
                                
                            }
                            else{
                                btnJoin.isHidden = false
                                btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                            }
                        }
                    }
                }
                else{
                    if(data?.rideType == "O"){
                        btnJoin.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                    }
                    else{
                        if(data?.poolTaxiBooking == "0"){
                            btnJoin.isHidden = false
                            btnOfferVehicle.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]-1-[offerv(==join)]|", options: [], metrics: nil, views: viewsDict)
                        }
                        else{
                            btnJoin.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                        }
                    }
                }
            }
            view.addConstraints(btnConstraints!)
        }
        
        
        
        func sendSOSRequest(_ value :String...) -> Void
        {
            showIndicator("Sending Email & Sms...")
            let reqObject =  GetRideDetailsRequest()
            
            reqObject.userID = UserInfo.sharedInstance.userID
            reqObject.userName = UserInfo.sharedInstance.fullName
            reqObject.userMobile = UserInfo.sharedInstance.mobile
            reqObject.gender = UserInfo.sharedInstance.gender == "M" ? "His" : "Her"
            reqObject.currentLocation = value[2]
            
            
            reqObject.vehicleNumber = ""
            if  let regNo = data?.vehicleRegNo
            {
                reqObject.vehicleNumber = regNo
            }
            
            reqObject.vehicleModel = ""
            if  let regNo = data?.vehicleModel
            {
                reqObject.vehicleModel = regNo
            }
            
            reqObject.vehicleType = ""
            if  let regNo = data?.vehicleType
            {
                reqObject.vehicleType = regNo
            }
            
            let poolID : NSString!
            poolID = data?.poolID as NSString!
            
            reqObject.helpMobile = ""
            reqObject.helpMail = ""
            reqObject.sourceLocation = lblSource.text
            reqObject.destinationLocation =  lblDest.text
            reqObject.url = SOS_USERLOC+"?pool_id=\(poolID)&lat=\(value[0])&long=\(value[1])"
            reqObject.latitude = value[0]
            reqObject.longtitude = value[1]
            
            RideRequestor().getSOSForRideDetail(reqObject, success:{ (success, object) in
                hideIndicator()
                if(success == true) {
                    AlertController.showAlertFor("Alert", message: "RideAlly has sent Email & Sms to your SOS contacts successfully.\n Would you like to initiate emergency call?", okButtonTitle: "YES", okAction: {
                        if let url = URL(string: "tel://\(UserInfo.sharedInstance.selectedEmergencyNumber)"), UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.openURL(url)
                        }
                    }, cancelButtonTitle: "Later") {
                    }
                } else {
                    AlertController.showAlertForMessage((object as AnyObject).message)
                }
            }){ (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
            
        }
        func callAddressBookForPressedButtonObject() -> Void
        {
            let authorizationStatus = ABAddressBookGetAuthorizationStatus()
            
            switch authorizationStatus
            {
            case .denied, .restricted:
                displayCantAddContactAlert()
            case .authorized:
                self.addressBookController.peoplePickerDelegate = self
                self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                    "phoneNumbers.@count > 0", argumentArray: nil)
                self.present(self.addressBookController, animated: true, completion: nil)
            case .notDetermined:
                promptForAddressBookRequestAccess()
            }
            
        }
        
        func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController)
        {
            
            self.addressBookController.dismiss(animated: true, completion: nil)
        }
        
        var fullName = ""
        var contactNumber = ""
        var emailId = ""
        func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord)
        {
            if let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String
            {
                fullName = first
                
                if let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String
                {
                    fullName = first+last
                }
            }
            
            let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones != "")
            {
                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                {
                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                        var personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                        let areaCode = personNumber.characters.count-10
                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                        if(areaCode > 0) {
                            personNumber = personNumber.substring(from: startIndex)
                        }
                        contactNumber = personNumber
                    }
                }
            }
            
            let unmanagedPhones1 = ABRecordCopyValue(person, kABPersonEmailProperty)
            let phones1 = Unmanaged.fromOpaque((unmanagedPhones1?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones1 != "")
            {
                if let unmanagedPhone1 = ABMultiValueCopyValueAtIndex(phones1, 0)
                {
                    if  let phone1: String = Unmanaged.fromOpaque(unmanagedPhone1.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        emailId = phone1
                    }
                } else {
                    emailId = ""
                }
            }
            if(contactNumber != "") {
                sendSaveContactListRequest()
            }
        }
        func openSettings()
        {
            let url = URL(string: UIApplicationOpenSettingsURLString)
            UIApplication.shared.openURL(url!)
        }
        
        
        func promptForAddressBookRequestAccess()
        {
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            ABAddressBookRequestAccessWithCompletion(addressBookRef)
            {
                (granted: Bool, error: Error?) in
                DispatchQueue.main.async
                {
                    if !granted
                    {
                        //print("Just denied")
                        self.displayCantAddContactAlert()
                    } else
                    {
                        //print("Just authorized")
                        self.addressBookController.peoplePickerDelegate = self
                        self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                            "phoneNumbers.@count > 0", argumentArray: nil)
                        self.present(self.addressBookController, animated: true, completion: nil)
                    }
                }
            }
        }
        
        
        func displayCantAddContactAlert() {
            let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                        message: "You must give the app permission to add the contact first.",
                                                        preferredStyle: .alert)
            cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
                style: .default,
                handler: { action in
                    self.openSettings()
            }))
            cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            present(cantAddContactAlert, animated: true, completion: nil)
        }
        
        func sendSaveContactListRequest() -> Void
        {
            let reqObj = SaveEmergencyContactRequest()
            showIndicator("Loading...")
            
            reqObj.userID = UserInfo.sharedInstance.userID
            
            reqObj.firstPersonName = ""
            if fullName != ""
            {
                reqObj.firstPersonName = fullName
            }
            
            reqObj.secondPersonName = nil
            reqObj.firstPersonNumber = ""
            if contactNumber != ""
            {
                let stringArray = contactNumber.components(separatedBy: CharacterSet.decimalDigits.inverted)
                var firstPersonNumber = NSArray(array: stringArray).componentsJoined(by: "")
                let areaCode = firstPersonNumber.characters.count-10
                let startIndex = firstPersonNumber.characters.index(firstPersonNumber.startIndex, offsetBy: areaCode)
                if(areaCode > 0) {
                    firstPersonNumber = firstPersonNumber.substring(from: startIndex)
                }
                reqObj.firstPersonNumber = firstPersonNumber
            }
            
            reqObj.secondPersonNumber = nil
            reqObj.firstPersonEmail = ""
            if  emailId != ""
            {
                reqObj.firstPersonEmail = emailId
            }
            
            reqObj.secondPersonEmail = ""
            reqObj.defaultPerson = "1"
            
            EmergencyContactRequestor().sendSaveContactListRequest(reqObj, success:{ (success, object) in
                
                hideIndicator()
                
                if (object as! SaveEmergencyContactResponse).code == "4600"
                {
                    UserInfo.sharedInstance.selectedEmergencyNumber = reqObj.firstPersonNumber!
                    UserInfo.sharedInstance.selectedEmergencyEmail = reqObj.firstPersonEmail!
                    self.btnSOS_clicked()
                }
                else
                {
                    AlertController.showToastForError("Record Not Found.")
                }
                
            }){ (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
            
        }
    }
