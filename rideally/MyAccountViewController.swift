    //
    //  MyAccountViewController.swift
    //  rideally
    //
    //  Created by Raghunathan on 10/19/16.
    //  Copyright © 2016 rideally. All rights reserved.
    //
    
    import UIKit
    import CoreGraphics
    import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

    
    //    class MyAccountCell: UITableViewCell, UIScrollViewDelegate {
    //
    //        let couponCode = UILabel()
    //        let couponDescription = UILabel()
    //        let couponExpiryDate = UILabel()
    //        let readMore = UIButton()
    //        var onUserActionBlock: actionBlockWithParam?
    //
    //        var data: OffersPromotionsData!{
    //            didSet{
    //                buildCell()
    //            }
    //        }
    //
    //        required init?(coder aDecoder: NSCoder) {
    //            super.init(coder: aDecoder)
    //        }
    //
    //        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    //            super.init(style: style, reuseIdentifier: reuseIdentifier)
    //            commonInit()
    //        }
    //
    //        func commonInit()
    //        {
    //            self.backgroundColor = UIColor.clearColor()
    //            self.clipsToBounds = true
    //
    //            couponCode.font = boldFontWithSize(15)
    //            couponCode.textColor = Colors.ORANGE_TEXT_COLOR
    //            couponCode.lineBreakMode = NSLineBreakMode.ByWordWrapping
    //            couponCode.translatesAutoresizingMaskIntoConstraints = false
    //            couponCode.numberOfLines = 0
    //            self.contentView.addSubview(couponCode)
    //
    //            couponExpiryDate.font = boldFontWithSize(15)
    //            couponExpiryDate.textColor = Colors.ORANGE_TEXT_COLOR
    //            couponExpiryDate.lineBreakMode = NSLineBreakMode.ByWordWrapping
    //            couponExpiryDate.translatesAutoresizingMaskIntoConstraints = false
    //            couponExpiryDate.numberOfLines = 0
    //            couponExpiryDate.textAlignment = .Right
    //            self.contentView.addSubview(couponExpiryDate)
    //
    //            couponDescription.font = boldFontWithSize(15)
    //            couponDescription.textColor = Colors.GRAY_COLOR
    //            couponDescription.lineBreakMode = NSLineBreakMode.ByWordWrapping
    //            couponDescription.translatesAutoresizingMaskIntoConstraints = false
    //            //couponDescription.numberOfLines = 2
    //            self.contentView.addSubview(couponDescription)
    //
    //            readMore.titleLabel?.font = normalFontWithSize(14)!
    //            readMore.setTitle("READ MORE", forState: .Normal)
    //            readMore.setTitleColor(Colors.ORANGE_COLOR, forState: .Normal)
    //            readMore.translatesAutoresizingMaskIntoConstraints = false
    //            readMore.addTarget(self, action: #selector(btnReadMore_clicked), forControlEvents: .TouchDown)
    //            self.contentView.addSubview(readMore)
    //
    //            self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[code]-5-[desc]-5-[readMore]-5-|", options: [], metrics: nil, views: ["code":couponCode, "desc":couponDescription, "readMore":readMore]))
    //            self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-15-[code]-5-[date(==code)]-15-|", options: [], metrics: nil, views: ["code":couponCode,"date":couponExpiryDate]))
    //            self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-15-[desc]-5-|", options: [], metrics: nil, views: ["desc":couponDescription]))
    //            self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[readMore(80)]-10-|", options: [], metrics: nil, views: ["readMore":readMore]))
    //            self.contentView.addConstraint(NSLayoutConstraint(item: couponExpiryDate, attribute: .CenterY, relatedBy: .Equal, toItem: couponCode, attribute: .CenterY, multiplier: 1, constant: 0))
    //
    //        }
    //
    //        func btnReadMore_clicked() {
    //            let viewHolder = AlertContentViewHolder()
    //            viewHolder.heightConstraintValue = 95
    ////            let scrollView = UIScrollView()
    ////            scrollView.backgroundColor = Colors.WHITE_COLOR
    ////            scrollView.delegate = self
    ////            scrollView.contentSize = CGSizeMake(1000, 1000)
    //            let view = UIView()
    //            view.translatesAutoresizingMaskIntoConstraints = false
    //            view.backgroundColor = Colors.WHITE_COLOR
    //
    //            //            scrollView.showsVerticalScrollIndicator = false
    //            //            scrollView.bounces = false
    //            //            scrollView.delegate = self
    //            //
    //            //            scrollView.translatesAutoresizingMaskIntoConstraints = false
    //            //
    //            //            //view.addSubview(scrollView)
    //            //            scrollView.addSubview(contentView)
    //            let lblInfo = UILabel()
    //            lblInfo.text = data.couponDescription
    //            lblInfo.numberOfLines = 0
    //            lblInfo.lineBreakMode = .ByWordWrapping
    //            lblInfo.translatesAutoresizingMaskIntoConstraints = false
    //            lblInfo.font = normalFontWithSize(13)
    //            view.addSubview(lblInfo)
    //
    //            view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-10-[lbl]|", options: [], metrics: nil, views: ["lbl":lblInfo]))
    //            view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblInfo]))
    ////            scrollView.addSubview(view)
    ////            //            //self.contentView.addSubview(scrollView)
    ////            scrollView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view" : view]))
    ////            scrollView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view" : view]))
    //            //            let contentWidthConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 0)
    //            //            scrollView.addConstraint(contentWidthConstraint)
    //            viewHolder.view = view
    //
    //            AlertController.showAlertFor(data.couponCode, message: "", contentView: viewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: true, okAction: {
    //            })
    //        }
    //
    //        func buildCell()
    //        {
    //            let inputFormatter = NSDateFormatter()
    //            inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    //            let showDate = inputFormatter.dateFromString(data.couponExpiryDate!)
    //            inputFormatter.dateFormat = "dd MMM yyyy"
    //            let resultString = inputFormatter.stringFromDate(showDate!)
    //
    //            if(data.couponCode == "RAF") {
    //                couponCode.text = data.couponCode!+UserInfo.sharedInstance.userID
    //            } else {
    //                couponCode.text = data.couponCode
    //            }
    //            if((data.couponDescription?.containsString("Steps"))!) {
    //                couponDescription.numberOfLines = 2
    //                readMore.hidden = false
    //            } else {
    //                couponDescription.numberOfLines = 0
    //                readMore.hidden = true
    //            }
    //            couponExpiryDate.text = "Expires on: "+removeYearFromDate(resultString)
    //            couponDescription.text = data.couponDescription
    //            setNeedsLayout()
    //        }
    //    }
    
    class MyAccountViewController: BaseScrollViewController,PGTransactionDelegate
    {
        public func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
            AlertController.showAlertForMessage(ERROR_MESSAGE_STRING)
                      self.dismiss(animated: true, completion: nil)
        }
        
    public func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        AlertController.showAlertForMessage(ERROR_MESSAGE_STRING)
                  self.dismiss(animated: true, completion: nil)
        }
        
    let transactionValueButton_encash = UIButton()
        let transactionValueButton = UIButton()
        let encashView  = UIView()
        let rechargeView = UIView()
        //let couponView = UIView()
        let resultView = UIView()
        let segmentEncash_Recharge = HMSegmentedControl()
        let pointTextBox = RATextField()
        let existingValueLabel = UILabel()
        let redeemableValueLabel = UILabel()
        let bonusValueLabel = UILabel()
        let bonusValueLabel_encash = UILabel()
        let totalValueLabel_encash = UILabel()
        let totalValueLabel_recharge = UILabel()
        let rechanrgeAmountLabel = UILabel()
        
        let resultLabel = UILabel()
        
        let rechargeTo50_Button = UIButton()
        let rechargeTo100_Button = UIButton()
        let rechargeTo150_Button = UIButton()
        let rechargeTo200_Button = UIButton()
        
        let rechargeButton = UIButton()
        let radioButtonInstance = DLRadioButton()
        
        let encashButton = UIButton()
        
        var selectedAmount : NSString!
        //var bonusPoints = 0
        //        var offersPromotionsData = [OffersPromotionsData]()
        //        let table = UITableView()
        var comingFrom = ""
        
        override func viewDidLoad()
        {
            super.viewDidLoad()
            
            self.navigationController?.navigationBar.isHidden = false
            self.navigationItem.title = "My Account"
            self.view.backgroundColor = Colors.WHITE_COLOR
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
            
            
            createMyAccountBaseView()
            //            if(comingFrom == "notification") {
            //                couponView.hidden = false
            //                createCouponView()
            //            } else {
            //                getOffersPromotions()
            //            }
            createRechargeView()
            createEncashView()
            createResultView()
            getUserPointsData()
            //table.tableFooterView = UIView()
        }
        
        //        func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //            return offersPromotionsData.count
        //        }
        //
        //        func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //            let cell = tableView.dequeueReusableCellWithIdentifier("myaccountcell") as! MyAccountCell
        //            cell.data = offersPromotionsData[indexPath.row]
        //            cell.selectionStyle = UITableViewCellSelectionStyle.None
        //            cell.onUserActionBlock = { (selectedAction) -> Void in
        //                self.cell_btnAction(selectedAction as! UILabel, indexPath: indexPath)
        //            }
        //            return cell
        //        }
        //
        //        func cell_btnAction(action: UILabel, indexPath: NSIndexPath)
        //        {
        //            action.numberOfLines = 0
        //            action.sizeToFit()
        //        }
        //
        //        func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //            if(offersPromotionsData[indexPath.row].couponCode == "RAF") {
        //                NSNotificationCenter.defaultCenter().postNotificationName("shareButton", object: nil)
        //            }
        //        }
        //
        //        func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        //            cell.backgroundColor = Colors.WHITE_COLOR
        //        }
        //
        func createMyAccountBaseView() -> Void
        {
            self.segmentEncash_Recharge.backgroundColor = UIColor.clear
            self.segmentEncash_Recharge.sectionTitles = ["RECHARGE", "ENCASH"]
            self.segmentEncash_Recharge.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.GREEN_COLOR]
            self.segmentEncash_Recharge.selectedTitleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
            self.segmentEncash_Recharge.selectionStyle = HMSegmentedControlSelectionStyleBox
            self.segmentEncash_Recharge.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone
            self.segmentEncash_Recharge.selectionIndicatorColor = Colors.GREEN_COLOR
            self.segmentEncash_Recharge.selectionIndicatorBoxOpacity = 1.0
            self.segmentEncash_Recharge.translatesAutoresizingMaskIntoConstraints = false
            self.segmentEncash_Recharge.segmentEdgeInset = UIEdgeInsets.zero
            //            if(comingFrom == "notification") {
            //                self.segmentEncash_Recharge.selectedSegmentIndex = 2
            //            } else {
            self.segmentEncash_Recharge.selectedSegmentIndex = 0
            //}
            self.segmentEncash_Recharge.addTarget(self, action: #selector(segmentTypeChanged), for: .valueChanged)
            self.view.addSubview(self.segmentEncash_Recharge)
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[segEncash_Recharge(40)]", options:[], metrics:nil, views: ["segEncash_Recharge": self.segmentEncash_Recharge]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[segEncash_Recharge]|", options:[], metrics:nil, views: ["segEncash_Recharge": self.segmentEncash_Recharge]))
            
            
            self.encashView.backgroundColor = Colors.WHITE_COLOR
            self.encashView.isHidden = true
            self.encashView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.encashView)
            
            self.rechargeView.backgroundColor = Colors.WHITE_COLOR
            self.rechargeView.isHidden = false
            self.rechargeView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.rechargeView)
            
            //            self.couponView.backgroundColor = Colors.WHITE_COLOR
            //            self.couponView.hidden = true
            //            self.couponView.translatesAutoresizingMaskIntoConstraints = false
            //            self.view.addSubview(self.couponView)
            
            
            self.resultView.backgroundColor = Colors.WHITE_COLOR
            self.resultView.isHidden = true
            self.resultView.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(self.resultView)
            
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[encashView]|", options:[], metrics:nil, views: ["encashView": self.encashView]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[encashView]|", options:[], metrics:nil, views: ["encashView": self.encashView]))
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[rechargeView]|", options:[], metrics:nil, views: ["rechargeView": self.rechargeView]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[rechargeView]|", options:[], metrics:nil, views: ["rechargeView": self.rechargeView]))
            
            //            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-100-[couponView]|", options:[], metrics:nil, views: ["couponView": self.couponView]))
            //            self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[couponView]|", options:[], metrics:nil, views: ["couponView": self.couponView]))
            
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-100-[resultView(230)]", options:[], metrics:nil, views: ["resultView": self.resultView]))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[resultView]-15-|", options:[], metrics:nil, views: ["resultView": self.resultView]))
        }
        
        
        func createResultView() -> Void
        {
            
            self.resultView.layer.borderWidth = 1.0
            self.resultView.layer.borderColor = UIColor.lightGray.cgColor
            self.resultView.layer.cornerRadius = 5.0
            
            let tickIcon = UIImageView()
            tickIcon.image = UIImage(named: "Tickmark")
            tickIcon.translatesAutoresizingMaskIntoConstraints = false
            self.resultView.addSubview(tickIcon)
            
            
            let titleLabel = UILabel()
            titleLabel.text = "Payment Successful !"
            titleLabel.textAlignment = NSTextAlignment.center
            titleLabel.translatesAutoresizingMaskIntoConstraints = false
            titleLabel.font = boldFontWithSize(22)
            titleLabel.textColor = getColorByHex(0x617F23, alpha: 1.0)
            self.resultView.addSubview(titleLabel)
            
            resultLabel.textAlignment = NSTextAlignment.center
            resultLabel.translatesAutoresizingMaskIntoConstraints = false
            resultLabel.numberOfLines = 0
            resultLabel.clipsToBounds = true;
            resultLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            resultLabel.font = normalFontWithSize(17)
            resultLabel.textColor = Colors.GRAY_COLOR
            self.resultView.addSubview(resultLabel)
            
            let bottomLabel = UILabel()
            bottomLabel.text = "Thank you for doing business with RideAlly."
            bottomLabel.textAlignment = NSTextAlignment.center
            bottomLabel.translatesAutoresizingMaskIntoConstraints = false
            bottomLabel.font = normalFontWithSize(15)
            bottomLabel.textColor = Colors.GRAY_COLOR
            self.resultView.addSubview(bottomLabel)
            
            
            self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[tick(20)]-10-[titleLbl(30)]-20-[resultLbl(100)]-5-[bottomLbl(20)]", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel,"tick": tickIcon]))
            
            //self.resultView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[tick]-50-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel,"tick": tickIcon]))
            
            self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-50-[titleLbl]-50-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel]))
            self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[resultLbl]-20-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel]))
            self.resultView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[bottomLbl]-20-|", options:[], metrics:nil, views: ["titleLbl": titleLabel,"resultLbl": resultLabel,"bottomLbl": bottomLabel]))
            
            self.resultView.addConstraint(NSLayoutConstraint(item: tickIcon, attribute: .centerX, relatedBy: .equal, toItem: self.resultView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        }
        
        func createRechargeView() -> Void
        {
            existingValueLabel.textAlignment = NSTextAlignment.left
            existingValueLabel.translatesAutoresizingMaskIntoConstraints = false
            existingValueLabel.font = normalFontWithSize(20)
            existingValueLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(existingValueLabel)
            
            let pointsDespLabel = UILabel()
            pointsDespLabel.text = "Existing"
            pointsDespLabel.textAlignment = NSTextAlignment.left
            pointsDespLabel.translatesAutoresizingMaskIntoConstraints = false
            pointsDespLabel.font = normalFontWithSize(14)
            pointsDespLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(pointsDespLabel)
            
            let plusLabel = UILabel()
            plusLabel.text = "+"
            plusLabel.textAlignment = NSTextAlignment.left
            plusLabel.translatesAutoresizingMaskIntoConstraints = false
            plusLabel.font = normalFontWithSize(14)
            plusLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(plusLabel)
            
            bonusValueLabel.textAlignment = NSTextAlignment.left
            bonusValueLabel.translatesAutoresizingMaskIntoConstraints = false
            bonusValueLabel.font = normalFontWithSize(20)
            bonusValueLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(bonusValueLabel)
            
            let redeemDespLabel = UILabel()
            redeemDespLabel.text = "Bonus"
            redeemDespLabel.textAlignment = NSTextAlignment.left
            redeemDespLabel.translatesAutoresizingMaskIntoConstraints = false
            redeemDespLabel.font = normalFontWithSize(14)
            redeemDespLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(redeemDespLabel)
            
            let equalsToLabel = UILabel()
            equalsToLabel.text = "="
            equalsToLabel.textAlignment = NSTextAlignment.left
            equalsToLabel.translatesAutoresizingMaskIntoConstraints = false
            equalsToLabel.font = normalFontWithSize(14)
            equalsToLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(equalsToLabel)
            
            totalValueLabel_recharge.textAlignment = NSTextAlignment.left
            totalValueLabel_recharge.translatesAutoresizingMaskIntoConstraints = false
            totalValueLabel_recharge.font = normalFontWithSize(20)
            totalValueLabel_recharge.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(totalValueLabel_recharge)
            
            let totalDespLabel = UILabel()
            totalDespLabel.text = "Total Points"
            totalDespLabel.textAlignment = NSTextAlignment.left
            totalDespLabel.translatesAutoresizingMaskIntoConstraints = false
            totalDespLabel.font = normalFontWithSize(14)
            totalDespLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(totalDespLabel)
            
            
            transactionValueButton.setImage(UIImage(named: "transaction"), for: UIControlState())
            transactionValueButton.setImage(UIImage(named: "transaction"), for: .highlighted)
            transactionValueButton.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            transactionValueButton.translatesAutoresizingMaskIntoConstraints = false
            transactionValueButton.isHidden = true
            self.rechargeView.addSubview(transactionValueButton)
            
            let transactionLabel = UILabel()
            transactionLabel.text = "Transaction History"
            transactionLabel.textAlignment = NSTextAlignment.center
            transactionLabel.translatesAutoresizingMaskIntoConstraints = false
            transactionLabel.font = normalFontWithSize(14)
            transactionLabel.textColor = Colors.GREEN_COLOR
            transactionLabel.isUserInteractionEnabled = true
            let tapGesture = UITapGestureRecognizer(target:self, action:#selector(transactionHistory))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            transactionLabel.addGestureRecognizer(tapGesture)
            self.rechargeView.addSubview(transactionLabel)
            
            let lineSeperator = UIView()
            lineSeperator.backgroundColor = Colors.GRAY_COLOR
            lineSeperator.translatesAutoresizingMaskIntoConstraints = false
            self.rechargeView.addSubview(lineSeperator)
            
            let rechargeDespLabel = UILabel()
            rechargeDespLabel.text = "Recharge to avail rides"
            rechargeDespLabel.textAlignment = NSTextAlignment.center
            rechargeDespLabel.translatesAutoresizingMaskIntoConstraints = false
            rechargeDespLabel.font = normalFontWithSize(16)
            rechargeDespLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(rechargeDespLabel)
            
            
            rechargeTo50_Button.backgroundColor = UIColor.clear
            rechargeTo50_Button.translatesAutoresizingMaskIntoConstraints = false
            rechargeTo50_Button.titleLabel?.font = normalFontWithSize(20)
            rechargeTo50_Button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 1);
            rechargeTo50_Button.titleEdgeInsets = UIEdgeInsetsMake(0, 1, 0, 0);
            rechargeTo50_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
            rechargeTo50_Button.setTitle("50", for: UIControlState())
            rechargeTo50_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
            rechargeTo50_Button.setTitleColor(Colors.GRAY_COLOR, for: .highlighted)
            rechargeTo50_Button.layer.borderWidth = 2.0
            rechargeTo50_Button.layer.masksToBounds = true
            rechargeTo50_Button.layer.borderColor = Colors.GREEN_COLOR.cgColor
            rechargeTo50_Button.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            self.rechargeView.addSubview(rechargeTo50_Button)
            
            
            rechargeTo100_Button.backgroundColor = UIColor.clear
            rechargeTo100_Button.translatesAutoresizingMaskIntoConstraints = false
            rechargeTo100_Button.titleLabel?.font = normalFontWithSize(20)
            rechargeTo100_Button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 1);
            rechargeTo100_Button.titleEdgeInsets = UIEdgeInsetsMake(0, 1, 0, 0);
            rechargeTo100_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
            rechargeTo100_Button.setTitle("100", for: UIControlState())
            rechargeTo100_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
            rechargeTo100_Button.setTitleColor(Colors.GRAY_COLOR, for: .highlighted)
            rechargeTo100_Button.layer.borderWidth = 2.0
            rechargeTo100_Button.layer.masksToBounds = true
            rechargeTo100_Button.layer.borderColor = Colors.GREEN_COLOR.cgColor
            rechargeTo100_Button.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            self.rechargeView.addSubview(rechargeTo100_Button)
            
            
            rechargeTo150_Button.backgroundColor = UIColor.clear
            rechargeTo150_Button.translatesAutoresizingMaskIntoConstraints = false
            rechargeTo150_Button.titleLabel?.font = normalFontWithSize(20)
            rechargeTo150_Button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 1);
            rechargeTo150_Button.titleEdgeInsets = UIEdgeInsetsMake(0, 1, 0, 0);
            rechargeTo150_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
            rechargeTo150_Button.setTitle("200", for: UIControlState())
            rechargeTo150_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
            rechargeTo150_Button.setTitleColor(Colors.GRAY_COLOR, for: .highlighted)
            rechargeTo150_Button.layer.borderWidth = 2.0
            rechargeTo150_Button.layer.masksToBounds = true
            rechargeTo150_Button.layer.borderColor = Colors.GREEN_COLOR.cgColor
            rechargeTo150_Button.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            self.rechargeView.addSubview(rechargeTo150_Button)
            
            
            rechargeTo200_Button.backgroundColor = UIColor.clear
            rechargeTo200_Button.translatesAutoresizingMaskIntoConstraints = false
            rechargeTo200_Button.titleLabel?.font = normalFontWithSize(20)
            rechargeTo200_Button.setTitle("500", for: UIControlState())
            rechargeTo200_Button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 1);
            rechargeTo200_Button.titleEdgeInsets = UIEdgeInsetsMake(0, 1, 0, 0);
            rechargeTo200_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
            rechargeTo200_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
            rechargeTo200_Button.setTitleColor(Colors.GRAY_COLOR, for: .highlighted)
            rechargeTo200_Button.layer.borderWidth = 2.0
            rechargeTo200_Button.layer.masksToBounds = true
            rechargeTo200_Button.layer.borderColor = Colors.GREEN_COLOR.cgColor
            rechargeTo200_Button.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            self.rechargeView.addSubview(rechargeTo200_Button)
            
            let PaytmDespLabel = UILabel()
            PaytmDespLabel.text = "* 2.5% Paytm transaction charges."
            PaytmDespLabel.textAlignment = NSTextAlignment.right
            PaytmDespLabel.translatesAutoresizingMaskIntoConstraints = false
            PaytmDespLabel.font = normalFontWithSize(16)
            PaytmDespLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(PaytmDespLabel)
            
            
            rechanrgeAmountLabel.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
            rechanrgeAmountLabel.layer.borderWidth = 1.0
            rechanrgeAmountLabel.text = "Amount to recharge"
            rechanrgeAmountLabel.textAlignment = NSTextAlignment.center
            rechanrgeAmountLabel.translatesAutoresizingMaskIntoConstraints = false
            rechanrgeAmountLabel.font = normalFontWithSize(15)
            rechanrgeAmountLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(rechanrgeAmountLabel)
            
            let rechargedetailLbl = UILabel()
            rechargedetailLbl.text = "1 Rs = 1 Point"
            rechargedetailLbl.textAlignment = NSTextAlignment.right
            rechargedetailLbl.translatesAutoresizingMaskIntoConstraints = false
            rechargedetailLbl.font = normalFontWithSize(12)
            rechargedetailLbl.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(rechargedetailLbl)
            
            let rechargeViaLabel = UILabel()
            rechargeViaLabel.text = "Recharge Via"
            rechargeViaLabel.textAlignment = NSTextAlignment.left
            rechargeViaLabel.translatesAutoresizingMaskIntoConstraints = false
            rechargeViaLabel.font = boldFontWithSize(15)
            rechargeViaLabel.textColor = Colors.GRAY_COLOR
            self.rechargeView.addSubview(rechargeViaLabel)
            
            
            //radioButtonInstance.setTitle("", forState: .Normal)
            radioButtonInstance.isSelected = true
            radioButtonInstance.translatesAutoresizingMaskIntoConstraints = false
            radioButtonInstance.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
            radioButtonInstance.iconColor = Colors.GREEN_COLOR
            radioButtonInstance.indicatorColor = Colors.GREEN_COLOR
            radioButtonInstance.titleLabel?.font = boldFontWithSize(15)
            self.rechargeView.addSubview(radioButtonInstance)
            
            
            let imageView_payTM = UIImageView()
            imageView_payTM.translatesAutoresizingMaskIntoConstraints = false
            imageView_payTM.image = UIImage(named: "S_paytm")
            self.rechargeView.addSubview(imageView_payTM)
            
            
            rechargeButton.titleLabel?.font = boldFontWithSize(15)
            rechargeButton.backgroundColor = Colors.GREEN_COLOR
            rechargeButton.translatesAutoresizingMaskIntoConstraints = false
            rechargeButton.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            rechargeButton.setTitle("RECHARGE", for: UIControlState())
            rechargeButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
            rechargeButton.setTitleColor(Colors.GRAY_COLOR, for: .highlighted)
            self.rechargeView.addSubview(rechargeButton)
            
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[pointsValueLbl(20)]-5-[pointsValueDesp(12)]-10-[transactionLbl]-5-[line(1)]-10-[rechargeDespLbl(20)]-20-[rechargeTo50_Btn]-15-[PaytmDespLbl(20)][rechanrgeAmountLbl(40)]-5-[rechargedetail(15)]", options:[], metrics:nil, views: ["pointsValueLbl": existingValueLabel,"pointsValueDesp": pointsDespLabel,"line": lineSeperator,"rechargeDespLbl": rechargeDespLabel,"rechargeTo50_Btn": rechargeTo50_Button,"PaytmDespLbl":PaytmDespLabel,"rechanrgeAmountLbl": rechanrgeAmountLabel,"rechargedetail":rechargedetailLbl,"rechargeViaLbl":rechargeViaLabel,"radioButton":radioButtonInstance,"rechargeButton": rechargeButton,"paytmIcon":imageView_payTM,"transactionLbl":transactionLabel]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[rechargeViaLbl(20)]-10-[radioButton(15)]-10-[rechargeButton(40)]-10-|", options:[], metrics:nil, views: ["pointsValueLbl": existingValueLabel,"pointsValueDesp": pointsDespLabel,"line": lineSeperator,"rechargeDespLbl": rechargeDespLabel,"rechargeTo50_Btn": rechargeTo50_Button,"PaytmDespLbl":PaytmDespLabel,"rechanrgeAmountLbl": rechanrgeAmountLabel,"rechargedetail":rechargedetailLbl,"rechargeViaLbl":rechargeViaLabel,"radioButton":radioButtonInstance,"rechargeButton": rechargeButton,"paytmIcon":imageView_payTM]))
            
            
            //self.rechargeView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[transactionValueBtn]-2-[transactionLbl]", options:[], metrics:nil, views: ["transactionValueBtn": transactionValueButton,"transactionLbl":transactionLabel]))
            //self.rechargeView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[pointsValueLbl(100)]", options:[], metrics:nil, views: ["pointsValueLbl": pointsValueLabel,"pointsValueDesp": pointsDespLabel,"line": lineSeperator]))
            
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[pointsValueLbl(50)]-padding-[plusLbl(8)]-padding-[redeemValueLbl(==pointsValueLbl)]-padding-[equalLbl(8)]-padding-[totalValueLbl(==pointsValueLbl)]", options:[], metrics:["padding":(self.view.frame.size.width/10)-5], views: ["pointsValueLbl": existingValueLabel,"redeemValueLbl": bonusValueLabel,"totalValueLbl":totalValueLabel_recharge,"plusLbl":plusLabel,"equalLbl":equalsToLabel]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[pointsValueDesp(50)]-padding-[redeemDespLbl(40)]-padding-[totalDespLbl(200)]", options:[], metrics:["padding":(self.view.frame.size.width/6)], views: ["pointsValueDesp": pointsDespLabel,"redeemDespLbl": redeemDespLabel,"totalDespLbl":totalDespLabel]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[transactionLbl(120)]-10-|", options: [], metrics: nil, views: ["transactionLbl":transactionLabel]))
            
            //self.rechargeView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-50-[pointsValueDesp(40)]", options:[], metrics:nil, views: ["pointsValueLbl": pointsValueLabel,"pointsValueDesp": pointsDespLabel,"line": lineSeperator]))
            //            self.rechargeView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[transactionValueBtn(100)]-20-|", options:[], metrics:nil, views: ["transactionValueBtn": transactionValueButton]))
            //            self.rechargeView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[transactionLbl]-20-|", options:[], metrics:nil, views: ["transactionLbl": transactionLabel]))
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options:[], metrics:nil, views: ["line": lineSeperator]))
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[rechargeDespLbl]|", options:[], metrics:nil, views: ["rechargeDespLbl": rechargeDespLabel]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[PaytmDespLbl(250)]-25-|", options:[], metrics:nil, views: ["PaytmDespLbl": PaytmDespLabel]))
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[rechargedetail(100)]-25-|", options:[], metrics:nil, views: ["rechargedetail": rechargedetailLbl]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[rechanrgeAmountLbl]-25-|", options:[], metrics:nil, views: ["rechanrgeAmountLbl": rechanrgeAmountLabel]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[rechargeViaLbl]", options:[], metrics:nil, views: ["rechargeViaLbl": rechargeViaLabel]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[radioButton]-5-[paytmIcon(50)]", options:[], metrics:nil, views: ["radioButton": radioButtonInstance,"paytmIcon":imageView_payTM]))
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[rechargeButton]-25-|", options:[], metrics:nil, views: ["rechargeButton": rechargeButton]))
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[paytmIcon(20)]", options:[], metrics:nil, views: ["paytmIcon":imageView_payTM]))
            
            
            
            self.rechargeView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[rechargeTo50_Btn]-padding-[rechargeTo100_Btn(==rechargeTo50_Btn)]-padding-[rechargeTo150_Btn(==rechargeTo50_Btn)]-padding-[rechargeTo200_Btn(==rechargeTo50_Btn)]-25-|", options:[], metrics:["padding":15], views: ["rechargeTo50_Btn": rechargeTo50_Button,"rechargeTo100_Btn": rechargeTo100_Button,"rechargeTo150_Btn": rechargeTo150_Button,"rechargeTo200_Btn": rechargeTo200_Button]))
            
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: imageView_payTM, attribute: .centerY, relatedBy: .equal, toItem: radioButtonInstance, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: rechargeTo50_Button, attribute: .height, relatedBy: .equal, toItem: rechargeTo50_Button, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: rechargeTo100_Button, attribute: .centerY, relatedBy: .equal, toItem: rechargeTo50_Button, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: rechargeTo100_Button, attribute: .height, relatedBy: .equal, toItem: rechargeTo50_Button, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: rechargeTo150_Button, attribute: .centerY, relatedBy: .equal, toItem: rechargeTo50_Button, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: rechargeTo150_Button, attribute: .height, relatedBy: .equal, toItem: rechargeTo50_Button, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: rechargeTo200_Button, attribute: .centerY, relatedBy: .equal, toItem: rechargeTo50_Button, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: rechargeTo200_Button, attribute: .height, relatedBy: .equal, toItem: rechargeTo50_Button, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            //self.rechargeView.addConstraint(NSLayoutConstraint(item: transactionValueButton, attribute: .CenterY, relatedBy: .Equal, toItem: pointsValueLabel, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
            
            
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: bonusValueLabel, attribute: .centerY, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: bonusValueLabel, attribute: .height, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: totalValueLabel_recharge, attribute: .centerY, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: totalValueLabel_recharge, attribute: .height, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: plusLabel, attribute: .centerY, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: plusLabel, attribute: .height, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: equalsToLabel, attribute: .centerY, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: equalsToLabel, attribute: .height, relatedBy: .equal, toItem: existingValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: redeemDespLabel, attribute: .centerY, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: redeemDespLabel, attribute: .height, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            self.rechargeView.addConstraint(NSLayoutConstraint(item: totalDespLabel, attribute: .centerY, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.rechargeView.addConstraint(NSLayoutConstraint(item: totalDespLabel, attribute: .height, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            
        }
        
        
        func createEncashView() -> Void
        {
            
            redeemableValueLabel.textAlignment = NSTextAlignment.left
            redeemableValueLabel.translatesAutoresizingMaskIntoConstraints = false
            redeemableValueLabel.font = normalFontWithSize(20)
            redeemableValueLabel.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(redeemableValueLabel)
            
            let pointsDespLabel = UILabel()
            pointsDespLabel.text = "Redeemable"
            pointsDespLabel.textAlignment = NSTextAlignment.left
            pointsDespLabel.translatesAutoresizingMaskIntoConstraints = false
            pointsDespLabel.font = normalFontWithSize(14)
            pointsDespLabel.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(pointsDespLabel)
            
            let plusLabel = UILabel()
            plusLabel.text = "+"
            plusLabel.textAlignment = NSTextAlignment.left
            plusLabel.translatesAutoresizingMaskIntoConstraints = false
            plusLabel.font = normalFontWithSize(14)
            plusLabel.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(plusLabel)
            
            bonusValueLabel_encash.textAlignment = NSTextAlignment.left
            bonusValueLabel_encash.translatesAutoresizingMaskIntoConstraints = false
            bonusValueLabel_encash.font = normalFontWithSize(20)
            bonusValueLabel_encash.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(bonusValueLabel_encash)
            
            let redeemDespLabel = UILabel()
            redeemDespLabel.text = "Bonus"
            redeemDespLabel.textAlignment = NSTextAlignment.left
            redeemDespLabel.translatesAutoresizingMaskIntoConstraints = false
            redeemDespLabel.font = normalFontWithSize(14)
            redeemDespLabel.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(redeemDespLabel)
            
            let equalsToLabel = UILabel()
            equalsToLabel.text = "="
            equalsToLabel.textAlignment = NSTextAlignment.left
            equalsToLabel.translatesAutoresizingMaskIntoConstraints = false
            equalsToLabel.font = normalFontWithSize(14)
            equalsToLabel.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(equalsToLabel)
            
            totalValueLabel_encash.textAlignment = NSTextAlignment.left
            totalValueLabel_encash.translatesAutoresizingMaskIntoConstraints = false
            totalValueLabel_encash.font = normalFontWithSize(20)
            totalValueLabel_encash.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(totalValueLabel_encash)
            
            let totalDespLabel = UILabel()
            totalDespLabel.text = "Total Points"
            totalDespLabel.textAlignment = NSTextAlignment.left
            totalDespLabel.translatesAutoresizingMaskIntoConstraints = false
            totalDespLabel.font = normalFontWithSize(14)
            totalDespLabel.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(totalDespLabel)
            
            transactionValueButton_encash.setImage(UIImage(named: "transaction"), for: UIControlState())
            transactionValueButton_encash.setImage(UIImage(named: "transaction"), for: .highlighted)
            transactionValueButton_encash.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            transactionValueButton_encash.translatesAutoresizingMaskIntoConstraints = false
            transactionValueButton.isHidden = true
            self.encashView.addSubview(transactionValueButton_encash)
            
            let transactionLabel = UILabel()
            transactionLabel.text = "Transaction History"
            transactionLabel.textAlignment = NSTextAlignment.center
            transactionLabel.translatesAutoresizingMaskIntoConstraints = false
            transactionLabel.font = normalFontWithSize(14)
            transactionLabel.textColor = Colors.GREEN_COLOR
            transactionLabel.isUserInteractionEnabled = true
            let tapGesture1 = UITapGestureRecognizer(target:self, action:#selector(transactionHistory))
            tapGesture1.numberOfTapsRequired = 1
            tapGesture1.numberOfTouchesRequired = 1
            transactionLabel.addGestureRecognizer(tapGesture1)
            self.encashView.addSubview(transactionLabel)
            
            let lineSeperator = UIView()
            lineSeperator.backgroundColor = Colors.GRAY_COLOR
            lineSeperator.translatesAutoresizingMaskIntoConstraints = false
            self.encashView.addSubview(lineSeperator)
            
            
            let encashPointDespLabel = UILabel()
            encashPointDespLabel.text = "Go ahead and encash your points"
            encashPointDespLabel.textAlignment = NSTextAlignment.left
            encashPointDespLabel.translatesAutoresizingMaskIntoConstraints = false
            encashPointDespLabel.font = normalFontWithSize(15)
            encashPointDespLabel.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(encashPointDespLabel)
            
            
            pointTextBox.placeholder =  "Enter number of points to encash (min 550)"
            pointTextBox.layer.borderWidth = 1.0
            pointTextBox.delegate = self
            pointTextBox.returnKeyType = .done
            pointTextBox.font = normalFontWithSize(14)
            pointTextBox.textColor = Colors.GRAY_COLOR
            pointTextBox.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
            pointTextBox.translatesAutoresizingMaskIntoConstraints = false
            self.encashView.addSubview(pointTextBox)
            
            
            let rechargedetailLbl = UILabel()
            rechargedetailLbl.text = "1 Point = 1 Rs"
            rechargedetailLbl.textAlignment = NSTextAlignment.right
            rechargedetailLbl.translatesAutoresizingMaskIntoConstraints = false
            rechargedetailLbl.font = normalFontWithSize(12)
            rechargedetailLbl.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(rechargedetailLbl)
            
            let encashTCLbl = UILabel()
            encashTCLbl.text = "Terms & Conditions"
            encashTCLbl.textAlignment = NSTextAlignment.left
            encashTCLbl.translatesAutoresizingMaskIntoConstraints = false
            encashTCLbl.isUserInteractionEnabled = true
            encashTCLbl.font = normalFontWithSize(14)
            encashTCLbl.textColor = Colors.GREEN_COLOR
            self.encashView.addSubview(encashTCLbl)
            let tapGesture = UITapGestureRecognizer(target:self, action:#selector(showTermsCond))
            tapGesture.numberOfTapsRequired = 1
            tapGesture.numberOfTouchesRequired = 1
            encashTCLbl.addGestureRecognizer(tapGesture)
            
            encashButton.titleLabel?.font = boldFontWithSize(15)
            encashButton.backgroundColor = Colors.GREEN_COLOR
            encashButton.translatesAutoresizingMaskIntoConstraints = false
            encashButton.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
            encashButton.setTitle("ENCASH", for: UIControlState())
            encashButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
            encashButton.setTitleColor(Colors.GRAY_COLOR, for: .highlighted)
            self.encashView.addSubview(encashButton)
            
            let lineSeperator1 = UIView()
            lineSeperator1.backgroundColor = Colors.GRAY_COLOR
            lineSeperator1.translatesAutoresizingMaskIntoConstraints = false
            self.encashView.addSubview(lineSeperator1)
            
            
            let imageView_encashCard = UIImageView()
            imageView_encashCard.translatesAutoresizingMaskIntoConstraints = false
            imageView_encashCard.image = UIImage(named: "petroCard")
            self.encashView.addSubview(imageView_encashCard)
            
            let encashCardDetail = UILabel()
            encashCardDetail.text = "RideAlly will issue petro card for every rider."
            encashCardDetail.lineBreakMode = NSLineBreakMode.byWordWrapping
            encashCardDetail.numberOfLines = 0
            encashCardDetail.textAlignment = NSTextAlignment.left
            encashCardDetail.translatesAutoresizingMaskIntoConstraints = false
            encashCardDetail.font = normalFontWithSize(14)
            encashCardDetail.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(encashCardDetail)
            
            
            let imageView_paytmcard = UIImageView()
            imageView_paytmcard.translatesAutoresizingMaskIntoConstraints = false
            imageView_paytmcard.image = UIImage(named: "PaytmCard")
            self.encashView.addSubview(imageView_paytmcard)
            
            
            let payTMDetail = UILabel()
            payTMDetail.text = "Rider can transfer their points to paytm wallet."
            payTMDetail.lineBreakMode = NSLineBreakMode.byWordWrapping
            payTMDetail.numberOfLines = 0
            payTMDetail.textAlignment = NSTextAlignment.left
            payTMDetail.translatesAutoresizingMaskIntoConstraints = false
            payTMDetail.font = normalFontWithSize(14)
            payTMDetail.textColor = Colors.GRAY_COLOR
            self.encashView.addSubview(payTMDetail)
            
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[pointsValueLbl(20)]-5-[pointsValueDesp(10)]-10-[transactionLbl]-5-[line(1)]-15-[encashPointDespLbl(20)]-10-[pointTxtBox(40)]-5-[rechargedetail]-20-[enchashTC]-5-[encashBtn(40)]-10-[lineSep(1)]-10-[imageView_encash(50)]-15-[imageView_paytm(==imageView_encash)]", options:[], metrics:nil, views: ["pointsValueLbl": redeemableValueLabel,"pointsValueDesp": pointsDespLabel,"line": lineSeperator,"encashPointDespLbl":encashPointDespLabel,"pointTxtBox":pointTextBox,"rechargedetail":rechargedetailLbl,"enchashTC":encashTCLbl,"encashBtn":encashButton,"lineSep":lineSeperator1,"imageView_encash":imageView_encashCard,"imageView_paytm":imageView_paytmcard,"transactionLbl":transactionLabel]))
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[pointsValueLbl(50)]-padding-[plusLbl(8)]-padding-[redeemValueLbl(==pointsValueLbl)]-padding-[equalLbl(8)]-padding-[totalValueLbl(==pointsValueLbl)]", options:[], metrics:["padding":(self.view.frame.size.width/10)-5], views: ["pointsValueLbl": redeemableValueLabel,"redeemValueLbl": bonusValueLabel_encash,"totalValueLbl":totalValueLabel_encash,"plusLbl":plusLabel,"equalLbl":equalsToLabel]))
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[pointsValueDesp(75)]-padding-[redeemDespLbl(40)]-padding-[totalDespLbl(200)]", options:[], metrics:["padding":(self.view.frame.size.width/6)-10], views: ["pointsValueDesp": pointsDespLabel,"redeemDespLbl": redeemDespLabel,"totalDespLbl":totalDespLabel]))
            
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[encashPointDespLbl]", options:[], metrics:nil, views: ["encashPointDespLbl": encashPointDespLabel]))
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[transactionLbl(120)]-10-|", options: [], metrics: nil, views: ["transactionLbl":transactionLabel]))
            //
            //            self.encashView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[transactionValueBtn]-2-[transactionLbl]", options:[], metrics:nil, views: ["transactionValueBtn": transactionValueButton_encash,"transactionLbl":transactionLabel]))
            //
            //            self.encashView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[transactionValueBtn(100)]-10-|", options:[], metrics:nil, views: ["transactionValueBtn": transactionValueButton_encash]))
            //
            //            self.encashView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[transactionLbl]-10-|", options:[], metrics:nil, views: ["transactionLbl": transactionLabel]))
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options:[], metrics:nil, views: ["line": lineSeperator]))
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[pointTxtBox]-25-|", options:[], metrics:nil, views: ["pointTxtBox":pointTextBox]))
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[rechargedetail]-25-|", options:[], metrics:nil, views: ["rechargedetail":rechargedetailLbl]))
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[encashTC]-25-|", options:[], metrics:nil, views: ["encashTC":encashTCLbl]))
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[encashBtn]-25-|", options:[], metrics:nil, views: ["encashBtn":encashButton]))
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineSep]|", options:[], metrics:nil, views: ["lineSep": lineSeperator1]))
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[imageView_encash(100)]-10-[encashCard]-10-|", options:[], metrics:nil, views: ["imageView_encash":imageView_encashCard,"encashCard":encashCardDetail]))
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-25-[imageView_paytm(100)]-10-[payTM]-10-|", options:[], metrics:nil, views: ["imageView_paytm":imageView_paytmcard,"payTM":payTMDetail]))
            
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[encashCard(40)]", options:[], metrics:nil, views: ["imageView_encash":imageView_encashCard,"encashCard":encashCardDetail]))
            self.encashView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[payTM(40)]", options:[], metrics:nil, views: ["imageView_encash":imageView_encashCard,"payTM":payTMDetail]))
            
            self.encashView.addConstraint(NSLayoutConstraint(item: bonusValueLabel_encash, attribute: .centerY, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.encashView.addConstraint(NSLayoutConstraint(item: bonusValueLabel_encash, attribute: .height, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            self.encashView.addConstraint(NSLayoutConstraint(item: totalValueLabel_encash, attribute: .centerY, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.encashView.addConstraint(NSLayoutConstraint(item: totalValueLabel_encash, attribute: .height, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            
            self.encashView.addConstraint(NSLayoutConstraint(item: plusLabel, attribute: .centerY, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.encashView.addConstraint(NSLayoutConstraint(item: plusLabel, attribute: .height, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            self.encashView.addConstraint(NSLayoutConstraint(item: equalsToLabel, attribute: .centerY, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.encashView.addConstraint(NSLayoutConstraint(item: equalsToLabel, attribute: .height, relatedBy: .equal, toItem: redeemableValueLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            
            self.encashView.addConstraint(NSLayoutConstraint(item: redeemDespLabel, attribute: .centerY, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.encashView.addConstraint(NSLayoutConstraint(item: redeemDespLabel, attribute: .height, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            self.encashView.addConstraint(NSLayoutConstraint(item: totalDespLabel, attribute: .centerY, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.encashView.addConstraint(NSLayoutConstraint(item: totalDespLabel, attribute: .height, relatedBy: .equal, toItem: pointsDespLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
            
            //            self.encashView.addConstraint(NSLayoutConstraint(item: transactionValueButton_encash, attribute: .CenterY, relatedBy: .Equal, toItem: pointsValueLabel_encash, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
            
            self.encashView.addConstraint(NSLayoutConstraint(item: encashCardDetail, attribute: .centerY, relatedBy: .equal, toItem: imageView_encashCard, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
            self.encashView.addConstraint(NSLayoutConstraint(item: payTMDetail, attribute: .centerY, relatedBy: .equal, toItem: imageView_paytmcard, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        }
        //        func createCouponView() -> Void
        //        {
        //            table.separatorColor = Colors.GRAY_COLOR
        //            table.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        //            table.delegate = self
        //            table.dataSource = self
        //            table.rowHeight = UITableViewAutomaticDimension
        //            table.estimatedRowHeight = 40
        //            table.translatesAutoresizingMaskIntoConstraints = false
        //            table.registerClass(MyAccountCell.self, forCellReuseIdentifier: "myaccountcell")
        //            self.couponView.addSubview(table)
        //            self.couponView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[table]|", options: [], metrics: nil, views:["table":table]))
        //            self.couponView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[table]|", options: [], metrics: nil, views:["table":table]))
        //        }
        //
        func buttonPressedEvent(_ sender:UIButton) -> Void
        {
            
            if sender ==  rechargeButton
            {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"My Account",
                    AnalyticsParameterItemName: "Recharge",
                    AnalyticsParameterContentType:"Recharge Button"
                    ])
                if  self.rechanrgeAmountLabel.text?.characters.count > 0
                {
                    if  self.radioButtonInstance.isSelected
                    {
                        self.configPaytmMerchant()
                    }
                    else
                    {
                        AlertController.showToastForError("Please select an paytm option.")
                    }
                }
                else
                {
                    AlertController.showToastForError("Please select an amount to recharge.")
                }
            }
            
            if sender == encashButton
            {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"My Account",
                    AnalyticsParameterItemName: "Encash",
                    AnalyticsParameterContentType:"Encash Button"
                    ])
                if  pointTextBox.text?.characters.count > 0
                {
                    if let value = Int(pointTextBox.text!)
                    {
                        if let redeemValue = Int(self.redeemableValueLabel.text!)
                        {
                            if(value < redeemValue || value == redeemValue) {
                                if value % 550 == 0
                                {
                                    let pointsEncashViewInstance = PointsEncashView()
                                    let alertViewHolder =   AlertContentViewHolder()
                                    alertViewHolder.heightConstraintValue = 200
                                    alertViewHolder.view = pointsEncashViewInstance
                                    
                                    AlertController.showAlertFor("Go and encash your points", message: "", contentView: alertViewHolder, okButtonTitle:"Submit", willHaveAutoDismiss: true, okAction:
                                        {
                                            
                                            if pointsEncashViewInstance.phoneTextField.isHidden == false
                                            {
                                                if pointsEncashViewInstance.phoneTextField.text?.characters.count == 10
                                                {
                                                    
                                                    let reqObj = GetRedeemPointsOnEncashRequest()
                                                    showIndicator("Loading...")
                                                    
                                                    reqObj.userID = UserInfo.sharedInstance.userID
                                                    reqObj.firstName = UserInfo.sharedInstance.firstName
                                                    reqObj.mobileNumber = UserInfo.sharedInstance.mobile
                                                    reqObj.emailID = UserInfo.sharedInstance.email
                                                    reqObj.transType = "encash"
                                                    reqObj.transAmt = String(Int(round(Double(self.pointTextBox.text!)! - (Double(self.pointTextBox.text!)!*0.06))))
                                                    reqObj.redeemAmt = self.pointTextBox.text
                                                    reqObj.redeemOpt = "paytm"
                                                    reqObj.cardType = ""
                                                    reqObj.redeemAddress = pointsEncashViewInstance.phoneTextField.text
                                                    
                                                    MyAccountRequestor().sendRedeemPointsRequest(reqObj, success:{ (success, object) in
                                                        hideIndicator()
                                                        
                                                        
                                                        if let data = object as? NSDictionary
                                                        {
                                                            if let keyCode = (data.object(forKey: "code")) as? NSString
                                                            {
                                                                
                                                                if keyCode == "2993"
                                                                {
                                                                    
                                                                    if self.redeemableValueLabel.text?.characters.count > 0
                                                                    {
                                                                        var pointValue  = Int(self.redeemableValueLabel.text!)
                                                                        let encashValue = Int(self.pointTextBox.text!)
                                                                        let bonusValue = Int(self.bonusValueLabel.text!)
                                                                        pointValue = pointValue! - encashValue!
                                                                        let totalPtVal = pointValue! + bonusValue!
                                                                        self.totalValueLabel_encash.text = "\(totalPtVal)"
                                                                        self.totalValueLabel_recharge.text = "\(totalPtVal)"
                                                                        self.redeemableValueLabel.text = "\(pointValue!)"
                                                                        self.existingValueLabel.text = "\(pointValue!)"
                                                                        UserInfo.sharedInstance.availablePoints = Int(self.totalValueLabel_encash.text!)!
                                                                        self.segmentTypeChanged()
                                                                    }
                                                                    
                                                                    self.pointTextBox.text = ""
                                                                    AlertController.showToastForInfo("Your transfer to paytm request has been received. The money will be transferred to your paytm wallet within 2 working days.")
                                                                }
                                                            }
                                                        }
                                                        
                                                    }){ (error) in
                                                        hideIndicator()
                                                        AlertController.showAlertForError(error)
                                                    }
                                                }
                                                else
                                                {
                                                    AlertController.showToastForError("Please provide 10 digit mobile number.")
                                                }
                                                
                                            }
                                            
                                            
                                            if pointsEncashViewInstance.addressTextField.isHidden == false
                                            {
                                                if pointsEncashViewInstance.addressTextField.text?.characters.count > 0
                                                {
                                                    
                                                    
                                                    if pointsEncashViewInstance.txtcardType.text == "Shell" || pointsEncashViewInstance.txtcardType.text == "BP"
                                                    {
                                                        let reqObj = GetRedeemPointsOnEncashRequest()
                                                        showIndicator("Loading...")
                                                        
                                                        reqObj.userID = UserInfo.sharedInstance.userID
                                                        reqObj.firstName = UserInfo.sharedInstance.firstName
                                                        reqObj.mobileNumber = UserInfo.sharedInstance.mobile
                                                        reqObj.emailID = UserInfo.sharedInstance.email
                                                        reqObj.transType = "encash"
                                                        reqObj.transAmt = String(Int(self.pointTextBox.text!)! - 50)
                                                        reqObj.redeemAmt = self.pointTextBox.text
                                                        reqObj.redeemOpt = "card"
                                                        reqObj.cardType = pointsEncashViewInstance.txtcardType.text
                                                        reqObj.redeemAddress = pointsEncashViewInstance.addressTextField.text
                                                        
                                                        MyAccountRequestor().sendRedeemPointsRequest(reqObj, success:{ (success, object) in
                                                            hideIndicator()
                                                            
                                                            
                                                            if self.totalValueLabel_encash.text?.characters.count > 0
                                                            {
                                                                var pointValue  = Int(self.totalValueLabel_encash.text!)
                                                                let encashValue = Int(self.pointTextBox.text!)
                                                                let bonusValue = Int(self.bonusValueLabel.text!)
                                                                pointValue = pointValue! - encashValue!
                                                                let totalPtVal = pointValue! + bonusValue!
                                                                self.totalValueLabel_encash.text = "\(totalPtVal)"
                                                                self.totalValueLabel_recharge.text = "\(totalPtVal)"
                                                                self.redeemableValueLabel.text = "\(pointValue!)"
                                                                self.existingValueLabel.text = "\(pointValue!)"
                                                                UserInfo.sharedInstance.availablePoints = Int(self.totalValueLabel_encash.text!)!
                                                                self.segmentTypeChanged()
                                                            }
                                                            
                                                            self.pointTextBox.text = ""
                                                            AlertController.showToastForInfo("Your encash request has been received. The petro card will be delivered to your address within 15 days.")
                                                            
                                                        }){ (error) in
                                                            hideIndicator()
                                                            AlertController.showAlertForError(error)
                                                        }
                                                    }
                                                    else
                                                    {
                                                        AlertController.showToastForError("Please select the card type.")
                                                    }
                                                    
                                                }
                                                else
                                                {
                                                    AlertController.showToastForError("Please provide your delivery address.")
                                                }
                                            }
                                        }
                                        , cancelButtonTitle: "Cancel", cancelAction:
                                        {
                                            
                                    })
                                }
                                else
                                {
                                    AlertController.showToastForError("Please select redeemable point multiple of 550 only.")
                                }
                            } else {
                                AlertController.showToastForError("Sorry, you dont have sufficient redeemable points.")
                            }
                        }
                    }
                    
                }
                else
                {
                    AlertController.showToastForError("Please select redeemable point multiple of 500 only.")//Sorry, you cannot encash as your redeemable points is zero
                }
                
            }
            
            if  sender == transactionValueButton || sender == transactionValueButton_encash
            {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"My Account",
                    AnalyticsParameterItemName: "Recharge and Encas",
                    AnalyticsParameterContentType:"Transaction Button"
                    ])
                let reqObj = GetUserPointsOnRecahargeRequest()
                showIndicator("Loading...")
                reqObj.userID =  UserInfo.sharedInstance.userID
                
                MyAccountRequestor().sendGetUserPointsRequest(reqObj, success:{ (success, object) in
                    hideIndicator()
                    let vc = PointsTableViewController()
                    vc.data = (object as! GetUserPointsOnRecahargeResponse).dataObj!
                    self.navigationController?.pushViewController(vc, animated: true)
                }){ (error) in
                    hideIndicator()
                    AlertController.showAlertForError(error)
                }
            }
            
            if  sender == self.rechargeTo50_Button || sender == self.rechargeTo100_Button || sender == self.rechargeTo150_Button || sender == self.rechargeTo200_Button
            {
                self.rechargeTo50_Button.isSelected = false
                self.rechargeTo100_Button.isSelected = false
                self.rechargeTo150_Button.isSelected = false
                self.rechargeTo200_Button.isSelected = false
                
                rechargeTo50_Button.backgroundColor = UIColor.clear
                rechargeTo100_Button.backgroundColor = UIColor.clear
                rechargeTo150_Button.backgroundColor = UIColor.clear
                rechargeTo200_Button.backgroundColor = UIColor.clear
                
                rechargeTo50_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
                rechargeTo100_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
                rechargeTo150_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
                rechargeTo200_Button.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
                
                rechargeTo50_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
                rechargeTo100_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
                rechargeTo150_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
                rechargeTo200_Button.setImage(UIImage(named: "rupee_green_big"), for: UIControlState())
                
                if  sender == self.rechargeTo50_Button
                {
                    self.selectedAmount = "50"
                    sender.isSelected = true
                    sender.backgroundColor = Colors.GREEN_COLOR
                    sender.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
                    sender.setImage(UIImage(named: "rupee_white_big"), for: UIControlState())
                    
                    let calValue : Double!
                    calValue = (50 + (50 * (2.5/100)))
                    self.rechanrgeAmountLabel.text =   "\(calValue)"
                    
                }
                else if sender == self.rechargeTo100_Button
                {
                    self.selectedAmount = "100"
                    sender.isSelected = true
                    sender.backgroundColor = Colors.GREEN_COLOR
                    sender.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
                    sender.setImage(UIImage(named: "rupee_white_big"), for: UIControlState())
                    
                    let calValue : Double!
                    calValue = (100 + (100 * (2.5/100)))
                    self.rechanrgeAmountLabel.text =   "\(calValue)"
                }
                else if sender == self.rechargeTo150_Button
                {
                    self.selectedAmount = "200"
                    sender.isSelected = true
                    sender.backgroundColor = Colors.GREEN_COLOR
                    sender.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
                    sender.setImage(UIImage(named: "rupee_white_big"), for: UIControlState())
                    
                    let calValue : Double!
                    calValue = (200 + (200 * (2.5/100)))
                    self.rechanrgeAmountLabel.text =   "\(calValue)"
                }
                else if sender == self.rechargeTo200_Button
                {
                    self.selectedAmount = "500"
                    sender.isSelected = true
                    sender.backgroundColor = Colors.GREEN_COLOR
                    sender.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
                    sender.setImage(UIImage(named: "rupee_white_big"), for: UIControlState())
                    
                    let calValue : Double!
                    calValue = (500 + (500 * (2.5/100)))
                    self.rechanrgeAmountLabel.text =   "\(calValue)"
                }
            }
            
        }
        
        func segmentTypeChanged() -> Void
        {
            //var redeemVal = "0"
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                self.encashView.isHidden = true
                self.rechargeView.isHidden = true
                //self.couponView.hidden = true
                
                if(self.segmentEncash_Recharge.selectedSegmentIndex == 0)
                {
                    self.rechargeView.isHidden = false
                }
                    //                else if(self.segmentEncash_Recharge.selectedSegmentIndex == 2)
                    //                {
                    //                    self.couponView.hidden = false
                    //                    createCouponView()
                    //                }
                else
                {
                    self.encashView.isHidden = false
                    if  redeemableValueLabel.text?.characters.count > 0 && redeemableValueLabel.text != "0"
                    {
                        if let value = redeemableValueLabel.text
                        {
                            let val:Int = Int(value)!
                            if(val >= 550)
                            {
                                self.pointTextBox.isUserInteractionEnabled = true
                                self.pointTextBox.alpha = 1.0
                            } else {
                                self.pointTextBox.isUserInteractionEnabled = false
                                self.pointTextBox.alpha = 0.6
                            }
                        }
                    } else {
                        self.pointTextBox.isUserInteractionEnabled = false
                        self.pointTextBox.alpha = 0.6
                    }
                }
            }
        }
        
        func transactionHistory() {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"My Account",
                AnalyticsParameterItemName: "Recharge and Encas",
                AnalyticsParameterContentType:"Transaction Button"
                ])
            let reqObj = GetUserPointsOnRecahargeRequest()
            showIndicator("Loading...")
            reqObj.userID =  UserInfo.sharedInstance.userID
            
            MyAccountRequestor().sendGetUserPointsRequest(reqObj, success:{ (success, object) in
                hideIndicator()
                let vc = PointsTableViewController()
                vc.data = (object as! GetUserPointsOnRecahargeResponse).dataObj!
                vc.myAccounts = true
                self.navigationController?.pushViewController(vc, animated: true)
            }){ (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
        }
        
        func closeView()
        {
            if  self.resultView.isHidden
            {
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                self.resultView.isHidden = true
                self.rechargeView.isHidden = false
                self.encashView.isHidden = false
                //self.couponView.hidden = false
                self.segmentEncash_Recharge.isHidden = false
                
                if self.segmentEncash_Recharge.selectedSegmentIndex == 0
                {
                    self.getUserPointsData()
                    self.encashView.isHidden = true
                    //self.couponView.hidden = true
                }
                    //                else if(self.segmentEncash_Recharge.selectedSegmentIndex == 2)
                    //                {
                    //                    self.encashView.hidden = true
                    //                    self.rechargeView.hidden = true
                    //                }
                else
                {
                    self.rechargeView.isHidden = true
                    //self.couponView.hidden = true
                }
            }
            
        }
        
        func getUserPointsData() -> Void
        {
            let reqObj = GetUserPointsOnRecahargeRequest()
            showIndicator("Loading...")
            reqObj.userID =  UserInfo.sharedInstance.userID
            
            MyAccountRequestor().sendGetUserPointsRequest(reqObj, success:{ (success, object) in
                hideIndicator()
                if (object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable != nil
                {
                    //Recharge
                    let pointValue_existing = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable)! - ((object as! GetUserPointsOnRecahargeResponse).dataObj?.bonusPts)!
                    self.existingValueLabel.text = "\(pointValue_existing)"
                    self.bonusValueLabel.text = "\(((object as! GetUserPointsOnRecahargeResponse).dataObj?.bonusPts)!)"
                    self.totalValueLabel_recharge.text = "\(((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable)!)"
                    
                    //Encash
                    self.redeemableValueLabel.text = "\(pointValue_existing)"
                    self.bonusValueLabel_encash.text = "\(((object as! GetUserPointsOnRecahargeResponse).dataObj?.bonusPts)!)"
                    self.totalValueLabel_encash.text = "\(((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable)!)"
                    
                    UserInfo.sharedInstance.maxAllowedPoints = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.maximumallowedpoints)!
                    UserInfo.sharedInstance.availablePoints = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable)!
                }
                if (object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsearned != nil
                {
                    UserInfo.sharedInstance.earnedPointsViaRecharge = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsearned)!
                }
            }){ (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
        }
        
        
        func configPaytmMerchant() -> Void
        {
            
            let  pgMerchantConfig = PGMerchantConfiguration.default()
            pgMerchantConfig?.checksumGenerationURL = PAYTM_GENERATE
            pgMerchantConfig?.checksumValidationURL = PAYTM_VERIFY
            
            /*
             https://rideally.com/PaytmKit/generateChecksum.php
             https://rideally.com/PaytmKit/verifyChecksum.php
             
             Live Envi
             Merchant Detail:
             Website name       :WAP-   Hariprakashwap WEB-   Hariprakashweb
             MID                        : rideal18020254537437
             Merchant Key         : ZlQhHCGumzP8z2BN
             
             Industry_type_ID    : Retail110
             Channel_ID            : WEB
             WAP
             
             Merchant Dashboard: Login details.
             
                         URL:   https://gateway.paytm.in/PayTMSecured/app/auth/login
                         User name:  Hariprakash
                         Password:    Rideally#123
             
             */
            
            
            if(ENVIRONMENT == "LIVE")
            {
                let orderDict: [String:String] = [
                    "MID" : "rideal18020254537437",
                    "CHANNEL_ID" : "WAP",
                    "INDUSTRY_TYPE_ID" : "Retail110",
                    "WEBSITE" : "Hariprakashwap",
                    "TXN_AMOUNT" : self.rechanrgeAmountLabel.text!,
                    "ORDER_ID" : "RECHARGE-ORDER\(Int(arc4random_uniform(214128631) + 1))",
                    "CUST_ID" : UserInfo.sharedInstance.userID,
                    "REQUEST_TYPE" : "DEFAULT"
                ]
                
                
                let order: PGOrder = PGOrder(params: orderDict)
                let txnController: PGTransactionViewController = PGTransactionViewController(transactionFor: order)
                txnController.serverType =  eServerTypeProduction
                txnController.merchant = pgMerchantConfig
                txnController.delegate = self
                self.present(txnController, animated: true, completion: nil)
            }
            else
            {
                let orderDict: [String:String] = [
                    "MID" : "RideAl10117568768303",
                    "CHANNEL_ID" : "WAP",
                    "INDUSTRY_TYPE_ID" : "Retail",
                    "WEBSITE" : "RideAllywap",
                    "TXN_AMOUNT" : self.rechanrgeAmountLabel.text!,
                    "ORDER_ID" : "RECHARGE-ORDER\(Int(arc4random_uniform(214128631) + 1))",
                    "CUST_ID" : UserInfo.sharedInstance.userID,
                    "REQUEST_TYPE" : "DEFAULT"
                ]
                
                let order: PGOrder = PGOrder(params: orderDict)
                PGServerEnvironment.selectServerDialog(self.view, completionHandler: {(type: ServerType) -> Void in
                    
                    let txnController: PGTransactionViewController = PGTransactionViewController(transactionFor: order)
                    
                    if type != eServerTypeNone
                    {
                        txnController.serverType =  type
                        txnController.merchant = pgMerchantConfig
                        txnController.delegate = self
                        self.present(txnController, animated: true, completion: nil)
                    }
                })
            }
        }
        
        func didSucceedTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable: Any]!)
        {
            if  let respobj:NSDictionary = response as NSDictionary? 
            {
                let reqObj = UpdateTransactionOnRecahargeRequest()
                
                showIndicator("Loading...")
                
                reqObj.userID = UserInfo.sharedInstance.userID
                reqObj.channelType = "WAP"
                reqObj.bankName    =  respobj.value(forKey: "BANKNAME") as? String
                reqObj.bankTransactionID    = respobj.value(forKey: "BANKTXNID") as? String
                reqObj.currency    = respobj.value(forKey: "CURRENCY") as? String
                reqObj.gateWayname    = respobj.value(forKey: "GATEWAYNAME") as? String
                reqObj.validCheckSum    = respobj.value(forKey: "IS_CHECKSUM_VALID") as? String
                reqObj.mid    = respobj.value(forKey: "MID") as? String
                reqObj.orderID    = respobj.value(forKey: "ORDERID") as? String
                reqObj.paymentMode    = respobj.value(forKey: "PAYMENTMODE") as? String
                reqObj.responseCode    = respobj.value(forKey: "RESPCODE") as? String
                reqObj.responseMSG    = respobj.value(forKey: "RESPMSG") as? String
                reqObj.status    = respobj.value(forKey: "STATUS") as? String
                reqObj.transactionAmt   = respobj.value(forKey: "TXNAMOUNT") as? String
                reqObj.transactionDate    = respobj.value(forKey: "TXNDATE") as? String
                reqObj.transactionID    = respobj.value(forKey: "TXNID") as? String
                reqObj.transactionFrom = "2"
                reqObj.rechargeAmount = self.selectedAmount as String
                reqObj.firstName = UserInfo.sharedInstance.firstName
                reqObj.emailID = UserInfo.sharedInstance.email
                reqObj.mobileNumber = UserInfo.sharedInstance.mobile
                
                
                self.rechargeView.isHidden = false
                self.encashView.isHidden = false
                //self.couponView.hidden = false
                self.segmentEncash_Recharge.isHidden = false
                self.resultView.isHidden = true
                
                
                MyAccountRequestor().sendUpdateTransactionRequest(reqObj, success:{ (success, object) in
                    hideIndicator()
                    
                    if let data = object as? NSDictionary
                    {
                        
                        if data.value(forKey: "code") as! NSString == "2992"
                        {
                            if let transactionID = (data.object(forKey: "data")) as? NSArray
                            {
                                let transactionIDValue = transactionID[0] as! String
                                
                                self.rechargeView.isHidden = true
                                self.encashView.isHidden = true
                                //self.couponView.hidden = true
                                self.segmentEncash_Recharge.isHidden = true
                                self.resultView.isHidden = false
                                
                                let fullString = NSMutableAttributedString(string: "We have received your payment of")
                                let image1Attachment = NSTextAttachment()
                                image1Attachment.image = UIImage(named: "rupee_gray.png")
                                let image1String = NSAttributedString(attachment: image1Attachment)
                                fullString.append(image1String)
                                
                                var boldString = "\(reqObj.rechargeAmount!)"
                                var range = NSMakeRange(0, "\(reqObj.rechargeAmount!)".characters.count)
                                fullString.append(self.attributedString(from: boldString, nonBoldRange: range))
                                
                                fullString.append(NSAttributedString(string:". Your reference ID for the transaction is"))
                                
                                boldString = " \(transactionIDValue)"
                                range = NSMakeRange(0, "\(transactionIDValue)".characters.count)
                                fullString.append(self.attributedString(from: boldString, nonBoldRange: range))
                                
                                self.resultLabel.attributedText = fullString
                                
                                //self.resultLabel.text = "We have received your payment of \(reqObj.rechargeAmount!) cost. Your reference ID for the transaction is \(transactionIDValue)"
                            }
                        }
                        else
                        {
                            if let transactionID = (data.object(forKey: "data")) as? NSArray
                            {
                                let transactionIDValue = transactionID[0] as! String
                                
                                AlertController.showAlertForMessage("There is some problem in transaction. Your reference ID for this transaction is \(transactionIDValue). Please try again")
                            }
                        }
                    }
                    
                }){ (error) in
                    hideIndicator()
                    AlertController.showAlertForError(error)
                }
            }
            
            self.dismiss(animated: true, completion: nil)
        }
        
//        func didFailTransaction(_ controller: PGTransactionViewController!, error: NSError!, response: [AnyHashable: Any]!)
//        {
//            
//            AlertController.showAlertForMessage(ERROR_MESSAGE_STRING)
//            self.dismiss(animated: true, completion: nil)
//        }
//        
//        func didCancelTransaction(_ controller: PGTransactionViewController!, error: NSError!, response: [AnyHashable: Any]!)
//        {
//            self.dismiss(animated: true, completion: nil)
//        }
        
        func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable: Any]!)
        {
            //        if let value = response
            //        {
            //            print("CASTransaction resp is:",value)
            //        }
        }
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool
        {
            
            if  self.pointTextBox == textField
            {
                self.pointTextBox.resignFirstResponder()
            }
            
            return true
        }
        
        func textField(_ textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool
        {
            let newCharacters = CharacterSet(charactersIn: string)
            let boolIsNumber = CharacterSet.decimalDigits.isSuperset(of: newCharacters)
            if boolIsNumber == true {
                return true
            } else {
                if string == "." {
                    let countdots = textField.text!.components(separatedBy: ".").count - 1
                    if countdots == 0 {
                        return true
                    } else {
                        if countdots > 0 && string == "." {
                            return false
                        } else {
                            return true
                        }
                    }
                } else {
                    return false
                }
            }
        }
        
        override func viewDidLayoutSubviews()
        {
            super.viewDidLayoutSubviews()
            
            rechargeTo50_Button.layer.cornerRadius = self.rechargeTo50_Button.frame.size.height/2
            rechargeTo100_Button.layer.cornerRadius = self.rechargeTo100_Button.frame.size.height/2
            rechargeTo150_Button.layer.cornerRadius = self.rechargeTo150_Button.frame.size.height/2
            rechargeTo200_Button.layer.cornerRadius = self.rechargeTo200_Button.frame.size.height/2
        }
        
        func getColorByHex(_ rgbHexValue:UInt32, alpha:Double = 1.0) -> UIColor {
            let red = Double((rgbHexValue & 0xFF0000) >> 16) / 256.0
            let green = Double((rgbHexValue & 0xFF00) >> 8) / 256.0
            let blue = Double((rgbHexValue & 0xFF)) / 256.0
            
            return UIColor(red: CGFloat(red), green: CGFloat(green), blue: CGFloat(blue), alpha: CGFloat(alpha))
        }
        
        func attributedString(from string: String, nonBoldRange: NSRange?) -> NSAttributedString {
            
            let fontSize = UIFont.systemFontSize
            
            let attrs = [
                NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize),
                NSForegroundColorAttributeName: UIColor.black
            ]
            
            let nonBoldAttribute = [
                NSFontAttributeName: UIFont.boldSystemFont(ofSize: fontSize),
                NSForegroundColorAttributeName: UIColor.black
                //NSFontAttributeName: UIFont.systemFontOfSize(fontSize()),
            ]
            
            let attrStr = NSMutableAttributedString(string: string, attributes: attrs)
            
            if let range = nonBoldRange
            {
                attrStr.setAttributes(nonBoldAttribute, range: range)
            }
            
            return attrStr
        }
        
        //        func getOffersPromotions()
        //        {
        //            let reqObj = OffersPromotionsRequest()
        //            reqObj.user_id = UserInfo.sharedInstance.userID
        //            //showIndicator("Getting Offers & Promotions..")
        //            OffersPromotionsRequestor().getOffersPromotions(reqObj, success:{(success, object) in
        //                //hideIndicator()
        //                if (object as! OffersPromotionsResponse).code == "2999" {
        //                    self.offersPromotionsData = (object as! OffersPromotionsResponse).dataObj!
        //                }
        //            }) { (error) in
        //                //hideIndicator()
        //            }
        //        }
        
        func showTermsCond() {
            let viewHolder = AlertContentViewHolder()
            viewHolder.heightConstraintValue = 95
            
            let view = UIView()
            view.backgroundColor = Colors.WHITE_COLOR
            let lblInfo = UILabel()
            lblInfo.text = "1. User can redeem their points in multiples of 550 only.\n2. To redeem your points using 'Petro Card'\na. User should have minimum 550 points and can request for the card.\nb. RideAlly will deduct 50 points as service charge, hence you will be getting ₹ 500 equal petro card.\nc. The requested petro card will be delivered to your provided address within 15 working days.\n3. To redeem your points using 'Paytm'\na. User should have minimum 550 points and can request for transfer to their Paytm account.\nb. RideAlly will charge 6% service charge, hence ₹ 517 would be transferred to your Paytm\nc. The requested money will be transferred to user's Paytm account within 7 working days."
            lblInfo.numberOfLines = 0
            lblInfo.lineBreakMode = .byWordWrapping
            lblInfo.translatesAutoresizingMaskIntoConstraints = false
            lblInfo.font = normalFontWithSize(13)
            view.addSubview(lblInfo)
            
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblInfo]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblInfo]))
            
            viewHolder.view = view
            
            AlertController.showAlertFor("Terms and Conditions", message: "", contentView: viewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: false, okAction: {
                self.dismiss(animated: true, completion: nil)
            })
        }
    }
