//
//  RideViewController.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import AddressBook
import AddressBookUI
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class RideViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource,ABPeoplePickerNavigationControllerDelegate  {
    
    let tableView = UITableView()
    var tableHeightConstraint: NSLayoutConstraint?
    var tableTopConstraint: NSLayoutConstraint?
    let rowHt: CGFloat = 115
    let segment = HMSegmentedControl()
    let segmentRides = HMSegmentedControl()
    
    var currentSelectedSegmentIndex: UInt = 0
    
    var currentPageNumber = 1
    var hasMoreEntries = true
    var noData = false
    var needRideObj: NeedRideRequest?
    var offerVehicleObj: OfferVehicleRequest?
    var searchReqObj: SearchRideRequest?
    var isMyRidesVisible = false
    var isOnStart = true
    let btnRides = UIButton(type: .custom)
    let lblRides = UILabel()
    var titleHeightConstraint: NSLayoutConstraint?
    var iconFilter = UIButton()
    var selectedDates = [String]()
    let pastTimeLbl = UILabel()
    var vehicleDefaultDetails: VehicleDefaultResponse!
    var allRidesData: [Ride]?
    var ridePoints = 0
    var allRides = false
    var myRides = false
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    var localPoolID = ""
    var localPoolScope = ""
    var myRidesData: MyRidesResponse?{
        didSet{
            //if(segmentRides.selectedSegmentIndex == 0){
            dataSource = (myRidesData!.data!.myPoolRides)!
            //            }
            //            else{
            //                dataSource = (myRidesData!.data!.myTaxiRides)!
            //            }
        }
    }
    
    var ongoingRidesData: MyRidesResponse?{
        didSet{
            dataSource = (ongoingRidesData?.dataOngoing?.myPoolRides)!
        }
    }
    
    var completedRidesData: MyRidesResponse?{
        didSet{
            dataSource = (completedRidesData!.dataCompleted!.myPoolRides)!
        }
    }
    
    
    var dataSource = [Ride](){
        didSet{
            if(dataSource.count > 0){
                tableView.isHidden = false
                view.removeEmptyView()
                tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt)
                tableView.reloadData()
            }
            else{
                tableHeightConstraint?.constant = 0
                if(segmentRides.selectedSegmentIndex == 0) {
                    showEmptyView("You do not have any upcoming rides right now.")
                } else if(segmentRides.selectedSegmentIndex == 1) {
                    showEmptyView("You do not have any ongoing rides right now.")
                } else {
                    showEmptyView("You do not have any completed rides right now.")
                }
            }
        }
    }
    
    override func hasTabBarItems() -> Bool
    {
        ISWPRIDESSCREEN = false
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //compareAppVersions()
        if(allRides) {
            getAllRidesList()
        } else if(isSearchResults) {
            isMyRidesVisible = true
            titleHeightConstraint?.constant = 0
            segmentRides.isHidden = true
            self.navigationItem.title = "Search Result(s)"
            self.segment.selectedSegmentIndex = 1
            self.currentSelectedSegmentIndex = 1
            addSubViews()
        } else {
            getMyRides()
            //getMyRides("onStart")
        }
        if(UserInfo.sharedInstance.availablePoints == 0 || UserInfo.sharedInstance.maxAllowedPoints == "") {
            getUserPoints()
        }
        //self.navigationController!.navigationBar.translucent = false
        //self.navigationController?.tabBarController?.tabBar.translucent = false
        self.edgesForExtendedLayout = UIRectEdge()
        if(allRides || isSearchResults) {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        } else if(myRides) {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        } else {
            self.navigationController?.navigationBar.isHidden = false
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(showCreateRide))
        }
        NotificationCenter.default.addObserver(self, selector: #selector(comingFromRideDetail), name: NSNotification.Name(rawValue: "updateRidesList"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(comingFromOffersPromotions), name: NSNotification.Name(rawValue: "myTaxiRides"), object: nil)
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "KInviteSendNotificationallRides"),object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotificationallRides"), object: nil)
        self.tableView.tableFooterView = UIView() //remove extra lines
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showCreateRide() {
        let ins = UIApplication.shared.delegate as! AppDelegate
        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
            ins.gotoHome()
        } else {
        if(UserInfo.sharedInstance.myPendingWorkplaces != "" && UserInfo.sharedInstance.myWorkplaces != "") {
            if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                ins.gotoHome(1)
            } else {
                ins.gotoHome()
            }
        } else {
            ins.gotoHome()
        }
        }
    }
    
    override func showMenu() {
//        if let drawerVC = self.evo_drawerController?.centerViewController as? DrawerViewController {
//            drawerVC.toggleLeftDrawerSideAnimated(true, completion: nil)
//        }
//        self.evo_drawerController?.closeDrawerAnimated(true, completion:nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func addSubViews()
    {
        let titleView = UIView()
        titleView.translatesAutoresizingMaskIntoConstraints = false
        
        segmentRides.backgroundColor = UIColor.clear
        segmentRides.sectionTitles = ["Upcoming", "Ongoing", "Completed"]
        //segmentRides.sectionTitles = ["My Pool Rides", "My Taxi Rides"]
        segmentRides.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.GREEN_COLOR]
        segmentRides.selectedTitleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
        segmentRides.selectionStyle = HMSegmentedControlSelectionStyleBox
        segmentRides.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone
        segmentRides.selectionIndicatorColor = Colors.GREEN_COLOR
        segmentRides.selectionIndicatorBoxOpacity = 1.0
        segmentRides.translatesAutoresizingMaskIntoConstraints = false
        segmentRides.segmentEdgeInset = UIEdgeInsets.zero
        segmentRides.selectedSegmentIndex = 0
        //segmentRides.hidden = true
        segmentRides.layer.borderColor = Colors.GREEN_COLOR.cgColor
        segmentRides.layer.borderWidth = 1
        
        segmentRides.addTarget(self, action: #selector(rideTypeChanged), for: .valueChanged)
        titleView.addSubview(segmentRides)
        
        iconFilter.setImage(UIImage(named: "filter_gray_new"), for: UIControlState())
        iconFilter.translatesAutoresizingMaskIntoConstraints = false
        iconFilter.addTarget(self, action: #selector(btnFilter_clicked), for: .touchDown)
        iconFilter.isHidden = true
        titleView.addSubview(iconFilter)
        
        titleView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[segment(30)]", options: [], metrics: nil, views: ["segment":segmentRides]))
        titleView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-35-[segment]-15-[ifilter(30)]-5-|", options: [], metrics: nil, views: ["segment":segmentRides, "ifilter":iconFilter]))
        titleView.addConstraint(NSLayoutConstraint(item: iconFilter, attribute: .centerY, relatedBy: .equal, toItem: segmentRides, attribute: .centerY, multiplier: 1, constant: 0))
        titleView.addConstraint(NSLayoutConstraint(item: iconFilter, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30))
        
        self.view.addSubview(titleView)

        tableView.separatorColor = Colors.GRAY_COLOR
        tableView.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(RideCell.self, forCellReuseIdentifier: "ridecell")
        tableView.register(MyRideCell.self, forCellReuseIdentifier: "myridecell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        
//        let btnView = UIView()
//        btnView.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addSubview(btnView)
//        
//        btnView.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
//        btnView.addTopViewBorder(2, color: Colors.GENERAL_BORDER_COLOR, offset: CGPointZero)
//        
//        let btnAddRides = UIButton(type: .Custom)
//        setUpBtn(btnAddRides, title:"AddRide")
//        btnAddRides.addTarget(self, action: #selector(btnAddRideClicked), forControlEvents: .TouchDown)
//        btnView.addSubview(btnAddRides)
//        
//        let lblAddRides = UILabel()
//        lblAddRides.text = "ADD"
//        lblAddRides.textAlignment = .Center
//        lblAddRides.font = normalFontWithSize(11)
//        lblAddRides.translatesAutoresizingMaskIntoConstraints = false
//        //view1.addSubview(lblAddRides)
//        btnAddRides.addSubview(lblAddRides)
//        
//        btnAddRides.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblAddRides]))
//        btnAddRides.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[lbl(10)]-2-|", options: [], metrics: nil, views: ["lbl":lblAddRides]))
//        
//        let btnSearch = UIButton(type: .Custom)
//        setUpBtn(btnSearch, title:"Search")
//        btnSearch.addTarget(self, action: #selector(btnSearchClicked), forControlEvents: .TouchDown)
//        btnView.addSubview(btnSearch)
//        
//        let lblSearch = UILabel()
//        lblSearch.text = "SEARCH"
//        lblSearch.textAlignment = .Center
//        lblSearch.font = normalFontWithSize(11)
//        lblSearch.translatesAutoresizingMaskIntoConstraints = false
//        //view1.addSubview(lblSearch)
//        btnSearch.addSubview(lblSearch)
//        
//        btnSearch.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblSearch]))
//        btnSearch.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[lbl(10)]-2-|", options: [], metrics: nil, views: ["lbl":lblSearch]))
        if(!allRides && !isSearchResults) {
       titleHeightConstraint = NSLayoutConstraint(item: titleView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 45)
        
        view.addConstraint(titleHeightConstraint!)
        }
//
//        if(isMyRidesVisible) {
//            setUpBtn(btnRides, title:"MyRide")
//            lblRides.text = "MY RIDES"
//            isMyRidesVisible = true
//            titleHeightConstraint?.constant = 0
//            segmentRides.hidden = true
//            //lblTitle.text = "List of all Ride(s)"
//            currentPageNumber = 1
//            hasMoreEntries = true
//            //dataSource.removeAll()
//        } else {
//            setUpBtn(btnRides, title:"Explore")
//            lblRides.text = "EXPLORE"
//            isMyRidesVisible = false
//            segmentRides.hidden = false
//            titleHeightConstraint?.constant = 45
//            //lblTitle.text = "List of my Pool Ride(s)"
//        }
//        //isOnStart = true
//        setUpBtn(btnRides, title:"Explore")
//        btnRides.addTarget(self, action: #selector(btnRidesClicked(_:)), forControlEvents: .TouchDown)
//        btnView.addSubview(btnRides)
//        lblRides.textAlignment = .Center
//        lblRides.font = normalFontWithSize(11)
//        lblRides.translatesAutoresizingMaskIntoConstraints = false
//        //view1.addSubview(lblRides)
//        btnRides.addSubview(lblRides)
//        
//        btnRides.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblRides]))
//        btnRides.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[lbl(10)]-2-|", options: [], metrics: nil, views: ["lbl":lblRides]))
//        
//        lblSearch.textColor = Colors.GRAY_COLOR
//        lblAddRides.textColor = Colors.GRAY_COLOR
//        lblRides.textColor = Colors.GRAY_COLOR
//        
//        let viewsDict = ["add":btnAddRides, "lbladd":lblAddRides, "search":btnSearch, "lblsearch":lblSearch, "rides":btnRides, "lblrides":lblRides]
//        
//        btnView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[add][search(==add)][rides(==add)]|", options: [], metrics: nil, views: viewsDict))
//        btnView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-2-[add]|", options: [], metrics: nil, views: viewsDict))
//        btnView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-2-[search]|", options: [], metrics: nil, views: viewsDict))
//        btnView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-2-[rides]|", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title][table]|", options: [], metrics: nil, views: ["title":titleView, "table":tableView]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: ["title":titleView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
        //view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[btns]|", options: [], metrics: nil, views: ["btns":btnView]))
    }
    
    func showEmptyView(_ msg: String)
    {
        tableView.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "ic_empty_cart", actionHandler: nil)
    }
    
    func getAllRides()
    {
        showIndicator("Loading Rides.")
        let reqObj = AllRidesRequest()
        reqObj.page = "\(currentPageNumber)"
        reqObj.pooltype = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        reqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        reqObj.reqTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        reqObj.rideScope = ""
        reqObj.searchType = "generic"
        reqObj.showRtype = "AC"
        reqObj.userId = UserInfo.sharedInstance.userID
        
        RideRequestor().getAllRides(reqObj, success: { (success, object) in
            hideIndicator()
            if((object as! AllRidesResponse).pools!.count == 0){
                self.hasMoreEntries = false
            }
            //self.dataSource.removeAll()
            self.dataSource.append(contentsOf: (object as! AllRidesResponse).pools!)
        }) { (error) in
            hideIndicator()
        }
    }
    
    func getAllRidesList(){
        isMyRidesVisible = true
        isSearchResults = false
        titleHeightConstraint?.constant = 0
        segmentRides.isHidden = true
        self.navigationItem.title = "Existing Rides"
        currentPageNumber = 1
        hasMoreEntries = true
        //dataSource.removeAll()
        self.addSubViews()
        getAllRides()
    }

    func getMyRides(){
        isMyRidesVisible = false
        isSearchResults = false
        segmentRides.isHidden = false
        titleHeightConstraint?.constant = 45
        self.navigationItem.title = "My Rides"
        self.addSubViews()
        showIndicator("Loading My Rides.")
        
        RideRequestor().getMyRides({ (success, object) in
            hideIndicator()
            self.myRidesData = object as? MyRidesResponse
            
        }) { (error) in
            hideIndicator()
        }
    }
    
    func getMyRides(_ triggerFrom: String)
    {
        showIndicator("Loading Rides.")
        
        RideRequestor().getMyRides({ (success, object) in
            hideIndicator()
            self.myRidesData = object as? MyRidesResponse
            //self.dataSource = (self.myRidesData!.data?.myPoolRides)!
            //print("\ndatasourcemyrides:>> \(self.dataSource)")
            if(triggerFrom == "onStart") {
                self.isOnStart = false
                if(self.myRidesData!.data?.myPoolRides!.count == 0) {
                    self.isMyRidesVisible = false
                } else {
                    self.isMyRidesVisible = true
                }
                self.addSubViews()
                self.btnRidesClicked(self.btnRides)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func getOngoingRides()
    {
        showIndicator("Loading Rides.")
        
        RideRequestor().getOngoingRides({ (success, object) in
            hideIndicator()
            self.ongoingRidesData = object as? MyRidesResponse
            self.dataSource = (self.ongoingRidesData?.dataOngoing?.myPoolRides)!
            //            if(triggerFrom == "onStart") {
            //                self.isOnStart = false
            //                if(self.myRidesData!.data?.myPoolRides!.count == 0) {
            //                    self.isMyRidesVisible = false
            //                } else {
            //                    self.isMyRidesVisible = true
            //                }
            //                self.addSubViews()
            //                self.btnRidesClicked(self.btnRides)
            //            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func getCompletedRides(_ key: String)
    {
        showIndicator("Loading Rides.")
        
        RideRequestor().getCompletedRides(key, success:{(success, object) in
            hideIndicator()
            self.completedRidesData = object as? MyRidesResponse
            self.dataSource = (self.completedRidesData?.dataCompleted?.myPoolRides)!
            //if(triggerFrom == "onStart") {
            //self.isOnStart = false
            //                if(self.completedRidesData?.dataCompleted?.myPoolRides?.count == 0) {
            //                    self.isMyRidesVisible = false
            //                } else {
            //                    self.isMyRidesVisible = true
            //                }
            //self.addSubViews()
            //self.btnRidesClicked(self.btnRides)
            //}
        }) { (error) in
            hideIndicator()
        }
    }
    
    func comingFromOffersPromotions()
    {
        segmentRides.selectedSegmentIndex = 0
        isMyRidesVisible = true
        isOnStart = true
        btnRidesClicked(btnRides)
    }
    
    func comingFromRideDetail()
    {
        isMyRidesVisible = true
        isOnStart = true
        btnRidesClicked(btnRides)
    }
    
    func updateRidesList()
    {
        if(segment.selectedSegmentIndex == 2 || segment.selectedSegmentIndex == 0){
            getMyRides("onClick")
        }
        else if(segment.selectedSegmentIndex == 3){
            currentPageNumber = 1
            hasMoreEntries = true
            dataSource.removeAll()
            getAllRides()
        }
        else if(segment.selectedSegmentIndex == 1){
            if(searchReqObj != nil) {
                showIndicator("Updating.")
                RideRequestor().searchRides(searchReqObj!, success: { (success, object) in
                    hideIndicator()
                    if(success){
                        if((object as! AllRidesResponse).pools!.count > 0){
                            self.dataSource = (object as! AllRidesResponse).pools!
                        }
                    }
                }) { (error) in
                    hideIndicator()
                }
            }
        }
    }
    
    func showSearchRides()
    {
        let childVC = SearchRideViewController()
        childVC.hidesBottomBarWhenPushed = true
        childVC.completionBlock = { (rides, reqObj) -> Void in
            
            self.isSearchResults = true
            self.searchReqObj = reqObj as? SearchRideRequest
            self.dataSource = rides as! [Ride]
            //self.lblTitle.text = "Search Result(s)"
            self.navigationItem.title = "Search Result(s)"
            self.titleHeightConstraint?.constant = 0
            self.segmentRides.isHidden = true
            self.segment.selectedSegmentIndex = 1
            self.currentSelectedSegmentIndex = 1
        }
        self.navigationController?.pushViewController(childVC, animated: true)
    }
    
    //let lblTitle = UILabel()
    
    func createNeedRide()
    {
        NeedRideRequestor().createNeedaRide(needRideObj!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                
                self.needRideObj = nil
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! NeedRideResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.triggerFrom = "ride"
                            vc.onDeleteActionBlock = {
                                self.comingFromRideDetail()
                            }
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! NeedRideResponse).code == "173") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! NeedRideResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func createOfferVehicleRide () {
        OfferVehicleRequestor().createOfferVehicle(offerVehicleObj!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                self.offerVehicleObj = nil
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! OfferVehicleResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.triggerFrom = "ride"
                            vc.onDeleteActionBlock = {
                                self.comingFromRideDetail()
                            }
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! OfferVehicleResponse).code == "173") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! OfferVehicleResponse).message!)
                }
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func btnAddNew_clicked()
    {
        if(needRideObj != nil){
            showIndicator("Creating New Ride..")
            CommonRequestor().getDistanceDuration(self.needRideObj!.fromLocation!, destination: self.needRideObj!.toLocation!, success: { (success, object) in
                self.needRideObj!.duration = ""
                self.needRideObj!.distance = ""
                if let data = object as? NSDictionary{
                    let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                    let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                    if((elements["status"] as! String) == "OK")
                    {
                        let distance = ((elements["distance"] as! NSDictionary)["text"])!
                        let duration = ((elements["duration"] as! NSDictionary)["text"])!
                        self.needRideObj!.duration = duration as! String
                        self.needRideObj!.distance = distance as! String
                    }
                }
                self.createNeedRide()
                }, failure: { (error) in
                    self.createNeedRide()
            })
        }
        else if(offerVehicleObj != nil){
            showIndicator("Creating Offer a vehicle..")
            CommonRequestor().getDistanceDuration(self.offerVehicleObj!.fromLocation!, destination: self.offerVehicleObj!.toLocation!, success: { (success, object) in
                self.offerVehicleObj!.duration = ""
                self.offerVehicleObj!.distance = ""
                if let data = object as? NSDictionary{
                    let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                    let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                    if((elements["status"] as! String) == "OK")
                    {
                        let distance = ((elements["distance"] as! NSDictionary)["text"])!
                        let duration = ((elements["duration"] as! NSDictionary)["text"])!
                        self.offerVehicleObj!.duration = duration as? String
                        self.offerVehicleObj!.distance = distance as? String
                    }
                }
                self.createOfferVehicleRide()
                }, failure: { (error) in
                    self.createOfferVehicleRide()
            })
        }
    }
    
    func btnFilter_clicked() {
        let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let lastWeek = UIAlertAction(title: "Last week", style: .default, handler: { (action) in
            self.getCompletedRides("0")
        })
        vc.addAction(lastWeek)
        
        let lastMnth = UIAlertAction(title: "Last month", style: .default, handler: { (action) in
            self.getCompletedRides("1")
        })
        vc.addAction(lastMnth)
        
        let last3Mnth = UIAlertAction(title: "Last 3 months", style: .default, handler: { (action) in
            self.getCompletedRides("3")
        })
        vc.addAction(last3Mnth)
        
        let last6Mnth = UIAlertAction(title: "Last 6 months", style: .default, handler: { (action) in
            self.getCompletedRides("6")
        })
        vc.addAction(last6Mnth)
        
        let older = UIAlertAction(title: "Older than 6 months", style: .default, handler: { (action) in
            self.getCompletedRides("12")
        })
        vc.addAction(older)
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        })
        vc.addAction(actionCancel)
        
        present(vc, animated: true, completion: nil)
    }
    
    /*func getTitleView() -> SubView
     {
     let view = UIView()
     
     lblTitle.text = ""
     lblTitle.textColor = Colors.GRAY_COLOR
     lblTitle.font = normalFontWithSize(15)
     lblTitle.translatesAutoresizingMaskIntoConstraints = false
     let tapGesture = UITapGestureRecognizer(target: self, action: #selector(btnAddNew_clicked))
     lblTitle.addGestureRecognizer(tapGesture)
     view.addSubview(lblTitle)
     
     segmentRides.backgroundColor = UIColor.clearColor()
     segmentRides.sectionTitles = ["My Pool Rides", "My Taxi Rides"]
     segmentRides.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.GREEN_COLOR]
     segmentRides.selectedTitleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
     segmentRides.selectionStyle = HMSegmentedControlSelectionStyleBox
     segmentRides.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone
     segmentRides.selectionIndicatorColor = Colors.GREEN_COLOR
     segmentRides.selectionIndicatorBoxOpacity = 1.0
     segmentRides.translatesAutoresizingMaskIntoConstraints = false
     segmentRides.segmentEdgeInset = UIEdgeInsetsZero
     segmentRides.selectedSegmentIndex = 0
     segmentRides.hidden = true
     segmentRides.layer.borderColor = Colors.GREEN_COLOR.CGColor
     segmentRides.layer.borderWidth = 1
     
     segmentRides.addTarget(self, action: #selector(rideTypeChanged), forControlEvents: .ValueChanged)
     view.addSubview(segmentRides)
     
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[title(30)]-5-[segment(30)]", options: [], metrics: nil, views: ["title":lblTitle, "segment":segmentRides]))
     
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[title]-5-|", options: [], metrics: nil, views: ["title":lblTitle]))
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[segment]-5-|", options: [], metrics: nil, views: ["segment":segmentRides]))
     
     let subview = SubView()
     subview.view = view
     subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
     subview.heightConstraint = NSLayoutConstraint(item: view, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 75)
     return subview
     }
     
     func getTableView() -> SubView
     {
     let view = UIView()
     
     tableView.separatorColor = UIColor.clearColor()
     tableView.delegate = self
     tableView.dataSource = self
     tableView.scrollEnabled = false
     tableView.registerClass(RideCell.self, forCellReuseIdentifier: "ridecell")
     tableView.registerClass(MyRideCell.self, forCellReuseIdentifier: "myridecell")
     tableView.translatesAutoresizingMaskIntoConstraints = false
     view.addSubview(tableView)
     
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
     view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[table]-5-|", options: [], metrics: nil, views: ["table":tableView]))
     
     tableHeightConstraint = NSLayoutConstraint(item: view, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
     
     let subview = SubView()
     subview.view = view
     subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
     subview.heightConstraint = tableHeightConstraint!
     return subview
     }*/
    
    func rideTypeChanged()
    {
        //        if(segmentRides.selectedSegmentIndex == 0){
        //            dataSource = myRidesData!.data!.myPoolRides!
        //        }
        //        else{
        //            dataSource = myRidesData!.data!.myTaxiRides!
        //        }
        if(segmentRides.selectedSegmentIndex == 0){
            iconFilter.isHidden = true
            //dataSource = myRidesData!.data!.myPoolRides!
            getMyRides("onClick")
        } else if(segmentRides.selectedSegmentIndex == 1){
            iconFilter.isHidden = true
            getOngoingRides()
            //dataSource = ongoingRidesData!.dataOngoing!.myPoolRides!
        } else{
            iconFilter.isHidden = false
            getCompletedRides("0")
            //dataSource = completedRidesData!.dataCompleted!.myPoolRides!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return rowHt
    }
    
    var isSearchResults = false
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if(isMyRidesVisible == false && isSearchResults == false)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myridecell") as! MyRideCell
                if(dataSource.count>0) {
                    cell.selectedIndex = self.segmentRides.selectedSegmentIndex
                    cell.data = dataSource[indexPath.row]
                }
                cell.onUserActionBlock = { (selectedAction) -> Void in
                    self.cell_btnAction(selectedAction as! String, indexPath: indexPath)
                }
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ridecell") as! RideCell
                if(dataSource.count>0) {
                    cell.data = dataSource[indexPath.row]
                }
                cell.onUserActionBlock = { (selectedAction) -> Void in
                    self.cell_btnAction(selectedAction as! String, indexPath: indexPath)
                }
                return cell
            }
        }
    
    //cancel/reject/unjoin - refresh list
    
    func getPickUpDropLocation(_ triggerFrom: String)
    {
        let joinOrOfferRideObj = JoinOrOfferRide()
        let vc = RideDetailMapViewController()
        vc.sourceLong = selectedRide?.sourceLong
        vc.sourceLat = selectedRide?.sourceLat
        vc.sourceLocation = selectedRide?.source
        vc.destLong = selectedRide?.destLong
        vc.destLat = selectedRide?.destLat
        vc.destLocation = selectedRide?.destination
        vc.taxiSourceID = selectedRide.sourceID!
        vc.taxiDestID = selectedRide.destID!
        vc.poolTaxiBooking = selectedRide.poolTaxiBooking!
        vc.poolId = selectedRide.poolID!
        vc.rideType = selectedRide.poolRideType!
        vc.triggerFrom = triggerFrom
        vc.wpInsuranceStatus = String(selectedRide.poolGroupInsStatus!)
        
        if let poolScope = self.selectedRide.poolScope {
            if poolScope == "PUBLIC" {
                vc.isWP = false
            } else {
                vc.isWP = true
            }
        }
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.hidesBottomBarWhenPushed = true
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            if(self.selectedRide.poolTaxiBooking == "1") {
                showIndicator("Joining Taxi Ride..")
                RideRequestor().joinTaxiRide(self.selectedRide!.poolID!, userId: UserInfo.sharedInstance.userID, pickupPoint: pickUpLoc as? String, dropPoint: dropLoc as? String, comments: "",pCost: String(self.selectedRide.finalCost!), costMain: String(self.selectedRide.fullCost!), success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Join Taxi Ride", message: "You have successfully joined this ride.", okButtonTitle: "Ok", okAction: {
                            self.updateTaxiDetails()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Join Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            } else {
                joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
                joinOrOfferRideObj.poolID = self.selectedRide.poolID!
                joinOrOfferRideObj.vehicleRegNo = ""
                joinOrOfferRideObj.vehicleCapacity = ""
                joinOrOfferRideObj.cost = "0"
                joinOrOfferRideObj.rideMsg = ""
                joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
                joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
                joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
                joinOrOfferRideObj.dropPoint = dropLoc as? String
                joinOrOfferRideObj.dropPointLat = dropLat as? String
                joinOrOfferRideObj.dropPointLong = dropLong as? String
                
                if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                    showIndicator("Joining Ride..")
                    OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                                self.updateRideDetails()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    
                }
                else{
                    AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
                }
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func updateRideDetails()
    {
        showIndicator("Fetching Ride Details")
        RideRequestor().getRideDetails(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                //let vc = RideDetailViewController()
                vc.taxiSourceID = self.selectedRide.sourceID!
                vc.taxiDestID = self.selectedRide.destID!
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = object as? RideDetail
                if let poolScope = self.selectedRide.poolScope {
                    if poolScope == "PUBLIC" {
                        vc.triggerFrom = "ride"
                    } else {
                        vc.triggerFrom = "place"
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func updateTaxiDetails()
    {
        showIndicator("Fetching Ride Details")
        RideRequestor().getTaxiDetails(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let vc = TaxiDetailViewController()
                vc.taxiSourceID = self.selectedRide.sourceID!
                vc.taxiDestID = self.selectedRide.destID!
                vc.hidesBottomBarWhenPushed = true
                vc.data = object as? RideDetail
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func joinRide() {
        if(UserInfo.sharedInstance.shareMobileNumber == false){
            AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                self.getPickUpDropLocation("joinRide")
                }, cancelButtonTitle: "LATER", cancelAction: {
            })
        }
        else{
            getPickUpDropLocation("joinRide")
        }
    }
    
    func acceptRide() {
        AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Accepting ride request..")
            RideRequestor().acceptRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Accept Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateRideDetails()
                    })
                }else{
                    AlertController.showAlertFor("Accept Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    
    var selectedRide: Ride!
    var datePicker = UIDatePicker()
    var timePicker = UIDatePicker()
    var txtDate = UITextField()
    var txtTime = UITextField()
    var selectedTxtField: UITextField?
    
    var btn1: UIButton?
    var btn2: UIButton?
    var btn3: UIButton?
    var btn4: UIButton?
    var btn5: UIButton?
    var btn6: UIButton?
    var btn7: UIButton?
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            contactListArray.removeAllObjects()
            mobileMutArr.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject]{
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = localPoolID
            vc.triggerFrom = "allRides"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    self.contactListArray.removeAllObjects()
                    self.mobileMutArr.removeAllObjects()
                    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                    for record:ABRecord in contactList as [AnyObject]{
                        let contactPerson: ABRecord = record
                        let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                        if(contactName != "") {
                            var personNumber = ""
                            let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                            if(phones != "")
                            {
                                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                {
                                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                    {
                                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                        personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                        let areaCode = personNumber.characters.count-10
                                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                        if(areaCode > 0) {
                                            personNumber = personNumber.substring(from: startIndex)
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            self.mobileMutArr.add(personNumber)
                            let nameString = "\(contactName) \(personNumber)"
                            self.contactListArray.add(nameString)
                        }
                    }
                    
                    let vc = ContactsListViewController()
                    vc.contactListArr = self.contactListArray
                    vc.mobileMutArr = self.mobileMutArr
                    vc.poolID = self.localPoolID
                    vc.triggerFrom = "allRides"
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        //        AlertController.showAlertFor("Cannot Add Contact", message: "You must give the app permission to add the contact first.", okButtonTitle: "Settings", okAction: {
        //            self.openSettings()
        //            }, cancelButtonTitle: "OK", cancelAction: nil)
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObject()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg("", wpKey: "", poolID: localPoolID)
    }
    
    func grpMsg_clicked() {
        self.dismiss(animated: true, completion: nil)
        showIndicator("Fetching members.")
        WorkplaceRequestor().getWPMembers("", groupID: localPoolScope, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = InviteGroupMembersViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.wpDetails = (object as! WPMembersResponseData).groupDetails
            vc.membersList = (object as! WPMembersResponseData).members
            self.navigationController?.pushViewController(vc, animated: true)
        }, failure: { (error) in
            hideIndicator()
        })
    }

    func cell_btnAction(_ action: String, indexPath: IndexPath)
    {
        selectedRide = dataSource[indexPath.row]
        if(action == "invite"){
            segment.selectedSegmentIndex = 3
            localPoolID = self.selectedRide.poolID!
            if let poolScope = self.selectedRide.poolScope {
                if poolScope == "PUBLIC" {
                    let inviteView = InviteNormalRideView()
                    let alertViewHolder =   AlertContentViewHolder()
                    alertViewHolder.heightConstraintValue = 95
                    alertViewHolder.view = inviteView
                    inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
                    inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
                    
                    AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
                } else {
                    localPoolScope = poolScope
                    let inviteView = InviteWPRideView()
                    let alertViewHolder =   AlertContentViewHolder()
                    alertViewHolder.heightConstraintValue = 95
                    alertViewHolder.view = inviteView
                    inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
                    inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
                    inviteView.groupMsgBtn.addTarget(self, action: #selector(grpMsg_clicked), for: .touchDown)
                    
                    AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
                }
            }
        }
        else if(action == "joinride"){
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                if(selectedRide.poolTaxiBooking != "1" && selectedRide.cost != "0" && selectedRide.distance != "" && selectedRide.distance != "0") {
                    ridePoints = Int(selectedRide.cost!)! * Int(round(Double(selectedRide.distance!)!))
                    if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                        if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                            AlertController.showAlertFor("Join Ride", message: "\("SORRY! You cannot join this ride because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and join this ride.")", okButtonTitle: "RECHARGE", okAction: {
                                let vc = MyAccountViewController()
                                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                                }, cancelButtonTitle: "CANCEL", cancelAction: {})
                        } else {
                            joinRide()
                        }
                    } else {
                        joinRide()
                    }
                } else {
                    joinRide()
                }
            }
        }
        else if(action == "cancel"){
            if(selectedRide.poolTaxiBooking == "1"){
                //cancel taxi ride
                AlertController.showAlertFor("Cancel Taxi Ride", message: "Would you really like to cancel this taxi ride?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Cancelling Ride.")
                    RideRequestor().cancelTaxiRide(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateRidesList()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
            else{
                //cancel
                AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel this ride?", okButtonTitle: "Yes", okAction: {
                    //from memberslist - FR
                    //from ridedetail & ridelist = JR
                    showIndicator("Cancelling Ride.")
                    RideRequestor().cancelRequest(self.selectedRide!.poolID!, userId: UserInfo.sharedInstance.userID, cancelrequestType: "JR", success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Cancel Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.segment.selectedSegmentIndex = 3
                                self.updateRidesList()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Cancel Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                    }, cancelButtonTitle: "Later", cancelAction: {
                })
            }
        }
        else if(action == "accept"){
            if(selectedRide.poolTaxiBooking == "1"){
                AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this invited ride request?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Accepting ride request..")
                    let youpay = self.selectedRide.finalCost ?? 4
                    let fullcost = self.selectedRide.fullCost ?? 4
                    RideRequestor().acceptTaxiRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, pCost: String(youpay), costMain: String(fullcost), success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Accept Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateTaxiDetails()
                            })
                        }else{
                            AlertController.showAlertFor("Accept Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }, cancelButtonTitle: "Cancel") {
                }
            } else {
                if(selectedRide.poolTaxiBooking != "1" && selectedRide.cost != "0" && selectedRide.distance != "" && selectedRide.distance != "0") {
                    ridePoints = Int(selectedRide.cost!)! * Int(round(Double(selectedRide.distance!)!))
                    if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                        if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                            AlertController.showAlertFor("Accept Ride", message: "\("SORRY! You cannot accept this ride request because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and accept this ride request.")", okButtonTitle: "RECHARGE", okAction: {
                                let vc = MyAccountViewController()
                                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                                }, cancelButtonTitle: "CANCEL", cancelAction: {})
                        } else {
                            acceptRide()
                        }
                    } else {
                        acceptRide()
                    }
                } else {
                    acceptRide()
                }
            }
        }
        else if(action == "reject"){
            if(selectedRide.poolTaxiBooking == "1"){
                AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Rejecting ride request..")
                    RideRequestor().rejectTaxiRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Reject Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateTaxiDetails()
                            })
                        }else{
                            AlertController.showAlertFor("Reject Taxi Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }, cancelButtonTitle: "Cancel") {
                }
                
            } else {
                AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
                    showIndicator("Rejecting ride request..")
                    RideRequestor().rejectRideRequest(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Reject Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                                self.updateRideDetails()
                            })
                        }else{
                            AlertController.showAlertFor("Reject Ride", message: object as? String)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                }, cancelButtonTitle: "Cancel") {
                }
            }
        }
        else if(action == "offeravehicle"){
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                self.getPickUpDropLocation("offerVehicle")
            }
        }
        else if(action == "showmembers"){
            showIndicator("Getting members list..")
            RideRequestor().getRideMembers(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success){
                    let vc = MembersViewController()
                    vc.hidesBottomBarWhenPushed = true
                    vc.poolID = self.selectedRide.poolID
                    vc.membersList = (object as! RideMembersResponse).poolUsers
                    vc.poolTaxiBooking = self.selectedRide.poolTaxiBooking
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }
        else if(action == "unjoinride"){
            unjoinRide()
        }
        else if(action == "threedot") {
            var alertItem1 = ""
            if(self.segmentRides.selectedSegmentIndex == 0) {
                if(self.selectedRide.createdBy == UserInfo.sharedInstance.userID){
                    alertItem1 = "INVITE"
                }else {
                    if let poolStatus = self.selectedRide.poolStatus {
                        if(poolStatus == "J") {
                            alertItem1 = "UNJOIN"
                        }
                    }
                }
            } else {
                alertItem1 = "REPEAT"
            }
            let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            let item1 = UIAlertAction(title: alertItem1, style: .default, handler: { (action) in
                if(alertItem1 == "INVITE") {
                    self.segment.selectedSegmentIndex = 0
                    self.localPoolID = self.selectedRide.poolID!
                    if let poolScope = self.selectedRide.poolScope {
                        if poolScope == "PUBLIC" {
                            let inviteView = InviteNormalRideView()
                            let alertViewHolder =   AlertContentViewHolder()
                            alertViewHolder.heightConstraintValue = 95
                            alertViewHolder.view = inviteView
                            inviteView.messageBtn.addTarget(self, action: #selector(self.message_clicked), for: .touchDown)
                            inviteView.whatsAppBtn.addTarget(self, action: #selector(self.whatsApp_clicked), for: .touchDown)
                            
                            AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
                        } else {
                            let inviteView = InviteWPRideView()
                            let alertViewHolder =   AlertContentViewHolder()
                            alertViewHolder.heightConstraintValue = 95
                            alertViewHolder.view = inviteView
                            inviteView.messageBtn.addTarget(self, action: #selector(self.message_clicked), for: .touchDown)
                            inviteView.whatsAppBtn.addTarget(self, action: #selector(self.whatsApp_clicked), for: .touchDown)
                            inviteView.groupMsgBtn.addTarget(self, action: #selector(self.grpMsg_clicked), for: .touchDown)
                            
                            AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
                        }
                    }
                } else if(alertItem1 == "UNJOIN") {
                    self.unjoinRide()
                } else {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "hh:mm a"
                    dateFormatter.timeZone = TimeZone(identifier: "IST")
                    let rideTime = dateFormatter.date(from: self.selectedRide.startTime!)
                    //rideTime = NSDate(timeInterval: -600, sinceDate: rideTime!)
                    let strCurrentTime = dateFormatter.string(from: Date())
                    let currentTime = dateFormatter.date(from: strCurrentTime)
                    let todayDate = Date()
                    dateFormatter.dateFormat = "dd-MMM-yyyy"
                    let currentDate = dateFormatter.string(from: todayDate) as String
                    if(currentTime!.compare(rideTime!) == ComparisonResult.orderedDescending){
                        self.rescheduleAlert("repeat")
                    } else {
                        let reqObj = recreateRideRequest()
                        reqObj.poolId = self.selectedRide.poolID
                        reqObj.userId = UserInfo.sharedInstance.userID
                        reqObj.datesSelected = currentDate
                        reqObj.poolStartTime = self.selectedRide.startTime
                        showIndicator("Creating Ride...")
                        RideRequestor().recreateRide(reqObj, success:{ (success, object) in
                            hideIndicator()
                            if (success)
                            {
                                AlertController.showAlertFor("Create Ride", message: "Your new ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                                    self.getMyRides("onStart")
                                })
                            }
                            else{
                                if((object as! recreateRideResponse).code == "173") {
                                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                                } else {
                                    AlertController.showToastForError((object as! recreateRideResponse).message!)
                                }
                            }
                            
                            })
                        { (error) in
                            hideIndicator()
                        }
                    }
                }
            })
            vc.addAction(item1)
            
            let item2 = UIAlertAction(title: "RECURRING", style: .default, handler: { (action) in
                let viewHolder = AlertContentViewHolder()
                viewHolder.heightConstraintValue = 95
                
                let view = UIView()
                view.backgroundColor = Colors.WHITE_COLOR
                
                let lblTitle = UILabel()
                lblTitle.text = "Select Dates"
                lblTitle.textColor = Colors.GRAY_COLOR
                lblTitle.font = boldFontWithSize(11)
                lblTitle.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(lblTitle)
                
                let lblMonth = UILabel()
                lblMonth.text = prettyDateStringFromDate(Date(), toFormat: "MMM yyyy")
                lblMonth.textColor = Colors.GREEN_COLOR
                lblMonth.font = boldFontWithSize(14)
                lblMonth.textAlignment = .center
                lblMonth.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(lblMonth)
                
                let datesList = self.getDates().0
                
                let datesView = UIView()
                datesView.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(datesView)
                
                self.btn1 = self.configDateButton()
                self.btn1!.setTitle(datesList[0].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
                self.btn1!.setTitle(datesList[0].replacingOccurrences(of: " ", with: "\n"), for: .selected)
                datesView.addSubview(self.btn1!)
                
                self.btn2 = self.configDateButton()
                self.btn2!.setTitle(datesList[1].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
                self.btn2!.setTitle(datesList[1].replacingOccurrences(of: " ", with: "\n"), for: .selected)
                datesView.addSubview(self.btn2!)
                
                self.btn3 = self.configDateButton()
                self.btn3!.setTitle(datesList[2].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
                self.btn3!.setTitle(datesList[2].replacingOccurrences(of: " ", with: "\n"), for: .selected)
                datesView.addSubview(self.btn3!)
                
                self.btn4 = self.configDateButton()
                self.btn4!.setTitle(datesList[3].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
                self.btn4!.setTitle(datesList[3].replacingOccurrences(of: " ", with: "\n"), for: .selected)
                datesView.addSubview(self.btn4!)
                
                self.btn5 = self.configDateButton()
                self.btn5!.setTitle(datesList[4].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
                self.btn5!.setTitle(datesList[4].replacingOccurrences(of: " ", with: "\n"), for: .selected)
                datesView.addSubview(self.btn5!)
                
                self.btn6 = self.configDateButton()
                self.btn6!.setTitle(datesList[5].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
                self.btn6!.setTitle(datesList[5].replacingOccurrences(of: " ", with: "\n"), for: .selected)
                datesView.addSubview(self.btn6!)
                
                self.btn7 = self.configDateButton()
                self.btn7!.setTitle(datesList[6].replacingOccurrences(of: " ", with: "\n"), for: UIControlState())
                self.btn7!.setTitle(datesList[6].replacingOccurrences(of: " ", with: "\n"), for: .selected)
                datesView.addSubview(self.btn7!)
                
                let btnsDict = ["b1":self.btn1!,"b2":self.btn2!,"b3":self.btn3!,"b4":self.btn4!,"b5":self.btn5!,"b6":self.btn6!,"b7":self.btn7!]
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[b1]-5-[b2(==b1)]-5-[b3(==b1)]-5-[b4(==b1)]-5-[b5(==b1)]-5-[b6(==b1)]-5-[b7(==b1)]-5-|", options: [], metrics: nil, views: btnsDict))
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b1(50)]-5-|", options: [], metrics: nil, views: btnsDict))
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b2(50)]-5-|", options: [], metrics: nil, views: btnsDict))
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b3(50)]-5-|", options: [], metrics: nil, views: btnsDict))
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b4(50)]-5-|", options: [], metrics: nil, views: btnsDict))
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b5(50)]-5-|", options: [], metrics: nil, views: btnsDict))
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b6(50)]-5-|", options: [], metrics: nil, views: btnsDict))
                datesView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[b7(50)]-5-|", options: [], metrics: nil, views: btnsDict))
                
                let lblLine1 = UILabel()
                lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
                lblLine1.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(lblLine1)
                
                let lblTime = UILabel()
                lblTime.text = "Select Time"
                lblTime.textColor = Colors.GRAY_COLOR
                lblTime.font = boldFontWithSize(11)
                lblTime.translatesAutoresizingMaskIntoConstraints = false
                view.addSubview(lblTime)
                
                self.txtTime.layer.borderWidth = 0.5
                self.txtTime.delegate = self
                self.txtTime.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
                self.txtTime.translatesAutoresizingMaskIntoConstraints = false
                self.txtTime.font = boldFontWithSize(14)
                
                self.timePicker.datePickerMode = .time
                self.timePicker.locale = Locale(identifier: "NL")
                self.txtTime.inputView = self.timePicker
                self.txtTime.text = prettyDateStringFromString(self.selectedRide.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
                let dateFormatter = DateFormatter()
                
                dateFormatter.dateFormat = "HH:mm"
                if let time = self.txtTime.text {
                    self.timePicker.date = dateFormatter.date(from: time)!
                }
                
                let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
                keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
                let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donePressed))
                let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
                let toolbarButton = [flexSpace,doneButton]
                keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
                self.txtTime.inputAccessoryView = keyboardDoneButtonShow
                
                let imgV = UIImageView(image: UIImage(named: "time")!)
                imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                imgV.contentMode = .center
                
                self.txtTime.leftView = imgV
                self.txtTime.leftViewMode = .always
                view.addSubview(self.txtTime)
                self.txtTime.placeholder = "Select Time"
                
                
                let viewsDict = ["lbltitle":lblTitle, "lblmonth":lblMonth, "dates":datesView, "line1":lblLine1, "lblTime":lblTime, "txtTime":self.txtTime]
                
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbltitle(20)]-5-[lblmonth(20)]-5-[dates(60)]-5-[line1(0.5)]-5-[lblTime(20)]-5-[txtTime(30)]|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbltitle]|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblmonth]-5-|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[dates]-5-|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lblTime]|", options: [], metrics: nil, views: viewsDict))
                view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txtTime(150)]|", options: [], metrics: nil, views: viewsDict))
                viewHolder.view = view
                
                AlertController.showAlertFor("Recurring Ride", message: "", contentView: viewHolder, okButtonTitle: "SUBMIT", willHaveAutoDismiss: false, okAction: {
                    let reqObj = recreateRideRequest()
                    let dates = self.getDates().1
                    if(self.btn1?.isSelected == true){
                        self.selectedDates.append(dates[0])
                    }
                    if(self.btn2?.isSelected == true){
                        self.selectedDates.append(dates[1])
                    }
                    if(self.btn3?.isSelected == true){
                        self.selectedDates.append(dates[2])
                    }
                    if(self.btn4?.isSelected == true){
                        self.selectedDates.append(dates[3])
                    }
                    if(self.btn5?.isSelected == true){
                        self.selectedDates.append(dates[4])
                    }
                    if(self.btn6?.isSelected == true){
                        self.selectedDates.append(dates[5])
                    }
                    if(self.btn7?.isSelected == true){
                        self.selectedDates.append(dates[6])
                    }
                    
                    if(self.selectedDates.count > 0){
                        reqObj.datesSelected = self.selectedDates.joined(separator: ",")
                    }
                    else{
                        AlertController.showToastForError("Please choose a date for your travel.")
                        return
                    }
                    reqObj.poolId = self.selectedRide.poolID
                    reqObj.userId = UserInfo.sharedInstance.userID
                    reqObj.poolStartTime = self.txtTime.text
                    self.dismiss(animated: true, completion:{
                        showIndicator("Creating Ride...")
                        RideRequestor().recreateRide(reqObj, success:{ (success, object) in
                            hideIndicator()
                            if (success)
                            {
                                self.getMyRides("onStart")
                            }
                            else{
                                if((object as! recreateRideResponse).code == "173") {
                                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                                } else {
                                    AlertController.showToastForError((object as! recreateRideResponse).message!)
                                }
                            }
                            })
                        { (error) in
                            hideIndicator()
                        }
                    })
                    }, cancelButtonTitle: "Cancel", cancelAction:
                    {
                        self.dismiss(animated: true, completion: nil)
                })
            })
            vc.addAction(item2)
            
            let item3 = UIAlertAction(title: "RESCHEDULE", style: .default, handler: { (action) in
                if(self.segmentRides.selectedSegmentIndex == 0) {
                    self.rescheduleAlert("upcomingreschedule")
                } else {
                    self.rescheduleAlert("completedreschedule")
                }
            })
            vc.addAction(item3)
            
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                
            })
            vc.addAction(actionCancel)
            
            present(vc, animated: true, completion: nil)
        }
    }
    
    func rescheduleAlert(_ triggerFrom: String) {
        let viewHolder = AlertContentViewHolder()
        viewHolder.heightConstraintValue = 95
        
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        self.txtDate.layer.borderWidth = 0.5
        self.txtDate.delegate = self
        self.txtDate.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        self.txtDate.translatesAutoresizingMaskIntoConstraints = false
        self.txtDate.font = boldFontWithSize(14)
        
        self.datePicker.datePickerMode = .date
        self.txtDate.inputView = self.datePicker
        self.datePicker.minimumDate = Date()
        self.datePicker.addTarget(self, action: #selector(self.datePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        self.txtDate.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "cal")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        self.txtDate.leftView = imgV
        self.txtDate.leftViewMode = .always
        
        view.addSubview(self.txtDate)
        
        self.txtTime.layer.borderWidth = 0.5
        self.txtTime.delegate = self
        self.txtTime.font = boldFontWithSize(14)
        self.txtTime.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        self.txtTime.translatesAutoresizingMaskIntoConstraints = false
        
        self.timePicker.datePickerMode = .time
        self.timePicker.locale = Locale(identifier: "NL")
        self.txtTime.inputView = self.timePicker
        self.timePicker.addTarget(self, action: #selector(self.timePicked), for: .valueChanged)
        
        self.txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV1 = UIImageView(image: UIImage(named: "time")!)
        imgV1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV1.contentMode = .center
        
        self.txtTime.leftView = imgV1
        self.txtTime.leftViewMode = .always
        view.addSubview(self.txtTime)
        
        pastTimeLbl.text = "Sorry, you can not create ride for past time"
        pastTimeLbl.translatesAutoresizingMaskIntoConstraints = false
        pastTimeLbl.textColor = Colors.RED_COLOR
        pastTimeLbl.font = normalFontWithSize(12)
        view.addSubview(pastTimeLbl)
        if(triggerFrom == "upcomingreschedule") {
            txtDate.text = prettyDateStringFromString(self.selectedRide.startDate, fromFormat: "dd MMM yyyy", toFormat: "dd-MMM-yyyy")
        } else {
            txtDate.text = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        }
        txtTime.text = prettyDateStringFromString(self.selectedRide.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        if let date = txtDate.text {
            datePicker.date = dateFormatter.date(from: date)!
        }
        
        dateFormatter.dateFormat = "HH:mm"
        if let time = txtTime.text {
            timePicker.date = dateFormatter.date(from: time)!
        }
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[dateTxt(30)]-5-[pastTimeLbl]|", options: [], metrics: nil, views: ["dateTxt":self.txtDate, "pastTimeLbl":pastTimeLbl]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[dateTxt]-10-[timeTxt(==dateTxt)]-10-|", options: [], metrics: nil, views: [ "dateTxt":self.txtDate, "timeTxt":self.txtTime]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[pastTimeLbl]-10-|", options: [], metrics: nil, views: [ "pastTimeLbl": pastTimeLbl]))
        view.addConstraint(NSLayoutConstraint(item: self.txtTime, attribute: .centerY, relatedBy: .equal, toItem: self.txtDate, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: self.txtTime, attribute: .height, relatedBy: .equal, toItem: self.txtDate, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        viewHolder.view = view
        if(triggerFrom == "repeat") {
            pastTimeLbl.isHidden = false
        } else {
            pastTimeLbl.isHidden = true
        }
        
        if(self.selectedRide.createdBy == UserInfo.sharedInstance.userID) {
            AlertController.showAlertFor("Reschedule Ride", message: "", contentView: viewHolder, okButtonTitle: "SUBMIT", willHaveAutoDismiss: false, okAction: {
                if(triggerFrom == "upcomingreschedule") {
                    self.updateRide()
                } else {
                    self.recreateRide(triggerFrom)
                }
                }, cancelButtonTitle: "Cancel", cancelAction:
                {
                    self.dismiss(animated: true, completion: nil)
            })
        } else {
            self.recreateAlert()
        }
    }
    
    func recreateAlert () {
        let viewHolder = AlertContentViewHolder()
        viewHolder.heightConstraintValue = 95
        
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        let lblInfo = UILabel()
        lblInfo.text = "You are not ride initiator, hence you cannot update the ride. Would you like to create new ride?"
        lblInfo.numberOfLines = 0
        //lblInfo.lineBreakMode = .ByWordWrapping
        lblInfo.translatesAutoresizingMaskIntoConstraints = false
        lblInfo.font = normalFontWithSize(12)
        view.addSubview(lblInfo)
        
        self.txtDate.layer.borderWidth = 0.5
        self.txtDate.delegate = self
        self.txtDate.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        self.txtDate.translatesAutoresizingMaskIntoConstraints = false
        self.txtDate.font = boldFontWithSize(14)
        
        self.datePicker.datePickerMode = .date
        self.txtDate.inputView = self.datePicker
        self.datePicker.minimumDate = Date()
        self.datePicker.addTarget(self, action: #selector(self.datePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(self.donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        self.txtDate.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "cal")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        self.txtDate.leftView = imgV
        self.txtDate.leftViewMode = .always
        
        view.addSubview(self.txtDate)
        
        self.txtTime.layer.borderWidth = 0.5
        self.txtTime.delegate = self
        self.txtTime.font = boldFontWithSize(14)
        self.txtTime.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        self.txtTime.translatesAutoresizingMaskIntoConstraints = false
        
        self.timePicker.datePickerMode = .time
        self.timePicker.locale = Locale(identifier: "NL")
        self.txtTime.inputView = self.timePicker
        self.timePicker.addTarget(self, action: #selector(self.timePicked), for: .valueChanged)
        
        self.txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV1 = UIImageView(image: UIImage(named: "time")!)
        imgV1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV1.contentMode = .center
        
        self.txtTime.leftView = imgV1
        self.txtTime.leftViewMode = .always
        view.addSubview(self.txtTime)
        
        txtDate.text = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        txtTime.text = prettyDateStringFromString(self.selectedRide.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        if let date = txtDate.text {
            datePicker.date = dateFormatter.date(from: date)!
        }
        
        txtTime.text = prettyDateStringFromString(self.selectedRide.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
        dateFormatter.dateFormat = "HH:mm"
        if let time = txtTime.text {
            timePicker.date = dateFormatter.date(from: time)!
        }
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[infoLbl]-10-[dateTxt(30)]|", options: [], metrics: nil, views: ["dateTxt":self.txtDate, "infoLbl":lblInfo]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[dateTxt]-10-[timeTxt(==dateTxt)]-10-|", options: [], metrics: nil, views: [ "dateTxt":self.txtDate, "timeTxt":self.txtTime]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[infoLbl]-10-|", options: [], metrics: nil, views: [ "infoLbl": lblInfo]))
        view.addConstraint(NSLayoutConstraint(item: self.txtTime, attribute: .centerY, relatedBy: .equal, toItem: self.txtDate, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: self.txtTime, attribute: .height, relatedBy: .equal, toItem: self.txtDate, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        viewHolder.view = view
        
        AlertController.showAlertFor("Confirm For Recreate Ride", message: "", contentView: viewHolder, okButtonTitle: "SUBMIT", willHaveAutoDismiss: false, okAction: {
            self.recreateRide("completedreschedule")
            }, cancelButtonTitle: "Cancel", cancelAction:
            {
                self.dismiss(animated: true, completion: nil)
        })
    }
    func datePicked()
    {
        
    }
    
    func timePicked()
    {
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    
    func donePressed()
    {
        if(selectedTxtField == txtDate){
            //needRideObj.rideDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            selectedTxtField?.text = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
        }
        else if(selectedTxtField == txtTime){
            //needRideObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            selectedTxtField?.text = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        }
        let minDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        let minTime = prettyDateStringFromDate(Date(), toFormat: "HH:mm")
        //airportRideObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        let compareResult = minDate.compare(txtDate.text!)
        let compareResultTime = minTime.compare(txtTime.text!)
        if(compareResult == ComparisonResult.orderedSame && compareResultTime == ComparisonResult.orderedDescending) {
            pastTimeLbl.isHidden = false
        } else {
            pastTimeLbl.isHidden = true
        }
        selectedTxtField?.resignFirstResponder()
    }
    
    func configDateButton() -> UIButton
    {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.borderWidth = 1
        btn.layer.borderColor = Colors.GREEN_COLOR.cgColor
        btn.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btn.setTitleColor(Colors.WHITE_COLOR, for: .selected)
        btn.addTarget(self, action: #selector(dateBtnClicked(_:)), for: .touchDown)
        
        btn.titleLabel?.textAlignment = .center
        btn.titleLabel?.lineBreakMode = .byWordWrapping
        btn.titleLabel?.font = boldFontWithSize(14)
        return btn
    }
    
    func dateBtnClicked(_ btn : UIButton)
    {
        if(btn.isSelected == true){
            btn.backgroundColor = Colors.WHITE_COLOR
            btn.isSelected = false
        }
        else{
            btn.isSelected = true
            btn.backgroundColor = Colors.GREEN_COLOR
        }
    }
    
    func getDates() -> ([String],[String])
    {
        var datesList = [String]()
        var datesObjList = [String]()
        
        var today = Date()
        
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        today = today.addingTimeInterval(86400)
        datesList.append(prettyDateStringFromDate(today, toFormat: "dd EEE"))
        datesObjList.append(prettyDateStringFromDate(today, toFormat: "dd-MMM-yyyy"))
        
        return (datesList, datesObjList)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.WHITE_COLOR
        if(isMyRidesVisible == true){
            if(hasMoreEntries){
                let lastElement = dataSource.count - 1
                if indexPath.row == lastElement{
                    currentPageNumber = currentPageNumber + 1
                    getAllRides()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(segmentRides.selectedSegmentIndex != 2) {
            showIndicator("Fetching Ride Details")
            //                        if(isMyRidesVisible == false && segmentRides.selectedSegmentIndex == 1){
            //                            RideRequestor().getTaxiDetails(dataSource[indexPath.row].poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            //                                hideIndicator()
            //                                if(success){
            //                                    let vc = TaxiDetailViewController()
            //                                    if let sourceID = self.dataSource[indexPath.row].sourceID{
            //                                        vc.taxiSourceID = sourceID
            //                                    }
            //                                    if let destID = self.dataSource[indexPath.row].destID{
            //                                        vc.taxiDestID = destID
            //                                    }
            //                                    vc.isFirstLevel = true
            //                                    vc.onDeleteActionBlock = {
            //                                        self.updateRidesList()
            //                                    }
            //                                    vc.hidesBottomBarWhenPushed = true
            //                                    vc.data = object as? RideDetail
            //                                    self.navigationController?.pushViewController(vc, animated: true)
            //                                }
            //                            }) { (error) in
            //                                hideIndicator()
            //                            }
            //                        } else {
            if(self.dataSource[indexPath.row].poolTaxiBooking == "1") {
                RideRequestor().getTaxiDetails(dataSource[indexPath.row].poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success){
                        let vc = TaxiDetailViewController()
                        if let sourceID = self.dataSource[indexPath.row].sourceID{
                            vc.taxiSourceID = sourceID
                        }
                        if let destID = self.dataSource[indexPath.row].destID{
                            vc.taxiDestID = destID
                        }
                        vc.isFirstLevel = true
                        vc.onDeleteActionBlock = {
                            self.updateRidesList()
                        }
                        vc.hidesBottomBarWhenPushed = true
                        vc.data = object as? RideDetail
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }) { (error) in
                    hideIndicator()
                }
            } else {
                RideRequestor().getRideDetails(dataSource[indexPath.row].poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success){
                        //let vc = RideDetailViewController()
                        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                        let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                        if let sourceID = self.dataSource[indexPath.row].sourceID{
                            vc.taxiSourceID = sourceID
                        }
                        if let destID = self.dataSource[indexPath.row].destID{
                            vc.taxiDestID = destID
                        }
                        vc.isFirstLevel = true
                        vc.onDeleteActionBlock = {
                            self.updateRidesList()
                        }
                        vc.hidesBottomBarWhenPushed = true
                        vc.edgesForExtendedLayout = UIRectEdge()
                        vc.data = object as? RideDetail
                        if let poolScope = self.dataSource[indexPath.row].poolScope {
                            if poolScope == "PUBLIC" {
                                vc.triggerFrom = "ride"
                            } else {
                                vc.triggerFrom = "place"
                            }
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }) { (error) in
                    hideIndicator()
                }
            }
            //}
        }
    }
    
    func redirectScreen(_ haveData: String?) {
        let vc = CreateRideMapViewController()
        //vc.vehicleDefaultDetails = vehicleDefaultDetails
        vc.allRidesData = self.allRidesData
        vc.haveData = haveData!
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnAddRideClicked()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Ride Tab",
            AnalyticsParameterItemName: "Footer",
            AnalyticsParameterContentType:"Need A Ride Button"
            ])
        showIndicator("Loading Rides.")
        let reqObj = AllRidesRequest()
        reqObj.page = ""
        reqObj.pooltype = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        reqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        reqObj.reqTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        reqObj.rideScope = ""
        reqObj.searchType = "generic"
        reqObj.showRtype = "AC"
        reqObj.userId = UserInfo.sharedInstance.userID
        
        RideRequestor().getAllRides(reqObj, success: { (success, object) in
            hideIndicator()
//            if((object as! AllRidesResponse).pools!.count == 0){
//                self.hasMoreEntries = false
//            }
            //self.dataSource.removeAll()
            self.allRidesData = ((object as! AllRidesResponse).pools!)
            RideRequestor().getDefaultVehicle({ (successVeh, objectVeh) in
                if (successVeh) {
                    self.vehicleDefaultDetails = objectVeh as! VehicleDefaultResponse
                    self.redirectScreen("yesData")
                } else {
                    self.redirectScreen("noData")
                }
            }) { (error) in
                hideIndicator()
            }
        }) { (error) in
            hideIndicator()
        }

        
        
        
        //        let vc = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        //        let actionNeedRide = UIAlertAction(title: "Need a Ride", style: .Default, handler: { (action) in
        //            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
        //                kFIRParameterItemID:"Ride Tab",
        //                kFIRParameterItemName: "Footer",
        //                kFIRParameterContentType:"Need A Ride Button"
        //                ])
        //            let childVC = CreateRideMapViewController()
        //            childVC.hidesBottomBarWhenPushed = true
        //            self.navigationController?.pushViewController(childVC, animated: true)
        //        })
        //        vc.addAction(actionNeedRide)
        //
        //        let actionOfferVehicle = UIAlertAction(title: "Offer a Vehicle", style: .Default, handler: { (action) in
        //            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
        //                kFIRParameterItemID:"Ride Tab",
        //                kFIRParameterItemName: "Footer",
        //                kFIRParameterContentType:"Offer A Vehicle Button"
        //                ])
        //            let childVC = OfferVehicleViewController()
        //            childVC.hidesBottomBarWhenPushed = true
        //            self.navigationController?.pushViewController(childVC, animated: true)
        //        })
        //        vc.addAction(actionOfferVehicle)
        //
        //        let actionShareTaxi = UIAlertAction(title: "Share a Taxi", style: .Default, handler: { (action) in
        //            FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
        //                kFIRParameterItemID:"Ride Tab",
        //                kFIRParameterItemName: "Footer",
        //                kFIRParameterContentType:"Share A Taxi Button"
        //                ])
        //            let childVC = ShareTaxiViewController()
        //            childVC.hidesBottomBarWhenPushed = true
        //            self.navigationController?.pushViewController(childVC, animated: true)
        //        })
        //        vc.addAction(actionShareTaxi)
        //
        //        let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) in
        //        })
        //        vc.addAction(actionCancel)
        //
        //        presentViewController(vc, animated: true, completion: nil)
    }
    
    func btnSearchClicked()
    {
        showSearchRides()
    }
    
    func btnRidesClicked(_ btn: UIButton)
    {
        isSearchResults = false
        if(isMyRidesVisible){
            //btn.setImage(UIImage(named: "Explore"), forState: .Normal)
            lblRides.text = "EXPLORE"
            isMyRidesVisible = false
            
            segmentRides.isHidden = false
            titleHeightConstraint?.constant = 45
            self.navigationItem.title = "My Rides"
            //lblTitle.text = "List of my Pool Ride(s)"
            if(isOnStart) {
                if(segmentRides.selectedSegmentIndex == 0) {
                    getMyRides("onClick")
                } else if(segmentRides.selectedSegmentIndex == 1) {
                    getOngoingRides()
                } else {
                    getCompletedRides("0")
                }
            }
            //             let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
            //             let vc : RideViewMapViewController = storyboard.instantiateViewControllerWithIdentifier("RideViewMapViewController") as! RideViewMapViewController
            //             vc.hidesBottomBarWhenPushed = true
            //             vc.data = dataSource
            //             self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            isOnStart = true
            btn.setImage(UIImage(named: "MyRide"), for: UIControlState())
            lblRides.text = "MY RIDES"
            isMyRidesVisible = true
            
            titleHeightConstraint?.constant = 0
            segmentRides.isHidden = true
            self.navigationItem.title = "Existing Rides"
            currentPageNumber = 1
            hasMoreEntries = true
            dataSource.removeAll()
            getAllRides()
        }
        
    }
    
//    func setUpBtn(btn: UIButton, title: String)
//    {
//        btn.contentVerticalAlignment = .Top
//        btn.setImage(UIImage(named: title), forState: .Normal)
//        btn.translatesAutoresizingMaskIntoConstraints = false
//    }
    
    func recreateRide (_ triggerFrom: String) {
        let reqObj = recreateRideRequest()
        reqObj.poolId = self.selectedRide.poolID
        reqObj.userId = UserInfo.sharedInstance.userID
        reqObj.datesSelected = self.txtDate.text
        reqObj.poolStartTime = self.txtTime.text
        let minDate = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "dd-MMM-yyyy")
        let minTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        let compareResult = minDate.compare(txtDate.text!)
        let compareResultTime = minTime.compare(txtTime.text!)
        if(compareResult != ComparisonResult.orderedSame || compareResultTime != ComparisonResult.orderedDescending) {
            self.dismiss(animated: true, completion:{
                showIndicator("Creating Ride...")
                RideRequestor().recreateRide(reqObj, success:{ (success, object) in
                    hideIndicator()
                    if (success)
                    {
                        AlertController.showAlertFor("Create Ride", message: "Your new ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                            if(triggerFrom == "completedreschedule") {
                                let poolID = (object as! recreateRideResponse).data?.first?.poolId
                                showIndicator("Fetching Ride Details")
                                RideRequestor().getRideDetails(poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                    hideIndicator()
                                    if(success){
                                        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                        let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                        //let vc = RideDetailViewController()
                                        vc.hidesBottomBarWhenPushed = true
                                        vc.edgesForExtendedLayout = UIRectEdge()
                                        vc.data = object as? RideDetail
                                        vc.triggerFrom = "ride"
                                        vc.isFirstLevel = true
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }) { (error) in
                                    hideIndicator()
                                }
                            } else {
                                self.getMyRides("onStart")
                            }
                        })
                    }
                    else{
                        if((object as! recreateRideResponse).code == "173") {
                            AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                        } else {
                            AlertController.showToastForError((object as! recreateRideResponse).message!)
                        }
                    }
                    
                    })
                { (error) in
                    hideIndicator()
                }
            })
        } else {
            AlertController.showToastForError("Sorry, You can create ride after 5 mins from current time.")
        }
    }
    
    func updateRide () {
        let updateReqObj = UpdateRideRequest()
        updateReqObj.from_lon = self.selectedRide.sourceLong
        updateReqObj.from_lat = self.selectedRide.sourceLat
        updateReqObj.from_location = self.selectedRide.fullSource
        updateReqObj.to_location = self.selectedRide.fullDest
        updateReqObj.to_lat = self.selectedRide.destLat
        updateReqObj.to_lon = self.selectedRide.destLong
        updateReqObj.cost = self.selectedRide.cost
        updateReqObj.cost_type = "PK"
        updateReqObj.poolCategory = self.selectedRide.poolCategory
        updateReqObj.poolID = self.selectedRide.poolID
        updateReqObj.poolShared = self.selectedRide.poolShared
        //updateReqObj.poolstartdate = self.selectedRide.startDate
        //updateReqObj.poolstarttime = self.selectedRide.startTime
        updateReqObj.pooltype = self.selectedRide.poolRideType
        updateReqObj.rideMessage = self.selectedRide.message
        updateReqObj.rideType = self.selectedRide.rideType
        updateReqObj.seatLeft = self.selectedRide.seatsLeft
        updateReqObj.vehregno = self.selectedRide.vehicleRegNo
        updateReqObj.userID = self.selectedRide.createdBy
        updateReqObj.email = self.selectedRide.ownerEmail
        updateReqObj.duration = self.selectedRide.duration
        updateReqObj.distance = self.selectedRide.distance
        updateReqObj.via = self.selectedRide.via
        updateReqObj.via_lat = self.selectedRide.viaLat
        updateReqObj.via_lon = self.selectedRide.viaLon
        
        updateReqObj.poolstarttime = txtTime.text
        updateReqObj.poolstartdate = txtDate.text
        
        let minDate = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "dd-MMM-yyyy")
        let minTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        //airportRideObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
        let compareResult = minDate.compare(txtDate.text!)
        let compareResultTime = minTime.compare(txtTime.text!)
        
        if(updateReqObj.from_location != updateReqObj.to_location){
            if(compareResult != ComparisonResult.orderedSame || compareResultTime != ComparisonResult.orderedDescending) {
                self.dismiss(animated: true, completion:{
                    showIndicator("Updating Ride..")
                    RideRequestor().updateRide(updateReqObj, success: { (success, object) in
                        hideIndicator()
                        
                        if(success == true){
                            showIndicator("Fetching Ride Details")
                            RideRequestor().getRideDetails(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                hideIndicator()
                                if(success){
                                    let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                    let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                    //let vc = RideDetailViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    vc.data = object as? RideDetail
                                    vc.triggerFrom = "ride"
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }) { (error) in
                                hideIndicator()
                            }
                        }
                        else{
                            AlertController.showToastForError((object as! BaseModel).message!)
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
                })
            } else {
                AlertController.showToastForError("Sorry, You can update ride after 5 mins from current time.")
            }
        }
        else{
            AlertController.showAlertFor("Update Ride", message: "Please choose different locations for source and destination.")
        }
    }
    
    func unjoinRide () {
        AlertController.showAlertFor("Unjoin Ride", message: "Would you really like to unjoin from this ride?", okButtonTitle: "YES", okAction: {
            showIndicator("Unjoining the ride..")
            RideRequestor().unJoinRide(self.selectedRide.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Unjoin Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateRidesList()
                    })
                }else{
                    AlertController.showAlertFor("Unjoin Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "LATER") {
        }
    }
    
    func checkIfUserUsesFBAccountForLoginORSignUP() -> Void
    {
        
        if  UserInfo.sharedInstance.mobile.characters.count > 0
        {
            callMobileNumberVerificationForNormaLogin()
        }
        else
        {
            callMobileNumberVerificationForFBLogin()
        }
        
    }
    
    func callMobileNumberVerificationForNormaLogin() -> Void
    {
        if  UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3"
        {
            let verifyMobileViewInstance = VerifyMobileNumberWithCodeView()
            let alertViewHolder =   AlertContentViewHolder()
            alertViewHolder.heightConstraintValue = 200
            alertViewHolder.view = verifyMobileViewInstance
            
            
            AlertController.showAlertFor("Verify mobile number", message: "", contentView: alertViewHolder, okButtonTitle: "RESEND CODE", willHaveAutoDismiss: false, okAction: {
                
                hideIndicator()
                
                if verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count > 0 && verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count == 10
                {
                    let reqObject = ResendCodeNumberRequest()
                    reqObject.userID = UserInfo.sharedInstance.userID
                    reqObject.mobileNumber = verifyMobileViewInstance.mobileNumberTextBox.text
                    
                    showIndicator("Loading...")
                    
                    VerifyMobileNumberRequestor().resendCodeNumber(reqObject, success:{ (success, object) in
                        
                        hideIndicator()
                        
                        if (object as! ResendCodeNumberResponse).code == "257"
                        {
                            UserInfo.sharedInstance.mobile = verifyMobileViewInstance.mobileNumberTextBox.text!
                            AlertController.showToastForInfo("You would receive verification code shortly via OTP. Please enter the code to verify your number.")
                        }
                        else if (object as! ResendCodeNumberResponse).code == "199"
                        {
                            AlertController.showToastForError("Sorry, this mobile number is already registered with us.")
                        }
                        else
                        {
                            AlertController.showToastForError("Something went wrong. Please try again.")
                        }
                        
                        })
                    {(error) in
                        hideIndicator()
                        AlertController.showAlertForError(error)
                    }
                }
                else if verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count > 0 && verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count < 10
                {
                    AlertController.showToastForError("Please provide valid 'Mobile No'.")
                }
                else
                {
                    AlertController.showToastForError("Please provide your Indian mobile number to proceed further.")
                }
                
                }, cancelButtonTitle: "VERIFY", cancelAction: {
                    
                    if UserInfo.sharedInstance.mobile == verifyMobileViewInstance.mobileNumberTextBox.text
                    {
                        if verifyMobileViewInstance.smsCodeNumberTextBox.text?.characters.count > 0
                        {
                            let reqObject = VerifyMobileNumberRequest()
                            showIndicator("Loading...")
                            reqObject.userID = UserInfo.sharedInstance.userID
                            reqObject.verificationCode = verifyMobileViewInstance.smsCodeNumberTextBox.text
                            
                            VerifyMobileNumberRequestor().getVerifyNumber(reqObject, success:{ (success, object) in
                                
                                hideIndicator()
                                
                                if (object as! VerifyMobileNumberResponse).code == "251"
                                {
                                    if(UserInfo.sharedInstance.userState == "3") {
                                        UserInfo.sharedInstance.userState = "5"
                                    } else {
                                        UserInfo.sharedInstance.userState = "4"
                                    }
                                    AlertController.showToastForInfo("Your mobile number is verified successfully.")
                                    self.dismiss(animated: true, completion: nil)
                                    self.getMyRides("onStart")
                                }
                                    
                                else if (object as! VerifyMobileNumberResponse).code == "252"
                                {
                                    AlertController.showToastForError("Verification code is incorrect. Please try again.")
                                }
                                else
                                {
                                    AlertController.showToastForError("Something went wrong. Please try again.")
                                }
                                
                                })
                            {(error) in
                                hideIndicator()
                                AlertController.showAlertForError(error)
                            }
                        }
                        else
                        {
                            AlertController.showToastForError("Please provide verification code.")
                        }
                        
                    }
                    else
                    {
                        AlertController.showToastForError("Your number does not match with registered number. Please either resend code or enter existing number.")
                    }
            })
            
        } else {
            self.getMyRides("onStart")
        }
    }
    
    func callMobileNumberVerificationForFBLogin() -> Void
    {
        if  UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3"
        {
            let alertViewHolder =   AlertContentViewHolder()
            alertViewHolder.heightConstraintValue = 200
            let verifyMobileViewInstance = VerifyMobileNumberView()
            alertViewHolder.view = verifyMobileViewInstance
            
            AlertController.showAlertFor("", message: "", contentView: alertViewHolder, okButtonTitle: "SEND CODE", willHaveAutoDismiss: false, okAction: {
                
                if verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count > 0 && verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count == 10
                {
                    
                    let reqObject = ResendCodeNumberRequest()
                    reqObject.userID = UserInfo.sharedInstance.userID
                    reqObject.mobileNumber = verifyMobileViewInstance.mobileNumberTextBox.text
                    UserInfo.sharedInstance.mobile = verifyMobileViewInstance.mobileNumberTextBox.text!
                    
                    showIndicator("Loading...")
                    
                    VerifyMobileNumberRequestor().resendCodeNumber(reqObject, success:{ (success, object) in
                        
                        hideIndicator()
                        
                        if (object as! ResendCodeNumberResponse).code == "257"
                        {
                            AlertController.showToastForInfo("You would receive verification code shortly via OTP. Please enter the code to verify your number.")
                            
                            self.dismiss(animated: true, completion: {
                                self.callMobileNumberVerificationForNormaLogin()
                            })
                        }
                        else if (object as! ResendCodeNumberResponse).code == "199"
                        {
                            AlertController.showToastForError("Sorry, this mobile number is already registered with us.")
                        }
                        else
                        {
                            AlertController.showToastForError("Something went wrong. Please try again.")
                        }
                        
                        })
                    {(error) in
                        hideIndicator()
                        AlertController.showAlertForError(error)
                    }
                }
                else if verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count > 0 && verifyMobileViewInstance.mobileNumberTextBox.text?.characters.count < 10
                {
                    AlertController.showToastForError("Please provide valid 'Mobile No'.")
                }
                else
                {
                    AlertController.showToastForError("Please provide your Indian mobile number to proceed further.")
                }
                
                }, cancelButtonTitle: nil, cancelAction: nil)
        } else {
            self.getMyRides("onStart")
        }
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
        self.updateRidesList()
    }
}
