//
//  WPMapViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 12/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces


class WPMapViewController: UIViewController , CLLocationManagerDelegate, UITextFieldDelegate,GMSMapViewDelegate{
    var data: WorkPlace?
    let segment = HMSegmentedControl()
    
    let cell_height: CGFloat = 40.0
    let big_cell_height: CGFloat = 60.0
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var mapView:GMSMapView!
    let searchTextField = UITextField()
    var placeSearchResultsArray = [AnyObject]()
    var plusImage = UIButton()
    var suggestionView = UIView()
    var currentLocationView = UIView()
    var promptTableView = UITableView()
    var tableHeightConstraint: NSLayoutConstraint?
    var userMovedMap: Bool = false
    var locationButton: UIButton = UIButton(type: .system)
    let appDelegate: AppDelegate = sharedAppDelegate()!
    var locationButtonAction: VoidCompletionHandler?
    var shouldShowCancelButton = false
    var cancelButton = UIButton(type: .system)
    var currentLocationButton = UIButton(type: .custom)
    var locationName: String = ""
    var onSelectionBlock: actionBlockWithParam?
    
    var signUPButton = UIButton()
    var termsofConditionLabelText = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.APP_BG
        title = "Workplace Location"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        self.setupGoogleMaps()
    }
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Workplace Location"
    }
    
    
    func cancelButtonAction () {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGoogleMaps () {
        let wpLatitude = data!.wpLat != nil ? data!.wpLat : data!.wpDetailLat
        let wpLongitude = data!.wpLong != nil ? data!.wpLong : data!.wpDetailLong
        let camera = GMSCameraPosition.camera(withLatitude: Double(wpLatitude!)!, longitude: Double(wpLongitude!)!, zoom: 16)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        self.view.sendSubview(toBack: self.mapView)
        
        let viewsDict = ["mapView": mapView]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[mapView]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[mapView]-5-|", options: [], metrics: nil, views: viewsDict))
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(wpLatitude!)!, longitude: Double(wpLongitude!)!)
        marker.title = data!.wpName
        marker.snippet = data!.wpLocation
        marker.icon = UIImage(named: "btn_set_location")
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = mapView
            }
    
    // MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationCoord = manager.location!.coordinate
        self.moveMapToCoord(self.locationCoord)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
        } else if status == .denied {
            AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
        }
    }
       //MARK:- place search
    func findSearchResultsForText(_ searchText: String) {
        let characterSet = CharacterSet.letters
        let range = searchText.rangeOfCharacter(from: characterSet)
        if (range != nil) {
            self.getPlacesForText(searchText)
        } else {
            self.getPincodesForText(searchText)
        }
    }
    
    func getPincodesForText(_ searchText: String) {
    }
    
    func getPlacesForText(_ searchText: String) {
        let placesClient = GMSPlacesClient()
        
        let filter = GMSAutocompleteFilter()
        filter.country = INDIA_COUNTRY_CODE
        filter.type = GMSPlacesAutocompleteTypeFilter.geocode
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: filter) { [weak self] (results, error:Error?) -> Void in
            self?.placeSearchResultsArray.removeAll()
            if results == nil {
                return
            }
            self?.placeSearchResultsArray = results! as [AnyObject]
            self?.presentAddressData()
        }
    }
    
    let MAX_ROWS = 5
    let section_height: CGFloat = 20
    func presentAddressData () {
        let count = self.placeSearchResultsArray.count > MAX_ROWS ? MAX_ROWS : self.placeSearchResultsArray.count
        let height_cell = cell_height
        let extra = count != 0 ? section_height : 0
        self.tableHeightConstraint?.constant = CGFloat(count) * height_cell + extra
        self.promptTableView.reloadData()
        self.suggestionView.layoutIfNeeded()
    }
    
    //MARK:- Navigation to Master Service
    func performDidSelectForGoogleAPI (_ placeObject: GMSAutocompletePrediction) {
        let client = GMSPlacesClient()
        client.lookUpPlaceID(placeObject.placeID!) { [weak self] (place, error) -> Void in
            if let coord = place?.coordinate {
                self?.moveMapToCoord(coord)
            }
        }
        
        // added to reset tableView on selection
        searchTextField.resignFirstResponder()
        resetAddressTable()
    }
    
    func resetAddressTable () {
        self.placeSearchResultsArray.removeAll()
        presentAddressData()
    }
    
    func selectLocationButtonTapped (_ sender: AnyObject!) {
        
        if let completionHandler = locationButtonAction {
            completionHandler()
        } else {
            if let coord = self.locationCoord{
                proceedWithSelectedLatLong(coord)
            }
        }
    }
    
    func proceedWithSelectedLatLong (_ coord : CLLocationCoordinate2D) {
        
        onSelectionBlock?(["lat":"\(coord.latitude)", "long":"\(coord.longitude)", "location":searchTextField.text!])
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func getDataForCoord (_ coord : CLLocationCoordinate2D) {
    }
    
    func goToCurrentLocation (_ sender: UIButton) {
        if let location = mapView.myLocation {
            self.userMovedMap = true
            moveMapToCoord(location.coordinate)
        }
        
    }
    
    // MARK:- Google Map Delegates
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        /*self.locationCoord = coordinate
         self.moveMapToCoord(self.locationCoord)
         reverseGeocodeCoordinate(self.locationCoord)*/
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.userMovedMap = self.userMovedMap ? self.userMovedMap : gesture
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if self.userMovedMap == true {
            self.userMovedMap = false
            self.locationCoord = position.target
            self.moveMapToCoord(self.locationCoord)
            //reverseGeocodeCoordinate(self.locationCoord)
        }
    }
    
    func moveMapToCoord(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        self.locationCoord = coordinate
        let camera = self.mapView.camera
        let cameraPosition = GMSCameraPosition.camera(withTarget: coord, zoom: camera.zoom)
        self.mapView.animate(to: cameraPosition)
    }
}
