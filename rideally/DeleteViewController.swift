//
//  DeleteViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 12/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class DeleteViewController: UIViewController {

    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var alertMessage: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func yesButtonClicked(_ sender: AnyObject) {
        
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kDeleteWorkPlaceNotification"), object: nil)
        }
    }

    @IBAction func laterButtonClicked(_ sender: AnyObject) {
        
        self.dismiss(animated: true) {
            
        }
    }
}
