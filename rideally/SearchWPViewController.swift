//
//  SearchWPViewController.swift
//  rideally
//
//  Created by Sarav on 29/10/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class SearchWPViewController: BaseScrollViewController {
    
    var completionBlock: actionBlockWith4ParamsString?
    var resetBlock: actionBlock?
    var selectedTxtField: UITextField?
    let txtWPName = UITextField()
    let txtWPLocation = UITextField()
    
    var wpLat: String?
    var wpLong: String?
    let lblErrorMsg = UILabel()
    var allPlacesRB :  DLRadioButton!
    var workplaceRB : DLRadioButton!
    var housingRB :  DLRadioButton!
    var eventRB : DLRadioButton!
    var wpSearchData: WPSearchRequest?
    var categorySelected: String?
    var searchResult: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.APP_BG
        title = "Search Workplace"
        addViews()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        txtWPName.resignFirstResponder()
        txtWPLocation.resignFirstResponder()
    }
    func getLocationView() -> SubView
    {
        let view = UIView()
        
        let lblByName = UILabel()
        lblByName.translatesAutoresizingMaskIntoConstraints = false
        lblByName.text = "By Name"
        lblByName.textColor = Colors.GRAY_COLOR
        lblByName.font = normalFontWithSize(15)
        view.addSubview(lblByName)
        
        let imgLoc = UIImageView(image: UIImage(named: "work_loc"))
        txtWPName.translatesAutoresizingMaskIntoConstraints = false
        txtWPName.placeholder = "Enter Name"
        txtWPName.leftView = imgLoc
        txtWPName.textColor = Colors.GRAY_COLOR
        txtWPName.font = boldFontWithSize(15)
        txtWPName.leftViewMode = .always
        txtWPName.delegate = self
        txtWPName.returnKeyType = .done
        view.addSubview(txtWPName)
        
        let lblByLocation = UILabel()
        lblByLocation.translatesAutoresizingMaskIntoConstraints = false
        lblByLocation.text = "By Location"
        lblByLocation.textColor = Colors.GRAY_COLOR
        lblByLocation.font = normalFontWithSize(15)
        view.addSubview(lblByLocation)
        
        let imgSource = UIImageView(image: UIImage(named: "dest"))
        let rightArrow = UIImageView(image: UIImage(named: "rightArrow"))
        txtWPLocation.translatesAutoresizingMaskIntoConstraints = false
        txtWPLocation.placeholder = "Search workplace or nearby location"
        txtWPLocation.font = boldFontWithSize(15)
        txtWPLocation.textColor = Colors.GRAY_COLOR
        txtWPLocation.delegate = self
        txtWPLocation.leftView = imgSource
        txtWPLocation.leftViewMode = .always
        txtWPLocation.rightView = rightArrow
        //txtWPLocation.rightViewMode = .Always
        txtWPLocation.returnKeyType = .done
        view.addSubview(txtWPLocation)
        
        txtWPName.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        txtWPLocation.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        
        let lblByCategory = UILabel()
        lblByCategory.translatesAutoresizingMaskIntoConstraints = false
        lblByCategory.text = "By Category"
        lblByCategory.textColor = Colors.GRAY_COLOR
        lblByCategory.font = normalFontWithSize(15)
        view.addSubview(lblByCategory)
        
        allPlacesRB = getRadioButtonView("All Places")
        allPlacesRB.translatesAutoresizingMaskIntoConstraints = false
        allPlacesRB.addTarget(self, action: #selector(categorySelection(_:)), for: .touchDown)
        view.addSubview(allPlacesRB)
        
        workplaceRB = getRadioButtonView("Workplace")
        workplaceRB.addTarget(self, action: #selector(categorySelection(_:)), for: .touchDown)
        workplaceRB.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(workplaceRB)
        
        housingRB = getRadioButtonView("Housing Complex")
        housingRB.translatesAutoresizingMaskIntoConstraints = false
        housingRB.addTarget(self, action: #selector(categorySelection(_:)), for: .touchDown)
        view.addSubview(housingRB)
        
        eventRB = getRadioButtonView("Events")
        eventRB.addTarget(self, action: #selector(categorySelection(_:)), for: .touchDown)
        eventRB.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(eventRB)
        
        lblErrorMsg.textColor = Colors.GRAY_COLOR
        lblErrorMsg.font = boldFontWithSize(15)
        lblErrorMsg.translatesAutoresizingMaskIntoConstraints = false
        lblErrorMsg.isHidden = true
        lblErrorMsg.numberOfLines = 0
        lblErrorMsg.isUserInteractionEnabled = true
        lblErrorMsg.textAlignment = .center
        let nowText = "Request new Workplace"
        let combinedString = "Oops. no workplace found for your search. Please \(nowText)" as NSString
        let range = combinedString.range(of: nowText)
        let attributedString = NSMutableAttributedString(string: combinedString as String)
        let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1] as [String : Any]
        attributedString.addAttributes(attributes, range: range)
        lblErrorMsg.attributedText = attributedString
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(requestWorkplace))
        lblErrorMsg.addGestureRecognizer(tapGesture)
        view.addSubview(lblErrorMsg)
        
        //TODO://
        if(wpSearchData != nil) {
            txtWPName.text = wpSearchData?.wpName
            txtWPLocation.text = wpSearchData?.wpLocation
            wpLat = wpSearchData?.wpLat
            wpLong = wpSearchData?.wpLong
            if(categorySelected == "yes") {
                allPlacesRB.isSelected = true
            } else {
                if(wpSearchData?.category == "7") {
                    workplaceRB.isSelected = true
                } else if(wpSearchData?.category == "6") {
                    housingRB.isSelected = true
                } else if(wpSearchData?.category == "11") {
                    eventRB.isSelected = true
                } else {
                    allPlacesRB.isSelected = false
                    workplaceRB.isSelected = false
                    housingRB.isSelected = false
                    eventRB.isSelected = false
                }
            }
        }
        
        let viewsDict = ["byName":lblByName, "name":txtWPName, "byLoc":lblByLocation, "location":txtWPLocation, "byCat":lblByCategory, "rb1":allPlacesRB,"rb2":workplaceRB, "rb3":housingRB, "rb4":eventRB, "errmsg":lblErrorMsg]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[byName(30)]-5-[name(30)]-10-[byLoc(30)]-5-[location(30)]-10-[byCat(30)]-10-[rb1]-10-[rb2]-10-[rb3]-10-[rb4]-20-[errmsg]", options: [], metrics: nil, views: viewsDict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[byName]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[name]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[byLoc]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[location]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[byCat]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[rb1(67)]", options: [], metrics: nil, views:viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[rb2(72)]", options: [], metrics: nil, views:viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[rb3(105)]", options: [], metrics: nil, views:viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[rb4(53)]", options: [], metrics: nil, views:viewsDict))
        //self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[btn]-10-|", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[errmsg]-10-|", options: [], metrics: nil, views: viewsDict))
        //addBottomView(getBottomView())
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 345)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        let btnReset = UIButton()
        btnReset.setTitle("RESET", for: UIControlState())
        btnReset.backgroundColor = Colors.GRAY_COLOR
        btnReset.titleLabel?.font = boldFontWithSize(20)
        btnReset.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnReset.translatesAutoresizingMaskIntoConstraints = false
        btnReset.addTarget(self, action: #selector(resetFields), for: .touchDown)
        view.addSubview(btnReset)
        
        let btnSearch = UIButton()
        btnSearch.setTitle("APPLY", for: UIControlState())
        btnSearch.backgroundColor = Colors.GRAY_COLOR
        btnSearch.titleLabel?.font = boldFontWithSize(20)
        btnSearch.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnSearch.translatesAutoresizingMaskIntoConstraints = false
        btnSearch.addTarget(self, action: #selector(searchWPs), for: .touchDown)
        view.addSubview(btnSearch)
        
        //btn.addTopViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPointZero)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btnReset(40)]", options: [], metrics: nil, views: ["btnReset":btnReset]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnReset]-1-[btnSearch(==btnReset)]|", options: [], metrics: nil, views: ["btnReset":btnReset,"btnSearch":btnSearch]))
        view.addConstraint(NSLayoutConstraint(item: btnSearch, attribute: .centerY, relatedBy: .equal, toItem: btnReset, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: btnSearch, attribute: .height, relatedBy: .equal, toItem: btnReset, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }

    func addViews () {
        viewsArray.append(getLocationView())
        addBottomView(getBottomView())
    }
    
    func getRadioButtonView(_ titleValue:String) ->  DLRadioButton{
        let radioButtonInstance = DLRadioButton()
        radioButtonInstance.setTitle(titleValue, for: UIControlState())
        radioButtonInstance.setTitleColor(Colors.GRAY_COLOR, for: UIControlState())
        radioButtonInstance.iconColor = Colors.GREEN_COLOR
        radioButtonInstance.indicatorColor = Colors.GREEN_COLOR
        radioButtonInstance.titleLabel?.font = boldFontWithSize(12)
        radioButtonInstance.titleLabel?.textAlignment = .left
        return radioButtonInstance
    }

    func categorySelection(_ btn: DLRadioButton)
    {
        if(btn == allPlacesRB){
            allPlacesRB.isSelected = true
            workplaceRB.isSelected = false
            housingRB.isSelected = false
            eventRB.isSelected = false
        } else if(btn == workplaceRB){
            allPlacesRB.isSelected = false
            workplaceRB.isSelected = true
            housingRB.isSelected = false
            eventRB.isSelected = false
        } else if(btn == housingRB){
            allPlacesRB.isSelected = false
            workplaceRB.isSelected = false
            housingRB.isSelected = true
            eventRB.isSelected = false
        }else{
            allPlacesRB.isSelected = false
            workplaceRB.isSelected = false
            housingRB.isSelected = false
            eventRB.isSelected = true
        }
    }
    
    func selectedCategory() -> String {
        if  workplaceRB.isSelected == true {
            return "7"
        } else if housingRB.isSelected == true {
            return "6"
        } else if eventRB.isSelected == true {
            return "11"
        } else {
            return ""
        }
    }
    
    func requestWorkplace()
    {
        let vc = AddWPViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.wpName = self.txtWPName.text
        vc.wpLoc = self.txtWPLocation.text
        vc.wpLat = self.wpLat
        vc.wpLong = self.wpLong
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxtField = textField
        if(selectedTxtField == txtWPName) {
            txtWPName.becomeFirstResponder()
            //txtWPLocation.becomeFirstResponder()
        } else if(selectedTxtField == txtWPLocation) {
            txtWPName.resignFirstResponder()
            //txtWPLocation.becomeFirstResponder()
            mapForWPLocation()
            txtWPLocation.resignFirstResponder()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func mapForWPLocation()
    {
        let vc = LocationViewController()
        vc.onSelectionBlock = { (location) -> Void in
            if let loc = location as? [String: String]
            {
                self.txtWPLocation.text = loc["location"]
                self.wpLat = loc["lat"]
                self.wpLong = loc["long"]
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func resetFields () {
        txtWPName.text = ""
        txtWPLocation.text = ""
        wpLat = ""
        wpLong = ""
        allPlacesRB.isSelected = false
        workplaceRB.isSelected = false
        housingRB.isSelected = false
        eventRB.isSelected = false
        _ = self.navigationController?.popViewController(animated: true)
        if(searchResult == true) {
            self.resetBlock?()
        }
    }
    
    func searchWPs()
    {
        var isAllPlacesSelected = ""
        txtWPName.becomeFirstResponder()
        txtWPName.resignFirstResponder()
        lblErrorMsg.isHidden = true
        //if(txtWPName.text?.characters.count > 0){
        
        if(txtWPName.text?.characters.count > 0 || txtWPLocation.text?.characters.count > 0){
            
            let reqObj = WPSearchRequest()
            reqObj.wpName = txtWPName.text
            reqObj.wpLocation = txtWPLocation.text
            reqObj.wpLat = self.wpLat
            reqObj.wpLong = self.wpLong
            reqObj.userId = UserInfo.sharedInstance.userID
            reqObj.category = selectedCategory()
            if(allPlacesRB.isSelected == true) {
                isAllPlacesSelected = "yes"
            } else {
                isAllPlacesSelected = "no"
            }
            showIndicator("Searching Workplace.")
            WorkplaceRequestor().searchWorkplace(reqObj, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    //TODO Changes for private wp
                    if((object as! WPSearchResponse).code == "1400" && (((object as! WPSearchResponse).data?.first?.wpMembershipStatus == "1") || ((object as! WPSearchResponse).data?.first?.wpScope != "1" && (object as! WPSearchResponse).data?.first?.wpTieUpStatus != "1"))) {
                        var message = ""
                        if((object as! WPSearchResponse).data?.first?.wpMembershipStatus == "1") {
                            message = "You already joined in workplace. Please share your travel plan."
                        } else {
                            message = "Workplace is already registered with us. Please join and share your travel plan."
                        }
                        AlertController.showAlertFor("Workplace", message: message, okAction: {
                            showIndicator("Fetching Rides.")
                            WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: ((object as! WPSearchResponse).data?.first?.wpID)!, success: { (success, objectRides) in
                                hideIndicator()
                                
                                let vc = WPRidesViewController()
                                vc.hidesBottomBarWhenPushed = true
                                vc.data = (object as! WPSearchResponse).data?.first
                                vc.dataSourceToOfc = (objectRides as! WPRidesResponse).pools?.officeGoing
                                vc.dataSourceToHome = (objectRides as! WPRidesResponse).pools?.homeGoing
                                self.navigationController?.pushViewController(vc, animated: true)
                            }) { (error) in
                                hideIndicator()
                            }
                        })
                    } else {
                        _ = self.navigationController?.popViewController(animated: true)
                        self.completionBlock?((object as! WPSearchResponse).data, reqObj, (object as! WPSearchResponse).code, isAllPlacesSelected)
                    }
                }
                else{
                    if((object as! WPSearchResponse).code == "1403") {
                        self.lblErrorMsg.isHidden = false
                    } else {
                        AlertController.showToastForError((object as! WPSearchResponse).message!)
                    }
                }
            }) { (error) in
                hideIndicator()
            }
        }
        else{
            AlertController.showToastForError("Please provide either workplace Name or Location.")
        }
    }
}
