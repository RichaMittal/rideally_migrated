//
//  WorkplaceViewController.swift
//  rideally
//
//  Created by Sarav on 31/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import Crashlytics
import Alamofire
import GoogleMaps
import FirebaseAnalytics
import AddressBook
import AddressBookUI
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class WorkplaceViewController: BaseScrollViewController, UITableViewDataSource, UITableViewDelegate, ABPeoplePickerNavigationControllerDelegate {
    let tableView = UITableView()
    let responseTableView = UITableView()
    var tableHeightConstraint: NSLayoutConstraint?
    var tableTopConstraint: NSLayoutConstraint?
    let rowHt: CGFloat = 80
    var domainsList = [WPDomainsResponseData]()
    let segment = HMSegmentedControl()
    var isMyPlacesVisible = false
    var isAllPlacesVisible = false
    var isOnStart = true
    var isSearchClicked = false
    var noData = false
    var currentSelectedSegmentIndex: UInt = 0
    var iconFilter = UIButton()
    var categorySelected: String?
    var wpReqObj: AddWPRequest?
    var wpSearchData: WPSearchRequest?
    var wpresponses: [InviteWPResult]?
    var localHideAnywhere = "0"
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    var localWpId = ""
    var localWpKey = ""
    var localWpName = ""
    
    lazy var titleView: SubView = self.getTitleView()
    var dataSource = [WorkPlace](){
        didSet{
            if(dataSource.count > 0){
                tableView.isHidden = false
                view.removeEmptyView()
                tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt)
                selectedIndexPath = nil
                tableView.reloadData()
            }
            else{
                tableHeightConstraint?.constant = 0
                showEmptyView("You have not joined any Corporate groups to start sharing, you can join your company group by searching in \"All\" tab.")
                //viewsArray.append(errorView())
                //errorView()
            }
        }
    }
    
    //    func errorView (){
    //        let view = UIView()
    //        view.translatesAutoresizingMaskIntoConstraints = false
    //
    //        let lblErrorMsg = UILabel()
    //        lblErrorMsg.textColor = Colors.GRAY_COLOR
    //        lblErrorMsg.font = boldFontWithSize(15)
    //        lblErrorMsg.text = "Oops. no rides found for you search. Please Create new ride."
    //        lblErrorMsg.translatesAutoresizingMaskIntoConstraints = false
    //        //lblErrorMsg.hidden = true
    //        lblErrorMsg.numberOfLines = 0
    //        lblErrorMsg.textAlignment = .Center
    //        view.addSubview(lblErrorMsg)
    //
    //        let btnSearch = UIButton()
    //        btnSearch.setTitle("SEARCH", forState: .Normal)
    //        btnSearch.backgroundColor = Colors.GREEN_COLOR
    //        btnSearch.titleLabel?.font = boldFontWithSize(20)
    //        btnSearch.setTitleColor(Colors.WHITE_COLOR, forState: .Normal)
    //        btnSearch.translatesAutoresizingMaskIntoConstraints = false
    //        //btnSearch.addTarget(self, action: #selector(searchRides), forControlEvents: .TouchDown)
    //        view.addSubview(btnSearch)
    //
    //        let viewsDict = ["errmsg":lblErrorMsg, "btn":btnSearch]
    //
    //        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[errmsg]-5-[btn]|", options: [], metrics: nil, views: viewsDict))
    //        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[errmsg]-5-|", options: [], metrics: nil, views: viewsDict))
    //        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[btn]-5-|", options: [], metrics: nil, views: viewsDict))
    //
    //
    ////        view.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .CenterY, relatedBy: .Equal, toItem: lblSource, attribute: .CenterY, multiplier: 1, constant: 0))
    ////        view.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .CenterY, relatedBy: .Equal, toItem: lblDest, attribute: .CenterY, multiplier: 1, constant: 0))
    //
    ////
    ////        let sub = SubView()
    ////        sub.view = view
    ////        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
    ////        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 195)
    ////        return sub
    //    }
    
    override func hasTabBarItems() -> Bool
    {
        ISWPRIDESSCREEN = false
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.tabBarController?.tabBar.isTranslucent = false
        //self.navigationItem.title = "Workplace"
        //addViews()
        getProfileDetails("onStart")
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_menu"), style: .plain, target: self, action: #selector(showMenu))
        NotificationCenter.default.addObserver(self, selector: #selector(updateList), name: NSNotification.Name(rawValue: "updateWpsList"), object: nil)
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AfterCancel(_:)), name: "AfterCancel", object: nil)
         NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "KInviteSendNotification"),object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotification"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Workplace"
        removeTopView()
        addTopView(titleView)
    }
    
//    override func viewWillDisappear(animated: Bool) {
//        super.viewWillDisappear(animated)
//        NSNotificationCenter.defaultCenter().removeObserver(self, name:"KInviteSendNotification",object: nil )
//    }
    
    func addViews()
    {
        //viewsArray.append(titleView)
        
        viewsArray.append(getTableView())
        if(UserInfo.sharedInstance.myPendingWorkplaces != "" && UserInfo.sharedInstance.myWorkplaces != "") {
            if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                getMyWP("onStart")
            } else {
                redirectToAllPlaces("onRedirect")
            }
        } else {
            redirectToAllPlaces("onRedirect")
        }
    }
    
    func showEmptyView(_ msg: String)
    {
        tableView.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "ic_empty_cart", actionHandler: nil)
    }
    
    let lblTitle = UILabel()
    
    func getTitleView() -> SubView
    {
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        lblTitle.text = ""
        lblTitle.textColor = Colors.GRAY_COLOR
        lblTitle.font = normalFontWithSize(15)
        lblTitle.numberOfLines = 0
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblTitle)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onClickTitleLbl))
        lblTitle.addGestureRecognizer(tapGesture)
        
        iconFilter.setImage(UIImage(named: "filter_gray_new"), for: UIControlState())
        iconFilter.translatesAutoresizingMaskIntoConstraints = false
        iconFilter.isUserInteractionEnabled = true
        iconFilter.addTarget(self, action: #selector(btnFilter_clicked), for: .touchDown)
        view.addSubview(iconFilter)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[title]|", options: [], metrics: nil, views: ["title":lblTitle]))
        //view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[title]-5-|", options: [], metrics: nil, views: ["title":lblTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[title]-5-[ifilter(60)]-5-|", options: [], metrics: nil, views: ["title":lblTitle, "ifilter":iconFilter]))
        view.addConstraint(NSLayoutConstraint(item: iconFilter, attribute: .centerY, relatedBy: .equal, toItem: lblTitle, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: iconFilter, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 60))
        
        let subview = SubView()
        subview.view = view
        subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        subview.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 35)
        return subview
    }
    
    func onClickTitleLbl()
    {
        if let reqObj = wpReqObj{
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemID:"Add Workplace Page",
                AnalyticsParameterItemName: "After Search Event",
                AnalyticsParameterContentType:"Add Workplace Button"
                ])
            showIndicator("Requesting for Workplace..")
            WorkplaceRequestor().addWorkPlace(reqObj, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Request Workplace", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.updateWPsList("Other")
                    })
                }
                else{
                    AlertController.showAlertFor("Request Workplace", message: object as? String)
                }
            }) { (error) in
                hideIndicator()
            }
        } else if wpSearchData != nil {
            let vc = AddWPViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.wpName = self.wpSearchData?.wpName
            vc.wpLoc = self.wpSearchData?.wpLocation
            vc.wpLat = self.wpSearchData?.wpLat
            vc.wpLong = self.wpSearchData?.wpLong
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getTableView() -> SubView
    {
        let view = UIView()
        
        //tableView.separatorColor = UIColor.clearColor()
        tableView.separatorColor = Colors.GRAY_COLOR
        tableView.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(WorkplaceCell.self, forCellReuseIdentifier: "wpcell")
        tableView.register(ExpandedWPCell.self, forCellReuseIdentifier: "ewpcell")
        tableView.register(AllWPCell.self, forCellReuseIdentifier: "allwpcell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[table]-5-|", options: [], metrics: nil, views: ["table":tableView]))
        
        tableHeightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        let subview = SubView()
        subview.view = view
        subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        subview.heightConstraint = tableHeightConstraint!
        return subview
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int?
        if(tableView == self.tableView) {
            count = dataSource.count
        }
        if(tableView == self.responseTableView) {
            count = wpresponses!.count
        }
        return count!
    }
    
    var selectedIndexPath: IndexPath?
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == self.responseTableView) {
            return UITableViewAutomaticDimension
        } else {
            if((selectedIndexPath != nil) && (selectedIndexPath == indexPath))
            {
                return (rowHt + 40)
            }
            else{
                return rowHt
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == self.responseTableView) {
            let cell = UITableViewCell()
            cell.textLabel?.font = normalFontWithSize(14)
            cell.textLabel?.numberOfLines = 0
            if (wpresponses![indexPath.row].type == "mobile"){
                cell.textLabel?.text = "\(wpresponses![indexPath.row].mobile!) - \(wpresponses![indexPath.row].result!)"
            }else{
                cell.textLabel?.text = "\(wpresponses![indexPath.row].email!) - \(wpresponses![indexPath.row].result!)"
            }
            return cell
        } else {
            if((selectedIndexPath != nil) && (selectedIndexPath == indexPath))
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ewpcell") as! ExpandedWPCell
                cell.data = dataSource[indexPath.row]
                cell.onRideCreationDetail = { (isStartLocationWP) -> Void in
                    self.getWpRides(self.dataSource[indexPath.row].wpID!, wpData: self.dataSource[indexPath.row], triggerFrom: "LIST")
                }
                cell.callUpdateVehicle = { (wpInsuranceStatus) -> Void in
                    let vc = RegisterNewVehicleController()
                    vc.isUpdateReqneedtobeSend = "1"
                    vc.isSeatCapacityNeedToBeUpdated = "1"
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    if(wpInsuranceStatus != "" && wpInsuranceStatus != nil && wpInsuranceStatus == "1") {
                        vc.isWpInsuranceStatus = true
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                //            cell.onRideCreationDetail = { (isStartLocationWP) -> Void in
                //                var lastRideType = ""
                //                WorkplaceRequestor().getLastRide({ (success, object) in
                //                    let lastRideObj = (object as! LastRideResponse).data?.first
                //                    if(success == true){
                //                        lastRideType = (lastRideObj?.rideType)!
                //                    }
                //                    else {
                //                        lastRideType = ""
                //                    }
                //                    let vc = WPCreateRideViewController()
                //                    vc.isStartLocWP = isStartLocationWP as! Bool
                //                    vc.wpData = cell.data
                //                    vc.lastRideType = lastRideType
                //                    vc.hidesBottomBarWhenPushed = true
                //                    self.navigationController?.pushViewController(vc, animated: true)
                //                    }, failure: { (error) in
                //                        lastRideType = ""
                //                })
                //            }
                
                cell.onCreateRide = { (searchResults, newRideReqObj, rideDetailData) -> Void in
                    if(rideDetailData == nil){
                        let vc = ExistingRidesViewController()
                        vc.dataSource = searchResults as? [WPRide]
                        vc.newRideReqObj = (newRideReqObj as? WPNewRideRequest)!
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                        let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                        //let vc = RideDetailViewController()
                        vc.hidesBottomBarWhenPushed = true
                        vc.edgesForExtendedLayout = UIRectEdge()
                        vc.data = rideDetailData as? RideDetail
                        vc.wpData = cell.data
                        vc.triggerFrom = "place"
                        vc.inviteFlag = "wpMap"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
                cell.onUserActionBlock = { (selectedAction) -> Void in
                    self.performActionFor("ADDRIDE", indexPath: indexPath)
                }
                
                return cell
            }
            else{
                var cell: WorkplaceCell?
                if(isMyPlacesVisible == true){
                    cell = tableView.dequeueReusableCell(withIdentifier: "wpcell") as? WorkplaceCell
                }
                else{
                    cell = tableView.dequeueReusableCell(withIdentifier: "allwpcell") as! AllWPCell
                }
                cell!.data = dataSource[indexPath.row]
                cell!.onUserActionBlock = { (selectedAction) -> Void in
                    self.performActionFor(selectedAction as! String, indexPath: indexPath)
                }
                return cell!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.WHITE_COLOR
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        ISSEARCHRESULT = false
        if(SEARCHREQUEST != nil) {
            SEARCHREQUEST = nil
        }
        let selectedWp = dataSource[indexPath.row]
        
        if(selectedWp.wpTieUpStatus == "1"){ // && selectedWp.wpMembershipStatus != "1"
            checkDomainMatch(selectedWp, triggerFrom: "DIDSELECT")
        }  else if(selectedWp.wpScope == "1" && selectedWp.wpTieUpStatus == "0" && selectedWp.wpMembershipStatus != "1"){
            showWpDetail(selectedWp.wpID!)
        } else {
            getWpRides(selectedWp.wpID!, wpData: selectedWp, triggerFrom: "DIDSELECT")
        }
    }
    
    func showWpDetail (_ wpID: String) {
        WorkplaceRequestor().getWPMembers("", groupID: wpID, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = WPDetailViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.data = (object as! WPMembersResponseData).groupDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }) { (error) in
            hideIndicator()
        }
    }
    
    func isValidEmailFormate(_ stringValue:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: stringValue)
    }
    
    func getSecEmail(_ selectedWPdata: WorkPlace, flag: Bool)
    {
        var okButtonTitle = ""
        let viewHolder = AlertContentViewHolder()
        viewHolder.heightConstraintValue = 95
        
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        self.txtEmailID.translatesAutoresizingMaskIntoConstraints = false
        self.txtEmailID.placeholder = "ENTER COMPANY EMAIL ID"
        self.txtEmailID.font = normalFontWithSize(14)
        self.txtEmailID.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        view.addSubview(self.txtEmailID)
        
        let  termsOfuse =  UILabel()
        termsOfuse.backgroundColor = UIColor.clear
        termsOfuse.textColor = Colors.BLACK_COLOR
        termsOfuse.textAlignment = NSTextAlignment.center
        termsOfuse.isUserInteractionEnabled = true
        let myAttribute = [NSFontAttributeName: normalFontWithSize(12)!]
        let myString = NSMutableAttributedString(string: "By clicking SUBMIT, you agree to our Terms of Use", attributes: myAttribute)
        let myRange = NSRange(location: 36, length: 13)
        myString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange)
        termsOfuse.attributedText = myString
        let tapGesture = UITapGestureRecognizer(target:self, action:#selector(termsOfUsePressed(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        termsOfuse.addGestureRecognizer(tapGesture)
        termsOfuse.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(termsOfuse)

        let lblInfo = UILabel()
        if(flag == true) {
            if(UserInfo.sharedInstance.secEmail != "") {
                self.txtEmailID.text = UserInfo.sharedInstance.secEmail
            } else {
                self.txtEmailID.text = UserInfo.sharedInstance.email
            }
            lblInfo.text = "We are waiting for your email verification. If you didnt receive email yet, you can click on resend verification link below."
            okButtonTitle = "Resend Link"
            //termsOfuse.hidden = true
        } else {
            lblInfo.text = "You may need to provide official email ID to access this workplace. Please provide email id to proceed further."
            okButtonTitle = "Submit"
            //termsOfuse.hidden = false
        }
        lblInfo.numberOfLines = 0
        lblInfo.lineBreakMode = .byWordWrapping
        lblInfo.translatesAutoresizingMaskIntoConstraints = false
        lblInfo.font = normalFontWithSize(12)
        view.addSubview(lblInfo)
        
//        if(flag == true) {
//        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-10-[lbl]-10-[txt(30)]-10-|", options: [], metrics: nil, views: ["txt":self.txtEmailID, "lbl":lblInfo, "termsOfUse":termsOfuse]))
//        
//        } else {
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[lbl]-10-[txt(30)]-10-[termsOfUse(30)]|", options: [], metrics: nil, views: ["txt":self.txtEmailID, "lbl":lblInfo, "termsOfUse":termsOfuse]))
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[termsOfUse]-10-|", options: [], metrics: nil, views: ["termsOfUse":termsOfuse]))
        //}
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[txt]-10-|", options: [], metrics: nil, views: ["txt":self.txtEmailID]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblInfo]))
        
        
        viewHolder.view = view
        
        AlertController.showAlertFor("Email verification", message: "", contentView: viewHolder, okButtonTitle: okButtonTitle, willHaveAutoDismiss: false, okAction: {
            if(self.txtEmailID.text?.characters.count == 0) {
                AlertController.showToastForError("Please provide 'Company Email Id'.")
            } else if(self.txtEmailID.text?.characters.count != 0 && !self.isValidEmailFormate(self.txtEmailID.text!))
            {
                AlertController.showToastForError("Please enter valid 'Company Email Id'.")
            } else {
                let myDomain = self.txtEmailID.text!.components(separatedBy: "@").last
                
                var domainMatch = false
                for domain in self.domainsList{
                    if(domain.domainName == myDomain){
                        domainMatch = true
                    }
                }
                if(domainMatch == true){
                    if(self.txtEmailID.text == UserInfo.sharedInstance.email) {
                        self.dismiss(animated: true, completion: {
                            showIndicator("Sending verification link...")
                            WorkplaceRequestor().addSecEmail(UserInfo.sharedInstance.userID, email: self.txtEmailID.text!, groupKey:selectedWPdata.groupKey!, groupID: selectedWPdata.wpID!, success: { (success, object) in
                                hideIndicator()
                                if(success == true) {
                                    UserInfo.sharedInstance.secEmail = self.txtEmailID.text!
                                    if(UserInfo.sharedInstance.hideAnywhere != "") {
                                        self.localHideAnywhere = UserInfo.sharedInstance.hideAnywhere
                                    }
                                    if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
                                        UserInfo.sharedInstance.hideAnywhere = "1"
                                    }
                                    AlertController.showAlertFor("Email verification", message: "We have sent you a verification link to your email id (\(self.txtEmailID.text!)). Please verify it to proceed further.", okAction: {
                                        self.txtEmailID.text = ""
                                        //if(flag == false) {
                                        self.isMyPlacesVisible = true
                                        //}
                                        self.updateWPsList("Join")
                                    })
                                } else {
                                    AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: {
                                        self.txtEmailID.text = ""
                                    })
                                }
                                }, failure: { (error) in
                                    hideIndicator()
                            })
                        })
                    } else {
                        let reqObj = emailIDRequestModel()
                        reqObj.user_email = self.txtEmailID.text
                        reqObj.user_id = UserInfo.sharedInstance.userID
                        showIndicator("Loading...")
                        SignUpRequestor().isEmailIdalreadyexist(reqObj, success:{ (success, object) in
                            
                            hideIndicator()
                            
                            if (object as! emailIDResponseModel).code == "1454"
                            {
                                self.dismiss(animated: true, completion: {
                                    showIndicator("Sending verification link...")
                                    WorkplaceRequestor().addSecEmail(UserInfo.sharedInstance.userID, email: self.txtEmailID.text!, groupKey:selectedWPdata.groupKey!, groupID: selectedWPdata.wpID!, success: { (success, object) in
                                        hideIndicator()
                                        if(success == true) {
                                            UserInfo.sharedInstance.secEmail = self.txtEmailID.text!
                                            if(UserInfo.sharedInstance.hideAnywhere != "") {
                                                self.localHideAnywhere = UserInfo.sharedInstance.hideAnywhere
                                            }
                                            if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
                                                UserInfo.sharedInstance.hideAnywhere = "1"
                                            }
                                            AlertController.showAlertFor("Email verification", message: "We have sent you a verification link to your email id (\(self.txtEmailID.text!)). Please verify it to proceed further.", okAction: {
                                                self.txtEmailID.text = ""
                                                //if(flag == false) {
                                                self.isMyPlacesVisible = true
                                                //}
                                                self.updateWPsList("Join")
                                            })
                                        } else {
                                            AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: {
                                                self.txtEmailID.text = ""
                                            })
                                        }
                                        }, failure: { (error) in
                                            hideIndicator()
                                    })
                                })
                            }
                            else
                            {
                                AlertController.showToastForError("This 'Email Id' already exists.")
                            }
                            
                        }){ (error) in
                            hideIndicator()
                            AlertController.showAlertForError(error)
                        }
                    }
                } else {
                    self.dismiss(animated: true, completion: {
                        AlertController.showAlertFor("Error", message: "Sorry, we could not authorize your \(selectedWPdata.wpName!) email id. Please check your email id (\(self.txtEmailID.text!)) again or register again with correct email id or contact customer care at 080 4600 4600.", okButtonTitle: "Ok", okAction: {
                            self.txtEmailID.text = ""
                        })
                    })
                }
            }
            }, cancelButtonTitle: "Cancel", cancelAction: {
                self.dismiss(animated: true, completion: nil)
                self.txtEmailID.text = ""
        })
    }
    
    func termsOfUsePressed(_ sender:AnyObject)  {
        self.dismiss(animated: true, completion: nil)
        let vc = StaticPagesViewController()
        if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
            vc.touType = "specific"
        } else {
            vc.touType = "generic"
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func sendJoinWPrequest(_ selectedWPdata: WorkPlace)
    {
        let tieup_wpuser: String?
        if(selectedWPdata.wpTieUpStatus == "1") {
            tieup_wpuser = "1"
        } else {
            tieup_wpuser = ""
        }
        showIndicator("Sending request to Join Workplace.")
        WorkplaceRequestor().joinWorkPlace(selectedWPdata.wpID!, groupScope: selectedWPdata.wpScope!, userMsg: "",tieupWpUser: tieup_wpuser!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                if(UserInfo.sharedInstance.hideAnywhere != "") {
                    self.localHideAnywhere = UserInfo.sharedInstance.hideAnywhere
                }
                if(UserInfo.sharedInstance.touHideAnywhere != "" && UserInfo.sharedInstance.touHideAnywhere == "1") {
                    UserInfo.sharedInstance.hideAnywhere = "1"
                }
                AlertController.showAlertFor("Join Workplace", message: object as? String, okButtonTitle: "Ok", okAction: {
                    if(selectedWPdata.wpScope == "0" || selectedWPdata.wpTieUpStatus == "1") {
                        self.getWpRides(selectedWPdata.wpID!, wpData: selectedWPdata, triggerFrom: "JOIN")
                    } else {
                        self.isMyPlacesVisible = true
                        self.updateWPsList("Join")
                    }
                })
            }
            else{
                AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: nil)
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        //AlertController.showToastForInfo(String(authorizationStatus.rawValue))
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            contactListArray.removeAllObjects()
            mobileMutArr.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject]{
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = localWpId
            vc.isWp = true
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    self.contactListArray.removeAllObjects()
                    self.mobileMutArr.removeAllObjects()
                    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                    for record:ABRecord in contactList as [AnyObject]{
                        let contactPerson: ABRecord = record
                        let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                        if(contactName != "") {
                            var personNumber = ""
                            let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                            if(phones != "")
                            {
                                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                {
                                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                    {
                                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                        personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                        let areaCode = personNumber.characters.count-10
                                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                        if(areaCode > 0) {
                                            personNumber = personNumber.substring(from: startIndex)
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            self.mobileMutArr.add(personNumber)
                            let nameString = "\(contactName) \(personNumber)"
                            self.contactListArray.add(nameString)
                        }
                    }
                    
                    let vc = ContactsListViewController()
                    vc.contactListArr = self.contactListArray
                    vc.mobileMutArr = self.mobileMutArr
                    vc.poolID = self.localWpId
                    vc.isWp = true
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }

    func displayCantAddContactAlert() {
        //        AlertController.showAlertFor("Cannot Add Contact", message: "You must give the app permission to add the contact first.", okButtonTitle: "Settings", okAction: {
        //            self.openSettings()
        //            }, cancelButtonTitle: "OK", cancelAction: nil)
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }

    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObject()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg(localWpName, wpKey: localWpKey, poolID: localWpId)
    }
    
    let txtEmailID = RATextField()
    func performActionFor(_ userSelection: String, indexPath: IndexPath)
    {
        let selectedWPdata = dataSource[indexPath.row]
        
        if(userSelection == "ADDRIDE"){
            //if(isMyWPListVisible == true){
            if(selectedIndexPath != indexPath)
            {
                tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt) + 40
                selectedIndexPath = indexPath
            }
            else
            {
                tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt)
                selectedIndexPath = nil
            }
            tableView.reloadData()
            //}
        }
        else if(userSelection == "INVITE"){
            localWpName = selectedWPdata.wpName!
            localWpId = selectedWPdata.wpID!
            localWpKey = selectedWPdata.groupKey!
            
            let inviteView = InviteNormalRideView()
            let alertViewHolder =   AlertContentViewHolder()
            alertViewHolder.heightConstraintValue = 95
            alertViewHolder.view = inviteView
            
            inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
            inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
            
            AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
        }
        else if(userSelection == "JOIN"){
            checkDomainMatch(selectedWPdata, triggerFrom: "JOIN")
        }
        else if(userSelection == "CANCEL"){
            showIndicator("Cancelling request.")
            WorkplaceRequestor().cancelJoinWorkPlace(selectedWPdata.wpID!, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Cancel Request", message: "You have cancelled the request successfully.", okButtonTitle: "Ok", okAction: {
                        self.updateWPsList("Other")
                    })
                }else{
                    AlertController.showAlertFor("Error", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }
        else if(userSelection == "MEMBERS"){
            if((selectedWPdata.wpScope == "1" || selectedWPdata.wpTieUpStatus == "1") && (selectedWPdata.wpMembershipStatus != "1")){
                AlertController.showAlertFor("Workplace Members", message: "You may need to join the workplace to see members list.", okButtonTitle: "Ok", okAction: nil)
            }
            else{
                showIndicator("Fetching members.")
                WorkplaceRequestor().getWPMembers("", groupID: selectedWPdata.wpID!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
                    hideIndicator()
                    let vc = WPMembersViewController()
                    vc.hidesBottomBarWhenPushed = true
                    vc.wpDetails = (object as! WPMembersResponseData).groupDetails
                    vc.membersList = (object as! WPMembersResponseData).members
                    self.navigationController?.pushViewController(vc, animated: true)
                    }, failure: { (error) in
                        hideIndicator()
                })
            }
        }
        else if(userSelection == "RIDES"){
            if((selectedWPdata.wpScope == "1" || selectedWPdata.wpTieUpStatus == "1") && (selectedWPdata.wpMembershipStatus != "1")){
                AlertController.showAlertFor("Workplace Members", message: "You may need to join the workplace to see rides list.", okButtonTitle: "Ok", okAction: nil)
            }
            else{
                getWpRides(selectedWPdata.wpID!, wpData: selectedWPdata, triggerFrom: "RIDES")
            }
        }
    }
    
    func checkDomainMatch(_ selectedWPdata: WorkPlace, triggerFrom: String) {
        UserInfo.sharedInstance.touHideAnywhere = selectedWPdata.wpHideAnywhere!
        //if(selectedWPdata.wpScope == "1"){
            if(selectedWPdata.wpTieUpStatus == "1"){
                if(UserInfo.sharedInstance.email != ""){
                    let reqObj = ProfileDetailsRequest()
                    reqObj.userID = UserInfo.sharedInstance.userID
                    ProfileRequestor().profileDetailRequest(reqObj, success:{ (success, object) in
                        if (object as! ProfileDetailsResponse).code == "415"
                        {
                            if((object as! ProfileDetailsResponse).dataObj?.first?.user_state == "3" || (object as! ProfileDetailsResponse).dataObj?.first?.user_state == "5") {
                                UserInfo.sharedInstance.isEmailIDVerified = true
                            } else {
                                UserInfo.sharedInstance.isEmailIDVerified = false
                            }
                            if((object as! ProfileDetailsResponse).dataObj?.first?.secondary_email_status == "2") {
                                UserInfo.sharedInstance.isSecEmailIDVerified = true
                            } else {
                                UserInfo.sharedInstance.isSecEmailIDVerified = false
                            }
                            if((object as! ProfileDetailsResponse).dataObj?.first?.secondary_email != nil) {
                                UserInfo.sharedInstance.secEmail = ((object as! ProfileDetailsResponse).dataObj?.first?.secondary_email)!
                            }
                            if((object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere != nil && (object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere != "") {
                                UserInfo.sharedInstance.hideAnywhere = ((object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere)!
                            }
                            if((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myWorkplace != nil && (object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myWorkplace != "") {
                                UserInfo.sharedInstance.myWorkplaces = ((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myWorkplace)!
                            }
                            if((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myPendingWorkplace != nil && (object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myPendingWorkplace != "") {
                                UserInfo.sharedInstance.myPendingWorkplaces = ((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myPendingWorkplace)!
                            }
                        }
                    

                    WorkplaceRequestor().getWPDomains(selectedWPdata.wpID!, success: { (success, object) in
                        if(success){
                            self.domainsList = object as! [WPDomainsResponseData]
                            let mySecDomain = UserInfo.sharedInstance.secEmail.components(separatedBy: "@").last
                            let myPrimDomain = UserInfo.sharedInstance.email.components(separatedBy: "@").last
                            var domainMatch = false
                            for domain in self.domainsList{
                                if(domain.domainName == myPrimDomain || domain.domainName == mySecDomain){
                                    domainMatch = true
                                }
                            }
                            if(domainMatch == true){
                                //check if email is verified else show popup
                                if(UserInfo.sharedInstance.isEmailIDVerified == true || UserInfo.sharedInstance.isSecEmailIDVerified == true){
                                    if(triggerFrom == "DIDSELECT") {
                                    //if(selectedWPdata.wpMembershipStatus == "1") {
                                        self.getWpRides(selectedWPdata.wpID!, wpData: selectedWPdata, triggerFrom: "DIDSELECT")
                                    } else {
                                        self.sendJoinWPrequest(selectedWPdata)
                                    }
                                }
                                else{
                                    self.getSecEmail(selectedWPdata,flag: true)
                                }
                                
                            } else{
                                self.getSecEmail(selectedWPdata,flag: false)
                            }
                        }
                        }, failure: { (error) in
                            
                    })
                    }){ (error) in
                        AlertController.showAlertForError(error)
                    }
                }
                else{
                    self.getSecEmail(selectedWPdata,flag: false)
                }
            } else {
                self.sendJoinWPrequest(selectedWPdata)
            }
//        }
//        else if(selectedWPdata.wpScope == "0"){
//            self.sendJoinWPrequest(selectedWPdata)
//        }
    }
    
    func getWpRides(_ wpId: String, wpData: WorkPlace, triggerFrom: String) {
        showIndicator("Fetching Rides.")
        WorkplaceRequestor().getWPRides(prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy"), reqTime: prettyDateStringFromDate(Date(), toFormat: "hh:mm a"), groupID: wpId, success: { (success, object) in
            WorkplaceRequestor().getWPMembers("", groupID: wpId, distanceRange: "1", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, objectMembers) in
                hideIndicator()
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : RideMapViewController = storyboard.instantiateViewController(withIdentifier: "RideMapViewController") as! RideMapViewController
                vc.hidesBottomBarWhenPushed = true
                vc.data = wpData
                vc.dataSourceToOfc = (object as! WPRidesResponse).pools?.officeGoing
                vc.dataSourceToHome = (object as! WPRidesResponse).pools?.homeGoing
                vc.wpMembers = (objectMembers as! WPMembersResponseData).members
                if(triggerFrom == "JOIN") {
                    vc.onSelectBackBlock = {
                        self.updateWPsList("Join")
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }) { (error) in
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func setUpBtn(_ btn: UIButton, title: String)
    {
        btn.contentVerticalAlignment = .top
        btn.setImage(UIImage(named: title), for: UIControlState())
        btn.translatesAutoresizingMaskIntoConstraints = false
    }
    
    let btnWPs = UIButton(type: .custom)
    let lblWPs = UILabel()
    let lblJoined = UILabel()
    let btnJoined = UIButton(type: .custom)
    func getButtonSubView () -> SubView {
        
        let view1 = UIView()
        view1.backgroundColor = UIColor(red: 245.0/255.0, green: 245.0/255.0, blue: 245.0/255.0, alpha: 1)
        view1.addTopViewBorder(2, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        
        let btnAddWP = UIButton(type:.custom)
        setUpBtn(btnAddWP, title:"addWP")
        btnAddWP.addTarget(self, action: #selector(btnAddWPClicked), for: .touchDown)
        view1.addSubview(btnAddWP)
        
        let lblAddWP = UILabel()
        lblAddWP.text = "REQUEST"
        lblAddWP.textAlignment = .center
        lblAddWP.font = normalFontWithSize(11)
        lblAddWP.translatesAutoresizingMaskIntoConstraints = false
        btnAddWP.addSubview(lblAddWP)
        
        btnAddWP.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblAddWP]))
        btnAddWP.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lbl(10)]-2-|", options: [], metrics: nil, views: ["lbl":lblAddWP]))
        
        setUpBtn(btnJoined, title:"wp_joined_green")
        btnJoined.addTarget(self, action: #selector(btnJoinedClicked), for: .touchDown)
        view1.addSubview(btnJoined)
        
        lblJoined.text = "JOINED"
        lblJoined.textAlignment = .center
        lblJoined.font = normalFontWithSize(11)
        lblJoined.translatesAutoresizingMaskIntoConstraints = false
        //view1.addSubview(lblSearch)
        btnJoined.addSubview(lblJoined)
        
        btnJoined.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblJoined]))
        btnJoined.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lbl(10)]-2-|", options: [], metrics: nil, views: ["lbl":lblJoined]))
        setUpBtn(btnWPs, title:"Explore")
        lblWPs.text = "ALL"
        btnWPs.addTarget(self, action: #selector(btnAllClicked(_:)), for: .touchDown)
        view1.addSubview(btnWPs)
        lblWPs.textAlignment = .center
        lblWPs.font = normalFontWithSize(11)
        lblWPs.translatesAutoresizingMaskIntoConstraints = false
        btnWPs.addSubview(lblWPs)
        
        btnWPs.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["lbl":lblWPs]))
        btnWPs.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lbl(10)]-2-|", options: [], metrics: nil, views: ["lbl":lblWPs]))
        
        
        lblJoined.textColor = Colors.GREEN_COLOR
        lblAddWP.textColor = Colors.GRAY_COLOR
        lblWPs.textColor = Colors.GRAY_COLOR
        
        let viewsDict = ["add":btnAddWP, "lbladd":lblAddWP, "search":btnJoined, "lblsearch":lblJoined, "rides":btnWPs, "lblWPs":lblWPs]
        
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[add][search(==add)][rides(==add)]|", options: [], metrics: nil, views: viewsDict))
        //        view1.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[lbladd][lblsearch(==lbladd)][lblWPs(==lbladd)]|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-2-[add]|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-2-[search]|", options: [], metrics: nil, views: viewsDict))
        view1.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-2-[rides]|", options: [], metrics: nil, views: viewsDict))
        
        let sub = SubView()
        sub.view = view1
        sub.padding = UIEdgeInsetsMake(10, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 45)
        
        return sub
    }
    
    func btnAddWPClicked()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Workplace Tab",
            AnalyticsParameterItemName: "Footer",
            AnalyticsParameterContentType:"Add Workplace Button"
            ])
        //getProfileDetails()
        let vc = AddWPViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.onExistingWPsBlock = { (wps, reqObj) -> Void in
            self.wpReqObj = reqObj as? AddWPRequest
            self.dataSource = wps as! [WorkPlace]
            
            self.lblTitle.isUserInteractionEnabled = true
            self.titleView.heightConstraint?.constant = 50
            let nowText = "Request new Place"
            let combinedString = "You may join below mentioned existing workplaces or \(nowText)" as NSString
            let range = combinedString.range(of: nowText)
            let attributedString = NSMutableAttributedString(string: combinedString as String)
            let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1] as [String : Any]
            attributedString.addAttributes(attributes, range: range)
            self.lblTitle.attributedText = attributedString
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    func btnSearchClicked()
    //    {
    //        FIRAnalytics.logEventWithName(kFIREventSelectContent, parameters: [
    //            kFIRParameterItemID:"Workplace Tab",
    //            kFIRParameterItemName: "Search Workplace",
    //            kFIRParameterContentType:"Search Button"
    //            ])
    //        getProfileDetails()
    //        let vc = SearchWPViewController()
    //        //vc.data = wpData
    //        vc.hidesBottomBarWhenPushed = true
    //        vc.completionBlock = { (wps, reqObj, wpCode, category) -> Void in
    //            self.isSearchClicked = true
    //            self.wpSearchData = reqObj as? WPSearchRequest
    //            self.dataSource = wps as! [WorkPlace]
    //            self.categorySelected = category
    //            let wpName = self.wpSearchData?.wpName!
    //            if(wpCode == "1400") {
    //                self.titleView.heightConstraint?.constant = 50
    //                self.lblTitle.text = "Place is already registered with us. Please join and share your travel plan"
    //            } else if(wpCode == "1401") {
    //                self.lblTitle.userInteractionEnabled = true
    //                self.titleView.heightConstraint?.constant = 50
    //                let nowText = "Request new Place"
    //                let combinedString = "You may share rides in places shown below or \(nowText)" as NSString
    //                let range = combinedString.rangeOfString(nowText)
    //                let attributedString = NSMutableAttributedString(string: combinedString as String)
    //                let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1]
    //                attributedString.addAttributes(attributes, range: range)
    //                self.lblTitle.attributedText = attributedString
    //            } else if(wpCode == "1413") {
    //                self.lblTitle.userInteractionEnabled = true
    //                self.titleView.heightConstraint?.constant = 80
    //                let nowText = "Request new Place"
    //                let combinedString = "Sorry, we have not found place with \(wpName!). You may share rides in places shown below or \(nowText)" as NSString
    //                let range = combinedString.rangeOfString(nowText)
    //                let attributedString = NSMutableAttributedString(string: combinedString as String)
    //                let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1]
    //                attributedString.addAttributes(attributes, range: range)
    //                self.lblTitle.attributedText = attributedString
    //            } else {
    //                self.titleView.heightConstraint?.constant = 35
    //                self.lblTitle.text = "Search Result(s)"
    //            }
    //        }
    //        self.navigationController?.pushViewController(vc, animated: true)
    //    }
    
    func updateWPsList(_ triggerFrom: String)
    {
        if(triggerFrom == "Join") {
            if(localHideAnywhere != UserInfo.sharedInstance.hideAnywhere) {
                let ins = UIApplication.shared.delegate as! AppDelegate
                if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                    ins.gotoHome()
                } else {
                    ins.gotoHome(1)
                }
            } else {
                getProfileDetails("onUpdate")
                if(isMyPlacesVisible){
                    titleView.heightConstraint?.constant = 35
                    lblTitle.text = "My Workplace(s)"
                    self.lblTitle.isUserInteractionEnabled = false
                    getMyWP("onUpdate")
                }
                else{
                    titleView.heightConstraint?.constant = 35
                    lblTitle.text = "All Workplace(s)"
                    self.lblTitle.isUserInteractionEnabled = false
                    getAllWP("normal")
                }
            }
        } else {
            getProfileDetails("onUpdate")
            if(isMyPlacesVisible){
                titleView.heightConstraint?.constant = 35
                lblTitle.text = "My Workplace(s)"
                self.lblTitle.isUserInteractionEnabled = false
                getMyWP("onUpdate")
            }
            else{
                titleView.heightConstraint?.constant = 35
                lblTitle.text = "All Workplace(s)"
                self.lblTitle.isUserInteractionEnabled = false
                getAllWP("normal")
            }
        }
    }
    
    func updateList() {
        getProfileDetails("onUpdate")
        if(isMyPlacesVisible){
            titleView.heightConstraint?.constant = 35
            lblTitle.text = "My Workplace(s)"
            self.lblTitle.isUserInteractionEnabled = false
            getMyWP("onUpdate")
        }
        else{
            titleView.heightConstraint?.constant = 35
            lblTitle.text = "All Workplace(s)"
            self.lblTitle.isUserInteractionEnabled = false
            getAllWP("normal")
        }
    }
    
    func btnJoinedClicked () {
        if(!isMyPlacesVisible) {
            self.isMyPlacesVisible = true
            self.isAllPlacesVisible = false
            getMyWP("onClick")
        }
    }
    
    func btnAllClicked(_ btn: UIButton)
    {
        titleView.heightConstraint?.constant = 35
        lblTitle.text = "All Workplace(s)"
        self.lblTitle.isUserInteractionEnabled = false
        if(!isAllPlacesVisible) {
            self.isMyPlacesVisible = false
            self.isAllPlacesVisible = true
            self.wpSearchData?.wpName = ""
            self.wpSearchData?.wpLocation = ""
            self.wpSearchData?.wpLat = ""
            self.wpSearchData?.wpLong = ""
            self.wpSearchData?.category = ""
            self.categorySelected = ""
            getAllWP("normal")
        }
    }
    
    func redirectToAllPlaces(_ triggerFrom: String) {
        titleView.heightConstraint?.constant = 35
        lblTitle.text = "All Workplace(s)"
        self.lblTitle.isUserInteractionEnabled = false
        getAllWP(triggerFrom)
    }
    
    func getMyWP(_ triggerFrom: String)
    {
        setUpBtn(btnJoined, title:"wp_joined_green")
        setUpBtn(btnWPs, title:"Explore")
        lblJoined.textColor = Colors.GREEN_COLOR
        lblWPs.textColor = Colors.GRAY_COLOR
        titleView.heightConstraint?.constant = 35
        lblTitle.text = "My Workplace(s)"
        self.lblTitle.isUserInteractionEnabled = false
        
        isSearchClicked = false
        selectedIndexPath = nil
        //isMyWPListVisible = true
        iconFilter.isHidden = true
        removeTopView()
        addTopView(titleView)
        showIndicator("Loading Workplaces.")
        WorkplaceRequestor().getMyWorkplaces({ (success, object) in
            hideIndicator()
            self.dataSource = (object as! WorkPlacesResponse).data!.myOfcGroups!
            if(triggerFrom == "onStart" || triggerFrom == "redirectSummary"  || triggerFrom == "onUpdate") {
                self.isOnStart = false
                self.isMyPlacesVisible = true
                self.isAllPlacesVisible = false
                self.addBottomView(self.getButtonSubView())
                if(triggerFrom == "redirectSummary") {
                    self.getWpRides((self.dataSource.first?.wpID)!, wpData: self.dataSource.first!, triggerFrom: "redirection")
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func getAllWP(_ triggerFrom: String)
    {
        setUpBtn(btnJoined, title:"wp_joined_gray")
        setUpBtn(btnWPs, title:"Explore_green")
        lblJoined.textColor = Colors.GRAY_COLOR
        lblWPs.textColor = Colors.GREEN_COLOR
        self.iconFilter.setImage(UIImage(named: "filter_gray_new"), for: UIControlState())
        iconFilter.isHidden = false
        isSearchClicked = false
        removeTopView()
        addTopView(titleView)
        //getProfileDetails()
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Workplace Tab",
            AnalyticsParameterItemName: "Footer",
            AnalyticsParameterContentType:"All Workplace Button"
            ])
        selectedIndexPath = nil
        
        showIndicator("Loading Workplaces.")
        WorkplaceRequestor().getAllWorkPlace({ (success, object) in
            hideIndicator()
            self.dataSource = (object as! WorkPlacesResponse).data!.allOfcGroups!
            if(triggerFrom == "onRedirect") {
                self.isMyPlacesVisible = false
                self.isAllPlacesVisible = true
                self.addBottomView(self.getButtonSubView())
                self.setUpBtn(self.btnJoined, title:"wp_joined_gray")
                self.setUpBtn(self.btnWPs, title:"Explore_green")
                self.lblJoined.textColor = Colors.GRAY_COLOR
                self.lblWPs.textColor = Colors.GREEN_COLOR
            } else {
                self.isMyPlacesVisible = false
                self.isAllPlacesVisible = true
                self.wpSearchData?.wpName = ""
                self.wpSearchData?.wpLocation = ""
                self.wpSearchData?.wpLat = ""
                self.wpSearchData?.wpLong = ""
                self.wpSearchData?.category = ""
                self.categorySelected = ""
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func comingFromWpDetail()
    {
        isAllPlacesVisible = false
        btnAllClicked(btnWPs)
    }
    
    func getProfileDetails(_ triggerFrom: String) {
        let reqObj = ProfileDetailsRequest()
        reqObj.userID = UserInfo.sharedInstance.userID
        showIndicator("Loading Workplaces.")
        ProfileRequestor().profileDetailRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            if (object as! ProfileDetailsResponse).code == "415"
            {
                if((object as! ProfileDetailsResponse).dataObj?.first?.user_state == "3" || (object as! ProfileDetailsResponse).dataObj?.first?.user_state == "5") {
                    UserInfo.sharedInstance.isEmailIDVerified = true
                } else {
                    UserInfo.sharedInstance.isEmailIDVerified = false
                }
                if((object as! ProfileDetailsResponse).dataObj?.first?.secondary_email_status == "2") {
                    UserInfo.sharedInstance.isSecEmailIDVerified = true
                } else {
                    UserInfo.sharedInstance.isSecEmailIDVerified = false
                }
                if((object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere != nil && (object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere != "") {
                    UserInfo.sharedInstance.hideAnywhere = ((object as! ProfileDetailsResponse).dataObj?.first?.hideAnywhere)!
                }
                if((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myWorkplace != nil && (object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myWorkplace != "") {
                    UserInfo.sharedInstance.myWorkplaces = ((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myWorkplace)!
                }
                if((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myPendingWorkplace != nil && (object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myPendingWorkplace != "") {
                    UserInfo.sharedInstance.myPendingWorkplaces = ((object as! ProfileDetailsResponse).dataObj?.first?.userDetail?.myPendingWorkplace)!
                }
                if(triggerFrom == "onStart"){
                    self.addViews()
                }
            } else {
                if(triggerFrom == "onStart"){
                    self.addViews()
                }
            }
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
            if(triggerFrom == "onStart"){
                self.addViews()
            }
        }
    }
    
    func btnFilter_clicked() {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Workplace Tab",
            AnalyticsParameterItemName: "Search Workplace",
            AnalyticsParameterContentType:"Search Button"
            ])
        //getProfileDetails()
        let vc = SearchWPViewController()
        vc.wpSearchData = self.wpSearchData
        vc.categorySelected = self.categorySelected
        vc.searchResult = isSearchClicked
        vc.hidesBottomBarWhenPushed = true
        vc.resetBlock = { () -> Void in
            self.redirectToAllPlaces("onSearch")
        }
        vc.completionBlock = { (wps, reqObj, wpCode, category) -> Void in
            self.isAllPlacesVisible = false
            self.isSearchClicked = true
            self.wpSearchData = reqObj as? WPSearchRequest
            self.dataSource = wps as! [WorkPlace]
            self.categorySelected = category
            let wpName = self.wpSearchData?.wpName!
            self.iconFilter.setImage(UIImage(named: "filter_applied"), for: UIControlState())
            if(wpCode == "1400") {
                self.titleView.heightConstraint?.constant = 50
                self.lblTitle.text = "Workplace is already registered with us. Please join and share your travel plan"
            } else if(wpCode == "1401") {
                self.lblTitle.isUserInteractionEnabled = true
                self.titleView.heightConstraint?.constant = 50
                let nowText = "Request new Workplace"
                let combinedString = "You may share rides in workplaces shown below or \(nowText)" as NSString
                let range = combinedString.range(of: nowText)
                let attributedString = NSMutableAttributedString(string: combinedString as String)
                let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1] as [String : Any]
                attributedString.addAttributes(attributes, range: range)
                self.lblTitle.attributedText = attributedString
            } else if(wpCode == "1413") {
                self.lblTitle.isUserInteractionEnabled = true
                self.titleView.heightConstraint?.constant = 80
                let nowText = "Request new Workplace"
                let combinedString = "Sorry, we have not found workplace with \(wpName!). You may share rides in workplaces shown below or \(nowText)" as NSString
                let range = combinedString.range(of: nowText)
                let attributedString = NSMutableAttributedString(string: combinedString as String)
                let attributes = [NSForegroundColorAttributeName: Colors.GREEN_COLOR, NSUnderlineStyleAttributeName: 1] as [String : Any]
                attributedString.addAttributes(attributes, range: range)
                self.lblTitle.attributedText = attributedString
            } else {
                self.titleView.heightConstraint?.constant = 35
                self.lblTitle.text = "Search Result(s)"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
       self.updateWPsList("Other")
    }
}
