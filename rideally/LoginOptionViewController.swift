//
//  LoginOptionViewController.swift
//  rideally
//
//  Created by Raghunathan on 8/7/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class LoginOptionViewController: BaseScrollViewController {
    
    var  loaderView : UIView!
    var activityView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        setBGImageForView()
        createLoginOptionView()
        loadActivityLoader()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = Colors.GREEN_COLOR
        }
    }
    
    func loadActivityLoader() -> Void
    {
        self.loaderView = UIView()
        self.loaderView.backgroundColor = UIColor.black
        self.loaderView.isHidden = true
        self.loaderView.layer.borderWidth = 1.0
        self.loaderView.layer.borderColor = UIColor.white.cgColor
        self.loaderView.layer.cornerRadius = 5.0
        self.loaderView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.loaderView)
        
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[loaderView(35)]", options:[], metrics:nil, views: ["loaderView": self.loaderView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[loaderView(100)]", options:[], metrics:nil, views: ["loaderView": self.loaderView]))
        self.view.addConstraint(NSLayoutConstraint(item: self.loaderView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: self.loaderView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        
        let titleVlaue = UILabel()
        titleVlaue.text = "Loading..."
        titleVlaue.textAlignment = NSTextAlignment.left
        titleVlaue.font = boldFontWithSize(15)
        titleVlaue.textColor = UIColor.white
        titleVlaue.translatesAutoresizingMaskIntoConstraints = false
        self.loaderView.addSubview(titleVlaue)
        
        
        activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityView.hidesWhenStopped = true;
        activityView.translatesAutoresizingMaskIntoConstraints = false
        self.loaderView.addSubview(activityView)
        
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-1-[titleVlaue]-1-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[activityView]-5-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
        self.loaderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[activityView(25)]-5-[titleVlaue]-5-|", options:[], metrics:nil, views: ["titleVlaue": titleVlaue,"activityView": activityView]))
        
    }
    
    
    func setBGImageForView() -> Void {
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "LoginBG.png")
        self.view.addSubview(backgroundImage)
    }
    
    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.WHITE_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(20)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    func createLoginOptionView()->Void
    {
        let  facebookDescriptioLabelText  =  getLabelSubViews("We will not Post on your behalf")
        let  orLabelText        =  getLabelSubViews("OR")
        let  loginDescriptionLabelText  =  getLabelSubViews("Use your Email ID or Mobile Number")
        let  facebookButtonView =  getButtonSubViews("")
        facebookButtonView.backgroundColor = UIColor.clear
        //facebookButtonView.setBackgroundImage(UIImage(named:"LoginFacebook"), forState:UIControlState.Normal)
        facebookButtonView.setImage(UIImage(named:"LoginFacebook"), for: UIControlState())
        facebookButtonView.contentMode = .center
        facebookButtonView.tag = 99
        let  loginButtonView =  getButtonSubViews("Login")
        loginButtonView.tag = 100
        let  signUpButtonView =  getButtonSubViews("Sign Up")
        signUpButtonView.tag = 101
        
        let  loginOptionView = UIView()
        self.view.addSubview(loginOptionView)
        
        
        loginOptionView.addSubview(facebookButtonView)
        loginOptionView.addSubview(facebookDescriptioLabelText)
        loginOptionView.addSubview(orLabelText)
        loginOptionView.addSubview(loginDescriptionLabelText)
        loginOptionView.addSubview(loginButtonView)
        loginOptionView.addSubview(signUpButtonView)
        
        
        loginOptionView.translatesAutoresizingMaskIntoConstraints = false
        facebookButtonView.translatesAutoresizingMaskIntoConstraints = false
        facebookDescriptioLabelText.translatesAutoresizingMaskIntoConstraints = false
        orLabelText.translatesAutoresizingMaskIntoConstraints = false
        loginDescriptionLabelText.translatesAutoresizingMaskIntoConstraints = false
        loginButtonView.translatesAutoresizingMaskIntoConstraints = false
        signUpButtonView.translatesAutoresizingMaskIntoConstraints = false
        
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[loginOptView(350)]", options:[], metrics:nil, views: ["loginOptView": loginOptionView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[loginOptView]|", options:[], metrics:nil, views: ["loginOptView": loginOptionView]))
        
        self.view.addConstraint(NSLayoutConstraint(item: loginOptionView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        
        
        let subViewsDict = ["facebookButton": facebookButtonView,"facebookDespt":facebookDescriptioLabelText,"orValue":orLabelText,"loginDesp":loginDescriptionLabelText,"loginButton":loginButtonView,"signUpButton":signUpButtonView]
        loginOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[facebookButton(40)]-10-[facebookDespt(20)]-30-[orValue(20)]-30-[loginDesp(20)]-10-[loginButton(==facebookButton)]-30-[signUpButton(==facebookButton)]", options:[], metrics:nil, views: subViewsDict))
        
        loginOptionView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[facebookButton]-15-|", options:[], metrics:nil, views: subViewsDict))
        
        
        loginOptionView.addConstraint(NSLayoutConstraint(item: facebookButtonView, attribute: .centerX, relatedBy: .equal, toItem: loginOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginOptionView.addConstraint(NSLayoutConstraint(item: facebookDescriptioLabelText, attribute: .centerX, relatedBy: .equal, toItem: loginOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginOptionView.addConstraint(NSLayoutConstraint(item: orLabelText, attribute: .centerX, relatedBy: .equal, toItem: loginOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginOptionView.addConstraint(NSLayoutConstraint(item: loginDescriptionLabelText, attribute: .centerX, relatedBy: .equal, toItem: loginOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        
        loginOptionView.addConstraint(NSLayoutConstraint(item: loginButtonView, attribute: .centerX, relatedBy: .equal, toItem: loginOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginOptionView.addConstraint(NSLayoutConstraint(item: loginButtonView, attribute: .width, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        loginOptionView.addConstraint(NSLayoutConstraint(item: signUpButtonView, attribute: .centerX, relatedBy: .equal, toItem: loginOptionView, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        loginOptionView.addConstraint(NSLayoutConstraint(item: signUpButtonView, attribute: .width, relatedBy: .equal, toItem: facebookButtonView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
    }
    
    func buttonPressed (_ sender: AnyObject)
    {
        
        if  sender.tag != 99
        {
            var viewControllerInstance:UIViewController = LoginViewController()
            
            if   sender.tag == 100
            {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"Welcome Page",
                    AnalyticsParameterItemName: "Before Login Home",
                    AnalyticsParameterContentType:"Login Button"
                    ])
            }
            
            if sender.tag == 101
            {
                viewControllerInstance =  SignUpViewController()
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemID:"Welcome Page",
                    AnalyticsParameterItemName: "Before Login Home",
                    AnalyticsParameterContentType:"Sign Up Button"
                    ])
            }
            
            let ins = UIApplication.shared.delegate as! AppDelegate
            ins.changeRootViewControllerWithViewController(UINavigationController(rootViewController: viewControllerInstance))
        }
        else
        {
            facebookButtonPressedEvent()
        }
    }
    
    
    func facebookButtonPressedEvent() -> Void {
        
        let fbLogin = FBSDKLoginManager()
        fbLogin.logOut()
        
        fbLogin.logIn(withReadPermissions: ["public_profile","email"], from: self, handler: { (result, error) -> Void in
            
            self.view.bringSubview(toFront: self.loaderView)
            self.loaderView.isHidden = false
            self.activityView.startAnimating()
            self.view.isUserInteractionEnabled = false
            
            if (error == nil)
            {
                
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                
                if(fbloginresult.isCancelled)
                {
                    self.view.isUserInteractionEnabled = true
                    self.loaderView.isHidden = true
                    self.activityView.stopAnimating()
                }
                else
                {
                    self.getDetailsAndLogin()
                }
            }
            else
            {
                self.view.isUserInteractionEnabled = true
                self.loaderView.isHidden = true
                self.activityView.stopAnimating()
            }
        })
        
    }
    
    func getDetailsAndLogin() -> Void
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Welcome Page",
            AnalyticsParameterItemName: "Before Login Home",
            AnalyticsParameterContentType:"FaceBook Login Button"
            ])
        
        if((FBSDKAccessToken.current()) != nil)
        {
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id,gender,name,first_name,last_name, email,link,birthday"]).start(completionHandler: { (connection, result, error) -> Void in
                
                
                if (error == nil)
                {
                    let resultDict = result as! NSDictionary
                    let reqObj = FBLoginRequest()
                    reqObj.device_id = UIDevice.current.identifierForVendor?.uuidString
                    reqObj.id = resultDict.value(forKey: "id") as? String
                    reqObj.email = resultDict.value(forKey: "email") as? String
                    reqObj.firstName = resultDict.value(forKey: "first_name") as? String
                    reqObj.lastName = resultDict.value(forKey: "last_name") as? String
                    reqObj.link = resultDict.value(forKey: "link") as? String
                    reqObj.gender = resultDict.value(forKey: "gender") as? String
                    
                    
                    LoginRequestor().sendFBLoginRequest(reqObj, success:{ (success, object) in
                        
                        if ((object as! FBLoginResponse).FBdataObj?.first?.user_id)?.characters.count > 0
                        {
                            
                            self.view.isUserInteractionEnabled = true
                            self.loaderView.isHidden = true
                            self.activityView.stopAnimating()
                            UserInfo.sharedInstance.userID = ((object as! FBLoginResponse).FBdataObj?.first?.user_id)!
                            UserInfo.sharedInstance.userState = ((object as! FBLoginResponse).FBdataObj?.first?.user_state)!
                            UserInfo.sharedInstance.profilepicStatus = ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_status)!
                            UserInfo.sharedInstance.fullName = ((object as! FBLoginResponse).FBdataObj?.first?.full_name)!
                            UserInfo.sharedInstance.firstName = ((object as! FBLoginResponse).FBdataObj?.first?.first_name)!
                            UserInfo.sharedInstance.lastName = ((object as! FBLoginResponse).FBdataObj?.first?.last_name)!
                            UserInfo.sharedInstance.gender = ((object as! FBLoginResponse).FBdataObj?.first?.gender)!
                            
                            if((object as! FBLoginResponse).FBdataObj?.first?.work_address != nil) {
                                UserInfo.sharedInstance.workAddress = ((object as! FBLoginResponse).FBdataObj?.first?.work_address)!
                                UserInfo.sharedInstance.workLatValue = ((object as! FBLoginResponse).FBdataObj?.first?.work_lat)!
                                UserInfo.sharedInstance.workLongValue = ((object as! FBLoginResponse).FBdataObj?.first?.work_long)!
                            } else {
                                UserInfo.sharedInstance.workAddress = ""
                                UserInfo.sharedInstance.workLatValue = ""
                                UserInfo.sharedInstance.workLongValue = ""
                            }
                            
                            if((object as! FBLoginResponse).FBdataObj?.first?.home_address != nil) {
                                UserInfo.sharedInstance.homeAddress = ((object as! FBLoginResponse).FBdataObj?.first?.home_address)!
                                UserInfo.sharedInstance.homeLatValue = ((object as! FBLoginResponse).FBdataObj?.first?.home_lat)!
                                UserInfo.sharedInstance.homeLongValue = ((object as! FBLoginResponse).FBdataObj?.first?.home_long)!
                            } else {
                                UserInfo.sharedInstance.homeAddress = ""
                                UserInfo.sharedInstance.homeLatValue = ""
                                UserInfo.sharedInstance.homeLongValue = ""
                            }
                            
                            UserInfo.sharedInstance.dob = ((object as! FBLoginResponse).FBdataObj?.first?.dob)!
                            UserInfo.sharedInstance.faceBookid = ((object as! FBLoginResponse).FBdataObj?.first?.fb_id)!
                            UserInfo.sharedInstance.email = ((object as! FBLoginResponse).FBdataObj?.first?.email)!
                            UserInfo.sharedInstance.mobile = ((object as! FBLoginResponse).FBdataObj?.first?.mobile_number)!
                            UserInfo.sharedInstance.isOfficialEmail = ((object as! FBLoginResponse).FBdataObj?.first?.isOfficialEmail)!
                            UserInfo.sharedInstance.secEmail = ""
                            if((object as! FBLoginResponse).FBdataObj?.first?.secondary_email != nil) {
                                UserInfo.sharedInstance.secEmail = ((object as! FBLoginResponse).FBdataObj?.first?.secondary_email)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.secondary_email_status == "2") {
                                UserInfo.sharedInstance.isSecEmailIDVerified = true
                            } else {
                                UserInfo.sharedInstance.isSecEmailIDVerified = false
                            }
                            
                            UserInfo.sharedInstance.isEmailIDVerified = false
                            if (((object as! FBLoginResponse).FBdataObj?.first?.user_state) == "3" || ((object as! FBLoginResponse).FBdataObj?.first?.user_state) == "5")
                            {
                                UserInfo.sharedInstance.isEmailIDVerified = true
                            }
                            
                            UserInfo.sharedInstance.vehicleSeqID = ""
                            UserInfo.sharedInstance.vehicleModel = ""
                            UserInfo.sharedInstance.vehicleRegNo = ""
                            UserInfo.sharedInstance.vehicleSeatCapacity = ""
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.work_address)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.workAddress = ((object as! FBLoginResponse).FBdataObj?.first?.work_address)!
                            }
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_seqID)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeqID = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_seqID)!
                            }
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_model)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleModel = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_model)!
                            }
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_regNo)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleRegNo = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_regNo)!
                            }
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_capacity)?.characters.count > 0
                            {
                                UserInfo.sharedInstance.vehicleSeatCapacity = ((object as! FBLoginResponse).FBdataObj?.first?.default_Vehicle_capacity)!
                            }
                            
                            var boolVal = true
                            if ((object as! FBLoginResponse).FBdataObj?.first?.share_mobile_number)! == "0"
                            {
                                boolVal = false
                            }
                            UserInfo.sharedInstance.shareMobileNumber = boolVal
                            
                            
                            if ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_status)! == "0"
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                            }
                            else
                            {
                                UserInfo.sharedInstance.profilepicUrl = ""
                                if ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_url)?.characters.count > 0
                                {
                                    UserInfo.sharedInstance.profilepicUrl = ((object as! FBLoginResponse).FBdataObj?.first?.profilepic_url)!
                                }
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides != "") {
                                UserInfo.sharedInstance.allRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allRides)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces != "") {
                                UserInfo.sharedInstance.allWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.allWorkplaces)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace != "") {
                                UserInfo.sharedInstance.myWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myWorkplace)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace != "") {
                                UserInfo.sharedInstance.myPendingWorkplaces = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.myPendingWorkplace)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered != "") {
                                UserInfo.sharedInstance.offeredRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.offered)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed != "") {
                                UserInfo.sharedInstance.neededRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.needed)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined != "") {
                                UserInfo.sharedInstance.joinedRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.joined)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked != "") {
                                UserInfo.sharedInstance.bookedRides = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.booked)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints != nil && (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints != "") {
                                UserInfo.sharedInstance.maxAllowedPoints = ((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.maximumallowedpoints)!
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable != nil && String(describing: (object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable) != "") {
                                UserInfo.sharedInstance.availablePoints = (((object as! FBLoginResponse).FBdataObj?.first?.FBuserDetail?.pointsDetail?.pointsavailable)!)
                            }
                            if((object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere != nil && (object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere != "") {
                                UserInfo.sharedInstance.hideAnywhere = ((object as! FBLoginResponse).FBdataObj?.first?.hideAnywhere)!
                            }
                            self.sendDeviceToken()
                            UserInfo.sharedInstance.isLoggedIn = true
                            
                            if (object as! FBLoginResponse).code == "411" && (UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3") {
                                hideIndicator()
                                AlertController.showAlertFor("RideAlly", message: "Welcome \(UserInfo.sharedInstance.firstName) ! you are successfully registered with RideAlly.", okButtonTitle: "Ok", okAction: {
                                    AlertController.showToastForInfo("Please provide 'Mobile Number' to proceed further.")
                                    let vc = MobileVerificationViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                })
                            } else if UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3" {
                                hideIndicator()
                                if(UserInfo.sharedInstance.mobile == "") {
                                    AlertController.showToastForInfo("Please provide 'Mobile Number' to proceed further.")
                                } else {
                                    AlertController.showToastForInfo("Please verify 'Mobile Number' to proceed further.")
                                }
                                let vc = MobileVerificationViewController()
                                vc.hidesBottomBarWhenPushed = true
                                vc.edgesForExtendedLayout = UIRectEdge()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else if(UserInfo.sharedInstance.homeAddress == "") {
                                hideIndicator()
                                AlertController.showToastForInfo("Please provide 'Home Location' to proceed further.")
                                let vc = SignUpMapViewController()
                                self.navigationController?.pushViewController(vc, animated: true)
                            } else {
                                hideIndicator()
                                let myRides = Int(UserInfo.sharedInstance.offeredRides)! + Int(UserInfo.sharedInstance.neededRides)! + Int(UserInfo.sharedInstance.joinedRides)! + Int(UserInfo.sharedInstance.bookedRides)!
                                let ins = UIApplication.shared.delegate as! AppDelegate
                                if(myRides > 0) { // || UserInfo.sharedInstance.isOfficialEmail == "0"
                                    let vc = RideViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                } else if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                                    if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0) {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    } else if(Int(UserInfo.sharedInstance.myWorkplaces) == 1){
                                        self.getMyWP()
                                    } else {
                                        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                            ins.gotoHome()
                                        } else {
                                            ins.gotoHome(1)
                                        }
                                    }
                                } else {
                                    let vc = PoolOptionsViewController()
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        }
                        else
                        {
                            AlertController.showToastForError("User_ID is not there in the response.")
                        }
                    }){ (error) in
                        hideIndicator()
                        AlertController.showAlertForError(error)
                    }
                }
                else
                {
                    hideIndicator()
                    AlertController.showToastForError("Sorry, either 'Email Id' or 'Password' is incorrect.")
                }
            })
        }
    }
    
    
}
