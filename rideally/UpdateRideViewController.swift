//
//  UpdateRideViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/3/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import FirebaseAnalytics

class UpdateRideViewController : UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIScrollViewDelegate {
    
    var scrollView = UIScrollView()
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var mapView:GMSMapView!
    
    let sourceView = UIView()
    let destView = UIView()
    let viaView = UIView()
    let dateView = UIView()
    let vehView = UIView()
    let commentView = UIView()
    let bottonBtnView = UIView()
    
    let lblSource = UILabel()
    let lblDest = UILabel()
    let lblVia = UILabel()
    let txtDate = UITextField()
    let txtTime = UITextField()
    let txtTravel = UITextField()
    let lblVehModel = UILabel()
    let lblSeatLeft = UILabel()
    let lblCost = UILabel()
    let lblComment = UILabel()
    let txtComment = UITextField()
    let locationButton = UIButton()
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    let pickerView = UIPickerView()
    let iconVeh = UIImageView()
    let stepperSeats = PKYStepper()
    let stepperCost = PKYStepper()
    let imgLine = UIImageView()
    let imgLine1 = UIImageView()
    let imgLine2 = UIImageView()
    let imgLine3 = UIImageView()
    
    var mapSourcePinImageVIew = UIImageView()
    var mapDestPinImageVIew = UIImageView()
    
    var currentLocationView = UIView()
    var userMovedMap: Bool = false
    
    let appDelegate: AppDelegate = sharedAppDelegate()!
    var locationButtonAction: VoidCompletionHandler?
    
    var shouldShowCancelButton = false
    
    var cancelButton = UIButton(type: .system)
    var currentLocationButton = UIButton(type: .custom)
    var locationName: String = ""
    
    var onSelectionBlock: actionBlockWithParam?
    var onConfirmLocationsBlock: actionBlockWith6Params?
    
    var jumpOneScreen: Bool = false
    var updateData: RideDetail?
    var vehicleConfig: VehicleConfigResponse!
    var vehicleDefaultDetails: VehicleDefaultResponse!
    
    var routePolylineIndex0: GMSPolyline!
    var routePolylineIndex1: GMSPolyline!
    var routePolylineIndex2: GMSPolyline!
    var routePolyline: GMSPolyline!
    
    var polylineMutArray : NSMutableArray? = NSMutableArray()
    var viaMutArray : NSMutableArray? = NSMutableArray()
    
    let updateReqObj = UpdateRideRequest()
    var selectedTxtField: UITextField?
    var pickerData: [String] = [String]()
    var triggerFrom = ""
    var costValue = ""
    var SeatValue = ""
    var valueSelected: String?
    var toggleState = 1
    let rightArrowSource = UIImageView()
    let rightArrowDest = UIImageView()
    var changedLocation = false
    
    func goBack()
    {
        if(jumpOneScreen){
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
        }
        else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ISVEHICLEUPDATED = false
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton
        // Do any additional setup after loading the view, typically from a nib.
        mapSourcePinImageVIew.image = UIImage(imageLiteralResourceName: "marker_ride_green")
        mapDestPinImageVIew.image = UIImage(imageLiteralResourceName: "marker_ride_green")
        title = "Update Ride"
        if UserInfo.sharedInstance.gender == "M" {
            pickerData = ["Any", "Only Male"]
        }else if UserInfo.sharedInstance.gender == "F" {
            pickerData = ["Any", "Only Female"]
        }else{
            pickerData = ["Any","Only Male","Only Female"]
        }
        getConfigData()
        setupTextfield()
        setupBottomView()
        self.setupGoogleMaps()
        //        scrollView.backgroundColor = Colors.APP_BG
        //        selectedTextField = nil
        //        scrollView.showsVerticalScrollIndicator = false
        //        scrollView.bounces = false
        //        scrollView.delegate = self
        //
        //        scrollView.translatesAutoresizingMaskIntoConstraints = false
        //
        //        self.view.addSubview(scrollView)
        self.addOverlayToMapView((updateData?.sourceLat)!,sourceLang: (updateData?.sourceLong)!,distinationLat: (updateData?.destinationLat)!,distinationLang: (updateData?.destinationLong)!)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UpdateRideViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        //        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UpdateRideViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(ISVEHICLEUPDATED) {
            getDefaultVehicle()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGoogleMaps () {
        
        let lat = Double((updateData?.sourceLat)!)!
        let lng = Double((updateData?.sourceLong)!)!
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 16)
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        //self.moveMapToCoord(CLLocationCoordinate2DMake(lat, lng))
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        self.view.sendSubview(toBack: self.mapView)
        
        let dict = ["mapView": mapView, "source":sourceView, "via":viaView, "dest": destView, "date":dateView, "veh":vehView, "comment":commentView, "btn":bottonBtnView] as [String : Any]
        if(updateData?.rideType == "N") {
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(25)][via(25)][dest(25)][mapView][date(30)][comment(65)][btn(40)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            vehView.isHidden = true
        } else {
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(30)][via(30)][dest(30)][mapView][date(30)][veh(30)][comment(65)][btn(40)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[veh]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
            vehView.isHidden = false
        }
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[source]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dest]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[via]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[date]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[comment]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        updateReqObj.from_location = updateData?.source
        updateReqObj.from_lat = updateData?.sourceLat
        updateReqObj.from_lon = updateData?.sourceLong
        updateReqObj.to_location = updateData?.destination
        updateReqObj.to_lat = updateData?.destinationLat
        updateReqObj.to_lon = updateData?.destinationLong
        updateReqObj.poolstartdate = prettyDateStringFromString(updateData?.startDate, fromFormat: "dd-MMM-yyyy", toFormat: "dd-MMM-yyyy")
        updateReqObj.poolstarttime = prettyDateStringFromString(updateData?.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
        updateReqObj.pooltype = updateData?.poolType
        updateReqObj.seatLeft = updateData?.seatsLeft
        updateReqObj.vehregno = updateData?.vehicleRegNo
        updateReqObj.cost = updateData?.cost
        updateReqObj.flipLoc = "0"
        updateReqObj.officeRideType = updateData?.officeRideType
    }
    
    func setupTextfield () {
        
        sourceView.translatesAutoresizingMaskIntoConstraints = false
        sourceView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(sourceView)
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        sourceView.addSubview(iconSource)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.font = boldFontWithSize(13)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(mapForFrom))
        lblSource.isUserInteractionEnabled = true
        lblSource.addGestureRecognizer(tapGestureFrom)
        sourceView.addSubview(lblSource)
        
        rightArrowSource.image = UIImage(named: "rightArrow")
        rightArrowSource.translatesAutoresizingMaskIntoConstraints = false
        rightArrowSource.isHidden = false
        if(triggerFrom == "place" && updateData?.officeRideType == "C") {
            rightArrowSource.isHidden = true
            lblSource.isUserInteractionEnabled = false
        }
        sourceView.addSubview(rightArrowSource)
        
        sourceView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[source(20)]|", options: [], metrics: nil, views: ["source":lblSource]))
        sourceView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource(15)][source][iarrow(15)]-10-|", options: [], metrics: nil, views: ["isource":iconSource, "source":lblSource, "iarrow":rightArrowSource]))
        
        sourceView.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        sourceView.addConstraint(NSLayoutConstraint(item: lblSource, attribute: .centerY, relatedBy: .equal, toItem: rightArrowSource, attribute: .centerY, multiplier: 1, constant: 0))
        
        viaView.translatesAutoresizingMaskIntoConstraints = false
        viaView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(viaView)
        
        let iconVia = UIImageView()
        iconVia.image = UIImage(named: "Via_green")
        iconVia.translatesAutoresizingMaskIntoConstraints = false
        viaView.addSubview(iconVia)
        
        lblVia.textColor = Colors.GRAY_COLOR
        lblVia.font = boldFontWithSize(13)
        lblVia.translatesAutoresizingMaskIntoConstraints = false
        viaView.addSubview(lblVia)
        
        viaView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[via(20)]|", options: [], metrics: nil, views: ["via":lblVia]))
        viaView.addConstraint(NSLayoutConstraint(item: iconVia, attribute: .centerY, relatedBy: .equal, toItem: lblVia, attribute: .centerY, multiplier: 1, constant: 0))
        
        if triggerFrom == "ride" {
            viaView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[ivia(6)]-5-[via]-5-|", options: [], metrics: nil, views: ["ivia":iconVia, "via":lblVia]))
        } else {
            let iconFlip = UIImageView()
            iconFlip.image = UIImage(named: "airport_flip")
            iconFlip.translatesAutoresizingMaskIntoConstraints = false
            let tapGestureFlip = UITapGestureRecognizer(target: self, action: #selector(FlipLocations))
            iconFlip.isUserInteractionEnabled = true
            iconFlip.addGestureRecognizer(tapGestureFlip)
            viaView.addSubview(iconFlip)
            viaView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[ivia(6)]-5-[via][iflip(20)]-10-|", options: [], metrics: nil, views: ["ivia":iconVia, "via":lblVia, "iflip":iconFlip]))
            viaView.addConstraint(NSLayoutConstraint(item: iconFlip, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20))
            viaView.addConstraint(NSLayoutConstraint(item: lblVia, attribute: .centerY, relatedBy: .equal, toItem: iconFlip, attribute: .centerY, multiplier: 1, constant: 0))
        }
        
        destView.translatesAutoresizingMaskIntoConstraints = false
        destView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(destView)
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        destView.addSubview(iconDest)
        
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = boldFontWithSize(13)
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForTo))
        lblDest.isUserInteractionEnabled = true
        lblDest.addGestureRecognizer(tapGestureTo)
        destView.addSubview(lblDest)
        
        rightArrowDest.image = UIImage(named: "rightArrow")
        rightArrowDest.translatesAutoresizingMaskIntoConstraints = false
        rightArrowDest.isHidden = false
        destView.addSubview(rightArrowDest)
        if(triggerFrom == "place" && updateData?.officeRideType == "G") {
            rightArrowDest.isHidden = true
            lblDest.isUserInteractionEnabled = false
        }
        destView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dest(20)]|", options: [], metrics: nil, views: ["dest":lblDest]))
        destView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest(15)][dest][iarrow(15)]-10-|", options: [], metrics: nil, views: ["idest":iconDest, "dest":lblDest, "iarrow":rightArrowDest]))
        
        destView.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        destView.addConstraint(NSLayoutConstraint(item: lblDest, attribute: .centerY, relatedBy: .equal, toItem: rightArrowDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        lblSource.text = updateData?.source
        lblDest.text = updateData?.destination
        if let via = updateData?.via {
            lblVia.text = "\("VIA-") \(via)"
        } else {
            lblVia.text = ""
        }
    }
    
    func setupBottomView () {
        
        dateView.translatesAutoresizingMaskIntoConstraints = false
        dateView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(dateView)
        
        txtDate.delegate = self
        txtDate.textColor = Colors.GRAY_COLOR
        txtDate.translatesAutoresizingMaskIntoConstraints = false
        txtDate.font = boldFontWithSize(13)
        
        datePicker.datePickerMode = .date
        txtDate.inputView = datePicker
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(datePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtDate.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "cal")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        txtDate.leftView = imgV
        txtDate.leftViewMode = .always
        dateView.addSubview(txtDate)
        
        imgLine.image = UIImage(named: "gray_separator")
        imgLine.translatesAutoresizingMaskIntoConstraints = false
        dateView.addSubview(imgLine)
        
        txtTime.delegate = self
        txtTime.textColor = Colors.GRAY_COLOR
        txtTime.font = boldFontWithSize(13)
        txtTime.translatesAutoresizingMaskIntoConstraints = false
        
        timePicker.datePickerMode = .time
        //timePicker.locale = NSLocale(localeIdentifier: "NL")
        txtTime.inputView = timePicker
        timePicker.date = Date().addingTimeInterval(300)
        timePicker.addTarget(self, action: #selector(timePicked), for: .valueChanged)
        
        txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV1 = UIImageView(image: UIImage(named: "time")!)
        imgV1.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        imgV1.contentMode = .center
        
        txtTime.leftView = imgV1
        txtTime.leftViewMode = .always
        dateView.addSubview(txtTime)
        
        imgLine1.image = UIImage(named: "gray_separator")
        imgLine1.translatesAutoresizingMaskIntoConstraints = false
        dateView.addSubview(imgLine1)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        txtTravel.delegate=self
        txtTravel.textColor = Colors.GRAY_COLOR
        txtTravel.translatesAutoresizingMaskIntoConstraints = false
        txtTravel.inputView = pickerView
        txtTravel.font = boldFontWithSize(13)
        let imgV2 = UIImageView(image: UIImage(named: "travel_with_new")!)
        imgV2.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        imgV2.contentMode = .center
        txtTravel.leftView = imgV2
        txtTravel.leftViewMode = .always
        let imgV3 = UIImageView(image: UIImage(named: "dropdown")!)
        imgV3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV3.contentMode = .center
        txtTravel.rightView = imgV3
        txtTravel.rightViewMode = .always
        
        let keyboardDoneButtonShow1 = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow1.barStyle = UIBarStyle .blackTranslucent
        let doneButton1 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton1 = [flexSpace1,doneButton1]
        keyboardDoneButtonShow1.setItems(toolbarButton1, animated: false)
        txtTravel.inputAccessoryView = keyboardDoneButtonShow1
        dateView.addSubview(txtTravel)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        dateView.addSubview(lblLine1)
        
        dateView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[date(20)][line1(0.5)]|", options: [], metrics: nil, views: ["date":txtDate, "line1":lblLine1]))
        dateView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[date][iline(1)][time(==date)][iline1(1)][travel(==date)]|", options: [], metrics: nil, views: ["date":txtDate, "time":txtTime, "travel":txtTravel, "iline":imgLine, "iline1":imgLine1]))
        dateView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: ["line1":lblLine1]))
        
        dateView.addConstraint(NSLayoutConstraint(item: txtDate, attribute: .centerY, relatedBy: .equal, toItem: imgLine, attribute: .centerY, multiplier: 1, constant: 0))
        dateView.addConstraint(NSLayoutConstraint(item: imgLine, attribute: .centerY, relatedBy: .equal, toItem: txtTime, attribute: .centerY, multiplier: 1, constant: 0))
        dateView.addConstraint(NSLayoutConstraint(item: txtTime, attribute: .centerY, relatedBy: .equal, toItem: imgLine1, attribute: .centerY, multiplier: 1, constant: 0))
        dateView.addConstraint(NSLayoutConstraint(item: imgLine1, attribute: .centerY, relatedBy: .equal, toItem: txtTravel, attribute: .centerY, multiplier: 1, constant: 0))
        
        txtDate.text = updateData?.startDateDisplay
        txtTime.text = updateData?.startTimeDisplay
        
        let date = prettyDateStringFromString(updateData?.startDate, fromFormat: "dd-MMM-yyyy", toFormat: "dd-MMM-yyyy")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        datePicker.date = dateFormatter.date(from: date)!
        
        let time = prettyDateStringFromString(updateData?.startTime, fromFormat: "hh:mm a", toFormat: "HH:mm")
        dateFormatter.dateFormat = "HH:mm"
        timePicker.date = dateFormatter.date(from: time)!
        
        
        if(updateData?.poolType == "M"){
            txtTravel.text = "Only Male"
        }
        else if(updateData?.poolType == "F"){
            txtTravel.text = "Only Female"
        }
        else{
            txtTravel.text = "Any"
        }
        
        vehView.translatesAutoresizingMaskIntoConstraints = false
        vehView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(vehView)
        
        iconVeh.translatesAutoresizingMaskIntoConstraints = false
        vehView.addSubview(iconVeh)
        
        lblVehModel.textColor = Colors.GRAY_COLOR
        lblVehModel.font = boldFontWithSize(13)
        lblVehModel.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureVeh = UITapGestureRecognizer(target: self, action: #selector(selectVehicle))
        lblVehModel.isUserInteractionEnabled = true
        lblVehModel.addGestureRecognizer(tapGestureVeh)
        vehView.addSubview(lblVehModel)
        
        imgLine2.image = UIImage(named: "gray_separator")
        imgLine2.translatesAutoresizingMaskIntoConstraints = false
        vehView.addSubview(imgLine2)
        
        let iconSeat = UIImageView()
        iconSeat.image = UIImage(named: "seat")
        iconSeat.translatesAutoresizingMaskIntoConstraints = false
        vehView.addSubview(iconSeat)
        
        lblSeatLeft.textColor = Colors.GRAY_COLOR
        lblSeatLeft.font = boldFontWithSize(13)
        lblSeatLeft.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureSeatLeft = UITapGestureRecognizer(target: self, action: #selector(selectCost))
        lblSeatLeft.isUserInteractionEnabled = true
        lblSeatLeft.addGestureRecognizer(tapGestureSeatLeft)
        vehView.addSubview(lblSeatLeft)
        
        imgLine3.image = UIImage(named: "gray_separator")
        imgLine3.translatesAutoresizingMaskIntoConstraints = false
        vehView.addSubview(imgLine3)
        
        let iconCost = UIImageView()
        iconCost.image = UIImage(named: "fare_green-1")
        iconCost.translatesAutoresizingMaskIntoConstraints = false
        vehView.addSubview(iconCost)
        
        lblCost.textColor = Colors.GRAY_COLOR
        lblCost.font = boldFontWithSize(13)
        lblCost.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureCost = UITapGestureRecognizer(target: self, action: #selector(selectCost))
        lblCost.isUserInteractionEnabled = true
        lblCost.addGestureRecognizer(tapGestureCost)
        vehView.addSubview(lblCost)
        
        let lblLine2 = UILabel()
        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine2.translatesAutoresizingMaskIntoConstraints = false
        vehView.addSubview(lblLine2)
        
        vehView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[model(20)][line2(0.5)]|", options: [], metrics: nil, views: ["model":lblVehModel, "line2":lblLine2]))
        vehView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imodel(15)]-5-[model][iline2(1)]-10-[iseat(15)]-5-[seat(==model)][iline3(1)]-10-[icost(15)]-5-[cost(==model)]|", options: [], metrics: nil, views: ["imodel":iconVeh, "model":lblVehModel, "iseat":iconSeat, "seat":lblSeatLeft, "icost":iconCost, "cost":lblCost, "iline2":imgLine2, "iline3":imgLine3]))
        vehView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line2]|", options: [], metrics: nil, views: ["line2":lblLine2]))
        
        vehView.addConstraint(NSLayoutConstraint(item: iconVeh, attribute: .centerY, relatedBy: .equal, toItem: lblVehModel, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: lblVehModel, attribute: .centerY, relatedBy: .equal, toItem: imgLine2, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: imgLine2, attribute: .centerY, relatedBy: .equal, toItem: iconSeat, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: iconSeat, attribute: .centerY, relatedBy: .equal, toItem: lblSeatLeft, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: lblSeatLeft, attribute: .centerY, relatedBy: .equal, toItem: imgLine3, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: imgLine3, attribute: .centerY, relatedBy: .equal, toItem: iconCost, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: iconCost, attribute: .centerY, relatedBy: .equal, toItem: lblCost, attribute: .centerY, multiplier: 1, constant: 0))
        
        //lblVehModel.text = updateData?.vehicleType
        if let vehicleType = updateData?.vehicleType{
            if(vehicleType.lowercased() == "bike") {
                iconVeh.image = UIImage(named: "veh_bike")
            } else if(vehicleType.lowercased() == "auto") {
                iconVeh.image = UIImage(named: "auto")
            } else if(vehicleType.lowercased() == "car") {
                iconVeh.image = UIImage(named: "veh_car")
            } else if(vehicleType.lowercased() == "cab") {
                iconVeh.image = UIImage(named: "cab")
            } else if(vehicleType.lowercased() == "suv") {
                iconVeh.image = UIImage(named: "suv")
            } else if(vehicleType.lowercased() == "tempo") {
                iconVeh.image = UIImage(named: "tempo")
            } else if(vehicleType.lowercased() == "bus") {
                iconVeh.image = UIImage(named: "bus")
            }
            lblVehModel.text = vehicleType.capitalized
        }
        SeatValue = (updateData?.seatsLeft)!
        lblSeatLeft.text = SeatValue
        if let cost = updateData?.cost {
            costValue = cost
            lblCost.text = "\(cost) \("/ Km")"
            stepperCost.value = Int32(cost)!
            stepperCost.countLabel.text = "\(cost)"
        }
        
        commentView.translatesAutoresizingMaskIntoConstraints = false
        commentView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(commentView)
        
        lblComment.text = "COMMENT"
        lblComment.font = boldFontWithSize(12)
        lblComment.textColor = Colors.GRAY_COLOR
        lblComment.translatesAutoresizingMaskIntoConstraints = false
        commentView.addSubview(lblComment)
        
        txtComment.font = boldFontWithSize(13)
        txtComment.placeholder = "Enter your comment here"
        txtComment.translatesAutoresizingMaskIntoConstraints = false
        txtComment.returnKeyType = UIReturnKeyType.done
        txtComment.delegate = self
        commentView.addSubview(txtComment)
        
        let viewsDict = ["lbl":lblComment, "txt":txtComment]
        
        commentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl(20)][txt(20)]", options: [], metrics: nil, views: viewsDict))
        
        commentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[lbl]-5-|", options: [], metrics: nil, views: viewsDict))
        commentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[txt]-5-|", options: [], metrics: nil, views: viewsDict))
        
        bottonBtnView.translatesAutoresizingMaskIntoConstraints = false
        bottonBtnView.backgroundColor = Colors.WHITE_COLOR
        self.view.addSubview(bottonBtnView)
        
        locationButton.setTitle("UPDATE", for: UIControlState())
        locationButton.backgroundColor = Colors.GRAY_COLOR
        locationButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        locationButton.titleLabel?.font = boldFontWithSize(16.0)
        locationButton.translatesAutoresizingMaskIntoConstraints = false
        bottonBtnView.addSubview(locationButton)
        locationButton.addTarget(self, action: #selector(proceed), for: UIControlEvents.touchUpInside)
        bottonBtnView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(40)]", options: [], metrics: nil, views: ["btn":locationButton]))
        bottonBtnView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":locationButton]))
        
    }
    
    func addOverlayToMapView(_ sourceLat: String,sourceLang: String,distinationLat: String,distinationLang: String ){
        self.mapView.clear()
        
        showIndicator("Loading Map path...")
        var sourceStr = ""
        var destinationStr = ""
        if(updateData?.officeRideType == "C") {
            destinationStr = "\(sourceLat),\(sourceLang)"
            sourceStr = "\(distinationLat), \(distinationLang)"
        } else {
            sourceStr = "\(sourceLat),\(sourceLang)"
            destinationStr = "\(distinationLat), \(distinationLang)"
        }
        
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        //print(directionURL," directionURL --------- ")
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                //  print(response)
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    self.addmarkers(legs)
                    if(self.changedLocation) {
                        self.addPolyLineWithEncodedStringInMap(routes as NSArray)
                    } else {
                        self.addPolyLineUpdateViewWithEncodedStringInMap(routes as NSArray)
                    }
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }
    
    // MARK:- Google Map Direction Api Call
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    // MARK:- Add markers
    func addmarkers(_ legs : NSArray) {
        let legsStartLocDict = legs[0] as! NSDictionary
        let startLocationDictionary = legsStartLocDict["start_location"] as! NSDictionary
        self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
        let legsEndLocDict = legs[legs.count - 1] as! NSDictionary
        let endLocationDictionary = legsEndLocDict["end_location"] as! NSDictionary
        self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
        
        //self.mapRideView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
        
        let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
        let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
        let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
        let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
        self.mapView.camera = camera
        
        self.originMarker = GMSMarker(position: self.originCoordinate)
        self.originMarker.map = self.mapView
        self.originMarker.tracksInfoWindowChanges = true
        self.originMarker.title = updateData?.source
        
        self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
        self.destinationMarker.map = self.mapView
        self.destinationMarker.tracksInfoWindowChanges = true
        
        self.destinationMarker.title = updateData?.destination
        if(triggerFrom == "ride") {
            if(updateData?.rideType == "O") {
                self.originMarker.icon = UIImage(named:"marker_ride_orange")
                self.destinationMarker.icon = UIImage(named:"marker_ride_green")
            } else {
                self.originMarker.icon = UIImage(named:"marker_profile_orange")
                self.destinationMarker.icon = UIImage(named:"marker_profile_green")
            }
        } else {
            //            if(updateData?.officeRideType == "C") {
            //                self.originMarker.icon = UIImage(named:"marker_wp_green")
            //                self.destinationMarker.icon = UIImage(named:"marker_home_green")
            //            } else {
            self.originMarker.icon = UIImage(named:"marker_home_green")
            self.destinationMarker.icon = UIImage(named:"marker_wp_green")
            // }
        }
    }
    
    func addPolyLineWithEncodedStringInMap(_ routes: NSArray) {
        self.polylineMutArray? .removeAllObjects()
        self.viaMutArray? .removeAllObjects()
        for index in 0..<routes.count{
            let routesDicts = routes[index] as! NSDictionary
            let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
            let points = overview_polyline ["points"] as! NSString
            let summary = routesDicts ["summary"] as! NSString
            let path = GMSMutablePath(fromEncodedPath: points as String)
            let routePolylineObje = GMSPolyline(path: path)
            
            if index == 0 {
                self.routePolylineIndex0 = routePolylineObje
                lblVia.text = "\("VIA-") \(summary as String)"
                updateReqObj.via = routesDicts ["summary"] as? String
                updateReqObj.waypoints = points as String
            }else if index == 1{
                self.routePolylineIndex1 = routePolylineObje
            }else{
                self.routePolylineIndex2 = routePolylineObje
            }
            
            if index  == 0 {
                routePolylineObje.zIndex = 100
                routePolylineObje.strokeColor = Colors.POLILINE_GREEN
            }else{
                routePolylineObje.strokeColor = Colors.POLILINE_GRAY
            }
            
            routePolylineObje.strokeWidth = 5.0
            routePolylineObje.isTappable = true
            routePolylineObje.title = String(index)
            self.polylineMutArray? .add(points)
            self.viaMutArray? .add(summary)
            routePolylineObje.map = self.mapView
            
            let bounds = GMSCoordinateBounds(path: path!)
            mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
        }
        
    }
    
    func addPolyLineUpdateViewWithEncodedStringInMap(_ routes: NSArray) {
        var flagPolyGreen = false
        for index in 0..<routes.count{
            let routesDicts = routes[index] as! NSDictionary
            let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
            let points = overview_polyline ["points"] as! NSString
            let path = GMSMutablePath(fromEncodedPath: points as String)
            let routePolylineObje = GMSPolyline(path: path)
            let summary = routesDicts ["summary"] as! NSString
            var via1 = ""
            var via2 = ""
            var via3 = ""
            if index == 0 {
                self.routePolylineIndex0 = routePolylineObje
                via1 = summary as String
            }else if index == 1{
                self.routePolylineIndex1 = routePolylineObje
                via2 = summary as String
            }else{
                self.routePolylineIndex2 = routePolylineObje
                via3 = summary as String
            }
            if(updateData?.via == via1) {
                flagPolyGreen = true
            } else if(updateData?.via == via2) {
                flagPolyGreen = true
            } else if(updateData?.via == via3) {
                flagPolyGreen = true
            }
            if flagPolyGreen {
                routePolylineObje.zIndex = 100
                routePolylineObje.strokeColor = Colors.POLILINE_GREEN
                lblVia.text = "\("VIA-") \(summary as String)"
                updateReqObj.via = summary as String
                updateReqObj.waypoints = points as String
                flagPolyGreen = false
            }else{
                routePolylineObje.strokeColor = Colors.POLILINE_GRAY
            }
            
            routePolylineObje.strokeWidth = 5.0
            routePolylineObje.isTappable = true
            routePolylineObje.title = String(index)
            self.polylineMutArray? .add(points)
            self.viaMutArray? .add(summary)
            routePolylineObje.map = mapView
            let bounds = GMSCoordinateBounds(path: path!)
            mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        let selectedPolyline : GMSPolyline = overlay as! GMSPolyline
        let selectedIndex = Int(selectedPolyline.title!)
        let polylinePath  = self.polylineMutArray?[selectedIndex!] as! String
        let viaStr  = self.viaMutArray?[selectedIndex!] as! String
        lblVia.text = "\("VIA-") \(viaStr)"
        updateReqObj.via = viaStr
        updateReqObj.waypoints = polylinePath
        for index in 0..<(self.polylineMutArray?.count)! {
            if index == 0 {
                self.routePolylineIndex0.strokeColor = Colors.POLILINE_GRAY
            }
            if index == 1 {
                self.routePolylineIndex1.strokeColor = Colors.POLILINE_GRAY
            }
            if index == 2  {
                self.routePolylineIndex2.strokeColor = Colors.POLILINE_GRAY
            }
        }
        selectedPolyline.zIndex = 100
        selectedPolyline.strokeColor = Colors.POLILINE_GREEN
    }
    
    func mapForFrom()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            if(triggerFrom == "place") {
                vc.isFromWP = true
            }
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.updateReqObj.from_location = self.lblSource.text
                    self.updateReqObj.from_lat = loc["lat"]
                    self.updateReqObj.from_lon = loc["long"]
                    if(self.triggerFrom == "place") {
                        if(self.updateReqObj.from_location != UserInfo.sharedInstance.homeAddress){
                            AlertController.showAlertFor("Home Location", message: "Do you want to save \(self.updateReqObj.from_location!) as your Home location", okButtonTitle: "Change", okAction: {
                                self.saveHomeLocation(loc["location"]!, lat: loc["lat"]!, long: loc["long"]!)
                                }, cancelButtonTitle: "Cancel", cancelAction: {
                                    _ = self.navigationController?.popViewController(animated: true)})
                        }
                    }
                    self.changedLocation = true
                    self.addOverlayToMapView(self.updateReqObj.from_lat!,sourceLang: self.updateReqObj.from_lon!,distinationLat: (self.updateData?.destinationLat)!,distinationLang: (self.updateData?.destinationLong)!)
                }
            }
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func mapForTo()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            if(triggerFrom == "place") {
                vc.isFromWP = true
            }
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.updateReqObj.to_location = self.lblDest.text
                    self.updateReqObj.to_lat = loc["lat"]
                    self.updateReqObj.to_lon = loc["long"]
                    if(self.triggerFrom == "place") {
                        if(self.updateReqObj.to_location != UserInfo.sharedInstance.homeAddress){
                            AlertController.showAlertFor("Home Location", message: "Do you want to save \(self.updateReqObj.to_location!) as your Home location", okButtonTitle: "Change", okAction: {
                                self.saveHomeLocation(loc["location"]!, lat: loc["lat"]!, long: loc["long"]!)
                                }, cancelButtonTitle: "Cancel", cancelAction: {
                                    _ = self.navigationController?.popViewController(animated: true)})
                        }
                    }
                    self.changedLocation = true
                    self.addOverlayToMapView((self.updateData?.sourceLat)!,sourceLang: (self.updateData?.sourceLong)!,distinationLat: self.updateReqObj.to_lat!,distinationLang: self.updateReqObj.to_lon!)
                }
            }
             vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func saveHomeLocation(_ loc: String,lat: String,long: String) {
        let reqObj = SetHomeLocationRequest()
        showIndicator("Loading...")
        reqObj.user_id = UserInfo.sharedInstance.userID
        reqObj.home_loc = loc
        reqObj.home_loc_lat = lat
        reqObj.home_loc_long = long
        
        ProfileRequestor().sendsetHomeLocationRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            
            if (object as! SetHomeLocationResponse).code == "8535"
            {
                
                if (object as! SetHomeLocationResponse).dataObj?.first != nil
                {
                    UserInfo.sharedInstance.homeAddress = loc
                    UserInfo.sharedInstance.homeLatValue = lat
                    UserInfo.sharedInstance.homeLongValue = long
                    _ = self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    AlertController.showToastForError("There is no User_ID found in the response.")
                }
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
    
    func datePicked()
    {
        
    }
    
    func timePicked()
    {
        
    }
    
    func donePressed()
    {
        if(selectedTxtField == txtDate){
            updateReqObj.poolstartdate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            selectedTxtField?.text = prettyDateStringFromDate(datePicker.date, toFormat: "dd MMM")
        }
        else if(selectedTxtField == txtTime){
            updateReqObj.poolstarttime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            selectedTxtField?.text = prettyDateStringFromDate(timePicker.date, toFormat: "hh:mm a")
        } else if(selectedTxtField == txtTravel) {
            txtTravel.text = valueSelected
            if (valueSelected == "Only Male"){
                updateReqObj.pooltype  = "M"
            }else if (valueSelected == "Only Female"){
                updateReqObj.pooltype  = "F"
            }else{
                txtTravel.text = "Any"
                updateReqObj.pooltype  = "A"
            }
        }
        selectedTxtField?.resignFirstResponder()
    }
    
    func FlipLocations()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Update Ride Screen",
            AnalyticsParameterContentType:"Update Ride Screen"
            ])
        var fromLoc: String?
        var toLoc: String?
        
        if(toggleState == 1) {
            lblDest.isUserInteractionEnabled = true
            lblSource.isUserInteractionEnabled = false
            rightArrowDest.isHidden = false
            rightArrowSource.isHidden = true
            updateReqObj.flipLoc = "1"
            if(updateData?.officeRideType == "G") {
                updateReqObj.officeRideType = "C"
            } else {
                updateReqObj.officeRideType = "G"
            }
            toggleState = 2
        } else {
            lblSource.isUserInteractionEnabled = true
            lblDest.isUserInteractionEnabled = false
            rightArrowSource.isHidden = false
            rightArrowDest.isHidden = true
            updateReqObj.flipLoc = "0"
            updateReqObj.officeRideType = updateData?.officeRideType
            toggleState = 1
        }
        fromLoc = lblSource.text
        toLoc = lblDest.text
        
        lblSource.text = toLoc
        lblDest.text = fromLoc
    }
    
    func selectVehicle () {
        let vc = AddVehicleViewController()
        vc.isFromWp = "1"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func selectCost () {
        let viewHolder = AlertContentViewHolder()
        viewHolder.heightConstraintValue = 95
        
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        let lblSeat = UILabel()
        lblSeat.text = "Seat : "
        lblSeat.textColor = Colors.GRAY_COLOR
        lblSeat.font = normalFontWithSize(12)
        lblSeat.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblSeat)
        
        stepperSeats.setBorderColor(Colors.GRAY_COLOR)
        stepperSeats.setBorderWidth(0.5)
        stepperSeats.countLabel.layer.borderWidth = 0.5
        stepperSeats.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperSeats.valueChangedCallback = { (stepper, count) -> Void in
            self.stepperSeats.countLabel.text = "\(count)"
        }
        stepperSeats.setLabelTextColor(Colors.GRAY_COLOR)
        stepperSeats.value = 1
        stepperSeats.minimum = 1
        stepperSeats.value = Int32(SeatValue)!
        stepperSeats.countLabel.text = "\(SeatValue)"
        stepperSeats.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Seat selection is exceeding its maximum seat available.")
            }
        }
        
        stepperSeats.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperSeats)
        
        let lblCostTitle = UILabel()
        lblCostTitle.text = "Cost : "
        lblCostTitle.textColor = Colors.GRAY_COLOR
        lblCostTitle.font = normalFontWithSize(12)
        lblCostTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCostTitle)
        
        stepperCost.setBorderColor(Colors.GRAY_COLOR)
        stepperCost.setBorderWidth(0.5)
        stepperCost.countLabel.layer.borderWidth = 0.5
        stepperCost.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperCost.valueChangedCallback = { (stepper, count) -> Void in
            self.stepperCost.countLabel.text = "\(count)"
        }
        stepperCost.setLabelTextColor(Colors.GRAY_COLOR)
        stepperCost.value = Int32(costValue)!
        //stepperCost.minimum = 1
        
        stepperCost.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Sorry, you can not select more than " + String(describing: stepper?.maximum)+".")
            }
        }
        
        stepperCost.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperCost)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[lblSeat(40)]-10-[lblCost(40)]|", options: [], metrics: nil, views: ["lblSeat":lblSeat, "lblCost":lblCostTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblSeat]-10-[stepperSeat(150)]-10-|", options: [], metrics: nil, views: ["lblSeat":lblSeat, "stepperSeat":stepperSeats]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblCost]-10-[stepperCost(150)]-10-|", options: [], metrics: nil, views: ["lblCost":lblCostTitle, "stepperCost":stepperCost]))
        
        view.addConstraint(NSLayoutConstraint(item: lblSeat, attribute: .centerY, relatedBy: .equal, toItem: stepperSeats, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stepperSeats, attribute: .height, relatedBy: .equal, toItem: lblSeat, attribute: .height, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblCostTitle, attribute: .centerY, relatedBy: .equal, toItem: stepperCost, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stepperCost, attribute: .height, relatedBy: .equal, toItem: lblCostTitle, attribute: .height, multiplier: 1, constant: 0))
        viewHolder.view = view
        
        AlertController.showAlertFor("Fare", message: "", contentView: viewHolder, okButtonTitle: "OK", willHaveAutoDismiss: false, okAction: {
            self.dismiss(animated: true, completion:{
                self.costValue = self.stepperCost.countLabel.text!
                self.lblCost.text = "\(self.costValue) \("/ Km")"
                self.updateReqObj.cost = self.costValue
                
                self.SeatValue = self.stepperSeats.countLabel.text!
                self.lblSeatLeft.text = self.SeatValue
                self.updateReqObj.seatLeft = self.SeatValue
            })
        })
    }
    
    func proceed()
    {
        updateReqObj.cost_type = updateData?.costType
        updateReqObj.poolCategory = updateData?.poolCategory
        updateReqObj.poolID = updateData?.poolID
        updateReqObj.poolShared = updateData?.poolShared
        updateReqObj.rideType = updateData?.rideType
        updateReqObj.userID = updateData?.createdBy
        updateReqObj.email = updateData?.ownerEmail
        updateReqObj.via_lat = ""
        updateReqObj.via_lon = ""
        updateReqObj.rideMessage = txtComment.text
        
        if(updateReqObj.from_location != updateReqObj.to_location){
            showIndicator("Updating Ride..")
            CommonRequestor().getDistanceDuration(updateReqObj.from_location!, destination: updateReqObj.to_location!, success: { (success, object) in
                if let data = object as? NSDictionary{
                    let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                    let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                    self.updateReqObj.duration = ""
                    self.updateReqObj.distance = ""
                        if((elements["status"] as! String) == "OK")
                        {
                            let distance = ((elements["distance"] as! NSDictionary)["text"])!
                            let duration = ((elements["duration"] as! NSDictionary)["text"])!
                        self.updateReqObj.duration = duration as? String
                        self.updateReqObj.distance = distance as? String
                    }
                }
                RideRequestor().updateRide(self.updateReqObj, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Update Ride", message: "Your ride has been updated successfully.", okAction: {
                            let poolID = (self.updateData?.poolID)!
                            
                            showIndicator("Fetching Ride Details")
                            RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                hideIndicator()
                                if(success){
                                    let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                    let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                    //let vc = RideDetailViewController()
                                    vc.hidesBottomBarWhenPushed = true
                                    vc.edgesForExtendedLayout = UIRectEdge()
                                    vc.data = object as? RideDetail
                                    if let poolScope = (object as? RideDetail)?.ownerScope {
                                        if poolScope == "PUBLIC" {
                                            vc.triggerFrom = "ride"
                                        } else {
                                            vc.triggerFrom = "place"
                                        }
                                    }
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }) { (error) in
                                hideIndicator()
                            }
                        })
                    }
                    else{
                        if((object as! BaseModel).code == "46") {
                            AlertController.showToastForError("Please update ride time.")
                        } else if((object as! BaseModel).code == "48") {
                            AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to update on the same time.")
                        } else if((object as! BaseModel).code == "49") {
                            AlertController.showAlertFor("Update Ride", message: "Sorry, you can not update travel with, if members are joined or pending.")
                        } else {
                            AlertController.showToastForError((object as! BaseModel).message!)
                        }
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
                }, failure: { (error) in
                    showIndicator("Updating Ride..")
                    RideRequestor().updateRide(self.updateReqObj, success: { (success, object) in
                        hideIndicator()
                        if(success == true){
                            AlertController.showAlertFor("Update Ride", message: "Your ride has been updated successfully.", okAction: {
                                let poolID = (self.updateData?.poolID)!
                                
                                showIndicator("Fetching Ride Details")
                                RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                                    hideIndicator()
                                    if(success){
                                        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                                        let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                                        //let vc = RideDetailViewController()
                                        vc.hidesBottomBarWhenPushed = true
                                        vc.edgesForExtendedLayout = UIRectEdge()
                                        vc.data = object as? RideDetail
                                        if let poolScope = (object as? RideDetail)?.ownerScope {
                                            if poolScope == "PUBLIC" {
                                                vc.triggerFrom = "ride"
                                            } else {
                                                vc.triggerFrom = "place"
                                            }
                                        }
                                        self.navigationController?.pushViewController(vc, animated: true)
                                    }
                                }) { (error) in
                                    hideIndicator()
                                }
                            })
                        }
                        else{
                            if((object as! BaseModel).code == "46") {
                                AlertController.showAlertFor("Update Ride", message: "Please update ride time.")
                            } else if((object as! BaseModel).code == "48") {
                                AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to update on the same time.")
                            } else if((object as! BaseModel).code == "49") {
                                AlertController.showAlertFor("Update Ride", message: "Sorry, you can not update travel with, if members are joined or pending.")
                            } else {
                                AlertController.showToastForError((object as! BaseModel).message!)
                            }
                        }
                        }, failure: { (error) in
                            hideIndicator()
                    })
            })
        }
        else{
            AlertController.showAlertFor("Update Ride", message: "Please choose different locations for source and destination.")
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        valueSelected = pickerData[row] as String
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        let titleData = pickerData[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: FONT_NAME.BOLD_FONT, size:  14.0)!])
        pickerLabel.textAlignment = .center
        pickerLabel.attributedText = myTitle
        return pickerLabel
    }
    
    //MARK:- Get Vehicle Default Details Service Call.
    func getDefaultVehicle()
    {
        RideRequestor().getDefaultVehicle({ (success, object) in
            if (success) {
                self.vehicleDefaultDetails = object as! VehicleDefaultResponse
                if let vehicleType = self.vehicleDefaultDetails.data?.first?.vehicleType{
                    if(vehicleType.lowercased() == "bike") {
                        self.iconVeh.image = UIImage(named: "veh_bike")
                    } else if(vehicleType.lowercased() == "auto") {
                        self.iconVeh.image = UIImage(named: "auto")
                    } else if(vehicleType.lowercased() == "car") {
                        self.iconVeh.image = UIImage(named: "veh_car")
                    } else if(vehicleType.lowercased() == "cab") {
                        self.iconVeh.image = UIImage(named: "cab")
                    } else if(vehicleType.lowercased() == "suv") {
                        self.iconVeh.image = UIImage(named: "suv")
                    } else if(vehicleType.lowercased() == "tempo") {
                        self.iconVeh.image = UIImage(named: "tempo")
                    } else if(vehicleType.lowercased() == "bus") {
                        self.iconVeh.image = UIImage(named: "bus")
                    }
                    self.lblVehModel.text = vehicleType.capitalized
                    let poolJoinedMembers = Int((self.updateData?.poolJoinedMembers)!)
                    let seatLeft = Int((self.vehicleDefaultDetails.data?.first?.vehicleCapacity)!)!-1
                    self.SeatValue = String(seatLeft-poolJoinedMembers!)
                    self.lblSeatLeft.text = self.SeatValue
                    var cost = "0"
                    var maxAmt = 0
                    if (self.lblVehModel.text?.lowercased() == "bike"){
                        cost = self.vehicleConfig.bike!
                        maxAmt = Int(self.vehicleConfig.bike!)!
                    }else if (self.lblVehModel.text?.lowercased() == "car"){
                        cost = self.vehicleConfig.car!
                        maxAmt = Int(self.vehicleConfig.car!)!
                    }else if (self.lblVehModel.text?.lowercased() == "suv"){
                        cost = self.vehicleConfig.suv!
                        maxAmt = Int(self.vehicleConfig.suv!)!
                    }else{
                        cost = "0"
                        maxAmt = 99999
                    }
                    self.costValue = cost
                    self.lblCost.text = "\(cost) \("/ Km")"
                    self.stepperCost.maximum = Int32(maxAmt)
                    self.stepperCost.value = Int32(cost)!
                    self.stepperCost.countLabel.text = "\(cost)"
                    self.stepperSeats.maximum = Int32(self.SeatValue)!
                    self.stepperSeats.value = Int32((self.updateData?.seatsLeft)!)!
                    self.stepperSeats.countLabel.text = "\(self.updateData?.seatsLeft)"
                    self.updateReqObj.seatLeft = self.SeatValue
                    self.updateReqObj.vehregno = self.vehicleDefaultDetails.data?.first?.vehicleRegistrationNumber
                    self.updateReqObj.cost = cost
                }
            }
        }) { (error) in
        }
    }
    
    //MARK:- Get Vehicle Details Service Call.
    func getConfigData()
    {
        var configType = ""
        var groupId = ""
        if(triggerFrom == "place") {
            configType = "workplace"
            groupId = (updateData?.ownerScope)!
        } else {
            configType = "ride"
            groupId = ""
        }
        RideRequestor().getVehicleConfig(configType, groupID: groupId, success: { (success, object) in
            self.vehicleConfig = object as! VehicleConfigResponse
            var cost = "0"
            var maxAmt = 0
            if (self.lblVehModel.text?.lowercased() == "bike"){
                cost = self.vehicleConfig.bike!
                maxAmt = Int(self.vehicleConfig.bike!)!
            }else if (self.lblVehModel.text?.lowercased() == "car"){
                cost = self.vehicleConfig.car!
                maxAmt = Int(self.vehicleConfig.car!)!
            }else if (self.lblVehModel.text?.lowercased() == "suv"){
                cost = self.vehicleConfig.suv!
                maxAmt = Int(self.vehicleConfig.suv!)!
            }else{
                cost = "0"
                maxAmt = 99999
            }
            //self.lblCost.text = "\(cost) \("/ Km")"
            self.stepperCost.maximum = Int32(maxAmt)
            self.stepperCost.value = Int32(cost)!
            self.stepperCost.countLabel.text = "\(cost)"
            self.stepperSeats.maximum = Int32((self.updateData?.seatsLeft)!)!
            self.stepperSeats.value = Int32((self.updateData?.seatsLeft)!)!
            //self.getDefaultVehicle()
            
        }) { (error) in
        }
    }
    
    //    func keyboardWillShow(notification:NSNotification){
    //        self.scrollView.scrollEnabled = true
    //        var userInfo = notification.userInfo!
    //        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
    //        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
    //
    //        var contentInset:UIEdgeInsets = self.scrollView.contentInset
    //        contentInset.bottom = keyboardFrame.size.height + 20
    //
    //        self.scrollView.contentInset = contentInset
    //        self.scrollView.scrollIndicatorInsets = contentInset
    //
    //        var aRect : CGRect = self.view.frame
    //        aRect.size.height -= keyboardFrame.height
    //        if let activeFieldPresent = selectedTxtField
    //        {
    //            if (!CGRectContainsPoint(aRect, activeFieldPresent.frame.origin))
    //            {
    //                self.scrollView.scrollRectToVisible(activeFieldPresent.frame, animated: true)
    //            }
    //        }
    //
    //        keyBoardDidMoveUp(CGRectGetMinY(keyboardFrame))
    //    }
    //
    //    func keyboardWillHide(notification:NSNotification){
    //
    //        var contentInset:UIEdgeInsets = self.scrollView.contentInset
    //        contentInset.bottom = 0
    //        self.scrollView.contentInset = contentInset
    //    }
    //
    //    func keyBoardDidMoveUp (keyboardMinY: CGFloat) {
    //
    //    }
}
