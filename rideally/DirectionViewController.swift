//
//  DirectionViewController.swift
//  rideally
//
//  Created by Sarav on 21/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class DirectionViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {

    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var mapView:GMSMapView!
    //var mapCenterPinImageVIew = UIImageView()
    
    var fromLocation: String?
    var toLocation: String?
    
    var fromLat: String?
    var fromLong: String?    
    var toLat: String?
    var toLong: String?
    
    let appDelegate: AppDelegate = sharedAppDelegate()!
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //mapCenterPinImageVIew.image = UIImage(imageLiteral: "btn_set_location")
        
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;

        title = "Direction"
        self.setupGoogleMaps()
        self.getDirections()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.locationCoord == nil {
            self.getCurrentLocation()
        }
    }
    
    func setupGoogleMaps () {
        
        if let coord = self.locationCoord {
            let camera = GMSCameraPosition.camera(withLatitude: coord.latitude,
                                                              longitude: coord.longitude, zoom: 16)
            self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        } else {
            let camera = GMSCameraPosition.camera(withLatitude: 20.593684, longitude: 78.962880, zoom: 16)
            self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        }
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.mapView)
        self.view.sendSubview(toBack: self.mapView)
        
        let dict = ["mapView": mapView]
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        // pin layout
        //mapCenterPinImageVIew.translatesAutoresizingMaskIntoConstraints = false
        //self.mapView.addSubview(self.mapCenterPinImageVIew)
        
        //self.mapView.addConstraint(NSLayoutConstraint(item: self.mapCenterPinImageVIew, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.CenterX, multiplier: 1.0, constant: 0))
        //self.mapView.addConstraint(NSLayoutConstraint(item: self.mapCenterPinImageVIew, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self.mapView, attribute: NSLayoutAttribute.CenterY, multiplier: 1.0, constant: -(mapCenterPinImageVIew.image?.size.height)!/2.0 + 10))
    }
    
    func getCurrentLocation () {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        
        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .denied {
                //                AlertController.showAlertForMessage("You have denied access to your location, please enable it in settings.")
                callLocationServiceEnablePopUp()
                return
            }
        }
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else {
            print("location service not enabled")
        }
    }
    
    
    func callLocationServiceEnablePopUp() -> Void
    {
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedWhenInUse, .restricted, .denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order to be notified, please open this app's settings and set location access to 'Always'.",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        default:
            break
            
            
        }
    }

    
    
    // MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationCoord = manager.location!.coordinate
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
        } else if status == .denied {
            //AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
            callLocationServiceEnablePopUp()
        }
    }
    
    // MARK:- Google Map Delegates
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.locationCoord = position.target
    }
    
    var selectedRoute: Dictionary<NSObject, AnyObject>!
    var overviewPolyline: Dictionary<NSObject, AnyObject>!
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var routePolyline: GMSPolyline!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!

    func getDirections()
    {
        var url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(fromLocation!)&destination=\(toLocation!)&mode=driving"
//        url = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        DispatchQueue.main.async(execute: { () -> Void in
            let directionsData = try? Data(contentsOf: URL(string: url)!)
            
            do{
                let dictionary: Dictionary<NSObject, AnyObject> = try JSONSerialization.jsonObject(with: directionsData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<NSObject, AnyObject>
                
                let status = dictionary["status" as NSObject] as! String
                
                if status == "OK" {
                    self.selectedRoute = (dictionary["routes" as NSObject] as! Array<Dictionary<NSObject, AnyObject>>)[0]
                    self.overviewPolyline = self.selectedRoute["overview_polyline" as NSObject] as! Dictionary<NSObject, AnyObject>
                    
                    let legs = self.selectedRoute["legs" as NSObject] as! Array<Dictionary<NSObject, AnyObject>>
                    
                    let startLocationDictionary = legs[0]["start_location" as NSObject] as! Dictionary<NSObject, AnyObject>
                    self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat" as NSObject] as! Double, startLocationDictionary["lng" as NSObject] as! Double)
                    
                    let endLocationDictionary = legs[legs.count - 1]["end_location" as NSObject] as! Dictionary<NSObject, AnyObject>
                    self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat" as NSObject] as! Double, endLocationDictionary["lng" as NSObject] as! Double)
                }
                
                
                //self.mapView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
                let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
                let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
                let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
                let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
                self.mapView.camera = camera

                self.originMarker = GMSMarker(position: self.originCoordinate)
                self.originMarker.map = self.mapView
                self.originMarker.icon = GMSMarker.markerImage(with: UIColor.green)
                self.originMarker.title = self.fromLocation
                
                self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
                self.destinationMarker.map = self.mapView
                self.destinationMarker.icon = GMSMarker.markerImage(with: UIColor.red)
                self.destinationMarker.title = self.toLocation
                
                let route = self.overviewPolyline["points" as NSObject] as! String
                
                let path: GMSPath = GMSPath(fromEncodedPath: route)!
                self.routePolyline = GMSPolyline(path: path)
                self.routePolyline.strokeColor = Colors.GREEN_COLOR
                self.routePolyline.strokeWidth = 5.0
                self.routePolyline.map = self.mapView

            }
            catch{
                
            }
            
        })
    }
}
