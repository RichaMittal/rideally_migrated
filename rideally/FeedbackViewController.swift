//
//  FeedbackViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 9/19/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
class FeedbackViewController: BaseScrollViewController,RateViewDelegate {
    
    var feedbackTextBox = UITextField()
    var rateValue : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Feedback"
        viewsArray.append(createFeedbackViewSubviews())
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        let likeControl:FBSDKLikeControl = FBSDKLikeControl()
        likeControl.objectID = "https://www.facebook.com/FacebookDevelopers"
        likeControl.likeControlStyle = FBSDKLikeControlStyle.boxCount
        likeControl.frame = CGRect(x: 200, y: 350, width: 30, height: 15)
        likeControl.likeControlHorizontalAlignment = .left
        self.view.addSubview(likeControl)
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func rateView(_ rateView:RateView!, ratingDidChange rating: CFloat) {
        rateValue = Int(rating)
    }
    
    func createFeedbackViewSubviews() ->  SubView{
        
        let descriptionLabel = UILabel()
        descriptionLabel.font = normalFontWithSize(14)
        descriptionLabel.backgroundColor = UIColor.clear
        descriptionLabel.textColor = Colors.GRAY_COLOR
        descriptionLabel.text="HELP US TO SERVE YOU BETTER"
        descriptionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        
        let rateView = RateView()
        rateView.notSelectedImage=UIImage(named: "Star_Empty")
        //self.rateView.halfSelectedImage = [UIImage imageNamed:@"kermit_half.png"];
        rateView.fullSelectedImage = UIImage(named: "Star_Filled")
        rateView.delegate = self
        rateView.rating = 0
        rateView.editable = true
        rateView.maxRating = 5
        
        feedbackTextBox.returnKeyType = UIReturnKeyType.next
        feedbackTextBox.delegate = self
        feedbackTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        feedbackTextBox.placeholder = "Enter your Comment/Feedback"
        feedbackTextBox.returnKeyType = UIReturnKeyType.done
        feedbackTextBox.font = normalFontWithSize(15)!
        feedbackTextBox.textColor = Colors.GRAY_COLOR
        
        let btnSubmit = UIButton()
        btnSubmit.backgroundColor = Colors.GRAY_COLOR
        btnSubmit.setTitle("SUBMIT", for: UIControlState())
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnSubmit.titleLabel?.font = boldFontWithSize(15)
        btnSubmit.translatesAutoresizingMaskIntoConstraints = false
        btnSubmit.addTarget(self, action: #selector(submitButtonPressed(_:)), for: .touchDown)
        
        let  appstore =   UIButton()
        appstore.setImage(UIImage(named:"appstore"), for:UIControlState())
        appstore.translatesAutoresizingMaskIntoConstraints = false
        appstore.addTarget(self, action: #selector(appStore_clicked), for: .touchDown)
        
        let  fblike =   UIButton()
        fblike.setImage(UIImage(named:"fblike"), for:UIControlState())
        fblike.translatesAutoresizingMaskIntoConstraints = false
        fblike.addTarget(self, action: #selector(fbLike_clicked), for: .touchDown)
        fblike.isHidden = true
        
        let rateUsLabel = UILabel()
        rateUsLabel.font = normalFontWithSize(14)
        rateUsLabel.backgroundColor = UIColor.clear
        rateUsLabel.textColor = Colors.GRAY_COLOR
        rateUsLabel.text="RATE US"
        
        let likeUsLabel = UILabel()
        likeUsLabel.font = normalFontWithSize(14)
        likeUsLabel.backgroundColor = UIColor.clear
        likeUsLabel.textColor = Colors.GRAY_COLOR
        likeUsLabel.text="LIKE US"
        
        let  feedbackView = UIView()
        feedbackView.addSubview(descriptionLabel)
        feedbackView.addSubview(feedbackTextBox)
        feedbackView.addSubview(btnSubmit)
        feedbackView.addSubview(appstore)
        feedbackView.addSubview(fblike)
        feedbackView.addSubview(rateUsLabel)
        feedbackView.addSubview(likeUsLabel)
        feedbackView.addSubview(rateView)
        
        rateView.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        feedbackTextBox.translatesAutoresizingMaskIntoConstraints = false
        rateUsLabel.translatesAutoresizingMaskIntoConstraints = false
        likeUsLabel.translatesAutoresizingMaskIntoConstraints = false
        
        let subViewsDict = ["description": descriptionLabel, "rateview": rateView, "feedback":feedbackTextBox, "submit":btnSubmit, "appstore": appstore, "fblike": fblike, "rateus": rateUsLabel, "likeus": likeUsLabel]
        feedbackView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[description(40)]-30-[rateview(30)]-30-[feedback(30)]-35-[submit(35)]-35-[appstore(30)]-15-[rateus(15)]", options:[], metrics:nil, views: subViewsDict))
        feedbackView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[description]-10-|", options:[], metrics:nil, views: subViewsDict))
        feedbackView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[submit]-10-|", options: [], metrics: nil, views: ["submit": btnSubmit]))
        feedbackView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[appstore]-20-[fblike(==appstore)]-10-|", options: [], metrics: nil, views: subViewsDict))
        feedbackView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-60-[rateus]-60-[likeus(==rateus)]-10-|", options: [], metrics: nil, views: subViewsDict))
        
        feedbackView.addConstraint(NSLayoutConstraint(item: rateView, attribute: .left, relatedBy: .equal, toItem: descriptionLabel, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        feedbackView.addConstraint(NSLayoutConstraint(item: rateView, attribute: .width, relatedBy: .equal, toItem: descriptionLabel, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        feedbackView.addConstraint(NSLayoutConstraint(item: feedbackTextBox, attribute: .left, relatedBy: .equal, toItem: rateView, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        feedbackView.addConstraint(NSLayoutConstraint(item: feedbackTextBox, attribute: .width, relatedBy: .equal, toItem: rateView, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        feedbackView.addConstraint(NSLayoutConstraint(item: fblike, attribute: .centerY, relatedBy: .equal, toItem: appstore, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        feedbackView.addConstraint(NSLayoutConstraint(item: fblike, attribute: .height, relatedBy: .equal, toItem: appstore, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        feedbackView.addConstraint(NSLayoutConstraint(item: likeUsLabel, attribute: .centerY, relatedBy: .equal, toItem: rateUsLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        feedbackView.addConstraint(NSLayoutConstraint(item: likeUsLabel, attribute: .height, relatedBy: .equal, toItem: rateUsLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        
        let feedbackSubViewInstance = SubView()
        feedbackSubViewInstance.view = feedbackView
        feedbackSubViewInstance.padding = UIEdgeInsetsMake(10, 0, 0, 0)
        feedbackSubViewInstance.heightConstraint = NSLayoutConstraint(item: feedbackView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 400)
        
        return feedbackSubViewInstance
    }
    
    func appStore_clicked()
    {
        UIApplication.shared.openURL(URL(string: "itms://itunes.apple.com/us/app/rideally/id1143210362?mt=8")!)
    }
    
    func fbLike_clicked()
    {
    }
    func submitButtonPressed(_ Sender:UIButton) -> Void {
        callFunctionWhenDoneButtonisPressed()
    }
    
    func sendFeedbackRequest() -> Void {
        showIndicator("Loading...")
        let reqObj = FeedbackRequest()
        reqObj.user_id = UserInfo.sharedInstance.userID
        reqObj.name = UserInfo.sharedInstance.fullName
        reqObj.email = UserInfo.sharedInstance.email
        reqObj.subject = "Feedback From IOS App"
        reqObj.feedbackMsg = feedbackTextBox.text
        reqObj.rating = String(self.rateValue)
        FeedbackRequestor().sendFeedbackRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! FeedbackResponse).code == "1800"
            {
                
                AlertController.showAlertFor("RideAlly", message: "Thank you for sending your valuable feedback.", okButtonTitle: "ok", okAction: {
                    
                    let ins = UIApplication.shared.delegate as! AppDelegate
                    ins.gotoHome()
                    //TODO: move to home
                    //self.navigationController?.pushViewController(DashBoardViewController(), animated: true)
                })
            }
            else{
                AlertController.showAlertFor("RideAlly", message: "Sorry, there is some technical problem while sending feedback, please try later.", okButtonTitle: "ok", okAction: {
                    _ = self.navigationController?.popViewController(animated: false)
                })
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
    
    func callFunctionWhenDoneButtonisPressed() -> Void {
        if  self.rateValue == 0{
            AlertController.showToastForError("Please Rate our services.")
        }
        else if  feedbackTextBox.text?.characters.count == 0{
            AlertController.showToastForError("Message field cannot be left blank.")
        }
        else
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                sendFeedbackRequest()
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == feedbackTextBox)
        {
            callFunctionWhenDoneButtonisPressed()
            feedbackTextBox.resignFirstResponder()
        }
        
        return true
    }
}
