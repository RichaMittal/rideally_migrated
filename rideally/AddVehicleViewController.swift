//
//  AddVehicleViewController.swift
//  rideally
//
//  Created by Raghu on 06/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class AddVehicleViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource,VehicleListCellDelegate {
    
    var  indexPathValue : Int?
    let tableView = UITableView()
    var addButtonView = UIButton()
    var tableHeightConstraint: NSLayoutConstraint?
    var tableTopConstraint: NSLayoutConstraint?
    var isFromWp : String?
    let rowHt: CGFloat = 100
    var isWpInsuranceStatus = false
    
    var dataSource = [VehicleData](){
        didSet{
            if(dataSource.count > 0){
                tableView.isHidden = false
                view.removeEmptyView()
                tableHeightConstraint?.constant = (CGFloat(dataSource.count) * rowHt)
                tableView.reloadData()
            }
            else{
                showEmptyView("No vehicle List Found")
            }
        }
    }
    
    func getButtonSubViews(_ titleLabelValue: String) ->  UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(16)
        buttonObject.backgroundColor = Colors.GREEN_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitle(titleLabelValue, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: .highlighted)
        return buttonObject
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Vehicles"
        self.tableView.tableFooterView = UIView()
        viewsArray.append(createTableView())
        addBottomView(getBottomView())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAllVehicleList()
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        self.addButtonView = getButtonSubViews("ADD NEW VEHICLE")
        self.addButtonView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(self.addButtonView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn]|", options: [], metrics: nil, views: ["btn":self.addButtonView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":self.addButtonView]))
        
        let sub = SubView()
        sub.view = view
        sub.view?.backgroundColor = UIColor.green
        
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func createTableView() -> SubView
    {
        let view = UIView()
        self.indexPathValue = 0
        
        tableView.separatorColor = Colors.GRAY_COLOR
        tableView.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        tableView.register(VehicleListCell.self, forCellReuseIdentifier: "VehicleListCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table]|", options: [], metrics: nil, views: ["table":tableView,"button":self.addButtonView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":tableView]))
        
        tableHeightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 0)
        
        let subview = SubView()
        subview.view = view
        subview.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        subview.heightConstraint = tableHeightConstraint!
        return subview
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexPathValue = indexPath.row
        self.saveDefaultVehicleDetails(dataSource[indexPath.row])
        self.sendDefaultVehicleRequest()
        self.tableView .reloadData()
    }
    
    func saveDefaultVehicleDetails( _ data:VehicleData) -> Void
    {
        UserInfo.sharedInstance.vehicleSeqID = data.vehicleSeqId!
        UserInfo.sharedInstance.vehicleType = data.vehicleType!
        UserInfo.sharedInstance.vehicleModel = data.vehicleModel!
        UserInfo.sharedInstance.vehicleRegNo = data.RegNo!
        UserInfo.sharedInstance.vehicleSeatCapacity = data.capacity!
        UserInfo.sharedInstance.vehicleKind = data.vehicleKind!
        UserInfo.sharedInstance.vehicleOwnerName = data.vehicleOwnerName!
        UserInfo.sharedInstance.insurerName = data.insurerName!
        UserInfo.sharedInstance.insuredCmpnyName = data.insuredCmpnyName!
        UserInfo.sharedInstance.insuranceId = data.insuranceId!
        UserInfo.sharedInstance.insuranceExpiryDate = data.insuranceExpiryDate!
        UserInfo.sharedInstance.insUploadStatus = data.insUploadStatus!
        UserInfo.sharedInstance.insUrl = data.insUrl!
        UserInfo.sharedInstance.rcUploadStatus = data.rcUploadStatus!
        UserInfo.sharedInstance.rcUrl = data.rcUrl!
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if dataSource.count  > 0
        {
            return dataSource.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHt
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VehicleListCell") as! VehicleListCell
        cell.data = dataSource[indexPath.row]
        //print("the val is",self.indexPathValue,indexPath.row)
        if  self.indexPathValue == indexPath.row
        {
            cell.isDefaultVehicle.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
            cell.isDefaultVehicle.isSelected = true
        }
        else
        {
            cell.isDefaultVehicle.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
            cell.isDefaultVehicle.isSelected = false
        }
        
        cell.cellDelegate=self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.WHITE_COLOR
    }
    
    func showEmptyView(_ msg: String)
    {
        tableHeightConstraint?.constant = 0
        tableView.isHidden = true
        let attributedString = NSMutableAttributedString(string: msg)
        view.addEmptyImageViewWithText(nil, attributedText: attributedString, imageNamed: "", actionHandler: nil)
    }
    
    func sendDefaultVehicleRequest() -> Void {
        
        if  UserInfo.sharedInstance.userID.characters.count > 0 && UserInfo.sharedInstance.vehicleSeqID.characters.count > 0
        {
            let reqObj = SetDefaultVehicleRequest()
            showLoadingIndicator("Loading...")
            reqObj.userID = UserInfo.sharedInstance.userID
            reqObj.vehicleSeqID = UserInfo.sharedInstance.vehicleSeqID
            
            ProfileRequestor().sendSetDefaultVehicleRequest(reqObj, success:{ (success, object) in
                self.hideLoadingIndiactor()
                AlertController.showToastForInfo((object as! SetDefaultVehicleResponse).message!)
                if self.isFromWp != nil && self.isFromWp == "1" {
                    ISVEHICLEUPDATED = true
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }){ (error) in
                self.hideLoadingIndiactor()
                AlertController.showAlertForError(error)
            }
        }
        else
        {
            AlertController.showToastForError("Invalid UserID/VehicleSeqID provided in the Request")
        }
        
    }
    
    func getAllVehicleList() -> Void {
        
        if  UserInfo.sharedInstance.userID.characters.count > 0
        {
            let reqObj = GetALLVehicleListRequest()
            showLoadingIndicator("Loading...")
            reqObj.userID = UserInfo.sharedInstance.userID
            
            
            ProfileRequestor().getAddVehicleListData(reqObj, success:{ (success, object) in
                self.hideLoadingIndiactor()
                
                if (object as! GetALLVehicleListResponse).code == "0"
                {
                    
                    if (object as! GetALLVehicleListResponse).dataObj?.first != nil
                    {
                        self.tableView.isHidden = false
                        self.dataSource = (object as! GetALLVehicleListResponse).dataObj!
                        self.tableView.reloadData()
                    }
                }
                else
                {
                    self.showEmptyView("No Vehicle List Found")
                }
                
            }){ (error) in
                self.hideLoadingIndiactor()
                AlertController.showAlertForError(error)
            }
        }
        else
        {
            AlertController.showToastForError("Invalid UserID provided in the Request")
        }
    }
    
    func deleteSelectedVehicleList() -> Void {
        
        if UserInfo.sharedInstance.vehicleSeqID.characters.count > 0
        {
            let reqObj = DeleteSelecteVehicleRequest()
            showLoadingIndicator("Loading...")
            
            reqObj.seqID = UserInfo.sharedInstance.vehicleSeqID
            
            ProfileRequestor().deleteVehicleListData(reqObj, success:{ (success, object) in
                self.hideLoadingIndiactor()
                
                if (object as! DeleteSelecteVehicleResponse).code == "212"
                {
                    AlertController.showToastForInfo("Vehicle deleted successfully")
                    if self.isFromWp != nil && self.isFromWp == "1" {
                        ISVEHICLEUPDATED = true
                    }
                    self.getAllVehicleList()
                }
                else
                {
                    if (object as! DeleteSelecteVehicleResponse).code == "211" {
                        AlertController.showToastForError("Sorry, you are not able to delete vehicle information if there are any active rides.")
                    } else {
                        AlertController.showToastForError("Error in deletion")
                    }
                }
                
            }){ (error) in
                self.hideLoadingIndiactor()
                AlertController.showAlertForError(error)
            }
        }
    }
    
    func isDefaultButtonPressedEventCalled() -> Void
    {
        var indexval = 0
        
        for item in self.dataSource
        {
            if  UserInfo.sharedInstance.vehicleSeqID == item.vehicleSeqId
            {
                indexval = self.dataSource.index(of: item)!
                break
            }
        }
        
        sendDefaultVehicleRequest()
        let rowToSelect:IndexPath = IndexPath(row: indexval, section: 0);
        self.tableView.selectRow(at: rowToSelect, animated: true, scrollPosition: UITableViewScrollPosition.none);
        self.tableView(self.tableView, didSelectRowAt: rowToSelect);
    }
    
    func updateButtonPresseEven() {
        let vc = RegisterNewVehicleController()
        vc.isUpdateReqneedtobeSend = "1"
        vc.isWpInsuranceStatus = isWpInsuranceStatus
        if (self.isFromWp != nil && self.isFromWp == "1") {
            var indexval = 0
            
            for item in self.dataSource
            {
                if  UserInfo.sharedInstance.vehicleSeqID == item.vehicleSeqId
                {
                    indexval = self.dataSource.index(of: item)!
                    break
                }
            }
            let rowToSelect:IndexPath = IndexPath(row: indexval, section: 0)
            if(rowToSelect.row == 0) {
                vc.isFromWp = "1"
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func deleteButtonPressEvent(_ sender: UIButton) {
        
        AlertController.showAlertFor("Delete Vehicle", message: "Would you really like to delete this vehicle?", okButtonTitle: "YES", okAction: {
            self.deleteSelectedVehicleList()
        }, cancelButtonTitle: "NO") {
        }
    }
    
    func buttonPressed (_ sender: AnyObject)
    {
        let vc = RegisterNewVehicleController()
        vc.isUpdateReqneedtobeSend = "0"
        vc.isWpInsuranceStatus = isWpInsuranceStatus
        if (self.isFromWp != nil && self.isFromWp == "1") {
            vc.isFromWp = "1"
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
