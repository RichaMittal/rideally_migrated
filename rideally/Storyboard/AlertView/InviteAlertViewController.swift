//
//  InviteAlertViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 12/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI

class InviteAlertViewController: UIViewController,ABPeoplePickerNavigationControllerDelegate {
    var addressBookController = ABPeoplePickerNavigationController()
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    var inviteFlag = ""
    @IBOutlet weak var alertMessage: UILabel!
    var poolID: String?
    var wpKey: String?
    var wpName: String?
    var triggerFrom = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func messageButtonClicked(_ sender: AnyObject) {
        //self.dismissViewControllerAnimated(true) {
        //            let msg = "YOUR MSG"
        //            let urlWhats = "whatsapp://send?text=\(msg)"
        //            if let urlString = urlWhats.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
        //                if let whatsappURL = NSURL(string: urlString) {
        //                    if UIApplication.sharedApplication().canOpenURL(whatsappURL) {
        //                        UIApplication.sharedApplication().openURL(whatsappURL)
        //                    } else {
        //                        // Cannot open whatsapp
        //                    }
        //                }
        //            }
        
        //            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
        //            if #available(iOS 9.0, *) {
        //                let vc : MessageViewController = storyboard.instantiateViewControllerWithIdentifier("MessageViewController") as! MessageViewController
        //                vc.poolID = self.poolID
        //                vc.triggerFrom = "rideDetail"
        //                //vc.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext //All objects and view are transparent
        //                self.navigationController?.pushViewController(vc, animated: true)
        //            } else {
        //                // Fallback on earlier versions
        //            }
        if(triggerFrom == "" || triggerFrom == "allRides" || triggerFrom == "createRide" || triggerFrom == "wpMap" || triggerFrom == "wpRide" || (inviteFlag != "" && inviteFlag == "wpMap")) {
            self.dismiss(animated: true, completion: nil)
        }
            //self.callAddressBookForPressedButtonObject()
            //return
        //}
        // return
        // self.navigationController?.pushViewController(vc, animated: true)
        //}
    }
    
    @IBAction func whatsAppButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true) {
            self.sendTextMessageButtonTapped(sender)
        }
    }
    
    func sendTextMessageButtonTapped(_ sender: UIButton) {
        let name = UserInfo.sharedInstance.firstName
        var urlString = ""
        if(wpName != nil && wpName != "") {
            urlString = "\(name) \("wants you to travel together with colleagues in secured place,") \(wpName!)\(". Do join it at") \(SERVER)\("/workplace/")\(wpKey!)"
        } else {
            urlString = name + " has invited you to join the ride on RideAlly. Please join ride at "+SERVER+"ride/ridedetail?pool_id=" + "\(poolID ?? "")"
        }
        let urlStringEncoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let url  = URL(string: "whatsapp://send?text=\(urlStringEncoded!)")
        
        if UIApplication.shared.canOpenURL(url!) {
            UIApplication.shared.openURL(url!)
        }
        
    }
    
    //In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "inviteMessageSegue" {
            //if #available(iOS 9.0, *) {
            let authorizationStatus = ABAddressBookGetAuthorizationStatus()
            
            switch authorizationStatus
            {
            case .denied, .restricted:
                displayCantAddContactAlert()
            case .authorized:
                contactListArray.removeAllObjects()
                mobileMutArr.removeAllObjects()
                let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
                let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                for record:ABRecord in contactList as [AnyObject]{
                    let contactPerson: ABRecord = record
                    let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                    if(contactName != "") {
                        var personNumber = ""
                        let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                        let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                        if(phones != "")
                        {
                            if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                            {
                                if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                {
                                    let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                    personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                    let areaCode = personNumber.characters.count-10
                                    let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                    if(areaCode > 0) {
                                        personNumber = personNumber.substring(from: startIndex)
                                    }
                                }
                            } else {
                                continue
                            }
                        }
                        mobileMutArr.add(personNumber)
                        let nameString = "\(contactName) \(personNumber)"
                        contactListArray.add(nameString)
                    }
                }
                
                let vc = segue.destination as! MessageViewController
                vc.contactListArray = self.contactListArray
                vc.phoneMutArr = self.mobileMutArr
                vc.poolID = self.poolID
                vc.triggerFrom = triggerFrom
            case .notDetermined:
                let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
                ABAddressBookRequestAccessWithCompletion(addressBookRef)
                {
                    (granted: Bool, error: Error?) in
                    DispatchQueue.main.async
                    {
                        if !granted
                        {
                            //print("Just denied")
                            self.displayCantAddContactAlert()
                        } else
                        {
                            self.contactListArray.removeAllObjects()
                            self.mobileMutArr.removeAllObjects()
                            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                            for record:ABRecord in contactList as [AnyObject]{
                                let contactPerson: ABRecord = record
                                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                                if(contactName != "") {
                                    var personNumber = ""
                                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                                    if(phones != "")
                                    {
                                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                        {
                                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                            {
                                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                                let areaCode = personNumber.characters.count-10
                                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                                if(areaCode > 0) {
                                                    personNumber = personNumber.substring(from: startIndex)
                                                }
                                            }
                                        } else {
                                            continue
                                        }
                                    }
                                    self.mobileMutArr.add(personNumber)
                                    let nameString = "\(contactName) \(personNumber)"
                                    self.contactListArray.add(nameString)
                                }
                            }
                            
                            let vc = segue.destination as! MessageViewController
                            vc.contactListArray = self.contactListArray
                            vc.phoneMutArr = self.mobileMutArr
                            vc.poolID = self.poolID
                            vc.triggerFrom = self.triggerFrom
                        }
                    }
                }
            }
            
            //                } else {
            //                    // Fallback on earlier versions
            //                }
        }    }
    
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBAction func cancelButtonClicked(_ sender: AnyObject) {
        self.dismiss(animated: true) {
        }
    }
    
    func displayCantAddContactAlert() {
//        AlertController.showAlertFor("Cannot Add Contact", message: "You must give the app permission to add the contact first.", okButtonTitle: "Settings", okAction: {
//            self.openSettings()
//            }, cancelButtonTitle: "OK", cancelAction: nil)
                let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                            message: "You must give the app permission to add the contact first.",
                                                            preferredStyle: .alert)
                cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
                    style: .default,
                    handler: { action in
                        self.openSettings()
                }))
                cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
}
