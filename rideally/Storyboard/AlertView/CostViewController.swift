//
//  CostViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 11/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class CostViewController: UIViewController {
    

    @IBOutlet weak var incrementButton: UIButton!
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var decrementButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var seatLabel: UILabel!
    @IBOutlet weak var seatIncrement: UIButton!
    @IBOutlet weak var seatDecrement: UIButton!
    
    var vehicleConfig: VehicleConfigResponse!
    var vehicleDefaultDetails: VehicleDefaultResponse!
    var data: WorkPlace?
    var cost = 0
    var maxCost = 0
    var seat = 0
    var maxSeat = 0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        if let vehType = vehicleDefaultDetails.data?.first?.vehicleType {
            self.vehicleName.text = vehType
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (self.vehicleDefaultDetails.data?.first?.vehicleType?.lowercased() == "bike"){
            self.maxCost = Int(self.vehicleConfig.bike!)!
        }else if (self.vehicleDefaultDetails.data?.first?.vehicleType?.lowercased() == "car"){
            self.maxCost = Int(self.vehicleConfig.car!)!
        }else if (self.vehicleDefaultDetails.data?.first?.vehicleType?.lowercased() == "suv"){
            self.maxCost = Int(self.vehicleConfig.suv!)!
        }else{
            self.maxCost = 99999
        }
        if let vehCapacity = self.vehicleDefaultDetails.data?.first?.vehicleCapacity {
            self.maxSeat = Int(vehCapacity)!-1
            self.seatLabel.text = String(Int(vehCapacity)!-1)
        }
        if maxCost == 99999 {
             self.priceLabel.text = "0"
        } else {
            self.priceLabel.text = String(self.maxCost)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func incrementButtonClicked(_ sender: AnyObject) {
        var local: Int = Int(self.priceLabel.text!)!
        local += 1
        if(local > maxCost) {
            AlertController.showToastForError("Sorry, you can not select more than " + String(maxCost)+".")
        } else {
            self.cost = local
            self.priceLabel.text = String(self.cost)
        }
    }
    
    @IBAction func decrementButtonClicked(_ sender: AnyObject) {
        var local: Int = Int(self.priceLabel.text!)!
        local -= 1
        if(local < 0) {
            AlertController.showToastForError("Sorry, you can not select less than 0.")
        } else {
            self.cost = local
            self.priceLabel.text = String(self.cost)
        }
    }
    
    @IBAction func removeCostView(_ sender: AnyObject) {
        
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "kGetPriceNotification"), object: nil, userInfo: ["cost":self.priceLabel.text!,"vehicleConfig":self.vehicleConfig,"seat":self.seatLabel.text!])
        }
    }
    
    @IBAction func addSeatuBtonClicked(_ sender: AnyObject) {
        var local: Int = Int(self.seatLabel.text!)!
        local += 1
        if(local > maxSeat) {
            AlertController.showToastForError("Seat selection is exceeding its maximum seat available.")
        } else {
            self.seat = local
            self.seatLabel.text = String(self.seat)
        }
    }
    
    @IBAction func decrementSeatButtonClicked(_ sender: AnyObject) {
        var local: Int = Int(self.seatLabel.text!)!
        local -= 1
        if(local < 1) {
            AlertController.showToastForError("Vehicle minimum seatleft should be 1.")
        } else {
            self.seat = local
            self.seatLabel.text = String(self.seat)
        }
    }
}
