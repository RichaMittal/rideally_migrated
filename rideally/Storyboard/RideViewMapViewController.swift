//
//  RideMapViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 07/02/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire

// MARK: - GMSMapViewDelegate
extension RideViewMapViewController: GMSMapViewDelegate,GMSPanoramaViewDelegate {
    
}

class RideViewMapViewController: UIViewController {
    var data: [Ride]?
    var infoWindow : RideWindowInfoView!
    var sourceLoc = ""
    var sourceLat = ""
    var sourceLong = ""
    var destloc = ""
    var destLat = ""
    var destLong = ""
    var poolTaxiBooking = ""
    var poolId = ""
    var rideType = ""
    @IBOutlet weak var mapView: GMSMapView!
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
  
        self.addAllMarker()
    }
    
    func addAllMarker() {
        let count = (self.data?.count)! as Int
        if count == 0 {
            _ = self.navigationController!.popViewController(animated: false)
            return
        }
        
        for index in 0...(count-1){
            
            let ride = self.data![index]
            let camera: GMSCameraPosition =
                GMSCameraPosition.camera(withLatitude: Double(ride.sourceLat!)!, longitude: Double(ride.sourceLong!)!, zoom: 12.0)
            mapView.camera = camera
            let marker = GMSMarker()
            marker.position = camera.target
            marker.snippet = ride.source
            
            if ride.poolRideType == "N" {
                marker.icon = UIImage(named: "marker_ride_green")
            }else{
                marker.icon = UIImage(named: "marker_profile_green")
            }
            
            //marker.appearAnimation = GMSMarkerAnimation
            //marker.userData = ["markerInfo" : ride]
            marker.accessibilityLabel = "\(index)"
            marker.map = self.mapView
        }
    }
    
    
    @objc(mapView:didTapAtCoordinate:) func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
        }
    }
    @objc(mapView:didTapMarker:) func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        //   marker.icon = UIImage(named: "marker_profile_orange")
        
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
        }
        
        infoWindow = Bundle.main.loadNibNamed("RideWindowInfoView", owner: self, options: nil)!.first as! RideWindowInfoView
        infoWindow.frame = CGRect(x: (self.mapView.frame.size.width-340)/2, y: (self.mapView.frame.size.height-80)/2, width: 340, height: 100)
        let index:Int! = Int(marker.accessibilityLabel!)
        let rideObj = self.data![index]
        
        //  let source = (marker.userData as! Ride).source
        infoWindow.name.text = rideObj.ownerName
        
        if rideObj.poolRideType == "N" {
        infoWindow.seatAvailable.text = (rideObj.seatsLeft! == "0") ? "No Seat left" : String(format: "%@%@", rideObj.seatsLeft!,"Seat left")
        }else{
           infoWindow.seatAvailable.text = ""
        }
        
        //infoWindow.profile: UIImageView!
        infoWindow.offerVehicle.text = rideObj.dateTime
        infoWindow.address.text = rideObj.source
        infoWindow.joinButton.isHidden = true
        infoWindow.offerRideButton.isHidden = true
        
        infoWindow.markerWindowButton.addTarget(self, action: #selector(markerWindowButtonTapped), for:.touchUpInside)
        infoWindow.markerWindowButton.tag = index
        
        if rideObj.poolRideType == "O" {
            infoWindow.joinButton.isHidden = false
        } else {
            infoWindow.joinButton.isHidden = false
            infoWindow.offerRideButton.isHidden = false
        }
        infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
        
        infoWindow.offerRideButton.addTarget(self, action: #selector(offerRideButtonTapped), for:.touchUpInside)
        
        infoWindow.center = mapView.center
        print(marker.groundAnchor,"marker groundAnchor point")
        print(marker.infoWindowAnchor,"marker infoWindowAnchor point")
        
        self.mapView.addSubview(infoWindow)
        
        sourceLoc = rideObj.source!
        sourceLat = rideObj.sourceLat!
        sourceLong = rideObj.sourceLong!
        destloc = rideObj.destination!
        destLat = rideObj.destLat!
        destLong = rideObj.destLong!
        poolTaxiBooking = rideObj.poolTaxiBooking!
        poolId = rideObj.poolID!
        rideType = rideObj.poolRideType!
        
        return true
    }
   
    
    func markerWindowButtonTapped(_ button : UIButton){
        
        let rideObj  = self.data![button.tag]
        showIndicator("Fetching Ride Details")
        RideRequestor().getRideDetails(rideObj.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                
                if self.infoWindow != nil {
                    self.infoWindow.removeFromSuperview()
                }
                //TODO: need to load Ride Details. Update and Invite and Delete.
                /*
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewControllerWithIdentifier("WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                vc.data = object as? RideDetail
                //vc.wpData = self.data
                //vc.vehicleConfig = self.vehicleConfig
                //vc.vehicleDefaultDetails = self.vehicleDefaultDetails as VehicleDefaultResponse
                self.navigationController?.pushViewController(vc, animated: true)
                */
                
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    
    // MARK:- JOIN / Offer Button Tap
    func joinButtonTapped () {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            if(UserInfo.sharedInstance.shareMobileNumber == false){
                AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                    self.getPickUpDropLocation("joinRide")
                    }, cancelButtonTitle: "LATER", cancelAction: {
                })
            }
            else{
                getPickUpDropLocation("joinRide")
            }
        }
    }
    
    func offerRideButtonTapped() {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            getPickUpDropLocation("offerVehicle")
        }
    }
    
    func getPickUpDropLocation(_ triggerFrom: String)
    {
        let joinOrOfferRideObj = JoinOrOfferRide()
        let vc = RideDetailMapViewController()
        vc.sourceLong = self.sourceLong
        vc.sourceLat = self.sourceLat
        vc.sourceLocation = self.sourceLoc
        vc.destLong = self.destLong
        vc.destLat = self.destLat
        vc.destLocation = self.destloc
        vc.poolTaxiBooking = self.poolTaxiBooking
        vc.poolId = self.poolId
        vc.rideType = self.rideType
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.hidesBottomBarWhenPushed = true
        vc.triggerFrom = triggerFrom
        vc.isWP = false
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
            joinOrOfferRideObj.poolID = self.poolId
            joinOrOfferRideObj.vehicleRegNo = ""
            joinOrOfferRideObj.vehicleCapacity = ""
            joinOrOfferRideObj.cost = "0"
            joinOrOfferRideObj.rideMsg = ""
            joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
            joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
            joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
            joinOrOfferRideObj.dropPoint = dropLoc as? String
            joinOrOfferRideObj.dropPointLat = dropLat as? String
            joinOrOfferRideObj.dropPointLong = dropLong as? String
            
            if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                showIndicator("Joining Ride..")
                OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                            //TODO : need to Update Ride Details.
                            self.updateRideDetails()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
                
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func updateRideDetails()
    {
        showIndicator("Fetching Ride Details")
        RideRequestor().getRideDetails(self.poolId, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.data = object as? RideDetail
                vc.triggerFrom = "ride"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
}

