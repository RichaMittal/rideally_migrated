//
//  AutoCompletePlaceController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 08/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces


class AutoCompletePlaceController: UITableViewController {
    
    var locationCoord:CLLocationCoordinate2D?
    var locationName: String?
    let cell_height: CGFloat = 40.0
    let big_cell_height: CGFloat = 60.0
    let NormalAddressCell = "NormalAddressCell"
    var placeSearchResultsArray = [AnyObject]()
    
    
    // 1. Globally define a "special notification key" constant that can be broadcast / tuned in to...
    let autoCompletedNotificationKey = "com.RideAlly.AutoCompletedNotificationKey"
    let updateTextFieldNotificationKey = "com.RideAlly.UpdateTextFieldNotificationKey"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(AutoCompletePlaceController.autoCompletedNotification), name: NSNotification.Name(rawValue: autoCompletedNotificationKey), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func autoCompletedNotification(_ notification:Notification) -> Void {
        guard let userInfo = notification.userInfo,
            let placeSearch  = userInfo["searchText"] as? String else {
                print("No userInfo found in notification")
                return
        }
        self.getPlacesForText(placeSearch)
        
    }
    
    func getPlacesForText(_ searchText: String) {
        let placesClient = GMSPlacesClient()
        
        let filter = GMSAutocompleteFilter()
        filter.country = INDIA_COUNTRY_CODE
        filter.type = GMSPlacesAutocompleteTypeFilter.geocode
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: filter) { [weak self] (results, error:Error?) -> Void in
            self?.placeSearchResultsArray.removeAll()
            if results == nil {
                return
            }
            self?.placeSearchResultsArray = results! as [AnyObject]
            self?.presentAddressData()
        }
    }
    
    let MAX_ROWS = 5
    let section_height: CGFloat = 20
    func presentAddressData () {
        // let count = self.placeSearchResultsArray.count > MAX_ROWS ? MAX_ROWS : self.placeSearchResultsArray.count
        // let height_cell = cell_height
        // let extra = count != 0 ? section_height : 0
        // self.tableHeightConstraint?.constant = CGFloat(count) * height_cell + extra
        self.tableView.reloadData()
        // self.suggestionView.layoutIfNeeded()
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placeSearchResultsArray.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cell_height
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cellIdentifier: String?
        var text: String?
        let loadRecentAddressCell: Bool = false
        
        cellIdentifier = NormalAddressCell
        let object: GMSAutocompletePrediction = self.placeSearchResultsArray[indexPath.row] as! GMSAutocompletePrediction
        text = object.attributedFullText.string
        
        var cell = UITableViewCell()
        if loadRecentAddressCell == false {
            cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: cellIdentifier!)
            cell.textLabel?.text = text!
            cell.textLabel?.font = boldFontWithSize(13.0)
        }
        //        } else {
        //            let addrCell = UITableViewCell()
        //            return addrCell
        //        }
        return cell
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object: GMSAutocompletePrediction = self.placeSearchResultsArray[indexPath.row] as! GMSAutocompletePrediction
        //object.attributedFullText.string
        
        locationName = object.attributedFullText.string
        performDidSelectForGoogleAPI(object)
        
    }
    
    //MARK:- Navigation to Master Service
    func performDidSelectForGoogleAPI (_ placeObject: GMSAutocompletePrediction) {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
        let client = GMSPlacesClient()
        client.lookUpPlaceID(placeObject.placeID!) { [weak self] (place, error) -> Void in
            if let coord = place?.coordinate {
                // self?.moveMapToCoord(coord)
                self!.locationCoord = coord
                
                let latitude = coord.latitude
                let longitute = coord.longitude
                _ = ["searchText" : placeObject.attributedFullText.string as AnyObject ,
                                "latitude"   : latitude as AnyObject,
                                "longitute"  : longitute as AnyObject] as [String: AnyObject]
                //NotificationCenter.default.post(name: self?.updateTextFieldNotificationKey, object: nil, userInfo: userInfo)
//                NotificationCenter.default.postNotificationName(self!.updateTextFieldNotificationKey,
//                                                                          object: nil,
//                                                                          userInfo: userInfo)
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : ChoosLocationViewController = storyboard.instantiateViewController(withIdentifier: "ChoosLocationViewController") as! ChoosLocationViewController
                vc.locationCoord = self!.locationCoord
                vc.locationAddress = self!.locationName;
                self!.present(vc, animated: true, completion: nil)
            }
        }
        }
        // added to reset tableView on selection
        //  searchTextField.resignFirstResponder()
        //  resetAddressTable()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //chooseLocationSegue
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            if segue.identifier == "chooseLocationSegue" {
                let vc = segue.destination as! ChoosLocationViewController
                vc.locationCoord = self.locationCoord
                vc.locationAddress = self.locationName;
            }
        }
        
    }
    
}
