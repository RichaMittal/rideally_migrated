//
//  homeWorkMarkerInfo.swift
//  rideally
//
//  Created by Pratik Bhaliya on 14/04/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class HomeWorkMarkerInfo: UIView {

    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var address: UILabel!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profile.layer.cornerRadius = 30.0
        self.profile.layer.masksToBounds = true
    }

}
