//
//  RideMapViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 07/02/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import Popover
import AddressBook
import AddressBookUI

extension RideMapViewController: GMSMapViewDelegate,GMSPanoramaViewDelegate {
    
}


extension RideMapViewController: CustomeAlertViewControllerDelegate,UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate  {
    
}

class RideMapViewController: BaseScrollViewController, UIPickerViewDelegate, UIPickerViewDataSource, ABPeoplePickerNavigationControllerDelegate {
    // Map Object
    @IBOutlet weak var mapView: GMSMapView!
    // View Object
    @IBOutlet weak var dateTimePickerView: UIView!
    @IBOutlet weak var submitButtonView: UIView!
    @IBOutlet weak var offerDetailView: UIView!
    @IBOutlet weak var offerPriceView: UIView!
    
    // Button Object
    @IBOutlet weak var switchView: UIView!
    @IBOutlet weak var getRideButton: UIButton!
    @IBOutlet weak var offerRideButton: UIButton!
    @IBOutlet weak var trafficButton: UIButton!
    @IBOutlet weak var flipButton: UIButton!
    @IBOutlet weak var searchScreenButton: UIButton!
    @IBOutlet weak var searchScreenButton1: UIButton!
    
    @IBOutlet weak var pickerButton: UIButton!
    // Date Picker
    @IBOutlet weak var datePickerView: UIDatePicker!
    // Label Object
    @IBOutlet weak var leftSideTitleLabel: UILabel!
    @IBOutlet weak var leftSideDetailLabel: UILabel!
    @IBOutlet weak var rightSideTitleLabel: UILabel!
    @IBOutlet weak var rightSideDetailsLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    // Layout Constraint
    @IBOutlet weak var submitButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var dateViewHeight: NSLayoutConstraint!
    @IBOutlet weak var vehicleDetailButton: UIButton!
    @IBOutlet weak var priceButton: UIButton!
    
    @IBOutlet weak var anyPickerDoneButton: UIButton!
    @IBOutlet weak var anyPickerView: UIPickerView!
    @IBOutlet weak var anyPickerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var anyBgView: UIView!
    @IBOutlet weak var anyButton: UIButton!
    var pickerData: [String] = [String]()
    
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var vehicleView: UIView!
    
    var data: WorkPlace?
    var wpDetailData: WPMembersResponseData?
    var dataSourceToOfc: [WPRide]?
    var dataSourceToHome: [WPRide]?
    var newRideReqObj = WPNewRideRequest()
    var routePolylineIndex0: GMSPolyline?
    var routePolylineIndex1: GMSPolyline?
    var routePolylineIndex2: GMSPolyline?
    var routePolylineOrange: GMSPolyline!
    
    var vehicleDefaultDetails = VehicleDefaultResponse()
    var vehicleConfig = VehicleConfigResponse()
    var rideData: Ride!
    
    var offerRideMutArray : NSMutableArray!
    var getRideMutArray : NSMutableArray!
    var polylineMutArray : NSMutableArray? = NSMutableArray()
    var viaMutArray : NSMutableArray? = NSMutableArray()
    var isHomeToWork : Bool!
    var nRideMutArray : NSMutableArray? = NSMutableArray()
    var oRideMutArray : NSMutableArray? = NSMutableArray()
    
    var mapMarkerMutArray : NSMutableArray? = NSMutableArray()
    
    
    var selectedLatitute : String!
    var selectedLongitute : String!
    
    var selectedHLatitute : String!
    var selectedHLongitute : String!
    var selectedWLatitute : String!
    var selectedWLongitute : String!
    
    var selectedHLocation : String!
    var selectedWLocation : String!
    
    var selectedDateTime : Date!
    var totalDistanceInMeters: UInt = 0
    var totalDistance: String!
    var totalDurationInSeconds: UInt = 0
    var totalDuration: String!
    
    var isGetRide : Bool!
    var isOfferRide : Bool!
    
    var isDefaultVechile : Bool!
    
    var infoWindow : RideWindowInfoView!
    var memberInfoWindow : MemberWindowInfoView!
    var customMarker: HomeWorkMarkerInfo!
    var sourceLoc = ""
    var sourceLat = ""
    var sourceLong = ""
    var destloc = ""
    var destLat = ""
    var destLong = ""
    var poolTaxiBooking = ""
    var poolId = ""
    var rideType = ""
    var rideCost = ""
    var rideDistance = ""
    var poolScope = ""
    var responses: [InviteResult]?
    var isFrom = ""
    var ownerName = ""
    // MARK:- View Delegate Method
    let lblVehModel = UILabel()
    let lblSeatLeft = UILabel()
    let lblCost = UILabel()
    let lblComment = UILabel()
    let txtComment = UITextField()
    let locationButton = UIButton()
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    let pickerView = UIPickerView()
    let iconVeh = UIImageView()
    let stepperSeats = PKYStepper()
    let stepperCost = PKYStepper()
    var wpMembers: [WPMember]?
    var isMemberInfoWindow: Bool?
    var userMobileNo = ""
    var onSelectBackBlock: actionBlock?
    var signupFlow = false
    var ridePoints = 0
    var homeAddressUpdated = false
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    
    override func hasTabBarItems() -> Bool
    {
        ISFROM = isFrom
        ISWPRIDESSCREEN = true
        ISWPMAPSCREEN = true
        WPDATA = data
        WPMEMBERS = wpMembers
        DATASOURCETOOFC = dataSourceToOfc
        DATASOURCETOHOME = dataSourceToHome
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(UserInfo.sharedInstance.availablePoints == 0 || UserInfo.sharedInstance.maxAllowedPoints == "") {
            getUserPoints()
        }
        self.navigationController?.navigationBar.isHidden = false
        
        self.selectedHLatitute = UserInfo.sharedInstance.homeLatValue
        self.selectedHLongitute = UserInfo.sharedInstance.homeLongValue
        self.selectedWLatitute = UserInfo.sharedInstance.workLatValue
        self.selectedWLongitute = UserInfo.sharedInstance.workLongValue
        
        
        self.selectedHLocation = UserInfo.sharedInstance.homeAddress
        self.selectedWLocation = UserInfo.sharedInstance.workAddress
        //if(signupFlow) {
            if(data?.wpLat != nil){
                data?.wpLat = data?.wpLat
            } else {
                data?.wpLat = data?.wpDetailLat
            }
            if(data?.wpLong != nil){
                data?.wpLong = data?.wpLong
            } else {
                data?.wpLong = data?.wpDetailLong
            }
            if(data?.wpLocation != nil){
                data?.wpLocation = data?.wpLocation
            } else {
                data?.wpLocation = data?.wpDetailLocation
            }
            if(data?.wpMembershipStatus != nil){
                data?.wpMembershipStatus = data?.wpMembershipStatus
            } else {
                data?.wpMembershipStatus = data?.wpDetailMembershipStatus
            }
        //}
        pickerData = ["Any", "Only Male", "Only Female"]
        self.isGetRide = true
        self.isOfferRide = false
        self.searchScreenButton.isUserInteractionEnabled = true
        self.searchScreenButton1.isUserInteractionEnabled = false
        
        //Register Notification
        self.registerNotification()
        // UI Update..
        self.initallyUpdateUI()
        
        // Load Map and Show Polyline..
        self.loadGMSMap()
        //
        self.isDefaultVechile = false
        
        // Connect data:
        self.anyPickerView?.delegate = self
        self.anyPickerView?.dataSource = self
        self.loadInitallyContent()
        self.selectedLatitute = UserInfo.sharedInstance.homeLatValue
        self.selectedLongitute = UserInfo.sharedInstance.homeLongValue
         NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "KInviteSendNotificationWpDetail"),object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotificationWpDetail"), object: nil)
        
        
        if(signupFlow == true) {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = Colors.GREEN_COLOR
            //self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .Plain, target: self, action: #selector(backButtonClicked))
        }
        //setVehicleView()
    }
    
    //    func setVehicleView () {
    //        let vehView = UIView()
    //        vehView.translatesAutoresizingMaskIntoConstraints = false
    //        vehView.backgroundColor = Colors.WHITE_COLOR
    //        self.view.addSubview(vehView)
    //
    //        iconVeh.translatesAutoresizingMaskIntoConstraints = false
    //        vehView.addSubview(iconVeh)
    //
    //        lblVehModel.textColor = Colors.GRAY_COLOR
    //        lblVehModel.font = boldFontWithSize(13)
    //        lblVehModel.translatesAutoresizingMaskIntoConstraints = false
    //        //let tapGestureVeh = UITapGestureRecognizer(target: self, action: #selector(selectVehicle))
    //        lblVehModel.userInteractionEnabled = true
    //        //lblVehModel.addGestureRecognizer(tapGestureVeh)
    //        lblVehModel.text = "Car"
    //        vehView.addSubview(lblVehModel)
    //
    //        let iconSeat = UIImageView()
    //        iconSeat.image = UIImage(named: "seat")
    //        iconSeat.translatesAutoresizingMaskIntoConstraints = false
    //        vehView.addSubview(iconSeat)
    //
    //        lblSeatLeft.textColor = Colors.GRAY_COLOR
    //        lblSeatLeft.font = boldFontWithSize(13)
    //        lblSeatLeft.translatesAutoresizingMaskIntoConstraints = false
    //        //let tapGestureSeatLeft = UITapGestureRecognizer(target: self, action: #selector(selectCost))
    //        lblSeatLeft.userInteractionEnabled = true
    //        lblSeatLeft.text = "4"
    //        //lblSeatLeft.addGestureRecognizer(tapGestureSeatLeft)
    //        vehView.addSubview(lblSeatLeft)
    //
    //        let iconCost = UIImageView()
    //        iconCost.image = UIImage(named: "fare_green-1")
    //        iconCost.translatesAutoresizingMaskIntoConstraints = false
    //        vehView.addSubview(iconCost)
    //
    //        lblCost.textColor = Colors.GRAY_COLOR
    //        lblCost.font = boldFontWithSize(13)
    //        lblCost.translatesAutoresizingMaskIntoConstraints = false
    //        //let tapGestureCost = UITapGestureRecognizer(target: self, action: #selector(selectCost))
    //        lblCost.userInteractionEnabled = true
    //        //lblCost.addGestureRecognizer(tapGestureCost)
    //        lblCost.text = "4 / Km"
    //        vehView.addSubview(lblCost)
    //
    //        let lblLine2 = UILabel()
    //        lblLine2.backgroundColor = Colors.GENERAL_BORDER_COLOR
    //        lblLine2.translatesAutoresizingMaskIntoConstraints = false
    //        vehView.addSubview(lblLine2)
    //
    //        vehView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-5-[model(20)][line2(0.5)]|", options: [], metrics: nil, views: ["model":lblVehModel, "line2":lblLine2]))
    //        vehView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-15-[imodel(15)]-5-[model][iseat(15)]-5-[seat(==model)][icost(15)]-5-[cost(==model)]|", options: [], metrics: nil, views: ["imodel":iconVeh, "model":lblVehModel, "iseat":iconSeat, "seat":lblSeatLeft, "icost":iconCost, "cost":lblCost]))
    //        vehView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[line2]|", options: [], metrics: nil, views: ["line2":lblLine2]))
    //
    //        vehView.addConstraint(NSLayoutConstraint(item: iconVeh, attribute: .CenterY, relatedBy: .Equal, toItem: lblVehModel, attribute: .CenterY, multiplier: 1, constant: 0))
    //        vehView.addConstraint(NSLayoutConstraint(item: lblVehModel, attribute: .CenterY, relatedBy: .Equal, toItem: iconSeat, attribute: .CenterY, multiplier: 1, constant: 0))
    //        vehView.addConstraint(NSLayoutConstraint(item: iconSeat, attribute: .CenterY, relatedBy: .Equal, toItem: lblSeatLeft, attribute: .CenterY, multiplier: 1, constant: 0))
    //        vehView.addConstraint(NSLayoutConstraint(item: lblSeatLeft, attribute: .CenterY, relatedBy: .Equal, toItem: iconCost, attribute: .CenterY, multiplier: 1, constant: 0))
    //        vehView.addConstraint(NSLayoutConstraint(item: iconCost, attribute: .CenterY, relatedBy: .Equal, toItem: lblCost, attribute: .CenterY, multiplier: 1, constant: 0))
    //        dateView.addSubview(vehView)
    //        dateView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[veh(30)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["veh":vehView]))
    //        dateView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[veh]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["veh":vehView]))
    //
    //        //lblVehModel.text = updateData?.vehicleType
    ////        if let vehicleType = updateData?.vehicleType{
    ////            if(vehicleType.lowercaseString == "bike") {
    ////                iconVeh.image = UIImage(named: "veh_bike")
    ////            } else if(vehicleType.lowercaseString == "auto") {
    ////                iconVeh.image = UIImage(named: "auto")
    ////            } else if(vehicleType.lowercaseString == "car") {
    ////                iconVeh.image = UIImage(named: "veh_car")
    ////            } else if(vehicleType.lowercaseString == "cab") {
    ////                iconVeh.image = UIImage(named: "cab")
    ////            } else if(vehicleType.lowercaseString == "suv") {
    ////                iconVeh.image = UIImage(named: "suv")
    ////            } else if(vehicleType.lowercaseString == "tempo") {
    ////                iconVeh.image = UIImage(named: "tempo")
    ////            } else if(vehicleType.lowercaseString == "bus") {
    ////                iconVeh.image = UIImage(named: "bus")
    ////            }
    ////            lblVehModel.text = vehicleType.capitalizedString
    ////        }
    ////        SeatValue = (updateData?.seatsLeft)!
    ////        lblSeatLeft.text = SeatValue
    ////        if let cost = updateData?.cost {
    ////            costValue = cost
    ////            lblCost.text = "\(cost) \("/ Km")"
    ////            stepperCost.value = Int32(cost)!
    ////            stepperCost.countLabel.text = "\(cost)"
    ////        }
    //    }
    
    func loadInitallyContent()  {
        // Update Flip Address from User defaule user Already Set Locations.
        self.initallyAddressLocation(true)
        // Update Current Time Date Picker With extra 5 min because When We booked the Ride. So required 5 min before running ride.
        self.updateDatePicker()
        // Service Call to Get Ride Type.
        self.getISRideType()
        self.loadGetRideDetails(true)
        // initally Update Service Request
        newRideReqObj.userId = UserInfo.sharedInstance.userID
        newRideReqObj.selected_dates = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "dd-MMM-yyyy")
        self.selectedDateTime = Date().addingTimeInterval(300)
        newRideReqObj.flip = "0" // (if Home to work) / 1 (If Work to Home)
        newRideReqObj.ridetype = "N" // N = GET RIDE  or O = Offer ride.
        
        
        if UserInfo.sharedInstance.gender == "M" {
            pickerData = ["Any", "Only Male"]
            
        }else if UserInfo.sharedInstance.gender == "F" {
            pickerData = ["Any", "Only Female"]
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        if self.isDefaultVechile == true {
            self.getConfigData()
        }
        data = WPDATA
        if(ISSEARCHRESULT) {
            self.dataSourceToOfc = SEARCHSOURCETOOFC
            self.dataSourceToHome = SEARCHSOURCETOHOME
            self.loadGMSMap()
            self.loadInitallyContent()
        } else {
            self.dataSourceToOfc = DATASOURCETOOFC
            self.dataSourceToHome = DATASOURCETOHOME
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addTabbarleftOptions()
    {
        var image = UIImage()
        if data == nil {
            return
        }
        if let url = data?.wpLogoURL{
            if(url != ""){
                let url  = URL(string: "\(URLS.IMG_URL)\(data!.wpLogoURL!)")
                let  imageData = try? Data(contentsOf: url!)
                image = UIImage(data: imageData!)! as UIImage
            }
            else{
                image = UIImage(named: "work_loc")!
            }
        }
        else{
            image = UIImage(named: "work_loc")!
        }
        
        let view1 = UIView(frame: CGRect(x: 0,y: 0,width: 200,height: 30))
        // view1.backgroundColor = Colors.RED_COLOR
        let backButton = UIButton()
        backButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        backButton.setImage(UIImage(named: "back"), for: UIControlState())
        backButton.addTarget(self, action: #selector(backButtonClicked), for: .touchDown)
        view1.addSubview(backButton)
        
        let btnProfile = UIButton()
        btnProfile.frame = CGRect(x: 30, y: 0, width: 30, height: 30)
        // data!.wpName?.capitalizedString
        btnProfile.setImage(image, for: UIControlState())
        btnProfile.layer.cornerRadius = 15
        btnProfile.layer.masksToBounds = true
        btnProfile.addTarget(self, action: #selector(showWPDetail), for: .touchDown)
        view1.addSubview(btnProfile)
        
        let btnProfileName = UIButton()
        btnProfileName.frame = CGRect(x: 62,y: 0, width: 130, height: 30)
        // btnProfileName.backgroundColor = Colors.GRAY_COLOR
        let nameTitle =  data!.wpName?.capitalized
        btnProfileName.setTitle(nameTitle, for: UIControlState())
        btnProfileName.titleLabel?.textAlignment = NSTextAlignment.left
        //  btnProfileName.titleLabel?.minimumScaleFactor = 8.0; // or some more adequate size
        btnProfileName.titleLabel?.numberOfLines = 1 // Dynamic number of lines
        btnProfileName.titleLabel?.lineBreakMode = NSLineBreakMode.byTruncatingTail
        //btnProfileName.titleLabel?.adjustsFontSizeToFitWidth = true;
        btnProfileName.titleLabel?.font = UIFont(name: FONT_NAME.NORMAL_FONT, size: 20.0)
        // btnProfileName.sizeToFit()
        btnProfileName.addTarget(self, action: #selector(showWPDetail), for: .touchDown)
        view1.addSubview(btnProfileName)
        
        let leftBarItem = UIBarButtonItem(customView: view1)
        self.navigationItem.leftBarButtonItem = leftBarItem
        
    }
    
    func backButtonClicked(){
        if(ISFROM != "" || signupFlow) {
            ISFROM = ""
            let ins = UIApplication.shared.delegate as! AppDelegate
            if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                ins.gotoHome()
            } else {
                ins.gotoHome(1)
            }
        } else {
            _ = self.navigationController?.popToRootViewController(animated: true)
            onSelectBackBlock?()
        }
    }
    
    func showWPDetail()
    {
//        let vc = WPDetailViewController()
//        vc.hidesBottomBarWhenPushed = true
//        vc.edgesForExtendedLayout = .None
//        vc.data = data
//        self.navigationController?.pushViewController(vc, animated: true)
        
        showIndicator("Fetching Workplace Detail.")
        WorkplaceRequestor().getWPMembers("", groupID: data!.wpID!, distanceRange: "1", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = WPDetailViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.data = (object as! WPMembersResponseData).groupDetails
            self.navigationController?.pushViewController(vc, animated: true)
        }) { (error) in
            hideIndicator()
        }
    }
    
    func registerNotification (){
        NotificationCenter.default.addObserver(self, selector: #selector(updateHomeAddressNotification), name:NSNotification.Name(rawValue: "kSetHomeAddressNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateHomeDefaultAddressNotification), name:NSNotification.Name(rawValue: "kSetHomeDefaultAddressNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(getPriceNotification), name:NSNotification.Name(rawValue: "kGetPriceNotification"), object: nil)
        
    }
    
    // MARK:- Custome Load time method.
    func initallyUpdateUI()  {
        // Default Home to work location..
        self.isHomeToWork = true
        // Set MapView Delegate..
        mapView.delegate = self
        // Switch View add layer..
        switchView.layer.borderWidth = 1
        switchView.layer.borderColor = Colors.BG_GREEN_COLOR.cgColor
        // Base View Color set to white..
        self.view.backgroundColor = UIColor.white
        // Navigation Title Set.
        //navigationItem.title = data!.wpName?.capitalizedString
        self.addTabbarleftOptions()
        // Update First time
        //Source
        newRideReqObj.toLat = UserInfo.sharedInstance.homeLatValue
        newRideReqObj.toLong = UserInfo.sharedInstance.homeLongValue
        newRideReqObj.toLocation = UserInfo.sharedInstance.homeAddress
        //Distination
        newRideReqObj.fromLat =  data?.wpLat
        newRideReqObj.fromLong = data?.wpLong
        newRideReqObj.fromLocation = data?.wpLocation
        newRideReqObj.poolType  = "A"
    }
    
    func loadGMSMap() {
        // Set GMS Camera Positioning..
        let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: Double((data?.wpLat)!)!, longitude: Double((data?.wpLong)!)!, zoom: 12.0)
        mapView.camera = camera
        // Add marker..
        // let marker = GMSMarker()
        //  marker.position = camera.target
        //  marker.snippet = "Ride"
        //  marker.appearAnimation = kGMSMarkerAnimationPop
        //  marker.groundAnchor = CGPointMake(1, 1)
        //  marker.map = self.mapView
        
        // Get Path from GMS Service Requeset.
        self.addOverlayToMapView(UserInfo.sharedInstance.homeLatValue,sourceLong: UserInfo.sharedInstance.homeLongValue,destinationLat: data!.wpLat!,destinationLong: data!.wpLong!)
        
        self.selectedWLatitute = data!.wpLat
        self.selectedWLongitute = data!.wpLong!
        self.selectedWLocation = data!.wpLocation
        
    }
    
    func updateDatePicker() {
        self.datePickerView.minimumDate = Date().addingTimeInterval(300)
        self.datePickerView.setDate(Date().addingTimeInterval(300), animated: true)
        self.timeLabel.text =  prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "MMM d, hh:mm a")
    }
    
    func initallyAddressLocation(_ active : Bool)  {
        if active {
            self.isHomeToWork = true
            self.leftSideTitleLabel.text = "Home"
            self.leftSideDetailLabel.text = self.selectedHLocation
            self.rightSideTitleLabel.text = "Work"
            self.rightSideDetailsLabel.text = self.data!.wpLocation! as String
            
            newRideReqObj.flip = "0" // (if Home to work) / 1 (If Work to Home)
            
            if ((self.originMarker) != nil){
                self.originMarker.icon = UIImage(named:"marker_home_green")
                self.destinationMarker.icon = UIImage(named:"marker_wp_green")
            }
            
            self.searchScreenButton.isUserInteractionEnabled = true
            self.searchScreenButton1.isUserInteractionEnabled = false
            
        }else{
            
            self.searchScreenButton.isUserInteractionEnabled = false
            self.searchScreenButton1.isUserInteractionEnabled = true
            if ((self.originMarker) != nil){
                self.originMarker.icon = UIImage(named:"marker_home_orange")//
                self.destinationMarker.icon = UIImage(named:"marker_wp_orange")
            }
            self.isHomeToWork = false
            self.leftSideTitleLabel.text = "Work"
            self.rightSideTitleLabel.text = "Home"
            self.rightSideDetailsLabel.text = self.selectedHLocation
            self.leftSideDetailLabel.text = self.data!.wpLocation! as String
            newRideReqObj.flip = "1" // 0  (if Home to work) / 1 (If Work to Home)
        }
        
        //Source
        newRideReqObj.toLat = UserInfo.sharedInstance.homeLatValue
        newRideReqObj.toLong = UserInfo.sharedInstance.homeLongValue
        newRideReqObj.toLocation = UserInfo.sharedInstance.homeAddress
        //Distination
        newRideReqObj.fromLat =  data?.wpLat
        newRideReqObj.fromLong = data?.wpLong
        newRideReqObj.fromLocation = data?.wpLocation
        
        // Get / Offer depenting on O and N
        self.getISRideType()
    }
    
    
    func initallyflipAddressLocation(_ active : Bool)  {
        if active {
            self.isHomeToWork = true
            self.leftSideTitleLabel.text = "Home"
            self.leftSideDetailLabel.text = self.selectedHLocation
            self.rightSideTitleLabel.text = "Work"
            self.rightSideDetailsLabel.text = self.data!.wpLocation! as String
            
            
            newRideReqObj.flip = "0" // (if Home to work) / 1 (If Work to Home)
            
            if ((self.originMarker) != nil){
                self.originMarker.icon = UIImage(named:"marker_home_green")
                self.destinationMarker.icon = UIImage(named:"marker_wp_green")
            }
            self.searchScreenButton.isUserInteractionEnabled = true
            self.searchScreenButton1.isUserInteractionEnabled = false
            
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
            
        }else{
            self.searchScreenButton.isUserInteractionEnabled = false
            self.searchScreenButton1.isUserInteractionEnabled = true
            if ((self.originMarker) != nil){
                self.originMarker.icon = UIImage(named:"marker_home_orange")//
                self.destinationMarker.icon = UIImage(named:"marker_wp_orange")
            }
            self.isHomeToWork = false
            self.leftSideTitleLabel.text = "Work"
            self.rightSideTitleLabel.text = "Home"
            self.rightSideDetailsLabel.text = self.selectedHLocation
            self.leftSideDetailLabel.text = self.data!.wpLocation! as String
            newRideReqObj.flip = "1" // 0  (if Home to work) / 1 (If Work to Home)
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
        }
        
        //Source
        newRideReqObj.toLat = self.selectedHLatitute
        newRideReqObj.toLong = self.selectedHLongitute
        newRideReqObj.toLocation = self.selectedHLocation
        //Distination
        newRideReqObj.fromLat =  data?.wpLat
        newRideReqObj.fromLong = data?.wpLong
        newRideReqObj.fromLocation = data?.wpLocation
        
        // Get / Offer depenting on O and N
        self.getISRideType()
    }
    
    
    // MARK:- Google Map Direction Api Call
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    
    var originSelectedRideMarker: GMSMarker!
    var destinationSelectedRideMarker: GMSMarker!
    
    func addOverlayToMapView(_ sourceLat: String,sourceLong: String,destinationLat: String,destinationLong: String ){
        showIndicator("Loading Map path...")
        let sourceStr = "\(sourceLat),\(sourceLong)"
        let destinationStr = "\(destinationLat), \(destinationLong)"
        //TODO: hard code key value addd Need to remove.
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true&sensor=false"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        // print(directionURL," directionURL --------- ")
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    if routes.count == 0{
                        return
                    }
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    self.addmarkers(legs)
                    self.calculateTotalDistanceAndDuration(legs)
                    self.addPolyLineWithEncodedStringInMap(routes as NSArray)
                    self.addWpMemberMarker()
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }
    
    func addRideOverlayToMapView(_ sourceLat: String,sourceLang: String,distinationLat: String,distinationLang: String ){
        showIndicator("Loading Map path...")
        let sourceStr = "\(sourceLat),\(sourceLang)"
        let destinationStr = "\(distinationLat), \(distinationLang)"
        //TODO: hard code key value addd Need to remove.
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        // print(directionURL," directionURL --------- ")
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    // self.addmarkers(legs)
                    let legsStartLocDict = legs[0] as! NSDictionary
                    let startLocationDictionary = legsStartLocDict["start_location"] as! NSDictionary
                    self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
                    let legsEndLocDict = legs[legs.count - 1] as! NSDictionary
                    let endLocationDictionary = legsEndLocDict["end_location"] as! NSDictionary
                    self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
                    
                    let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
                    let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
                    let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
                    let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
                    self.mapView.camera = camera
                    
                    // self.mapView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
                    
                    self.originSelectedRideMarker = GMSMarker(position: self.originCoordinate)
                    
                    self.destinationSelectedRideMarker = GMSMarker(position: self.destinationCoordinate)
                    self.destinationSelectedRideMarker.map = self.mapView
                    self.destinationSelectedRideMarker.snippet = self.data!.wpDetailLocation
                    
                    self.destinationSelectedRideMarker.icon = UIImage(named:"marker_wp_orange")
                    self.destinationMarker.icon = UIImage(named:"marker_wp_orange")
                    self.destinationSelectedRideMarker.title = "destination"
                    self.destinationSelectedRideMarker.title = self.selectedWLocation
                    self.destinationSelectedRideMarker.accessibilityLabel = "\(1313)"
                    
                    //self.calculateTotalDistanceAndDuration(legs)
                    
                    let routesDicts = routes[0] as! NSDictionary
                    let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
                    let points = overview_polyline ["points"] as! NSString
                    
                    
                    if self.routePolylineOrange != nil {
                        self.routePolylineOrange = nil
                    }
                    
                    let path = GMSMutablePath(fromEncodedPath: points as String)
                    self.routePolylineOrange = GMSPolyline(path: path)
                    
                    self.routePolylineOrange.strokeColor = Colors.POLILINE_ORANGE
                    self.routePolylineOrange.strokeWidth = 5.0
                    self.routePolylineOrange.isTappable = false
                    //self.routePolylineOrange.title = String(describing: index)
                    self.routePolylineOrange.map = self.mapView
                    self.routePolylineOrange.zIndex = 110
                    let boundsPath = GMSCoordinateBounds(path: path!)
                    self.mapView.animate(with: GMSCameraUpdate.fit(boundsPath, withPadding: 80))
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }
    
    func calculateTotalDistanceAndDuration(_ legs : NSArray) {
        totalDistanceInMeters = 0
        totalDurationInSeconds = 0
        let legsDict = legs[0] as! NSDictionary
        totalDistanceInMeters += (legsDict["distance"] as! NSDictionary)["value"] as! UInt
        totalDurationInSeconds += (legsDict["duration"] as! NSDictionary)["value"] as! UInt
        
        let distanceInKilometers: Double = Double(totalDistanceInMeters / 1000)
        totalDistance = "\(distanceInKilometers) Km"
        newRideReqObj.distance = totalDistance
        
        let mins = totalDurationInSeconds / 60
        
        newRideReqObj.duration = "\(distanceInKilometers) mins"
        
        let hours = mins / 60
        let days = hours / 24
        let remainingHours = hours % 24
        let remainingMins = mins % 60
        let remainingSecs = totalDurationInSeconds % 60
        totalDuration = "Duration: \(days) d, \(remainingHours) h, \(remainingMins) mins, \(remainingSecs) secs"
    }
    
    
    // MARK:- Add markers
    func addmarkers(_ legs : NSArray) {
        let legsStartLocDict = legs[0] as! NSDictionary
        let startLocationDictionary = legsStartLocDict["start_location"] as! NSDictionary
        self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
        let legsEndLocDict = legs[legs.count - 1] as! NSDictionary
        let endLocationDictionary = legsEndLocDict["end_location"] as! NSDictionary
        self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
        
        let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
        let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
        let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
        let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
        self.mapView.camera = camera
        
        // self.mapView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
        
        self.originMarker = GMSMarker(position: self.originCoordinate)
        self.originMarker.map = self.mapView
        self.originMarker.isTappable = true
        self.originMarker.snippet = self.data!.wpDetailLocation
        
        if self.isHomeToWork == true {
            self.originMarker.icon = UIImage(named:"marker_home_green")
        }else{
            self.originMarker.icon = UIImage(named:"marker_home_orange")
        }
        //self.originMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        self.originMarker.tracksInfoWindowChanges = true
        self.originMarker.title = self.selectedHLocation
        self.originMarker.accessibilityLabel = "\(1212)"
        
        self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
        self.destinationMarker.map = self.mapView
        self.destinationMarker.isTappable = true
        self.destinationMarker.snippet = self.data!.wpDesc
        // self.destinationMarker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        // self.destinationMarker.tracksInfoWindowChanges = false
        if self.isHomeToWork == true {
            self.destinationMarker.icon = UIImage(named:"marker_wp_green")
        }else{
            self.destinationMarker.icon = UIImage(named:"marker_wp_orange")
        }
        self.destinationMarker.title = self.selectedWLocation
        self.destinationMarker.accessibilityLabel = "\(1313)"
    }
    
    
    // MARK:- Add PolyLine
    
    func addPolyLineWithEncodedStringInMap(_ routes: NSArray) {
        self.polylineMutArray? .removeAllObjects()
        self.viaMutArray? .removeAllObjects()
        for index in 0..<routes.count{
            let routesDicts = routes[index] as! NSDictionary
            let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
            let points = overview_polyline ["points"] as! NSString
            let summary = routesDicts ["summary"] as! NSString
            let path = GMSMutablePath(fromEncodedPath: points as String)
            let routePolylineObje = GMSPolyline(path: path)
            
            if index == 0 {
                self.routePolylineIndex0 = routePolylineObje
                newRideReqObj.via = routesDicts ["summary"] as? String
                newRideReqObj.waypoints = points as String
            }else if index == 1{
                self.routePolylineIndex1 = routePolylineObje
            }else{
                self.routePolylineIndex2 = routePolylineObje
            }
            
            if index  == 0 {
                routePolylineObje.zIndex = 100
                if(isHomeToWork == true) {
                    routePolylineObje.strokeColor = Colors.POLILINE_GREEN
                } else {
                    routePolylineObje.strokeColor = Colors.POLILINE_ORANGE
                }
            }else{
                routePolylineObje.strokeColor = Colors.POLILINE_GRAY
            }
            
            routePolylineObje.strokeWidth = 5.0
            routePolylineObje.isTappable = true
            routePolylineObje.title = String(index)
            self.polylineMutArray? .add(points)
            self.viaMutArray? .add(summary)
            routePolylineObje.map = self.mapView
            
            let bounds = GMSCoordinateBounds(path: path!)
            mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
        }
        
    }
    
    func loadGetRideDetails (_ isFirstTime:Bool ) {
        newRideReqObj.ridetype = "N"
        newRideReqObj.vehregno = ""
        newRideReqObj.veh_capacity = ""
        newRideReqObj.cost = "0"
        
        self.offerDetailView.isHidden = true
        self.offerPriceView.isHidden = true
        self.getRideButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        self.getRideButton.backgroundColor = Colors.GREEN_COLOR
        
        self.offerRideButton.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        self.offerRideButton.backgroundColor = Colors.WHITE_COLOR
        
        if dataSourceToOfc?.count  == 0 || dataSourceToOfc == nil {
            return
        }
        
        if self.isHomeToWork == true {
            if dataSourceToOfc?.count  == 0 || dataSourceToOfc == nil {
                return
            }
            self.rideData = dataSourceToOfc![0]
        }else  {
            if dataSourceToHome?.count  == 0 || dataSourceToHome == nil {
                return
            }
            self.rideData = dataSourceToHome![0]
        }
        
        self.mapView.clear()
        if rideData.poolRideType == "O" {
            self.addOverlayToMapView(self.selectedHLatitute!,sourceLong: self.selectedHLongitute!,destinationLat: self.selectedWLatitute!,destinationLong: self.selectedWLatitute!)
        }
        
        if self.oRideMutArray?.count == 0 || oRideMutArray == nil {
            return
        }
        var rowsCnt = 0
        if let list = self.oRideMutArray{
            rowsCnt = list.count
        }
        
        for index in 0..<rowsCnt{
            // print("\(index) times 5 is \(index * 5)")
            let ride = self.oRideMutArray![index] as! Ride as Ride!
            var lat: Double
            var long: Double
            if(isHomeToWork == true) {
                lat = Double(ride!.sourceLat!)! + (Double(index) * 0.000002)
                long = Double(ride!.sourceLong!)!
            } else {
                lat = Double(ride!.destLat!)! + (Double(index) * 0.000002)
                long = Double(ride!.destLong!)!
            }
            
            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14.0)
            let marker = GMSMarker()
            marker.position = camera.target
            marker.snippet = ride?.source
            marker.icon = UIImage(named: "marker_ride_green")
            //marker.appearAnimation = GMSMarkerAnimation
            marker.groundAnchor = CGPoint(x: 1, y: 1)
            marker.accessibilityLabel = "\(index)"
            self.mapMarkerMutArray!.add(marker)
            marker.map = self.mapView
        }
    }
    
    func loadOfferRide (){
        
        newRideReqObj.ridetype = "O"
        //newRideReqObj.cost = self.vehicleConfig. // (Need to pass proper Value)
        self.offerDetailView.isHidden = false
        self.offerPriceView.isHidden = false
        self.offerRideButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        self.offerRideButton.backgroundColor = Colors.GREEN_COLOR
        
        self.getRideButton.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        self.getRideButton.backgroundColor = Colors.WHITE_COLOR
        if self.isHomeToWork == true {
            if dataSourceToOfc?.count == 0  || dataSourceToOfc == nil{
                return
            }
            self.rideData = dataSourceToOfc![0]
        }else{
            if dataSourceToHome?.count == 0 || dataSourceToHome == nil {
                return
            }
            self.rideData = dataSourceToHome![0]
        }
        
        if let rideType = rideData.poolRideType {
            if(rideType == "N" ) {
                self.mapView.clear()
                self.addOverlayToMapView(self.selectedHLatitute!,sourceLong: self.selectedHLongitute!,destinationLat: self.selectedWLatitute!,destinationLong: self.selectedWLongitute!)
            }
        }
        
        if(rideData.vehicleType == nil){
            // self.offerPriceView.hidden = true
            self.priceButton.isUserInteractionEnabled = true
        }else{
            // self.offerPriceView.hidden = false
            self.priceButton.isUserInteractionEnabled = true
        }
        
        if self.nRideMutArray?.count == 0 || nRideMutArray == nil{
            return
        }
        
        var rowsCnt = 0
        if let list = self.nRideMutArray{
            rowsCnt = list.count
        }
        
        if rowsCnt <= 0 {
            return;
        }
        
        for index in 0..<rowsCnt{
            let ride = self.nRideMutArray![index] as! Ride as Ride!
            
            var lat: Double
            var long: Double
            if(isHomeToWork == true) {
                lat = Double(ride!.sourceLat!)! + (Double(index) * 0.000002)
                long = Double(ride!.sourceLong!)!
            } else {
                lat = Double(ride!.destLat!)! + (Double(index) * 0.000002)
                long = Double(ride!.destLong!)!
            }
            
            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14.0)
            //    mapView.camera = camera
            let marker = GMSMarker()
            marker.title = ride?.sourceID
            marker.position = camera.target
            marker.snippet = ride?.source
            marker.groundAnchor = CGPoint(x: 1, y: 1)
            marker.icon = UIImage(named: "marker_profile_green")
            //marker.groundAnchor = CGPoint(x: 20, y: 20)
            //marker.appearAnimation = GMSMarkerAnimation
            marker.accessibilityLabel = "\(index)"
            self.mapMarkerMutArray!.add(marker)
            marker.map = self.mapView
        }
    }
    
    
    // MARK:- Segue Actions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        hideInfoWindow()
        if segue.identifier == "searchSegue" {
            let vc = segue.destination as! SearchModelViewController
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext //All objects and view are transparent
            vc.isControlerType = ""
        } else if segue.identifier == "searchSegue" {
            //let vc = segue.destinationViewController as! SearchModelViewController
        }
    }
    
    @objc(mapView:didChangeCameraPosition:) func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        // if let tempPoint = position {
        if infoWindow != nil {
            //infoWindow.center = mapView.projection.pointForCoordinate()
        }
        // }
        
    }
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let index:Int! = Int(marker.accessibilityLabel!)
        if index == 1212 || index == 1313 {
            if customMarker != nil {
                customMarker.removeFromSuperview()
            }
            if memberInfoWindow != nil {
                memberInfoWindow.removeFromSuperview()
            }
            if infoWindow != nil {
                infoWindow.removeFromSuperview()
            }
            customMarker = Bundle.main.loadNibNamed("HomeWorkMarkerInfo", owner: self, options: nil)!.first as! HomeWorkMarkerInfo
            customMarker.frame = CGRect(x: (self.mapView.frame.size.width-260)/2, y: (self.mapView.frame.size.height-80)/2, width: 260, height: 80)
            if(marker.title == selectedHLocation) {
                customMarker.name.text = "Home"
                if(UserInfo.sharedInstance.profilepicStatus == "1"){
                    if UserInfo.sharedInstance.profilepicUrl != ""{
                        customMarker.profile.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(UserInfo.sharedInstance.profilepicUrl)"))
                    }
                    else{
                        if UserInfo.sharedInstance.faceBookid != ""{
                            customMarker.profile.kf.setImage(with: URL(string: "https://graph.facebook.com/\(UserInfo.sharedInstance.faceBookid)/picture?type=large&return_ssl_resources=1"))
                        }
                    }
                }
                else{
                    customMarker.profile.image = UIImage(named: "placeholder")
                }
            } else {
                customMarker.name.text = data?.wpName
                if let url = data?.wpLogoURL{
                    if(url != ""){
                        customMarker.profile.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                        //                        customMarker.profile.layer.borderColor = Colors.GREEN_COLOR.CGColor
                        //                        profilePic.layer.borderWidth = 1.0
                        if(data?.wpScope == "1" || data?.wpTieUpStatus == "1"){
                            //TODO: SHOWlock
                        }
                    }
                    else{
                        if(data?.wpScope == "1" || data?.wpTieUpStatus == "1"){
                            customMarker.profile.image = UIImage(named: "wpPlaceholderL")
                            //TODO: show lock
                        }
                        else{
                            customMarker.profile.image = UIImage(named: "wpPlaceholder")
                        }
                        let url = URL(string:SERVER+"static/groups/"+(data?.wpID)!+".jpg")
                        customMarker.profile.kf.setImage(with: url, placeholder: customMarker.profile.image, options: nil, progressBlock: nil, completionHandler: nil)
                    }
                }
                else{
                    if(data?.wpScope == "1" || data?.wpTieUpStatus == "1"){
                        customMarker.profile.image = UIImage(named: "wpPlaceholderL")
                        //TODO: show lock
                    }
                    else{
                        customMarker.profile.image = UIImage(named: "wpPlaceholder")
                    }
                    let url = URL(string:SERVER+"static/groups/"+(data?.wpID)!+".jpg")
                    customMarker.profile.kf.setImage(with: url, placeholder: customMarker.profile.image, options: nil, progressBlock: nil, completionHandler: nil)
                    
                }
            }
            customMarker.profile.layer.masksToBounds = true
            customMarker.profile.layer.cornerRadius = 25
            customMarker.address.text = marker.title
        }
        else {
            return nil
        }
        return customMarker
    }
    
    @objc(mapView:didTapMarker:) func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let index:Int! = Int(marker.accessibilityLabel!)
        
        if index == nil || index == 1212 || index == 1313 {
            return false
        } else if(marker.accessibilityLabel?.contains("99999"))! {
            isMemberInfoWindow = true
            if infoWindow != nil {
                infoWindow.removeFromSuperview()
            }
            if memberInfoWindow != nil {
                memberInfoWindow.removeFromSuperview()
            }
            if customMarker != nil {
                customMarker.removeFromSuperview()
            }
            let subStringIndex = Int((marker.accessibilityLabel! as NSString).substring(from: 5))!
            let wpMembersObj = self.wpMembers![subStringIndex]
            memberInfoWindow = Bundle.main.loadNibNamed("MemberWindowInfoView", owner: self, options: nil)!.first as! MemberWindowInfoView
            memberInfoWindow.frame = CGRect(x: (self.mapView.frame.size.width-260)/2, y: (self.mapView.frame.size.height-80)/2, width: 260, height: 80)
            memberInfoWindow.memberName.text = wpMembersObj.fname
            memberInfoWindow.homeAddress.text = wpMembersObj.homeLoc
            memberInfoWindow.userDetailBtn.addTarget(self, action: #selector(customMarkerWindowTapped), for:.touchUpInside)
            memberInfoWindow.userDetailBtn.tag = subStringIndex
            if(wpMembersObj.profilePicStatus == "1"){
                if let url = wpMembersObj.profilePicUrl{
                    memberInfoWindow.profile.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                }
                else{
                    if let url = wpMembersObj.fbID{
                        memberInfoWindow.profile.kf.setImage(with: URL(string: "https://graph.facebook.com/\(url)/picture?type=large&return_ssl_resources=1"))
                    }
                }
            }
            else{
                memberInfoWindow.profile.image = UIImage(named: "placeholder")
            }
            memberInfoWindow.rideWithMeBtn.isHidden = true
            if((isHomeToWork == true && wpMembersObj.isActiveGoingUser == 0) || isHomeToWork == false && wpMembersObj.isActiveComingUser == 0) {
                memberInfoWindow.rideWithMeBtn.isHidden = false
                memberInfoWindow.rideWithMeBtn.addTarget(self, action: #selector(rideWithMe), for:.touchUpInside)
                memberInfoWindow.rideWithMeBtn.tag = subStringIndex
            }
            self.mapView.addSubview(memberInfoWindow)
            //return customMarker
        } else{
            isMemberInfoWindow = false
            if infoWindow != nil {
                hideInfoWindow()
            }
            if memberInfoWindow != nil {
                memberInfoWindow.removeFromSuperview()
            }
            if customMarker != nil {
                customMarker.removeFromSuperview()
            }
            self.originSelectedRideMarker = marker
            infoWindow = Bundle.main.loadNibNamed("RideWindowInfoView", owner: self, options: nil)!.first as! RideWindowInfoView
            infoWindow.frame = CGRect(x: (self.mapView.frame.size.width-340)/2, y: (self.mapView.frame.size.height-80)/2, width: 300, height: 130)
            let rideObj : Ride
            
            if self.offerDetailView.isHidden == false{
                rideObj = self.nRideMutArray![index] as! Ride
            }else{
                rideObj = self.oRideMutArray![index] as! Ride
            }
            infoWindow.name.text = rideObj.ownerName
            if ((rideObj.poolRideType == "N") || (rideObj.rideType == "N")) {
                
                infoWindow.offerVehicle.text = "Needed Ride on " + (rideObj.dateTime)!
                marker.icon = UIImage(named: "marker_profile_orange")
                infoWindow.seatAvailable.text = ""
                infoWindow.seatLeftTag.isHidden = true
                infoWindow.sepraterLine.isHidden = true
            }else{
                infoWindow.offerVehicle.text = "Offering Vehicle on " + (rideObj.dateTime)!
                
                infoWindow.seatAvailable.text = (rideObj.seatsLeft! == "0") ? "No Seat left" : String(format: "%@", rideObj.seatsLeft!)
                infoWindow.seatLeftTag.isHidden = false
                infoWindow.sepraterLine.isHidden = false
                marker.icon = UIImage(named: "marker_ride_orange")
                
            }
            infoWindow.destinationLbl.text = rideObj.destination
            
            if rideObj.via != nil {
                infoWindow.via.text = "VIA- " + (rideObj.via)!
            } else {
                infoWindow.via.text = ""
            }
            
            infoWindow.address.text = rideObj.source
            infoWindow.markerWindowButton.addTarget(self, action: #selector(markerWindowButtonTapped), for:.touchUpInside)
            infoWindow.markerWindowButton.tag = index
            if(rideObj.ownerProfilePicStatus == "1"){
                if let url = rideObj.ownerProfilePicUrl{
                    infoWindow.profile.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                }
                else{
                    if let url = rideObj.ownerFBID{
                        infoWindow.profile.kf.setImage(with: URL(string: "https://graph.facebook.com/\(url)/picture?type=large&return_ssl_resources=1"))
                    }
                }
            }
            else{
                infoWindow.profile.image = UIImage(named: "placeholder")
            }
            
            if((rideObj.poolRideType == "N") || (rideObj.rideType == "N")){
                if(rideObj.createdBy == UserInfo.sharedInstance.userID){
                    //  showInvite()
                    infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                    infoWindow.joinButton.isHidden = false
                    infoWindow.offerRideButton.isHidden = true
                    infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                    
                }
                else{
                    if let poolStatus = rideObj.poolStatus{
                        if let requestedStatus = rideObj.requestedStatus{
                            if(poolStatus == "P" && requestedStatus == "P"){
                                //    showCancel()
                                infoWindow.joinButton .setTitle("CANCEL", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(cancelButtonTapped), for:.touchUpInside)
                                
                                
                            }
                            else if(poolStatus == "J"){
                                // showInvite()
                                infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                            }
                            else if((poolStatus == "U")||(poolStatus == "R")||(poolStatus == "C")||(poolStatus == "P")){
                                // showOfferVehicle()
                                //  showJoin()
                                infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = false
                                infoWindow.offerRideButton .setTitle("OFFER A VEHICLE", for: UIControlState())
                                infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                                
                                infoWindow.offerRideButton.addTarget(self, action: #selector(offerRideButtonTapped), for:.touchUpInside)
                                
                            }
                            else if(poolStatus == "F"){
                                // showAccept()
                                // showReject()
                                infoWindow.joinButton .setTitle("ACCEPT", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = false
                                infoWindow.offerRideButton .setTitle("REJECT", for: UIControlState())
                                infoWindow.joinButton.addTarget(self, action: #selector(acceptButtonTapped), for:.touchUpInside)
                                
                                infoWindow.offerRideButton.addTarget(self, action: #selector(rejectButtonTapped), for:.touchUpInside)
                                
                            }
                            else{
                                //Nothing
                                infoWindow.joinButton.isHidden = true
                                infoWindow.offerRideButton.isHidden = true
                            }
                        }
                    }
                    else{
                        //showOfferVehicle()
                        // showJoin()
                        infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                        infoWindow.joinButton.isHidden = false
                        infoWindow.offerRideButton.isHidden = false
                        infoWindow.offerRideButton .setTitle("OFFER A VEHICLE", for: UIControlState())
                        infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                        
                        infoWindow.offerRideButton.addTarget(self, action: #selector(offerRideButtonTapped), for:.touchUpInside)
                    }
                }
            }
            else if((rideObj.poolRideType == "O") || (rideObj.rideType == "O")){
                if(rideObj.createdBy == UserInfo.sharedInstance.userID){
                    //  showInvite()
                    infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                    infoWindow.joinButton.isHidden = false
                    infoWindow.offerRideButton.isHidden = true
                    infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                    
                }
                else{
                    if let poolStatus = rideObj.poolStatus{
                        if let requestedStatus = rideObj.requestedStatus{
                            if(poolStatus == "P" && requestedStatus == "P"){
                                // showCancel()
                                infoWindow.joinButton .setTitle("CANCEL", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(cancelButtonTapped), for:.touchUpInside)
                                
                            }
                            else if(poolStatus == "J"){
                                //  showInvite()
                                infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                                
                            }
                            else if((poolStatus == "U")||(poolStatus == "R")||(poolStatus == "C")||(poolStatus == "P")){
                                // showJoin()
                                infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                            }
                            else if(poolStatus == "F"){
                                //   showAccept()
                                //   showReject()
                                infoWindow.joinButton .setTitle("ACCEPT", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = false
                                infoWindow.offerRideButton .setTitle("REJECT", for: UIControlState())
                                infoWindow.joinButton.addTarget(self, action: #selector(acceptButtonTapped), for:.touchUpInside)
                                
                                infoWindow.offerRideButton.addTarget(self, action: #selector(rejectButtonTapped), for:.touchUpInside)
                            }
                            else{
                                //Nothing
                                infoWindow.joinButton.isHidden = true
                                infoWindow.offerRideButton.isHidden = true
                            }
                        }
                    }
                    else{
                        //  showJoin()
                        infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                        infoWindow.joinButton.isHidden = false
                        infoWindow.offerRideButton.isHidden = true
                        infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                    }
                }
            }
            infoWindow.center = mapView.center
            self.mapView.addSubview(infoWindow)
            sourceLoc = rideObj.source!
            sourceLat = rideObj.sourceLat!
            sourceLong = rideObj.sourceLong!
            destloc = rideObj.destination!
            destLat = rideObj.destLat!
            destLong = rideObj.destLong!
            poolTaxiBooking = rideObj.poolTaxiBooking!
            poolId = rideObj.poolID!
            rideType = rideObj.poolRideType!
            ownerName = rideObj.ownerName!
            rideCost = rideObj.cost!
            rideDistance = rideObj.distance!
            poolScope = rideObj.poolScope!
            if(sourceLat != "" && sourceLong != "" && data?.wpLat != nil && data?.wpLat != "" && data?.wpLong != nil && data?.wpLong != ""){
                self.addRideOverlayToMapView(sourceLat,sourceLang: sourceLong,distinationLat: data!.wpLat!,distinationLang: data!.wpLong!)
            }
        }
        return true
    }
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            contactListArray.removeAllObjects()
            mobileMutArr.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject]{
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = poolId
            vc.triggerFrom = "wpMap"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    self.contactListArray.removeAllObjects()
                    self.mobileMutArr.removeAllObjects()
                    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                    for record:ABRecord in contactList as [AnyObject]{
                        let contactPerson: ABRecord = record
                        let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                        if(contactName != "") {
                            var personNumber = ""
                            let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                            if(phones != "")
                            {
                                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                {
                                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                    {
                                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                        personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                        let areaCode = personNumber.characters.count-10
                                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                        if(areaCode > 0) {
                                            personNumber = personNumber.substring(from: startIndex)
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            self.mobileMutArr.add(personNumber)
                            let nameString = "\(contactName) \(personNumber)"
                            self.contactListArray.add(nameString)
                        }
                    }
                    
                    let vc = ContactsListViewController()
                    vc.contactListArr = self.contactListArray
                    vc.mobileMutArr = self.mobileMutArr
                    vc.poolID = self.poolId
                    vc.triggerFrom = "wpMap"
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObject()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg("", wpKey: "", poolID: poolId)
    }

    func grpMsg_clicked() {
        self.dismiss(animated: true, completion: nil)
        showIndicator("Fetching members.")
        WorkplaceRequestor().getWPMembers("", groupID: poolScope, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = InviteGroupMembersViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.wpDetails = (object as! WPMembersResponseData).groupDetails
            vc.membersList = (object as! WPMembersResponseData).members
            self.navigationController?.pushViewController(vc, animated: true)
        }, failure: { (error) in
            hideIndicator()
        })
    }

    func inviteButtonTapped() {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            
            let inviteView = InviteWPRideView()
            let alertViewHolder =   AlertContentViewHolder()
            alertViewHolder.heightConstraintValue = 95
            alertViewHolder.view = inviteView
            
            inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
            inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
            inviteView.groupMsgBtn.addTarget(self, action: #selector(grpMsg_clicked), for: .touchDown)
            
            AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
            
        }
    }
    
    func cancelButtonTapped() {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel this ride?", okButtonTitle: "Yes", okAction: {
                //from memberslist - FR
                //from ridedetail & ridelist = JR
                showIndicator("Cancelling Ride.")
                RideRequestor().cancelRequest(self.poolId, userId: UserInfo.sharedInstance.userID, cancelrequestType: "JR", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Cancel Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            if self.isHomeToWork == true {
                                self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
                            }else{
                                self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
                            }
                        })
                    }
                    else{
                        AlertController.showAlertFor("Cancel Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
                }, cancelButtonTitle: "Later", cancelAction: {
            })
        }
    }
    
    func acceptRide() {
        AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Accepting ride request..")
            RideRequestor().acceptRideRequest(self.poolId, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Accept Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        if self.isHomeToWork == true {
                            self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
                        }else{
                            self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
                        }
                    })
                }else{
                    AlertController.showAlertFor("Accept Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    
    func acceptButtonTapped() {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            if(self.poolTaxiBooking != "1" && self.rideCost != "0" && self.rideDistance != "" && self.rideDistance != "0") {
                ridePoints = Int(self.rideCost)! * Int(round(Double(self.rideDistance)!))
                if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                    if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                        AlertController.showAlertFor("Accept Ride", message: "\("SORRY! You cannot accept this ride request because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and accept this ride request.")", okButtonTitle: "RECHARGE", okAction: {
                            let vc = MyAccountViewController()
                            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                            }, cancelButtonTitle: "CANCEL", cancelAction: {})
                    } else {
                        acceptRide()
                    }
                } else {
                    acceptRide()
                }
            } else {
                acceptRide()
            }
        }
    }
    
    func rejectButtonTapped() {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
                showIndicator("Rejecting ride request..")
                RideRequestor().rejectRideRequest(self.poolId, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Reject Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            if self.isHomeToWork == true {
                                self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
                            }else{
                                self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
                            }
                        })
                    }else{
                        AlertController.showAlertFor("Reject Ride", message: object as? String)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
            }, cancelButtonTitle: "Cancel") {
            }
        }
    }
    
    func showMembersDetail (_ userId: String?) {
        showIndicator("Getting member's detail..")
        RideRequestor().getMembersDetails(userId!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                self.hideInfoWindow()
                let vc = MemberDetailViewController()
                vc.dataSource = (object as? MemberDetailResponse)?.data?.userInfo
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
        
    }
    // reset custom infowindow whenever marker is tapped
    @objc(mapView:didTapInfoWindowOfMarker:) func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let index:Int! = Int(marker.accessibilityLabel!)
        if index == 1313 {
            self.showWPDetail()
        }
    }
    
    @objc(mapView:didTapAtCoordinate:) func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        hideInfoWindow()
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
    }
    
    @objc(mapView:idleAtCameraPosition:) func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
    }
    
//    func mapView(_ mapView: GMSMapView, didTapOverlay overlay: GMSOverlay) {
//        // overlay.fillColor = UIColor.whiteColor()
//        // overlay.map?.
//        let selectedPolyline : GMSPolyline = overlay as! GMSPolyline
//        
//        let selectedIndex = Int(selectedPolyline.title!)
//        let polylinePath  = self.polylineMutArray?[selectedIndex!] as! String
//        let viaStr  = self.viaMutArray?[selectedIndex!] as! String
//        newRideReqObj.via = viaStr
//        newRideReqObj.waypoints = polylinePath
//        
//        for index in 0..<(self.polylineMutArray?.count)! {
//            if index == 0 {
//                self.routePolylineIndex0?.strokeColor = Colors.POLILINE_GRAY;
//            }
//            
//            if index == 1 {
//                self.routePolylineIndex1?.strokeColor = Colors.POLILINE_GRAY
//            }
//            
//            if index == 2  {
//                self.routePolylineIndex2?.strokeColor = Colors.POLILINE_GRAY
//            }
//        }
//        selectedPolyline.zIndex = 100
//        if(isHomeToWork == true) {
//            selectedPolyline.strokeColor = Colors.POLILINE_GREEN
//        } else {
//            selectedPolyline.strokeColor = Colors.POLILINE_ORANGE
//        }
//    }
    
    func markerWindowButtonTapped(_ button : UIButton){
        
        let rideObj = (self.offerDetailView.isHidden == true ) ? self.oRideMutArray![button.tag] as! Ride : self.nRideMutArray![button.tag] as! Ride
        showIndicator("Fetching Ride Details")
        RideRequestor().getRideDetails(rideObj.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                self.hideInfoWindow()
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                vc.isFirstLevel = true
                vc.data = object as? RideDetail
                vc.wpData = self.data
                vc.isCreatedRide = false
                vc.triggerFrom = "place"
                vc.inviteFlag = "wpMap"
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func customMarkerWindowTapped(_ button: UIButton) {
        showMembersDetail(wpMembers![button.tag].uID)
    }
    // MARK:-  Get Ride Type.
    func getISRideType()  {
        //TODO : Need to check why this value is getting nil or no data...
        if dataSourceToOfc == nil {
            return
        }
        
        if (self.isHomeToWork == true) {
            var rowsCnt = 0
            if let list = dataSourceToOfc{
                rowsCnt = list.count
            }
            self.nRideMutArray?.removeAllObjects()
            self.oRideMutArray?.removeAllObjects()
            var rideDataObject: Ride!
            for index in 0..<rowsCnt{
                rideDataObject  = dataSourceToOfc![index]
                if rideDataObject.poolRideType == "N" {
                    nRideMutArray! .add(rideDataObject)
                }else{
                    oRideMutArray! .add(rideDataObject)
                }
            }
        }else{
            var rowsCnt = 0
            if let list = dataSourceToHome{
                rowsCnt = list.count
            }
            var rideDataObject: Ride!
            self.nRideMutArray?.removeAllObjects()
            self.oRideMutArray?.removeAllObjects()
            
            if rowsCnt <= 0 {
                return
            }
            
            for index in 0..<rowsCnt{
                rideDataObject  = dataSourceToHome![index]
                if rideDataObject.poolRideType == "N" {
                    nRideMutArray! .add(rideDataObject)
                }else{
                    oRideMutArray! .add(rideDataObject)
                }
            }
        }
    }
    
    // MARK:- Create New Ride Using Submit Button...
    @IBAction func createRideButtonClicked(_ sender: AnyObject) {
        createRideRequest()
    }
    
    
    @IBAction func loadSearchScreenButtonClicked(_ button:UIButton) {
        
    }
    
    func rideWithMe(_ button: UIButton) {
        AlertController.showAlertFor("RIDE WITH ME", message: "\("You would be inviting \"")\(wpMembers![button.tag].fname!)\("\" to ride with you. Would you like to continue?")", okButtonTitle: "YES", okAction: {
            self.userMobileNo = self.wpMembers![button.tag].mobile!
            self.hideInfoWindow()
            self.createRideRequest()
            }, cancelButtonTitle: "LATER", cancelAction: nil)
    }
    
    // MARK:- Test Requiest
    func createRideRequest() {
        
        newRideReqObj.pool_scope = data!.wpID // (group_id of the workplace inside which ride is being created)
        if(data?.wpScope == "1") {
            newRideReqObj.pool_shared = "0"
        } else {
            newRideReqObj.pool_shared = "1"
        }
        if(newRideReqObj.flip == "1"){
            newRideReqObj.comingtime = prettyDateStringFromDate(datePickerView.date, toFormat: "HH:mm")
            newRideReqObj.goingtime = ""
        }
        else{
            newRideReqObj.goingtime = prettyDateStringFromDate(datePickerView.date, toFormat: "HH:mm")
            newRideReqObj.comingtime = ""
        }
        newRideReqObj.ridemessage = ""
        newRideReqObj.return_on = "0"
        newRideReqObj.pool_shared = "1"
        newRideReqObj.cost_type =  "PK"
        newRideReqObj.pool_category = "2"
        newRideReqObj.created_from = "2"
        createRide()
    }
    
    //MARK:- Creat New Ride Service Call.
    func createRide()
    {
        if(newRideReqObj.ridetype == "O"){
            let todayDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.string(from: todayDate) as String
            if(newRideReqObj.vehregno == "" || newRideReqObj.vehregno == nil){
                AlertController.showAlertFor("Create Ride", message: "Please provide vehicle details.", okButtonTitle: "Ok", okAction: nil)
                return
            } else if(newRideReqObj.vehregno != "" && data?.wpInsuranceStatus == "1" && UserInfo.sharedInstance.insuranceId == "") {
                AlertController.showAlertFor("Create Ride", message: "Please provide vehicle insurance details to proceed further.", okButtonTitle: "Ok", okAction: {
                    let vc = RegisterNewVehicleController()
                    vc.isUpdateReqneedtobeSend = "1"
                    vc.isFromWp = "1"
                    vc.noVehicle = true
                    if(self.data?.wpInsuranceStatus != "" && self.data?.wpInsuranceStatus != nil && self.data?.wpInsuranceStatus == "1") {
                        vc.isWpInsuranceStatus = true
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                })
                return
            } else if(newRideReqObj.vehregno != "" && data?.wpInsuranceStatus == "1" && UserInfo.sharedInstance.insuranceId != "" && UserInfo.sharedInstance.insuranceExpiryDate.compare(date) == ComparisonResult.orderedAscending) {
                AlertController.showAlertFor("Create Ride", message: "Please update vehicle insurance details to proceed further.", okButtonTitle: "Ok", okAction: {
                    let vc = RegisterNewVehicleController()
                    vc.isUpdateReqneedtobeSend = "1"
                    vc.isFromWp = "1"
                    vc.noVehicle = true
                    if(self.data?.wpInsuranceStatus != "" && self.data?.wpInsuranceStatus != nil && self.data?.wpInsuranceStatus == "1") {
                        vc.isWpInsuranceStatus = true
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                })
                return
            }
        }
        
        showIndicator("Creating Ride..")
        WorkplaceRequestor().addWPRide(self.newRideReqObj, success: { (success, object) in
            hideIndicator()
            if(success){
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! NeedRideResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            vc.isFirstLevel = true
                            vc.data = object as? RideDetail
                            vc.wpData = self.data
                            vc.isCreatedRide = true
                            vc.triggerFrom = "place"
                            vc.inviteFlag = "wpMap"
                            vc.mobileNo = self.userMobileNo
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! NeedRideResponse).code == "1811") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! NeedRideResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    //MARK:- Update  Ride Every time when time changes/ file Address And change cost others.
    func getWpRidesDetails(_ wpId: String, reqTimeDate: Date, startLat: String, startLong: String, endLat: String, endLong: String) {
        if(ISSEARCHRESULT) {
            self.dataSourceToOfc = SEARCHSOURCETOOFC
            self.dataSourceToHome = SEARCHSOURCETOHOME
            if(self.isHomeToWork == true) {
                self.addOverlayToMapView(startLat,sourceLong: startLong,destinationLat: endLat,destinationLong: endLong)
            } else {
                self.addOverlayToMapView(endLat,sourceLong: endLong,destinationLat: startLat,destinationLong: startLong)
            }
            self.getISRideType()
            if (self.isGetRide == true){
                self.loadGetRideDetails (false)
            }
            
            if(self.isOfferRide == true){
                self.loadOfferRide()
            }
        } else {
            if(homeAddressUpdated) {
                updateMapWithMember(wpId, reqTimeDate: reqTimeDate, startLat: startLat, startLong: startLong, endLat: endLat, endLong: endLong)
                homeAddressUpdated = false
            } else {
                updateMap(wpId, reqTimeDate: reqTimeDate, startLat: startLat, startLong: startLong, endLat: endLat, endLong: endLong)
            }
        }
    }
    
    func updateMapWithMember(_ wpId: String, reqTimeDate: Date, startLat: String, startLong: String, endLat: String, endLong: String) {
        showIndicator("Fetching Rides.")
        WorkplaceRequestor().getWPRides(newRideReqObj.selected_dates!, reqTime: prettyDateStringFromDate(reqTimeDate, toFormat: "hh:mm a"), groupID: (data?.wpID)!, success: { (success, object) in
            WorkplaceRequestor().getWPMembers("", groupID: (self.data?.wpID)!, distanceRange: "1", homeLat: self.selectedHLatitute, homeLon: self.selectedHLongitute, success: { (success, objectMembers) in
                hideIndicator()
                
                self.dataSourceToOfc = (object as! WPRidesResponse).pools?.officeGoing
                self.dataSourceToHome = (object as! WPRidesResponse).pools?.homeGoing
                self.wpMembers = (objectMembers as! WPMembersResponseData).members
                self.mapView.clear()
                if(self.isHomeToWork == true) {
                    self.addOverlayToMapView(startLat,sourceLong: startLong,destinationLat: endLat,destinationLong: endLong)
                } else {
                    self.addOverlayToMapView(endLat,sourceLong: endLong,destinationLat: startLat,destinationLong: startLong)
                }
                self.getISRideType()
                if (self.isGetRide == true){
                    self.loadGetRideDetails (false)
                }
                
                if(self.isOfferRide == true){
                    self.loadOfferRide()
                }
            }) { (error) in
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func updateMap(_ wpId: String, reqTimeDate: Date, startLat: String, startLong: String, endLat: String, endLong: String) {
        showIndicator("Fetching Rides.")
        WorkplaceRequestor().getWPRides(newRideReqObj.selected_dates!, reqTime: prettyDateStringFromDate(reqTimeDate, toFormat: "hh:mm a"), groupID: (data?.wpID)!, success: { (success, object) in
                hideIndicator()
                
                self.dataSourceToOfc = (object as! WPRidesResponse).pools?.officeGoing
                self.dataSourceToHome = (object as! WPRidesResponse).pools?.homeGoing
                self.mapView.clear()
                if(self.isHomeToWork == true) {
                    self.addOverlayToMapView(startLat,sourceLong: startLong,destinationLat: endLat,destinationLong: endLong)
                } else {
                    self.addOverlayToMapView(endLat,sourceLong: endLong,destinationLat: startLat,destinationLong: startLong)
                }
                self.getISRideType()
                if (self.isGetRide == true){
                    self.loadGetRideDetails (false)
                }
                
                if(self.isOfferRide == true){
                    self.loadOfferRide()
                }
        }) { (error) in
            hideIndicator()
        }
    }
    
    // MARK:-  Enabled or desable...
    @IBAction func trafficButtonClicked(_ button: UIButton) {
        if button.isSelected {
            self.mapView.isTrafficEnabled = false
        }else{
            self.mapView.isTrafficEnabled = true
        }
        button.isSelected = !button.isSelected
    }
    
    // MARK:- Get Ride/Offer/Traffic Button Action
    @IBAction func getRideButtonClicked(_ button: UIButton) {
        self.isDefaultVechile = false
        self.isGetRide = true
        self.isOfferRide = false
        hideInfoWindow()
        if(ISSEARCHRESULT) {
            if(self.oRideMutArray?.count == 0) {
                AlertController.showToastForInfo("No ride found.")
            } else {
                AlertController.showToastForInfo("\((self.oRideMutArray?.count)!) \("ride(s) found.")")
            }
        }
        if self.isHomeToWork == true {
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
        }else{
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
        }
        
    }
    
    @IBAction func offerVehicleButtonClicked(_ sender: AnyObject) {
        self.isGetRide = false
        self.isOfferRide = true
        self.isDefaultVechile = true
        hideInfoWindow()
        if(ISSEARCHRESULT) {
            if(self.nRideMutArray?.count == 0) {
                AlertController.showToastForInfo("No ride found.")
            } else {
                AlertController.showToastForInfo("\((self.nRideMutArray?.count)!) \("ride(s) found.")")
            }
        }
        if self.isHomeToWork == true {
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
        }else{
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
        }
        self.getConfigData()
    }
    
    // MARK:- Update Address using flip Button
    @IBAction func flipButtonClicked(_ sender: UIButton) {
        self.isDefaultVechile = false
        hideInfoWindow()
        DispatchQueue.main.async(execute: {
            self.initallyflipAddressLocation(sender.isSelected)
            sender.isSelected = !sender.isSelected
        });
    }
    
    //MARK:- Bike Button Clicked and Load Add vechicle Screens.
    @IBAction func bikeButtonClicked(_ button: UIButton) {
        self.isDefaultVechile = true
        hideInfoWindow()
        if(button.currentTitle == "Add vehicle"){
            let vc = RegisterNewVehicleController()
            vc.isFromWp = "1"
            vc.noVehicle = true
            if(data?.wpInsuranceStatus != "" && data?.wpInsuranceStatus != nil && data?.wpInsuranceStatus == "1") {
                vc.isWpInsuranceStatus = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = AddVehicleViewController()
            vc.isFromWp = "1"
            if(data?.wpInsuranceStatus != "" && data?.wpInsuranceStatus != nil && data?.wpInsuranceStatus == "1") {
                vc.isWpInsuranceStatus = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK:- Cost Button Clicked Need to Get cost of ride Details.
    @IBAction func costButtonClicked(_ sender: AnyObject) {
        self.isDefaultVechile = true
        hideInfoWindow()
        let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
        let vc : CostViewController = storyboard.instantiateViewController(withIdentifier: "CostViewController") as! CostViewController
        vc.data = data
        vc.vehicleConfig = self.vehicleConfig
        vc.vehicleDefaultDetails = self.vehicleDefaultDetails as VehicleDefaultResponse
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext //All objects and view are transparent
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func pickerButtonClicked(_ button: UIButton) {
        self.isDefaultVechile = false
        self.anyBgView?.isHidden = true
        self.anyPickerViewHeight?.constant = 0.0
        self.submitButtonHeight.constant = 0.0;
        hideInfoWindow()
        if button.isSelected {
            self.dateTimePickerView.isHidden = true
            self.dateViewHeight.constant = 0.0
            self.submitButtonHeight.constant = 50.0;
        }else{
            self.dateTimePickerView.isHidden = false
            self.dateViewHeight.constant = 250.0
            self.submitButtonHeight.constant = 0.0;
            
        }
        self.submitButtonView.isHidden = !self.dateTimePickerView.isHidden
        button.isSelected = !button.isSelected
    }
    
    @IBAction func timePickerSelected(_ sender: AnyObject) {
        self.isDefaultVechile = false
        hideInfoWindow()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, hh:mm a"
        let strDate = dateFormatter.string(from: datePickerView.date)
        newRideReqObj.selected_dates = prettyDateStringFromDate(datePickerView.date, toFormat: "dd-MMM-yyyy")
        self.selectedDateTime = datePickerView.date
        self.timeLabel.text = strDate
    }
    
    @IBAction func pickerDoneButtonClicked(_ sender: AnyObject) {
        self.isDefaultVechile = false
        self.pickerButton.isSelected = false
        self.dateTimePickerView.isHidden = true
        self.dateViewHeight.constant = 0.0
        self.submitButtonHeight.constant = 50.0;
        self.submitButtonView.isHidden = !self.dateTimePickerView.isHidden
        newRideReqObj.selected_dates = prettyDateStringFromDate(datePickerView.date, toFormat: "dd-MMM-yyyy")
        self.selectedDateTime = datePickerView.date
        if self.isHomeToWork == true {
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
        }else{
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
        }
    }
    
    // MARK:- Show Alert View
    func setHomeAddressShowAlert(_ addressName : String){
        self.dismiss(animated: true, completion: {
            
            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
            let vc : CustomeAlertViewController = storyboard.instantiateViewController(withIdentifier: "CustomeAlertViewController") as! CustomeAlertViewController
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext //All objects and view are transparent
            vc.address = addressName
            vc.latitude = self.selectedLatitute
            vc.longitude = self.selectedLongitute
            self.present(vc, animated: true, completion: nil)
            
        })
    }
    
    //MARK:- Cost AlertView
    func getPriceNotification (_ notification:Notification) -> Void {
        self.isDefaultVechile = true
        guard let userInfo = notification.userInfo else { return }
        
        if let seatValue = userInfo["seat"] {
            self.newRideReqObj.veh_capacity = String(describing: seatValue)
        }
        
        if let costValue = userInfo["cost"] {
            newRideReqObj.cost = String(describing: costValue)
            self.priceButton.setTitle(String(describing: costValue) + " / km", for: UIControlState())
            self.priceButton.isUserInteractionEnabled = true
        }
    }
    
    func updateHomeDefaultAddressNotification (_ notification:Notification) -> Void {
        self.isDefaultVechile = false
        homeAddressUpdated = true
        var homeAddress = UserInfo.sharedInstance.homeAddress
        guard let userInfo = notification.userInfo else { return }
        if let placeName = userInfo["HomeAddress"] as? String {
            homeAddress = placeName
        }
        
        if (isHomeToWork == true) {
            self.leftSideDetailLabel.text = homeAddress
        }else{
            self.rightSideDetailsLabel.text = homeAddress
        }
        
        var latitute = UserInfo.sharedInstance.homeLatValue as String
        if let lat = userInfo["latitude"] as? String {
            latitute = lat
        }
        self.selectedLatitute = latitute
        newRideReqObj.toLat = self.selectedLatitute
        var longitute = UserInfo.sharedInstance.homeLongValue
        if let long = userInfo["longitude"] as? String {
            longitute = long
        }
        newRideReqObj.toLong = self.selectedLongitute
        self.selectedLongitute = longitute
        self.selectedHLatitute = latitute
        self.selectedHLongitute = longitute
        self.selectedHLocation = homeAddress
        if self.isHomeToWork == true {
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
        }else{
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
        }
        
    }
    
    //MARK:- Update AlertView
    func updateHomeAddressNotification (_ notification:Notification) -> Void {
        self.isDefaultVechile = false
        var homeAddress = UserInfo.sharedInstance.homeAddress
        guard let userInfo = notification.userInfo else { return }
        if let placeName = userInfo["HomeAddress"] as? String {
            homeAddress = placeName
            
        }
        self.selectedHLocation = homeAddress
        newRideReqObj.toLocation = homeAddress
        
        self.selectedLatitute =  UserInfo.sharedInstance.homeLatValue as String
        if let lat = userInfo["latitude"] as? Double {
            self.selectedLatitute = String(format:"%f", lat)
            
        }
        newRideReqObj.toLat = self.selectedLatitute
        
        self.selectedLongitute = UserInfo.sharedInstance.homeLongValue
        if let long = userInfo["longitude"] as? Double {
            self.selectedLongitute = String(format:"%f", long)
        }
        newRideReqObj.toLong = self.selectedLongitute
        self.selectedHLatitute = self.selectedLatitute
        self.selectedHLongitute = self.selectedLongitute
        
        
        self.setHomeAddressShowAlert(homeAddress)
    }
    
    // MARK:- JOIN / Offer Button Tap
    
    func joinRide() {
        if(UserInfo.sharedInstance.shareMobileNumber == false){
            AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                self.getPickUpDropLocation("joinRide")
                }, cancelButtonTitle: "LATER", cancelAction: {
            })
        }
        else{
            getPickUpDropLocation("joinRide")
        }
    }
    
    func joinButtonTapped () {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                if(self.poolTaxiBooking != "1" && self.rideCost != "0" && self.rideDistance != "" && self.rideDistance != "0") {
                    ridePoints = Int(self.rideCost)! * Int(round(Double(self.rideDistance)!))
                    if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                        if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                            AlertController.showAlertFor("Join Ride", message: "\("SORRY! You cannot join this ride because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and join this ride.")", okButtonTitle: "RECHARGE", okAction: {
                                let vc = MyAccountViewController()
                                self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                                }, cancelButtonTitle: "CANCEL", cancelAction: {})
                        } else {
                            joinRide()
                        }
                    } else {
                        joinRide()
                    }
                } else {
                    joinRide()
                }
            }
        }
    }
    
    func offerRideButtonTapped() {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            getPickUpDropLocation("offerVehicle")
        }
    }
    
    func getPickUpDropLocation(_ triggerFrom: String)
    {
        let joinOrOfferRideObj = JoinOrOfferRide()
        let vc = RideDetailMapViewController()
        vc.sourceLong = self.sourceLong
        vc.sourceLat = self.sourceLat
        vc.sourceLocation = self.sourceLoc
        vc.destLong = self.destLong
        vc.destLat = self.destLat
        vc.destLocation = self.destloc
        vc.poolTaxiBooking = self.poolTaxiBooking
        vc.poolId = self.poolId
        vc.rideType = self.rideType
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.hidesBottomBarWhenPushed = true
        vc.triggerFrom = triggerFrom
        vc.isWP = true
        vc.wpInsuranceStatus = (data?.wpInsuranceStatus)!
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
            joinOrOfferRideObj.poolID = self.poolId
            joinOrOfferRideObj.vehicleRegNo = ""
            joinOrOfferRideObj.vehicleCapacity = ""
            joinOrOfferRideObj.cost = "0"
            joinOrOfferRideObj.rideMsg = ""
            joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
            joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
            joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
            joinOrOfferRideObj.dropPoint = dropLoc as? String
            joinOrOfferRideObj.dropPointLat = dropLat as? String
            joinOrOfferRideObj.dropPointLong = dropLong as? String
            
            if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                showIndicator("Joining Ride..")
                OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                            if self.isHomeToWork == true {
                                self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
                            }else{
                                self.getWpRidesDetails((self.data?.wpID)!, reqTimeDate: self.datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
                            }
                        })
                    }
                    else{
                        AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
                
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Get Vehicle Default Details Service Call.
    func getVehicleDefaultConfigData()
    {
        showIndicator("Fetching vehicle Default.")
        RideRequestor().getDefaultVehicle({ (success, object) in
            hideIndicator()
            if (success) {
                //TODO:
                self.vehicleDefaultDetails = object as! VehicleDefaultResponse
                if(self.newRideReqObj.ridetype == "O"){
                    self.offerPriceView.isHidden = false
                    
                }else{
                    self.offerPriceView.isHidden = true
                }
                var cost = 0
                self.priceButton.isUserInteractionEnabled = true
                self.vehicleDetailButton .setTitle(self.vehicleDefaultDetails.data?.first?.vehicleRegistrationNumber, for: UIControlState())
                
                if(self.vehicleDefaultDetails.data?.first?.vehicleType!.lowercased() == "bike") {
                    self.vehicleDetailButton.setTitle("Bike", for: UIControlState())
                    self.vehicleDetailButton.setImage(UIImage(named: "veh_bike"), for: UIControlState())
                    cost = Int(self.vehicleConfig.bike!)!
                } else if(self.vehicleDefaultDetails.data?.first?.vehicleType!.lowercased() == "auto") {
                    self.vehicleDetailButton.setTitle("Auto", for: UIControlState())
                    self.vehicleDetailButton.setImage(UIImage(named: "auto"), for: UIControlState())
                    
                } else if(self.vehicleDefaultDetails.data?.first?.vehicleType!.lowercased() == "car") {
                    self.vehicleDetailButton.setTitle("Car", for: UIControlState())
                    self.vehicleDetailButton.setImage(UIImage(named: "veh_car"), for: UIControlState())
                    cost = Int(self.vehicleConfig.car!)!
                    
                } else if(self.vehicleDefaultDetails.data?.first?.vehicleType!.lowercased() == "cab") {
                    self.vehicleDetailButton.setTitle("Cab", for: UIControlState())
                    self.vehicleDetailButton.setImage(UIImage(named: "cab"), for: UIControlState())
                    
                } else if(self.vehicleDefaultDetails.data?.first?.vehicleType!.lowercased() == "suv") {
                    cost = Int(self.vehicleConfig.suv!)!
                    self.vehicleDetailButton.setTitle("Suv", for: UIControlState())
                    self.vehicleDetailButton.setImage(UIImage(named: "suv"), for: UIControlState())
                } else if(self.vehicleDefaultDetails.data?.first?.vehicleType!.lowercased() == "tempo") {
                    self.vehicleDetailButton.setTitle("Tempo", for: UIControlState())
                    self.vehicleDetailButton.setImage(UIImage(named: "tempo"), for: UIControlState())
                } else if(self.vehicleDefaultDetails.data?.first?.vehicleType!.lowercased() == "bus") {
                    self.vehicleDetailButton.setTitle("Bus", for: UIControlState())
                    self.vehicleDetailButton.setImage(UIImage(named: "bus"), for: UIControlState())
                }
                
                self.priceButton.setTitle(String(cost) + " / km", for: UIControlState())
                self.priceButton.isUserInteractionEnabled = true
                self.newRideReqObj.cost = String(cost)
                self.newRideReqObj.vehregno = self.vehicleDefaultDetails.data?.first?.vehicleRegistrationNumber!
                if let vehCapacity = self.vehicleDefaultDetails.data?.first?.vehicleCapacity {
                    self.newRideReqObj.veh_capacity = String(Int(vehCapacity)!-1)
                }
                
            }else{
                self.offerPriceView.isHidden = true
                self.vehicleDetailButton .setTitle("Add vehicle", for: UIControlState())
                self.vehicleDetailButton .setImage(UIImage(named: "AddNewVehicle"), for: UIControlState())
                self.priceButton.isUserInteractionEnabled = false
                self.priceButton .setTitle("", for: UIControlState())
                self.priceButton .setImage(nil, for: UIControlState())
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    //MARK:- Get Vehicle Details Service Call.
    func getConfigData()
    {
        showIndicator("Fetching vehicle config.")
        RideRequestor().getVehicleConfig("workplace", groupID: (data?.wpID)!, success: { (success, object) in
            hideIndicator()
            if (!success) {
                return
            }
            var cost = 0
            self.vehicleConfig = object as! VehicleConfigResponse
            
            if (self.vehicleDefaultDetails.data?.first?.vehicleType?.lowercased() == "bike"){
                cost = Int(self.vehicleConfig.bike!)!
            }else if (self.vehicleDefaultDetails.data?.first?.vehicleType?.lowercased() == "car"){
                cost = Int(self.vehicleConfig.car!)!
            }else if (self.vehicleDefaultDetails.data?.first?.vehicleType?.lowercased() == "suv"){
                cost = Int(self.vehicleConfig.suv!)!
            }else{
                cost = 0
            }
            print("cost",cost)
            if(self.isOfferRide == true){
                self.offerPriceView.isHidden = false
                //self.priceButton .setTitle(String(cost) + "/ km", forState: UIControlState.Normal)
                //let fareImage = UIImage(named: "fare_green-1")
                //self.priceButton .setImage(fareImage, forState: UIControlState.Normal)
            }else{
                self.offerPriceView.isHidden = true
            }
            self.getVehicleDefaultConfigData()
            
        }) { (error) in
            hideIndicator()
        }
    }
    
    @IBAction func anyButtonClicked(_ button: UIButton) {
        
        self.dateTimePickerView.isHidden = true
        self.dateViewHeight.constant = 0.0
        if button.isSelected {
            self.anyBgView?.isHidden = true
            self.anyPickerViewHeight?.constant = 0.0
            self.submitButtonHeight.constant = 50.0;
        }else{
            self.anyBgView?.isHidden = false
            self.anyPickerViewHeight?.constant = 140.0
            self.submitButtonHeight.constant = 0.0;
            
        }
        self.submitButtonView.isHidden = !(self.anyBgView?.isHidden)!
        button.isSelected = !button.isSelected
        
    }
    @IBAction func anyPickerDoneButtonClicked(_ sender: AnyObject) {
        
        self.anyButton?.isSelected = false
        self.anyBgView?.isHidden = true
        self.anyPickerViewHeight?.constant = 0.0
        self.submitButtonHeight.constant = 50.0;
        self.submitButtonView.isHidden = !(self.anyBgView?.isHidden)!
        
        
        //TODO: Pass Any information.
        newRideReqObj.selected_dates = prettyDateStringFromDate(datePickerView.date, toFormat: "dd-MMM-yyyy")
        self.selectedDateTime = datePickerView.date
        if self.isHomeToWork == true {
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedHLatitute, startLong: self.selectedHLongitute, endLat: self.selectedWLatitute, endLong: self.selectedWLongitute)
        }else{
            self.getWpRidesDetails((data?.wpID)!, reqTimeDate: datePickerView.date, startLat: self.selectedWLatitute, startLong: self.selectedWLongitute, endLat: self.selectedHLatitute, endLong: self.selectedHLongitute)
        }
    }
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let valueSelected = pickerData[row] as String
        
        if (valueSelected == "Only Male"){
            //TODO: need to find how to update this values.
            newRideReqObj.poolType  = "M" //(A for Any, M for Male only, F for Female only)        }else if (valueSelected == "Male") {
            
        }else if (valueSelected == "Only Female"){
            newRideReqObj.poolType  = "F"
        }else{
            newRideReqObj.poolType  = "A"
        }
        
        self.anyButton.setTitle(valueSelected, for: UIControlState())
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        let titleData = pickerData[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: FONT_NAME.BOLD_FONT, size:  14.0)!])
        pickerLabel.textAlignment = .center
        pickerLabel.attributedText = myTitle
        return pickerLabel
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
    }
       
    func hideInfoWindow () {
        if memberInfoWindow != nil {
            memberInfoWindow.removeFromSuperview()
        }
        if customMarker != nil {
            customMarker.removeFromSuperview()
        }
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            if(isMemberInfoWindow == false) {
                for index in 0..<(self.mapMarkerMutArray?.count)!{
                    let markerPin = self.mapMarkerMutArray![index] as! GMSMarker
                    
                    if self.isGetRide == true {
                        markerPin.icon = UIImage(named:"marker_ride_green")
                    }else{
                        markerPin.icon = UIImage(named:"marker_profile_green")
                    }
                }
                
                self.routePolylineOrange.map = nil
                self.originSelectedRideMarker.map = nil
                self.destinationSelectedRideMarker.map = nil
                
                self.originMarker.icon = UIImage(named:"marker_home_green")//
                self.destinationMarker.icon = UIImage(named:"marker_wp_green")
            }
        }
    }
    
    func addWpMemberMarker() {
        var count = 0
        if(self.wpMembers != nil) {
            count = (self.wpMembers?.count)! as Int
        }
        if count == 0 {
            return
        }
        var flag = 0
        for var index in 0..<(count){
            if(flag > 0) {
                index -= flag
            }
            let wpMembersObj = self.wpMembers![index]
            if((wpMembersObj.uID == UserInfo.sharedInstance.userID) || (wpMembersObj.membershipStatus != "1" && wpMembersObj.membershipStatus != "0")) {
                self.wpMembers?.remove(at: index)
                flag += 1
                continue
            }
            if wpMembersObj.homeLat == nil || wpMembersObj.homeLon == nil {
                continue
            }
            
            let camera: GMSCameraPosition =
                GMSCameraPosition.camera(withLatitude: Double(wpMembersObj.homeLat!)!, longitude: Double(wpMembersObj.homeLon!)!, zoom: 12.0)
            let memberMarker = GMSMarker()
            memberMarker.position = camera.target
            memberMarker.snippet = wpMembersObj.homeLat
            if(isHomeToWork == true) {
                if(wpMembersObj.isActiveGoingUser == 1) {
                    memberMarker.icon = UIImage(named: "marker_member_active")
                } else {
                    memberMarker.icon = UIImage(named: "marker_member_inactive")
                }
            } else {
                if(wpMembersObj.isActiveComingUser == 1) {
                    memberMarker.icon = UIImage(named: "marker_member_active")
                } else {
                    memberMarker.icon = UIImage(named: "marker_member_inactive")
                }
            }
            //memberMarker.appearAnimation = GMSMarkerAnimation
            memberMarker.accessibilityLabel = "\(99999)\(index)"
            memberMarker.isTappable = true
            memberMarker.map = self.mapView
        }
    }
}
