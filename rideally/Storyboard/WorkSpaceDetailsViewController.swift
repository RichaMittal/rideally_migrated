//
//  WorkSpaceDetailsViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 07/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import AddressBook
import AddressBookUI
import FirebaseAnalytics
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


extension WorkSpaceDetailsViewController: GMSMapViewDelegate,GMSPanoramaViewDelegate, CLLocationManagerDelegate,ABPeoplePickerNavigationControllerDelegate,UIPopoverControllerDelegate, UIPopoverPresentationControllerDelegate,UITableViewDelegate, UITableViewDataSource {
    
}

class WorkSpaceDetailsViewController: UIViewController {
    
    var data: RideDetail?
    var wpData: WorkPlace?
    var waypoint: String!
    var isCreatedRide: Bool!
    var routePolyline: GMSPolyline!
    var responses: [InviteResult]?
    var polylineMutArray : NSMutableArray? = NSMutableArray()
    var makePolyLine : NSMutableArray? = NSMutableArray()
    var triggerFrom = ""
    var mobileNo = ""
    var signupFlow = false
    @IBOutlet weak var mapRideView: GMSMapView!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var viaLabel: UILabel!
    @IBOutlet weak var distinationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var memberLabel: UILabel!
    @IBOutlet weak var travelWith: UILabel!
    @IBOutlet weak var topTimeView: UIView!
    @IBOutlet weak var topDistanceOtherView: UIView!
    @IBOutlet weak var topMainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var sosButton: UIButton!
    
    // Offer Details
    @IBOutlet weak var line1: UILabel!
    @IBOutlet weak var line2: UILabel!
    @IBOutlet weak var line3: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var vechileType: UILabel!
    @IBOutlet weak var seatImage: UIImageView!
    @IBOutlet weak var seatValue: UILabel!
    @IBOutlet weak var priceImage: UIImageView!
    @IBOutlet weak var priceValue: UILabel!
    @IBOutlet weak var StartRideBtn: UIButton!
    
    @IBOutlet weak var imgMembers: UIImageView!
    var isSOSButtonPressed = false
    let locationManager = CLLocationManager()
    var addressBookController = ABPeoplePickerNavigationController()
    var btnConstraints: [NSLayoutConstraint]?
    
    let btnInvite = UIButton()
    let btnEdit = UIButton()
    let btnDelete = UIButton()
    let btnCancel = UIButton()
    let btnJoin = UIButton()
    let btnUnjoin = UIButton()
    let btnAccept = UIButton()
    let btnReject = UIButton()
    let btnOfferVehicle = UIButton()
    
    var onDeleteActionBlock: actionBlock?
    var onSelectBackBlock: actionBlock?
    var onUpdateSearchResultBlock: actionBlock?
    var fromSearchesult = false
    
    var isFirstLevel = false
    var taxiSourceID = ""
    var taxiDestID = ""
    var tripStarted = false
    var ridePoints = 0
    var inviteFlag = ""
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Ride Details"
        if(UserInfo.sharedInstance.availablePoints == 0 || UserInfo.sharedInstance.maxAllowedPoints == "") {
            getUserPoints()
        }
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "KInviteSendNotificationDetail"),object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotificationDetail"), object: nil)
        self.mapRideView.delegate = self
        updateRideDetails()
        _ = getBtnView()
        if(mobileNo != "") {
            sendInvitation()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isSOSButtonPressed = false
        sosButton.isHidden = true
        if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
            if(data?.currentStatus?.lowercased() != "c"){
                //if(data?.vehicleRegNo != nil){
                sosButton.isHidden = false
                //}
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goBack()
    {
        if(isFirstLevel){
            _ = self.navigationController?.popViewController(animated: true)
            onSelectBackBlock?()
            if(fromSearchesult) {
                fromSearchesult = false
                onUpdateSearchResultBlock?()
            }
        } else if(signupFlow) {
            let ins = UIApplication.shared.delegate as! AppDelegate
            ins.gotoHome()
        } else {
            _ = self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: Notification.Name(rawValue: "updateRidesList"), object: nil)
        }
    }
    
    func updateRideDetails() {
        self.mapRideView.clear()
        self.mapRideView.delegate = self
        self.initiallyUpdateUsingValue()
        
        if let wayPointStr = (self.data?.waypoints) {
            //if ((wayPointStr.characters.count != 0) && (wayPointStr != "null") && (wayPointStr != "")) {
            
            let prefix = String(wayPointStr.characters.prefix(1))
            if (prefix == "[") {
                var points = wayPointStr.replacingOccurrences(of: "\\n", with: "")
                points = points.replacingOccurrences(of: "\\r", with:"")
                let data = points.data(using: String.Encoding.utf8)
                
                var jsonResult: [[String: AnyObject]] = [] //<- use Swift type
                
                do{
                    try jsonResult =  JSONSerialization.jsonObject(with: data!, options: []) as! [[String: AnyObject]] //<- convert to Swift type, no need to specify options
                    
                } catch let error as NSError {
                    print(error)
                }
                
                let path = GMSMutablePath()
                
                for i in 0..<jsonResult.count {
                    // let dict = json!.objectAtIndex[i] as! [String:AnyObject]
                    if let dict = jsonResult [i] as? NSDictionary {
                        
                        let latTemp =  dict["lat"] as! Double
                        let longTemp =  dict["lng"] as! Double
                        path.add(CLLocationCoordinate2DMake(latTemp, longTemp))
                    }
                }
                
                self.routePolyline = GMSPolyline(path: path)
                self.routePolyline.strokeColor = Colors.GREEN_COLOR
                self.routePolyline.strokeWidth = 5.0
                self.routePolyline.map = self.mapRideView
                
                let bounds = GMSCoordinateBounds(path: path)
                mapRideView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
                
            }else{
                self.addRideWaypoint(wayPointStr)
            }
            //}
        }else{
            if let wayPointStr = (self.data?.waypoints) {
                self.addRideWaypoint(wayPointStr)
            }
        }
        self.addMarkersWithResponse()
    }
    func addRideWaypoint(_ points : String) {
        
        if points.characters.count == 0 || points == "0" {
            return
        }
        
        let path = GMSMutablePath(fromEncodedPath: points as String)
        self.routePolyline = GMSPolyline(path: path)
        self.routePolyline.strokeColor = Colors.GREEN_COLOR
        self.routePolyline.strokeWidth = 5.0
        self.routePolyline.map = self.mapRideView
        
        let bounds = GMSCoordinateBounds(path: path!)
        mapRideView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
    }
    
    func initiallyUpdateUsingValue() {
        imgMembers.isUserInteractionEnabled = true
        memberLabel.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
        imgMembers.addGestureRecognizer(tapGesture)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
        memberLabel.addGestureRecognizer(tapGesture1)
        
        self.sourceLabel.text = data?.source
        if let via = data?.via {
            self.viaLabel.text = "VIA- " + via
        } else {
            self.viaLabel.text = ""
        }
        
        if let destination = data?.destination {
            self.distinationLabel.text = destination
        }
        
        if data?.poolType == "A" {
            self.travelWith.text = "Any"
        }else if data?.poolType == "F" {
            self.travelWith.text = "Only Female"
        }else{
            self.travelWith.text = "Only Male"
        }
        var combineTime = ""
        if let date = data?.startDateDisplay {
            combineTime = date + " "
        }
        if let time = data?.startTimeDisplay {
            self.timeLabel.text = combineTime + time
        }
        if let distance = data?.distance {
            self.distanceLabel.text = distance + " KMs"
        }
        
        self.durationLabel.text = data?.duration
        if data?.poolTaxiBooking == "1"{
            self.memberLabel.text = data?.poolJoinedMembers
        }
        else{
            if let cnt = data?.poolJoinedMembers{
                self.memberLabel.text = "\(Int(cnt)! + 1)"
            }
        }
        
        if(data?.rideType == "O" || data?.poolType == "O"){
            self.showOfferRideDetails()
        }else{
            self.hideOfferRideDetails()
        }
    }
    
    func hideOfferRideDetails(){
        self.line1.isHidden = true
        self.line2.isHidden = true
        self.line3.isHidden = true
        self.vehicleImage.isHidden = true
        self.vechileType.isHidden = true
        self.seatImage.isHidden = true
        self.seatValue.isHidden = true
        self.priceImage.isHidden = true
        self.priceValue.isHidden = true
        //  self.carSeatView.hidden = true
        //  self.carSeatViewHeight.constant = 0
    }
    
    func showOfferRideDetails(){
        self.line1.isHidden = false
        self.line2.isHidden = false
        self.line3.isHidden = false
        
        self.priceImage.isHidden = false
        self.seatImage.isHidden = false
        
        self.seatValue.isHidden = false
        self.priceValue.isHidden = false
        self.vehicleImage.isHidden = false
        self.vechileType.isHidden = false
        
        if let seatsLeft = data?.seatsLeft {
            self.seatValue.text = seatsLeft
        }
        if let cost = data?.cost {
            self.priceValue.text = cost + " / km"
        }
        if let vehicleType = data?.vehicleType {
            self.vechileType.text = vehicleType
        }
        //        if let vehicleRegNo = data?.vehicleRegNo{
        //        }else{
        //        }
        //
        //        if (data?.seatsLeft)! == 0 {
        //        }else{
        //        }
        //
        //        if (data?.cost)! == 0 {
        //        }else{
        //        }
        
        var vehicleImage : UIImage!
        
        if let vehicleType = data?.vehicleType {
            if(vehicleType.lowercased() == "bike") {
                vehicleImage = UIImage(named: "veh_bike")
            } else if(vehicleType.lowercased() == "auto") {
                vehicleImage = UIImage(named: "auto")
            } else if(vehicleType.lowercased() == "car") {
                vehicleImage = UIImage(named: "veh_car")
            } else if(vehicleType.lowercased() == "cab") {
                vehicleImage = UIImage(named: "cab")
            } else if(vehicleType.lowercased() == "suv") {
                vehicleImage = UIImage(named: "suv")
            } else if(vehicleType.lowercased() == "tempo") {
                vehicleImage = UIImage(named: "tempo")
            } else if(vehicleType.lowercased() == "bus") {
                vehicleImage = UIImage(named: "bus")
            }
        }
        if let vehImg = vehicleImage {
            self.vehicleImage.image = vehImg
        }
    }
    
    func addOverlayToMapView(_ sourceLat: String,sourceLang: String,distinationLat: String,distinationLang: String ){
        self.mapRideView.clear()
        
        showIndicator("Loading Map path...")
        let sourceStr = "\(sourceLat),\(sourceLang)"
        let destinationStr = "\(distinationLat), \(distinationLang)"
        
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    self.addmarkers(legs)
                    self.addPolyLineWithEncodedStringInMap(routes as NSArray)
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }
    
    func addPoolUserMarker() {
        let count = (self.data?.poolUsers!.count)! as Int
        if count == 0 {
            return
        }
        
        for index in 0..<(count){
            let poolUsersObj = self.data!.poolUsers![index]
            
            if poolUsersObj.dropLatitude == nil || poolUsersObj.dropLongitude == nil {
                return
            }
            
            if poolUsersObj.pickupLatitude == nil || poolUsersObj.pickupLongitude == nil {
                return
            }
            
            let camera: GMSCameraPosition =
                GMSCameraPosition.camera(withLatitude: Double((poolUsersObj.pickupLatitude)!)!, longitude: Double((poolUsersObj.pickupLongitude)!)!, zoom: 12.0)
            
            let dropCamera: GMSCameraPosition =
                GMSCameraPosition.camera(withLatitude: Double((poolUsersObj.dropLatitude)!)!, longitude: Double((poolUsersObj.dropLongitude)!)!, zoom: 12.0)
            let pickupMarker = GMSMarker()
            pickupMarker.position = camera.target
            pickupMarker.snippet = poolUsersObj.pickupLocation
            pickupMarker.icon = UIImage(named: "map_G_HW_15x15")
            //pickupMarker.appearAnimation = GMSMarkerAnimation
            pickupMarker.accessibilityLabel = "\(index)"
            pickupMarker.map = self.mapRideView
            
            let dropMarker = GMSMarker()
            dropMarker.position = dropCamera.target
            dropMarker.snippet = poolUsersObj.pickupLocation
            dropMarker.icon = UIImage(named: "map_G_HW_15x15")
            //dropMarker.appearAnimation = GMSMarkerAnimation
            dropMarker.accessibilityLabel = "\(index)"
            dropMarker.map = self.mapRideView
        }
    }
    
    // For Update View
    func addOverlayToMapFroUpdateView(_ sourceLat: String,sourceLang: String,distinationLat: String,distinationLang: String ){
        self.mapRideView.clear()
        
        showIndicator("Loading Map path...")
        let sourceStr = "\(sourceLat),\(sourceLang)"
        let destinationStr = "\(distinationLat), \(distinationLang)"
        
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    self.addmarkers(legs)
                    self.addPolyLineUpdateViewWithEncodedStringInMap(routes as NSArray)
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }
    
    // MARK:- Google Map Direction Api Call
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    // MARK:- Add markers
    func addmarkers(_ legs : NSArray) {
        let legsStartLocDict = legs[0] as! NSDictionary
        let startLocationDictionary = legsStartLocDict["start_location"] as! NSDictionary
        self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
        let legsEndLocDict = legs[legs.count - 1] as! NSDictionary
        let endLocationDictionary = legsEndLocDict["end_location"] as! NSDictionary
        self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
        
        let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
        let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
        let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
        let camera = self.mapRideView.camera(for: bounds, insets: UIEdgeInsets())!
        self.mapRideView.camera = camera
        
        self.originMarker = GMSMarker(position: self.originCoordinate)
        self.originMarker.map = self.mapRideView
        self.originMarker.tracksInfoWindowChanges = true
        self.originMarker.snippet = data?.source
        
        self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
        self.destinationMarker.map = self.mapRideView
        self.destinationMarker.tracksInfoWindowChanges = true
        self.destinationMarker.snippet = data?.destination
        if(triggerFrom == "ride") {
            if(data?.rideType == "O") {
                self.originMarker.icon = UIImage(named:"marker_ride_orange")
                self.destinationMarker.icon = UIImage(named:"marker_ride_green")
            } else {
                self.originMarker.icon = UIImage(named:"marker_profile_orange")
                self.destinationMarker.icon = UIImage(named:"marker_profile_green")
            }
        } else {
            if(data?.officeRideType == "C") {
                self.originMarker.icon = UIImage(named:"marker_wp_green")
                self.destinationMarker.icon = UIImage(named:"marker_home_green")
            } else {
                self.originMarker.icon = UIImage(named:"marker_home_green")
                self.destinationMarker.icon = UIImage(named:"marker_wp_green")
            }
        }
    }
    
    func addMarkersWithResponse() {
        self.originCoordinate = CLLocationCoordinate2DMake(Double((data?.sourceLat!)!)!, Double((data?.sourceLong!)!)!)
        
        self.destinationCoordinate = CLLocationCoordinate2DMake(Double((data?.destinationLat)!)! , Double((data?.destinationLong)!)!
        )
        
        let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
        let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
        let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
        let camera = self.mapRideView.camera(for: bounds, insets: UIEdgeInsets())!
        self.mapRideView.camera = camera
        
        self.originMarker = GMSMarker(position: self.originCoordinate)
        self.originMarker.map = self.mapRideView
        self.originMarker.tracksInfoWindowChanges = true
        self.originMarker.snippet = data?.source
        //self.originMarker.appearAnimation = GMSMarkerAnimation
        
        self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
        self.destinationMarker.map = self.mapRideView
        self.destinationMarker.tracksInfoWindowChanges = true
        self.destinationMarker.snippet = data?.destination
        //self.destinationMarker.appearAnimation = GMSMarkerAnimation
        if(triggerFrom == "ride") {
            if(data?.rideType == "O") {
                self.originMarker.icon = UIImage(named:"marker_ride_orange")
                self.destinationMarker.icon = UIImage(named:"marker_ride_green")
            } else {
                self.originMarker.icon = UIImage(named:"marker_profile_orange")
                self.destinationMarker.icon = UIImage(named:"marker_profile_green")
            }
        } else {
            if(data?.officeRideType == "C") {
                self.originMarker.icon = UIImage(named:"marker_wp_green")
                self.destinationMarker.icon = UIImage(named:"marker_home_green")
            } else {
                self.originMarker.icon = UIImage(named:"marker_home_green")
                self.destinationMarker.icon = UIImage(named:"marker_wp_green")
            }
        }
    }
    
    func addPolyLineUpdateViewWithEncodedStringInMap(_ routes: NSArray) {
        
        for index in 0..<routes.count{
            let routesDicts = routes[index] as! NSDictionary
            let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
            let points = overview_polyline ["points"] as! NSString
            let path = GMSMutablePath(fromEncodedPath: points as String)
            let routePolylineObje = GMSPolyline(path: path)
            
            routePolylineObje.strokeWidth = 5.0
            routePolylineObje.isTappable = true
            routePolylineObje.title = String(index)
            self.polylineMutArray? .add(points)
            routePolylineObje.map = mapRideView
            
            let bounds = GMSCoordinateBounds(path: path!)
            mapRideView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
        }
    }
    
    // MARK:- Add PolyLine
    func addPolyLineWithEncodedStringInMap(_ routes: NSArray) {
        
        // for index in 0...routes.count-1{
        let routesDicts = routes[0] as! NSDictionary
        let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
        let points = overview_polyline ["points"] as! NSString
        let path = GMSMutablePath(fromEncodedPath: points as String)
        self.routePolyline = GMSPolyline(path: path)
        self.routePolyline.strokeColor = Colors.GREEN_COLOR
        self.routePolyline.strokeWidth = 5.0
        //        self.routePolyline.tappable = true
        //        self.routePolyline.geodesic = true
        //self.routePolyline.spans = [GMSStyleSpan(color: Colors.RED_COLOR)]
        self.routePolyline.map = self.mapRideView
        
        let bounds = GMSCoordinateBounds(path: path!)
        mapRideView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
        
        
        //  }
    }
    
    func gotoTripCalculationVC()
    {
        let vc = TripCalculationViewController()
        vc.data = self.data
        vc.onSelectBackBlock = {
            self.updateRideDetails()
        }
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func startRide()
    {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID:"Start Ride Activity",
            AnalyticsParameterItemName: "Button",
            AnalyticsParameterContentType:"Start Ride"
            ])
        let todayDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        var date = dateFormatter.string(from: todayDate) as String
        if(date.hasPrefix("0")) {
            date.remove(at: date.startIndex)
        }
        if let startDate = data?.startDate {
            if(date.contains(startDate)){
                dateFormatter.dateFormat = "hh:mm a"
                //dateFormatter.timeZone = NSTimeZone(name: "IST")
                var rideTime: Date?
                if let startTime = data?.startTime {
                    rideTime = dateFormatter.date(from: startTime)!
                }
                rideTime = Date(timeInterval: -600, since: rideTime!)
                let strCurrentTime = dateFormatter.string(from: Date())
                let currentTime = dateFormatter.date(from: strCurrentTime)
                if(currentTime!.compare(rideTime!) == ComparisonResult.orderedDescending){
                    self.locationManager.requestWhenInUseAuthorization()
                    AlertController.showAlertFor("Start Ride", message: "Would you like to start now?", okButtonTitle: "Start", okAction: {
                        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                            AnalyticsParameterItemID:"Start Ride Activity",
                            AnalyticsParameterItemName: "Ok Button",
                            AnalyticsParameterContentType:"Start Ride"
                            ])
                        showIndicator("Fetching current location..")
                        //TODO: check in all files for below condition.
                        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                            self.tripStarted = false
                            self.locationManager.delegate = self
                            self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                            self.locationManager.startUpdatingLocation()
                        }
                        else{
                            hideIndicator()
                            AlertController.showAlertFor("Enable Location Services", message: "Enable location services from Settings > Privacy for the application to proceed.")
                        }
                        
                    }, cancelButtonTitle: "Cancel") {
                        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                            AnalyticsParameterItemID:"Start Ride Activity",
                            AnalyticsParameterItemName: "Cancel Button",
                            AnalyticsParameterContentType:"Start Ride"
                            ])
                    }
                } else {
                    AlertController.showAlertFor("Start Ride", message: "Ride can only start before 10 minutes or update your start time.", okAction: {})
                }
            } else {
                AlertController.showAlertFor("Start Ride", message: "Ride can only start before 10 minutes or update your start time.", okAction: {})
            }
        }
    }
    
    func trackRide()
    {
        if let status = data?.currentStatus
        {
            if((status.lowercased() == "s") || (status.lowercased() == "b")){
                let vc = TripCalculationViewController()
                vc.data = self.data
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if(data?.vehicleOwnerID == nil) {
                AlertController.showAlertFor("Track Ride", message: "It will be enabled when ride is having vehicle.", okAction: {})
            } else {
                AlertController.showAlertFor("Track Ride", message: "Please wait for vehicle to reach your place.", okAction: {})
            }
        }
    }
    
    
    @IBAction func StartRideClicked(_ sender: AnyObject) {
        if let status = data?.currentStatus
        {
            if(status.lowercased() == "c"){
                AlertController.showToastForInfo("RIDE COMPLETED!")
                //btnStart.setTitle("RIDE COMPLETED!", forState: .Normal)
            }
            else if((status.lowercased() == "s") || (status.lowercased() == "b")){
                StartRideBtn.setImage(UIImage(named: "track_ride"), for: UIControlState())
                trackRide()
            }
        }
        else{
            StartRideBtn.isHidden = true
            if(data?.currentStatus?.lowercased() != "c"){
                StartRideBtn.isHidden = false
                if(data?.vehicleOwnerID == UserInfo.sharedInstance.userID && data?.vehicleRegNo != nil){
                    startRide()
                } else {
                    if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
                        StartRideBtn.setImage(UIImage(named: "track_ride"), for: UIControlState())
                        trackRide()
                    } else {
                        StartRideBtn.isHidden = true
                    }
                }
            }
        }
    }
    
    @IBAction func trafficeButtonClicked(_ button: UIButton) {
        
        if button.isSelected {
            self.mapRideView.isTrafficEnabled = false
        }else{
            self.mapRideView.isTrafficEnabled = true
        }
        button.isSelected = !button.isSelected
    }
    
    // MARK:- SOS Button Clicked.
    @IBAction func sosButtonClicked(_ sender: AnyObject) {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            if(data?.vehicleOwnerID == nil) {
                AlertController.showAlertFor("SOS", message: "It will be enabled when ride is having vehicle.", okAction: {})
            } else {
                if  ((data?.poolGroups?.count > 0 && data?.poolGroups?.first?.helpNumber != nil && data?.poolGroups?.first?.helpNumber != "") || (UserInfo.sharedInstance.selectedEmergencyNumber.characters.count > 0))
                {
                    self.isSOSButtonPressed = true
                    self.locationManager.requestWhenInUseAuthorization()
                    
                    if CLLocationManager.locationServicesEnabled()
                    {
                        self.locationManager.delegate = self
                        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                        self.locationManager.startUpdatingLocation()
                    }
                }
                else
                {
                    callAddressBookForPressedButtonObject()
                }
            }
        }
    }
    
    func setUpBtn(_ btn: UIButton, imageName: String, title: String, parent: UIView)
    {
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.setImage(UIImage(named: imageName), for: UIControlState())
        btn.isHidden = true
        parent.addSubview(btn)
        
        parent.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]|", options: [], metrics: nil, views: ["btn":btn]))
    }
    
    func getBtnView() -> SubView
    {
        
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        setUpBtn(btnInvite, imageName: "invite", title:"Invite", parent: view)
        btnInvite.addTarget(self, action: #selector(btnInvite_clicked), for: .touchDown)
        
        setUpBtn(btnEdit, imageName: "edit", title:"Edit", parent: view)
        btnEdit.addTarget(self, action: #selector(btnEdit_clicked), for: .touchDown)
        
        setUpBtn(btnDelete, imageName: "delete", title:"Delete", parent: view)
        btnDelete.addTarget(self, action: #selector(btnDelete_clicked), for: .touchDown)
        
        setUpBtn(btnCancel, imageName: "cancel", title:"Cancel", parent: view)
        btnCancel.addTarget(self, action: #selector(btnCancel_clicked), for: .touchDown)
        
        setUpBtn(btnJoin, imageName: "join", title:"Join", parent: view)
        btnJoin.addTarget(self, action: #selector(btnJoin_clicked), for: .touchDown)
        
        setUpBtn(btnUnjoin, imageName: "unjoin", title:"Unjoin", parent: view)
        btnUnjoin.addTarget(self, action: #selector(btnUnjoin_clicked), for: .touchDown)
        
        setUpBtn(btnAccept, imageName: "accept", title:"Accept", parent: view)
        btnAccept.addTarget(self, action: #selector(btnAccept_clicked), for: .touchDown)
        
        setUpBtn(btnReject, imageName: "reject", title:"Reject", parent: view)
        btnReject.addTarget(self, action: #selector(btnReject_clicked), for: .touchDown)
        
        setUpBtn(btnOfferVehicle, imageName: "offeravehicle", title:"Offer a Vehicle", parent: view)
        btnOfferVehicle.addTarget(self, action: #selector(btnOfferVehicle_clicked), for: .touchDown)
        
        footerView.addSubview(view)
        setVisibleBtns(view)
        StartRideBtn.isHidden = true
        if(data?.poolTaxiBooking != "1"){
            StartRideBtn.isHidden = false
            if(data?.currentStatus == nil && data?.currentStatus?.lowercased() != "c"){
                StartRideBtn.isHidden = false
                if(data?.vehicleOwnerID == UserInfo.sharedInstance.userID && data?.vehicleRegNo != nil){
                    StartRideBtn.isHidden = false
                } else {
                    if((data?.createdBy == UserInfo.sharedInstance.userID) || (data?.joinStatus == "J")){
                        StartRideBtn.isHidden = false
                        StartRideBtn.setImage(UIImage(named: "track_ride"), for: UIControlState())
                    } else {
                        StartRideBtn.isHidden = true
                    }
                }
            }
        }
        if let status = data?.currentStatus
        {
            if((status.lowercased() == "s") || (status.lowercased() == "b")){
                StartRideBtn.setImage(UIImage(named: "track_ride"), for: UIControlState())
            }
        }
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":view]))
        footerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(45)]|", options: [], metrics: nil, views: ["view":view]))
        let sub = SubView()
        sub.view = footerView
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 45)
        return sub
    }
    
    func setVisibleBtns(_ view: UIView)
    {
        if(btnConstraints != nil){
            view.removeConstraints(btnConstraints!)
            btnConstraints = nil
        }
        
        btnInvite.isHidden = true
        btnEdit.isHidden = true
        btnDelete.isHidden = true
        btnCancel.isHidden = true
        btnJoin.isHidden = true
        btnUnjoin.isHidden = true
        btnAccept.isHidden = true
        btnReject.isHidden = true
        btnOfferVehicle.isHidden = true
        
        //        if let status = data?.currentStatus
        //        {
        //            if((status.lowercaseString == "s") || (status.lowercaseString == "b") || (status.lowercaseString == "c")){
        //                return
        //            }
        //        }
        
        let viewsDict = ["invite":btnInvite, "edit":btnEdit, "delete":btnDelete, "cancel":btnCancel, "join":btnJoin, "unjoin":btnUnjoin, "offerv":btnOfferVehicle, "accept":btnAccept, "reject":btnReject]
        
        if(data?.createdBy == UserInfo.sharedInstance.userID){
            if(data?.poolTaxiBooking == "0"){
                //INIVTE, EDIT, DELETE
                btnInvite.isHidden = false
                btnEdit.isHidden = false
                btnDelete.isHidden = false
                
                btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[edit(==invite)]-1-[delete(==invite)]|", options: [], metrics: nil, views: viewsDict)
            }
            else{
                //INVITE, CANCEL
                btnCancel.isHidden = false
                if(data?.poolShared != "0") {
                    btnInvite.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[cancel(==invite)]|", options: [], metrics: nil, views: viewsDict)
                } else {
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]|", options: [], metrics: nil, views: viewsDict)
                }
            }
        }
        else{
            if(data?.joinStatus != nil && data?.requestedStatus != nil){
                if(data?.joinStatus == "P" && data?.requestedStatus == "P"){
                    btnCancel.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[cancel]|", options: [], metrics: nil, views: viewsDict)
                }
                else if(data?.joinStatus == "J" && data?.requestedStatus == "A"){
                    //INVITE
                    btnInvite.isHidden = false
                    if(data?.poolTaxiBooking == "0"){
                        //UNJOIN
                        btnUnjoin.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[unjoin(==invite)]|", options: [], metrics: nil, views: viewsDict)
                    }
                    else{
                        //CANCEL
                        btnCancel.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[invite]-1-[cancel(==invite)]|", options: [], metrics: nil, views: viewsDict)
                    }
                }
                else if(data?.joinStatus == "F" && data?.requestedStatus == "F"){
                    //btnInvite.hidden = false
                    btnAccept.isHidden = false
                    btnReject.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[accept]-1-[reject(==accept)]|", options: [], metrics: nil, views: viewsDict)
                }
                else if((data?.joinStatus == "U" && data?.requestedStatus == "A") || (data?.joinStatus == "P" && data?.requestedStatus == "C") || (data?.joinStatus == "S" && data?.requestedStatus == "C") || (data?.joinStatus == "R" && data?.requestedStatus == "R") || (data?.joinStatus == "O" && data?.requestedStatus == "A") || (data?.joinStatus == "F" && data?.requestedStatus == "C")){
                    if(data?.rideType == "O"){
                        btnJoin.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                        
                    }
                    else{
                        if(data?.poolTaxiBooking == "0"){
                            btnJoin.isHidden = false
                            btnOfferVehicle.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]-1-[offerv(==join)]|", options: [], metrics: nil, views: viewsDict)
                            
                        }
                        else{
                            btnJoin.isHidden = false
                            btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                        }
                    }
                }
            }
            else{
                if(data?.rideType == "O"){
                    btnJoin.isHidden = false
                    btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                }
                else{
                    if(data?.poolTaxiBooking == "0"){
                        btnJoin.isHidden = false
                        btnOfferVehicle.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]-1-[offerv(==join)]|", options: [], metrics: nil, views: viewsDict)
                    }
                    else{
                        btnJoin.isHidden = false
                        btnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[join]|", options: [], metrics: nil, views: viewsDict)
                    }
                }
            }
        }
        view.addConstraints(btnConstraints!)
    }
    
    func callAddressBookForPressedButtonObjectInvite() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            mobileMutArr.removeAllObjects()
            contactListArray.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject] {
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = (data?.poolID)!
            vc.triggerFrom = "rideDetail"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccessInvite()
        }
        
    }
    
    func promptForAddressBookRequestAccessInvite()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
                {
                    if !granted
                    {
                        //print("Just denied")
                        self.displayCantAddContactAlert()
                    } else
                    {
                        self.mobileMutArr.removeAllObjects()
                        self.contactListArray.removeAllObjects()
                        let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                        for record:ABRecord in contactList as [AnyObject] {
                            let contactPerson: ABRecord = record
                            let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                            if(contactName != "") {
                                var personNumber = ""
                                let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                                let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                                if(phones != "")
                                {
                                    if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                    {
                                        if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                        {
                                            let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                            personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                            let areaCode = personNumber.characters.count-10
                                            let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                            if(areaCode > 0) {
                                                personNumber = personNumber.substring(from: startIndex)
                                            }
                                        }
                                    } else {
                                        continue
                                    }
                                }
                                self.mobileMutArr.add(personNumber)
                                let nameString = "\(contactName) \(personNumber)"
                                self.contactListArray.add(nameString)
                            }
                        }
                        
                        let vc = ContactsListViewController()
                        vc.contactListArr = self.contactListArray
                        vc.mobileMutArr = self.mobileMutArr
                        vc.poolID = (self.data?.poolID)!
                        vc.triggerFrom = "rideDetail"
                        vc.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
            }
        }
    }
    
    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObjectInvite()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg("", wpKey: "", poolID: (data?.poolID)!)
    }
    
    func grpMsg_clicked() {
        self.dismiss(animated: true, completion: nil)
        showIndicator("Fetching members.")
        WorkplaceRequestor().getWPMembers("", groupID: (data?.ownerScope)!, distanceRange: "", homeLat: UserInfo.sharedInstance.homeLatValue, homeLon: UserInfo.sharedInstance.homeLongValue, success: { (success, object) in
            hideIndicator()
            let vc = InviteGroupMembersViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            vc.wpDetails = (object as! WPMembersResponseData).groupDetails
            vc.membersList = (object as! WPMembersResponseData).members
            self.navigationController?.pushViewController(vc, animated: true)
        }, failure: { (error) in
            hideIndicator()
        })
    }
    
    func btnInvite_clicked() {
        if let poolScope = data?.ownerScope {
            if poolScope == "PUBLIC" {
                let inviteView = InviteNormalRideView()
                let alertViewHolder =   AlertContentViewHolder()
                alertViewHolder.heightConstraintValue = 95
                alertViewHolder.view = inviteView
                inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
                inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
                
                AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
            } else {
                let inviteView = InviteWPRideView()
                let alertViewHolder =   AlertContentViewHolder()
                alertViewHolder.heightConstraintValue = 95
                alertViewHolder.view = inviteView
                inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
                inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
                inviteView.groupMsgBtn.addTarget(self, action: #selector(grpMsg_clicked), for: .touchDown)
                AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
            }
        }
    }
    
    func btnEdit_clicked()
    {
        let vc = UpdateRideViewController()
        vc.updateData = data
        vc.triggerFrom = triggerFrom
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func btnDelete_clicked()
    {
        AlertController.showAlertFor("Delete Ride", message: "Would you really like to delete this ride?", okButtonTitle: "Yes", okAction: {
            
            showIndicator("Deleting Ride..")
            
            RideRequestor().deleteRide(self.data!.poolID!, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Delete Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        _ = self.navigationController?.popViewController(animated: true)
                        self.onDeleteActionBlock?()
                    })
                }
                else{
                    AlertController.showAlertFor("Delete Ride", message: object as? String)
                }
                
            }, failure: { (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            })
        }, cancelButtonTitle: "Later") {
            
        }
    }
    
    func btnCancel_clicked()
    {
        if(data?.poolTaxiBooking == "1"){
            //cancel taxi ride
            AlertController.showAlertFor("Cancel Taxi Ride", message: "Would you really like to cancel this taxi ride?", okButtonTitle: "Yes", okAction: {
                showIndicator("Cancelling Taxi Ride..")
                RideRequestor().cancelTaxiRide(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            _ = self.navigationController?.popViewController(animated: true)
                            self.onDeleteActionBlock?()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Cancel Taxi Ride", message: object as? String)
                    }
                }, failure: { (error) in
                    hideIndicator()
                })
            }, cancelButtonTitle: "Later", cancelAction: {
            })
        }
        else{
            AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel this ride?", okButtonTitle: "Yes", okAction: {
                //from memberslist - FR
                //from ridedetail & ridelist = JR
                showIndicator("Cancelling Ride.")
                RideRequestor().cancelRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, cancelrequestType: "JR", success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Cancel Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                            self.getRideDetail()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Cancel Ride", message: object as? String)
                    }
                }, failure: { (error) in
                    hideIndicator()
                })
            }, cancelButtonTitle: "Later", cancelAction: {
            })
        }
    }
    
    func acceptRide() {
        AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Accepting ride request..")
            RideRequestor().acceptRideRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Accept Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.getRideDetail()
                    })
                }else{
                    AlertController.showAlertFor("Accept Ride", message: object as? String)
                }
            }, failure: { (error) in
                hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    
    func btnAccept_clicked()
    {
        if(self.data?.poolTaxiBooking != "1" && self.data?.points != "0" && self.data?.points != "") {
            ridePoints = Int((self.data?.points)!)!
            if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                    AlertController.showAlertFor("Accept Ride", message: "\("SORRY! You cannot accept this ride request because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and accept this ride request.")", okButtonTitle: "RECHARGE", okAction: {
                        let vc = MyAccountViewController()
                        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                    }, cancelButtonTitle: "CANCEL", cancelAction: {})
                } else {
                    acceptRide()
                }
            } else {
                acceptRide()
            }
        } else {
            acceptRide()
        }
    }
    
    func btnReject_clicked()
    {
        AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Rejecting ride request..")
            RideRequestor().rejectRideRequest(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Reject Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.getRideDetail()
                    })
                }else{
                    AlertController.showAlertFor("Reject Ride", message: object as? String)
                }
            }, failure: { (error) in
                hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    
    func joinRide() {
        if(UserInfo.sharedInstance.shareMobileNumber == false){
            AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                self.getPickUpDropLocation("joinRide")
            }, cancelButtonTitle: "LATER", cancelAction: {
            })
        }
        else{
            getPickUpDropLocation("joinRide")
        }
    }
    
    func btnJoin_clicked()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            if(self.data?.poolTaxiBooking != "1" && self.data?.points != "0" && self.data?.points != "") {
                ridePoints = Int((self.data?.points)!)!
                if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                    if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                        AlertController.showAlertFor("Join Ride", message: "\("SORRY! You cannot join this ride because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and join this ride.")", okButtonTitle: "RECHARGE", okAction: {
                            let vc = MyAccountViewController()
                            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                        }, cancelButtonTitle: "CANCEL", cancelAction: {})
                    } else {
                        joinRide()
                    }
                } else {
                    joinRide()
                }
            } else {
                joinRide()
            }
        }
    }
    
    func btnUnjoin_clicked()
    {
        AlertController.showAlertFor("Unjoin Ride", message: "Would you really like to unjoin from this ride?", okButtonTitle: "YES", okAction: {
            showIndicator("Unjoining the ride..")
            RideRequestor().unJoinRide(self.data!.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Unjoin Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        self.getRideDetail()
                    })
                }else{
                    AlertController.showAlertFor("Unjoin Ride", message: object as? String)
                }
            }, failure: { (error) in
                hideIndicator()
            })
        }, cancelButtonTitle: "LATER") {
        }
    }
    
    func btnOfferVehicle_clicked()
    {
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            self.getPickUpDropLocation("offerVehicle")
        }
    }
    
    func getPickUpDropLocation(_ triggerFrom: String)
    {
        let joinOrOfferRideObj = JoinOrOfferRide()
        let vc = RideDetailMapViewController()
        vc.sourceLong = data?.sourceLong
        vc.sourceLat = data?.sourceLat
        vc.sourceLocation = data?.source
        vc.destLong = data?.destinationLong
        vc.destLat = data?.destinationLat
        vc.destLocation = data?.destination
        vc.poolTaxiBooking = (data?.poolTaxiBooking)!
        vc.poolId = (data?.poolID)!
        vc.rideType = (data?.rideType)!
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.hidesBottomBarWhenPushed = true
        vc.triggerFrom = triggerFrom
        if(self.triggerFrom == "ride") {
            vc.isWP = false
        } else {
            vc.isWP = true
            vc.wpInsuranceStatus = (wpData?.wpInsuranceStatus)!
        }
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
            joinOrOfferRideObj.poolID = self.data!.poolID!
            joinOrOfferRideObj.vehicleRegNo = ""
            joinOrOfferRideObj.vehicleCapacity = ""
            joinOrOfferRideObj.cost = "0"
            joinOrOfferRideObj.rideMsg = ""
            joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
            joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
            joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
            joinOrOfferRideObj.dropPoint = dropLoc as? String
            joinOrOfferRideObj.dropPointLat = dropLat as? String
            joinOrOfferRideObj.dropPointLong = dropLong as? String
            
            if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                showIndicator("Joining Ride..")
                OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                            self.getRideDetail()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                    }
                }, failure: { (error) in
                    hideIndicator()
                })
                
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        manager.stopUpdatingLocation()
        
        hideIndicator()
        
        if  (isSOSButtonPressed == true)
        {
            self.tripStarted = true
            isSOSButtonPressed = false
            let geocoder = GMSGeocoder()
            
            geocoder.reverseGeocodeCoordinate(locValue) { [weak self] response , error in
                
                if let address = response?.firstResult() {
                    
                    let lines = address.lines as [String]!
                    self?.sendSOSRequest("\(address.coordinate.latitude)","\(address.coordinate.longitude)",(lines?.joined(separator: ","))!)
                }
                
            }
        }
        
        if(tripStarted == false){
            tripStarted = true
            let reqObj = TripRequest()
            let geocoder = GMSGeocoder()
            geocoder.reverseGeocodeCoordinate(locValue) { [weak self] response , error in
                if let address = response?.firstResult() {
                    
                    let lines = address.lines as [String]!
                    reqObj.userID = UserInfo.sharedInstance.userID
                    reqObj.poolID = self?.data?.poolID
                    reqObj.fromLocation = lines?.joined(separator: ",")
                    reqObj.fromLat = "\(locValue.latitude)"
                    reqObj.fromLong = "\(locValue.longitude)"
                    reqObj.toLocation = self?.data?.destination
                    reqObj.startTime = prettyDateStringFromDate(NSDate() as Date, toFormat: "hh:mm a")
                    
                    if(self?.data?.vehicleOwnerID == UserInfo.sharedInstance.userID){
                        if(self?.data?.vehicleOwnerID == self?.data?.createdBy) {
                            reqObj.poolOwnerType = "admin"
                        } else {
                            reqObj.poolOwnerType = ""
                        }
                        WorkplaceRequestor().startRideByDriver(reqObj, success: { (success, object) in
                            if(success == true){
                                self?.gotoTripCalculationVC()
                            }else{
                                AlertController.showAlertFor("Start Ride", message: object as? String)
                            }
                        }, failure: { (error) in
                        })
                    }
                    else{
                        reqObj.mobileNumber = UserInfo.sharedInstance.mobile
                        reqObj.deviceID = ""
                        reqObj.userName = UserInfo.sharedInstance.fullName
                        WorkplaceRequestor().startRideByRider(reqObj, success: { (success, object) in
                            if(success == true){
                                self?.gotoTripCalculationVC()
                            }
                            else{
                                AlertController.showAlertFor("Start Ride", message: object as? String)
                            }
                        }, failure: { (error) in
                        })
                    }
                }
                
            }
        }
    }
    
    func sendSOSRequest(_ value :String...) -> Void
    {
        showIndicator("Sending Email & Sms...")
        let reqObject =  GetRideDetailsRequest()
        
        reqObject.userID = UserInfo.sharedInstance.userID
        reqObject.userName = UserInfo.sharedInstance.fullName
        reqObject.userMobile = UserInfo.sharedInstance.mobile
        reqObject.gender = UserInfo.sharedInstance.gender == "M" ? "His" : "Her"
        reqObject.currentLocation = value[2]
        
        
        reqObject.vehicleNumber = ""
        if  let regNo = data?.vehicleRegNo
        {
            reqObject.vehicleNumber = regNo
        }
        
        reqObject.vehicleModel = ""
        if  let regNo = data?.vehicleModel
        {
            reqObject.vehicleModel = regNo
        }
        
        reqObject.vehicleType = ""
        if  let regNo = data?.vehicleType
        {
            reqObject.vehicleType = regNo
        }
        
        let poolID : NSString!
        poolID = data?.poolID as NSString!
        if(data?.poolGroups?.count > 0 && data?.poolGroups?.first?.helpNumber != nil && data?.poolGroups?.first?.helpNumber != "") {
            reqObject.helpMobile = data?.poolGroups?.first?.helpNumber
            reqObject.helpMail = data?.poolGroups?.first?.helpEmail
        } else {
            reqObject.helpMobile = ""
            reqObject.helpMail = ""
        }
        reqObject.sourceLocation = self.sourceLabel.text
        reqObject.destinationLocation =  self.distinationLabel.text
        reqObject.url = SOS_USERLOC+"?pool_id=\(poolID)&lat=\(value[0])&long=\(value[1])"
        reqObject.latitude = value[0]
        reqObject.longtitude = value[1]
        
        RideRequestor().getSOSForRideDetail(reqObject, success:{ (success, object) in
            hideIndicator()
            if(success == true) {
                AlertController.showAlertFor("Alert", message: "RideAlly has sent Email & Sms to your SOS contacts successfully.\n Would you like to initiate emergency call?", okButtonTitle: "YES", okAction: {
                    var contactNo = ""
                    if(self.data?.poolGroups?.count > 0 && self.data?.poolGroups?.first?.helpNumber != nil && self.data?.poolGroups?.first?.helpNumber != "") {
                        contactNo = (self.data?.poolGroups?.first?.helpNumber)!
                    } else {
                        contactNo = UserInfo.sharedInstance.selectedEmergencyNumber
                    }
                    if let url = URL(string: "tel://\(contactNo)"), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.openURL(url)
                    }
                }, cancelButtonTitle: "Later") {
                }
            } else {
                AlertController.showAlertForMessage((object as AnyObject).message)
            }
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            self.addressBookController.peoplePickerDelegate = self
            self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                "phoneNumbers.@count > 0", argumentArray: nil)
            self.present(self.addressBookController, animated: true, completion: nil)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
    }
    
    func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController)
    {
        
        self.addressBookController.dismiss(animated: true, completion: nil)
    }
    
    var fullName = ""
    var contactNumber = ""
    var emailId = ""
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord)
    {
        if let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String
        {
            fullName = first
            
            if let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String
            {
                fullName = first+last
            }
        }
        
        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
        let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
        if(phones != "")
        {
            if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
            {
                if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                {
                    let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                    var personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                    let areaCode = personNumber.characters.count-10
                    let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                    if(areaCode > 0) {
                        personNumber = personNumber.substring(from: startIndex)
                    }
                    contactNumber = personNumber
                }
            }
        }
        
        let unmanagedPhones1 = ABRecordCopyValue(person, kABPersonEmailProperty)
        let phones1 = Unmanaged.fromOpaque((unmanagedPhones1?.toOpaque())!).takeUnretainedValue() as NSString
        if(phones1 != "")
        {
            if let unmanagedPhone1 = ABMultiValueCopyValueAtIndex(phones1, 0)
            {
                if  let phone1: String = Unmanaged.fromOpaque(unmanagedPhone1.toOpaque()).takeUnretainedValue() as NSObject as? String
                {
                    emailId = phone1
                }
            } else {
                emailId = ""
            }
        }
        if(contactNumber != "") {
            sendSaveContactListRequest()
        }
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
                {
                    if !granted
                    {
                        //print("Just denied")
                        self.displayCantAddContactAlert()
                    } else
                    {
                        //print("Just authorized")
                        self.addressBookController.peoplePickerDelegate = self
                        self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                            "phoneNumbers.@count > 0", argumentArray: nil)
                        self.present(self.addressBookController, animated: true, completion: nil)
                    }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
                                                    style: .default,
                                                    handler: { action in
                                                        self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func sendSaveContactListRequest() -> Void
    {
        let reqObj = SaveEmergencyContactRequest()
        showIndicator("Loading...")
        
        reqObj.userID = UserInfo.sharedInstance.userID
        
        reqObj.firstPersonName = ""
        if fullName != ""
        {
            reqObj.firstPersonName = fullName
        }
        
        reqObj.secondPersonName = nil
        reqObj.firstPersonNumber = ""
        if contactNumber != ""
        {
            let stringArray = contactNumber.components(separatedBy: CharacterSet.decimalDigits.inverted)
            var firstPersonNumber = NSArray(array: stringArray).componentsJoined(by: "")
            let areaCode = firstPersonNumber.characters.count-10
            let startIndex = firstPersonNumber.characters.index(firstPersonNumber.startIndex, offsetBy: areaCode)
            if(areaCode > 0) {
                firstPersonNumber = firstPersonNumber.substring(from: startIndex)
            }
            reqObj.firstPersonNumber = firstPersonNumber
        }
        
        reqObj.secondPersonNumber = nil
        reqObj.firstPersonEmail = ""
        if  emailId != ""
        {
            reqObj.firstPersonEmail = emailId
        }
        
        reqObj.secondPersonEmail = ""
        reqObj.defaultPerson = "1"
        
        EmergencyContactRequestor().sendSaveContactListRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! SaveEmergencyContactResponse).code == "4600"
            {
                UserInfo.sharedInstance.selectedEmergencyNumber = reqObj.firstPersonNumber!
                UserInfo.sharedInstance.selectedEmergencyEmail = reqObj.firstPersonEmail!
                self.sosButtonClicked(reqObj)
            }
            else
            {
                AlertController.showToastForError("Record Not Found.")
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
    
    func deleteWorkplaceRide() {
        AlertController.showAlertFor("Delete Ride", message: "Would you really like to delete this ride?", okButtonTitle: "Yes", okAction: {
            
            showIndicator("Deleting Ride..")
            
            RideRequestor().deleteRide(self.data!.poolID!, success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Delete Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        _ = self.navigationController?.popViewController(animated: true)
                        self.onDeleteActionBlock?()
                    })
                }
                else{
                    AlertController.showAlertFor("Delete Ride", message: object as? String)
                }
                
            }, failure: { (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            })
        }, cancelButtonTitle: "Later") {
            
        }
    }
    
    func getRideDetail()  {
        let poolID = (self.data?.poolID)!
        showIndicator("Updating Ride Details")
        RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in hideIndicator()
            if(success){
                self.data = object as? RideDetail
                self.updateRideDetails()
                _ = self.getBtnView()
                hideIndicator()
            }
        }) { (error) in hideIndicator() }
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
       self.getRideDetail()
    }
    
    func getInviteResposeView() -> UIView
    {
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        let table = UITableView()
        //        table.frame = CGRectMake(20, 30, 250, 200)
        table.delegate = self
        table.tag = 33
        table.dataSource = self
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorColor = UIColor.clear
        view.addSubview(table)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table(100)]|", options: [], metrics: nil, views: ["table":table]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":table]))
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responses!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = normalFontWithSize(14)
        cell.textLabel?.numberOfLines = 0
        if let mobile = responses![indexPath.row].mobile{
            cell.textLabel?.text = "\(mobile) \(getMsgForCode(responses![indexPath.row].msgCode!))"
        }
        if let email = responses![indexPath.row].email{
            cell.textLabel?.text = "\(email) \(getMsgForCode(responses![indexPath.row].msgCode!))"
        }
        return cell
        
    }
    func getMsgForCode(_ code: String) -> String
    {
        let dict = ["161":"for tieup workplace domain mismatch",
                    "157":"personal not filled",
                    "158":"gender not filled",
                    "160":"is not registered with rideally, however inivitation has been sent successfully.",
                    "151":"Sorry, this is only female ride, hence you cannot invite a male.",
                    "152":"Sorry, this is only male ride, hence you cannot invite a female.",
                    "141":"Your invitation has been sent successfully & Notification Send to user.",
                    "142":"Your invitation has been sent successfully but Error in notification sending.",
                    "154":"Sorry, user has already joined in this ride.",
                    "155":"User already invited, however invitation has been sent successfully.",
                    "156":"User has already sent a request to join this ride. ",
                    "153":"Oops! You are the initiator of this ride, hence you cannot invite yourself."]
        
        return dict[code]!
    }
    
    func sendInvitation () {
        showIndicator("Sending Invites..")
        RideRequestor().sendInvite((data?.poolID)!, ownerId: UserInfo.sharedInstance.userID, emailIDs: "", mobileNOs: mobileNo, success: { (success, object) in
            hideIndicator()
            if(success){
                self.responses = (object as! InviteMembersResponse).data
                var msg = ""
                if let mobile = self.responses![0].mobile{
                    msg = "\(mobile) \(self.getMsgForCode(self.responses![0].msgCode!))"
                }
                AlertController.showAlertFor("Invite Members", message: msg, okAction: {
                    self.getRideDetail()
                })
            }
        }, failure: { (error) in
            hideIndicator()
        })
    }
    
    func showMembersList()
    {
        if((data?.joinStatus != nil && data?.joinStatus == "J") || (data?.createdBy == UserInfo.sharedInstance.userID)){
            //            if(mobileNo != "") {
            //                showIndicator("Fetching Ride Members...")
            //                RideRequestor().getRideDetails((data?.poolID)!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            //                    hideIndicator()
            //                    if(success){
            //                        let vc = MembersViewController()
            //                        vc.membersList = (object as! RideDetail).poolUsers
            //                        vc.poolID = self.data?.poolID
            //                        vc.poolTaxiBooking = self.data?.poolTaxiBooking
            //                        vc.hidesBottomBarWhenPushed = true
            //                        vc.edgesForExtendedLayout = .None
            //                        self.navigationController?.pushViewController(vc, animated: true)
            //                    }
            //                }) { (error) in
            //                    hideIndicator()
            //                }
            //            } else {
            let vc = MembersViewController()
            vc.membersList = data?.poolUsers
            vc.poolID = data?.poolID
            vc.poolTaxiBooking = data?.poolTaxiBooking
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            self.navigationController?.pushViewController(vc, animated: true)
            //}
        }
        else{
            AlertController.showToastForError("Please join the ride to see the members.")
        }
    }
    
    func getUserPoints() {
        let reqObj = GetUserPointsOnRecahargeRequest()
        reqObj.userID =  UserInfo.sharedInstance.userID
        
        MyAccountRequestor().sendGetUserPointsRequest(reqObj, success:{ (success, object) in
            if (object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable != nil
            {
                UserInfo.sharedInstance.maxAllowedPoints = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.maximumallowedpoints)!
                UserInfo.sharedInstance.availablePoints = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsavailable)!
            }
            if (object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsearned != nil
            {
                UserInfo.sharedInstance.earnedPointsViaRecharge = ((object as! GetUserPointsOnRecahargeResponse).dataObj?.pointsearned)!
            }
        }){ (error) in
            AlertController.showAlertForError(error)
        }
    }
}
