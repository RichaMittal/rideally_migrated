//
//  rideWindowInfoView.swift
//  rideally
//
//  Created by Pratik Bhaliya on 25/02/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class RideWindowInfoView: UIView {

    @IBOutlet weak var imgDest: UIImageView!
    @IBOutlet weak var imgSource: UIImageView!
    @IBOutlet weak var imgVia: UIImageView!
    @IBOutlet var seatAvailable: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var profile: UIImageView!
    @IBOutlet var offerVehicle: UILabel!
    @IBOutlet var joinButton: UIButton!
    @IBOutlet var address: UILabel!
    @IBOutlet  var via: UILabel!
    @IBOutlet  var destinationLbl: UILabel!
    @IBOutlet var offerRideButton: UIButton!
    @IBOutlet weak var markerWindowButton: UIButton!
    @IBOutlet weak var seatLeftTag: UILabel!
    @IBOutlet weak var sepraterLine: UILabel!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
    super.awakeFromNib()
     self.profile.layer.cornerRadius = 30.0
    self.profile.layer.masksToBounds = true
    }
    
    
    @IBAction func joinRideButtonClicked(_ sender: AnyObject) {
        
        
    }

}
