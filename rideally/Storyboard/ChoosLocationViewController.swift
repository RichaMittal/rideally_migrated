//
//  ChoosLocationViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 08/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ChoosLocationViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate {

    
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var locationAddress: String?
    var userMovedMap: Bool = false
    var isControlerType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "From"
        self.mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override  func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
        self.address.text = self.locationAddress
        if let coord = self.locationCoord {
            let camera = GMSCameraPosition.camera(withLatitude: coord.latitude, longitude: coord.longitude, zoom: 14.0)
            //self.mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
            self.mapView.animate(to: camera)
        } else {
            let camera = GMSCameraPosition.camera(withLatitude: 20.593684, longitude: 78.962880, zoom: 14.0)
            //self.mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
            self.mapView.animate(to: camera)
        }
    }
    
    //MARK:- back Button Clicked
    @IBAction func backButtonClicked(_ sender: AnyObject) {
    self.dismiss(animated: true) {
        }
    }
    
    //MARK:- current Location Button Clicked
    @IBAction func currentLocationButtonClicked(_ sender: AnyObject) {
        
        if let location = mapView.myLocation {
            self.userMovedMap = true
            moveMapToCoord(location.coordinate)
        }
    }
    
    @IBAction func pickedLocationButtonClicked(_ sender: AnyObject) {
        
        
        if self.isControlerType == "Update" {
            self.dismiss(animated: true, completion: {
            let latitude = self.locationCoord!.latitude
            let longitute = self.locationCoord!.longitude
            
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kUpdateAddressNotification"), object: nil, userInfo: ["HomeAddress":self.locationAddress!, "latitude": latitude , "longitude" : longitute])
//            NotificationCenter.defaultCenter().postNotificationName("kUpdateAddressNotification",
//                                                                      object: nil,
//                                                                      userInfo: ["HomeAddress":self.locationAddress!, "latitude": latitude , "longitude" : longitute])
            
            })
        }else{
        
        self.dismiss(animated: true, completion: {
            let latitude = self.locationCoord!.latitude
            let longitute = self.locationCoord!.longitude
            
            if self.isControlerType == "Update" {
            // Update Controller
                
            }else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "kSetHomeAddressNotification"), object: nil, userInfo: ["HomeAddress":self.locationAddress!, "latitude": latitude , "longitude" : longitute])
//            NotificationCenter.default.postNotificationName("kSetHomeAddressNotification",
//                object: nil,
//                userInfo: ["HomeAddress":self.locationAddress!, "latitude": latitude , "longitude" : longitute])
                
            }
        });
    }
}
    
    func getCurrentLocation () {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .denied {
                //                AlertController.showAlertForMessage("You have denied access to your location, please enable it in settings.")
                callLocationServiceEnablePopUp()
                return
            }
        }
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else {
            print("location service not enabled")
        }
    }
    
    func callLocationServiceEnablePopUp() -> Void
    {
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedWhenInUse, .restricted, .denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order to be notified, please open this app's settings and set location access to 'Always'.",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        default:
            break
            
            
        }
    }
    
    // MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationCoord = manager.location!.coordinate
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
        } else if status == .denied {
            //AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
            callLocationServiceEnablePopUp()
        }
    }
    
    // MARK:- Google Map Delegates
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        self.userMovedMap = self.userMovedMap ? self.userMovedMap : gesture
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if self.userMovedMap == true {
            self.userMovedMap = false
            self.locationCoord = position.target
            self.moveMapToCoord(self.locationCoord)
            reverseGeocodeCoordinate(self.locationCoord)
        }
    }
    
    func moveMapToCoord(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        self.locationCoord = coordinate
        let camera = self.mapView.camera
        let cameraPosition = GMSCameraPosition.camera(withTarget: coord, zoom: camera.zoom)
        self.mapView.animate(to: cameraPosition)
    }
    
    // MARK:- Reverse Geocode
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coord) { [weak self] response , error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]!
                self?.address.text = lines?.joined(separator: "\n")
                if let address = self?.address.text {
                    self?.locationAddress = address
                }
            }
        }
    }
}
