//
//  CustomeAlertViewController.swift
//  rideally
//
//  Created by Pratik Bhaliya on 11/03/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

protocol CustomeAlertViewControllerDelegate: class {
}

class CustomeAlertViewController: UIViewController {

    weak var delegate:CustomeAlertViewController?
    
    @IBOutlet weak var messageLabel: UILabel!
    var address : String!
    var latitude : String!
    var longitude : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let main_string =  "Do you want to save \(address) as your Home Location?"
        let string_to_color = address
        let range = (main_string as NSString).range(of: string_to_color!)
        
        let attributedString = NSMutableAttributedString(string:main_string)
        attributedString.addAttribute(NSForegroundColorAttributeName, value: Colors.BG_GREEN_COLOR, range: range)
        self.messageLabel.attributedText = attributedString
    }
    


    @IBAction func setHomeLocationYesButtonClicked(_ sender: AnyObject) {
        //TODO: Need tp set Address.
        UserInfo.sharedInstance.homeLongValue = longitude
        UserInfo.sharedInstance.homeLatValue = latitude
        UserInfo.sharedInstance.homeAddress = address
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "kSetHomeDefaultAddressNotification"),
                                                                  object: nil,
                                                                  userInfo: ["HomeAddress":self.address!, "latitude": latitude , "longitude" : longitude])

        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func noButtonClicked(_ sender: AnyObject) {
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "kSetHomeDefaultAddressNotification"),
                                                                  object: nil,
                                                                  userInfo: ["HomeAddress":self.address!, "latitude": latitude , "longitude" : longitude])
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
}
