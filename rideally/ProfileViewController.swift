//
//  ProfileViewController.swift
//  rideally
//
//  Created by Sarav on 24/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import MobileCoreServices

class ProfileViewController: UIViewController,UIImagePickerControllerDelegate {
    
    var  emailButtonView = UIButton()
    var  mobileNumberButtonView = UIButton()
    var  homeLocationButtonView = UIButton()
    var  workLocationButtonView = UIButton()
    var  vechileListButtonView = UIButton()
    
    
    var homeAddresslLabelValue = UILabel()
    var workAddresslLabelValue = UILabel()
    var carTypeLabelValue = UILabel()
    var carModelLabelValue = UILabel()
    var carSeatInfoLabelValue = UILabel()
    let  vehicleimageView = UIImageView()
    let  verifyEmailIcon  = UIImageView()
    
    let baseView = UIView()
    
    
    let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
    
    //let baseScrollView = UIScrollView()
    
    var yPost:  NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Profile"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        
        /* not avdicable to use*/
        //getProfileDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setBGImageForView()
        getAllVehicleList()
        
        (self.baseView.viewWithTag(1000) as? UILabel)?.text =  UserInfo.sharedInstance.fullName
        
        /*if  UserInfo.sharedInstance.vehicleSeqID.characters.count > 0
         {
         self.carTypeLabelValue.text = UserInfo.sharedInstance.vehicleType
         self.carModelLabelValue.text = UserInfo.sharedInstance.vehicleModel
         self.carSeatInfoLabelValue.text = UserInfo.sharedInstance.vehicleSeatCapacity
         self.setVehicleTypeImage(UserInfo.sharedInstance.vehicleType)
         }
         else
         {
         getAllVehicleList()
         }*/
    }
    
    
    
    
    
    override func loadView() {
        super.loadView()
        
        self.view.addSubview(backgroundImage)
        self.view.sendSubview(toBack: backgroundImage)
        //let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(ProfileViewController.profileImageTapped))
        //backgroundImage.userInteractionEnabled = true
        //backgroundImage.addGestureRecognizer(tapGestureRecognizer)
        
        
        createBaseView()
        createHeaderView()
        createEmailBGView()
        createMobileNumberBGView()
        createHomeaddressBGView()
        createWorkaddressBGView()
        createVechileListBGView()
    }
    
    
    
    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.BLACK_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.left
        return labelObject
    }
    
    
    func createButtonBGView() -> UIButton
    {
        let  buttonObject = UIButton()
        buttonObject.titleLabel?.font = boldFontWithSize(20)
        buttonObject.backgroundColor = Colors.WHITE_COLOR
        buttonObject.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        buttonObject.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        buttonObject.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        return buttonObject
    }
    
    
    func setBGImageForView() -> Void
    {
        if(UserInfo.sharedInstance.profilepicStatus == "1"){
            if UserInfo.sharedInstance.profilepicUrl != ""{
                backgroundImage.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(UserInfo.sharedInstance.profilepicUrl)"))
            }
            else{
                if UserInfo.sharedInstance.faceBookid != ""{
                    backgroundImage.kf.setImage(with: URL(string: "https://graph.facebook.com/\(UserInfo.sharedInstance.faceBookid)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            backgroundImage.image = UIImage(named: "DefaultProfile")
        }
    }
    
    func createBaseView() -> Void
    {
        self.baseView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.baseView)
        let swipeUp = UISwipeGestureRecognizer(target:self, action:#selector(respondToSwipeGesture(_:)))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        
        let swipeDown = UISwipeGestureRecognizer(target:self, action:#selector(respondToSwipeGestureDown(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        
        self.baseView.addGestureRecognizer(swipeUp)
        self.baseView.addGestureRecognizer(swipeDown)
        
        let  imageButtonView  = UIImageView()
        imageButtonView.translatesAutoresizingMaskIntoConstraints = false
        imageButtonView.image = UIImage(named: "Arrow_UP_ProfilePage")
        imageButtonView.tag = 100;
        self.baseView.addSubview(imageButtonView)
        
        
        let screenSize: CGRect = UIScreen.main.bounds
        let heightValue = screenSize.height - 60
        
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[baseview(415)]", options:[], metrics:nil, views: ["baseview": self.baseView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[baseview]|", options:[], metrics:nil, views: ["baseview": self.baseView]))
        
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[imgview(20)]", options:[], metrics:nil, views: ["imgview": imageButtonView]))
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-padding-[imgview(20)]", options:[], metrics:["padding":(screenSize.width/2)-10], views: ["imgview": imageButtonView]))
        
        
        //baseview.top = (self.view.top x multipler) + constant
        yPost = NSLayoutConstraint(item: baseView, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: heightValue)
        self.view.addConstraint(yPost)
    }
    
    
    
    func respondToSwipeGesture(_ gesture: UIGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer
        {
            switch swipeGesture.direction
            {
            case UISwipeGestureRecognizerDirection.up:
                let screenSize: CGRect = UIScreen.main.bounds
                self.baseView .viewWithTag(100)?.isHidden = true
                yPost.constant = screenSize.height - 410
            case UISwipeGestureRecognizerDirection.down:
                let screenSize: CGRect = UIScreen.main.bounds
                self.baseView .viewWithTag(100)?.isHidden = false
                yPost.constant = screenSize.height - 60
            default:
                break
            }
        }
    }
    
    func respondToSwipeGestureDown(_ gesture:UIGestureRecognizer)
    {
        self .respondToSwipeGesture(gesture)
    }
    
    
    func createHeaderView() -> Void {
        
        let  headerView = UIView()
        headerView.translatesAutoresizingMaskIntoConstraints = false
        self.baseView.addSubview(headerView)
        
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[headerview(40)]", options:[], metrics:nil, views: ["headerview": headerView]))
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[headerview]|", options:[], metrics:nil, views: ["headerview": headerView]))
        
        let  headerValueLabel = getLabelSubViews("Profile Name")
        headerValueLabel.text = UserInfo.sharedInstance.fullName
        headerValueLabel.tag = 1000
        headerValueLabel.font = normalFontWithSize(18)
        headerValueLabel.textColor = Colors.WHITE_COLOR
        headerValueLabel.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        headerValueLabel.textAlignment = NSTextAlignment.center
        headerValueLabel.translatesAutoresizingMaskIntoConstraints = false
        headerView.addSubview(headerValueLabel)
        
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[headerLbl]|", options:[], metrics:nil, views: ["headerLbl": headerValueLabel]))
        headerView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[headerLbl]|", options:[], metrics:nil, views: ["headerLbl": headerValueLabel]))
    }
    
    
    func createEmailBGView() -> Void {
        
        self.emailButtonView = createButtonBGView()
        
        let titleLabelValue = getLabelSubViews("EMAIL")
        titleLabelValue.textColor = Colors.GRAY_COLOR
        
        let emailLabelValue = getLabelSubViews(UserInfo.sharedInstance.email)
        emailLabelValue.isHidden = false
        emailLabelValue.font = normalFontWithSize(15)
        emailLabelValue.text = UserInfo.sharedInstance.email
        
        
        verifyEmailIcon.image = UIImage(named: "email_v")
        verifyEmailIcon.isHidden = true
        
        let verifyLabelView = getLabelSubViews("")
        verifyLabelView.tag = 500
        let myAttribute = [NSFontAttributeName: normalFontWithSize(14)!]
        let myString = NSMutableAttributedString(string: "Verify", attributes: myAttribute)
        let myRange = NSRange(location: 0, length: 6)
        myString.addAttribute(NSForegroundColorAttributeName, value: Colors.GREEN_COLOR, range: myRange)
        verifyLabelView.attributedText = myString
        let tapGesture = UITapGestureRecognizer(target:self, action:#selector(labelGesturePressed(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        verifyLabelView.isUserInteractionEnabled=true
        verifyLabelView.addGestureRecognizer(tapGesture)
        
        
        let  lineSeperator = UIView()
        lineSeperator.backgroundColor = UIColor.lightGray
        
        
        emailButtonView.translatesAutoresizingMaskIntoConstraints = false
        emailLabelValue.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        verifyLabelView.translatesAutoresizingMaskIntoConstraints = false
        verifyEmailIcon.translatesAutoresizingMaskIntoConstraints = false
        lineSeperator.translatesAutoresizingMaskIntoConstraints = false
        
        let  emailView = UIView()
        emailView.translatesAutoresizingMaskIntoConstraints = false
        self.baseView.addSubview(emailView)
        
        emailView.addSubview(self.emailButtonView)
        self.emailButtonView.addSubview(emailLabelValue)
        self.emailButtonView.addSubview(titleLabelValue)
        self.emailButtonView.addSubview(verifyLabelView)
        self.emailButtonView.addSubview(verifyEmailIcon)
        self.emailButtonView.addSubview(lineSeperator)
        
        if UserInfo.sharedInstance.userState == "3" || UserInfo.sharedInstance.userState == "5"
        {
            verifyEmailIcon.isHidden = false
            verifyLabelView.isHidden = true
        }
        
        
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[emailView(50)]", options:[], metrics:nil, views: ["emailView": emailView]))
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[emailView]|", options:[], metrics:nil, views: ["emailView": emailView]))
        
        emailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[emailButtonView]|", options:[], metrics:nil, views: ["emailButtonView": emailButtonView]))
        emailView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[emailButtonView]|", options:[], metrics:nil, views: ["emailButtonView": emailButtonView]))
        
        let subViewsDict = ["emailLabelVal":emailLabelValue,"titleLabelVal":titleLabelValue,"verifyLabel":verifyLabelView,"line":lineSeperator]
        self.emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[titleLabelVal]-85-|", options:[], metrics:nil, views: subViewsDict))
        self.emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[emailLabelVal]-85-|", options:[], metrics:nil, views: subViewsDict))
        self.emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[verifyLabel(65)]-2-|", options:[], metrics:nil, views: subViewsDict))
        
        self.emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[emailverify(15)]-30-|", options:[], metrics:nil, views: ["emailverify":verifyEmailIcon]))
        self.emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-15-[emailverify(15)]", options:[], metrics:nil, views: ["emailverify":verifyEmailIcon]))
        
        self.emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[verifyLabel]|", options:[], metrics:nil, views: subViewsDict))
        self.emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)][emailLabelVal(==titleLabelVal)]-4-[line(1)]", options:[], metrics:nil, views: subViewsDict))
        
        emailButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options:[], metrics:nil, views: ["line": lineSeperator]))
    }
    
    func createMobileNumberBGView() -> Void
    {
        
        self.mobileNumberButtonView = createButtonBGView()
        self.mobileNumberButtonView.tag = 101
        
        let titleLabelValue = getLabelSubViews("PHONE")
        titleLabelValue.textColor = Colors.GRAY_COLOR
        
        
        let mobileLabelValue = getLabelSubViews("+91"+"\(UserInfo.sharedInstance.mobile)")
        mobileLabelValue.font = normalFontWithSize(15)
        
        let  imageButtonView  = UIImageView()
        imageButtonView.image = UIImage(named: "RightArrow_user")
        
        let  lineSeperator = UIView()
        lineSeperator.backgroundColor = UIColor.lightGray
        
        self.mobileNumberButtonView.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        mobileLabelValue.translatesAutoresizingMaskIntoConstraints = false
        imageButtonView.translatesAutoresizingMaskIntoConstraints = false
        lineSeperator.translatesAutoresizingMaskIntoConstraints = false
        
        let  mobileNumberView = UIView()
        mobileNumberView.translatesAutoresizingMaskIntoConstraints = false
        self.baseView.addSubview(mobileNumberView)
        
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-110-[mobilenumberview(60)]", options:[], metrics:nil, views: ["mobilenumberview": mobileNumberView]))
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mobilenumberview]|", options:[], metrics:nil, views: ["mobilenumberview": mobileNumberView]))
        
        mobileNumberView.addSubview(self.mobileNumberButtonView)
        mobileNumberButtonView.addSubview(titleLabelValue)
        mobileNumberButtonView.addSubview(mobileLabelValue)
        mobileNumberButtonView.addSubview(imageButtonView)
        mobileNumberButtonView.addSubview(lineSeperator)
        
        
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mobileButtonView]|", options:[], metrics:nil, views: ["mobileButtonView": mobileNumberButtonView]))
        mobileNumberView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mobileButtonView]|", options:[], metrics:nil, views: ["mobileButtonView": mobileNumberButtonView]))
        
        let subViewsDict = ["titleLabelVal":titleLabelValue,"mobileLabelVal":mobileLabelValue,"imageButton":imageButtonView,"line":lineSeperator]
        
        self.mobileNumberButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[titleLabelVal]-85-|", options:[], metrics:nil, views: subViewsDict))
        self.mobileNumberButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)]-5-[mobileLabelVal(==titleLabelVal)]-9-[line(1)]", options:[], metrics:nil, views: subViewsDict))
        self.mobileNumberButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options:[], metrics:nil, views: ["line": lineSeperator]))
        
        self.mobileNumberButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[mobileLabelVal]-85-|", options:[], metrics:nil, views: subViewsDict))
        
        self.mobileNumberButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imageButton(15)]-30-|", options:[], metrics:nil, views: subViewsDict))
        self.mobileNumberButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-25-[imageButton(15)]", options:[], metrics:nil, views: subViewsDict))
    }
    
    func createHomeaddressBGView() -> Void
    {
        self.homeLocationButtonView = createButtonBGView()
        self.homeLocationButtonView.tag = 102
        
        let titleLabelValue = getLabelSubViews("HOME")
        titleLabelValue.textColor = Colors.GRAY_COLOR
        
        
        let  lineSeperator = UIView()
        lineSeperator.backgroundColor = UIColor.lightGray
        
        
        homeAddresslLabelValue = getLabelSubViews("")
        homeAddresslLabelValue.text = "Select Home Address"
        
        if  UserInfo.sharedInstance.homeAddress.characters.count > 0
        {
            homeAddresslLabelValue.text = UserInfo.sharedInstance.homeAddress
        }
        
        
        homeAddresslLabelValue.font = normalFontWithSize(13)
        homeAddresslLabelValue.numberOfLines = 0
        homeAddresslLabelValue.lineBreakMode = .byWordWrapping
        
        let  imageButtonView  = UIImageView()
        imageButtonView.image = UIImage(named: "RightArrow_user")
        
        
        let  imageView = UIImageView()
        imageView.image = UIImage(named: "Map_Indicator")
        
        self.homeLocationButtonView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        homeAddresslLabelValue.translatesAutoresizingMaskIntoConstraints = false
        imageButtonView.translatesAutoresizingMaskIntoConstraints = false
        lineSeperator.translatesAutoresizingMaskIntoConstraints = false
        
        let  homeAddrerssView = UIView()
        homeAddrerssView.translatesAutoresizingMaskIntoConstraints = false
        self.baseView.addSubview(homeAddrerssView)
        
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-170-[homeaddrerssview(80)]", options:[], metrics:nil, views: ["homeaddrerssview": homeAddrerssView]))
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[homeaddrerssview]|", options:[], metrics:nil, views: ["homeaddrerssview": homeAddrerssView]))
        
        
        homeAddrerssView.addSubview(self.homeLocationButtonView)
        self.homeLocationButtonView.addSubview(imageView)
        self.homeLocationButtonView.addSubview(titleLabelValue)
        self.homeLocationButtonView.addSubview(homeAddresslLabelValue)
        self.homeLocationButtonView.addSubview(imageButtonView)
        self.homeLocationButtonView.addSubview(lineSeperator)
        
        homeAddrerssView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[homeAddressView]|", options:[], metrics:nil, views: ["homeAddressView": homeLocationButtonView]))
        homeAddrerssView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[homeAddressView]|", options:[], metrics:nil, views: ["homeAddressView": homeLocationButtonView]))
        
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[imgView]-20-|", options: [], metrics: nil, views: ["imgView":imageView]))
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imgView(40)]", options: [], metrics: nil, views: ["imgView":imageView]))
        
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[titleLabelVal]-85-|", options:[], metrics:nil, views: ["titleLabelVal": titleLabelValue]))
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[homeAddresslLbl]-85-|", options:[], metrics:nil, views: ["homeAddresslLbl": homeAddresslLabelValue]))
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)][homeAddresslLbl(50)]-4-[line(1)]", options:[], metrics:nil, views: ["titleLabelVal": titleLabelValue,"homeAddresslLbl": homeAddresslLabelValue,"line":lineSeperator]))
        
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options:[], metrics:nil, views: ["line": lineSeperator]))
        
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imageButton(15)]-30-|", options:[], metrics:nil, views: ["imageButton": imageButtonView]))
        self.homeLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-35-[imageButton(15)]", options:[], metrics:nil, views: ["imageButton": imageButtonView]))
    }
    
    
    
    func createWorkaddressBGView() -> Void
    {
        self.workLocationButtonView = createButtonBGView()
        self.workLocationButtonView.tag = 103
        
        let titleLabelValue = getLabelSubViews("WORK")
        titleLabelValue.textColor = Colors.GRAY_COLOR
        
        
        workAddresslLabelValue = getLabelSubViews("Select Work Location")
        
        if  UserInfo.sharedInstance.workAddress.characters.count > 0
        {
            workAddresslLabelValue.text = UserInfo.sharedInstance.workAddress
        }
        
        workAddresslLabelValue.font = normalFontWithSize(13)
        workAddresslLabelValue.numberOfLines = 0
        workAddresslLabelValue.lineBreakMode = .byWordWrapping
        
        let  lineSeperator = UIView()
        lineSeperator.backgroundColor = UIColor.lightGray
        
        
        let  imageButtonView  = UIImageView()
        imageButtonView.image = UIImage(named: "RightArrow_user")
        
        
        let  imageView = UIImageView()
        imageView.image = UIImage(named: "Map_Indicator")
        
        
        self.workLocationButtonView.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        workAddresslLabelValue.translatesAutoresizingMaskIntoConstraints = false
        imageButtonView.translatesAutoresizingMaskIntoConstraints = false
        lineSeperator.translatesAutoresizingMaskIntoConstraints = false
        
        
        let  workAddrerssView = UIView()
        workAddrerssView.translatesAutoresizingMaskIntoConstraints = false
        self.baseView.addSubview(workAddrerssView)
        
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-250-[workaddrerssview(80)]", options:[], metrics:nil, views: ["workaddrerssview": workAddrerssView]))
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[workaddrerssview]|", options:[], metrics:nil, views: ["workaddrerssview": workAddrerssView]))
        
        
        workAddrerssView.addSubview(self.workLocationButtonView)
        self.workLocationButtonView.addSubview(imageView)
        self.workLocationButtonView.addSubview(titleLabelValue)
        self.workLocationButtonView.addSubview(workAddresslLabelValue)
        self.workLocationButtonView.addSubview(imageButtonView)
        self.workLocationButtonView.addSubview(lineSeperator)
        
        
        workAddrerssView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[workAddressView]|", options:[], metrics:nil, views: ["workAddressView": workLocationButtonView]))
        workAddrerssView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[workAddressView]|", options:[], metrics:nil, views: ["workAddressView": workLocationButtonView]))
        
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[imgView]-20-|", options: [], metrics: nil, views: ["imgView":imageView]))
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imgView(40)]", options: [], metrics: nil, views: ["imgView":imageView]))
        
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[titleLabelVal]-85-|", options:[], metrics:nil, views: ["titleLabelVal": titleLabelValue]))
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[workAddresslLbl]-85-|", options:[], metrics:nil, views: ["workAddresslLbl": workAddresslLabelValue]))
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)][workAddresslLbl(50)]-4-[line(1)]", options:[], metrics:nil, views: ["titleLabelVal": titleLabelValue,"workAddresslLbl": workAddresslLabelValue,"line":lineSeperator]))
        
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line]|", options:[], metrics:nil, views: ["line": lineSeperator]))
        
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imageButton(15)]-30-|", options:[], metrics:nil, views: ["imageButton": imageButtonView]))
        self.workLocationButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-35-[imageButton(15)]", options:[], metrics:nil, views: ["imageButton": imageButtonView]))
        
    }
    
    
    func createVechileListBGView() -> Void
    {
        
        self.vechileListButtonView = createButtonBGView()
        self.vechileListButtonView.tag = 104
        
        let titleLabelValue = getLabelSubViews("DEFAULT VEHICLE")
        titleLabelValue.textColor = Colors.GRAY_COLOR
        
        carTypeLabelValue = getLabelSubViews("")
        carTypeLabelValue.font = normalFontWithSize(13)
        
        carModelLabelValue = getLabelSubViews("")
        carModelLabelValue.font = normalFontWithSize(13)
        
        carSeatInfoLabelValue = getLabelSubViews("")
        carSeatInfoLabelValue.font = normalFontWithSize(11)
        carSeatInfoLabelValue.textColor = Colors.GRAY_COLOR
        
        let  imageButtonView  = UIImageView()
        imageButtonView.image = UIImage(named: "RightArrow_user")
        
        
        vehicleimageView.image = UIImage(named: "Ride")
        
        self.vechileListButtonView.translatesAutoresizingMaskIntoConstraints = false
        vehicleimageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabelValue.translatesAutoresizingMaskIntoConstraints = false
        carTypeLabelValue.translatesAutoresizingMaskIntoConstraints = false
        carModelLabelValue.translatesAutoresizingMaskIntoConstraints = false
        carSeatInfoLabelValue.translatesAutoresizingMaskIntoConstraints = false
        imageButtonView.translatesAutoresizingMaskIntoConstraints = false
        
        let  vechileListView = UIView()
        vechileListView.translatesAutoresizingMaskIntoConstraints = false
        self.baseView.addSubview(vechileListView)
        
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-330-[vechilelistview(80)]", options:[], metrics:nil, views: ["vechilelistview": vechileListView]))
        self.baseView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[vechilelistview]|", options:[], metrics:nil, views: ["vechilelistview": vechileListView]))
        
        vechileListView.addSubview(self.vechileListButtonView)
        self.vechileListButtonView.addSubview(vehicleimageView)
        self.vechileListButtonView.addSubview(titleLabelValue)
        self.vechileListButtonView.addSubview(carTypeLabelValue)
        self.vechileListButtonView.addSubview(carModelLabelValue)
        self.vechileListButtonView.addSubview(carSeatInfoLabelValue)
        self.vechileListButtonView.addSubview(imageButtonView)
        
        vechileListView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[vechileListView]|", options:[], metrics:nil, views: ["vechileListView": vechileListButtonView]))
        vechileListView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[vechileListView]|", options:[], metrics:nil, views: ["vechileListView": vechileListButtonView]))
        
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[imgView]-20-|", options: [], metrics: nil, views: ["imgView":vehicleimageView]))
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imgView(40)]", options: [], metrics: nil, views: ["imgView":vehicleimageView]))
        
        
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[titleLabelVal]-85-|", options:[], metrics:nil, views: ["titleLabelVal": titleLabelValue]))
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[type]-85-|", options:[], metrics:nil, views: ["type": carTypeLabelValue]))
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[model]-85-|", options:[], metrics:nil, views: ["model": carModelLabelValue]))
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-70-[seat]-85-|", options:[], metrics:nil, views: ["seat": carSeatInfoLabelValue]))
        
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[titleLabelVal(20)][type]-5-[model]-5-[seat]-5-|", options:[], metrics:nil, views: ["titleLabelVal": titleLabelValue,"type": carTypeLabelValue,"model": carModelLabelValue,"seat": carSeatInfoLabelValue]))
        
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imageButton(15)]-30-|", options:[], metrics:nil, views: ["imageButton": imageButtonView]))
        self.vechileListButtonView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-35-[imageButton(15)]", options:[], metrics:nil, views: ["imageButton": imageButtonView]))
    }
    
    
    func buttonPressed(_ objectInstance:AnyObject) -> Void{
        
        if objectInstance.tag == 101
        {
            self.navigationController?.pushViewController(UserDetailViewController(), animated: true)
        }
        else if objectInstance.tag == 102
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                let vc = MapViewController()
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.onSelectionBlock = { (location) -> Void in
                    if let loc = location as? [String: String]
                    {
                        self.homeAddresslLabelValue.text = loc["location"]
                        UserInfo.sharedInstance.homeAddress = loc["location"]!
                        UserInfo.sharedInstance.homeLatValue = loc["lat"]!
                        UserInfo.sharedInstance.homeLongValue = loc["long"]!
                        
                        self.sendHomeAddressedUpdateRequest()
                        
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if objectInstance.tag == 103
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                let vc = MapViewController()
                vc.edgesForExtendedLayout = UIRectEdge()
                vc.onSelectionBlock = { (location) -> Void in
                    if let loc = location as? [String: String]
                    {
                        self.workAddresslLabelValue.text = loc["location"]
                        UserInfo.sharedInstance.workAddress = loc["location"]!
                        UserInfo.sharedInstance.workLatValue = loc["lat"]!
                        UserInfo.sharedInstance.workLongValue = loc["long"]!
                        
                        self.sendWorkAddressedUpdateRequest()
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if objectInstance.tag == 104
        {
            if !isServerReachable() {
                AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
            } else {
                if self.carTypeLabelValue.text == "Register new vehicle"
                {
                    let vc = RegisterNewVehicleController()
                    vc.onAddSuccessAction = {
                        self.getAllVehicleList()
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else
                {
                    self.navigationController?.pushViewController(AddVehicleViewController(), animated: true)
                }
            }
        }
    }
    
    func sendHomeAddressedUpdateRequest() -> Void {
        
        let reqObj = SetHomeLocationRequest()
        showIndicator("Loading...")
        reqObj.user_id = UserInfo.sharedInstance.userID
        reqObj.home_loc = UserInfo.sharedInstance.homeAddress
        reqObj.home_loc_lat = UserInfo.sharedInstance.homeLatValue
        reqObj.home_loc_long = UserInfo.sharedInstance.homeLongValue
        
        ProfileRequestor().sendsetHomeLocationRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            
            if (object as! SetHomeLocationResponse).code == "8535"
            {
                
                if (object as! SetHomeLocationResponse).dataObj?.first != nil
                {
                    AlertController.showToastForInfo("Home Location updated.")
                    self.homeAddresslLabelValue.text = (object as! SetHomeLocationResponse).dataObj?.first?.address
                }
            }
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
    
    func sendWorkAddressedUpdateRequest() -> Void {
        
        let reqObj = SetWorkLocationRequest()
        showIndicator("Loading...")
        reqObj.user_id = UserInfo.sharedInstance.userID
        reqObj.wp_loc = UserInfo.sharedInstance.workAddress
        reqObj.wp_loc_lat = UserInfo.sharedInstance.workLatValue
        reqObj.wp_loc_long = UserInfo.sharedInstance.workLongValue
        
        ProfileRequestor().sendsetWorkLocationRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            
            if (object as! SetWorkLocationResponse).code == "8535"
            {
                if (object as! SetWorkLocationResponse).dataObj?.first != nil
                {
                    AlertController.showToastForInfo("Workplace Location updated.")
                    //self.workAddresslLabelValue.text = (object as! SetWorkLocationResponse).dataObj?.first?.address
                }
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
    
    func setVehicleTypeImage(_ vehicleObj:String) -> Void {
        var imgvalue : String!
        
        if  vehicleObj == "Bike"
        {
            imgvalue = "BikeIcon_Profile"
        }
        else if vehicleObj == "Auto"
        {
            imgvalue = "AutoIcon_Profile"
        }
        else if vehicleObj == "Car"
        {
            imgvalue = "CarIcon_Profile"
        }
        else if vehicleObj == "Cab"
        {
            imgvalue = "CabIcon_Profile"
        }
        else if vehicleObj == "Suv"
        {
            imgvalue = "SuvIcon_Profile"
        }
        else if vehicleObj == "Tempo"
        {
            imgvalue = "VanIcon_Profile"
        }
        else if vehicleObj == "Bus"
        {
            imgvalue = "BusIcon_Profile"
        } else {
            imgvalue = "CarIcon_Profile"
        }
        
        self.vehicleimageView.image = UIImage(named: imgvalue)
    }
    
    func getAllVehicleList() -> Void {
        
        if  UserInfo.sharedInstance.userID.characters.count > 0
        {
            let reqObj = GetALLVehicleListRequest()
            showIndicator("Loading...")
            reqObj.userID = UserInfo.sharedInstance.userID
            
            ProfileRequestor().getAddVehicleListData(reqObj, success:{ (success, object) in
                hideIndicator()
                
                if (object as! GetALLVehicleListResponse).code == "0"
                {
                    
                    if (object as! GetALLVehicleListResponse).dataObj?.first != nil
                    {
                        
                        for item in (object as! GetALLVehicleListResponse).dataObj!
                        {
                            if item.isDefault == "1"
                            {
                                self.carTypeLabelValue.text = item.vehicleType
                                self.carModelLabelValue.text = item.vehicleModel
                                self.carSeatInfoLabelValue.text = item.capacity
                                self.setVehicleTypeImage(item.vehicleType!)
                            }
                        }
                    }
                }
                else
                {
                    self.carTypeLabelValue.text = "Register new vehicle"
                    self.vehicleimageView.image = UIImage(named: "Ride")
                    self.carModelLabelValue.text = ""
                    self.carSeatInfoLabelValue.text = ""
                }
                
                
            }){ (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
        }
        else
        {
            AlertController.showToastForError("Invalid UserID provided in the Request.")
        }
    }
    
    func labelGesturePressed(_ sender:AnyObject)  {
        
        let reqObj = ResendEmailRequest()
        showIndicator("Loading...")
        reqObj.userID = UserInfo.sharedInstance.userID
        
        
        ProfileRequestor().sendVerifyEmailRequest(reqObj, success:{ (success, object) in
            hideIndicator()
            
            if (object as! ResendEmailResponse).code == "1099"
            {
                AlertController.showToastForInfo("A verification link has been sent to your Email Inbox. Please do check SPAM folder as well. Please verify your Email ID")
            }
            else
            {
                AlertController.showToastForError((object as! ResendEmailResponse).message!)
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
    
    
    /*  func profileImageTapped() -> Void {
     
     let vc = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
     
     let imgPicker = UIImagePickerController()
     imgPicker.delegate = self
     imgPicker.mediaTypes = [kUTTypeImage as String]
     
     let capturePhoto = UIAlertAction(title: "Capture Photo", style: .Default, handler: { (action) in
     imgPicker.sourceType = .Camera
     self.presentViewController(imgPicker, animated: true, completion: nil)
     })
     vc.addAction(capturePhoto)
     
     let photoLibrary = UIAlertAction(title: "Photo Library", style: .Default, handler: { (action) in
     imgPicker.sourceType = .PhotoLibrary
     self.presentViewController(imgPicker, animated: true, completion: nil)
     })
     vc.addAction(photoLibrary)
     
     let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: { (action) in
     
     })
     vc.addAction(actionCancel)
     
     presentViewController(vc, animated: true, completion: nil)
     }
     
     
     func presentImagePickerView() -> Void {
     let imgPicker = UIImagePickerController()
     imgPicker.delegate=self
     imgPicker.sourceType = .PhotoLibrary
     imgPicker.mediaTypes = [kUTTypeImage as String]
     self.presentViewController(imgPicker, animated: true, completion: nil)
     
     }
     
     
     //Uiimage Picker view delegate
     func imagePickerControllerDidCancel(picker: UIImagePickerController) {
     self.dismissViewControllerAnimated(true, completion: nil)
     }
     
     func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
     self.dismissViewControllerAnimated(true, completion: nil)
     }*/
    
    
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
}
