//
//  ForgetPasswordViewController.swift
//  rideally
//
//  Created by Raghunathan on 8/7/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class ForgetPasswordViewController: UIViewController,UITextFieldDelegate {

    var emailTextBox = RATextField()
    var emailLabel: UILabel!
    var yPost_Email:  NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Forgot Password"
    }

    
    override func loadView() {
        super.loadView()
        createForgotPasswordViewSubviews()
    }

    
    func createForgotPasswordViewSubviews() ->  Void
    {
     
        self.view.backgroundColor = Colors.WHITE_COLOR
        
        let descriptionLabel = UILabel()
        descriptionLabel.font = normalFontWithSize(15)
        descriptionLabel.backgroundColor = UIColor.clear
        descriptionLabel.numberOfLines = 0
        
        descriptionLabel.textColor = Colors.BLACK_COLOR
        descriptionLabel.text="Please enter your email address. You will receive a new password via email"
        descriptionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        descriptionLabel.numberOfLines = 0
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.textAlignment = NSTextAlignment.left
        
        emailLabel  =  getLabelSubViews("EMAIL")
        emailLabel.isHidden = true
        emailLabel.font = normalFontWithSize(12)
        emailLabel.textColor = Colors.GREEN_COLOR
        emailLabel.textAlignment = NSTextAlignment.left
        
        emailTextBox.returnKeyType = UIReturnKeyType.next
        emailTextBox.delegate = self
        emailTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        emailTextBox.placeholder = "EMAIL"
        emailTextBox.returnKeyType = UIReturnKeyType.done
        emailTextBox.font = normalFontWithSize(15)!
        emailTextBox.textColor = Colors.GRAY_COLOR
        
        
        let btn = UIButton()
        btn.setTitle("SUBMIT", for: UIControlState())
        btn.backgroundColor = Colors.GRAY_COLOR
        btn.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btn.titleLabel?.font = UIFont(name: FONT_NAME.BOLD_FONT, size: 15.0)
        btn.addTarget(self, action: #selector(submitButtonPressed(_:)), for: .touchDown)
        self.view.addSubview(btn)
        
        let  forgotView = UIView()
        self.view.addSubview(forgotView)
        forgotView.addSubview(descriptionLabel)
        forgotView.addSubview(emailTextBox)
        forgotView.addSubview(emailLabel!)
        
        forgotView.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        emailTextBox.translatesAutoresizingMaskIntoConstraints = false
        emailLabel!.translatesAutoresizingMaskIntoConstraints = false
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-70-[forgetview(200)]", options:[], metrics:nil, views: ["forgetview": forgotView,"btnSubmit":btn]))

        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btnSubmit(40)]|", options:[], metrics:nil, views: ["forgetview": forgotView,"btnSubmit":btn]))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[forgetview]|", options:[], metrics:nil, views: ["forgetview": forgotView]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btnSubmit]|", options:[], metrics:nil, views: ["btnSubmit":btn]))
        
        
        let subViewsDict = ["description": descriptionLabel,"email":emailTextBox] as [String : Any]
        forgotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[description(40)]-10-[email(30)]", options:[], metrics:nil, views: subViewsDict))
        forgotView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[description]-10-|", options:[], metrics:nil, views: ["description": descriptionLabel]))
        
        forgotView.addConstraint(NSLayoutConstraint(item: emailTextBox, attribute: .left, relatedBy: .equal, toItem: descriptionLabel, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        forgotView.addConstraint(NSLayoutConstraint(item: emailTextBox, attribute: .width, relatedBy: .equal, toItem: descriptionLabel, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        forgotView.addConstraint(NSLayoutConstraint(item: emailLabel!, attribute: .left, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.left, multiplier: 1, constant: 0))
        forgotView.addConstraint(NSLayoutConstraint(item: emailLabel!, attribute: .width, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        forgotView.addConstraint(NSLayoutConstraint(item: emailLabel!, attribute: .height, relatedBy: .equal, toItem: emailTextBox, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        yPost_Email = NSLayoutConstraint(item: emailLabel!, attribute: .top, relatedBy: .equal, toItem:descriptionLabel, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 0)
        forgotView.addConstraint(yPost_Email)
    }
    
    
    func submitButtonPressed(_ Sender:UIButton) -> Void {
        callFunctionWhenDoneButtonisPressed()
    }
    
    func sendForgotPasswordRequest() -> Void {
        
        let reqObj = ForgotPasswordRequest()
        showIndicator("Loading...")
        reqObj.email = emailTextBox.text
        
        ForgotPasswordRequestor().sendForgotPasswordRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! ForgotPasswordResponse).code == "465"
            {
                
                AlertController.showAlertFor("RideAlly", message: "Please check your Email Inbox for new 'Password Reset Link' and also check spam folder", okButtonTitle: "ok", okAction: {
                    _ = self.navigationController?.popViewController(animated: false)
                })
            }
            else if (object as! ForgotPasswordResponse).code == "466"{
                AlertController.showAlertFor("RideAlly", message:"Sorry, this 'Email Id' is not registered with RideAlly. Please provide registered email id")
            }
            else{
                AlertController.showAlertFor("RideAlly", message: (object as! ForgotPasswordResponse).message)
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
    }
    
    
    func isValidEmailFormate(_ stringValue:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: stringValue)
    }

    
    func handleValidation() ->  String{
        
        if  emailTextBox.text?.characters.count > 0{
            
            if !isValidEmailFormate(emailTextBox.text!)
            {
                return "Please enter valid 'Email Id'"
            }
        }
        else
        {
            return "Please provide 'Email Id'"
        }
        
        return ""
    }
    
    func callFunctionWhenDoneButtonisPressed() -> Void {
        
        if  handleValidation().characters.count  == 0
        {
            sendForgotPasswordRequest()
        }
        else
        {
            AlertController.showToastForError(handleValidation())
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if (textField == emailTextBox)
        {
            yPost_Email.constant = 0
            let placeHolder = NSAttributedString(string: "EMAIL", attributes: [NSForegroundColorAttributeName:Colors.WHITE_COLOR])
            emailTextBox.attributedPlaceholder = placeHolder
            emailLabel .isHidden = true
            callFunctionWhenDoneButtonisPressed()
            emailTextBox.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        if (textField === emailTextBox)
        {
            yPost_Email.constant = 25;
            emailTextBox.placeholder = ""
            emailLabel.isHidden = false
        }
    }

    func getLabelSubViews(_ textValue: String) -> UILabel
    {
        let labelObject = UILabel()
        labelObject.font = normalFontWithSize(15)
        labelObject.backgroundColor = UIColor.clear
        labelObject.textColor = Colors.WHITE_COLOR
        labelObject.text=textValue
        labelObject.textAlignment = NSTextAlignment.center
        return labelObject
    }
}
