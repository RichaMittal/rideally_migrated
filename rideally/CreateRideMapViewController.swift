//
//  CreateRideMapViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 6/2/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import Alamofire
import FirebaseAnalytics
import AddressBook
import AddressBookUI
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class CreateRideMapViewController : BaseScrollViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, ABPeoplePickerNavigationControllerDelegate{
    let segmentRides = HMSegmentedControl()
    //var scrollView = UIScrollView()
    let locationManager = CLLocationManager()
    var locationCoord:CLLocationCoordinate2D?
    var mapView:GMSMapView!
    
    let sourceView = UIView()
    let destView = UIView()
    let viaView = UIView()
    let dateView = UIView()
    let vehView = UIView()
    //let addVehView = UIView()
    //let commentView = UIView()
    let bottonBtnView = UIView()
    
    let lblSource = UILabel()
    let lblDest = UILabel()
    let lblVia = UILabel()
    let txtDate = UITextField()
    let txtTime = UITextField()
    let txtTravel = UITextField()
    let lblVehModel = UILabel()
    let lblSeatLeft = UILabel()
    let lblCost = UILabel()
    //let lblComment = UILabel()
    //let txtComment = UITextField()
    let locationButton = UIButton()
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    let pickerView = UIPickerView()
    let iconVeh = UIImageView()
    let iconSeat = UIImageView()
    let iconCost = UIImageView()
    let stepperSeats = PKYStepper()
    let stepperCost = PKYStepper()
    let imgLine = UIImageView()
    let imgLine1 = UIImageView()
    let imgLine2 = UIImageView()
    let imgLine3 = UIImageView()
    var iconTraffic = UIButton()
    var mapSourcePinImageVIew = UIImageView()
    var mapDestPinImageVIew = UIImageView()
    var btnAddVehicle = UIButton()
    
    var currentLocationView = UIView()
    var userMovedMap: Bool = false
    
    let appDelegate: AppDelegate = sharedAppDelegate()!
    var locationButtonAction: VoidCompletionHandler?
    
    var shouldShowCancelButton = false
    
    var cancelButton = UIButton(type: .system)
    var currentLocationButton = UIButton(type: .custom)
    var locationName: String = ""
    
    var onSelectionBlock: actionBlockWithParam?
    var onConfirmLocationsBlock: actionBlockWith6Params?
    
    var jumpOneScreen: Bool = false
    //var updateData: RideDetail?
    var vehicleConfig: VehicleConfigResponse!
    //var vehicleDefaultDetails: VehicleDefaultResponse!
    
    var routePolylineIndex0: GMSPolyline!
    var routePolylineIndex1: GMSPolyline!
    var routePolylineIndex2: GMSPolyline!
    var routePolyline: GMSPolyline!
    
    var polylineMutArray : NSMutableArray? = NSMutableArray()
    var viaMutArray : NSMutableArray? = NSMutableArray()
    
    let needRideObj = NeedRideRequest()
    let offerVehicleObj = OfferVehicleRequest()
    let searchReqObj = SearchRideRequest()
    var selectedTxtField: UITextField?
    var pickerData: [String] = [String]()
    var triggerFrom = ""
    var costValue = ""
    var SeatValue = ""
    var valueSelected: String?
    var toggleState = 1
    let rightArrowSource = UIImageView()
    let rightArrowDest = UIImageView()
    var changedLocation = false
    
    var placeSearchResultsArray = [AnyObject]()
    var mapCenterPinImageVIew = UIImageView()
    var btnTag = ""
    var signupFlow = false
    var haveData = ""
    var nRideMutArray : NSMutableArray? = NSMutableArray()
    var oRideMutArray : NSMutableArray? = NSMutableArray()
    var mapMarkerMutArray : NSMutableArray? = NSMutableArray()
    var searchRides: [Ride]?
    var infoWindow : RideWindowInfoView!
    var originSelectedRideMarker: GMSMarker!
    var destinationSelectedRideMarker: GMSMarker!
    var sourceLoc = ""
    var sourceLat = ""
    var sourceLong = ""
    var destloc = ""
    var destLat = ""
    var destLong = ""
    var poolTaxiBooking = ""
    var poolId = ""
    var rideType = ""
    var rideCost = ""
    var rideDistance = ""
    var poolGroupInsStatus = ""
    var routePolylineOrange: GMSPolyline!
    //var responses: [InviteResult]?
    //var isFrom = ""
    var ownerName = ""
    var ridePoints = 0
    var allRidesData: [Ride]?
    var showAllRides = false
    var tempSearchRides = false
    var responses: [InviteResult]?
    var mobileMutArr : NSMutableArray = []
    var contactListArray : NSMutableArray = []
    
    override func goBack()
    {
        if(jumpOneScreen){
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true);
        }
        else{
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func hasTabBarItems() -> Bool
    {
        ISWPRIDESSCREEN = false
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ISVEHICLEUPDATED = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.tabBarController?.tabBar.isTranslucent = false
        self.navigationItem.title = "Anywhere"
        if(signupFlow == true) {
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.view.backgroundColor = Colors.GREEN_COLOR
            hidesBottomBarWhenPushed = true
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        } else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "btn_menu"), style: .plain, target: self, action: #selector(showMenu))
        }
        
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "KInviteSendNotificationcreateRide"),object: nil )
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSendNotification), name:NSNotification.Name(rawValue: "KInviteSendNotificationcreateRide"), object: nil)
        
        mapSourcePinImageVIew.image = UIImage(imageLiteralResourceName: "marker_ride_green")
        mapDestPinImageVIew.image = UIImage(imageLiteralResourceName: "marker_ride_green")
        if UserInfo.sharedInstance.gender == "M" {
            pickerData = ["Any", "Only Male"]
        }else if UserInfo.sharedInstance.gender == "F" {
            pickerData = ["Any", "Only Female"]
        }else{
            pickerData = ["Any","Only Male","Only Female"]
        }
        
        getAllRides()
        getConfigData()
        //setupTextfield()
        //setupBottomView()
        viewsArray.append(setupTextfield())
        viewsArray.append(setupGoogleMaps())
        addBottomView(setupBottomView())
        //setupGoogleMaps()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(ISVEHICLEUPDATED) {
            getDefaultVehicle()
            ISVEHICLEUPDATED = false
        }
        if self.locationCoord == nil {
            self.getCurrentLocation()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupGoogleMaps () -> SubView {
        let view = UIView()
        view.backgroundColor = Colors.APP_BG
        if let coord = self.locationCoord {
            let camera = GMSCameraPosition.camera(withLatitude: coord.latitude,
                                                              longitude: coord.longitude, zoom: 16)
            self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        } else if(UserInfo.sharedInstance.homeLatValue != "") {
            let camera = GMSCameraPosition.camera(withLatitude: Double(UserInfo.sharedInstance.homeLatValue)!, longitude: Double(UserInfo.sharedInstance.homeLongValue)!, zoom: 16)
            self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        } else {
            let camera = GMSCameraPosition.camera(withLatitude: 20.593684, longitude: 78.962880, zoom: 16)
            self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        }
        
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.mapView.translatesAutoresizingMaskIntoConstraints = false
        
        segmentRides.backgroundColor = Colors.WHITE_COLOR
        segmentRides.sectionTitles = ["Get Ride", "Offer Vehicle"]
        segmentRides.titleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.GREEN_COLOR]
        segmentRides.selectedTitleTextAttributes = [NSFontAttributeName: boldFontWithSize(14)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
        segmentRides.selectionStyle = HMSegmentedControlSelectionStyleBox
        segmentRides.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone
        segmentRides.selectionIndicatorColor = Colors.GREEN_COLOR
        segmentRides.selectionIndicatorBoxOpacity = 1.0
        segmentRides.translatesAutoresizingMaskIntoConstraints = false
        segmentRides.segmentEdgeInset = UIEdgeInsets.zero
        if(btnTag != "" && btnTag == "103") {
            segmentRides.selectedSegmentIndex = 1
            vehView.isHidden = false
        } else {
            segmentRides.selectedSegmentIndex = 0
            vehView.isHidden = true
        }
        segmentRides.layer.borderColor = Colors.GREEN_COLOR.cgColor
        segmentRides.layer.borderWidth = 1
        //segmentRides.layer.cornerRadius = 5
        
        segmentRides.addTarget(self, action: #selector(rideTypeChanged), for: .valueChanged)
        self.mapView.addSubview(segmentRides)
        
        iconTraffic.setImage(UIImage(named: "traffic_orange"), for: UIControlState())
        iconTraffic.translatesAutoresizingMaskIntoConstraints = false
        iconTraffic.addTarget(self, action: #selector(trafficBtnClicked), for: .touchDown)
        self.mapView.addSubview(iconTraffic)
        
        let iconFilter = UIButton()
        iconFilter.setImage(UIImage(named:"t_filter_white"), for: UIControlState())
        iconFilter.translatesAutoresizingMaskIntoConstraints = false
        iconFilter.backgroundColor = Colors.GREEN_COLOR
        iconFilter.layer.cornerRadius = 15
        iconFilter.contentMode = .center
        iconFilter.addTarget(self, action: #selector(showSearchRides), for: .touchDown)
        self.mapView.addSubview(iconFilter)
        
        let iconListView = UIButton()
        iconListView.setImage(UIImage(named:"t_listview"), for: UIControlState())
        iconListView.translatesAutoresizingMaskIntoConstraints = false
        iconListView.backgroundColor = Colors.GREEN_COLOR
        iconListView.layer.cornerRadius = 15
        iconListView.contentMode = .center
        iconListView.addTarget(self, action: #selector(showAllRidesList), for: .touchDown)
        self.mapView.addSubview(iconListView)
        
        self.mapView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[iTraffic(30)]-10-[segment]-10-[iFilter(30)]-10-[iList(30)]-10-|", options: [], metrics:["padding":(view.frame.size.width/4)-20], views: ["segment":segmentRides,"iTraffic":iconTraffic,"iFilter":iconFilter,"iList":iconListView]))
        self.mapView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[segment(30)]", options: [], metrics: nil, views: ["segment":segmentRides]))
        self.mapView.addConstraint(NSLayoutConstraint(item: iconTraffic, attribute: .centerY, relatedBy: .equal, toItem: segmentRides, attribute: .centerY, multiplier: 1, constant: 0))
        self.mapView.addConstraint(NSLayoutConstraint(item: iconFilter, attribute: .centerY, relatedBy: .equal, toItem: segmentRides, attribute: .centerY, multiplier: 1, constant: 0))
        self.mapView.addConstraint(NSLayoutConstraint(item: iconFilter, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30))
        self.mapView.addConstraint(NSLayoutConstraint(item: iconListView, attribute: .centerY, relatedBy: .equal, toItem: segmentRides, attribute: .centerY, multiplier: 1, constant: 0))
        self.mapView.addConstraint(NSLayoutConstraint(item: iconListView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30))
        
        view.addSubview(self.mapView)
        view.sendSubview(toBack: self.mapView)
        
        let dict = ["mapView": mapView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[mapView]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: (self.view.frame.size.height) - 190)
        return sub
    }
    
    func setupTextfield () -> SubView {
        let view = UIView()
        view.backgroundColor = Colors.APP_BG
        
        sourceView.translatesAutoresizingMaskIntoConstraints = false
        sourceView.backgroundColor = Colors.WHITE_COLOR
        view.addSubview(sourceView)
        
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        sourceView.addSubview(iconSource)
        
        lblSource.text = "From (Select exact or nearby google places)"
        lblSource.textColor = Colors.GRAY_COLOR
        lblSource.font = boldFontWithSize(13)
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(mapForFrom))
        lblSource.isUserInteractionEnabled = true
        lblSource.addGestureRecognizer(tapGestureFrom)
        sourceView.addSubview(lblSource)
        
        rightArrowSource.image = UIImage(named: "rightArrow")
        rightArrowSource.translatesAutoresizingMaskIntoConstraints = false
        sourceView.addSubview(rightArrowSource)
        
        sourceView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[source(20)]|", options: [], metrics: nil, views: ["source":lblSource]))
        sourceView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[isource(15)][source][iarrow(15)]-10-|", options: [], metrics: nil, views: ["isource":iconSource, "source":lblSource, "iarrow":rightArrowSource]))
        
        sourceView.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        sourceView.addConstraint(NSLayoutConstraint(item: lblSource, attribute: .centerY, relatedBy: .equal, toItem: rightArrowSource, attribute: .centerY, multiplier: 1, constant: 0))
        
        viaView.translatesAutoresizingMaskIntoConstraints = false
        viaView.backgroundColor = Colors.WHITE_COLOR
        view.addSubview(viaView)
        
        let iconVia = UIImageView()
        iconVia.image = UIImage(named: "Via_green")
        iconVia.translatesAutoresizingMaskIntoConstraints = false
        viaView.addSubview(iconVia)
        
        lblVia.textColor = Colors.GRAY_COLOR
        lblVia.font = boldFontWithSize(13)
        lblVia.translatesAutoresizingMaskIntoConstraints = false
        viaView.addSubview(lblVia)
        
        viaView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[via(20)]|", options: [], metrics: nil, views: ["via":lblVia]))
        viaView.addConstraint(NSLayoutConstraint(item: iconVia, attribute: .centerY, relatedBy: .equal, toItem: lblVia, attribute: .centerY, multiplier: 1, constant: 0))
        viaView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[ivia(6)]-5-[via]-5-|", options: [], metrics: nil, views: ["ivia":iconVia, "via":lblVia]))
        
        destView.translatesAutoresizingMaskIntoConstraints = false
        destView.backgroundColor = Colors.WHITE_COLOR
        view.addSubview(destView)
        
        let iconDest = UIImageView()
        iconDest.image = UIImage(named: "dest")
        iconDest.translatesAutoresizingMaskIntoConstraints = false
        destView.addSubview(iconDest)
        
        lblDest.text = "To (Select exact or nearby google places)"
        lblDest.textColor = Colors.GRAY_COLOR
        lblDest.font = boldFontWithSize(13)
        lblDest.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureTo = UITapGestureRecognizer(target: self, action: #selector(mapForTo))
        lblDest.isUserInteractionEnabled = true
        lblDest.addGestureRecognizer(tapGestureTo)
        destView.addSubview(lblDest)
        
        rightArrowDest.image = UIImage(named: "rightArrow")
        rightArrowDest.translatesAutoresizingMaskIntoConstraints = false
        destView.addSubview(rightArrowDest)
        
        destView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[dest(20)]|", options: [], metrics: nil, views: ["dest":lblDest]))
        destView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[idest(15)][dest][iarrow(15)]-10-|", options: [], metrics: nil, views: ["idest":iconDest, "dest":lblDest, "iarrow":rightArrowDest]))
        
        destView.addConstraint(NSLayoutConstraint(item: iconDest, attribute: .centerY, relatedBy: .equal, toItem: lblDest, attribute: .centerY, multiplier: 1, constant: 0))
        destView.addConstraint(NSLayoutConstraint(item: lblDest, attribute: .centerY, relatedBy: .equal, toItem: rightArrowDest, attribute: .centerY, multiplier: 1, constant: 0))
        
        let dict = ["source":sourceView, "via":viaView, "dest": destView ]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(30)][via(30)][dest(30)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[source]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[dest]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[via]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))

        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 90)
        return sub
    }
    
    func setupBottomView () -> SubView {
        let view = UIView()
        view.backgroundColor = Colors.APP_BG
        
        dateView.translatesAutoresizingMaskIntoConstraints = false
        dateView.backgroundColor = Colors.WHITE_COLOR
        view.addSubview(dateView)
        
        txtDate.delegate = self
        txtDate.textColor = Colors.GRAY_COLOR
        txtDate.translatesAutoresizingMaskIntoConstraints = false
        txtDate.font = boldFontWithSize(13)
        
        datePicker.datePickerMode = .date
        txtDate.inputView = datePicker
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(datePicked), for: .valueChanged)
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtDate.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV = UIImageView(image: UIImage(named: "cal")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        txtDate.leftView = imgV
        txtDate.leftViewMode = .always
        dateView.addSubview(txtDate)
        
        imgLine.image = UIImage(named: "gray_separator")
        imgLine.translatesAutoresizingMaskIntoConstraints = false
        dateView.addSubview(imgLine)
        
        txtTime.delegate = self
        txtTime.textColor = Colors.GRAY_COLOR
        txtTime.font = boldFontWithSize(13)
        txtTime.translatesAutoresizingMaskIntoConstraints = false
        
        timePicker.datePickerMode = .time
        //timePicker.locale = NSLocale(localeIdentifier: "NL")
        txtTime.inputView = timePicker
        timePicker.date = Date().addingTimeInterval(300)
        //timePicker.timel
        timePicker.addTarget(self, action: #selector(timePicked), for: .valueChanged)
        
        txtTime.inputAccessoryView = keyboardDoneButtonShow
        
        let imgV1 = UIImageView(image: UIImage(named: "time")!)
        imgV1.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
        imgV1.contentMode = .center
        
        txtTime.leftView = imgV1
        txtTime.leftViewMode = .always
        dateView.addSubview(txtTime)
        
        imgLine1.image = UIImage(named: "gray_separator")
        imgLine1.translatesAutoresizingMaskIntoConstraints = false
        dateView.addSubview(imgLine1)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        txtTravel.delegate=self
        txtTravel.textColor = Colors.GRAY_COLOR
        txtTravel.translatesAutoresizingMaskIntoConstraints = false
        txtTravel.inputView = pickerView
        txtTravel.font = boldFontWithSize(13)
        let imgV2 = UIImageView(image: UIImage(named: "travel_with_new")!)
        imgV2.frame = CGRect(x: 0, y: 0, width: 45, height: 45)
        imgV2.contentMode = .center
        txtTravel.leftView = imgV2
        txtTravel.leftViewMode = .always
        let imgV3 = UIImageView(image: UIImage(named: "dropdown")!)
        imgV3.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV3.contentMode = .center
        txtTravel.rightView = imgV3
        txtTravel.rightViewMode = .always
        
        let keyboardDoneButtonShow1 = UIToolbar(frame: CGRect(x: 0, y: 0,  width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        keyboardDoneButtonShow1.barStyle = UIBarStyle .blackTranslucent
        let doneButton1 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace1 = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton1 = [flexSpace1,doneButton1]
        keyboardDoneButtonShow1.setItems(toolbarButton1, animated: false)
        txtTravel.inputAccessoryView = keyboardDoneButtonShow1
        dateView.addSubview(txtTravel)
        
        let lblLine1 = UILabel()
        lblLine1.backgroundColor = Colors.GENERAL_BORDER_COLOR
        lblLine1.translatesAutoresizingMaskIntoConstraints = false
        dateView.addSubview(lblLine1)
        
        dateView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[date(20)][line1(0.5)]|", options: [], metrics: nil, views: ["date":txtDate, "line1":lblLine1]))
        dateView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[date][iline(1)][time(==date)][iline1(1)][travel(==date)]|", options: [], metrics: nil, views: ["date":txtDate, "time":txtTime, "travel":txtTravel, "iline":imgLine, "iline1":imgLine1]))
        dateView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[line1]|", options: [], metrics: nil, views: ["line1":lblLine1]))
        
        dateView.addConstraint(NSLayoutConstraint(item: txtDate, attribute: .centerY, relatedBy: .equal, toItem: imgLine, attribute: .centerY, multiplier: 1, constant: 0))
        dateView.addConstraint(NSLayoutConstraint(item: imgLine, attribute: .centerY, relatedBy: .equal, toItem: txtTime, attribute: .centerY, multiplier: 1, constant: 0))
        dateView.addConstraint(NSLayoutConstraint(item: txtTime, attribute: .centerY, relatedBy: .equal, toItem: imgLine1, attribute: .centerY, multiplier: 1, constant: 0))
        dateView.addConstraint(NSLayoutConstraint(item: imgLine1, attribute: .centerY, relatedBy: .equal, toItem: txtTravel, attribute: .centerY, multiplier: 1, constant: 0))
        
        txtDate.text = prettyDateStringFromDate(Date(), toFormat: "dd MMM")
        txtTime.text = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "hh:mm a")
        txtTravel.text = "Any"
        
        vehView.translatesAutoresizingMaskIntoConstraints = false
        vehView.backgroundColor = Colors.WHITE_COLOR
        view.addSubview(vehView)
        
        iconVeh.translatesAutoresizingMaskIntoConstraints = false
        iconVeh.isHidden = true
        vehView.addSubview(iconVeh)
        
        lblVehModel.textColor = Colors.GRAY_COLOR
        lblVehModel.font = boldFontWithSize(13)
        lblVehModel.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureVeh = UITapGestureRecognizer(target: self, action: #selector(selectVehicle))
        lblVehModel.isUserInteractionEnabled = true
        lblVehModel.addGestureRecognizer(tapGestureVeh)
        vehView.addSubview(lblVehModel)
        
        imgLine2.image = UIImage(named: "gray_separator")
        imgLine2.translatesAutoresizingMaskIntoConstraints = false
        imgLine2.isHidden = true
        vehView.addSubview(imgLine2)
        
        iconSeat.image = UIImage(named: "seat")
        iconSeat.translatesAutoresizingMaskIntoConstraints = false
        iconSeat.isHidden = true
        vehView.addSubview(iconSeat)
        
        lblSeatLeft.textColor = Colors.GRAY_COLOR
        lblSeatLeft.font = boldFontWithSize(13)
        lblSeatLeft.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureSeatLeft = UITapGestureRecognizer(target: self, action: #selector(selectCost))
        lblSeatLeft.isUserInteractionEnabled = true
        lblSeatLeft.addGestureRecognizer(tapGestureSeatLeft)
        vehView.addSubview(lblSeatLeft)
        
        imgLine3.image = UIImage(named: "gray_separator")
        imgLine3.translatesAutoresizingMaskIntoConstraints = false
        imgLine3.isHidden = true
        vehView.addSubview(imgLine3)
        
        iconCost.image = UIImage(named: "fare_green-1")
        iconCost.translatesAutoresizingMaskIntoConstraints = false
        iconCost.isHidden = true
        vehView.addSubview(iconCost)
        
        lblCost.textColor = Colors.GRAY_COLOR
        lblCost.font = boldFontWithSize(13)
        lblCost.translatesAutoresizingMaskIntoConstraints = false
        let tapGestureCost = UITapGestureRecognizer(target: self, action: #selector(selectCost))
        lblCost.isUserInteractionEnabled = true
        lblCost.addGestureRecognizer(tapGestureCost)
        vehView.addSubview(lblCost)
        
        vehView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[model(20)]|", options: [], metrics: nil, views: ["model":lblVehModel]))
        vehView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[imodel(15)]-5-[model][iline2(1)]-10-[iseat(15)]-5-[seat(==model)][iline3(1)]-10-[icost(15)]-5-[cost(==model)]|", options: [], metrics: nil, views: ["imodel":iconVeh, "model":lblVehModel, "iseat":iconSeat, "seat":lblSeatLeft, "icost":iconCost, "cost":lblCost, "iline2":imgLine2, "iline3":imgLine3]))
        
        vehView.addConstraint(NSLayoutConstraint(item: iconVeh, attribute: .centerY, relatedBy: .equal, toItem: lblVehModel, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: lblVehModel, attribute: .centerY, relatedBy: .equal, toItem: imgLine2, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: imgLine2, attribute: .centerY, relatedBy: .equal, toItem: iconSeat, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: iconSeat, attribute: .centerY, relatedBy: .equal, toItem: lblSeatLeft, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: lblSeatLeft, attribute: .centerY, relatedBy: .equal, toItem: imgLine3, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: imgLine3, attribute: .centerY, relatedBy: .equal, toItem: iconCost, attribute: .centerY, multiplier: 1, constant: 0))
        vehView.addConstraint(NSLayoutConstraint(item: iconCost, attribute: .centerY, relatedBy: .equal, toItem: lblCost, attribute: .centerY, multiplier: 1, constant: 0))
        
        btnAddVehicle.translatesAutoresizingMaskIntoConstraints = false
        btnAddVehicle.setTitle(" REGISTER A NEW VEHICLE", for: UIControlState())
        btnAddVehicle.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btnAddVehicle.setImage(UIImage(named: "addV"), for: UIControlState())
        btnAddVehicle.contentHorizontalAlignment = .left
        btnAddVehicle.titleLabel?.font = boldFontWithSize(11)
        btnAddVehicle.addTarget(self, action: #selector(addNewVehicle), for: .touchDown)
        btnAddVehicle.isHidden = true
        vehView.addSubview(btnAddVehicle)
        
        vehView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[addVehBtn(20)]|", options: [], metrics: nil, views: ["addVehBtn":btnAddVehicle]))
        vehView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[addVehBtn]|", options: [], metrics: nil, views: ["addVehBtn":btnAddVehicle]))
        
        needRideObj.rideDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        needRideObj.rideTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        needRideObj.poolType = "A"
        
        offerVehicleObj.rideDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        offerVehicleObj.rideTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        offerVehicleObj.poolType = "A"
        
        searchReqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        searchReqObj.reqTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        searchReqObj.poolType = "A"
        
        locationButton.setTitle("ADD RIDE", for: UIControlState())
        locationButton.backgroundColor = Colors.GREEN_COLOR
        locationButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        locationButton.titleLabel?.font = boldFontWithSize(16.0)
        locationButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(locationButton)
        locationButton.addTarget(self, action: #selector(createRide), for: UIControlEvents.touchUpInside)

        let dict = ["date":dateView, "veh":vehView, "btn":locationButton]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[date(30)][veh(30)][btn(40)]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[date]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[veh]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: dict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":locationButton]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100)
        return sub

        
        
    }
    
    func getCurrentLocation () {
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        
        if CLLocationManager.locationServicesEnabled() == true {
            if CLLocationManager.authorizationStatus() == .denied {
                //                AlertController.showAlertForMessage("You have denied access to your location, please enable it in settings.")
                callLocationServiceEnablePopUp()
                return
            }
        }
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else {
            print("location service not enabled")
            lblSource.text = UserInfo.sharedInstance.homeAddress
            needRideObj.fromLocation = lblSource.text
            needRideObj.fromLat = UserInfo.sharedInstance.homeLatValue
            needRideObj.fromLong = UserInfo.sharedInstance.homeLongValue
            
            offerVehicleObj.fromLocation = lblSource.text
            offerVehicleObj.fromLat = UserInfo.sharedInstance.homeLatValue
            offerVehicleObj.fromLong = UserInfo.sharedInstance.homeLongValue 
            
            searchReqObj.startLoc = lblSource.text
            searchReqObj.fromLat = UserInfo.sharedInstance.homeLatValue
            searchReqObj.fromLong = UserInfo.sharedInstance.homeLongValue
            
            //allRidesMarker()
        }
    }
    
    func callLocationServiceEnablePopUp() -> Void
    {
        switch CLLocationManager.authorizationStatus()
        {
        case .authorizedWhenInUse, .restricted, .denied:
            let alertController = UIAlertController(
                title: "Background Location Access Disabled",
                message: "In order to be notified, please open this app's settings and set location access to 'Always'.",
                preferredStyle: .alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                }
            }
            alertController.addAction(openAction)
            self.present(alertController, animated: true, completion: nil)
        default:
            break
        }
    }
    
    // MARK:- CLLocationManager Delegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.locationCoord = manager.location!.coordinate
        self.moveMapToCoord(self.locationCoord)
        reverseGeocodeCoordinate(self.locationCoord)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error::\(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse {
            self.mapView.isMyLocationEnabled = true
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.distanceFilter = 100
            locationManager.startUpdatingLocation()
        } else if status == .denied {
            // AlertController.showAlertForMessage("To enable access to your location later, please go to settings.")
            callLocationServiceEnablePopUp()
        }
    }
    
    // MARK:- Google Map Delegates
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if infoWindow != nil {
            hideInfoWindow()
        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //self.userMovedMap = self.userMovedMap ? self.userMovedMap : gesture
        self.userMovedMap = false
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if self.userMovedMap == true {
            self.userMovedMap = false
            self.locationCoord = position.target
            self.moveMapToCoord(self.locationCoord)
            reverseGeocodeCoordinate(self.locationCoord)
        }
    }
    
    func moveMapToCoord(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        self.locationCoord = coordinate
        let camera = self.mapView.camera
        let cameraPosition = GMSCameraPosition.camera(withTarget: coord, zoom: camera.zoom)
        self.mapView.animate(to: cameraPosition)
    }
    
    // MARK:- Reverse Geocode
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D?) {
        guard let coord = coordinate else {
            return
        }
        let geocoder = GMSGeocoder()
        self.lblSource.lock()
        geocoder.reverseGeocodeCoordinate(coord) { [weak self] response , error in
            if let address = response?.firstResult() {
                let lines = address.lines as [String]!
                self?.lblSource.text = lines?.joined(separator: "")
                self?.needRideObj.fromLocation = self?.lblSource.text
                self?.needRideObj.fromLat = String(address.coordinate.latitude)
                self?.needRideObj.fromLong = String(address.coordinate.longitude)
                
                self?.offerVehicleObj.fromLocation = self?.lblSource.text
                self?.offerVehicleObj.fromLat = String(address.coordinate.latitude)
                self?.offerVehicleObj.fromLong = String(address.coordinate.longitude)
                
                self?.searchReqObj.startLoc = self?.lblSource.text
                self?.searchReqObj.fromLat = String(address.coordinate.latitude)
                self?.searchReqObj.fromLong = String(address.coordinate.longitude)
                if let text = self?.lblSource.text {
                    self?.locationName = text
                }
                self?.lblSource.unlock()
            }
        }
    }
    
    func trafficBtnClicked() {
        if infoWindow != nil {
            hideInfoWindow()
        }
        if(self.mapView.isTrafficEnabled == true) {
            self.mapView.isTrafficEnabled = false
        } else {
            self.mapView.isTrafficEnabled = true
        }
    }
    
    func rideTypeChanged() {
        if(segmentRides.selectedSegmentIndex == 0) {
            vehView.isHidden = true
        } else {
            vehView.isHidden = false
        }
        if((self.needRideObj.fromLat != nil && self.needRideObj.fromLat != "" && self.needRideObj.toLat != nil && self.needRideObj.toLat != "") || (self.offerVehicleObj.fromLat != nil && self.offerVehicleObj.fromLat != "" && self.offerVehicleObj.toLat != nil && self.offerVehicleObj.toLat != "")) {
            self.searchRide()
        } else {
            allRidesMarker()
        }
    }
    
    func addOverlayToMapView(_ sourceLat: String,sourceLang: String,distinationLat: String,distinationLang: String ){
        self.mapView.clear()
        
        showIndicator("Loading Map path...")
        var sourceStr = ""
        var destinationStr = ""
        //        if(updateData?.officeRideType == "C") {
        //            destinationStr = "\(sourceLat),\(sourceLang)"
        //            sourceStr = "\(distinationLat), \(distinationLang)"
        //        } else {
        sourceStr = "\(sourceLat),\(sourceLang)"
        destinationStr = "\(distinationLat), \(distinationLang)"
        //}
        
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        //print(directionURL," directionURL --------- ")
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                //  print(response)
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    self.addmarkers(legs)
                    self.addPolyLineWithEncodedStringInMap(routes as NSArray)
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }
    
    // MARK:- Google Map Direction Api Call
    var originCoordinate: CLLocationCoordinate2D!
    var destinationCoordinate: CLLocationCoordinate2D!
    var originMarker: GMSMarker!
    var destinationMarker: GMSMarker!
    // MARK:- Add markers
    func addmarkers(_ legs : NSArray) {
        let legsStartLocDict = legs[0] as! NSDictionary
        let startLocationDictionary = legsStartLocDict["start_location"] as! NSDictionary
        self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
        let legsEndLocDict = legs[legs.count - 1] as! NSDictionary
        let endLocationDictionary = legsEndLocDict["end_location"] as! NSDictionary
        self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
        
        //self.mapRideView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
        
        let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
        let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
        let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
        let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
        self.mapView.camera = camera
        
        self.originMarker = GMSMarker(position: self.originCoordinate)
        self.originMarker.map = self.mapView
        self.originMarker.tracksInfoWindowChanges = true
        self.originMarker.title = lblSource.text
        self.originMarker.accessibilityLabel = "1212"
        self.originMarker.isTappable = false
        
        self.destinationMarker = GMSMarker(position: self.destinationCoordinate)
        self.destinationMarker.map = self.mapView
        self.destinationMarker.tracksInfoWindowChanges = true
        self.destinationMarker.accessibilityLabel = "1313"
        self.destinationMarker.isTappable = false
        self.destinationMarker.title = lblDest.text
        
        if(segmentRides.selectedSegmentIndex == 1) {
            self.originMarker.icon = UIImage(named:"marker_ride_orange")
            self.destinationMarker.icon = UIImage(named:"marker_ride_green")
        } else {
            self.originMarker.icon = UIImage(named:"marker_profile_orange")
            self.destinationMarker.icon = UIImage(named:"marker_profile_green")
        }
    }
    
    func addPolyLineWithEncodedStringInMap(_ routes: NSArray) {
        self.polylineMutArray? .removeAllObjects()
        self.viaMutArray? .removeAllObjects()
        for index in 0..<routes.count{
            let routesDicts = routes[index] as! NSDictionary
            let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
            let points = overview_polyline ["points"] as! NSString
            let summary = routesDicts ["summary"] as! NSString
            let path = GMSMutablePath(fromEncodedPath: points as String)
            let routePolylineObje = GMSPolyline(path: path)
            
            if index == 0 {
                self.routePolylineIndex0 = routePolylineObje
                lblVia.text = "\("VIA-") \(summary as String)"
                needRideObj.via = routesDicts ["summary"] as? String
                needRideObj.waypoints = points as String
                
                offerVehicleObj.via = routesDicts ["summary"] as? String
                offerVehicleObj.waypoints = points as String
            }else if index == 1{
                self.routePolylineIndex1 = routePolylineObje
            }else{
                self.routePolylineIndex2 = routePolylineObje
            }
            
            if index  == 0 {
                routePolylineObje.zIndex = 100
                routePolylineObje.strokeColor = Colors.POLILINE_GREEN
            }else{
                routePolylineObje.strokeColor = Colors.POLILINE_GRAY
            }
            
            routePolylineObje.strokeWidth = 5.0
            routePolylineObje.isTappable = true
            routePolylineObje.title = String(index)
            self.polylineMutArray? .add(points)
            self.viaMutArray? .add(summary)
            routePolylineObje.map = self.mapView
            
            let bounds = GMSCoordinateBounds(path: path!)
            mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 80))
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap overlay: GMSOverlay) {
        if infoWindow != nil {
            hideInfoWindow()
        }
        let selectedPolyline : GMSPolyline = overlay as! GMSPolyline
        let selectedIndex = Int(selectedPolyline.title!)
        let polylinePath  = self.polylineMutArray?[selectedIndex!] as! String
        let viaStr  = self.viaMutArray?[selectedIndex!] as! String
        lblVia.text = "\("VIA-") \(viaStr)"
        needRideObj.via = viaStr
        needRideObj.waypoints = polylinePath
        offerVehicleObj.via = viaStr
        offerVehicleObj.waypoints = polylinePath
        for index in 0..<(self.polylineMutArray?.count)! {
            if index == 0 {
                self.routePolylineIndex0.strokeColor = Colors.POLILINE_GRAY
            }
            if index == 1 {
                self.routePolylineIndex1.strokeColor = Colors.POLILINE_GRAY
            }
            if index == 2  {
                self.routePolylineIndex2.strokeColor = Colors.POLILINE_GRAY
            }
        }
        selectedPolyline.zIndex = 100
        selectedPolyline.strokeColor = Colors.POLILINE_GREEN
    }
    
    func mapForFrom()
    {
        if infoWindow != nil {
            hideInfoWindow()
        }
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            if(triggerFrom == "place") {
                vc.isFromWP = true
            }
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblSource.text = loc["location"]
                    self.searchReqObj.startLoc = self.lblSource.text
                    self.searchReqObj.fromLat = loc["lat"]
                    self.searchReqObj.fromLong = loc["long"]
                    
                    self.needRideObj.fromLocation = self.lblSource.text
                    self.needRideObj.fromLat = loc["lat"]
                    self.needRideObj.fromLong = loc["long"]
                    if((self.needRideObj.fromLat != nil && self.needRideObj.fromLat != "" && self.needRideObj.toLat != nil && self.needRideObj.toLat != "") || (self.offerVehicleObj.fromLat != nil && self.offerVehicleObj.fromLat != "" && self.offerVehicleObj.toLat != nil && self.offerVehicleObj.toLat != "")) {
                        self.searchRide()
                    }
                    self.offerVehicleObj.fromLocation = self.lblSource.text
                    self.offerVehicleObj.fromLat = loc["lat"]
                    self.offerVehicleObj.fromLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func mapForTo()
    {
        if infoWindow != nil {
            hideInfoWindow()
        }
        if !isServerReachable() {
            AlertController.showAlertFor("Oops!", message: INTERNET_MESSAGE_STRING)
        } else {
            let vc = LocationViewController()
            vc.hidesBottomBarWhenPushed = true
            vc.edgesForExtendedLayout = UIRectEdge()
            if(triggerFrom == "place") {
                vc.isFromWP = true
            }
            vc.onSelectionBlock = { (location) -> Void in
                if let loc = location as? [String: String]
                {
                    self.lblDest.text = loc["location"]
                    self.searchReqObj.endLoc = self.lblDest.text
                    self.searchReqObj.toLat = loc["lat"]
                    self.searchReqObj.toLong = loc["long"]
                    
                    self.needRideObj.toLocation = self.lblDest.text
                    self.needRideObj.toLat = loc["lat"]
                    self.needRideObj.toLong = loc["long"]
                    if((self.needRideObj.fromLat != nil && self.needRideObj.fromLat != "" && self.needRideObj.toLat != nil && self.needRideObj.toLat != "") || (self.offerVehicleObj.fromLat != nil && self.offerVehicleObj.fromLat != "" && self.offerVehicleObj.toLat != nil && self.offerVehicleObj.toLat != "")) {
                        self.searchRide()
                    }
                    self.offerVehicleObj.toLocation = self.lblDest.text
                    self.offerVehicleObj.toLat = loc["lat"]
                    self.offerVehicleObj.toLong = loc["long"]
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func datePicked()
    {
        
    }
    
    func timePicked()
    {
        
    }
    
    func donePressed()
    {
        if(selectedTxtField == txtDate){
            needRideObj.rideDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            offerVehicleObj.rideDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            searchReqObj.reqDate = prettyDateStringFromDate(datePicker.date, toFormat: "dd-MMM-yyyy")
            selectedTxtField?.text = prettyDateStringFromDate(datePicker.date, toFormat: "dd MMM")
        }
        else if(selectedTxtField == txtTime){
            needRideObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            offerVehicleObj.rideTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            searchReqObj.reqTime = prettyDateStringFromDate(timePicker.date, toFormat: "HH:mm")
            selectedTxtField?.text = prettyDateStringFromDate(timePicker.date, toFormat: "hh:mm a")
        } else if(selectedTxtField == txtTravel) {
            txtTravel.text = valueSelected
            if (valueSelected == "Only Male"){
                needRideObj.poolType  = "M"
                offerVehicleObj.poolType  = "M"
                searchReqObj.poolType = "M"
            }else if (valueSelected == "Only Female"){
                needRideObj.poolType  = "F"
                offerVehicleObj.poolType  = "F"
                searchReqObj.poolType = "F"
            }else{
                txtTravel.text = "Any"
                needRideObj.poolType  = "A"
                offerVehicleObj.poolType  = "A"
                searchReqObj.poolType = "A"
            }
        }
        selectedTxtField?.resignFirstResponder()
        if((self.needRideObj.fromLat != nil && self.needRideObj.fromLat != "" && self.needRideObj.toLat != nil && self.needRideObj.toLat != "") || (self.offerVehicleObj.fromLat != nil && self.offerVehicleObj.fromLat != "" && self.offerVehicleObj.toLat != nil && self.offerVehicleObj.toLat != "")) {
            self.searchRide()
        }
    }
    
    func selectVehicle () {
        if infoWindow != nil {
            hideInfoWindow()
        }
        let vc = AddVehicleViewController()
        vc.isFromWp = "1"
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func selectCost () {
        if infoWindow != nil {
            hideInfoWindow()
        }
        let viewHolder = AlertContentViewHolder()
        viewHolder.heightConstraintValue = 95
        
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        let lblSeat = UILabel()
        lblSeat.text = "Seat : "
        lblSeat.textColor = Colors.GRAY_COLOR
        lblSeat.font = normalFontWithSize(12)
        lblSeat.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblSeat)
        
        stepperSeats.setBorderColor(Colors.GRAY_COLOR)
        stepperSeats.setBorderWidth(0.5)
        stepperSeats.countLabel.layer.borderWidth = 0.5
        stepperSeats.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperSeats.valueChangedCallback = { (stepper, count) -> Void in
            self.stepperSeats.countLabel.text = "\(count)"
        }
        stepperSeats.setLabelTextColor(Colors.GRAY_COLOR)
        stepperSeats.value = 1
        stepperSeats.minimum = 1
        stepperSeats.value = Int32(SeatValue)!
        stepperSeats.countLabel.text = "\(SeatValue)"
        stepperSeats.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Seat selection is exceeding its maximum seat available.")
            }
        }
        
        stepperSeats.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperSeats)
        
        let lblCostTitle = UILabel()
        lblCostTitle.text = "Cost : "
        lblCostTitle.textColor = Colors.GRAY_COLOR
        lblCostTitle.font = normalFontWithSize(12)
        lblCostTitle.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(lblCostTitle)
        
        stepperCost.setBorderColor(Colors.GRAY_COLOR)
        stepperCost.setBorderWidth(0.5)
        stepperCost.countLabel.layer.borderWidth = 0.5
        stepperCost.setButtonTextColor(Colors.GRAY_COLOR, for: UIControlState())
        stepperCost.valueChangedCallback = { (stepper, count) -> Void in
            self.stepperCost.countLabel.text = "\(count)"
        }
        stepperCost.setLabelTextColor(Colors.GRAY_COLOR)
        stepperCost.value = Int32(costValue)!
        //stepperCost.minimum = 1
        
        stepperCost.incrementCallback = { (stepper, newValue) -> Void in
            if(newValue == stepper?.maximum){
                AlertController.showToastForError("Sorry, you can not select more than " + String(describing: stepper?.maximum)+".")
            }
        }
        
        stepperCost.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stepperCost)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[lblSeat(40)]-10-[lblCost(40)]|", options: [], metrics: nil, views: ["lblSeat":lblSeat, "lblCost":lblCostTitle]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblSeat]-10-[stepperSeat(150)]-10-|", options: [], metrics: nil, views: ["lblSeat":lblSeat, "stepperSeat":stepperSeats]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblCost]-10-[stepperCost(150)]-10-|", options: [], metrics: nil, views: ["lblCost":lblCostTitle, "stepperCost":stepperCost]))
        
        view.addConstraint(NSLayoutConstraint(item: lblSeat, attribute: .centerY, relatedBy: .equal, toItem: stepperSeats, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stepperSeats, attribute: .height, relatedBy: .equal, toItem: lblSeat, attribute: .height, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: lblCostTitle, attribute: .centerY, relatedBy: .equal, toItem: stepperCost, attribute: .centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: stepperCost, attribute: .height, relatedBy: .equal, toItem: lblCostTitle, attribute: .height, multiplier: 1, constant: 0))
        viewHolder.view = view
        
        AlertController.showAlertFor("Fare", message: "", contentView: viewHolder, okButtonTitle: "OK", willHaveAutoDismiss: false, okAction: {
            self.dismiss(animated: true, completion:{
                self.costValue = self.stepperCost.countLabel.text!
                self.lblCost.text = "\(self.costValue) \("/ Km")"
                self.needRideObj.cost = self.costValue
                self.offerVehicleObj.cost = self.costValue
                
                self.SeatValue = self.stepperSeats.countLabel.text!
                self.lblSeatLeft.text = self.SeatValue
                self.offerVehicleObj.veh_capacity = self.SeatValue
            })
        })
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTxtField = nil
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        valueSelected = pickerData[row] as String
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let pickerLabel = UILabel()
        let titleData = pickerData[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: FONT_NAME.BOLD_FONT, size:  14.0)!])
        pickerLabel.textAlignment = .center
        pickerLabel.attributedText = myTitle
        return pickerLabel
    }
    
    func addNewVehicle() {
        if infoWindow != nil {
            hideInfoWindow()
        }
        let vc = RegisterNewVehicleController()
        vc.isFromWp = "1"
        vc.noVehicle = true
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Get Vehicle Default Details Service Call.
    
    func getDefaultVehicle()
    {
        RideRequestor().getDefaultVehicle({ (success, object) in
            if (success) {
                //self.vehicleDefaultDetails = object as! VehicleDefaultResponse
                self.setDefaultVehicle()
            } else {
                self.btnAddVehicle.isHidden = false
                self.iconVeh.isHidden = true
                self.iconSeat.isHidden = true
                self.iconCost.isHidden = true
                self.imgLine2.isHidden = true
                self.imgLine3.isHidden = true
                self.lblVehModel.isHidden = true
                self.lblSeatLeft.isHidden = true
                self.lblCost.isHidden = true
            }
        }) { (error) in
        }
    }
    
    func setDefaultVehicle()
    {
        if UserInfo.sharedInstance.vehicleType != ""{
            btnAddVehicle.isHidden = true
            iconVeh.isHidden = false
            iconSeat.isHidden = false
            iconCost.isHidden = false
            imgLine2.isHidden = false
            imgLine3.isHidden = false
            lblVehModel.isHidden = false
            lblSeatLeft.isHidden = false
            lblCost.isHidden = false
            if(UserInfo.sharedInstance.vehicleType.lowercased() == "bike") {
                self.iconVeh.image = UIImage(named: "veh_bike")
            } else if(UserInfo.sharedInstance.vehicleType.lowercased() == "auto") {
                self.iconVeh.image = UIImage(named: "auto")
            } else if(UserInfo.sharedInstance.vehicleType.lowercased() == "car") {
                self.iconVeh.image = UIImage(named: "veh_car")
            } else if(UserInfo.sharedInstance.vehicleType.lowercased() == "cab") {
                self.iconVeh.image = UIImage(named: "cab")
            } else if(UserInfo.sharedInstance.vehicleType.lowercased() == "suv") {
                self.iconVeh.image = UIImage(named: "suv")
            } else if(UserInfo.sharedInstance.vehicleType.lowercased() == "tempo") {
                self.iconVeh.image = UIImage(named: "tempo")
            } else if(UserInfo.sharedInstance.vehicleType.lowercased() == "bus") {
                self.iconVeh.image = UIImage(named: "bus")
            }
            self.lblVehModel.text = UserInfo.sharedInstance.vehicleType.capitalized
            let seatLeft = Int((UserInfo.sharedInstance.vehicleSeatCapacity))!-1
            self.SeatValue = String(seatLeft)
            self.lblSeatLeft.text = self.SeatValue
            var cost = "0"
            var maxAmt = 0
            if (self.lblVehModel.text?.lowercased() == "bike"){
                cost = self.vehicleConfig.bike!
                maxAmt = Int(self.vehicleConfig.bike!)!
            }else if (self.lblVehModel.text?.lowercased() == "car"){
                cost = self.vehicleConfig.car!
                maxAmt = Int(self.vehicleConfig.car!)!
            }else if (self.lblVehModel.text?.lowercased() == "suv"){
                cost = self.vehicleConfig.suv!
                maxAmt = Int(self.vehicleConfig.suv!)!
            }else{
                cost = "0"
                maxAmt = 99999
            }
            self.costValue = cost
            self.lblCost.text = "\(cost) \("/ Km")"
            self.stepperCost.maximum = Int32(maxAmt)
            self.stepperCost.value = Int32(cost)!
            self.stepperCost.countLabel.text = "\(cost)"
            self.stepperSeats.maximum = Int32(self.SeatValue)!
            //self.stepperSeats.value = Int32((self.updateData?.seatsLeft)!)!
            //self.stepperSeats.countLabel.text = "\(self.updateData?.seatsLeft)"
            self.offerVehicleObj.veh_capacity = self.SeatValue
            self.offerVehicleObj.vehregno = UserInfo.sharedInstance.vehicleRegNo
            self.offerVehicleObj.cost = cost
        }
    }
    
    //MARK:- Get Vehicle Details Service Call.
    func getConfigData()
    {
        RideRequestor().getVehicleConfig("ride", groupID: "", success: { (success, object) in
            self.vehicleConfig = object as! VehicleConfigResponse
            var cost = "0"
            var maxAmt = 0
            if (self.lblVehModel.text?.lowercased() == "bike"){
                cost = self.vehicleConfig.bike!
                maxAmt = Int(self.vehicleConfig.bike!)!
            }else if (self.lblVehModel.text?.lowercased() == "car"){
                cost = self.vehicleConfig.car!
                maxAmt = Int(self.vehicleConfig.car!)!
            }else if (self.lblVehModel.text?.lowercased() == "suv"){
                cost = self.vehicleConfig.suv!
                maxAmt = Int(self.vehicleConfig.suv!)!
            }else{
                cost = "0"
                maxAmt = 99999
            }
            //self.lblCost.text = "\(cost) \("/ Km")"
            self.stepperCost.maximum = Int32(maxAmt)
            self.stepperCost.value = Int32(cost)!
            self.stepperCost.countLabel.text = "\(cost)"
            //self.stepperSeats.maximum = Int32((self.updateData?.seatsLeft)!)!
            //self.stepperSeats.value = Int32((self.updateData?.seatsLeft)!)!
            self.getDefaultVehicle()
            if(self.haveData != "" && self.haveData == "yesData") {
                self.setDefaultVehicle()
            } else {
                self.btnAddVehicle.isHidden = false
            }
            
        }) { (error) in
        }
    }
    
    func createRide()
    {
        if(segmentRides.selectedSegmentIndex == 0) {
            self.needRideObj.userId = UserInfo.sharedInstance.userID
            if((self.needRideObj.fromLocation != nil) && (self.needRideObj.toLocation != nil))
            {
                if(self.needRideObj.fromLocation != self.needRideObj.toLocation){
                    if((self.needRideObj.rideDate != nil) && (self.needRideObj.rideTime != nil)){
                        
                        CommonRequestor().getDistanceDuration(self.needRideObj.fromLocation!, destination: self.needRideObj.toLocation!, success: { (success, object) in
                            self.needRideObj.duration = ""
                            self.needRideObj.distance = ""
                            if let data = object as? NSDictionary{
                                let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                                let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                                if((elements["status"] as! String) == "OK")
                                {
                                    let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                    let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                    self.needRideObj.duration = duration as! String
                                    self.needRideObj.distance = distance as! String
                                }
                            }
                            self.createNeedRideServcCall()
                            }, failure: { (error) in
                                self.createNeedRideServcCall()
                        })
                    }
                    else{
                        AlertController.showAlertFor("Need a Ride", message: "Please enter date and time for the Ride.")
                    }
                }
                else{
                    AlertController.showAlertFor("Need a Ride", message: "Please choose different locations for source and destination.")
                }
            }
            else{
                AlertController.showAlertFor("Need a Ride", message: "Please choose location in 'From' and 'To' field from Google Places.")
            }
        } else {
            self.offerVehicleObj.userId = UserInfo.sharedInstance.userID
            if((offerVehicleObj.fromLocation != nil) && (offerVehicleObj.toLocation != nil)){
                if(offerVehicleObj.fromLocation != offerVehicleObj.toLocation){
                    if((offerVehicleObj.rideDate != nil) && (offerVehicleObj.rideTime != nil))
                    {
                        if(offerVehicleObj.vehregno != nil && offerVehicleObj.vehregno != ""){
                            CommonRequestor().getDistanceDuration(self.offerVehicleObj.fromLocation!, destination: self.offerVehicleObj.toLocation!, success: { (success, object) in
                                self.offerVehicleObj.duration = ""
                                self.offerVehicleObj.distance = ""
                                if let data = object as? NSDictionary{
                                    let rows = (((data["rows"] as! NSArray)[0]) as! NSDictionary)
                                    let elements = (((rows["elements"] as! NSArray)[0]) as! NSDictionary)
                                    if((elements["status"] as! String) == "OK")
                                    {
                                        let distance = ((elements["distance"] as! NSDictionary)["text"])!
                                        let duration = ((elements["duration"] as! NSDictionary)["text"])!
                                        self.offerVehicleObj.duration = duration as? String
                                        self.offerVehicleObj.distance = distance as? String
                                    }
                                }
                                self.createOfferVehicleServcCall()
                                }, failure: { (error) in
                                    self.createOfferVehicleServcCall()
                            })
                        }
                        else{
                            AlertController.showAlertFor("Offer a Vehicle", message: "Please provide vehicle details.")
                        }
                    }
                    else{
                        AlertController.showAlertFor("Offer a Vehicle", message: "Please enter date and time for the Ride.")
                    }
                }
                else{
                    AlertController.showAlertFor("Offer a Vehicle", message: "Please choose different locations for source and destination.")
                }
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'From' and 'To' field from Google Places.")
            }
            
        }
    }
    
    func createNeedRideServcCall() {
        showIndicator("Creating New Ride..")
        NeedRideRequestor().createNeedaRide(self.needRideObj, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Create Ride", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! NeedRideResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.triggerFrom = "ride"
                            vc.inviteFlag = "wpMap"
                            vc.signupFlow = self.signupFlow
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! NeedRideResponse).code == "173") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! NeedRideResponse).message!)
                }
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func createOfferVehicleServcCall()
    {
        showIndicator("Creating Offer a vehicle..")
        OfferVehicleRequestor().createOfferVehicle(offerVehicleObj, success: { (success, object) in
            hideIndicator()
            if(success == true){
                AlertController.showAlertFor("Offer a Vehicle", message: "Your ride has been created successfully.", okButtonTitle: "Ok", okAction: {
                    let poolID = ((object as! OfferVehicleResponse).data!.first)!.poolID!
                    showIndicator("Fetching Ride Details")
                    RideRequestor().getRideDetails(poolID, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                            let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                            //let vc = RideDetailViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            vc.data = object as? RideDetail
                            vc.triggerFrom = "ride"
                            vc.inviteFlag = "wpMap"
                            vc.signupFlow = self.signupFlow
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                })
            }
            else{
                if((object as! OfferVehicleResponse).code == "173") {
                    AlertController.showToastForError("Sorry, You are already travelling on this time so you will not able to create new ride on the same time.")
                } else {
                    AlertController.showToastForError((object as! OfferVehicleResponse).message!)
                }
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func searchRide() {
        if infoWindow != nil {
            hideInfoWindow()
        }
        searchReqObj.userId = UserInfo.sharedInstance.userID
        // searchReqObj.poolType = self.btnAny.selected == true ? "A" : (UserInfo.sharedInstance.gender == "M" ? "M" : "F")
        searchReqObj.searchType = "recurrent"
        searchReqObj.ridetype = ""
        var selectedDates = [String]()
        selectedDates.append(searchReqObj.reqDate!)
        if(selectedDates.count > 0){
            searchReqObj.reqCurrentDate = selectedDates.joined(separator: ",")
        }
        if((self.searchReqObj.startLoc != nil) && (self.searchReqObj.endLoc != nil))
        {
            if(self.searchReqObj.startLoc != self.searchReqObj.endLoc){
                if((self.searchReqObj.reqDate != nil) && (self.searchReqObj.reqTime != nil)){
                    showIndicator("Searching for similair rides..")
                    RideRequestor().searchRides(searchReqObj, success: { (success, object) in
                        hideIndicator()
                        if(success){
                            self.showAllRides = false
                            self.tempSearchRides = true
                            self.searchRides = (object as! AllRidesResponse).pools!
                            self.mapView.clear()
                            if(self.needRideObj.fromLat != nil && self.needRideObj.fromLat != "" && self.needRideObj.toLat != nil && self.needRideObj.toLat != "") {
                                self.addOverlayToMapView(self.needRideObj.fromLat!,sourceLang: self.needRideObj.fromLong!,distinationLat: (self.needRideObj.toLat)!,distinationLang: (self.needRideObj.toLong)!)
                            }
                            if(self.offerVehicleObj.fromLat != nil && self.offerVehicleObj.fromLat != "" && self.offerVehicleObj.toLat != nil && self.offerVehicleObj.toLat != "") {
                                self.addOverlayToMapView(self.offerVehicleObj.fromLat!,sourceLang: self.offerVehicleObj.fromLong!,distinationLat: (self.offerVehicleObj.toLat)!,distinationLang: (self.offerVehicleObj.toLong)!)
                            }
                            if(self.searchRides?.count > 0){
                                self.getISRideType(self.searchRides)
                                self.updateMarker()
                            } else {
                                return
                            }
                        }
                    }) { (error) in
                        hideIndicator()
                    }
                }
                else{
                    AlertController.showAlertFor("Search Ride", message: "Please enter date and time for the Ride.")
                }
            }
            else{
                AlertController.showAlertFor("Search Ride", message: "Please choose different locations for source and destination.")
            }
        }
        else{
            AlertController.showAlertFor("Search Ride", message: "Please choose location in 'From' and 'To' field from Google Places.")
        }
        
    }
    
    func getISRideType(_ ridesData: [Ride]?) {
        var rowsCnt = 0
        if let list = ridesData{
            rowsCnt = list.count
        }
        self.nRideMutArray?.removeAllObjects()
        self.oRideMutArray?.removeAllObjects()
        var rideDataObject: Ride!
        for index in 0..<rowsCnt{
            rideDataObject  = ridesData![index]
            if rideDataObject.poolRideType == "N" {
                self.nRideMutArray!.add(rideDataObject)
            }else{
                self.oRideMutArray!.add(rideDataObject)
            }
        }
    }
    
    func updateMarker() {
        if(segmentRides.selectedSegmentIndex == 0) {
        if self.oRideMutArray?.count == 0 || oRideMutArray == nil {
            return
        }
        var rowsCnt = 0
        if let list = self.oRideMutArray{
            rowsCnt = list.count
        }
        
        for index in 0..<rowsCnt{
            let ride = self.oRideMutArray![index] as! Ride as Ride!
            let lat = Double(ride!.sourceLat!)! + (Double(index) * 0.000002)
            let long = Double(ride!.sourceLong!)!
            
            let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14.0)
            let marker = GMSMarker()
            marker.position = camera.target
            marker.snippet = ride?.source
            marker.icon = UIImage(named: "marker_ride_green")
            //marker.appearAnimation = GMSMarkerAnimation
            marker.groundAnchor = CGPoint(x: 1, y: 1)
            marker.accessibilityLabel = "\(index)"
            self.mapMarkerMutArray!.add(marker)
            marker.map = self.mapView
        }
        } else {
            if self.nRideMutArray?.count == 0 || nRideMutArray == nil{
                return
            }
            
            var rowsCnt = 0
            if let list = self.nRideMutArray{
                rowsCnt = list.count
            }
            
            if rowsCnt <= 0 {
                return
            }
            
            for index in 0..<rowsCnt{
                let ride = self.nRideMutArray![index] as! Ride as Ride!
                let lat = Double(ride!.sourceLat!)! + (Double(index) * 0.000002)
                let long = Double(ride!.sourceLong!)!
                
                let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 14.0)
                //    mapView.camera = camera
                let marker = GMSMarker()
                marker.title = ride?.sourceID
                marker.position = camera.target
                marker.snippet = ride?.source
                marker.groundAnchor = CGPoint(x: 1, y: 1)
                marker.icon = UIImage(named: "marker_profile_green")
                //marker.groundAnchor = CGPoint(x: 20, y: 20)
                //marker.appearAnimation = GMSMarkerAnimation
                marker.accessibilityLabel = "\(index)"
                self.mapMarkerMutArray!.add(marker)
                marker.map = self.mapView
            }
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        let index:Int! = Int(marker.accessibilityLabel!)
            if infoWindow != nil {
                hideInfoWindow()
            }
            self.originSelectedRideMarker = marker
            infoWindow = Bundle.main.loadNibNamed("RideWindowInfoView", owner: self, options: nil)!.first as! RideWindowInfoView
            infoWindow.frame = CGRect(x: (self.mapView.frame.size.width-340)/2, y: (self.mapView.frame.size.height-80)/2, width: 300, height: 130)
            let rideObj : Ride
            
            if segmentRides.selectedSegmentIndex == 1{
                rideObj = self.nRideMutArray![index] as! Ride
            }else{
                rideObj = self.oRideMutArray![index] as! Ride
            }
            infoWindow.name.text = rideObj.ownerName
            if ((rideObj.poolRideType == "N") || (rideObj.rideType == "N")) {
                
                infoWindow.offerVehicle.text = "Needed Ride on " + (rideObj.dateTime)!
                marker.icon = UIImage(named: "marker_profile_orange")
                infoWindow.seatAvailable.text = ""
                infoWindow.seatLeftTag.isHidden = true
                infoWindow.sepraterLine.isHidden = true
            }else{
                infoWindow.offerVehicle.text = "Offering Vehicle on " + (rideObj.dateTime)!
                
                infoWindow.seatAvailable.text = (rideObj.seatsLeft! == "0") ? "No Seat left" : String(format: "%@", rideObj.seatsLeft!)
                infoWindow.seatLeftTag.isHidden = false
                infoWindow.sepraterLine.isHidden = false
                marker.icon = UIImage(named: "marker_ride_orange")
                
            }
            infoWindow.destinationLbl.text = rideObj.destination
            
            if rideObj.via != nil {
                infoWindow.via.text = "VIA- " + (rideObj.via)!
            } else {
                infoWindow.via.text = ""
            }
            
            infoWindow.address.text = rideObj.source
            infoWindow.markerWindowButton.addTarget(self, action: #selector(markerWindowButtonTapped), for:.touchUpInside)
            infoWindow.markerWindowButton.tag = index
            if(rideObj.ownerProfilePicStatus == "1"){
                if let url = rideObj.ownerProfilePicUrl{
                    infoWindow.profile.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
                }
                else{
                    if let url = rideObj.ownerFBID{
                        infoWindow.profile.kf.setImage(with: URL(string: "https://graph.facebook.com/\(url)/picture?type=large&return_ssl_resources=1"))
                    }
                }
            }
            else{
                infoWindow.profile.image = UIImage(named: "placeholder")
            }
            
            if((rideObj.poolRideType == "N") || (rideObj.rideType == "N")){
                if(rideObj.createdBy == UserInfo.sharedInstance.userID){
                    //  showInvite()
                    infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                    infoWindow.joinButton.isHidden = false
                    infoWindow.offerRideButton.isHidden = true
                    infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                    
                }
                else{
                    if let poolStatus = rideObj.poolStatus{
                        if let requestedStatus = rideObj.requestedStatus{
                            if(poolStatus == "P" && requestedStatus == "P"){
                                //    showCancel()
                                infoWindow.joinButton .setTitle("CANCEL", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(cancelButtonTapped), for:.touchUpInside)
                                
                                
                            }
                            else if(poolStatus == "J"){
                                // showInvite()
                                infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                            }
                            else if((poolStatus == "U")||(poolStatus == "R")||(poolStatus == "C")||(poolStatus == "P")){
                                // showOfferVehicle()
                                //  showJoin()
                                infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = false
                                infoWindow.offerRideButton .setTitle("OFFER A VEHICLE", for: UIControlState())
                                infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                                
                                infoWindow.offerRideButton.addTarget(self, action: #selector(offerRideButtonTapped), for:.touchUpInside)
                                
                            }
                            else if(poolStatus == "F"){
                                // showAccept()
                                // showReject()
                                infoWindow.joinButton .setTitle("ACCEPT", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = false
                                infoWindow.offerRideButton .setTitle("REJECT", for: UIControlState())
                                infoWindow.joinButton.addTarget(self, action: #selector(acceptButtonTapped), for:.touchUpInside)
                                
                                infoWindow.offerRideButton.addTarget(self, action: #selector(rejectButtonTapped), for:.touchUpInside)
                                
                            }
                            else{
                                //Nothing
                                infoWindow.joinButton.isHidden = true
                                infoWindow.offerRideButton.isHidden = true
                            }
                        }
                    }
                    else{
                        //showOfferVehicle()
                        // showJoin()
                        infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                        infoWindow.joinButton.isHidden = false
                        infoWindow.offerRideButton.isHidden = false
                        infoWindow.offerRideButton .setTitle("OFFER A VEHICLE", for: UIControlState())
                        infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                        
                        infoWindow.offerRideButton.addTarget(self, action: #selector(offerRideButtonTapped), for:.touchUpInside)
                    }
                }
            }
            else if((rideObj.poolRideType == "O") || (rideObj.rideType == "O")){
                if(rideObj.createdBy == UserInfo.sharedInstance.userID){
                    //  showInvite()
                    infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                    infoWindow.joinButton.isHidden = false
                    infoWindow.offerRideButton.isHidden = true
                    infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                    
                }
                else{
                    if let poolStatus = rideObj.poolStatus{
                        if let requestedStatus = rideObj.requestedStatus{
                            if(poolStatus == "P" && requestedStatus == "P"){
                                // showCancel()
                                infoWindow.joinButton .setTitle("CANCEL", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(cancelButtonTapped), for:.touchUpInside)
                                
                            }
                            else if(poolStatus == "J"){
                                //  showInvite()
                                infoWindow.joinButton .setTitle("INVITE", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(inviteButtonTapped), for:.touchUpInside)
                                
                            }
                            else if((poolStatus == "U")||(poolStatus == "R")||(poolStatus == "C")||(poolStatus == "P")){
                                // showJoin()
                                infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = true
                                infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                            }
                            else if(poolStatus == "F"){
                                //   showAccept()
                                //   showReject()
                                infoWindow.joinButton .setTitle("ACCEPT", for: UIControlState())
                                infoWindow.joinButton.isHidden = false
                                infoWindow.offerRideButton.isHidden = false
                                infoWindow.offerRideButton .setTitle("REJECT", for: UIControlState())
                                infoWindow.joinButton.addTarget(self, action: #selector(acceptButtonTapped), for:.touchUpInside)
                                
                                infoWindow.offerRideButton.addTarget(self, action: #selector(rejectButtonTapped), for:.touchUpInside)
                            }
                            else{
                                //Nothing
                                infoWindow.joinButton.isHidden = true
                                infoWindow.offerRideButton.isHidden = true
                            }
                        }
                    }
                    else{
                        //  showJoin()
                        infoWindow.joinButton .setTitle("JOIN", for: UIControlState())
                        infoWindow.joinButton.isHidden = false
                        infoWindow.offerRideButton.isHidden = true
                        infoWindow.joinButton.addTarget(self, action: #selector(joinButtonTapped), for:.touchUpInside)
                    }
                }
        }
            infoWindow.center = mapView.center
            self.mapView.addSubview(infoWindow)
            sourceLoc = rideObj.source!
            sourceLat = rideObj.sourceLat!
            sourceLong = rideObj.sourceLong!
            destloc = rideObj.destination!
            destLat = rideObj.destLat!
            destLong = rideObj.destLong!
            poolTaxiBooking = rideObj.poolTaxiBooking!
            poolId = rideObj.poolID!
            rideType = rideObj.poolRideType!
            ownerName = rideObj.ownerName!
            rideCost = rideObj.cost!
            rideDistance = rideObj.distance!
            poolGroupInsStatus =  String(rideObj.poolGroupInsStatus!)
        
        if(showAllRides == false) {
            self.addRideOverlayToMapView(sourceLat,sourceLang: sourceLong,distinationLat: destLat,distinationLang: destLong)
        }
        return true
    }
    
    func hideInfoWindow () {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
                for index in 0..<(self.mapMarkerMutArray?.count)!{
                    let markerPin = self.mapMarkerMutArray![index] as! GMSMarker
                    
                    if segmentRides.selectedSegmentIndex == 0{
                        markerPin.icon = UIImage(named:"marker_ride_green")
                        if(showAllRides == false) {
                        self.originMarker.icon = UIImage(named:"marker_profile_orange")
                        self.destinationMarker.icon = UIImage(named:"marker_profile_green")
                        }
                    }else{
                        markerPin.icon = UIImage(named:"marker_profile_green")
                        if(showAllRides == false) {
                        self.originMarker.icon = UIImage(named:"marker_ride_orange")
                        self.destinationMarker.icon = UIImage(named:"marker_ride_green")
                        }
                    }
                    if(showAllRides == false) {
                        if(tempSearchRides == false) {
                            self.routePolylineOrange.map = nil
                            self.originSelectedRideMarker.map = nil
                            self.destinationSelectedRideMarker.map = nil
                        }
                    }
            }
        }
    }
    
    func addRideOverlayToMapView(_ sourceLat: String,sourceLang: String,distinationLat: String,distinationLang: String ){
        showIndicator("Loading Map path...")
        let sourceStr = "\(sourceLat),\(sourceLang)"
        let destinationStr = "\(distinationLat), \(distinationLang)"
        //TODO: hard code key value addd Need to remove.
        var directionURL = "https://maps.googleapis.com/maps/api/directions/json?origin=\(sourceStr)&destination=\(destinationStr)&mode=driving&alternatives=true"
        directionURL = directionURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        // print(directionURL," directionURL --------- ")
        Alamofire.request(directionURL, method: .get, parameters: nil).responseJSON { response in
            switch(response.result) {
            case .success(_):
                hideIndicator()
                if let data = response.result.value{
                    let dataDict = data as! NSDictionary
                    let routes = dataDict["routes"] as! NSArray
                    let routesDict = routes[0] as! NSDictionary
                    let legs = routesDict["legs"] as! NSArray
                    // self.addmarkers(legs)
                    let legsStartLocDict = legs[0] as! NSDictionary
                    let startLocationDictionary = legsStartLocDict["start_location"] as! NSDictionary
                    self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
                    let legsEndLocDict = legs[legs.count - 1] as! NSDictionary
                    let endLocationDictionary = legsEndLocDict["end_location"] as! NSDictionary
                    self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
                    
                    let vancouver = CLLocationCoordinate2D(latitude: self.originCoordinate.latitude, longitude: self.originCoordinate.longitude)
                    let calgary = CLLocationCoordinate2D(latitude: self.destinationCoordinate.latitude,longitude: self.destinationCoordinate.longitude)
                    let bounds = GMSCoordinateBounds(coordinate: vancouver, coordinate: calgary)
                    let camera = self.mapView.camera(for: bounds, insets: UIEdgeInsets())!
                    self.mapView.camera = camera
                    
                    // self.mapView.camera = GMSCameraPosition.cameraWithTarget(self.originCoordinate, zoom: 13.0)
                    
                    self.originSelectedRideMarker = GMSMarker(position: self.originCoordinate)
                    
                    self.destinationSelectedRideMarker = GMSMarker(position: self.destinationCoordinate)
                    self.destinationSelectedRideMarker.map = self.mapView
                    self.destinationSelectedRideMarker.snippet = self.destloc
                    
                    if self.segmentRides.selectedSegmentIndex == 0{
                        self.destinationSelectedRideMarker.icon = UIImage(named:"marker_profile_green")
                        self.destinationMarker.icon = UIImage(named:"marker_profile_green")
                    }else{
                        self.destinationSelectedRideMarker.icon = UIImage(named:"marker_ride_green")
                        self.destinationMarker.icon = UIImage(named:"marker_ride_green")
                    }
                    //self.destinationSelectedRideMarker.icon = UIImage(named:"marker_wp_orange")
                    //self.destinationMarker.icon = UIImage(named:"marker_wp_orange")
                    self.destinationSelectedRideMarker.title = "destination"
                    self.destinationSelectedRideMarker.title = self.destloc
                    self.destinationSelectedRideMarker.accessibilityLabel = "\(1313)"
                    self.destinationSelectedRideMarker.isTappable = false
                    
                    //self.calculateTotalDistanceAndDuration(legs)
                    
                    let routesDicts = routes[0] as! NSDictionary
                    let overview_polyline = routesDicts["overview_polyline"] as! NSDictionary
                    let points = overview_polyline ["points"] as! NSString
                    
                    
                    if self.routePolylineOrange != nil {
                        self.routePolylineOrange = nil
                    }
                    
                    let path = GMSMutablePath(fromEncodedPath: points as String)
                    self.routePolylineOrange = GMSPolyline(path: path)
                    
                    self.routePolylineOrange.strokeColor = Colors.POLILINE_ORANGE
                    self.routePolylineOrange.strokeWidth = 5.0
                    self.routePolylineOrange.isTappable = false
                    //self.routePolylineOrange.title = "\(self.index)"
                    self.routePolylineOrange.map = self.mapView
                    self.routePolylineOrange.zIndex = 110
                    let boundsPath = GMSCoordinateBounds(path: path!)
                    self.mapView.animate(with: GMSCameraUpdate.fit(boundsPath, withPadding: 80))
                }
                break
            case .failure(_):
                hideIndicator()
                break
            }
        }
    }

    func markerWindowButtonTapped(_ button : UIButton){
        
        let rideObj = (segmentRides.selectedSegmentIndex == 0) ? self.oRideMutArray![button.tag] as! Ride : self.nRideMutArray![button.tag] as! Ride
        showIndicator("Fetching Ride Details")
        RideRequestor().getRideDetails(rideObj.poolID!, userId: UserInfo.sharedInstance.userID, success: { (success, object) in
            hideIndicator()
            if(success){
                self.hideInfoWindow()
                let storyboard = UIStoryboard(name: "MapStoryboard", bundle: nil)
                let vc : WorkSpaceDetailsViewController = storyboard.instantiateViewController(withIdentifier: "WorkSpaceDetailsViewController") as! WorkSpaceDetailsViewController
                vc.isFirstLevel = true
                vc.data = object as? RideDetail
                vc.isCreatedRide = false
                vc.triggerFrom = "ride"
                vc.inviteFlag = "wpMap"
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            contactListArray.removeAllObjects()
            mobileMutArr.removeAllObjects()
            let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
            let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
            for record:ABRecord in contactList as [AnyObject] {
                let contactPerson: ABRecord = record
                let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                if(contactName != "") {
                    var personNumber = ""
                    let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                    let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                    if(phones != "")
                    {
                        if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                        {
                            if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                            {
                                let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                let areaCode = personNumber.characters.count-10
                                let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                if(areaCode > 0) {
                                    personNumber = personNumber.substring(from: startIndex)
                                }
                            }
                        } else {
                            continue
                        }
                    }
                    mobileMutArr.add(personNumber)
                    let nameString = "\(contactName) \(personNumber)"
                    contactListArray.add(nameString)
                }
            }
            let vc = ContactsListViewController()
            vc.contactListArr = self.contactListArray
            vc.mobileMutArr = self.mobileMutArr
            vc.poolID = poolId
            vc.triggerFrom = "createRide"
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    self.contactListArray.removeAllObjects()
                    self.mobileMutArr.removeAllObjects()
                    let contactList: NSArray = ABAddressBookCopyArrayOfAllPeople(addressBookRef).takeRetainedValue()
                    for record:ABRecord in contactList as [AnyObject] {
                        let contactPerson: ABRecord = record
                        let contactName = ABRecordCopyCompositeName(contactPerson).takeRetainedValue() as NSString
                        if(contactName != "") {
                            var personNumber = ""
                            let unmanagedPhones = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty)
                            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
                            if(phones != "")
                            {
                                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                                {
                                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                                    {
                                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                                        personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                                        let areaCode = personNumber.characters.count-10
                                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                                        if(areaCode > 0) {
                                            personNumber = personNumber.substring(from: startIndex)
                                        }
                                    }
                                } else {
                                    continue
                                }
                            }
                            self.mobileMutArr.add(personNumber)
                            let nameString = "\(contactName) \(personNumber)"
                            self.contactListArray.add(nameString)
                        }
                    }
                    
                    let vc = ContactsListViewController()
                    vc.contactListArr = self.contactListArray
                    vc.mobileMutArr = self.mobileMutArr
                    vc.poolID = self.poolId
                    vc.triggerFrom = "createRide"
                    vc.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func displayCantAddContactAlert() {
        //        AlertController.showAlertFor("Cannot Add Contact", message: "You must give the app permission to add the contact first.", okButtonTitle: "Settings", okAction: {
        //            self.openSettings()
        //            }, cancelButtonTitle: "OK", cancelAction: nil)
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    func message_clicked() {
        self.dismiss(animated: true, completion: nil)
        self.callAddressBookForPressedButtonObject()
    }
    
    func whatsApp_clicked() {
        self.dismiss(animated: true, completion: nil)
        sendWhatsAppMsg("", wpKey: "", poolID: poolId)
    }

    func inviteButtonTapped() {
        if infoWindow != nil {
            hideInfoWindow()
        }
        let inviteView = InviteNormalRideView()
        let alertViewHolder =   AlertContentViewHolder()
        alertViewHolder.heightConstraintValue = 95
        alertViewHolder.view = inviteView
        
        inviteView.messageBtn.addTarget(self, action: #selector(message_clicked), for: .touchDown)
        inviteView.whatsAppBtn.addTarget(self, action: #selector(whatsApp_clicked), for: .touchDown)
        
        AlertController.showAlertFor(nil, message: "", contentView: alertViewHolder, okButtonTitle: "CANCEL", willHaveAutoDismiss: true, okAction: nil)
    }
    
    func cancelButtonTapped() {
        AlertController.showAlertFor("Cancel Ride", message: "Would you really like to cancel this ride?", okButtonTitle: "Yes", okAction: {
            //from memberslist - FR
            //from ridedetail & ridelist = JR
            showIndicator("Cancelling Ride.")
            RideRequestor().cancelRequest(self.poolId, userId: UserInfo.sharedInstance.userID, cancelrequestType: "JR", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Cancel Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        if(self.showAllRides) {
                            self.refreshAllRidesData()
                        } else {
                            self.searchRide()
                        }
                    })
                }
                else{
                    AlertController.showAlertFor("Cancel Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
            }, cancelButtonTitle: "Later", cancelAction: {
        })
    }
    
    func acceptRide() {
        AlertController.showAlertFor("Accept Request", message: "Would you really like to accept this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Accepting ride request..")
            RideRequestor().acceptRideRequest(self.poolId, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Accept Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        if(self.showAllRides) {
                            self.refreshAllRidesData()
                        } else {
                            self.searchRide()
                        }
                    })
                }else{
                    AlertController.showAlertFor("Accept Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    
    func acceptButtonTapped() {
        if(self.poolTaxiBooking != "1" && self.rideCost != "0" && self.rideDistance != "" && self.rideDistance != "0") {
            ridePoints = Int(self.rideCost)! * Int(round(Double(self.rideDistance)!))
            if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                    AlertController.showAlertFor("Accept Ride", message: "\("SORRY! You cannot accept this ride request because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and accept this ride request.")", okButtonTitle: "RECHARGE", okAction: {
                        let vc = MyAccountViewController()
                        vc.hidesBottomBarWhenPushed = true
                        //vc.edgesForExtendedLayout = .None
                        self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                        }, cancelButtonTitle: "CANCEL", cancelAction: {})
                } else {
                    acceptRide()
                }
            } else {
                acceptRide()
            }
        } else {
            acceptRide()
        }
    }
    
    func rejectButtonTapped() {
        AlertController.showAlertFor("Reject Request", message: "Would you really like to reject this ride request?", okButtonTitle: "Yes", okAction: {
            showIndicator("Rejecting ride request..")
            RideRequestor().rejectRideRequest(self.poolId, userId: UserInfo.sharedInstance.userID, userType: "Friend", success: { (success, object) in
                hideIndicator()
                if(success == true){
                    AlertController.showAlertFor("Reject Ride", message: object as? String, okButtonTitle: "Ok", okAction: {
                        if(self.showAllRides) {
                            self.refreshAllRidesData()
                        } else {
                            self.searchRide()
                        }
                    })
                }else{
                    AlertController.showAlertFor("Reject Ride", message: object as? String)
                }
                }, failure: { (error) in
                    hideIndicator()
            })
        }, cancelButtonTitle: "Cancel") {
        }
    }
    
    func joinRide() {
        if(UserInfo.sharedInstance.shareMobileNumber == false){
            AlertController.showAlertFor("Join Ride", message: "Your Mobile Number will be shared with other potential member. Would you like to join in this ride?", okButtonTitle: "YES", okAction: {
                self.getPickUpDropLocation("joinRide")
                }, cancelButtonTitle: "LATER", cancelAction: {
            })
        }
        else{
            getPickUpDropLocation("joinRide")
        }
    }
    
    func joinButtonTapped () {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            if(self.poolTaxiBooking != "1" && self.rideCost != "0" && self.rideDistance != "" && self.rideDistance != "0") {
                ridePoints = Int(self.rideCost)! * Int(round(Double(self.rideDistance)!))
                if(UserInfo.sharedInstance.maxAllowedPoints != "") {
                    if((UserInfo.sharedInstance.availablePoints + Int(UserInfo.sharedInstance.maxAllowedPoints)! < ridePoints)){
                        AlertController.showAlertFor("Join Ride", message: "\("SORRY! You cannot join this ride because your available points is") \(UserInfo.sharedInstance.availablePoints) \("please recharge and join this ride.")", okButtonTitle: "RECHARGE", okAction: {
                            let vc = MyAccountViewController()
                            vc.hidesBottomBarWhenPushed = true
                            //vc.edgesForExtendedLayout = .None
                            self.present(UINavigationController(rootViewController: vc), animated: true, completion: nil)
                            }, cancelButtonTitle: "CANCEL", cancelAction: {})
                    } else {
                        joinRide()
                    }
                } else {
                    joinRide()
                }
            } else {
                joinRide()
            }
        }
    }

    func offerRideButtonTapped() {
        if infoWindow != nil {
            infoWindow.removeFromSuperview()
            getPickUpDropLocation("offerVehicle")
        }
    }
    
    func getPickUpDropLocation(_ triggerFrom: String)
    {
        let joinOrOfferRideObj = JoinOrOfferRide()
        let vc = RideDetailMapViewController()
        vc.sourceLong = self.sourceLong
        vc.sourceLat = self.sourceLat
        vc.sourceLocation = self.sourceLoc
        vc.destLong = self.destLong
        vc.destLat = self.destLat
        vc.destLocation = self.destloc
        vc.poolTaxiBooking = self.poolTaxiBooking
        vc.poolId = self.poolId
        vc.rideType = self.rideType
        vc.edgesForExtendedLayout = UIRectEdge()
        vc.hidesBottomBarWhenPushed = true
        vc.triggerFrom = triggerFrom
        vc.isWP = false
        vc.wpInsuranceStatus = poolGroupInsStatus
        
        vc.onConfirmLocationsBlock = { (pickUpLoc, pickUpLat, pickUpLong, dropLoc, dropLat, dropLong) -> Void in
            joinOrOfferRideObj.userID = UserInfo.sharedInstance.userID
            joinOrOfferRideObj.poolID = self.poolId
            joinOrOfferRideObj.vehicleRegNo = ""
            joinOrOfferRideObj.vehicleCapacity = ""
            joinOrOfferRideObj.cost = "0"
            joinOrOfferRideObj.rideMsg = ""
            joinOrOfferRideObj.pickupPoint = pickUpLoc as? String
            joinOrOfferRideObj.pickupPointLat = pickUpLat as? String
            joinOrOfferRideObj.pickupPointLong = pickUpLong as? String
            joinOrOfferRideObj.dropPoint = dropLoc as? String
            joinOrOfferRideObj.dropPointLat = dropLat as? String
            joinOrOfferRideObj.dropPointLong = dropLong as? String
            
            if((joinOrOfferRideObj.pickupPoint != nil) && (joinOrOfferRideObj.dropPoint != nil)){
                showIndicator("Joining Ride..")
                OfferVehicleRequestor().joinOrOfferRide(joinOrOfferRideObj, success: { (success, object) in
                    hideIndicator()
                    if(success == true){
                        AlertController.showAlertFor("Join Ride", message: "Thank you, Your request to join this ride has been sent to the initiator.", okButtonTitle: "Ok", okAction: {
                            if(self.showAllRides) {
                                self.refreshAllRidesData()
                            } else {
                                self.searchRide()
                            }
                        })
                    }
                    else{
                        if(((object as AnyObject).code)! == "77") {
                            AlertController.showToastForError("You have already sent a request to join this ride.")
                        } else {
                            AlertController.showAlertFor("Join Ride", message: object as? String, okButtonTitle: "Ok", okAction: nil)
                        }
                    }
                    }, failure: { (error) in
                        hideIndicator()
                })
                
            }
            else{
                AlertController.showAlertFor("Offer a Vehicle", message: "Please choose location in 'Start' and 'End' location from Google Places.")
            }
        }
        
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getAllRides() {
        showIndicator("Loading Rides.")
        let reqObj = AllRidesRequest()
        reqObj.page = ""
        reqObj.pooltype = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        reqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        reqObj.reqTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        reqObj.rideScope = ""
        reqObj.searchType = "generic"
        reqObj.showRtype = "AC"
        reqObj.userId = UserInfo.sharedInstance.userID
        
        RideRequestor().getAllRides(reqObj, success: { (success, object) in
            hideIndicator()
            self.allRidesData = ((object as! AllRidesResponse).pools!)
            RideRequestor().getDefaultVehicle({ (successVeh, objectVeh) in
                if (successVeh) {
                    //self.vehicleDefaultDetails = objectVeh as! VehicleDefaultResponse
                    self.allRidesMarker()
                } else {
                    self.allRidesMarker()
                }
            }) { (error) in
                hideIndicator()
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func allRidesMarker() {
        self.mapView.clear()
        if infoWindow != nil {
            hideInfoWindow()
        }
        let count = (self.allRidesData?.count)! as Int
        if count == 0 {
            return
        } else {
            showAllRides = true
            self.getISRideType(self.allRidesData)
            self.updateMarker()
        }
    }
    
    func refreshAllRidesData() {
        showIndicator("Refreshing Rides.")
        let reqObj = AllRidesRequest()
        reqObj.page = ""
        reqObj.pooltype = UserInfo.sharedInstance.gender == "M" ? "AM" : "AF"
        reqObj.reqDate = prettyDateStringFromDate(Date(), toFormat: "dd-MMM-yyyy")
        reqObj.reqTime = prettyDateStringFromDate(Date().addingTimeInterval(300), toFormat: "HH:mm")
        reqObj.rideScope = ""
        reqObj.searchType = "generic"
        reqObj.showRtype = "AC"
        reqObj.userId = UserInfo.sharedInstance.userID
        
        RideRequestor().getAllRides(reqObj, success: { (success, object) in
            hideIndicator()
            self.allRidesData = ((object as! AllRidesResponse).pools!)
            self.allRidesMarker()
        }) { (error) in
            hideIndicator()
        }
    }
    
    func showSearchRides()
    {
        let vc = SearchRideViewController()
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func showAllRidesList() {
        let vc = RideViewController()
        vc.allRides = true
        vc.hidesBottomBarWhenPushed = true
        vc.edgesForExtendedLayout = UIRectEdge()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func inviteSendNotification (_ notification:Notification) -> Void {
    }
}
