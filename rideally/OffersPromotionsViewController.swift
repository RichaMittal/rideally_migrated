//
//  OffersPromotionsViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 2/20/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit
import CoreGraphics

class OffersPromotionsCell: UITableViewCell {
    
    let couponCode = UILabel()
    let couponDescription = UILabel()
    let couponExpiryDate = UILabel()
    let readMore = UIButton()
    var onUserActionBlock: actionBlockWithParam?
    
    var data: OffersPromotionsData!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        
        couponCode.font = boldFontWithSize(15)
        couponCode.textColor = Colors.ORANGE_TEXT_COLOR
        couponCode.lineBreakMode = NSLineBreakMode.byWordWrapping
        couponCode.translatesAutoresizingMaskIntoConstraints = false
        couponCode.numberOfLines = 0
        self.contentView.addSubview(couponCode)
        
        couponExpiryDate.font = boldFontWithSize(15)
        couponExpiryDate.textColor = Colors.ORANGE_TEXT_COLOR
        couponExpiryDate.lineBreakMode = NSLineBreakMode.byWordWrapping
        couponExpiryDate.translatesAutoresizingMaskIntoConstraints = false
        couponExpiryDate.numberOfLines = 0
        couponExpiryDate.textAlignment = .right
        self.contentView.addSubview(couponExpiryDate)
        
        couponDescription.font = boldFontWithSize(15)
        couponDescription.textColor = Colors.GRAY_COLOR
        couponDescription.lineBreakMode = NSLineBreakMode.byWordWrapping
        couponDescription.translatesAutoresizingMaskIntoConstraints = false
        //couponDescription.numberOfLines = 2
        self.contentView.addSubview(couponDescription)
        
        readMore.titleLabel?.font = normalFontWithSize(14)!
        readMore.setTitle("READ MORE", for: UIControlState())
        readMore.setTitleColor(Colors.ORANGE_COLOR, for: UIControlState())
        readMore.translatesAutoresizingMaskIntoConstraints = false
        readMore.addTarget(self, action: #selector(btnReadMore_clicked), for: .touchDown)
        self.contentView.addSubview(readMore)
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[code]-5-[desc]-5-[readMore]-5-|", options: [], metrics: nil, views: ["code":couponCode, "desc":couponDescription, "readMore":readMore]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[code]-5-[date(==code)]-15-|", options: [], metrics: nil, views: ["code":couponCode,"date":couponExpiryDate]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[desc]-5-|", options: [], metrics: nil, views: ["desc":couponDescription]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[readMore(80)]-10-|", options: [], metrics: nil, views: ["readMore":readMore]))
        self.contentView.addConstraint(NSLayoutConstraint(item: couponExpiryDate, attribute: .centerY, relatedBy: .equal, toItem: couponCode, attribute: .centerY, multiplier: 1, constant: 0))
        
    }
    
    func btnReadMore_clicked() {
        let viewHolder = AlertContentViewHolder()
        viewHolder.heightConstraintValue = 95
        //            let scrollView = UIScrollView()
        //            scrollView.backgroundColor = Colors.WHITE_COLOR
        //            scrollView.delegate = self
        //            scrollView.contentSize = CGSizeMake(1000, 1000)
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Colors.WHITE_COLOR
        
        //            scrollView.showsVerticalScrollIndicator = false
        //            scrollView.bounces = false
        //            scrollView.delegate = self
        //
        //            scrollView.translatesAutoresizingMaskIntoConstraints = false
        //
        //            //view.addSubview(scrollView)
        //            scrollView.addSubview(contentView)
        let lblInfo = UILabel()
        lblInfo.text = data.couponDescription
        lblInfo.numberOfLines = 0
        lblInfo.lineBreakMode = .byWordWrapping
        lblInfo.translatesAutoresizingMaskIntoConstraints = false
        lblInfo.font = normalFontWithSize(13)
        view.addSubview(lblInfo)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[lbl]|", options: [], metrics: nil, views: ["lbl":lblInfo]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblInfo]))
        //            scrollView.addSubview(view)
        //            //            //self.contentView.addSubview(scrollView)
        //            scrollView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view" : view]))
        //            scrollView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view" : view]))
        //            let contentWidthConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: scrollView, attribute: NSLayoutAttribute.Width, multiplier: 1.0, constant: 0)
        //            scrollView.addConstraint(contentWidthConstraint)
        viewHolder.view = view
        
        AlertController.showAlertFor(data.couponCode, message: "", contentView: viewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: true, okAction: {
        })
    }
    
    func buildCell()
    {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let showDate = inputFormatter.date(from: data.couponExpiryDate!)
        inputFormatter.dateFormat = "dd MMM yyyy"
        let resultString = inputFormatter.string(from: showDate!)
        
        if(data.couponCode == "RAF") {
            couponCode.text = data.couponCode!+UserInfo.sharedInstance.userID
        } else {
            couponCode.text = data.couponCode
        }
        if((data.couponDescription?.contains("Steps"))!) {
            couponDescription.numberOfLines = 2
            readMore.isHidden = false
        } else {
            couponDescription.numberOfLines = 0
            readMore.isHidden = true
        }
        couponExpiryDate.text = "Expires on: "+removeYearFromDate(resultString)
        couponDescription.text = data.couponDescription
        setNeedsLayout()
    }
}

class OffersPromotionsViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource {
    
    var offersPromotionsData = [OffersPromotionsData]()
    let table = UITableView()
    var onSelectBackBlock: actionBlock?
    //lazy var tableView: SubView = self.getTableView()
    var comingFrom = ""
    
    override func viewDidLoad() {
        if(comingFrom == "") {
            getOffersPromotions()
        }
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        
        table.delegate = self
        table.dataSource = self
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 40
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorColor = Colors.GRAY_COLOR
        table.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        table.register(OffersPromotionsCell.self, forCellReuseIdentifier: "offerspromotionscell")
        self.view.addSubview(table)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views:["table":table]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-64-[table]|", options: [], metrics: nil, views:["table":table]))
        self.table.tableFooterView = UIView()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Offers & Promotions"
    }
    
    func closeView()
    {
        self.dismiss(animated: true, completion: nil)
        onSelectBackBlock?()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offersPromotionsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "offerspromotionscell") as! OffersPromotionsCell
        cell.data = offersPromotionsData[indexPath.row]
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.onUserActionBlock = { (selectedAction) -> Void in
            self.cell_btnAction(selectedAction as! UILabel, indexPath: indexPath)
        }
        return cell
    }
    
    func cell_btnAction(_ action: UILabel, indexPath: IndexPath)
    {
        action.numberOfLines = 0
        action.sizeToFit()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(offersPromotionsData[indexPath.row].couponCode == "RAF") {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "shareButton"), object: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = Colors.WHITE_COLOR
    }
    
    func getOffersPromotions()
    {
        let reqObj = OffersPromotionsRequest()
        reqObj.user_id = UserInfo.sharedInstance.userID
        showIndicator("Getting Offers & Promotions..")
        OffersPromotionsRequestor().getOffersPromotions(reqObj, success:{(success, object) in
            hideIndicator()
            if (object as! OffersPromotionsResponse).code == "2999" {
                self.offersPromotionsData = (object as! OffersPromotionsResponse).dataObj!
                self.table.reloadData()
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func labelGesturePressed(_ sender:AnyObject)  {
        self.dismiss(animated: true, completion:nil)
        let vc = StaticPagesViewController()
        vc.staticPage = "blr-airport-tc"
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
