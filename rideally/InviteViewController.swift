//
//  InviteViewController.swift
//  rideally
//
//  Created by Sarav on 27/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class InviteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    
    var onCompletion: actionBlock?
    var poolID: String?
    var ownerID: String?
    var responses: [InviteResult]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Invite Friends"
        // Do any additional setup after loading the view.
        
        let newBackButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem = newBackButton;

    }
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    let lblMobileTitle = UILabel()
    let lblMobileCode = UILabel()
    let txtMobile = RATextField()
    let lblMobileInfo = UILabel()
    
//    let lblOr = UILabel()
//    
//    let lblEmailTitle = UILabel()
//    let txtEmail = RATextField()
//    let lblEmailInfo = UILabel()
    
    let btnInvite = UIButton()
    
    override func loadView() {
        super.loadView()
        
        self.view.backgroundColor = Colors.WHITE_COLOR
        
        lblMobileTitle.text = "ENTER MOBILE NUMBER TO INVITE YOUR FRIENDS"
        lblMobileTitle.textColor = Colors.GREEN_COLOR
        lblMobileTitle.font = boldFontWithSize(12)
        lblMobileTitle.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(lblMobileTitle)

        lblMobileCode.text = "+91"
        lblMobileCode.textColor = Colors.GRAY_COLOR
        lblMobileCode.font = boldFontWithSize(14)
        lblMobileCode.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(lblMobileCode)

        txtMobile.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        txtMobile.font = normalFontWithSize(14)
        txtMobile.textColor = Colors.GRAY_COLOR
        txtMobile.translatesAutoresizingMaskIntoConstraints = false
        txtMobile.returnKeyType = .done
        txtMobile.delegate = self
        self.view.addSubview(txtMobile)
        
        lblMobileInfo.text = "You can enter multiple Mobile No(s) separated by comma (,)"
        lblMobileInfo.textColor = Colors.GRAY_COLOR
        lblMobileInfo.font = boldFontWithSize(12)
        lblMobileInfo.numberOfLines = 0
        lblMobileInfo.textAlignment = .center
        lblMobileInfo.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(lblMobileInfo)
        
//        lblOr.textAlignment = .Center
//        lblOr.text = "OR"
//        lblOr.textColor = Colors.GRAY_COLOR
//        lblOr.font = normalFontWithSize(15)
//        lblOr.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addSubview(lblOr)
//        
//        lblEmailTitle.text = "ENTER EMAIL ID TO INVITE YOUR FRIENDS"
//        lblEmailTitle.textColor = Colors.GREEN_COLOR
//        lblEmailTitle.font = boldFontWithSize(12)
//        lblEmailTitle.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addSubview(lblEmailTitle)
//        
//        txtEmail.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPointZero)
//        txtEmail.font = normalFontWithSize(14)
//        txtEmail.textColor = Colors.GRAY_COLOR
//        txtEmail.translatesAutoresizingMaskIntoConstraints = false
//        txtEmail.returnKeyType = .Done
//        txtEmail.delegate = self
//        self.view.addSubview(txtEmail)
//        
//        lblEmailInfo.text = "You can enter multiple email Id(s) separated by comma (,)"
//        lblEmailInfo.textColor = Colors.GRAY_COLOR
//        lblEmailInfo.font = boldFontWithSize(12)
//        lblEmailInfo.numberOfLines = 0
//        lblEmailInfo.textAlignment = .Center
//        lblEmailInfo.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addSubview(lblEmailInfo)
        
        btnInvite.backgroundColor = Colors.GRAY_COLOR
        btnInvite.setTitle("INVITE", for: UIControlState())
        btnInvite.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        btnInvite.titleLabel?.font = boldFontWithSize(15)
        btnInvite.translatesAutoresizingMaskIntoConstraints = false
        btnInvite.addTarget(self, action: #selector(btnInvite_clicked), for: .touchDown)
        self.view.addSubview(btnInvite)
        
        //let viewsDict = ["lblmobile":lblMobileTitle, "lblmobilecode":lblMobileCode, "txtmobile":txtMobile, "lblmobileinfo":lblMobileInfo, "lblor":lblOr, "lblemail":lblEmailTitle, "txtemail":txtEmail, "lblemailinfo":lblEmailInfo, "btninvite":btnInvite]
        
        let viewsDict = ["lblmobile":lblMobileTitle, "lblmobilecode":lblMobileCode, "txtmobile":txtMobile, "lblmobileinfo":lblMobileInfo, "btninvite":btnInvite]
        
        //self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-20-[lblmobile(20)][txtmobile(30)]-10-[lblmobileinfo]-20-[lblor]-20-[lblemail(20)][txtemail(30)]-10-[lblemailinfo]-25-[btninvite(35)]", options: [], metrics: nil, views: viewsDict))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[lblmobile(20)][txtmobile(30)]-10-[lblmobileinfo]-35-[btninvite(35)]", options: [], metrics: nil, views: viewsDict))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblmobile]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblmobilecode]-5-[txtmobile]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraint(NSLayoutConstraint(item: lblMobileCode, attribute: .centerY, relatedBy: .equal, toItem: txtMobile, attribute: .centerY, multiplier: 1, constant: 0))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lblmobileinfo]-10-|", options: [], metrics: nil, views: viewsDict))
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[lblor]-10-|", options: [], metrics: nil, views: viewsDict))
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[lblemail]-10-|", options: [], metrics: nil, views: viewsDict))
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[txtemail]-10-|", options: [], metrics: nil, views: viewsDict))
//        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[lblemailinfo]-10-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[btninvite]-10-|", options: [], metrics: nil, views: viewsDict))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func btnInvite_clicked()
    {
        //if((txtEmail.text?.characters.count > 0) || (txtMobile.text?.characters.count > 0)){
            if((txtMobile.text?.characters.count > 0)){
            if(txtMobile.text?.characters.count > 0)
            {
                if(!isMobileValid()){
                    AlertController.showAlertFor("Invite Members", message: "Please enter valid mobile number(s) to send invite.")
                    return
                }
            }
//            if(txtEmail.text?.characters.count > 0)
//            {
//                if(!isEmailValid()){
//                    AlertController.showAlertFor("Invite Members", message: "Please enter valid emailID(s) to send invite.")
//                    return
//                }
//            }
            sendRequest()
        }
        else{
            //AlertController.showAlertFor("Invite Members", message: "Please enter emailID(s) or mobile number(s) to send invite.")
                AlertController.showAlertFor("Invite Members", message: "Please enter mobile number(s) to send invite.")
        }
    }
    
    func sendRequest()
    {
        showIndicator("Sending Invites..")
        RideRequestor().sendInvite(poolID!, ownerId: ownerID!, emailIDs: "", mobileNOs: txtMobile.text, success: { (success, object) in
            hideIndicator()
            if(success){
                self.responses = (object as! InviteMembersResponse).data
                let view = self.getInviteResposeView()
                (view.viewWithTag(33) as? UITableView)?.reloadData()
                
                let alertViewHolder =   AlertContentViewHolder()
                alertViewHolder.heightConstraintValue = 100
                alertViewHolder.view = view
                
                AlertController.showAlertFor("Invite Members", message: "", contentView: alertViewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: true, okAction: {
                    _ = self.navigationController?.popViewController(animated: true)
                    self.onCompletion?()
                    }, cancelButtonTitle: nil, cancelAction: nil)
            }
            }, failure: { (error) in
                hideIndicator()
        })
    }
    
    func isMobileValid() -> Bool
    {
        let arr = txtMobile.text?.components(separatedBy: ",")
        for num in arr!
        {
            if(!isValidMobile(num)){
                return false
            }
        }
        return true
    }
//    func isEmailValid() -> Bool
//    {
//        let arr = txtEmail.text?.componentsSeparatedByString(",")
//        for num in arr!
//        {
//            if(!isValidEmail(num)){
//                return false
//            }
//        }
//        return true
//    }
    
    
    func getInviteResposeView() -> UIView
    {
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        let table = UITableView()
        table.delegate = self
        table.tag = 33
        table.dataSource = self
        table.translatesAutoresizingMaskIntoConstraints = false
        table.separatorColor = UIColor.clear
        view.addSubview(table)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table(100)]|", options: [], metrics: nil, views: ["table":table]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":table]))
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responses!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.font = normalFontWithSize(14)
        cell.textLabel?.numberOfLines = 0
        if let mobile = responses![indexPath.row].mobile{
            cell.textLabel?.text = "\(mobile) \(getMsgForCode(responses![indexPath.row].msgCode!))"
        }
        if let email = responses![indexPath.row].email{
            cell.textLabel?.text = "\(email) \(getMsgForCode(responses![indexPath.row].msgCode!))"
        }
        return cell
    }
    
    func getMsgForCode(_ code: String) -> String
    {
        let dict = ["161":"for tieup workplace domain mismatch",
                    "157":"personal not filled",
                    "158":"gender not filled",
                    "160":"is not registered with rideally, however inivitation has been sent successfully.",
                    "151":"Sorry, this is only female ride, hence you cannot invite a male.",
                    "152":"Sorry, this is only male ride, hence you cannot invite a female.",
                    "141":"Your invitation has been sent successfully & Notification Send to user.",
                    "142":"Your invitation has been sent successfully but Error in notification sending.",
                    "154":"Sorry, user has already joined in this ride.",
                    "155":"User already invited, however invitation has been sent successfully.",
                    "156":"User has already sent a request to join this ride. ",
                    "153":"Oops! You are the initiator of this ride, hence you cannot invite yourself."]
        
        return dict[code]!
    }
}
