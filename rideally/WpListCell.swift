//
//  WpListCell.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 5/23/17.
//  Copyright © 2017 rideally. All rights reserved.
//

//
//  WorkplaceCell.swift
//  rideally
//
//  Created by Sarav on 13/09/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import CoreGraphics
import FirebaseAnalytics

class WpListCell: UITableViewCell {

    let lblWPName = UILabel()
    let imgSource = UIImageView()
    let lblLocation = UILabel()
    
    var onUserActionBlock: actionBlockWithParam?
    var newRideReqObj = WPNewRideRequest()
    var searchResult:Bool?
    var data: WorkPlace!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        lblWPName.font = boldFontWithSize(13)
        lblLocation.font = boldFontWithSize(13)
        
        lblWPName.translatesAutoresizingMaskIntoConstraints = false
        lblLocation.translatesAutoresizingMaskIntoConstraints = false
        imgSource.translatesAutoresizingMaskIntoConstraints = false
        
        imgSource.image = UIImage(named: "loc_gray")
        self.contentView.addSubview(lblWPName)
        self.contentView.addSubview(lblLocation)
        self.contentView.addSubview(imgSource)
        
        imgSource.backgroundColor = UIColor.clear
        imgSource.layer.cornerRadius = 15
        imgSource.layer.masksToBounds = true
        
        imgSource.isUserInteractionEnabled = true
        
        lblWPName.textColor = Colors.GRAY_COLOR
        lblLocation.textColor = Colors.GRAY_COLOR
        
        let viewsDict = ["pic":imgSource, "name":lblWPName, "location":lblLocation]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[pic(30)]-5-[name]-15-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(30)]-10-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[name(20)][location(20)]-10-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblLocation, attribute: .left, relatedBy: .equal, toItem: lblWPName, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblLocation, attribute: .right, relatedBy: .equal, toItem: lblWPName, attribute: .right, multiplier: 1, constant: 0))
    }
    
    func loadMembers()
    {
        onUserActionBlock?("MEMBERS" as AnyObject?)
    }
    
    func loadRides()
    {
        onUserActionBlock?("RIDES" as AnyObject?)
    }
    
    func addRide()
    {
        onUserActionBlock?("ADDRIDE" as AnyObject?)
    }
    
    func btnActionClicked()
    {
        //onUserActionBlock?(btnAction.titleLabel?.text!)
    }
    
    func buildCell()
    {
        if(searchResult == true) {
            lblWPName.text = data.wpSearchedName
            lblLocation.text = data.wpSearchedLocation
        } else {
            lblWPName.text = data.wpName
            lblLocation.text = data.wpLocation
        }
    }
}
