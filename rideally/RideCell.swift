//
//  RideCell.swift
//  rideally
//
//  Created by Sarav on 02/08/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import Kingfisher

class MyRideCell: RideCell{
    
    override func buildCell() {
        lblVerification.isHidden = false
        verificationView.isHidden = true
        seatsIcon.isHidden = false
        lblSeatsCnt.isHidden = false
        vehIcon.isHidden = false
        imgLine.isHidden = false
        imgLine1.isHidden = false
        imgLine2.isHidden = false
        imgMembers.isHidden = false
        lblMembersCnt.isHidden = false
        threeDot.isHidden = false
        lblCost.isHidden = false
        imgCost.isHidden = false
        
        if let vehicleType = data.vehicleType {
            if(vehicleType.lowercased() == "bike") {
                vehIcon.image = UIImage(named: "veh_bike")
            } else if(vehicleType.lowercased() == "auto") {
                vehIcon.image = UIImage(named: "veh_auto")
            } else if(vehicleType.lowercased() == "car") {
                vehIcon.image = UIImage(named: "veh_car")
            } else if(vehicleType.lowercased() == "cab") {
                vehIcon.image = UIImage(named: "veh_cab")
            } else if(vehicleType.lowercased() == "suv") {
                vehIcon.image = UIImage(named: "veh_suv")
            } else if(vehicleType.lowercased() == "tempo") {
                vehIcon.image = UIImage(named: "veh_tempo")
            } else if(vehicleType.lowercased() == "bus") {
                vehIcon.image = UIImage(named: "veh_bus")
            }
        }
        if(data.poolTaxiBooking == "1") {
            lblVerification.text = "TAXI"
        } else {
            lblVerification.text = "POOL"
        }
        
        if(selectedIndex == 1) {
            threeDot.isHidden = true
            seatsIcon.isHidden = true
            lblSeatsCnt.isHidden = true
            vehIcon.isHidden = true
            imgLine1.isHidden = true
            imgLine2.isHidden = true
            imgMembers.isHidden = true
            lblMembersCnt.isHidden = true
        } else {
            if(data.poolTaxiBooking == "1") {
                threeDot.isHidden = true
            }
            if(data.rideType == "N" && data.vehicleRegNo == nil){
                seatsIcon.isHidden = true
                lblSeatsCnt.isHidden = true
                vehIcon.isHidden = true
                imgLine.isHidden = true
                imgLine1.isHidden = true
                imgLine2.isHidden = true
                lblCost.isHidden = true
                imgCost.isHidden = true
            }
        }
        var poolStartDate = ""
        if let date = (data as! MyRide).poolDate {
            let dateArray = date.components(separatedBy: " ")
            var currentDate = prettyDateStringFromDate(Date(), toFormat: "dd MMM yyyy")
            if(currentDate.hasPrefix("0")) {
                currentDate.remove(at: currentDate.startIndex)
            }
            let compareResult = currentDate.compare(date)
            if(compareResult == ComparisonResult.orderedSame) {
                poolStartDate = "Today"
            } else {
                poolStartDate = "\(dateArray[0]) \(dateArray[1])"
            }
        }
        if let time = (data as! MyRide).poolTime {
            lblDateTime.text = "\(poolStartDate) \(time)"
        }
        
        if(data.ownerProfilePicStatus == "1"){
            if data.ownerProfilePicUrl != nil &&  data.ownerProfilePicUrl != ""{
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\( data.ownerProfilePicUrl)"))
            }
            else if data.ownerFBID != nil && data.ownerFBID != ""{
                    profilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(data.ownerFBID)/picture?type=large&return_ssl_resources=1"))
            } else {
                profilePic.image = UIImage(named: "placeholder")
            }
        }
        else{
            profilePic.image = UIImage(named: "placeholder")
        }
        
        btn1.isHidden = true
        btn2.isHidden = true
        
        lblSource.text = data.source
        
        var nowText = ""
        var combinedString: NSString
        if let via = data.via {
            nowText = via
            combinedString = "\("VIA-") \(nowText)" as NSString
        } else {
            nowText = ""
            combinedString = "" as NSString
        }
        let range = combinedString.range(of: nowText)
        let attributedString = NSMutableAttributedString(string: combinedString as String)
        let attributes = [NSForegroundColorAttributeName: Colors.ORANGE_COLOR]
        attributedString.addAttributes(attributes, range: range)
        self.lblVia.attributedText = attributedString
        
        lblDestination.text = data.destination
        if(data.poolTaxiBooking == "1"){
            lblMembersCnt.text = data.poolJoinedMembers
            if(data.userCost != nil && data.userCost != "0" && data.userCost != "") {
                lblCost.text = "\(RUPEE_SYMBOL) \(data.userCost!) / \(data.costType!)"
            } else {
                lblCost.text = "\(RUPEE_SYMBOL) \(data.cost!) / \(data.costType!)"
            }
            //lblCost.text = "\(RUPEE_SYMBOL) \(data.userCost!) / \(data.costType!)"
        }
        else{
            if let cost = data.cost {
                if(Int(cost) == 0){
                    lblCost.text = "Free of Cost"
                } else {
                    lblCost.text = "\(cost) \("Pts /") \(data.costType!)"
                }
            }
            
            if let cnt = data.members{
                let cntInt = Int(cnt)!
                lblMembersCnt.text = "\(cntInt + 1)"
            }
        }
        lblSeatsCnt.text = data.seatsLeft
    }
}

class RideCell: UITableViewCell {
    
    let profilePic = UIImageView()
    let infoView = UIView()
    let lblSource = UILabel()
    let lblVia = UILabel()
    let lblDateTime = UILabel()
    let imgCost = UIImageView()
    let lblCost = UILabel()
    let lblDestination = UILabel()
    let imgLine = UIImageView()
    let imgSource = UIImageView()
    let imgVia = UIImageView()
    let imgDestination = UIImageView()
    let imgDate = UIImageView()
    let imgMembers = UIImageView()
    let lblMembersCnt = UILabel()
    let imgLine1 = UIImageView()
    let seatsIcon = UIImageView()
    let lblSeatsCnt = UILabel()
    let imgLine2 = UIImageView()
    let vehIcon = UIImageView()
    
    let btn1 = UIButton()
    let btn2 = UIButton()
    
    let emailIcon = UIImageView()
    let mobileIcon = UIImageView()
    let verificationView = UIView()
    
    let lblVerification = UILabel()
    
    let threeDot = UIButton()
    var onUserActionBlock: actionBlockWithParam?
    var selectedIndex: Int!
    var data: Ride!{
        didSet{
            buildCell()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        lblSource.font = normalFontWithSize(13)
        lblVia.font = normalFontWithSize(12)
        lblDateTime.font = normalFontWithSize(12)
        lblCost.font = normalFontWithSize(12)
        lblDestination.font = normalFontWithSize(13)
        
        profilePic.layer.cornerRadius = 20
        profilePic.layer.masksToBounds = true
        
        profilePic.isUserInteractionEnabled = true
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        infoView.translatesAutoresizingMaskIntoConstraints = false
        lblSource.translatesAutoresizingMaskIntoConstraints = false
        lblVia.translatesAutoresizingMaskIntoConstraints = false
        lblDateTime.translatesAutoresizingMaskIntoConstraints = false
        lblCost.translatesAutoresizingMaskIntoConstraints = false
        lblDestination.translatesAutoresizingMaskIntoConstraints = false
        imgLine.translatesAutoresizingMaskIntoConstraints = false
        imgSource.translatesAutoresizingMaskIntoConstraints = false
        imgVia.translatesAutoresizingMaskIntoConstraints = false
        imgDestination.translatesAutoresizingMaskIntoConstraints = false
        imgDate.translatesAutoresizingMaskIntoConstraints = false
        imgCost.translatesAutoresizingMaskIntoConstraints = false
        threeDot.translatesAutoresizingMaskIntoConstraints = false
        
        imgLine.image = UIImage(named: "gray_separator")
        imgSource.image = UIImage(named: "source")
        imgVia.image = UIImage(named: "Via_green")
        imgDestination.image = UIImage(named: "dest")
        imgDate.image = UIImage(named: "time")
        imgCost.image = UIImage(named: "fare_green-1")
        threeDot.setImage(UIImage(named: "three_dot"), for: UIControlState())
        threeDot.addTarget(self, action: #selector(btn_clicked(_:)), for: .touchDown)
        
        self.contentView.addSubview(profilePic)
        self.contentView.addSubview(infoView)
        self.contentView.addSubview(threeDot)
        profilePic.backgroundColor = UIColor.gray
        
//        verificationView.backgroundColor = Colors.BLACK_COLOR
//        verificationView.alpha = 0.8
//        verificationView.translatesAutoresizingMaskIntoConstraints = false
//        emailIcon.image = UIImage(named: "email")
//        emailIcon.translatesAutoresizingMaskIntoConstraints = false
//        verificationView.addSubview(emailIcon)
//        mobileIcon.image = UIImage(named: "phone")
//        mobileIcon.translatesAutoresizingMaskIntoConstraints = false
//        verificationView.addSubview(mobileIcon)
//        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[email(15)]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
//        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[mobile(15)]", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
//        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[mobile]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
//        verificationView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[email]|", options: [], metrics: nil, views: ["email":emailIcon, "mobile":mobileIcon]))
//        
//        profilePic.addSubview(verificationView)
//        
//        profilePic.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: [], metrics: nil, views: ["view":verificationView]))
//        profilePic.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[view(15)]|", options: [], metrics: nil, views: ["view":verificationView]))
        
        
        lblVerification.backgroundColor = Colors.BLACK_COLOR
        lblVerification.translatesAutoresizingMaskIntoConstraints = false
        lblVerification.alpha = 0.8
        lblVerification.textAlignment = .center
        lblVerification.textColor = Colors.WHITE_COLOR
        lblVerification.font = normalFontWithSize(9)
        lblVerification.isHidden = true
        profilePic.addSubview(lblVerification)
        
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblVerification]|", options: [], metrics: nil, views: ["lblVerification":lblVerification]))
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lblVerification(15)]|", options: [], metrics: nil, views: ["lblVerification":lblVerification]))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[pic(40)][info][threedot(30)]-5-|", options: [], metrics: nil, views: ["pic":profilePic, "info":infoView, "threedot":threeDot]))
        //self.contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-10-[pic(40)][info]-5-|", options: [], metrics: nil, views: ["pic":profilePic, "info":infoView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[info]-40-|", options: [], metrics: nil, views: ["info":infoView]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[pic(40)]-50-|", options: [], metrics: nil, views: ["pic":profilePic]))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[threedot(30)]-20-|", options: [], metrics: nil, views: ["threedot":threeDot]))
        
        infoView.addSubview(imgSource)
        infoView.addSubview(lblSource)
        infoView.addSubview(imgVia)
        infoView.addSubview(lblVia)
        infoView.addSubview(imgDestination)
        infoView.addSubview(lblDestination)
        infoView.addSubview(imgDate)
        infoView.addSubview(lblDateTime)
        infoView.addSubview(imgLine)
        infoView.addSubview(imgCost)
        infoView.addSubview(lblCost)
        
        lblSource.textColor = Colors.GRAY_COLOR
        lblVia.textColor = Colors.GRAY_COLOR
        lblDestination.textColor = Colors.GRAY_COLOR
        lblDateTime.textColor = Colors.ORANGE_COLOR
        lblCost.textColor = Colors.ORANGE_COLOR
        
        let viewsDict = ["source":lblSource, "via":lblVia, "date":lblDateTime, "dateicon":imgDate, "line":imgLine,"icost":imgCost, "cost":lblCost, "dest":lblDestination, "sourceicon":imgSource, "viaicon":imgVia, "desticon":imgDestination]
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[source(20)]-1-[via(20)]-1-[dest(20)]-1-[date(20)]", options: [], metrics: nil, views: viewsDict))
        
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[sourceicon(15)]-5-[source]-5-|", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[viaicon(6)]-10-[via]-5-|", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[desticon(15)]-5-[dest]-5-|", options: [], metrics: nil, views: viewsDict))
        infoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[dateicon(15)]-5-[date(<=150)]-5-[line(1)]-5-[icost(15)]-5-[cost]", options: [], metrics: nil, views: viewsDict))
        
        infoView.addConstraint(NSLayoutConstraint(item: imgSource, attribute: .centerY, relatedBy: .equal, toItem: lblSource, attribute: .centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: imgVia, attribute: .centerY, relatedBy: .equal, toItem: lblVia, attribute: .centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: imgVia, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 20))
        infoView.addConstraint(NSLayoutConstraint(item: imgDestination, attribute: .centerY, relatedBy: .equal, toItem: lblDestination, attribute: .centerY, multiplier: 1, constant: 0))
        
        infoView.addConstraint(NSLayoutConstraint(item: imgDate, attribute: .centerY, relatedBy: .equal, toItem: lblDateTime, attribute: .centerY, multiplier: 1, constant: 0))
        
        infoView.addConstraint(NSLayoutConstraint(item: lblDateTime, attribute: .centerY, relatedBy: .equal, toItem: imgLine, attribute: .centerY, multiplier: 1, constant: 0))
        infoView.addConstraint(NSLayoutConstraint(item: imgLine, attribute: .centerY, relatedBy: .equal, toItem: imgCost, attribute: .centerY, multiplier: 1, constant: 0))
        
        infoView.addConstraint(NSLayoutConstraint(item: imgCost, attribute: .centerY, relatedBy: .equal, toItem: lblCost, attribute: .centerY, multiplier: 1, constant: 0))
        
        imgMembers.translatesAutoresizingMaskIntoConstraints = false
        lblMembersCnt.translatesAutoresizingMaskIntoConstraints = false
        imgLine1.translatesAutoresizingMaskIntoConstraints = false
        seatsIcon.translatesAutoresizingMaskIntoConstraints = false
        seatsIcon.image = UIImage(named: "seat")
        lblSeatsCnt.translatesAutoresizingMaskIntoConstraints = false
        imgLine2.translatesAutoresizingMaskIntoConstraints = false
        vehIcon.translatesAutoresizingMaskIntoConstraints = false
        vehIcon.image = UIImage(named: "veh_car")
        imgMembers.image = UIImage(named: "members_new")
        imgMembers.isUserInteractionEnabled = true
        imgLine1.image = UIImage(named: "gray_separator")
        imgLine2.image = UIImage(named: "gray_separator")
        
        let tapMembersGesture = UITapGestureRecognizer(target: self, action: #selector(showMembersList))
        imgMembers.addGestureRecognizer(tapMembersGesture)
        
        lblMembersCnt.text = "-"
        lblMembersCnt.font = boldFontWithSize(12)
        lblMembersCnt.textColor = Colors.ORANGE_COLOR
        
        lblSeatsCnt.font = boldFontWithSize(12)
        lblSeatsCnt.text = "-"
        lblSeatsCnt.textColor = Colors.ORANGE_COLOR
        
        //lblLine1.backgroundColor = Colors.GRAY_COLOR
        //lblLine2.backgroundColor = Colors.GRAY_COLOR
        
        btn1.translatesAutoresizingMaskIntoConstraints = false
        btn2.translatesAutoresizingMaskIntoConstraints = false
        
        btn1.contentVerticalAlignment = .bottom
        btn2.contentVerticalAlignment = .bottom
        
        btn1.setTitle("btn1", for: UIControlState())
        btn2.setTitle("btn2", for: UIControlState())
        
        btn1.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        btn2.setTitleColor(Colors.GREEN_COLOR, for: UIControlState())
        
        btn1.titleLabel?.font = normalFontWithSize(13)
        btn2.titleLabel?.font = normalFontWithSize(13)
        
        btn1.addTarget(self, action: #selector(btn_clicked(_:)), for: .touchDown)
        btn2.addTarget(self, action: #selector(btn_clicked(_:)), for: .touchDown)
        
        self.contentView.addSubview(imgMembers)
        self.contentView.addSubview(lblMembersCnt)
        self.contentView.addSubview(imgLine1)
        self.contentView.addSubview(seatsIcon)
        self.contentView.addSubview(lblSeatsCnt)
        self.contentView.addSubview(imgLine2)
        self.contentView.addSubview(vehIcon)
        
        self.contentView.addSubview(btn1)
        self.contentView.addSubview(btn2)
        
        let viewsDict1 = ["members":imgMembers, "memberscnt":lblMembersCnt, "line1":imgLine1, "seats":seatsIcon, "seatscnt":lblSeatsCnt, "line2":imgLine2, "car":vehIcon, "btn1":btn1, "btn2":btn2]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[members]-2-[memberscnt]-5-[line1(1)]-4-[seats]-2-[seatscnt]-5-[line2(1)]-4-[car]", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[btn1(100)]-5-[btn2(75)]-5-|", options: [], metrics: nil, views: viewsDict1))
        
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[members]-5-|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[memberscnt]-3-|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line1(15)]-5-|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[seats]-5-|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[seatscnt]-3-|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[line2(15)]-5-|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[car]-2-|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn1(25)]|", options: [], metrics: nil, views: viewsDict1))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn2(25)]|", options: [], metrics: nil, views: viewsDict1))
    }
    
    func buildCell()
    {
        lblVerification.isHidden = false
        verificationView.isHidden = true
        if(data.poolTaxiBooking == "1") {
            lblVerification.text = "TAXI"
        } else {
            lblVerification.text = "POOL"
        }
        lblCost.isHidden = false
        imgCost.isHidden = false
        threeDot.isHidden = true
        seatsIcon.isHidden = false
        lblSeatsCnt.isHidden = false
        vehIcon.isHidden = false
        imgLine.isHidden = false
        imgLine1.isHidden = false
        imgLine2.isHidden = false
        if let vehicleType = data.vehicleType {
            if(vehicleType.lowercased() == "bike") {
                vehIcon.image = UIImage(named: "veh_bike")
            } else if(vehicleType.lowercased() == "auto") {
                vehIcon.image = UIImage(named: "veh_auto")
            } else if(vehicleType.lowercased() == "car") {
                vehIcon.image = UIImage(named: "veh_car")
            } else if(vehicleType.lowercased() == "cab") {
                vehIcon.image = UIImage(named: "veh_cab")
            } else if(vehicleType.lowercased() == "suv") {
                vehIcon.image = UIImage(named: "veh_suv")
            } else if(vehicleType.lowercased() == "tempo") {
                vehIcon.image = UIImage(named: "veh_tempo")
            } else if(vehicleType.lowercased() == "bus") {
                vehIcon.image = UIImage(named: "veh_bus")
            }
        }
        if(data.poolRideType == "N" && data.vehicleRegNo == nil){
            seatsIcon.isHidden = true
            lblSeatsCnt.isHidden = true
            vehIcon.isHidden = true
            imgLine.isHidden = true
            imgLine1.isHidden = true
            imgLine2.isHidden = true
            lblCost.isHidden = true
            imgCost.isHidden = true
        }
        
        if(data.ownerProfilePicStatus == "1"){
            if data.ownerProfilePicUrl != nil &&  data.ownerProfilePicUrl != ""{
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\( data.ownerProfilePicUrl)"))
            }
            else if data.rideOwnerFBID != nil && data.rideOwnerFBID != ""{
                profilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(data.rideOwnerFBID)/picture?type=large&return_ssl_resources=1"))
            } else {
                profilePic.image = UIImage(named: "placeholder")
            }
        }
        else{
            profilePic.image = UIImage(named: "placeholder")
        }
        
        btn1.isHidden = true
        btn2.isHidden = true
        
        lblSource.text = data.source
        
        var nowText = ""
        var combinedString: NSString
        if let via = data.via {
            nowText = via
            combinedString = "\("VIA-") \(nowText)" as NSString
        } else {
            nowText = ""
            combinedString = "" as NSString
        }
        let range = combinedString.range(of: nowText)
        let attributedString = NSMutableAttributedString(string: combinedString as String)
        let attributes = [NSForegroundColorAttributeName: Colors.ORANGE_COLOR]
        attributedString.addAttributes(attributes, range: range)
        self.lblVia.attributedText = attributedString
        
        var poolStartDate = ""
        if let date = data.startDate {
            let dateArray = date.components(separatedBy: " ")
            var currentDate = prettyDateStringFromDate(Date(), toFormat: "dd MMM yyyy")
            if(currentDate.hasPrefix("0")) {
                currentDate.remove(at: currentDate.startIndex)
            }
            let compareResult = currentDate.compare(date)
            if(compareResult == ComparisonResult.orderedSame) {
                poolStartDate = "Today"
            } else {
                poolStartDate = "\(dateArray[0]) \(dateArray[1])"
            }
        }
        if let time = data.startTime {
            lblDateTime.text = "\(poolStartDate) \(time)"
        }
        if(data.poolTaxiBooking == "1"){
            if(data.userCost != nil && data.userCost != "0" && data.userCost != "") {
                lblCost.text = "\(RUPEE_SYMBOL) \(data.userCost!) / \(data.costType!)"
            } else {
                lblCost.text = "\(RUPEE_SYMBOL) \(data.finalCost!) / \(data.costType!)"
            }
        }
        else{
            if let cost = data.cost {
                if(Int(cost) == 0){
                    lblCost.text = "Free of Cost"
                } else {
                    lblCost.text = "\(cost) \("Pts /") \(data.costType!)"
                }
            }
        }
        
        lblDestination.text = data.destination
        
        if let cnt = data.members{
            let cntInt = Int(cnt)!
            if(data.poolTaxiBooking == "1"){
                lblMembersCnt.text = "\(cntInt)"
            }
            else{
                lblMembersCnt.text = "\(cntInt + 1)"
            }
        }
        lblSeatsCnt.text = data.seatsLeft
        
        if(data.poolTaxiBooking == "1"){
            if(data.createdBy == UserInfo.sharedInstance.userID){
                if(data.poolShared != "0") {
                    showInvite()
                }
            }
            else{
                if let poolstatus = data.poolStatus{
                    if(poolstatus == "J"){
                        showInvite()
                    } else if(poolstatus == "S"){
                        showAccept()
                        showReject()
                    }
                    else{
                        showJoin()
                    }
                }
                else{
                    showJoin()
                }
            }
        }
        else{
            if((data.poolRideType == "N") || (data.rideType == "N")){
                if(data.createdBy == UserInfo.sharedInstance.userID){
                    showInvite()
                }
                else{
                    if let poolStatus = data.poolStatus{
                        if let requestedStatus = data.requestedStatus{
                            if(poolStatus == "P" && requestedStatus == "P"){
                                showCancel()
                            }
                            else if(poolStatus == "J"){
                                showInvite()
                            }
                            else if((poolStatus == "U")||(poolStatus == "R")||(poolStatus == "C")||(poolStatus == "P")){
                                showOfferVehicle()
                                showJoin()
                            }
                            else if(poolStatus == "F"){
                                showAccept()
                                showReject()
                            }
                            else{
                                //Nothing
                            }
                        }
                    }
                    else{
                        showOfferVehicle()
                        showJoin()
                    }
                }
            }
            else if((data.poolRideType == "O") || (data.rideType == "O")){
                if(data.createdBy == UserInfo.sharedInstance.userID){
                    showInvite()
                }
                else{
                    if let poolStatus = data.poolStatus{
                        if let requestedStatus = data.requestedStatus{
                            if(poolStatus == "P" && requestedStatus == "P"){
                                showCancel()
                            }
                            else if(poolStatus == "J"){
                                showInvite()
                            }
                            else if((poolStatus == "U")||(poolStatus == "R")||(poolStatus == "C")||(poolStatus == "P")){
                                showJoin()
                            }
                            else if(poolStatus == "F"){
                                showAccept()
                                showReject()
                            }
                            else{
                                //Nothing
                            }
                        }
                    }
                    else{
                        showJoin()
                    }
                }
            }
        }
        
        setNeedsLayout()
    }
    
    func btn_clicked(_ btn: UIButton){
        if let str = btn.titleLabel?.text {
            print("btn str",str)
            if(str == "INVITE"){
                onUserActionBlock?("invite" as AnyObject?)
            } else if(str == "JOIN RIDE"){
                onUserActionBlock?("joinride" as AnyObject?)
            } else if(str == "CANCEL"){
                onUserActionBlock?("cancel" as AnyObject?)
            } else if(str == "REJECT"){
                onUserActionBlock?("reject" as AnyObject?)
            } else if(str == "ACCEPT"){
                onUserActionBlock?("accept" as AnyObject?)
            } else if(str == "OFFER A VEHICLE"){
                onUserActionBlock?("offeravehicle" as AnyObject?)
            } else if(str == "UNJOIN RIDE"){
                onUserActionBlock?("unjoinride" as AnyObject?)
            }
        } else {
            onUserActionBlock?("threedot" as AnyObject?)
        }
    }
    
    
    func showMembersList()
    {
        if((data.poolStatus != nil && data.poolStatus == "J") || (data.createdBy == UserInfo.sharedInstance.userID)){
            //gotoMemberListVC
            onUserActionBlock?("showmembers" as AnyObject?)
        }
        else{
            AlertController.showToastForError("Please join the ride to see the members.")
        }
    }
    //invite, joinride, cancel, reject, accept, offeravehicle
    
    func showInvite()
    {
        btn2.isHidden = false
        btn2.setTitle("INVITE", for: UIControlState())
    }
    
    func showJoin()
    {
        btn2.isHidden = false
        btn2.setTitle("JOIN RIDE", for: UIControlState())
    }
    
    func showCancel()
    {
        btn2.isHidden = false
        btn2.setTitle("CANCEL", for: UIControlState())
    }
    
    func showAccept()
    {
        btn2.isHidden = false
        btn2.setTitle("REJECT", for: UIControlState())
    }
    func showReject()
    {
        btn1.isHidden = false
        btn1.setTitle("ACCEPT", for: UIControlState())
    }
    
    func showOfferVehicle()
    {
        btn1.isHidden = false
        btn1.setTitle("OFFER A VEHICLE", for: UIControlState())
    }
    
    func showUnjoin()
    {
        btn2.isHidden = false
        btn2.setTitle("UNJOIN RIDE", for: UIControlState())
    }
}
