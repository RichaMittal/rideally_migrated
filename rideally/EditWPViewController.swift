//
//  EditWPViewController.swift
//  rideally
//
//  Created by Sarav on 15/01/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class EditWPViewController: UIViewController, UITextFieldDelegate {
    
    var data: WorkPlace?
    let profilePic = UIImageView()
    
    func goBack()
    {
        _ = self.navigationController?.popViewController(animated: true)
    }
    func showPlaceholderPic()
    {
        if(data?.wpScope == "1" || data?.wpTieUpStatus == "1"){
            profilePic.image = UIImage(named: "wpPlaceholderL")
        }
        else{
            profilePic.image = UIImage(named: "wpPlaceholder")
        }
    }
    
    let lblWPURL = UILabel()
    let txtWPUID = UITextField()
    let txtDesc = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Colors.APP_BG
        
        title = "Update Workplace"
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        
        if let url = data?.wpLogoURL{
            if(url != ""){
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
            }
            else{
                showPlaceholderPic()
            }
        }
        else{
            showPlaceholderPic()
        }
        self.view.addSubview(profilePic)
        
        let lblWPname = UILabel()
        lblWPname.translatesAutoresizingMaskIntoConstraints = false
        lblWPname.text = data?.wpName
        if(data?.wpScope == "1" || data?.wpTieUpStatus == "1"){
            lblWPname.text = "\((data?.wpName)!)(Private)"
        }
        lblWPname.font = boldFontWithSize(15)
        lblWPname.textColor = Colors.GRAY_COLOR
        lblWPname.textAlignment = .center
        self.view.addSubview(lblWPname)
        
        let lblLine1 = getLineLabel()
        self.view.addSubview(lblLine1)
        
        let locView = UIView()
        locView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(locView)
        let iconSource = UIImageView()
        iconSource.image = UIImage(named: "source")
        iconSource.translatesAutoresizingMaskIntoConstraints = false
        locView.addSubview(iconSource)
        let lblLocation = UILabel()
        lblLocation.textColor = Colors.GRAY_COLOR
        lblLocation.text = data?.wpLocation != nil ? data?.wpLocation : data?.wpDetailLocation
        lblLocation.font = boldFontWithSize(14)
        lblLocation.numberOfLines = 0
        lblLocation.lineBreakMode = .byWordWrapping
        lblLocation.translatesAutoresizingMaskIntoConstraints = false
        locView.addSubview(lblLocation)
        
        let iconMap = UIImageView()
        iconMap.image = UIImage(named: "map")
        iconMap.translatesAutoresizingMaskIntoConstraints = false
        locView.addSubview(iconMap)
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(showDirection))
        iconMap.isUserInteractionEnabled = true
        iconMap.addGestureRecognizer(tapGesture1)
        
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[iS(15)]-5-[lbl]-5-[iM(30)]|", options: [], metrics: nil, views: ["iS":iconSource, "lbl":lblLocation, "iM":iconMap]))
        
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[iS(15)]", options: [], metrics: nil, views: ["iS":iconSource, "lbl":lblLocation, "iM":iconMap]))
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[iM(30)]", options: [], metrics: nil, views: ["iS":iconSource, "lbl":lblLocation, "iM":iconMap]))
        locView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[lbl]-5-|", options: [], metrics: nil, views: ["iS":
            iconSource, "lbl":lblLocation, "iM":iconMap]))
        locView.addConstraint(NSLayoutConstraint(item: iconSource, attribute: .centerY, relatedBy: .equal, toItem: lblLocation, attribute: .centerY, multiplier: 1, constant: 0))
        
        let lblLine2 = getLineLabel()
        self.view.addSubview(lblLine2)
        
        let urlView = UIView()
        urlView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(urlView)
        
        let lblURLTitle = UILabel()
        lblURLTitle.text = "WORKPLACE URL"
        lblURLTitle.font = boldFontWithSize(13)
        lblURLTitle.textColor = Colors.GRAY_COLOR
        lblURLTitle.translatesAutoresizingMaskIntoConstraints = false
        urlView.addSubview(lblURLTitle)
        
        lblWPURL.textColor = Colors.GRAY_COLOR
        lblWPURL.text = WORKPLACE_URL_BASE + (data?.groupKey)!
        lblWPURL.font = boldFontWithSize(15)
        lblWPURL.translatesAutoresizingMaskIntoConstraints = false
        urlView.addSubview(lblWPURL)
        
        if(data?.wpInvitedMembersCount == "0" && data?.wpPendingMembersCount == "0" && data?.wpMembers == "1") {
            txtWPUID.textColor = Colors.GRAY_COLOR
            txtWPUID.placeholder = "Enter any Unique Id"
            txtWPUID.text = data?.groupKey
            txtWPUID.font = boldFontWithSize(15)
            txtWPUID.translatesAutoresizingMaskIntoConstraints = false
            txtWPUID.delegate = self
            urlView.addSubview(txtWPUID)
            urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title(20)][lbl(20)][txt(40)]|", options: [], metrics: nil, views: ["title":lblURLTitle, "txt":txtWPUID, "lbl":lblWPURL]))
            urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: ["title":lblURLTitle, "txt":txtWPUID, "lbl":lblWPURL]))
            urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["title":lblURLTitle, "txt":txtWPUID, "lbl":lblWPURL]))
            urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[txt]|", options: [], metrics: nil, views: ["title":lblURLTitle, "txt":txtWPUID, "lbl":lblWPURL]))
        } else {
            urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title(20)][lbl(20)]|", options: [], metrics: nil, views: ["title":lblURLTitle, "lbl":lblWPURL]))
            urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: ["title":lblURLTitle, "lbl":lblWPURL]))
            urlView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lbl]|", options: [], metrics: nil, views: ["title":lblURLTitle, "lbl":lblWPURL]))
        }
        
        
        let lblLine3 = getLineLabel()
        self.view.addSubview(lblLine3)
        
        let descView = UIView()
        descView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(descView)
        let txtDesctitle = UILabel()
        txtDesctitle.translatesAutoresizingMaskIntoConstraints = false
        txtDesctitle.text = "WORKPLACE DESCRIPTION"
        txtDesctitle.font = boldFontWithSize(13)
        txtDesctitle.textColor = Colors.GRAY_COLOR
        descView.addSubview(txtDesctitle)
        txtDesc.translatesAutoresizingMaskIntoConstraints = false
        txtDesc.text = data?.wpDesc
        txtDesc.font = boldFontWithSize(14)
        txtDesc.textColor = Colors.BLACK_COLOR
        descView.addSubview(txtDesc)
        descView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[title(25)][desc]|", options: [], metrics: nil, views: ["title":txtDesctitle, "desc":txtDesc]))
        descView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[title]|", options: [], metrics: nil, views: ["title":txtDesctitle]))
        descView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[desc]|", options: [], metrics: nil, views: ["desc":txtDesc]))
        
        let lblLine4 = getLineLabel()
        self.view.addSubview(lblLine4)
        
        let viewsDict = ["imgV":profilePic, "lblname":lblWPname, "l1":lblLine1, "locV":locView, "l2":lblLine2, "descV":descView, "l3":lblLine3, "urlV":urlView, "l4":lblLine4]
        if(data?.wpInvitedMembersCount == "0" && data?.wpPendingMembersCount == "0" && data?.wpMembers == "1") {
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[imgV(100)]-10-[lblname(30)]-10-[l1(0.5)]-5-[locV(40)]-5-[l2(0.5)]-5-[urlV(80)]-5-[l3(0.5)]-5-[descV(50)]-5-[l4(0.5)]", options: [], metrics: nil, views: viewsDict))
        } else {
            self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-20-[imgV(100)]-10-[lblname(30)]-10-[l1(0.5)]-5-[locV(40)]-5-[l2(0.5)]-5-[urlV(45)]-5-[l3(0.5)]-5-[descV(50)]-5-[l4(0.5)]", options: [], metrics: nil, views: viewsDict))
        }
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[imgV(100)]", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lblname]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraint(NSLayoutConstraint(item: profilePic, attribute: .centerX, relatedBy: .equal, toItem: lblWPname, attribute: .centerX, multiplier: 1, constant: 0))
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l1]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[locV]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l2]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[urlV]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l3]|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[descV]-5-|", options: [], metrics: nil, views: viewsDict))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[l4]|", options: [], metrics: nil, views: viewsDict))
        
        txtWPUID.returnKeyType = .next
        txtDesc.delegate = self
        txtDesc.returnKeyType = .done
        
        let btnSave = UIButton()
        btnSave.translatesAutoresizingMaskIntoConstraints = false
        btnSave.backgroundColor = Colors.GRAY_COLOR
        btnSave.setTitle("SUBMIT", for: UIControlState())
        btnSave.addTarget(self, action: #selector(saveDetails), for: .touchDown)
        self.view.addSubview(btnSave)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btnSave]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[btn(40)]|", options: [], metrics: nil, views: ["btn":btnSave]))
    }
    
    func showDirection()
    {
        let vc = WPMapViewController()
        vc.data = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func saveDetails()
    {
        if(data?.wpInvitedMembersCount == "0" && data?.wpPendingMembersCount == "0" && data?.wpMembers == "1" && txtWPUID.text == ""){
            AlertController.showAlertFor("Update Workplace", message: "Workplace unique Id cannot be empty.", okAction: nil)
        }
        
        if(txtDesc.text != data?.wpDesc || (data?.wpInvitedMembersCount == "0" && data?.wpPendingMembersCount == "0" && data?.wpMembers == "1" && txtWPUID.text != data?.groupKey)) {
            let reqObj = UpdateWPRequest()
            reqObj.officeDesc = txtDesc.text
            if(data?.wpInvitedMembersCount == "0" && data?.wpPendingMembersCount == "0" && data?.wpMembers == "1") {
                reqObj.officeKey = txtWPUID.text
            } else {
                reqObj.officeKey = data?.groupKey
            }
            reqObj.userID = UserInfo.sharedInstance.userID
            reqObj.officeID = data?.wpID
            reqObj.officeName = data?.wpName
            reqObj.officeScope = data?.wpScope
            reqObj.officeType = "6"
            showIndicator("Updating..")
            WorkplaceRequestor().updateWorkPlace(reqObj, success: { (success, object) in
                hideIndicator()
                if(success){
                    AlertController.showAlertFor("Update Workplace", message: "Workplace has been updated successfully.", okAction: {
                        //TODO: Need to refresh Wp list, call api again to get wp
                        _ = self.navigationController?.popToRootViewController(animated: true)
                         NotificationCenter.default.post(name: Notification.Name(rawValue: "updateWpsList"), object: nil)
                    })
                }
                else{
                    AlertController.showAlertFor("Update Workplace", message: "Workplace updation failed. Please try again.", okAction: nil)
                }
            }) { (error) in
                hideIndicator()
            }
        }else{
            AlertController.showToastForError("Sorry, there are no changes to your workplace.")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(data?.wpInvitedMembersCount == "0" && data?.wpPendingMembersCount == "0" && data?.wpMembers == "1") {
            if(textField == txtWPUID){
                txtDesc.becomeFirstResponder()
            }
            else if(textField == txtDesc){
                txtDesc.resignFirstResponder()
            }
        } else {
            if(textField == txtDesc){
                txtDesc.resignFirstResponder()
            }
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == txtWPUID){
            let text: NSString = (textField.text ?? "") as NSString
            let resultString = text.replacingCharacters(in: range, with: string)
            updateURL(resultString)
        }
        return true
    }
    
    func updateURL(_ url: String)
    {
        lblWPURL.text = WORKPLACE_URL_BASE+"\(url)"
    }
    
    func getLineLabel() -> UILabel
    {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.backgroundColor = Colors.GENERAL_BORDER_COLOR
        return lbl
    }
}
