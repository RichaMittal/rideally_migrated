//
//  InviteGroupMembersViewController.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 6/15/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class GroupMemberCell: UITableViewCell {
    var data: WPMember!{
        didSet{
            buildCell()
        }
    }
    var wpDetails: WorkPlace?
    var onUserActionBlock: actionBlockWithParams?
    
    let profilePic = UIImageView()
    let lblName = UILabel()
    let lblDistance = UILabel()
    let lblRating = UILabel()
    let imgRating = UIImageView()
    
    let verificationView = UIView()
    let adminIcon = UIImageView()
    let checkBox = UIButton(type: .custom)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    func commonInit()
    {
        self.backgroundColor = UIColor.clear
        self.clipsToBounds = true
        self.selectionStyle = .none
        
        profilePic.layer.cornerRadius = 20
        profilePic.layer.masksToBounds = true
        profilePic.translatesAutoresizingMaskIntoConstraints = false
        profilePic.backgroundColor = UIColor.gray
        profilePic.isUserInteractionEnabled = true
        self.contentView.addSubview(profilePic)
        
        verificationView.backgroundColor = Colors.BLACK_COLOR
        verificationView.alpha = 0.8
        verificationView.translatesAutoresizingMaskIntoConstraints = false
        adminIcon.translatesAutoresizingMaskIntoConstraints = false
        verificationView.addSubview(adminIcon)
        
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[admin]|", options: [], metrics: nil, views: ["admin":adminIcon]))
        verificationView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[admin(15)]", options: [], metrics: nil, views: ["admin":adminIcon]))
        verificationView.addConstraint(NSLayoutConstraint(item: adminIcon, attribute: .centerX, relatedBy: .equal, toItem: verificationView, attribute: .centerX, multiplier: 1, constant: 0))
        
        profilePic.addSubview(verificationView)
        
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view":verificationView]))
        profilePic.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[view(15)]|", options: [], metrics: nil, views: ["view":verificationView]))
        
        lblName.translatesAutoresizingMaskIntoConstraints = false
        lblName.font = boldFontWithSize(15)
        lblName.textColor = Colors.GRAY_COLOR
        self.contentView.addSubview(lblName)
        
        lblDistance.translatesAutoresizingMaskIntoConstraints = false
        lblDistance.font = boldFontWithSize(10)
        lblDistance.textColor = Colors.ORANGE_TEXT_COLOR
        lblDistance.isHidden = true
        self.contentView.addSubview(lblDistance)
        
        checkBox.addTarget(self, action: #selector(checkBoxPressed(_:)), for: .touchUpInside)
        checkBox.setImage(UIImage(named: "Checkbox_unselected"), for: .normal)
        checkBox.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(checkBox)
        
        lblRating.translatesAutoresizingMaskIntoConstraints = false
        lblRating.font = boldFontWithSize(12)
        lblRating.textColor = Colors.GRAY_COLOR
        lblRating.isHidden = true
        self.contentView.addSubview(lblRating)
        
        imgRating.image = UIImage(named: "Star_Filled")
        imgRating.translatesAutoresizingMaskIntoConstraints = false
        imgRating.isHidden = true
        self.contentView.addSubview(imgRating)
        
        let viewsDict = ["pic":profilePic, "name":lblName, "distance":lblDistance, "rating":lblRating, "irating":imgRating, "checkBox":checkBox]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[pic(40)]", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[name]-5-[distance]-5-[rating]", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[pic(40)]-2-[name]-5-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[distance][checkBox(15)]-15-|", options: [], metrics: nil, views: viewsDict))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-75-[rating]-2-[irating]", options: [], metrics: nil, views: viewsDict))
        
        self.contentView.addConstraint(NSLayoutConstraint(item: lblDistance, attribute: .left, relatedBy: .equal, toItem: lblName, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblRating, attribute: .left, relatedBy: .equal, toItem: lblName, attribute: .left, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: imgRating, attribute: .centerY, relatedBy: .equal, toItem: lblRating, attribute: .centerY, multiplier: 1, constant: 0))
        self.contentView.addConstraint(NSLayoutConstraint(item: checkBox, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 15))
        self.contentView.addConstraint(NSLayoutConstraint(item: lblDistance, attribute: .centerY, relatedBy: .equal, toItem: checkBox, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    func buildCell()
    {
        adminIcon.isHidden = true
        verificationView.isHidden = true
        if(data.membershipType == "1"){
            verificationView.isHidden = false
            adminIcon.isHidden = false
            adminIcon.image = UIImage(named: "Admin")
        }
        
        var name = ""
        if let fname = data.fname{
            if(fname != "") {
                name = fname
                name.replaceSubrange(name.startIndex...name.startIndex, with: String(name[name.startIndex]).capitalized)
                //name.replaceRange(name.startIndex...name.startIndex, with: String(name[name.startIndex]).capitalizedString)
            }
        }
        if let lname = data.lname{
            if(lname != "") {
                var lastName = lname
                lastName.replaceSubrange(lastName.startIndex...lastName.startIndex, with: String(lastName[name.startIndex]).capitalized)
                //lastName.replaceRange(lastName.startIndex...lastName.startIndex, with: String(lastName[lastName.startIndex]).capitalizedString)
                name = "\(name) \(lastName)"
            }
        }
        lblName.text = name
        
        if(data.memberDistance != nil && data.memberDistance != "") {
            lblDistance.isHidden = false
            if(data.memberDistance == "0") {
                lblDistance.text = "\(data.memberDistance!) \("KM")"
            } else {
                var splitvalue = data.memberDistance?.components(separatedBy: ".")
                if(splitvalue![1] == "00") {
                    lblDistance.text = "\(splitvalue![0]) \("KM")"
                } else {
                    let numberOfPlaces = 2.0
                    let multiplier = pow(10.0, numberOfPlaces)
                    lblDistance.text = "\(String(round(Double(data.memberDistance!)! * multiplier) / multiplier)) \("KM")"
                }
            }
        } else {
            lblDistance.isHidden = true
        }
        
        if(data.avgRating != "" && data.avgRating != nil) {
            lblRating.isHidden = false
            imgRating.isHidden = false
            var splitvalue = data.avgRating?.components(separatedBy: ".")
            if(splitvalue![1] == "00") {
                lblRating.text = splitvalue![0]
            } else {
                let numberOfPlaces = 1.0
                let multiplier = pow(10.0, numberOfPlaces)
                lblRating.text = String(round(Double(data.avgRating!)! * multiplier) / multiplier)
            }
        } else {
            lblRating.isHidden = true
            imgRating.isHidden = true
        }
        
        if(data?.profilePicStatus == "1"){
            if let url = data?.profilePicUrl{
                profilePic.kf.setImage(with: URL(string: "\(URLS.IMG_URL)\(url)"))
            }
            else{
                if let fbID = data.fbID{
                    profilePic.kf.setImage(with: URL(string: "https://graph.facebook.com/\(fbID)/picture?type=large&return_ssl_resources=1"))
                }
            }
        }
        else{
            profilePic.image = UIImage(named: "placeholder")
        }
        
        setNeedsLayout()
    }
 
    func checkBoxPressed(_ sender: UIButton) -> Void {
        onUserActionBlock?(!sender.isSelected, data.mobile)
        
        sender.isSelected = !sender.isSelected
        
        checkBox.setImage(UIImage(named: "Checkbox_unselected"), for: .normal)
        
        if  sender.isSelected == true
        {
            checkBox.setImage(UIImage(named: "CheckBox_selected"), for: .normal)
            //phoneMutArr.addObject(data.mobile!)
            //saveDefaultVehicleDetails()
            //self.cellDelegate?.isDefaultButtonPressedEventCalled()
        }
    }
}

class InviteGroupMembersViewController: BaseScrollViewController, UITableViewDelegate, UITableViewDataSource {
    
    let table = UITableView()
    let responseTableView = UITableView()
    var membersList: [WPMember]?
    var joinedMembersList: NSDictionary?
    var wpDetails: WorkPlace?
    let rowHt: CGFloat = 70
    var mobileMutArr : NSMutableArray = []
    let btnSubmit = UIButton()
    var wpresponses: [InviteWPResult]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.APP_BG
        title = "\(wpDetails?.wpName ?? "") \("Members")"
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(goBack))
        var index = 0
        for member in membersList!{
            if(member.membershipStatus != "1" && member.membershipStatus != "0"){
                membersList?.remove(at: index)
            }
            else
            {
                index += 1
            }
        }
        viewsArray.append(getTableView())
        addBottomView(getBottomView())
    }
    
    func getTableView() ->SubView {
        let view = UIView()
        
        table.translatesAutoresizingMaskIntoConstraints = false
        table.delegate = self
        table.dataSource = self
        table.register(GroupMemberCell.self, forCellReuseIdentifier: "groupmembercell")
        //table.separatorColor = UIColor.clearColor()
        table.separatorColor = Colors.GRAY_COLOR
        table.separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 15)
        table.tableFooterView = UIView()
        view.addSubview(table)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":table]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[table]-5-|", options: [], metrics: nil, views: ["table":table]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: CGFloat(membersList!.count)*rowHt)
        return sub
    }
    
    func getBottomView() -> SubView
    {
        let view = UIView()
        
        btnSubmit.setTitle("SUBMIT", for: .normal)
        btnSubmit.backgroundColor = Colors.GREEN_COLOR
        btnSubmit.translatesAutoresizingMaskIntoConstraints = false
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: .normal)
        btnSubmit.setTitleColor(Colors.WHITE_COLOR, for: .highlighted)
        btnSubmit.titleLabel?.font = boldFontWithSize(16)
        btnSubmit.isHidden = true
        btnSubmit.addTarget(self, action: #selector(submitClicked), for: .touchDown)
        view.addSubview(btnSubmit)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[btn(40)]", options: [], metrics: nil, views: ["btn":btnSubmit]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[btn]|", options: [], metrics: nil, views: ["btn":btnSubmit]))
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
        return sub
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == responseTableView) {
            return wpresponses!.count
        } else {
            return membersList!.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(tableView == responseTableView) {
            return UITableViewAutomaticDimension
        } else {
            return rowHt
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == responseTableView {
            let cell = UITableViewCell()
            cell.textLabel?.font = normalFontWithSize(14)
            cell.textLabel?.numberOfLines = 0
            if (wpresponses![indexPath.row].type == "mobile"){
                cell.textLabel?.text = "\(wpresponses![indexPath.row].mobile!) - \(wpresponses![indexPath.row].result!)"
            }else{
                cell.textLabel?.text = "\(wpresponses![indexPath.row].email!) - \(wpresponses![indexPath.row].result!)"
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "groupmembercell") as! GroupMemberCell
            cell.wpDetails = wpDetails
            cell.data = membersList![indexPath.row]
            cell.onUserActionBlock = { (value, userID) -> Void in
                self.performAction(action: value as! Bool, indexPath: indexPath as NSIndexPath)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showIndicator("Getting member's detail..")
        RideRequestor().getMembersDetails((membersList![indexPath.row]).uID!, success: { (success, object) in
            hideIndicator()
            if(success == true){
                let vc = MemberDetailViewController()
                vc.dataSource = (object as? MemberDetailResponse)?.data?.userInfo
                vc.hidesBottomBarWhenPushed = true
                vc.edgesForExtendedLayout = UIRectEdge()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            hideIndicator()
        }
    }
    
    func performAction(action: Bool, indexPath: NSIndexPath){
        let selectedData = membersList![indexPath.row]
        if(action == true) {
            mobileMutArr.add(selectedData.mobile!)
        } else {
            mobileMutArr.remove(selectedData.mobile!)
        }
        
        if(mobileMutArr.count != 0) {
            btnSubmit.isHidden = false
        } else {
            btnSubmit.isHidden = true
        }
    }
    
    func submitClicked() {
        let stringRepresentation = self.mobileMutArr.componentsJoined(by: ",")
        sendRequest(phoneNumber: stringRepresentation, poolId: (wpDetails?.wpID)!)
    }
    
    func sendRequest(phoneNumber: String, poolId : String) {
        showIndicator("Sending Invites..")
        WorkplaceRequestor().sendWPInvite(poolId, userId: UserInfo.sharedInstance.userID, emailIDs: "", mobileNOs: phoneNumber, success: { (success, object) in
            hideIndicator()
            
            if(success){
                self.wpresponses = (object as! InviteMembersWPResponse).data
                //self.dismissViewControllerAnimated(true, completion: nil)
                let view = self.getInviteResposeView()
                (view.viewWithTag(33) as? UITableView)?.reloadData()
                
                let alertViewHolder =   AlertContentViewHolder()
                alertViewHolder.heightConstraintValue = 100
                alertViewHolder.view = view
                
                AlertController.showAlertFor("Invite Members", message: "", contentView: alertViewHolder, okButtonTitle: "Ok", willHaveAutoDismiss: true, okAction: {
                    _ = self.navigationController?.popViewController(animated: true)
                    //self.onCompletion?()
                }, cancelButtonTitle: nil, cancelAction: nil)
            }
            
            
        }) { (error) in
            hideIndicator()
        }
    }
    
    func getInviteResposeView() -> UIView
    {
        let view = UIView()
        view.backgroundColor = Colors.WHITE_COLOR
        
        responseTableView.delegate = self
        responseTableView.tag = 33
        responseTableView.dataSource = self
        responseTableView.translatesAutoresizingMaskIntoConstraints = false
        responseTableView.separatorColor = UIColor.clear
        view.addSubview(responseTableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[table(100)]|", options: [], metrics: nil, views: ["table":responseTableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[table]|", options: [], metrics: nil, views: ["table":responseTableView]))
        return view
    }
}
