//
//  InviteNormalRide.swift
//  rideally
//
//  Created by Richa Anurag Mittal on 6/20/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class InviteNormalRideView: UIView {
    let messageBtn =   UIButton()
    let whatsAppBtn =   UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createInviteNormalRideView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    func createInviteNormalRideView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = Colors.WHITE_COLOR
        
        let lblInfo = UILabel()
        lblInfo.text = "Would you like to invite"
        lblInfo.numberOfLines = 0
        lblInfo.lineBreakMode = .byWordWrapping
        lblInfo.translatesAutoresizingMaskIntoConstraints = false
        lblInfo.font = normalFontWithSize(15)
        lblInfo.textAlignment = .center
        self.addSubview(lblInfo)
        
        messageBtn.setImage(UIImage(named:"message"), for:UIControlState.normal)
        messageBtn.translatesAutoresizingMaskIntoConstraints = false
        //messageBtn.addTarget(self, action: #selector(message_clicked), forControlEvents: .TouchDown)
        self.addSubview(messageBtn)
        
        whatsAppBtn.setImage(UIImage(named:"whatsapp"), for:UIControlState.normal)
        whatsAppBtn.translatesAutoresizingMaskIntoConstraints = false
        //whatsAppBtn.addTarget(self, action: #selector(whasApp_clicked), forControlEvents: .TouchDown)
        self.addSubview(whatsAppBtn)
        
        let messageLabel = UILabel()
        messageLabel.font = normalFontWithSize(14)
        messageLabel.backgroundColor = UIColor.clear
        messageLabel.textColor = Colors.GRAY_COLOR
        messageLabel.text="MESSAGE"
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(messageLabel)
        
        let whatsAppLabel = UILabel()
        whatsAppLabel.font = normalFontWithSize(14)
        whatsAppLabel.backgroundColor = UIColor.clear
        whatsAppLabel.textColor = Colors.GRAY_COLOR
        whatsAppLabel.text="WHATSAPP"
        whatsAppLabel.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(whatsAppLabel)
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[lbl]-10-[msgBtn(30)]-10-[msglbl(15)]|", options: [], metrics: nil, views: ["lbl":lblInfo, "msgBtn": messageBtn, "msglbl": messageLabel]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[lbl]-10-|", options: [], metrics: nil, views: ["lbl":lblInfo]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[msgBtn][whatsAppBtn(==msgBtn)]-10-|", options: [], metrics: nil, views: ["msgBtn": messageBtn, "whatsAppBtn": whatsAppBtn]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-45-[msglbl]-20-[whatsAppLbl(==msglbl)]-10-|", options: [], metrics: nil, views: ["msglbl": messageLabel, "whatsAppLbl": whatsAppLabel]))
        
        self.addConstraint(NSLayoutConstraint(item: whatsAppBtn, attribute: .centerY, relatedBy: .equal, toItem: messageBtn, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: whatsAppBtn, attribute: .height, relatedBy: .equal, toItem: messageBtn, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: whatsAppLabel, attribute: .centerY, relatedBy: .equal, toItem: messageLabel, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: whatsAppLabel, attribute: .height, relatedBy: .equal, toItem: messageLabel, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
    }
}
