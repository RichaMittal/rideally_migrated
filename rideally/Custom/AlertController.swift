//
//  AlertController.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import UIKit

class AlertController: NSObject {
    
    class func showAlertForError (_ error: NSError?) {
        var message: String?
        if error != nil {
            message = error?.userInfo[""] as? String
        } else {
            message = ERROR_MESSAGE_STRING
        }
        
        showAlertForMessage(message)
    }
    
    class func showToastForError (_ message: String) {
        ToastView.setDefaultValue(Colors.RED_COLOR, forAttributeName: ToastViewBackgroundColorAttributeName, userInterfaceIdiom: .phone)
        ToastView.setDefaultValue(60.0 as AnyObject, forAttributeName: ToastViewPortraitOffsetYAttributeName, userInterfaceIdiom: .phone)
        Toast.makeText(message).show()
    }
    
    class func showToastForInfo (_ message: String) {
        ToastView.setDefaultValue(Colors.GREEN_COLOR, forAttributeName: ToastViewBackgroundColorAttributeName, userInterfaceIdiom: .phone)
        ToastView.setDefaultValue(60.0 as AnyObject, forAttributeName: ToastViewPortraitOffsetYAttributeName, userInterfaceIdiom: .phone)
        Toast.makeText(message).show()
    }
    
    class func showAlertForMessage (_ message: String?) {
        showAlertFor(nil, message: message)
    }
    
    class func showAlertFor (_ title: String?, message: String?) {
        showAlertFor(title, message: message, okButtonTitle: "Ok", okAction: nil)
    }
    
    class func showAlertFor (_ title: String?, message: String?, okAction: (()->Void)?) {
        showAlertFor(title, message: message, okButtonTitle: "OK", okAction: okAction)
    }
    
    class func showAlertFor (_ title: String?, message: String?, okButtonTitle: String?, okAction: (()->Void)?) {
        showAlertFor(title, message: message, okButtonTitle: okButtonTitle, okAction: okAction, cancelButtonTitle: nil, cancelAction: nil)
    }
    
    class func showAlertFor (_ title: String?, message: String?, okButtonTitle: String?, okAction: (()->Void)?, cancelButtonTitle: String?, cancelAction: (()->Void)?) {
        
        let alertVC = alertForTitle(title, message: message, okButtonTitle: okButtonTitle, willHaveAutoDismiss:nil, okAction: okAction, cancelButtonTitle: cancelButtonTitle, cancelAction: cancelAction)
        
        presentAlertVC(alertVC)
    }
    
    class func showAlertFor (_ title: String?, message: String?, contentView: AlertContentViewHolder?, okButtonTitle: String?, willHaveAutoDismiss: Bool?, okAction: (()->Void)?, cancelButtonTitle: String?, cancelAction: (()->Void)?) {
        
        let alertVC = alertForTitle(title, message: message, okButtonTitle: okButtonTitle, willHaveAutoDismiss:willHaveAutoDismiss, okAction: okAction, cancelButtonTitle: cancelButtonTitle, cancelAction: cancelAction)
        
        if contentView != nil {
            
            let holderView = UIView()
            holderView.backgroundColor = UIColor.clear
            
            let view = contentView!.view
            let padding = contentView!.padding
            var width = contentView!.widthConstraintValue
            width = width == 0 ? UIScreen.main.bounds.size.width*0.85 : width
            let height = contentView!.heightConstraintValue
            
            view?.translatesAutoresizingMaskIntoConstraints = false
            let dict = ["view": view]
            holderView.addSubview(view!)
            
            holderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(padding.left))-[view(\(width))]-(\(padding.right))-|", options: [], metrics: nil, views: dict))
            holderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(padding.top))-[view]-(\(padding.bottom))-|", options: [], metrics: nil, views: dict))
            holderView.addConstraint(NSLayoutConstraint(item: view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: height))
            
            
            alertVC.maximumWidth = padding.left + padding.right + width
            alertVC.alertViewContentView = view
            alertVC.alertViewBackgroundColor = view?.backgroundColor
        }
        
        presentAlertVC(alertVC)
    }
    
    class func showAlertFor (_ title: String?, message: String?, contentView: AlertContentViewHolder?, okButtonTitle: String?, willHaveAutoDismiss: Bool?, okAction: (()->Void)?) {
        
        let alertVC = alertForTitle(title, message: message, okButtonTitle: okButtonTitle, willHaveAutoDismiss:willHaveAutoDismiss, okAction: okAction, cancelButtonTitle: nil, cancelAction: nil)
        
        if contentView != nil {
            
            let holderView = UIView()
            holderView.backgroundColor = UIColor.clear
            
            let view = contentView!.view
            let padding = contentView!.padding
            var width = contentView!.widthConstraintValue
            width = width == 0 ? UIScreen.main.bounds.size.width*0.85 : width
            let height = contentView!.heightConstraintValue
            
            view?.translatesAutoresizingMaskIntoConstraints = false
            let dict = ["view": view]
            holderView.addSubview(view!)
            
            holderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(padding.left))-[view(\(width))]-(\(padding.right))-|", options: [], metrics: nil, views: dict))
            holderView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(padding.top))-[view]-(\(padding.bottom))-|", options: [], metrics: nil, views: dict))
            holderView.addConstraint(NSLayoutConstraint(item: view!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: height))
            
            
            alertVC.maximumWidth = padding.left + padding.right + width
            alertVC.alertViewContentView = view
            alertVC.alertViewBackgroundColor = view?.backgroundColor
        }
        
        presentAlertVC(alertVC)
    }

    class func presentAlertVC (_ alertVC: UIViewController) {
        guard let viewController = topMostController() else {
            return
        }
        
        if viewController.isMember(of: NYAlertViewController.self) {
            return
        }
        
        viewController.present(alertVC, animated: true, completion: nil)
    }
    
    
    class func showAlertForAttributedText (_ message: NSAttributedString, title: String?) {
        let alertVC = alertForTitle(title, message: "", okButtonTitle: "Ok", willHaveAutoDismiss:true, okAction: nil, cancelButtonTitle: nil, cancelAction: nil)
        
        let alertView = alertVC.view as? NYAlertView
        
        alertView?.messageTextView.attributedText = message
        alertVC.maximumWidth = 0.95*UIScreen.main.bounds.size.width
        alertVC.messageFont = normalFontWithSize(13)
        
        presentAlertVC(alertVC)
    }
    
    
    //MARK:- Alert Constructs
    class func alertForTitle (_ title: String?, message: String?, okButtonTitle: String?, willHaveAutoDismiss: Bool?, okAction: (()->Void)?, cancelButtonTitle: String?, cancelAction: (()->Void)?) -> NYAlertViewController {
        
        let alertVC = AlertController.alertForTitle(title, message: message)
        
        let willHandleDissmissHere = willHaveAutoDismiss != nil ? willHaveAutoDismiss : true
        
        if cancelButtonTitle != nil {
            let action = NYAlertAction()
            action.title = cancelButtonTitle
            action.style = .cancel
            action.handler = { (action) in
                if willHandleDissmissHere == true {
                    alertVC.dismiss(animated: true, completion: { () -> Void in
                        cancelAction?()
                    })
                } else {
                    cancelAction?()
                }
            }
            alertVC.addAction(action)
        }
        
        if okButtonTitle != nil {
            let action = NYAlertAction()
            action.title = okButtonTitle
            action.style = .default
            action.handler = { (action) in
                if willHandleDissmissHere == true {
                    alertVC.dismiss(animated: true, completion: { () -> Void in
                        okAction?()
                    })
                } else {
                    okAction?()
                }
            }
            alertVC.addAction(action)
            
        }

        return alertVC
    }
    
    class func alertForTitle (_ title: String?, message: String?) -> NYAlertViewController {
        var titleString = ""
        var messageString = ERROR_MESSAGE_STRING
        
        if title != nil {
            titleString = title!
        }
        
        if message != nil {
            messageString = message!
        }
        
        let nyAlertVC = NYAlertViewController.alert(withTitle: titleString, message: messageString)
        
        //TODO: change title/button color..
        nyAlertVC?.buttonColor = Colors.GRAY_COLOR
        nyAlertVC?.titleColor = Colors.GREEN_COLOR
        nyAlertVC?.titleFont = boldFontWithSize(16)
        nyAlertVC?.messageFont = normalFontWithSize(15)
        
        return nyAlertVC!
    }
        
}


class AlertContentViewHolder: NSObject {
    var view: UIView!
    var padding = UIEdgeInsets.zero
    var heightConstraintValue: CGFloat = 0
    var widthConstraintValue: CGFloat = 0
}
