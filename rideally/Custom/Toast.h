//
//  Toast.h
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT double ToastVersionNumber;
FOUNDATION_EXPORT const unsigned char ToastVersionString[];

#if __OBJC__
static NSTimeInterval const ToastShortDelay = 2.0;
static NSTimeInterval const ToastLongDelay = 3.5;
static NSString * const ToastViewBackgroundColorAttributeName = @"ToastViewBackgroundColorAttributeName";
static NSString * const ToastViewCornerRadiusAttributeName = @"ToastViewCornerRadiusAttributeName";
static NSString * const ToastViewTextInsetsAttributeName = @"ToastViewTextInsetsAttributeName";
static NSString * const ToastViewTextColorAttributeName = @"ToastViewTextColorAttributeName";
static NSString * const ToastViewFontAttributeName = @"ToastViewFontAttributeName";
static NSString * const ToastViewPortraitOffsetYAttributeName = @"ToastViewPortraitOffsetYAttributeName";
static NSString * const ToastViewLandscapeOffsetYAttributeName = @"ToastViewLandscapeOffsetYAttributeName";
#endif
