//
//  ToastWindow.swift
//  iOS
//
//  Created by saravanan on 15/07/16.
//  Copyright © 2016 sarav. All rights reserved.
//

import UIKit

open class ToastWindow: UIWindow {

    open static let sharedWindow: ToastWindow = {
        let window = ToastWindow(frame: UIScreen.main.bounds)
        window.isUserInteractionEnabled = false
        window.windowLevel = CGFloat.greatestFiniteMagnitude
        window.backgroundColor = .clear
        window.rootViewController = ToastWindowRootViewController()
        window.isHidden = false
        return window
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        NotificationCenter.default.addObserver(self,
            selector: #selector(ToastWindow.bringWindowToTop(_:)),
            name: NSNotification.Name.UIWindowDidBecomeVisible,
            object: nil
        )
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Bring ToastWindow to top when another window is being shown.
    func bringWindowToTop(_ notification: Notification) {
        if !(notification.object is ToastWindow) {
            type(of: self).sharedWindow.isHidden = true
            type(of: self).sharedWindow.isHidden = false
        }
    }

}


private class ToastWindowRootViewController: UIViewController {

    fileprivate convenience init() {
        self.init(nibName: nil, bundle: nil)
    }

    fileprivate override func viewDidLoad() {
        super.viewDidLoad()
    }

    fileprivate override var prefersStatusBarHidden : Bool {
        return UIApplication.shared.isStatusBarHidden
    }
    
    fileprivate override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIApplication.shared.statusBarStyle
    }

    fileprivate override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return .all
    }

}
