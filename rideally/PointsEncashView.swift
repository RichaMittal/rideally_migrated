//
//  PointsEncashView.swift
//  rideally
//
//  Created by Raghunathan on 10/21/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit

class PointsEncashView: UIView,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate
{
    let cardType = UILabel()
    var addressTextField = RATextField()
    var phoneTextField = RATextField()
    let txtcardType = RATextField()
    var radioButtonPayTm :  DLRadioButton!
    var radioButtonPetroCard : DLRadioButton!
    var selectedTxtField: UITextField?
    let pickerView = UIPickerView()
    var pickerDataSource :Array = ["Shell","BP"]
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        createPointsEncashView()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    func createPointsEncashView() -> Void
    {
        self.backgroundColor = Colors.WHITE_COLOR
        
        radioButtonPayTm = getRadioButtonView("Paytm")
        radioButtonPayTm.isSelected = true
        radioButtonPayTm.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(radioButtonPayTm)
        
        radioButtonPetroCard = getRadioButtonView("Petro Card")
        radioButtonPetroCard.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(radioButtonPetroCard)
        
        radioButtonPayTm.otherButtons = [radioButtonPetroCard]
        
        cardType.text = "Card type"
        cardType.isHidden = true
        cardType.textAlignment = NSTextAlignment.left
        cardType.translatesAutoresizingMaskIntoConstraints = false
        cardType.font = normalFontWithSize(14)
        cardType.textColor = Colors.GRAY_COLOR
        self.addSubview(cardType)
        
        pickerView.delegate = self
        pickerView.dataSource = self
        
        txtcardType.delegate = self
        txtcardType.translatesAutoresizingMaskIntoConstraints = false
        txtcardType.text = "Choose your card"
        txtcardType.isHidden = true
        txtcardType.inputView = pickerView
        txtcardType.font = boldFontWithSize(14)
        txtcardType.placeholder = "Select"
        self.addSubview(txtcardType)
        
        let imgV = UIImageView(image: UIImage(named: "dropdown")!)
        imgV.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        imgV.contentMode = .center
        
        txtcardType.rightView = imgV
        txtcardType.rightViewMode = .always
        
        
        let keyboardDoneButtonShow = UIToolbar(frame: CGRect(x: 0, y: 5,  width: self.frame.size.width, height: 30))
        keyboardDoneButtonShow.barStyle = UIBarStyle .blackTranslucent
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let toolbarButton = [flexSpace,doneButton]
        keyboardDoneButtonShow.setItems(toolbarButton, animated: false)
        txtcardType.inputAccessoryView = keyboardDoneButtonShow
        
        
        phoneTextField.layer.borderWidth = 1.0
        phoneTextField.delegate = self
        phoneTextField.returnKeyType = .done
        phoneTextField.font = boldFontWithSize(14)
        phoneTextField.textColor = Colors.BLACK_COLOR
        phoneTextField.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        phoneTextField.translatesAutoresizingMaskIntoConstraints = false
        phoneTextField.placeholder = "Enter your Paytm register mobile number"
        self.addSubview(phoneTextField)
        
        addressTextField.layer.borderWidth = 1.0
        addressTextField.delegate = self
        addressTextField.isHidden = true
        addressTextField.returnKeyType = .done
        addressTextField.font = boldFontWithSize(14)
        addressTextField.textColor = Colors.BLACK_COLOR
        addressTextField.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        addressTextField.translatesAutoresizingMaskIntoConstraints = false
        addressTextField.placeholder = "Enter your address here"
        self.addSubview(addressTextField)
        
        
        let subViewsDict = ["paytm": radioButtonPayTm,"petro":radioButtonPetroCard,"cardtype":cardType,"cardTypeTxt":txtcardType,"address":addressTextField,"phone":phoneTextField]
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[paytm(30)]-5-[cardtype(15)]-5-[cardTypeTxt(30)]-5-[address(50)]|", options:[], metrics:nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[paytm(100)]-5-[petro(==paytm)]", options: [], metrics: nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-15-[cardtype(100)]", options:[], metrics:nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[cardTypeTxt(250)]", options:[], metrics:nil, views: subViewsDict))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[address]-10-|", options:[], metrics:nil, views: subViewsDict))
        
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[phone]-10-|", options:[], metrics:nil, views: subViewsDict))
        
        self.addConstraint(NSLayoutConstraint(item:phoneTextField , attribute: .centerY, relatedBy: .equal, toItem:txtcardType, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item:phoneTextField , attribute: .height, relatedBy: .equal, toItem:addressTextField, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: radioButtonPetroCard, attribute: .centerY, relatedBy: .equal, toItem: radioButtonPayTm, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: radioButtonPetroCard, attribute: .height, relatedBy: .equal, toItem: radioButtonPayTm, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
    }
    
    func getRadioButtonView(_ titleValue:String) ->  DLRadioButton
    {
        let radioButtonInstance = DLRadioButton()
        radioButtonInstance.setTitle(titleValue, for: UIControlState())
        radioButtonInstance.setTitleColor(Colors.BLACK_COLOR, for: UIControlState())
        radioButtonInstance.addTarget(self, action: #selector(radioButtonPressedEvent(_:)), for: .touchUpInside)
        radioButtonInstance.iconColor = Colors.GREEN_COLOR
        radioButtonInstance.indicatorColor = Colors.GREEN_COLOR
        radioButtonInstance.titleLabel?.font = boldFontWithSize(15)
        return radioButtonInstance
    }
    
    func radioButtonPressedEvent(_ sender:DLRadioButton) -> Void
    {
        phoneTextField.isHidden = true
        addressTextField.isHidden = true
        txtcardType.isHidden = true
        cardType.isHidden = true
        
        if  sender == self.radioButtonPayTm
        {
            phoneTextField.isHidden = false
        }
        else
        {
            addressTextField.isHidden = false
            txtcardType.isHidden = false
            cardType.isHidden = false
        }
        
    }
    
    func donePressed() -> Void
    {
        selectedTxtField!.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        selectedTxtField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)
    {
        selectedTxtField = nil
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        
        if  self.phoneTextField == textField
        {
            self.phoneTextField.resignFirstResponder()
        }
        
        
        if  self.addressTextField == textField
        {
            self.addressTextField.resignFirstResponder()
        }
        
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let lenghtValue = (textField.text?.characters.count)! + string.characters.count - range.length
        
        if  textField == phoneTextField
        {
            
            if lenghtValue > 10
            {
                return false
            }
            else
            {
                let aSet = CharacterSet(charactersIn:"0123456789").inverted
                let compSepByCharInSet = string.components(separatedBy: aSet)
                let numberFiltered = compSepByCharInSet.joined(separator: "")
                return string == numberFiltered
            }
        }
        
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return pickerDataSource.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return pickerDataSource[row]
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        txtcardType.text = pickerDataSource[row]
    }
    
}
