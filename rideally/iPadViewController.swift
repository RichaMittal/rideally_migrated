//
//  iPadViewController.swift
//  rideally
//
//  Created by Sarav on 29/01/17.
//  Copyright © 2017 rideally. All rights reserved.
//

import UIKit

class iPadViewController: UIViewController, UIWebViewDelegate {

    let webview = UIWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webview.translatesAutoresizingMaskIntoConstraints = false
        webview.delegate = self
        self.view.addSubview(webview)
        
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[webview]|", options: [], metrics: nil, views: ["webview":webview]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-16-[webview]|", options: [], metrics: nil, views: ["webview":webview]))
        // Do any additional setup after loading the view.
        
        webview.loadRequest(URLRequest(url: URL(string: "https://www.rideally.com")!))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        //Status bar style and visibility
        UIApplication.shared.isStatusBarHidden = false
        UIApplication.shared.statusBarStyle = .lightContent
        
        //Change status bar color
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = Colors.GREEN_COLOR
        }
        
    }

    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        hideIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        hideIndicator()
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        showIndicator("Loading.")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
