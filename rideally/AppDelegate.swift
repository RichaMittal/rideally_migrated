//
//  AppDelegate.swift
//  rideally
//
//  Created by Sarav on 21/07/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import CoreData
import DrawerController
import GoogleMaps
import GooglePlaces
import FBSDKCoreKit
import AWSCore
import AWSS3
import Fabric
import Crashlytics
import Firebase
import FirebaseMessaging
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


extension AppDelegate {
    class func isIPhone5 () -> Bool{
        return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) == 568.0
    }
    class func isIPhone6 () -> Bool {
        return max(UIScreen.main.bounds.width, UIScreen.main.bounds.height) == 667.0
    }
    class func isIPhone6Plus () -> Bool {
        return max(UIScreen.main.bounds.width,     UIScreen.main.bounds.height) == 736.0
    }
}
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var drawerController: DrawerViewController!
    var dataSourceValue: WorkPlace?
    var ofcRides: [WPRide]?
    var homeRides: [WPRide]?
    var noData = false
    func getMainTabBarController() -> UITabBarController
    {
        let tabBarC = UITabBarController()
        if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
            let vc3 = WorkplaceViewController()
            vc3.title = "WORKPLACE"
            vc3.tabBarItem.image = UIImage(named: "Workplace")
            vc3.tabBarItem.selectedImage = UIImage(named: "Workplace_s")
            let navC3 = UINavigationController(rootViewController: vc3)
            
            let vc4 = AirportViewController()
            vc4.title = "AIRPORT"
            vc4.tabBarItem.image = UIImage(named: "Airport")
            vc4.tabBarItem.selectedImage = UIImage(named: "Airport_s")
            let navC4 = UINavigationController(rootViewController: vc4)
            
            
            tabBarC.viewControllers = [navC3, navC4]
        } else {
            let vc1 = CreateRideMapViewController()
            vc1.title = "ANYWHERE"
            vc1.tabBarItem.image = UIImage(named: "anywhere_gray")
            vc1.tabBarItem.selectedImage = UIImage(named: "anywhere_white")
            vc1.edgesForExtendedLayout = UIRectEdge()
            let navC1 = UINavigationController(rootViewController: vc1)
            
            let vc3 = WorkplaceViewController()
            vc3.title = "WORKPLACE"
            vc3.tabBarItem.image = UIImage(named: "Workplace")
            vc3.tabBarItem.selectedImage = UIImage(named: "Workplace_s")
            let navC3 = UINavigationController(rootViewController: vc3)
            
            let vc4 = AirportViewController()
            vc4.title = "AIRPORT"
            vc4.tabBarItem.image = UIImage(named: "Airport")
            vc4.tabBarItem.selectedImage = UIImage(named: "Airport_s")
            let navC4 = UINavigationController(rootViewController: vc4)
            
            
            tabBarC.viewControllers = [navC1, navC3, navC4]
        }
        
        return tabBarC
    }
    
    func gotoHome()
    {
        gotoHome(0)
    }
    
    func gotoHome(_ index: Int)
    {
        let tabVC = getMainTabBarController()
        tabVC.selectedIndex = index
        createHomeViewControllerWithCenterViewController(tabVC)
    }
    //    func gotoHome()
    //    {
    //        createHomeViewControllerWithCenterViewController(getMainTabBarController())
    //    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if(isDeviceiPad()){
            self.window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = iPadViewController()
            self.window?.makeKeyAndVisible()
            return true
        }else{
            GMSServices.provideAPIKey(API_KEYS.GOOGLE_API_KEY)
            GMSPlacesClient.provideAPIKey(API_KEYS.GOOGLE_API_KEY)
            
            FirebaseApp.configure()
            
            let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
            
            let notificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
            application.registerUserNotificationSettings(notificationSettings)
            application.registerForRemoteNotifications()
            
            NotificationCenter.default.addObserver(self,
                                                             selector: #selector(tokenRefreshNotification(_:)),
                                                             name: NSNotification.Name.InstanceIDTokenRefresh,
                                                             object: nil)
            FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
            Fabric.with([Crashlytics.self])
            
            let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1,identityPoolId: API_KEYS.AWS_API_KEY)
            let configuration = AWSServiceConfiguration(region: .USEast1,credentialsProvider: credentialsProvider)
            AWSServiceManager.default().defaultServiceConfiguration = configuration
            
            self.window = UIWindow(frame: UIScreen.main.bounds)
            if(UserInfo.sharedInstance.isLoggedIn){
                self.window?.makeKeyAndVisible()
                if  UserInfo.sharedInstance.userState == "1" || UserInfo.sharedInstance.userState == "2" || UserInfo.sharedInstance.userState == "3" {
                    hideIndicator()
                    if(UserInfo.sharedInstance.mobile == "") {
                        AlertController.showToastForInfo("Please provide 'Mobile Number' to proceed further.")
                    } else {
                        AlertController.showToastForInfo("Please verify 'Mobile Number' to proceed further.")
                    }
                    let vc = MobileVerificationViewController()
                    vc.hidesBottomBarWhenPushed = true
                    vc.edgesForExtendedLayout = UIRectEdge()
                    let navC = UINavigationController(rootViewController: vc)
                    window?.rootViewController = navC
                    self.window?.makeKeyAndVisible()
                } else if(UserInfo.sharedInstance.homeAddress == "") {
                    hideIndicator()
                    AlertController.showToastForInfo("Please provide 'Home Location' to proceed further.")
                    let vc = SignUpMapViewController()
                    let navC = UINavigationController(rootViewController: vc)
                    window?.rootViewController = navC
                    self.window?.makeKeyAndVisible()
                } else{
                    if (UserInfo.sharedInstance.offeredRides != "" && UserInfo.sharedInstance.neededRides != "" && UserInfo.sharedInstance.joinedRides != "" && UserInfo.sharedInstance.bookedRides != "") {
                        //self.window?.makeKeyAndVisible()
                        let myRides = Int(UserInfo.sharedInstance.offeredRides)! + Int(UserInfo.sharedInstance.neededRides)! + Int(UserInfo.sharedInstance.joinedRides)! + Int(UserInfo.sharedInstance.bookedRides)!
                        if(myRides > 0) { // || UserInfo.sharedInstance.isOfficialEmail == "0"
                            //gotoHome()
                            let vc = RideViewController()
                            vc.hidesBottomBarWhenPushed = true
                            vc.edgesForExtendedLayout = UIRectEdge()
                            let navC = UINavigationController(rootViewController: vc)
                            window?.rootViewController = navC
                            self.window?.makeKeyAndVisible()
                        } else if(Int(UserInfo.sharedInstance.myPendingWorkplaces) > 0 || Int(UserInfo.sharedInstance.myWorkplaces) > 0) {
                            if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1") {
                                gotoHome()
                            } else {
                                if(Int(UserInfo.sharedInstance.myWorkplaces) == 1){
                                    gotoHome(1)
                                } else {
                                    gotoHome(1)
                                }
                            }
                        } else {
                            if(UserInfo.sharedInstance.isGroupOptionsIn) {
                                if(UserInfo.sharedInstance.hideAnywhere != "" && UserInfo.sharedInstance.hideAnywhere == "1")
                                {
                                    let vc = WpSearchViewController()
                                    let navC = UINavigationController(rootViewController: vc)
                                    window?.rootViewController = navC
                                } else {
                                    let vc = GroupOptionsViewController()
                                    let navC = UINavigationController(rootViewController: vc)
                                    window?.rootViewController = navC
                                }
                                self.window?.makeKeyAndVisible()
                            } else if(UserInfo.sharedInstance.isRideOptionsIn) {
                                let vc = RideOptionsViewController()
                                let navC = UINavigationController(rootViewController: vc)
                                window?.rootViewController = navC
                                self.window?.makeKeyAndVisible()
                            } else {
                                let vc = PoolOptionsViewController()
                                let navC = UINavigationController(rootViewController: vc)
                                window?.rootViewController = navC
                                self.window?.makeKeyAndVisible()
                            }
                        }
                    } else {
                        UserInfo.sharedInstance.logoutUser()
                        let navC = UINavigationController(rootViewController: LoginOptionViewController())
                        window?.rootViewController = navC
                        self.window?.makeKeyAndVisible()
                    }
                }
            }
            else{
                let navC = UINavigationController(rootViewController: LoginOptionViewController())
                window?.rootViewController = navC
                self.window?.makeKeyAndVisible()
            }
            
            UITabBar.appearance().barTintColor = Colors.TABBAR_BG
            UITabBar.appearance().tintColor = Colors.WHITE_COLOR
            
            UINavigationBar.appearance().titleTextAttributes = [NSFontAttributeName: boldFontWithSize(20)!, NSForegroundColorAttributeName: Colors.WHITE_COLOR]
            UINavigationBar.appearance().tintColor = Colors.WHITE_COLOR
            UINavigationBar.appearance().barTintColor = Colors.GREEN_COLOR
            
            UIApplication.shared.statusBarStyle = .lightContent
            
            return true
        }
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        // NOTE: It can be nil here
        if let refreshedToken = InstanceID.instanceID().token(){
            print("InstanceID token: \(refreshedToken)")
            UserInfo.sharedInstance.deviceToken = refreshedToken
        }
        
        connectToFcm()
    }
    
    func connectToFcm() {
        Messaging.messaging().connect { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                if let refreshedToken = InstanceID.instanceID().token(){
                    print("Connected to FCM.")
                    print("token",refreshedToken)
                    UserInfo.sharedInstance.deviceToken = refreshedToken
                    //AlertController.showToastForInfo(UserInfo.sharedInstance.deviceToken)
                }
                //self.sendDeviceToken(self.refreshedToken)
            }
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func isDeviceiPad() -> Bool
    {
        if(UIDevice.current.userInterfaceIdiom == .pad){
            return true
        }
        else{
            return false
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }
    
    
    func createHomeViewControllerWithCenterViewController (_ centerViewController: UIViewController) {
        let leftSideDrawerViewController = LeftViewController()
        leftSideDrawerViewController.restorationIdentifier = "LeftControllerRestorationKey"
        
        //centerViewController.view.backgroundColor = Colors.HJ_RED_COLOR
        let navigationController = UINavigationController(rootViewController: centerViewController)
        navigationController.restorationIdentifier = "CenterNavigationControllerRestorationKey"
        
        self.drawerController = DrawerViewController(centerViewController: centerViewController, leftDrawerViewController: leftSideDrawerViewController, rightDrawerViewController: nil)
        self.drawerController.showsShadows = true
        self.drawerController.shouldStretchDrawer = true
        
        self.drawerController.restorationIdentifier = "Drawer"
        self.drawerController.maximumLeftDrawerWidth = 0.7*UIScreen.main.bounds.size.width//200.0
        self.drawerController.openDrawerGestureModeMask = .bezelPanningCenterView
        self.drawerController.closeDrawerGestureModeMask = .all
        
        self.drawerController.drawerVisualStateBlock = { (drawerController, drawerSide, percentVisible) in
            let block = ExampleDrawerVisualStateManager.sharedManager.drawerVisualStateBlockForDrawerSide(drawerSide)
            block?(drawerController, drawerSide, percentVisible)
        }
        
        let tintColor = UIColor(red: 29 / 255, green: 173 / 255, blue: 234 / 255, alpha: 1.0)
        self.window?.tintColor = tintColor
        
        self.changeRootViewControllerWithViewController(self.drawerController)
        
    }
    
    func changeRootViewControllerWithViewController(_ newViewController: UIViewController!) {
        
        if self.window?.rootViewController == nil {
            self.window?.rootViewController = newViewController
            return
        }
        
        guard let snapshot:UIView = (self.window?.snapshotView(afterScreenUpdates: true)) else {
            return
        }
        
        newViewController.view.addSubview(snapshot)
        
        self.window?.rootViewController = newViewController
        self.window?.makeKeyAndVisible()
        
        UIView.animate(withDuration: 0.3, animations: {() in
            snapshot.layer.opacity = 0
            snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
            }, completion: {
                (value: Bool) in
                snapshot.removeFromSuperview()
        });
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents .activateApp()
        connectToFcm()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    
    // MARK: - Core Data stack
    
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.ra.rideally" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "rideally", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as AnyObject?
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
}
