//
//  EmergencyContactViewController.swift
//  rideally
//
//  Created by Raghunathan on 10/19/16.
//  Copyright © 2016 rideally. All rights reserved.
//

import UIKit
import AddressBook
import AddressBookUI
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class EmergencyContactViewController: BaseScrollViewController,ABPeoplePickerNavigationControllerDelegate
{
    
    let fullName = UILabel()
    let contactNumber = UILabel()
    let isEmergencyContactselected = UIButton()
    let emergencyContactTextBox = RATextField()
    
    let fullName_middle = UILabel()
    let contactNumber_middle = UILabel()
    let isEmergencyContactselected_middle = UIButton()
    let emergencyContactTextBox_middle = RATextField()
    
    let titleLbl = UILabel()
    let bodyLbl = UILabel()
    let imageView = UIImageView()
    
    var generalInfoView = UIView()
    let addContactButton = UIButton()
    let saveContactButton = UIButton()
    
    var topContactView = UIView()
    var middleContactView = UIView()
    
    let threeDotButton = UIButton()
    let threeDotButton_middle = UIButton()
    
    var addressBookController = ABPeoplePickerNavigationController()
    
    var isMiddleViewShow : String!
    var isFirstViewRemoved : String!
    var isUpdateViewShow : String!
    var  isViewCalledFromRideDetailPage : String!
    var firstPersonNumber: String?
    var secondPersonNumber: String?
    
    //TODO: need to remove this additional button, temp purpose due to dynamic width adjustment issue this unneceessary buttons created
    let addContactButton_dup = UIButton()
    let addContactButton_mid = UIButton()
    let saveContactButton_dup = UIButton()
    let updateContactButton_dup = UIButton()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Emergency Contact"
        self.view.backgroundColor = Colors.WHITE_COLOR
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action: #selector(closeView))
        
        viewsArray.append(generalInformationView())
        addBottomView(getbottomView())
        
        self.isFirstViewRemoved = "0"
        self.isMiddleViewShow = "0"
        
        sendGetContactListRequest()
    }
    
    
    func generalInformationView() -> SubView
    {
        generalInfoView.backgroundColor = UIColor.clear
        generalInfoView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(generalInfoView)
        
        
        titleLbl.font = normalFontWithSize(15)
        titleLbl.backgroundColor = UIColor.clear
        titleLbl.textColor = Colors.GRAY_COLOR
        titleLbl.text = "Make your travel safer"
        titleLbl.textAlignment = NSTextAlignment.center
        titleLbl.translatesAutoresizingMaskIntoConstraints = false
        generalInfoView.addSubview(titleLbl)
        
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "phoneReceiver")
        generalInfoView.addSubview(imageView)
        
        
        self.topContactView = createContactView()
        topContactView.translatesAutoresizingMaskIntoConstraints = false
        topContactView.isHidden = true
        generalInfoView.addSubview(topContactView)
        
        
        fullName.textColor = Colors.BLACK_COLOR
        fullName.font = boldFontWithSize(15)
        fullName.backgroundColor = UIColor.clear
        fullName.translatesAutoresizingMaskIntoConstraints = false
        fullName.isUserInteractionEnabled = false
        topContactView.addSubview(fullName)
        
        
        contactNumber.textColor = Colors.BLACK_COLOR
        contactNumber.font = boldFontWithSize(15)
        contactNumber.backgroundColor = UIColor.clear
        contactNumber.translatesAutoresizingMaskIntoConstraints = false
        topContactView.addSubview(contactNumber)
        
        
        threeDotButton.setImage(UIImage(named: "three_dot"), for: UIControlState())
        threeDotButton.translatesAutoresizingMaskIntoConstraints = false
        threeDotButton.addTarget(self, action: #selector(deleteButtonPressed(_:)), for: .touchDown)
        topContactView.addSubview(threeDotButton)
        
        
        isEmergencyContactselected.translatesAutoresizingMaskIntoConstraints = false
        isEmergencyContactselected.isUserInteractionEnabled = false
        isEmergencyContactselected.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        topContactView.addSubview(isEmergencyContactselected)
        
        
        let despContent = UILabel()
        despContent.text = "Make call on this number during emergency"
        despContent.textColor = Colors.GRAY_COLOR
        despContent.font = normalFontWithSize(12)
        despContent.backgroundColor = UIColor.clear
        despContent.translatesAutoresizingMaskIntoConstraints = false
        topContactView.addSubview(despContent)
        
        
        emergencyContactTextBox.placeholder = "Enter emergency email address"
        emergencyContactTextBox.delegate = self
        emergencyContactTextBox.layer.borderWidth = 1.0
        emergencyContactTextBox.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        emergencyContactTextBox.addBottomViewBorder(1, color: Colors.GENERAL_BORDER_COLOR, offset: CGPoint.zero)
        emergencyContactTextBox.returnKeyType = UIReturnKeyType.done
        emergencyContactTextBox.font = normalFontWithSize(15)!
        emergencyContactTextBox.translatesAutoresizingMaskIntoConstraints = false
        emergencyContactTextBox.isUserInteractionEnabled = false
        topContactView.addSubview(emergencyContactTextBox)
        
        
        middleContactView = createContactView()
        middleContactView.translatesAutoresizingMaskIntoConstraints = false
        middleContactView.isHidden = true
        generalInfoView.addSubview(middleContactView)
        
        
        fullName_middle.font = boldFontWithSize(15)
        fullName_middle.textColor = Colors.BLACK_COLOR
        fullName_middle.backgroundColor = UIColor.clear
        fullName_middle.translatesAutoresizingMaskIntoConstraints = false
        fullName_middle.isUserInteractionEnabled = false
        middleContactView.addSubview(fullName_middle)
        
        
        contactNumber_middle.textColor = Colors.BLACK_COLOR
        contactNumber_middle.font = boldFontWithSize(15)
        contactNumber_middle.backgroundColor = UIColor.clear
        contactNumber_middle.translatesAutoresizingMaskIntoConstraints = false
        middleContactView.addSubview(contactNumber_middle)
        
        
        threeDotButton_middle.setImage(UIImage(named: "three_dot"), for: UIControlState())
        threeDotButton_middle.translatesAutoresizingMaskIntoConstraints = false
        threeDotButton_middle.addTarget(self, action: #selector(deleteButtonPressed(_:)), for: .touchDown)
        middleContactView.addSubview(threeDotButton_middle)
        
        
        isEmergencyContactselected_middle.translatesAutoresizingMaskIntoConstraints = false
        isEmergencyContactselected_middle.isUserInteractionEnabled = false
        isEmergencyContactselected_middle.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        middleContactView.addSubview(isEmergencyContactselected_middle)
        
        
        let despContent_middle = UILabel()
        despContent_middle.text = "Make call on this number during emergency"
        despContent_middle.font = normalFontWithSize(12)
        despContent_middle.textColor = Colors.GRAY_COLOR
        despContent_middle.backgroundColor = UIColor.clear
        despContent_middle.translatesAutoresizingMaskIntoConstraints = false
        middleContactView.addSubview(despContent_middle)
        
        
        
        emergencyContactTextBox_middle.placeholder = "Enter emergency email address"
        emergencyContactTextBox_middle.delegate = self
        emergencyContactTextBox_middle.layer.borderWidth = 1.0
        emergencyContactTextBox_middle.layer.borderColor = Colors.GENERAL_BORDER_COLOR.cgColor
        emergencyContactTextBox_middle.returnKeyType = UIReturnKeyType.done
        emergencyContactTextBox_middle.font = normalFontWithSize(15)!
        emergencyContactTextBox_middle.translatesAutoresizingMaskIntoConstraints = false
        emergencyContactTextBox_middle.isUserInteractionEnabled = false
        middleContactView.addSubview(emergencyContactTextBox_middle)
        
        
        bodyLbl.font = normalFontWithSize(15)
        bodyLbl.backgroundColor = UIColor.clear
        bodyLbl.textColor = Colors.GRAY_COLOR
        bodyLbl.lineBreakMode = NSLineBreakMode.byWordWrapping
        bodyLbl.numberOfLines = 0
        bodyLbl.text = "In case of emergency, contact your close ones Add them to your emergency contact"
        bodyLbl.textAlignment = NSTextAlignment.center
        bodyLbl.translatesAutoresizingMaskIntoConstraints = false
        generalInfoView.addSubview(bodyLbl)
        
        
        generalInfoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[topView(150)]-30-[middleView(==topView)]", options:[], metrics:nil, views: ["topView": topContactView,"middleView":middleContactView]))
        generalInfoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[topView]-20-|", options:[], metrics:nil, views: ["topView": topContactView,"middleView":middleContactView]))
        generalInfoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[middleView]-20-|", options:[], metrics:nil, views: ["topView": topContactView,"middleView":middleContactView]))
        
        topContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[fulname(20)]-5-[contact(==fulname)]-10-[emergencychkbox(20)]-15-[emergencyContact(40)]", options:[], metrics:nil, views: ["fulname": fullName,"contact":contactNumber,"emergencychkbox":isEmergencyContactselected,"emergencyContact":emergencyContactTextBox]))
        
        topContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[despContent(20)]", options:[], metrics:nil, views: ["despContent": despContent]))
        
        topContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[fulname]-10-[delete(20)]-20-|", options:[], metrics:nil, views: ["fulname": fullName,"contact":contactNumber,"emergencychkbox":isEmergencyContactselected,"emergencyContact":emergencyContactTextBox,"delete":threeDotButton]))
        
        topContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[contact]-50-|", options:[], metrics:nil, views: ["fulname": fullName,"contact":contactNumber,"emergencychkbox":isEmergencyContactselected,"emergencyContact":emergencyContactTextBox]))
        
        topContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[emergencychkbox(20)]-2-[despContent]-10-|", options:[], metrics:nil, views: ["fulname": fullName,"contact":contactNumber,"emergencychkbox":isEmergencyContactselected,"emergencyContact":emergencyContactTextBox,"despContent":despContent]))
        
        topContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[emergencyContact]-10-|", options:[], metrics:nil, views: ["fulname": fullName,"contact":contactNumber,"emergencychkbox":isEmergencyContactselected,"emergencyContact":emergencyContactTextBox]))
        
        topContactView.addConstraint(NSLayoutConstraint(item: threeDotButton, attribute: .centerY, relatedBy: .equal, toItem: fullName, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        topContactView.addConstraint(NSLayoutConstraint(item: threeDotButton, attribute: .height, relatedBy: .equal, toItem: fullName, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        topContactView.addConstraint(NSLayoutConstraint(item: despContent, attribute: .centerY, relatedBy: .equal, toItem: isEmergencyContactselected, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        topContactView.addConstraint(NSLayoutConstraint(item: despContent, attribute: .height, relatedBy: .equal, toItem: isEmergencyContactselected, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        
        
        middleContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-5-[fulname1(20)]-5-[contact1(==fulname1)]-10-[emergencychkbox1(20)]-15-[emergencyContact1(40)]", options:[], metrics:nil, views: ["fulname1": fullName_middle,"contact1":contactNumber_middle,"emergencychkbox1":isEmergencyContactselected_middle,"emergencyContact1":emergencyContactTextBox_middle]))
        
        middleContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[despContent1(20)]", options:[], metrics:nil, views: ["despContent1": despContent_middle]))
        middleContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[fulname1]-10-[delete1(20)]-20-|", options:[], metrics:nil, views: ["fulname1": fullName_middle,"contact1":contactNumber_middle,"emergencychkbox1":isEmergencyContactselected_middle,"emergencyContact1":emergencyContactTextBox_middle,"delete1":threeDotButton_middle]))
        
        middleContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[contact1]-50-|", options:[], metrics:nil, views: ["fulname1": fullName_middle,"contact1":contactNumber_middle,"emergencychkbox1":isEmergencyContactselected_middle,"emergencyContact1":emergencyContactTextBox_middle]))
        
        middleContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[emergencychkbox1(20)]-2-[despContent1]-10-|", options:[], metrics:nil, views: ["fulname1": fullName_middle,"contact1":contactNumber_middle,"emergencychkbox1":isEmergencyContactselected_middle,"emergencyContact1":emergencyContactTextBox_middle,"despContent1":despContent_middle]))
        
        middleContactView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[emergencyContact1]-10-|", options:[], metrics:nil, views: ["fulname1": fullName_middle,"contact1":contactNumber_middle,"emergencychkbox1":isEmergencyContactselected_middle,"emergencyContact1":emergencyContactTextBox_middle]))
        
        middleContactView.addConstraint(NSLayoutConstraint(item: threeDotButton_middle, attribute: .centerY, relatedBy: .equal, toItem: fullName_middle, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        middleContactView.addConstraint(NSLayoutConstraint(item: threeDotButton_middle, attribute: .height, relatedBy: .equal, toItem: fullName_middle, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        middleContactView.addConstraint(NSLayoutConstraint(item: despContent_middle, attribute: .centerY, relatedBy: .equal, toItem: isEmergencyContactselected_middle, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        middleContactView.addConstraint(NSLayoutConstraint(item: despContent_middle, attribute: .height, relatedBy: .equal, toItem: isEmergencyContactselected_middle, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        
        generalInfoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[titleLbl(40)]-10-[callbuttonIcon(100)]-10-[bodylbl(50)]", options:[], metrics:nil, views: ["titleLbl": titleLbl,"callbuttonIcon":imageView,"bodylbl":bodyLbl]))
        generalInfoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-5-[titleLbl]-5-|", options:[], metrics:nil, views: ["titleLbl": titleLbl]))
        generalInfoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-padding-[callbuttonIcon(100)]", options:[], metrics:["padding":(self.view.frame.size.width/2)-60], views: ["callbuttonIcon": imageView]))
        generalInfoView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-20-[bodylbl]-20-|", options:[], metrics:nil, views: ["bodylbl": bodyLbl]))
        
        //        generalInfoView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[footerLbl(20)]-5-[addbutton(40)]|", options:[], metrics:nil, views: ["addbutton": self.addContactButton,"footerLbl":footerLbl]))
        //        generalInfoView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[addbutton]|", options:[], metrics:nil, views: ["addbutton": self.addContactButton]))
        //        generalInfoView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[footerLbl]|", options:[], metrics:nil, views: ["footerLbl": footerLbl]))
        
        let loginSubViewInstance = SubView()
        loginSubViewInstance.view = generalInfoView
        loginSubViewInstance.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        loginSubViewInstance.heightConstraint = NSLayoutConstraint(item: generalInfoView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 400)
        
        return loginSubViewInstance
    }
    
    let footerLbl = UILabel()
    
    func getbottomView() -> SubView
    {
        
        let view = UIView()
        
        
        footerLbl.font = normalFontWithSize(15)
        footerLbl.backgroundColor = UIColor.clear
        footerLbl.textColor = Colors.GRAY_COLOR
        footerLbl.text = "You can add upto 2 contacts"
        footerLbl.textAlignment = NSTextAlignment.center
        footerLbl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(footerLbl)
        
        addContactButton.titleLabel?.font = boldFontWithSize(15)
        addContactButton.isHidden = true
        addContactButton.backgroundColor = Colors.GRAY_COLOR
        addContactButton.translatesAutoresizingMaskIntoConstraints = false
        addContactButton.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
        addContactButton.setTitle("ADD CONTACTS", for: UIControlState())
        addContactButton.titleLabel?.textAlignment = NSTextAlignment.center
        addContactButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        addContactButton.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        view.addSubview(addContactButton)
        
        
        addContactButton_dup.titleLabel?.font = boldFontWithSize(15)
        addContactButton_dup.backgroundColor = Colors.GRAY_COLOR
        addContactButton_dup.translatesAutoresizingMaskIntoConstraints = false
        addContactButton_dup.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
        addContactButton_dup.setTitle("ADD CONTACTS", for: UIControlState())
        addContactButton_dup.titleLabel?.textAlignment = NSTextAlignment.center
        addContactButton_dup.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        addContactButton_dup.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        view.addSubview(addContactButton_dup)
        
        addContactButton_mid.titleLabel?.font = boldFontWithSize(15)
        addContactButton_mid.backgroundColor = Colors.GRAY_COLOR
        addContactButton_mid.translatesAutoresizingMaskIntoConstraints = false
        addContactButton_mid.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
        addContactButton_mid.setTitle("ADD CONTACTS", for: UIControlState())
        addContactButton_mid.titleLabel?.textAlignment = NSTextAlignment.center
        addContactButton_mid.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        addContactButton_mid.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        addContactButton_mid.isHidden = true
        view.addSubview(addContactButton_mid)
        
        
        
        saveContactButton.titleLabel?.font = boldFontWithSize(15)
        saveContactButton.isHidden = true
        saveContactButton.backgroundColor = Colors.GRAY_COLOR
        saveContactButton.translatesAutoresizingMaskIntoConstraints = false
        saveContactButton.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
        saveContactButton.setTitle("SAVE CONTACTS", for: UIControlState())
        saveContactButton.titleLabel?.textAlignment = NSTextAlignment.center
        saveContactButton.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        saveContactButton.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        view.addSubview(saveContactButton)
        
        saveContactButton_dup.titleLabel?.font = boldFontWithSize(15)
        saveContactButton_dup.isHidden = true
        saveContactButton_dup.backgroundColor = Colors.GRAY_COLOR
        saveContactButton_dup.translatesAutoresizingMaskIntoConstraints = false
        saveContactButton_dup.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
        saveContactButton_dup.setTitle("SAVE CONTACTS", for: UIControlState())
        saveContactButton_dup.titleLabel?.textAlignment = NSTextAlignment.center
        saveContactButton_dup.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        saveContactButton_dup.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        view.addSubview(saveContactButton_dup)
        
        updateContactButton_dup.titleLabel?.font = boldFontWithSize(15)
        updateContactButton_dup.isHidden = true
        updateContactButton_dup.backgroundColor = Colors.GRAY_COLOR
        updateContactButton_dup.translatesAutoresizingMaskIntoConstraints = false
        updateContactButton_dup.addTarget(self, action: #selector(buttonPressedEvent(_:)), for: .touchUpInside)
        updateContactButton_dup.setTitle("UPDATE CONTACTS", for: UIControlState())
        updateContactButton_dup.titleLabel?.textAlignment = NSTextAlignment.center
        updateContactButton_dup.setTitleColor(Colors.WHITE_COLOR, for: UIControlState())
        updateContactButton_dup.setTitleColor(Colors.BLACK_COLOR, for: .highlighted)
        updateContactButton_dup.isHidden = true
        view.addSubview(updateContactButton_dup)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[footerLbl(20)]-5-[addbutton(40)]", options:[], metrics:nil, views: ["addbutton": self.addContactButton,"footerLbl":footerLbl]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-25-[addbutton_dup(40)]", options:[], metrics:nil, views: ["addbutton_dup": self.addContactButton_dup,"footerLbl":footerLbl]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-25-[addbutton_mid(40)]", options:[], metrics:nil, views: ["addbutton_mid": self.addContactButton_mid,"footerLbl":footerLbl]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[addbutton(width)][savebutton(==addbutton)]", options:[], metrics:["width":self.view.frame.size.width/2], views: ["addbutton": self.addContactButton,"savebutton":self.saveContactButton]))
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[footerLbl]|", options:[], metrics:nil, views: ["footerLbl": footerLbl]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[addbutton_dup]|", options:[], metrics:nil, views: ["addbutton_dup": addContactButton_dup]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[addbutton_mid]|", options:[], metrics:nil, views: ["addbutton_mid": addContactButton_mid]))
        
        view.addConstraint(NSLayoutConstraint(item: saveContactButton, attribute: .centerY, relatedBy: .equal, toItem: addContactButton, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: saveContactButton, attribute: .height, relatedBy: .equal, toItem: addContactButton, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .centerY, relatedBy: .equal, toItem: addContactButton_dup, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .height, relatedBy: .equal, toItem: addContactButton_dup, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .width, relatedBy: .equal, toItem: addContactButton_dup, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .centerY, relatedBy: .equal, toItem: addContactButton_mid, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .height, relatedBy: .equal, toItem: addContactButton_mid, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .width, relatedBy: .equal, toItem: addContactButton_mid, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .centerY, relatedBy: .equal, toItem: updateContactButton_dup, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .height, relatedBy: .equal, toItem: updateContactButton_dup, attribute: NSLayoutAttribute.height, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: saveContactButton_dup, attribute: .width, relatedBy: .equal, toItem: updateContactButton_dup, attribute: NSLayoutAttribute.width, multiplier: 1, constant: 0))
        
        
        //        widthConstraintForAddButton.constant = 0
        //        widthConstraintForSaveButton = NSLayoutConstraint(item: saveContactButton, attribute: .Width, relatedBy: .Equal, toItem: addContactButton, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0)
        
        let sub = SubView()
        sub.view = view
        sub.padding = UIEdgeInsetsMake(0, 0, 0, 0)
        sub.heightConstraint = NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 65)
        
        return sub
    }
    
    
    func isEmergencyNumberSharedPressed(_ sender:UIButton) -> Void
    {
        if sender == self.isEmergencyContactselected
        {
            sender.isSelected = !sender.isSelected
            
            self.isEmergencyContactselected.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
            
            if  sender.isSelected == true
            {
                sender.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
            }
            
            self.isEmergencyContactselected_middle.isSelected = false
            self.isEmergencyContactselected_middle.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        }
        
        
        if sender == self.isEmergencyContactselected_middle
        {
            sender.isSelected = !sender.isSelected
            self.isEmergencyContactselected_middle.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
            
            if  sender.isSelected == true
            {
                sender.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
            }
            
            
            self.isEmergencyContactselected.isSelected = false
            self.isEmergencyContactselected.setImage(UIImage(named: "Checkbox_unselected"), for: UIControlState())
        }
    }
    
    
    func createContactView() -> UIView
    {
        let viewInstace = UIView()
        viewInstace.backgroundColor = Colors.WHITE_COLOR
        viewInstace.layer.shadowOffset = CGSize(width: 3, height: 3)
        viewInstace.layer.shadowOpacity = 0.7
        viewInstace.layer.shadowRadius = 8
        return viewInstace
    }
    
    
    func buttonPressedEvent (_ sender: UIButton)
    {
        if sender == self.addContactButton_dup
        {
            self.isMiddleViewShow = "0"
            callAddressBookForPressedButtonObject()
        }
        else if sender == self.addContactButton || sender == self.addContactButton_mid
        {
            self.isMiddleViewShow = "1"
            callAddressBookForPressedButtonObject()
        }
        else if sender == self.saveContactButton || sender == self.saveContactButton_dup || sender == self.updateContactButton_dup
        {
            
            if self.emergencyContactTextBox.text?.characters.count > 0 &&  self.emergencyContactTextBox_middle.text?.characters.count > 0
            {
                
                if isValidEmailFormate(self.emergencyContactTextBox.text!) && isValidEmailFormate(self.emergencyContactTextBox_middle.text!)
                {
                    sendSaveContactListRequest("")
                }
                else
                {
                    AlertController.showToastForError("Please enter valid 'Email Id'.")
                }
            }
            else if self.emergencyContactTextBox.text?.characters.count > 0 && self.emergencyContactTextBox_middle.text?.characters.count == 0
            {
                if isValidEmailFormate(self.emergencyContactTextBox.text!)
                {
                    sendSaveContactListRequest("")
                }
                else
                {
                    AlertController.showToastForError("Please enter valid 'Email Id'.")
                }
            }
                
            else if self.emergencyContactTextBox.text?.characters.count == 0 && self.emergencyContactTextBox_middle.text?.characters.count > 0
            {
                if isValidEmailFormate(self.emergencyContactTextBox_middle.text!)
                {
                    sendSaveContactListRequest("")
                }
                else
                {
                    AlertController.showToastForError("Please enter valid 'Email Id'.")
                }
            }
                
            else if self.emergencyContactTextBox.text?.characters.count == 0 && self.emergencyContactTextBox_middle.text?.characters.count == 0
            {
                sendSaveContactListRequest("")
            }
            
        }
    }
    
    
    func isValidEmailFormate(_ stringValue:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: stringValue)
    }
    
    
    
    func deleteButtonPressed(_ Sender:UIButton) -> Void
    {
        let vc = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteContact = UIAlertAction(title: "Delete", style: .default, handler: { (action) in
            self.isFirstViewRemoved = "0"
            let reqObj = DeleteEmergencyContactRequest()
            showIndicator("Loading...")
            
            reqObj.userID = UserInfo.sharedInstance.userID
            if  Sender == self.threeDotButton
            {
                reqObj.mobileNumber = self.contactNumber.text
            } else if Sender == self.threeDotButton_middle {
                reqObj.mobileNumber = self.contactNumber_middle.text
            }
            EmergencyContactRequestor().deleteContactListRequest(reqObj, success:{ (success, object) in
                hideIndicator()
                if((object as! DelteEmergencyContactResponse).code == "4601") {
                    if  Sender == self.threeDotButton
                    {
                        if(self.contactNumber_middle.text?.characters.count>0) {
                            self.topContactView.isHidden = false
                            self.fullName.text = self.fullName_middle.text
                            self.contactNumber.text = self.contactNumber_middle.text
                            self.emergencyContactTextBox.text = self.emergencyContactTextBox_middle.text
                            self.fullName_middle.text = ""
                            self.contactNumber_middle.text = ""
                            self.emergencyContactTextBox_middle.text = ""
                            self.middleContactView.isHidden = true
                            reqObj.mobileNumber = self.contactNumber.text
                            EmergencyContactRequestor().deleteContactListRequest(reqObj, success:{ (success, object) in
                                if((object as! DelteEmergencyContactResponse).code == "4601") {
                                    self.sendSaveContactListRequest("delete")
                                }
                            }){ (error) in
                                AlertController.showAlertForError(error)
                            }
                            
                        } else {
                            AlertController.showToastForInfo("Contact deleted successfully.")
                            self.topContactView.isHidden = true
                            self.fullName.text = ""
                            self.contactNumber.text = ""
                            self.emergencyContactTextBox.text = ""
                            UserInfo.sharedInstance.selectedEmergencyNumber = ""
                            UserInfo.sharedInstance.selectedEmergencyEmail = ""
                        }
                        
                    }
                    else if Sender == self.threeDotButton_middle
                    {
                        AlertController.showToastForInfo("Contact deleted successfully.")
                        self.middleContactView.isHidden = true
                        self.fullName_middle.text = ""
                        self.contactNumber_middle.text = ""
                        self.emergencyContactTextBox_middle.text = ""
                        self.addContactButton_mid.isHidden = false
                        self.footerLbl.isHidden = false
                        UserInfo.sharedInstance.selectedEmergencyNumber = self.contactNumber.text!
                        UserInfo.sharedInstance.selectedEmergencyEmail = self.emergencyContactTextBox.text!
                    }
                    
                    
                    if self.topContactView.isHidden == true && self.middleContactView.isHidden == true
                    {
                        self.addContactButton.isHidden = true
                        self.saveContactButton.isHidden = true
                        self.saveContactButton_dup.isHidden = true
                        self.addContactButton_mid.isHidden = true
                        self.footerLbl.isHidden = false
                        self.titleLbl.isHidden =  false
                        self.bodyLbl.isHidden = false
                        self.imageView.isHidden = false
                        self.addContactButton_dup.isHidden = false
                    }
                }
            }){ (error) in
                hideIndicator()
                AlertController.showAlertForError(error)
            }
        })
        vc.addAction(deleteContact)
        let updateContact = UIAlertAction(title: "Update", style: .default, handler: { (action) in
            self.updateContactButton_dup.isHidden = false
            if(Sender == self.threeDotButton) {
                self.isUpdateViewShow = "0"
                self.contactNumber.isUserInteractionEnabled = true
                let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(self.callAddressBookForPressedButtonObject))
                self.contactNumber.addGestureRecognizer(tapGestureFrom)
                self.emergencyContactTextBox.isUserInteractionEnabled = true
                self.isEmergencyContactselected.isUserInteractionEnabled = true
                self.isEmergencyContactselected.addTarget(self, action: #selector(self.isEmergencyNumberSharedPressed(_:)), for: .touchUpInside)
            } else if(Sender == self.threeDotButton_middle) {
                self.isUpdateViewShow = "1"
                self.contactNumber_middle.isUserInteractionEnabled = true
                let tapGestureFrom = UITapGestureRecognizer(target: self, action: #selector(self.callAddressBookForPressedButtonObject))
                self.contactNumber_middle.addGestureRecognizer(tapGestureFrom)
                self.emergencyContactTextBox_middle.isUserInteractionEnabled = true
                self.isEmergencyContactselected_middle.isUserInteractionEnabled = true
                self.isEmergencyContactselected_middle.addTarget(self, action: #selector(self.isEmergencyNumberSharedPressed(_:)), for: .touchUpInside)
            }
        })
        vc.addAction(updateContact)
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        })
        vc.addAction(actionCancel)
        
        present(vc, animated: true, completion: nil)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
    
    
    func closeView()
    {
        
        if  (self.isViewCalledFromRideDetailPage != nil)
        {
            if  self.isViewCalledFromRideDetailPage.characters.count > 0
            {
                if  self.isViewCalledFromRideDetailPage == "1"
                {
                    _ = self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        else
        {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func callAddressBookForPressedButtonObject() -> Void
    {
        let authorizationStatus = ABAddressBookGetAuthorizationStatus()
        
        switch authorizationStatus
        {
        case .denied, .restricted:
            displayCantAddContactAlert()
        case .authorized:
            self.addressBookController.peoplePickerDelegate = self
            self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                "phoneNumbers.@count > 0", argumentArray: nil)
            self.present(self.addressBookController, animated: true, completion: nil)
        case .notDetermined:
            promptForAddressBookRequestAccess()
        }
        
    }
    
    func peoplePickerNavigationControllerDidCancel(_ peoplePicker: ABPeoplePickerNavigationController)
    {
        
        self.addressBookController.dismiss(animated: true, completion: nil)
    }
    
    func peoplePickerNavigationController(_ peoplePicker: ABPeoplePickerNavigationController, didSelectPerson person: ABRecord)
    {
        
        if self.topContactView.isHidden == true && self.middleContactView.isHidden == true
        {
            self.updateDataToTopView(person)
        }
        
        
        if self.isFirstViewRemoved == "1"
        {
            self.isFirstViewRemoved = "0"
            //self.updateDataToTopView(person)
            
            if  self.isMiddleViewShow == "1"
            {
                self.middleContactView.isHidden = false
            }
            
        }
        
        if  self.isMiddleViewShow == "1"
        {
            addContactButton_dup.isHidden = true
            titleLbl.isHidden =  true
            bodyLbl.isHidden = true
            imageView.isHidden = true
            addContactButton.isHidden = true
            saveContactButton.isHidden = true
            
            middleContactView.isHidden = false
            saveContactButton_dup.isHidden = false
            emergencyContactTextBox_middle.isUserInteractionEnabled = true
            
            if let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String
            {
                self.fullName_middle.text = first
                
                if let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String
                {
                    self.fullName_middle.text = first+last
                }
            }
            
            let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones != "")
            {
                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                {
                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                        var personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                        let areaCode = personNumber.characters.count-10
                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                        if(areaCode > 0) {
                            personNumber = personNumber.substring(from: startIndex)
                        }
                        self.contactNumber_middle.text = personNumber
                    }
                }
            }
            
            let unmanagedPhones1 = ABRecordCopyValue(person, kABPersonEmailProperty)
            let phones1 = Unmanaged.fromOpaque((unmanagedPhones1?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones1 != "")
            {
                if let unmanagedPhone1 = ABMultiValueCopyValueAtIndex(phones1, 0)
                {
                    if  let phone1: String = Unmanaged.fromOpaque(unmanagedPhone1.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        self.emergencyContactTextBox_middle.text = phone1
                    }
                } else {
                    self.emergencyContactTextBox_middle.text = ""
                }
            }
            
        }
        
        if  self.isUpdateViewShow != nil && self.isUpdateViewShow == "0"
        {
            
            if let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String
            {
                self.fullName.text = first
                
                if let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String
                {
                    self.fullName.text = first+last
                }
            }
            
            let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones != "")
            {
                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                {
                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                        var personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                        let areaCode = personNumber.characters.count-10
                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                        if(areaCode > 0) {
                            personNumber = personNumber.substring(from: startIndex)
                        }
                        self.contactNumber.text = personNumber
                    }
                }
            }
            
            let unmanagedPhones1 = ABRecordCopyValue(person, kABPersonEmailProperty)
            let phones1 = Unmanaged.fromOpaque((unmanagedPhones1?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones1 != "")
            {
                if let unmanagedPhone1 = ABMultiValueCopyValueAtIndex(phones1, 0)
                {
                    if  let phone1: String = Unmanaged.fromOpaque(unmanagedPhone1.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        self.emergencyContactTextBox.text = phone1
                    }
                } else {
                    self.emergencyContactTextBox.text = ""
                }
            }
        }
        
        if  self.isUpdateViewShow != nil && self.isUpdateViewShow == "1"
        {
            
            if let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String
            {
                self.fullName_middle.text = first
                
                if let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String
                {
                    self.fullName_middle.text = first+last
                }
            }
            
            let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
            let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones != "")
            {
                if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
                {
                    if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                        var personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                        let areaCode = personNumber.characters.count-10
                        let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                        if(areaCode > 0) {
                            personNumber = personNumber.substring(from: startIndex)
                        }
                        self.contactNumber_middle.text = personNumber
                    }
                }
            }
            
            let unmanagedPhones1 = ABRecordCopyValue(person, kABPersonEmailProperty)
            let phones1 = Unmanaged.fromOpaque((unmanagedPhones1?.toOpaque())!).takeUnretainedValue() as NSString
            if(phones1 != "")
            {
                if let unmanagedPhone1 = ABMultiValueCopyValueAtIndex(phones1, 0)
                {
                    if  let phone1: String = Unmanaged.fromOpaque(unmanagedPhone1.toOpaque()).takeUnretainedValue() as NSObject as? String
                    {
                        self.emergencyContactTextBox_middle.text = phone1
                    }
                } else {
                    self.emergencyContactTextBox_middle.text = ""
                }
            }
        }
        
    }
    
    func updateDataToTopView(_ person:ABRecord) -> Void
    {
        self.isMiddleViewShow = "0"
        addContactButton_dup.isHidden = true
        saveContactButton_dup.isHidden = true
        titleLbl.isHidden =  true
        bodyLbl.isHidden = true
        imageView.isHidden = true
        middleContactView.isHidden = true
        
        topContactView.isHidden = false
        addContactButton.isHidden = false
        saveContactButton.isHidden = false
        emergencyContactTextBox.isUserInteractionEnabled = true
        
        if let first = ABRecordCopyValue(person, kABPersonFirstNameProperty)?.takeRetainedValue() as? String
        {
            self.fullName.text = first
            
            if let last  = ABRecordCopyValue(person, kABPersonLastNameProperty)?.takeRetainedValue() as? String
            {
                self.fullName.text = first+last
            }
        }
        
        let unmanagedPhones = ABRecordCopyValue(person, kABPersonPhoneProperty)
        let phones = Unmanaged.fromOpaque((unmanagedPhones?.toOpaque())!).takeUnretainedValue() as NSString
        if(phones != "")
        {
            if  let unmanagedPhone = ABMultiValueCopyValueAtIndex(phones, 0)
            {
                if let phone: String = Unmanaged.fromOpaque(unmanagedPhone.toOpaque()).takeUnretainedValue() as NSObject as? String
                {
                    let stringArray = phone.components(separatedBy: CharacterSet.decimalDigits.inverted)
                    var personNumber = NSArray(array: stringArray).componentsJoined(by: "")
                    let areaCode = personNumber.characters.count-10
                    let startIndex = personNumber.characters.index(personNumber.startIndex, offsetBy: areaCode)
                    if(areaCode > 0) {
                        personNumber = personNumber.substring(from: startIndex)
                    }
                    self.contactNumber.text = personNumber
                }
            }
        }
        
        let unmanagedPhones1 = ABRecordCopyValue(person, kABPersonEmailProperty)
        let phones1 = Unmanaged.fromOpaque((unmanagedPhones1?.toOpaque())!).takeUnretainedValue() as NSString
        if(phones1 != "")
        {
            if let unmanagedPhone1 = ABMultiValueCopyValueAtIndex(phones1, 0)
            {
                if  let phone1: String = Unmanaged.fromOpaque(unmanagedPhone1.toOpaque()).takeUnretainedValue() as NSObject as? String
                {
                    self.emergencyContactTextBox.text = phone1
                }
            }
        }
        
        self.isEmergencyContactselected.isSelected = true
        self.isEmergencyContactselected.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
        
    }
    
    
    func openSettings()
    {
        let url = URL(string: UIApplicationOpenSettingsURLString)
        UIApplication.shared.openURL(url!)
    }
    
    
    func promptForAddressBookRequestAccess()
    {
        let addressBookRef: ABAddressBook = ABAddressBookCreateWithOptions(nil, nil).takeRetainedValue()
        ABAddressBookRequestAccessWithCompletion(addressBookRef)
        {
            (granted: Bool, error: Error?) in
            DispatchQueue.main.async
            {
                if !granted
                {
                    //print("Just denied")
                    self.displayCantAddContactAlert()
                } else
                {
                    //print("Just authorized")
                    self.addressBookController.peoplePickerDelegate = self
                    self.addressBookController.predicateForEnablingPerson = NSPredicate(format:
                        "phoneNumbers.@count > 0", argumentArray: nil)
                    self.present(self.addressBookController, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    func displayCantAddContactAlert() {
        let cantAddContactAlert = UIAlertController(title: "Cannot Add Contact",
                                                    message: "You must give the app permission to add the contact first.",
                                                    preferredStyle: .alert)
        cantAddContactAlert.addAction(UIAlertAction(title: "Change Settings",
            style: .default,
            handler: { action in
                self.openSettings()
        }))
        cantAddContactAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(cantAddContactAlert, animated: true, completion: nil)
    }
    
    
    
    func sendGetContactListRequest() -> Void
    {
        let reqObj = GetEmergencyContactRequest()
        showIndicator("Loading...")
        
        reqObj.userID = UserInfo.sharedInstance.userID
        
        EmergencyContactRequestor().sendGetContactListRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! GetEmergencyContactResponse).code == "18899"
            {
                
                self.firstPersonNumber = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber
                self.secondPersonNumber = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber
                if (object as! GetEmergencyContactResponse).dataObj?.first != nil
                {
                    
                    UserInfo.sharedInstance.isDefaultEmergencyNumberSelected = ""
                    
                    if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) != nil)
                    {
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson)?.characters.count > 0)
                        {
                            UserInfo.sharedInstance.isDefaultEmergencyNumberSelected = ((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson)!
                            
                            UserInfo.sharedInstance.selectedEmergencyEmail  = ""
                            UserInfo.sharedInstance.selectedEmergencyNumber = ""
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) == "1")
                            {
                                if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber) != nil)
                                {
                                    if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber)?.characters.count > 0)
                                    {
                                        UserInfo.sharedInstance.selectedEmergencyNumber = ((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber)!
                                    }
                                    
                                }
                                
                                
                                if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail) != nil)
                                {
                                    if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail)?.characters.count > 0)
                                    {
                                        UserInfo.sharedInstance.selectedEmergencyEmail = ((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail)!
                                    }
                                    
                                }
                            }
                            else if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) == "2")
                            {
                                
                                if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber) != nil)
                                {
                                    if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber)?.characters.count > 0)
                                    {
                                        UserInfo.sharedInstance.selectedEmergencyNumber = ((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber)!
                                    }
                                    
                                }
                                
                                
                                if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail) != nil)
                                {
                                    if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail)?.characters.count > 0)
                                    {
                                        UserInfo.sharedInstance.selectedEmergencyEmail = ((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail)!
                                    }
                                }
                            }
                            
                        }
                    }
                    
                    if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName) != nil) && (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName) != nil)
                    {
                        
                        self.titleLbl.isHidden =  true
                        self.bodyLbl.isHidden = true
                        self.imageView.isHidden = true
                        
                        
                        if ((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName?.characters.count > 0) && ((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName?.characters.count > 0)
                        {
                            
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) != nil)
                            {
                                if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson)?.characters.count > 0)
                                {
                                    UserInfo.sharedInstance.isDefaultEmergencyNumberSelected = ((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson)!
                                }
                            }
                            
                            
                            self.topContactView.isHidden = false
                            self.middleContactView.isHidden = false
                            self.footerLbl.isHidden = true
                            self.addContactButton_dup.isHidden = true
                            self.addContactButton.isHidden = true
                            self.saveContactButton.isHidden = true
                            self.saveContactButton_dup.isHidden = true
                            
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName) != nil)
                            {
                                self.fullName.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName) != nil)
                            {
                                self.fullName_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber) != nil)
                            {
                                self.contactNumber.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber) != nil)
                            {
                                self.contactNumber_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail) != nil)
                            {
                                self.emergencyContactTextBox.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail) != nil)
                            {
                                self.emergencyContactTextBox_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) != nil)
                            {
                                
                                if (object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson == "1"
                                {
                                    self.isEmergencyContactselected.isSelected = true
                                    self.isEmergencyContactselected.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
                                }
                                else if (object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson == "2"
                                {
                                    self.isEmergencyContactselected_middle.isSelected = true
                                    self.isEmergencyContactselected_middle.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
                                }
                            }
                        }
                        else if ((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName?.characters.count > 0) && ((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName?.characters.count == 0)
                        {
                            
                            self.topContactView.isHidden = false
                            self.middleContactView.isHidden = true
                            self.addContactButton_mid.isHidden = false
                            self.addContactButton_dup.isHidden = true
                            self.saveContactButton_dup.isHidden = true
                            self.addContactButton.isHidden = true
                            self.saveContactButton.isHidden = true
                            
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName) != nil)
                            {
                                self.fullName.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName
                            }
                            
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber) != nil)
                            {
                                self.contactNumber.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber
                            }
                            
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail) != nil)
                            {
                                self.emergencyContactTextBox.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail
                            }
                            
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) != nil)
                            {
                                
                                if (object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson == "1"
                                {
                                    self.isEmergencyContactselected.isSelected = true
                                    self.isEmergencyContactselected.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
                                    
                                }
                            }
                        }
                            
                        else if ((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName?.characters.count == 0) && ((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName?.characters.count > 0)
                        {
                            self.topContactView.isHidden = true
                            self.middleContactView.isHidden = false
                            self.addContactButton_dup.isHidden = true
                            self.saveContactButton_dup.isHidden = true
                            self.addContactButton.isHidden = true
                            self.saveContactButton.isHidden = true
                            self.addContactButton_mid.isHidden = false
                            self.isFirstViewRemoved = "1"
                            
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName) != nil)
                            {
                                self.fullName_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber) != nil)
                            {
                                self.contactNumber_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail) != nil)
                            {
                                self.emergencyContactTextBox_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail
                            }
                            
                            if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) != nil)
                            {
                                
                                if (object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson == "2"
                                {
                                    self.isEmergencyContactselected_middle.isSelected = true
                                    self.isEmergencyContactselected_middle.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
                                }
                            }
                        }
                    }
                        
                    else if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName) != nil) && (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName) == nil)
                    {
                        self.titleLbl.isHidden =  true
                        self.bodyLbl.isHidden = true
                        self.imageView.isHidden = true
                        
                        
                        self.topContactView.isHidden = false
                        self.middleContactView.isHidden = true
                        self.addContactButton_mid.isHidden = false
                        self.addContactButton_dup.isHidden = true
                        self.saveContactButton_dup.isHidden = true
                        self.addContactButton.isHidden = true
                        self.saveContactButton.isHidden = true
                        
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName) != nil)
                        {
                            self.fullName.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName
                        }
                        
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber) != nil)
                        {
                            self.contactNumber.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonNumber
                        }
                        
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail) != nil)
                        {
                            self.emergencyContactTextBox.text = (object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonEmail
                        }
                        
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) != nil)
                        {
                            
                            if (object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson == "1"
                            {
                                self.isEmergencyContactselected.isSelected = true
                                self.isEmergencyContactselected.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
                                
                            }
                        }
                        
                    }
                        
                        
                    else if (((object as! GetEmergencyContactResponse).dataObj?.first?.firstPersonName) == nil) && (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName) != nil)
                    {
                        self.titleLbl.isHidden =  true
                        self.bodyLbl.isHidden = true
                        self.imageView.isHidden = true
                        
                        self.topContactView.isHidden = true
                        self.middleContactView.isHidden = false
                        self.addContactButton_dup.isHidden = true
                        self.saveContactButton_dup.isHidden = true
                        self.addContactButton.isHidden = true
                        self.saveContactButton.isHidden = true
                        self.addContactButton_mid.isHidden = false
                        self.isFirstViewRemoved = "1"
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName) != nil)
                        {
                            self.fullName_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonName
                        }
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber) != nil)
                        {
                            self.contactNumber_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonNumber
                        }
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail) != nil)
                        {
                            self.emergencyContactTextBox_middle.text = (object as! GetEmergencyContactResponse).dataObj?.first?.secondPersonEmail
                        }
                        
                        if (((object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson) != nil)
                        {
                            
                            if (object as! GetEmergencyContactResponse).dataObj?.first?.defaultPerson == "2"
                            {
                                self.isEmergencyContactselected_middle.isSelected = true
                                self.isEmergencyContactselected_middle.setImage(UIImage(named: "CheckBox_selected"), for: UIControlState())
                            }
                        }
                    }
                    else
                    {
                        self.topContactView.isHidden = true
                        self.middleContactView.isHidden = true
                        self.addContactButton.isHidden = true
                        self.saveContactButton.isHidden = true
                        self.saveContactButton_dup.isHidden = true
                        self.titleLbl.isHidden =  false
                        self.bodyLbl.isHidden = false
                        self.imageView.isHidden = false
                        self.addContactButton_dup.isHidden = false
                    }
                }
            }
            else
            {
                AlertController.showToastForError("Record Not Found.")
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
    
    
    func sendSaveContactListRequest(_ action: String) -> Void
    {
        let reqObj = SaveEmergencyContactRequest()
        showIndicator("Loading...")
        
        reqObj.userID = UserInfo.sharedInstance.userID
        
        reqObj.firstPersonName = ""
        if  fullName.text?.characters.count > 0
        {
            reqObj.firstPersonName = fullName.text
        }
        
        reqObj.secondPersonName = ""
        if  fullName_middle.text?.characters.count > 0
        {
            reqObj.secondPersonName = fullName_middle.text
        } else {
            reqObj.secondPersonName = nil
        }
        
        reqObj.firstPersonNumber = ""
        if  contactNumber.text?.characters.count > 0
        {
            let stringArray = contactNumber.text?.components(separatedBy: CharacterSet.decimalDigits.inverted)
            var firstPersonNumber = NSArray(array: stringArray!).componentsJoined(by: "")
            let areaCode = firstPersonNumber.characters.count-10
            let startIndex = firstPersonNumber.characters.index(firstPersonNumber.startIndex, offsetBy: areaCode)
            if(areaCode > 0) {
                firstPersonNumber = firstPersonNumber.substring(from: startIndex)
            }
            reqObj.firstPersonNumber = firstPersonNumber
        }
        
        reqObj.secondPersonNumber = ""
        if  contactNumber_middle.text?.characters.count > 0
        {
            let stringArray = contactNumber_middle.text?.components(separatedBy: CharacterSet.decimalDigits.inverted)
            
            var secondPersonNumber = NSArray(array: stringArray!).componentsJoined(by: "")
            let areaCode = secondPersonNumber.characters.count-10
            let startIndex = secondPersonNumber.characters.index(secondPersonNumber.startIndex, offsetBy: areaCode)
            if(areaCode > 0) {
                secondPersonNumber = secondPersonNumber.substring(from: startIndex)
            }
            reqObj.secondPersonNumber = secondPersonNumber
        }
        
        reqObj.firstPersonEmail = ""
        if  emergencyContactTextBox.text?.characters.count > 0
        {
            reqObj.firstPersonEmail = emergencyContactTextBox.text
        }
        
        reqObj.secondPersonEmail = ""
        if  emergencyContactTextBox_middle.text?.characters.count > 0
        {
            reqObj.secondPersonEmail = emergencyContactTextBox_middle.text
        }
        
        reqObj.defaultPerson = ""
        
        if isEmergencyContactselected.isSelected
        {
            reqObj.defaultPerson = "1"
        }
        else if isEmergencyContactselected_middle.isSelected
        {
            reqObj.defaultPerson = "2"
        }
        
        EmergencyContactRequestor().sendSaveContactListRequest(reqObj, success:{ (success, object) in
            
            hideIndicator()
            
            if (object as! SaveEmergencyContactResponse).code == "4600"
            {
                self.contactNumber.isUserInteractionEnabled = false
                self.emergencyContactTextBox.isUserInteractionEnabled = false
                self.isEmergencyContactselected.isUserInteractionEnabled = false
                self.contactNumber_middle.isUserInteractionEnabled = false
                self.emergencyContactTextBox_middle.isUserInteractionEnabled = false
                self.isEmergencyContactselected_middle.isUserInteractionEnabled = false
                self.updateContactButton_dup.isHidden = true
                self.addContactButton.isHidden = true
                self.saveContactButton.isHidden = true
                self.saveContactButton_dup.isHidden = true
                self.addContactButton_dup.isHidden = true
                UserInfo.sharedInstance.isDefaultEmergencyNumberSelected = reqObj.defaultPerson!
                if(reqObj.defaultPerson == "1") {
                    UserInfo.sharedInstance.selectedEmergencyNumber = reqObj.firstPersonNumber!
                    UserInfo.sharedInstance.selectedEmergencyEmail = reqObj.firstPersonEmail!
                } else {
                    UserInfo.sharedInstance.selectedEmergencyNumber = reqObj.secondPersonNumber!
                    UserInfo.sharedInstance.selectedEmergencyEmail = reqObj.secondPersonEmail!
                }
                if(action == "delete") {
                    AlertController.showToastForInfo("Contact deleted successfully.")
                } else {
                    AlertController.showToastForInfo("Contact List updated successfully.")
                }
                if(self.contactNumber_middle.text?.characters.count > 0) {
                    self.addContactButton_mid.isHidden = true
                    self.footerLbl.isHidden = true
                } else {
                    self.addContactButton_mid.isHidden = false
                    self.footerLbl.isHidden = false
                }
            }
            else
            {
                AlertController.showToastForError("Record Not Found.")
            }
            
        }){ (error) in
            hideIndicator()
            AlertController.showAlertForError(error)
        }
        
    }
}
